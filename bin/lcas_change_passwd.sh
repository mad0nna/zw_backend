#!/bin/sh
# vim: ts=2 sw=2 et ff=unix fenc=utf8
# OVERVIEW
#  Change user password
# USAGE
#  lcas_change_user_passwd.sh <EMAIL> <NEW_PASSWORD>

TAG=lcas_change_user_passwd

usage () {
  echo "lcas_change_user_passwd.sh <EMAIL> <NEW_PASSWORD>"
  exit 1
}

log () {
  logger -t $TAG -i -s -p user.$1 "$1: $2"
}

# Constants
# Password salt. See /var/www/lcas/http_amqp_node/config/env/config_prod.php
SALT="19df4c881864075864be5ddbb24eb8116bbaebb5"
# DB access username, password and DB name
DB_USER=lcas
DB_PASS="L!veC0nnect"
DB_NAME=lcas
DB_HOST=localhost

# Check arguments
if [ $# -ne 2 ]; then
  usage
fi
EMAIL=$1
PASS=$2
log info "About to update password for $EMAIL"
HASH=`echo -n "${SALT}${PASS}" | sha1sum | cut -d' ' -f1`
SQL="UPDATE users SET pass = '${HASH}' WHERE email = '${EMAIL}';SELECT ROW_COUNT();"
TEMP=`echo ${SQL} | mysql -u ${DB_USER} -p${DB_PASS} -h ${DB_HOST} ${DB_NAME}`
N_ROWS=`echo $TEMP | cut -d' ' -f2`
if [ "${N_ROWS}" -ne 1 ]; then
  log warn "Failed to update the password for ${EMAIL}. # affected rows = ${N_ROWS}"
  exit 1
fi
log info "Updating password for ${EMAIL} successfully done."
