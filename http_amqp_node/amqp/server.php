<?php

/**
 * AMQPノードのエントリポイント。
 */

ini_set('display_errors', 0);
ini_set('max_execution_time', 0);

if(!isset($_SERVER['PHP_ENV'])) {
    throw new RuntimeException('PHP_ENV 定数が定義されていません。');
}

//アプリケーションの環境を表す定数
define('ENV', $_SERVER['PHP_ENV']);
//define('WHICH_AMQP', $_SERVER['WHICH_AMQP']);

require_once __DIR__ . '/../config/config_amqp.php';


$application = new \Lcas\AmqpNode\Application();

$application->run();
