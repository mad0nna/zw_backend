<?php

/**
 * LCASの管理を行うコマンド
 */

use Symfony\Component\Console\Application;

ini_set('display_errors', 0);
ini_set('max_execution_time', 0);

if(!isset($_SERVER['PHP_ENV'])) {
    throw new RuntimeException('PHP_ENV 定数が定義されていません。');
}

//アプリケーションの環境を表す定数
define('ENV', $_SERVER['PHP_ENV']);

require_once __DIR__ . '/../config/config_cli.php';


$application = new Application();
$application->addCommands(array(
    new \Lcas\Command\AmqpLogCleanCommand(),
    new \Lcas\Command\DeviceDataCleanCommand(),
    new \Lcas\Command\ScenarioEventCleanCommand(),
    new \Lcas\Command\SnsEndpointCleanCommand(),
));
$application->run();
