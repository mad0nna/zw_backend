<?php

/**
 * http-node用設定ファイル
 */

use Lcas\Log\Logger;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/constants.php';

ini_set('log_errors', 1);

//ログの設定は各config_x.phpで行う。
$defaultLogger = new \Monolog\Logger('default');
Logger::setLogger('default', $defaultLogger);


if(!defined('ENV')) {
    throw new RuntimeException('ENV定数が定義されていません。');
}

//環境別設定ファイルの読み込み
$envConfigPath = __DIR__ . sprintf('/env/%s/config_cli.php', basename(ENV));
if(!file_exists($envConfigPath)) {
    throw new RuntimeException("設定ファイル'{$envConfigPath}'が見つかりません。");
}

require_once $envConfigPath;
