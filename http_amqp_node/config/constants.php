<?php

//------------------------------------------------
// 環境・パス設定
define('ENV_DEV', 'dev');
define('ENV_PROD', 'production');

define('ROOT_DIR', dirname(__DIR__));
define('CONFIG_DIR', __DIR__);

//------------------------------------------------
// フラグ値の定義

// デバイスの通信方法（未使用）
define('CAPABILITY_ZWAVE', 1);
define('CAPABILITY_IP_CAMERA', 2);
define('CAPABILITY_BLUETOOTH', 3);
define('CAPABILITY_DUST_IP', 4);

// デバイスタイプ
define('DEVICE_TYPE_TEMPERATURE', 1);
define('DEVICE_TYPE_HUMIDITY', 2);
define('DEVICE_TYPE_MOTION', 3);
define('DEVICE_TYPE_OPEN_CLOSE', 4);
define('DEVICE_TYPE_LOCK', 5);
define('DEVICE_TYPE_COVER', 6);
define('DEVICE_TYPE_BATTERY', 7);
define('DEVICE_TYPE_LUMINANCE', 8);
define('DEVICE_TYPE_CALL_BUTTON', 9);
define('DEVICE_TYPE_SMART_LOCK', 10);
define('DEVICE_TYPE_POWER', 11);
define('DEVICE_TYPE_ACC_ENERGY', 12);
define('DEVICE_TYPE_POWER_SWITCH', 13);
define('DEVICE_TYPE_APPARENT_ENERGY', 14);
define('DEVICE_TYPE_ACC_GAS', 15);
define('DEVICE_TYPE_ACC_WATER', 16);
define('DEVICE_TYPE_HEART_RATE', 17);
define('DEVICE_TYPE_BREATHING_RATE', 18);
define('DEVICE_TYPE_BODY_MOTION', 19);
define('DEVICE_TYPE_SMOKE', 20);
define('DEVICE_TYPE_PERSON_STATE', 21);
define('DEVICE_TYPE_DISTANCE', 22);
define('DEVICE_TYPE_PARKERS_CAR', 23);
define('DEVICE_TYPE_PARKERS_LED', 24);
define('DEVICE_TYPE_PARKERS_MODE', 25);
define('DEVICE_TYPE_PARKERS_BATTERY', 26);

// ゲートウェイの状態
define('GATEWAY_STATE_CONNECTED', 1);
define('GATEWAY_STATE_DISCONNECTED', 2);
define('GATEWAY_STATE_INCLUSION', 3);
define('GATEWAY_STATE_EXCLUSION', 4);
define('GATEWAY_STATE_INITIAL', 5);
define('GATEWAY_STATE_CONFIGURING', 6);
define('GATEWAY_STATE_JOINING', 7);
define('GATEWAY_STATE_NORMAL', 8);
define('GATEWAY_STATE_FAILED', 9);

// ノードの状態
define('NODE_STATE_NOTINCLUDED', 0);
define('NODE_STATE_INCLUDED',    1);
define('NODE_STATE_FAILED',      2);

// ノードの状態（こちらの方が本物）
define('NODE_YET_ANOTHER_STATE_UNKNOWN', 0);
define('NODE_YET_ANOTHER_STATE_OPERATIONAL', 1);
define('NODE_YET_ANOTHER_STATE_LOST', 2);
define('NODE_YET_ANOTHER_STATE_NEGOTIATING', 3);

// 通信プロトコル
define('NODE_PROTOCOL_ZWAVE', 1);

// 未使用
define('NODE_ACTION_TYPE_BINARY', 1);
define('NODE_ACTION_TYPE_RANGE', 2);
define('NODE_ACTION_TYPE_TRIGGER', 3);

// ノードの共有レベル
define('NODE_SHARING_LEVEL_PRIVATE', 1);
define('NODE_SHARING_LEVEL_USERS', 2);

// ラベルのタイプ
define('LABEL_TYPE_GATEWAY',  1);
define('LABEL_TYPE_NODE',     2);
define('LABEL_TYPE_DEVICE',   3);
define('LABEL_TYPE_SCENARIO', 4);
define('LABEL_TYPE_USER',     5);
define('LABEL_TYPE_DASHBOARD_SCENARIO_STATUS', 6);

// ユーザーへの通知方法
define('PLATFORM_NONE', 1);
define('PLATFORM_APNS', 2);
define('PLATFORM_GCM',  3);
define('PLATFORM_SQS',  4);

// シナリオイベント発生時の動作
define('SCENARIO_ACTION_TYPE_SEND_MESSAGE', 1);

// シナリオ条件同士を繋ぐ演算子
define('SCENARIO_CONDITION_OPERATOR_AND', 1);
define('SCENARIO_CONDITION_OPERATOR_OR', 2);

// シナリオ条件の閾値への演算子
define('SCENARIO_OPERATOR_LESS', 1);
define('SCENARIO_OPERATOR_LESS_EQ', 2);
define('SCENARIO_OPERATOR_EQ', 3);
define('SCENARIO_OPERATOR_GREATER', 4);
define('SCENARIO_OPERATOR_GREATER_EQ', 5);
define('SCENARIO_OPERATOR_FREQ', 6);

// シナリオの種類
define('SCENARIO_TYPE_SIMPLE', 1);
define('SCENARIO_TYPE_WBGT', 2);
define('SCENARIO_TYPE_SCXML', 3);

// ユーザー認証結果
define('AUTH_RESULT_OK', 1);
define('AUTH_RESULT_NOT_PROVIDED', 2);
define('AUTH_RESULT_NOT_FOUND', 3);
define('AUTH_RESULT_EXPIRED', 4);

// ZIO からの通知の処理状況
define('ZWORKS_LOG_STATE_UNREAD', 0);
define('ZWORKS_LOG_STATE_READ', 1);
define('ZWORKS_LOG_STATE_PROCESSED', 2);

// ZIO へのリクエストの処理状況
define('ZWORKS_REQUEST_STATE_UNREAD', 0);
define('ZWORKS_REQUEST_STATE_READ', 1);
define('ZWORKS_REQUEST_STATE_PROCESSED', 2);

// ユーザー情報操作時にユーザーへ通知するトークンの種類
define('USER_CONFIRM_TOKEN_TYPE_NEW_USER', 1);
define('USER_CONFIRM_TOKEN_TYPE_CHANGE_EMAIL', 2);
define('USER_CONFIRM_TOKEN_TYPE_RESET_PASSWORD', 3);

// メールテンプレートの種類
define('MAIL_TEMPLATE_TYPE_NEW_USER', 1);
define('MAIL_TEMPLATE_TYPE_CHANGE_EMAIL', 2);
define('MAIL_TEMPLATE_TYPE_RESET_PASSWORD', 3);


//------------------------------------------------
// 設定値

// 認証トークン
define('AUTH_TOKEN_HEADER_NAME', 'X-LCAS-API-TOKEN');
define('AUTH_SESS_COOKIE_NAME', 'SESSID');
define('AUTH_TOKEN_LENGTH', 32);


//ユーザーのパスワード設定
define('PASSWORD_LENGTH_MIN', 6);
define('PASSWORD_LENGTH_MAX', 16);
define('PASSWORD_SPECIAL_CHARS', "@%+\/’!#$^?:,(){}[]~-_");

