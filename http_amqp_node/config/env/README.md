config/envディレクトリについて
==============================
このディレクトリは環境毎にディレクトリを作成し、その中に各種設定値を定義します。
各ディレクトリには以下のファイルが必要です。

 - config_common.php
 - config_http.php
 - config_amqp.php
 - config_cli.php

新しく環境を作成する際は、"_template"ディレクトリを複製して作成してください。
