<?php

/**
 * 環境別設定ファイル(amqp-node用)。
 */

use Lcas\Log\Logger;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy;
use Monolog\Handler\FingersCrossedHandler;
use Monolog\Logger as MonoLogLogger;

require_once __DIR__ . '/config_common.php';

//---------------------------------
// エラーログ設定
ini_set('display_errors', 0);
ini_set('error_log', ROOT_DIR . '/../data/logs/amqp-node.log');
$defaultLogger = Logger::get();

//DEBUGレベル以上を全て記録。
//amqp-nodeはライフサイクルが長い(基本的に常駐)ため、
//FingersCrossedHandlerを使用した場合メモリを大量消費する懸念があります。
$defaultLogger->pushHandler(new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, MonologLogger::DEBUG));

//ログのレベル：
// * DEBUG
// * INFO
// * NOTICE
// * WARNING
// * ERROR
// * CRITICAL
// * ALERT
// * EMERGENCY
