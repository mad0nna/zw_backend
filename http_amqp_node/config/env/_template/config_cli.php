<?php

/**
 * 環境別設定ファイル(CLI用)。
 */

use Lcas\Log\Logger;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Logger as MonoLogLogger;
use Monolog\Processor\UidProcessor;

require_once __DIR__ . '/config_common.php';

//---------------------------------
// データの保持期限の設定

define('HOLDING_PERIOD_DAYS_ZWORKS_NOTIFICATION', 30);  //AMQP通知の保持期間
define('HOLDING_PERIOD_DAYS_DEVICE_DATA', 3 * 356);  //デバイスデータの保持期間： 3年間
define('HOLDING_PERIOD_DAYS_SCENARIO_EVENT', 3 * 356);  //シナリオイベントの保持期間： 3年間

//---------------------------------
// エラーログ設定
ini_set('display_errors', 0);
ini_set('error_log', ROOT_DIR . '/../data/logs/cli.log');
$defaultLogger = Logger::get();

//検証用設定例
//デバッグレベル以上を全て記録。
//複数のプロセスが並列でログを記録した場合に備え、ログの各行にuidを追加。
$defaultLogger->pushHandler(new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, MonologLogger::DEBUG));
$defaultLogger->pushProcessor(new UidProcessor());

//ログのレベル：
// * DEBUG
// * INFO
// * NOTICE
// * WARNING
// * ERROR
// * CRITICAL
// * ALERT
// * EMERGENCY
