<?php

/**
 * 環境別設定ファイル(http-node, amqp-node共通)。
 */

use Lcas\Log\Logger;
use Lcas\ServiceContainer;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy;
use Monolog\Handler\FingersCrossedHandler;
use Monolog\Processor\UidProcessor;
use Monolog\Logger as MonoLogLogger;


//---------------------------------
// DB接続情報
define('DB_HOST', '');
define('DB_PORT', '');
define('DB_USER', '');
define('DB_PASS', '');
define('DB_NAME', '');

//---------------------------------
// AMQP接続情報
define('AMQP_HOST', '');
define('AMQP_PORT', '5672');
define('AMQP_USER', '');
define('AMQP_PASS', '');
define('AMQP_VHOST', '/v1/intermediate');
define('AMQP_EXCHANGE_NAME', 'intermediate'); // Intermediate -> Server
define('AMQP_QUEUE_NAME', 'intermediate_1');  // Server-> Intermediate
define('AMQP_TLS_CLIENT_CERT', '');
define('AMQP_TLS_CA_CERT',     '');
define('AMQP_MAX_RECONNECTION_TRY', 10);  //AMQP consumer用：接続断が発生した時に再接続する回数(再接続待ち時間は(N-1)^2秒)
define('AMQP_HEARTBEAT', 300);            //AMQP consumer用：ハートビート間隔
define('AMQP_REQUEST_TIMEOUT', 10);     //メッセージ送信クライアント用：AMQPへのExchange発行からタイムアウト判定するまでの秒数
define('AMQP_POLLING_INTERVAL', 1000);  //メッセージ送信クライアント用：AMQPのキューを監視する間隔(msec)

//---------------------------------
// AWS接続情報
define('AWS_ACCESS_KEY',      '');  //AWS APIのアクセスキー
define('AWS_SECRET_KEY',      '');
define('AWS_REGION',          'ap-northeast-1');  //SNS API呼び出し時に使用するリージョン
define('AWS_SNS_API_VERSION', '2010-03-31');      //SNS API呼び出し時に使用するAPI バージョン
define('AWS_SNS_API_PROXY',   '');                //Proxy環境下でAWS APIを呼び出す場合のみ指定。"http://xxx.xxx.xxx:xxx"
define('AWS_SNS_ANDROID_APP_ARN', '');            //Android への通知に使用するSNSのApplication ARN
define('AWS_SNS_IOS_APP_ARN', '');                //iOS への通知に使用するSNSのApplication ARN

//---------------------------------
// SALT値
//ユーザーのパスワード用
define('SALT_PASSWORD', '--CHANGE-ME--');

//---------------------------------
// サービス設定
$serviceContainer = ServiceContainer::getInstance();
$serviceContainer->set('zworks_request_adapter', new \Lcas\Adapter\ZworksRequestAdapterAmqp());
//zworks_request_adapterに指定可能な値
//$serviceContainer->set('zworks_request_adapter', new \Lcas\Adapter\ZworksRequestAdapterStub());
//$serviceContainer->set('zworks_request_adapter', new \Lcas\Adapter\ZworksRequestAdapterFixedGatewayStub());

//"Lcas\Adapter\ZworksRequestAdapterStub":
//  このクラスを指定した場合、実際のAMQPメッセージの宗信は行わず、AMQPメッセージが送信成功した場合の
//  応答が連想配列で返されます。Gateway Association Requestsの場合、新しいNode 3つを含むゲートウェイの情報が
//  連想配列で返されます。
//  実際の動作と異なり、同じMac Addressを指定しても毎回異る(毎回新しい)Nodeが3つ含まれて返されます。
//  AMQPサーバが存在しない環境で指定します。
//
//"Lcas\Adapter\ZworksRequestAdapterFixedGatewayStub":
//  このクラスを指定した場合、AMQPのメッセージの宗信は行わず、ダミーのレスポンスが返される点は
//  "ZworksRequestAdapterStub"と同じ動作となります。
//  Gateway Association Requestの際、Mac Addressを"1000000"、"2000000"のように
//  百万単位の数値で指定する必要があるものの、使用したMac Addressに対し毎回同じNodeの一覧を返す動作となります。

$serviceContainer->set('mobile_push_adapter', new \Lcas\Adapter\MobilePushAdapterSNS());
//mobile_push_adapterに指定可能な値
//$serviceContainer->set('mobile_push_adapter', new \Lcas\Adapter\MobilePushAdapterStub());

//"Lcas\Adapter\MobilePushAdapterStub":
//  このクラスを指定した場合、AWS APIの発行を含め何も処理を行いません。
//  AWS APIが利用出来ない環境で指定します。

//---------------------------------
// デバッグ用設定
//define('DEBUG_ALLOW_SIMPLE_PASSWORD', 1);  //単純なパスワードを許可
define('DEBUG_LOG_DB_QUERIES', 1);         //DBクエリをDEBUGレベルでログに記録
