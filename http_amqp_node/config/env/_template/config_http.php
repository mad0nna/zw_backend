<?php

/**
 * 環境別設定ファイル(http-node用)。
 */

use Lcas\Log\Logger;
use Lcas\ServiceContainer;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy;
use Monolog\Handler\FingersCrossedHandler;
use Monolog\Logger as MonoLogLogger;
use Monolog\Processor\UidProcessor;

require_once __DIR__ . '/config_common.php';

//---------------------------------
// URL設定
//APIのURLが"/some/dir/api/v3"のように
//"/api/v3"よりも上位の階層を含む場合、上位のディレクトリを指定する(/some/dir)。
define('HTTP_API_URL_PREFIX', '');

define('HTTP_API_FULL_URL_PREFIX', 'http://example.com' . HTTP_API_URL_PREFIX);


//---------------------------------
// 認証
// ユーザー認証のトークンの有効期間
define('AUTH_TOKEN_LIFE_TIME', 24 * 60 * 60 * 30);  //30日間
// CROSS_DOMAIN の Cookie がうまく扱えないブラウザがある場合に、カスタムヘッダに AUTH_TOKEN を含めるかどうか？
define('FORCE_CROSS_DOMAIN_COOKIE', false);

//---------------------------------
// デフォルトメールテンプレート(新規ユーザーの追加)
// 送信者
define('DEFAULT_MAIL_TEMPLATE_NEW_USER_CONFIRMATION_SENDER', 'noreply@liveconnect.jp');
// 件名
define('DEFAULT_MAIL_TEMPLATE_NEW_USER_CONFIRMATION_SUBJECT', '【LiveConnect】仮登録を受け付けました（本登録手続きのお願い）');
// 本文
define('DEFAULT_MAIL_TEMPLATE_NEW_USER_CONFIRMATION_CONTENT', "この度は、LiveConnect（ライブコネクト）への新規ユーザー登録申請、誠にありがとうございます。\n下記URLをクリックして本登録を完了してください。\n\nhttps://app-test.liveconnect.jp/signup-password.html?token={confirmation_token}&email={email_address}\n\n※URLの有効期限は 24 時間です。\n\nもし有効期限が切れてしまいました場合は、お手数ですが、再度登録申請をお願いいたします。\n\nなお、万が一、仮登録にお心当たりの無い場合はお手数ですが本メールを破棄頂きますようお願い申し上げます。\n本登録を行わなければ登録完了とはなりませんので、退会手続き等は必要ございません。");
define('DEFAULT_MAIL_TEMPLATE_NEW_USER_CONFIRMATION_CONTENT_TYPE', "text/plain");

//---------------------------------
// デフォルトメールテンプレート(メールアドレスの変更)
// 送信者
define('DEFAULT_MAIL_TEMPLATE_CHANGE_EMAIL_CONFIRMATION_SENDER', 'noreply@liveconnect.jp');
// 件名
define('DEFAULT_MAIL_TEMPLATE_CHANGE_EMAIL_CONFIRMATION_SUBJECT', '【LiveConnect】メールアドレス再設定のお願い');
// 本文
define('DEFAULT_MAIL_TEMPLATE_CHANGE_EMAIL_CONFIRMATION_CONTENT', "メールアドレス再設定のリクエストを受け付けました。\nLiveConnect（ライブコネクト）アカウントの\nメールアドレス再設定をリクエストされた場合は\n以下のURLをクリックし、パスワードを再登録のうえ\n設定を完了させてください。\n身に覚えがない場合にはこのメールは無視してください。\n\nhttps://app-test.liveconnect.jp/password-resetting.html?token={confirmation_token}&email={email_address}&action=update");
define('DEFAULT_MAIL_TEMPLATE_CHANGE_EMAIL_CONFIRMATION_CONTENT_TYPE', "text/plain");

//---------------------------------
// デフォルトメールテンプレート(パスワードのリセット)
// 送信者
define('DEFAULT_MAIL_TEMPLATE_RESET_PASSWORD_CONFIRMATION_SENDER', 'noreply@liveconnect.jp');
// 件名
define('DEFAULT_MAIL_TEMPLATE_RESET_PASSWORD_CONFIRMATION_SUBJECT', '【LiveConnect】パスワードのリセット・再設定のお願い');
// 本文
define('DEFAULT_MAIL_TEMPLATE_RESET_PASSWORD_CONFIRMATION_CONTENT', "パスワード再設定のリクエストを受け付けました。\nLiveConnect（ライブコネクト）アカウントの\nパスワード再設定をリクエストされた場合は以下のURLをクリックしてください。\n身に覚えがない場合にはこのメールは無視してください。\n\nhttps://app-test.liveconnect.jp/password-resetting.html?token={reset_token}&email={email_address}&action=reset");
define('DEFAULT_MAIL_TEMPLATE_RESET_PASSWORD_CONFIRMATION_CONTENT_TYPE', "text/plain");

//---------------------------------
// メール本文のプレースホルダー
define('MAIL_PLACEHOLDER_EMAIL', '{email_address}');
define('MAIL_PLACEHOLDER_CONFIRMATION_TOKEN', '{confirmation_token}');
define('MAIL_PLACEHOLDER_RESET_TOKEN', '{reset_token}');

//---------------------------------
// エラーログ設定
ini_set('display_errors', 0);
ini_set('error_log', ROOT_DIR . '/../data/logs/http-node.log');
$defaultLogger = Logger::get();

//検証用設定例
//DEBUGレベル以上を全て記録。
//複数のHTTPリクエストが並列してログを記録した場合に備え、ログの各行にUIDを追加。
$defaultLogger->pushHandler(new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, MonologLogger::DEBUG));
$defaultLogger->pushProcessor(new UidProcessor());

//本番用設定例
//INFOレベル以上を全て記録。
//ERRORレベル異常が発生した場合、その時点で未出力だった(DEBUGを含む)全てのログを出力
//$defaultLogger->pushHandler(
//    new FingersCrossedHandler(
//        new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, MonologLogger::INFO),
//        new ErrorLevelActivationStrategy(MonologLogger::ERROR)
//    )
//);
//$defaultLogger->pushProcessor(new UidProcessor());

//ログのレベル：
// * DEBUG
// * INFO
// * NOTICE
// * WARNING
// * ERROR
// * CRITICAL
// * ALERT
// * EMERGENCY


//---------------------------------
// デバッグ用設定
//define('DEBUG_ALLOW_SIMPLE_PASSWORD', 1);  //単純なパスワードを許可
