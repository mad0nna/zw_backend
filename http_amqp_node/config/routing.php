<?php

/**
 * HTTP API用のルーティング情報定義ファイル
 */

// $mapping = array(
//     'method' => HTTP メソッド名
//     'pattern' => URL(正規表現で記述)
//     'controller' => 対応するコントローラ名(Lcas\Controller\x)
//     'action' => 実行するメソッド名
// );

$routeBase = preg_quote(HTTP_API_URL_PREFIX . '/api/v3', '#');
$v3ApiMapping = array(
    array('method' => 'GET',  'pattern' => "#^{$routeBase}/login$#", 'controller' => 'LoginController', 'action' => 'loginStatusAction'),
    array('method' => 'POST', 'pattern' => "#^{$routeBase}/login$#", 'controller' => 'LoginController', 'action' => 'loginAction'),

    array('method' => 'GET',    'pattern' => "#^{$routeBase}/gateways$#",                'controller' => 'GatewayController', 'action' => 'listUserGatewaysAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/gateways/(?P<id>[0-9]+)$#", 'controller' => 'GatewayController', 'action' => 'getGatewayDetailAction'),
    array('method' => 'POST',   'pattern' => "#^{$routeBase}/gateways$#",                'controller' => 'GatewayController', 'action' => 'associateGatewayAction'),
    array('method' => 'PUT',    'pattern' => "#^{$routeBase}/gateways/(?P<id>[0-9]+)$#", 'controller' => 'GatewayController', 'action' => 'updateLabelAction'),
    array('method' => 'DELETE', 'pattern' => "#^{$routeBase}/gateways/(?P<id>[0-9]+)$#", 'controller' => 'GatewayController', 'action' => 'dissociateGatewayAction'),
    array('method' => 'POST',   'pattern' => "#^{$routeBase}/gateways/(?P<id>[0-9]+)$#", 'controller' => 'GatewayController', 'action' => 'updateGatewayStateAction'),
    array('method' => 'POST',   'pattern' => "#^{$routeBase}/gateways/(?P<id>[0-9]+)/firmware$#", 'controller' => 'GatewayController', 'action' => 'updateFirmwareAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/gateways/(?P<id>[0-9]+)/publickey$#", 'controller' => 'GatewayController', 'action' => 'getPublicKeyAction'),
    array('method' => 'POST',   'pattern' => "#^{$routeBase}/gateways/(?P<id>[0-9]+)/publickey/generate$#", 'controller' => 'GatewayController', 'action' => 'updatePublicKeyAction'),
    
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/nodes$#",                'controller' => 'NodeController', 'action' => 'listUserNodeAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/nodes/(?P<id>[0-9]+)$#", 'controller' => 'NodeController', 'action' => 'getNodeDetailAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/nodes/(?P<id>[0-9]+)/users/(?P<user_id>[0-9]+)$#", 'controller' => 'NodeController', 'action' => 'getUserNodeDetailAction'),
    array('method' => 'PUT',    'pattern' => "#^{$routeBase}/nodes/(?P<id>[0-9]+)$#", 'controller' => 'NodeController', 'action' => 'updateNodeAction'),
    array('method' => 'PUT',    'pattern' => "#^{$routeBase}/nodes/(?P<id>[0-9]+)/users/(?P<user_id>[0-9]+)$#", 'controller' => 'NodeController', 'action' => 'updateUserNodeAction'),
    array('method' => 'DELETE', 'pattern' => "#^{$routeBase}/nodes/(?P<id>[0-9]+)$#", 'controller' => 'NodeController', 'action' => 'deleteNodeAction'),

    array('method' => 'GET',  'pattern' => "#^{$routeBase}/devices$#",                       'controller' => 'DeviceController', 'action' => 'listUserDeviceAction'),
    array('method' => 'GET',  'pattern' => "#^{$routeBase}/devices/(?P<id>[0-9]+)$#",        'controller' => 'DeviceController', 'action' => 'getDeviceDetailAction'),
    array('method' => 'GET',  'pattern' => "#^{$routeBase}/devices/(?P<id>[0-9]+)/values$#", 'controller' => 'DeviceController', 'action' => 'listDeviceValueAction'),
    array('method' => 'PUT',  'pattern' => "#^{$routeBase}/devices/(?P<id>[0-9]+)$#",        'controller' => 'DeviceController', 'action' => 'updateLabelAction'),
    array('method' => 'POST', 'pattern' => "#^{$routeBase}/devices/(?P<id>[0-9]+)$#",        'controller' => 'DeviceController', 'action' => 'controlDeviceAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/scenarios$#",                 'controller' => 'ScenarioController', 'action' => 'listUserScenarioAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/scenarios/(?P<id>[0-9]+)/events$#", 'controller' => 'ScenarioController', 'action' => 'listScenarioEventsAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/scenarios/all/events$#",      'controller' => 'ScenarioController', 'action' => 'listUserScenarioEventsAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/scenarios/(?P<id>[0-9]+)$#",  'controller' => 'ScenarioController', 'action' => 'getScenarioDetailAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/scenarios/(?P<id>[0-9]+)/value/(?P<value>[0-9]+)$#",  'controller' => 'ScenarioController', 'action' => 'hideScenarioTileAction'),
    array('method' => 'PUT',    'pattern' => "#^{$routeBase}/scenarios/(?P<id>[0-9]+)$#",  'controller' => 'ScenarioController', 'action' => 'modifyScenarioAction'),
    array('method' => 'POST',   'pattern' => "#^{$routeBase}/scenarios$#",                 'controller' => 'ScenarioController', 'action' => 'registerScenarioAction'),
    array('method' => 'DELETE', 'pattern' => "#^{$routeBase}/scenarios/(?P<id>[0-9]+)$#",  'controller' => 'ScenarioController', 'action' => 'removeScenarioAction'),
    array('method' => 'POST',   'pattern' => "#^{$routeBase}/scenarios/autoconfig$#",                 'controller' => 'ScenarioController', 'action' => 'registerAutoConfigScenarioAction'),
    array('method' => 'DELETE', 'pattern' => "#^{$routeBase}/scenarios/(?P<id>[0-9]+)/autoconfig$#",  'controller' => 'ScenarioController', 'action' => 'removeAutoConfigScenarioAction'),
    
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/users/(?P<id>[0-9]+)$#", 'controller' => 'UserController', 'action' => 'getUserDetailAction'),
    array('method' => 'POST',   'pattern' => "#^{$routeBase}/users$#",                'controller' => 'UserController', 'action' => 'registerUserAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/users/self$#",           'controller' => 'UserController', 'action' => 'getUserAction'),
    array('method' => 'PUT',    'pattern' => "#^{$routeBase}/users/self$#",           'controller' => 'UserController', 'action' => 'updateUserAction'),
    array('method' => 'POST',   'pattern' => "#^{$routeBase}/users/confirmation$#",   'controller' => 'UserController', 'action' => 'confirmUserAction'),
    array('method' => 'POST',   'pattern' => "#^{$routeBase}/users/resettoken$#",     'controller' => 'UserController', 'action' => 'resetUserAction'),
    array('method' => 'DELETE', 'pattern' => "#^{$routeBase}/users/self$#",           'controller' => 'UserController', 'action' => 'removeUserAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/users/dashboard/status$#",         'controller' => 'UserController', 'action' => 'listUserDashboardStatusAction'),
    array('method' => 'PUT',    'pattern' => "#^{$routeBase}/users/dashboard/status$#",         'controller' => 'UserController', 'action' => 'updateUserDashboardStatusAction'),

    array('method' => 'POST',    'pattern' => "#^{$routeBase}/dashboard$#",           'controller' => 'DashboardController', 'action' => 'updateTileAction'),
    array('method' => 'GET',    'pattern' => "#^{$routeBase}/dashboard$#",           'controller' => 'DashboardController', 'action' => 'getUserTileAction'),
);

$mapping = array_merge($v3ApiMapping);
