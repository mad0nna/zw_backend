DROP TABLE IF EXISTS `scenario_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scenario_messages` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `timeline_message` VARCHAR(255) NOT NULL,
  `tile_message` VARCHAR(255) NOT NULL,
  `alert_message` VARCHAR(255) NOT NULL,
  `status_image` VARCHAR(255) NULL,
  `created` TIMESTAMP DEFAULT current_timestamp,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('﻿1', '鍵閉め忘れシナリオ', '鍵が開いたままになっています。', '鍵閉め忘れ', '鍵閉め忘れ検知', 'common/img/scenario/status/unlocked-door-detected.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('2', '生活見守りシナリオ', '活動が少ないようです。', '活動なし', '生活見守り検知', 'common/img/scenario/status/no-movement-alert.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('3', '熱中症危険度判定シナリオ', '熱中症の危険があります', '熱中症注意', '熱中症危険検知', 'common/img/scenario/status/risk-for-heat-stroke.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('4', 'ダニ・カビ発生注意判定シナリオ', 'ダニ・カビ発生の危険があります。', 'ダニ・カビ注意', 'ダニ・カビ注意検知', 'common/img/scenario/status/risk-for-mold.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('5', '食中毒注意判定シナリオ', '食中毒に注意が必要な室内環境になっています。', '食中毒注意', '食中毒注意検知', 'common/img/scenario/status/risk-for-food-poisoning.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('6', '風邪注意度判定シナリオ', '風邪に注意が必要な室内環境になっています。', '風邪注意', '風邪注意検知', 'common/img/scenario/status/risk-for-getting-a-cold.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('7', 'ドアの開閉シナリオ', '玄関のドアが開いています。', 'ドアオープン', 'ドアの開閉検知', 'common/img/scenario/status/door-opened.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('8', '心拍数の異常通知シナリオ', '心拍異常を検知しました', '心拍異常', '心拍数異常通知', 'common/img/scenario/status/heart-beat-is-irregular.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('9', '呼吸異常通知シナリオ', '呼吸異常を検知しました', '呼吸異常', '呼吸異常通知', 'common/img/scenario/status/breath-rate-is-irregular.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('10', '離床行動通知シナリオ', '離床行動を検知しました', '離床検知', '離床行動通知', 'common/img/scenario/status/left-bed-detected.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('11', '火災警報通知シナリオ', '火災を検知しました', '火災検知', '火災警報通知', 'common/img/scenario/status/fire-alert.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('12', '動き検知シナリオ', '見守っている方に動きがありました。', '動き検知', '動き検知', 'common/img/scenario/status/movement-detected.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('13', '離床検知', '離床行動を検知しました', '離床検知', '離床検知', 'common/img/scenario/status/left-bed-detected.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('14', '外出', 'Someone is leaving the house', '外出検知', '外出検知', 'common/img/scenario/status/left-home-detected.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('15', '帰宅', 'Someone is coming home', '帰宅検知', '帰宅検知', 'common/img/scenario/status/getting-home-detected.jpg', '');
INSERT INTO `scenario_messages` (`id`, `name`, `timeline_message`, `tile_message`, `alert_message`, `status_image`, `created`) VALUES ('16', '起床', 'Someone is waking up', '起床検知', '起床検知', 'common/img/scenario/status/waking-up-detected.jpg', '');