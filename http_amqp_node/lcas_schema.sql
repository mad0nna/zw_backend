-- MySQL dump 10.13  Distrib 5.6.27, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: lcas
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_tokens`
--

DROP TABLE IF EXISTS `auth_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_tokens` (
  `token` varbinary(64) NOT NULL DEFAULT '',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `expires` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `updated` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`token`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `capabilities`
--

DROP TABLE IF EXISTS `capabilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capabilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gateway_id` int(10) unsigned NOT NULL DEFAULT '0',
  `capability` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `idx_gateway_id` (`gateway_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_data`
--

DROP TABLE IF EXISTS `device_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_stamp` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `value` varchar(100) NOT NULL DEFAULT '',
  `unit` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_device_id_time_stamp` (`device_id`,`time_stamp`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `node_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `idx_node_id` (`node_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gateways`
--

DROP TABLE IF EXISTS `gateways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gateways` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mac_address` varchar(20) NOT NULL DEFAULT '',
  `fw_version` varchar(100) NOT NULL DEFAULT '',
  `manufacturer` varchar(100) NOT NULL DEFAULT '',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `prev_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `updated` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `public_key` VARBINARY(2048) NULL,
  `gw_name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_idx_mac_address` (`mac_address`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `labels`
--

DROP TABLE IF EXISTS `labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned,
  `label` varbinary(255) NOT NULL DEFAULT '',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `type` (`type`,`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `node_actions`
--

DROP TABLE IF EXISTS `node_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `node_id` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `idx_node_id` (`node_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nodes`
--

DROP TABLE IF EXISTS `nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nodes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gateway_id` int(10) unsigned NOT NULL DEFAULT '0',
  `protocol` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sharing_level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `location_type` varchar(100) NOT NULL DEFAULT '',
  `location_name` varchar(100) NOT NULL DEFAULT '',
  `manufacturer` varchar(100) NOT NULL DEFAULT '',
  `product` varchar(100) NOT NULL DEFAULT '',
  `serial_no` varchar(100) NOT NULL DEFAULT '',
  `prev_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `failed_cmd` varchar(255) NOT NULL DEFAULT '',
  `reason` varchar(50) NOT NULL DEFAULT '',
  `join_key_set` TINYINT(3) NULL,
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `updated` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `yet_another_state` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `node_type` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_gateway_id` (`gateway_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nodes_users`
--

DROP TABLE IF EXISTS `nodes_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nodes_users` (
  `node_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`node_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scenario_actions`
--

DROP TABLE IF EXISTS `scenario_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scenario_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scenario_id` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `idx_scenario_id` (`scenario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scenario_conditions`
--

DROP TABLE IF EXISTS `scenario_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scenario_conditions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scenario_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lhs` int(10) unsigned NOT NULL DEFAULT '0',
  `operator` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rhs` varchar(50) NOT NULL DEFAULT '',
  `unit` varchar(20) NOT NULL DEFAULT '',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `idx_scenario_id` (`scenario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scenario_events`
--

DROP TABLE IF EXISTS `scenario_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scenario_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scenario_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message` varchar(100) NOT NULL DEFAULT '',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `idx_scenario_id` (`scenario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scenarios`
--

DROP TABLE IF EXISTS `scenarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scenarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `scenario_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `condition_operator` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `trigger_after` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `temperature_dev` int(10) unsigned,
  `humidity_dev` int(10) unsigned,
  `wbgt_threshold` mediumint(8),
  `scxml` varchar(12000),
  `device_list` varchar(256),
  `action_state_list` varchar(1024),
  `enabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `enabled_period` varchar(11),
  `enabled_days` varchar(27),
  `auto_config` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `updated` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varbinary(255) NOT NULL DEFAULT '',
  `pass` varbinary(100) NOT NULL DEFAULT '',
  `platform` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `token` varbinary(255) NOT NULL DEFAULT '',
  `push_endpoint_arn` varbinary(255) NOT NULL DEFAULT '',
  `origin` varchar(255) NOT NULL DEFAULT '',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `updated` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_idx_email` (`email`),
  UNIQUE KEY `u_idx_login_id` (`login_id`),
  UNIQUE KEY `u_idx_name` (`name`),
  KEY `idx_email_pass` (`email`,`pass`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zworks_notifications`
--

DROP TABLE IF EXISTS `zworks_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zworks_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `correlation_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `updated` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `idx_created` (`created`),
  KEY `idx_correlation_id` (`correlation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zworks_requests`
--

DROP TABLE IF EXISTS `zworks_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zworks_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `command` varchar(50) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  `extra_data` text NOT NULL,
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `updated` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `idx_created` (`created`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-03 13:47:13

DROP TABLE IF EXISTS `user_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_tokens` (
  `token` varchar(255) NOT NULL,
  `user_id` int(10) unsigned,
  `token_type` int(10) unsigned NOT NULL,
  `email` varbinary(255) NOT NULL DEFAULT '',
  `platform` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `platform_token` varbinary(255) NOT NULL DEFAULT '',
  `origin` varchar(255) NOT NULL DEFAULT '',
  `expires` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`token`),
  UNIQUE KEY `u_idx_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `mail_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `origin` varchar(255) NOT NULL DEFAULT '',
  `platform` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `template_type` int(10) unsigned NOT NULL DEFAULT 0,
  `sender` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(1024) NOT NULL DEFAULT '',
  `content-type` varbinary(255) NOT NULL DEFAULT 'text/plain',
  `content` varchar(4096) NOT NULL DEFAULT '',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `updated` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_idx_email` (`origin`, `template_type`, `platform`),
  KEY `idx_origin_template_type_id` (`origin`, `template_type`, `platform`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `node_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_configs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `node_id` int(10) unsigned NOT NULL DEFAULT '0',
  `config_type` varchar(255) NOT NULL DEFAULT '',
  `param_type` varchar(255) NOT NULL DEFAULT '',
  `param_size` int(10) unsigned NOT NULL DEFAULT '0',
  `param_id` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT '',
  `update_pending` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `updated` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_idx_node_id_config_type_param_id` (`node_id`, `config_type`, `param_id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `firmware_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `firmware_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(255) DEFAULT NULL,
  `datetime` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `nodes_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nodes_apps` (
  `app_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, 
  `node_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `app_name` VARCHAR(45) NOT NULL,
  `created` DATETIME(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`app_id`, `node_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `apps_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_params` (
  `param_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, 
  `app_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `param_name` VARCHAR(45) NOT NULL,
  `param_value` VARBINARY(4096) NULL,
  `created` DATETIME(6) NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`param_id`, `app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `dust_gateways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dust_gateways` (
  `dust_gateway_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gateway_id` INT(10) UNSIGNED NOT NULL,
  `network_id` INT(10) UNSIGNED NULL,
  `created` DATETIME(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `updated` DATETIME(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  PRIMARY KEY (`dust_gateway_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `dashboard_tiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_tiles` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL ,
  `type` ENUM('device', 'scenario') NOT NULL,
  `entity_id` INT NOT NULL,
  `sequence` INT NOT NULL,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP,
  PRIMARY KEY (`id`));
/*!40101 SET character_set_client = @saved_cs_client */;