ALTER TABLE `gateways` 
ADD COLUMN `status` tinyint DEFAULT 0 AFTER `gw_name`;
 
ALTER TABLE `scenarios` 
ADD COLUMN `tile_status` tinyint DEFAULT 0 AFTER `auto_config`;