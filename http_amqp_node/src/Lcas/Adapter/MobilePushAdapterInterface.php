<?php

/**
 * @file
 * @brief MobileへのPush通知のインターフェース
 */

namespace Lcas\Adapter;


/**
 * @interface MobilePushAdapterInterface
 * @brief MobileへのPush通知のインターフェース
 */
interface MobilePushAdapterInterface{

    /**
     * デバイストークンからPush ID(Endpoint ARNに相当)を取得する
     * @param int $platform PLATFORM_X
     * @param string $token モバイルトークン
     * @return string PushId
     */
    public function registerToken($platform, $token);


    /**
     * 指定したPush ID(Endpoint ARNに相当)に対しPush通知を行う。
     * @param string $pushId PushId
     * @param string $message 通知メッセージ
     * @return void
     */
    public function publish($pushId, $message);


    /**
     * Endpoint ARNの一覧を取得する。
     * @param int $platform PLATFORM_XXX 定数
     * @param string $nextToken 次ページを参照するためのトークン
     * @return array Endpoint ARNやその属性を含んだ配列
     *
     * AWS SNS依存ではあるものの、戻り値はAWS SNS APIの
     * ListEndpointsByPlatformApplicationと同じ形式の配列を想定
     *
     * [Class Aws\Sns\SnsClient | AWS SDK for PHP](http://docs.aws.amazon.com/aws-sdk-php/v2/api/class-Aws.Sns.SnsClient.html#_listEndpointsByPlatformApplication)
     */
    public function getEndpointList($platform, $nextToken=null);

    /**
     * Endpointを削除する
     * @param string $endpointArn 削除対象のEndpoint ARN
     * @return boolean
     */
    public function deleteEndpoint($endpointArn);
}
