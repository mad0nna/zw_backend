<?php

/**
 * @file
 * @brief MobileへのPush通知の実装クラス(Amazon SNS)
 */

namespace Lcas\Adapter;


use Aws\Sns\SnsClient;
use Lcas\Log\Logger;


/**
 * @class MobilePushAdapterSNS
 * @brief  MobileへのPush通知の実装クラス(Amazon SNS)
 */
class MobilePushAdapterSNS implements MobilePushAdapterInterface{

    /**
     * @var SnsClient
     */
    private $snsClient;


    public function registerToken($platform, $token) {
        $client = $this->getClient();

        $appArn = '';
        switch($platform) {
            case PLATFORM_APNS: $appArn = AWS_SNS_IOS_APP_ARN; break;
            case PLATFORM_GCM:  $appArn = AWS_SNS_ANDROID_APP_ARN; break;
            case PLATFORM_NONE:
            case PLATFORM_SQS:
            	//None, SQSに対しては何も行わない。
                return '';
            default:
                throw new \InvalidArgumentException('無効なplatformが指定されました。 platform:' . $platform);
        }

        $request = array(
            'PlatformApplicationArn' => $appArn,
            'Token' => $token,
        );

        $logger = Logger::get();
        $logger->addDebug('SNS API CALL: createPlatformEndpoint: ' . var_export($request, true));
        $response = $client->createPlatformEndpoint($request);
        $logger->addDebug('SNS API RESPONSE: ' . var_export($response, true));

        $setAttributeRequest = array(
            'Attributes' => array('Enabled' => 'true'),
            'EndpointArn' => $response['EndpointArn'],
        );
        $logger->addDebug('SNS API CALL: setEndpointAttributes: ' . var_export($setAttributeRequest, true));
        $setAttributeResponse = $client->setEndpointAttributes($setAttributeRequest);
        $logger->addDebug('SNS API RESPONSE: ' . var_export($setAttributeResponse, true));

        return $response['EndpointArn'];
    }


    public function publish($pushId, $message) {

    }


    public function getEndpointList($platform, $nextToken=null) {
        $client = $this->getClient();

        $appArn = '';
        switch($platform) {
            case PLATFORM_APNS: $appArn = AWS_SNS_IOS_APP_ARN; break;
            case PLATFORM_GCM:  $appArn = AWS_SNS_ANDROID_APP_ARN; break;
            default:
                throw new \InvalidArgumentException('無効なplatformが指定されました。 platform:' . $platform);
        }

        $request = array(
            'PlatformApplicationArn' => $appArn,
        );
        if(!is_null($nextToken)) {
            $request['NextToken'] = $nextToken;
        }

        $logger = Logger::get();
        $logger->addDebug('SNS API CALL: listEndpointsByPlatformApplication: ' . var_export($request, true));
        $response = $client->listEndpointsByPlatformApplication($request);
        $logger->addDebug('SNS API RESPONSE: ' . var_export($response, true));

        return $response;
    }


    public function deleteEndpoint($endpointArn) {
        $client = $this->getClient();

        $request = array(
            'EndpointArn' => $endpointArn,
        );

        $logger = Logger::get();
        $logger->addDebug('SNS API CALL: deleteEndpoint: ' . var_export($request, true));
        $response = $client->deleteEndpoint($request);
        $logger->addDebug('SNS API RESPONSE: ' . var_export($response, true));

        return true;
    }


    private function getClient() {
        if($this->snsClient) {
            return $this->snsClient;
        }

        $options = array(
            'credentials' => array(
                'key' => AWS_ACCESS_KEY,
                'secret' => AWS_SECRET_KEY,
            ),
            'region' => AWS_REGION,
            'version' => AWS_SNS_API_VERSION,
        );

        if(AWS_SNS_API_PROXY != '') {
            $options['http'] = array(
                'proxy' => AWS_SNS_API_PROXY,
            );
        }

        $this->snsClient = new SnsClient($options);

        return $this->snsClient;
    }
}
