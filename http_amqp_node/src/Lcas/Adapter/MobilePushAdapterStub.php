<?php

/**
 * @file
 * @brief MobileへのPush通知のスタブ実装
 *
 * この実装では、各メソッドが呼ばれても何も行いません。
 * (AWS APIの利用出来ない環境を想定)
 */

namespace Lcas\Adapter;


use Aws\Sns\SnsClient;
use Lcas\Log\Logger;

/**
 * @class MobilePushAdapterStub
 * @brief MobileへのPush通知のスタブ実装(何もしない)
 *
 * この実装では、各メソッドが呼ばれても何も行いません。
 * (AWS APIの利用出来ない環境を想定)
 */
class MobilePushAdapterStub implements MobilePushAdapterInterface{

    public function registerToken($platform, $token) {
        //何も行わない
        return '';
    }



    public function publish($pushId, $message) {

    }

    public function getEndpointList($platform, $nextToken=null) {
        return array('Endpoints' => array());
    }

    public function deleteEndpoint($endpointArn) {

    }

}
