<?php

/**
 * @file
 * @brief z-works.ioへのリクエストの送信/通知の取得処理を行うアダプタ
 */

namespace Lcas\Adapter;
use Lcas\DB\DB;
use Lcas\Exception\ApiTimeoutException;
use Lcas\Log\Logger;
use Lcas\Repository\ZworksNotificationRepository;
use Lcas\Repository\ZworksRequestRepository;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPSSLConnection;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * @class ZworksRequestAdapterAmqp
 * @brief z-works.ioへのリクエストの送信/通知の取得処理を行うアダプタ
 */
class ZworksRequestAdapterAmqp implements ZworksRequestAdapterInterface {

    /**
     * @var AMQPStreamConnection
     */
    private $connection;

    /**
     * @var AMQPChannel
     */
    private $channel;

    private $command;
    private $correlationId;
    private $request;
    private $notification;

    private $zworksRequestRepository;
    private $zworksNotificationRepository;

    /**
     * コンストラクタ
     */
    public function __construct() {
        $this->connection = null;
        $this->command = '';
        $this->correrationId = 0;
        $this->request = null;
        $this->notification = null;


        //通知メッセージのポーリングはトランザクション内で行うため、
        //通常とは別のコネクションを使用して監視する。
        $forceNewConnection = true;
        $db = DB::getMasterDb($forceNewConnection);
        $this->zworksNotificationRepository = new ZworksNotificationRepository();
        $this->zworksNotificationRepository->setConnection($db);

        $this->zworksRequestRepository = new ZworksRequestRepository();
        $this->zworksRequestRepository->setConnection($db);
    }

    /**
     * 接続の初期化を行う。
     */
    public function initConnection() {
        if($this->connection) {
            return;
        }

        //TODO: ServiceContainerから取得する
        if(ENV == ENV_PROD || ENV == 'staging') {
            $ssl_options = array(
                'local_cert'    => AMQP_TLS_CLIENT_CERT,
                'cafile'        => AMQP_TLS_CA_CERT,
                'verify_peer'   => true,
            );
            $options = array(
                'login_method' => 'EXTERNAL',
                'connection_timeout' => 10,
            );
            $this->connection = new AMQPSSLConnection(AMQP_HOST, AMQP_PORT, AMQP_USER, AMQP_PASS, AMQP_VHOST, $ssl_options, $options);
        } else {
            $this->connection = new AMQPStreamConnection(AMQP_HOST, AMQP_PORT, AMQP_USER, AMQP_PASS, AMQP_VHOST);
        }

        $queueName = AMQP_QUEUE_NAME;
        $this->channel = $this->connection->channel();
//        //name: $queue
//        //passive: false
//        //durable: true // the queue will survive server restarts
//        //exclusive: false // the queue can be accessed in other channels
//        //auto_delete: false //the queue won't be deleted once the channel is closed.
//        $this->channel->queue_declare($queueName, false, true, false, false);
//
//        //name: $exchange
//        //type: direct
//        //passive: false
//        //durable: true // the exchange will survive server restarts
//        //auto_delete: false //the exchange won't be deleted once the channel is closed.
//        $exchangeName = AMQP_EXCHANGE_NAME;
//        $this->channel->exchange_declare($exchangeName, 'direct', false, true, false);
//        $this->channel->queue_bind($queueName, $exchangeName, $queueName);
    }


    public function sendMessage($request, $extraData=null) {
        $logger = Logger::get();

        if(!$this->connection) {
            $this->initConnection();
        }
        $this->request = $request;
        $json = json_encode($request);
        if(!$json) {
            throw new \RuntimeException('Failed to encode request into JSON. request: ' . $request);
        }

        $log = array(
            'command' => $request['command'],
            'data' => $json,
            'extra_data' => $extraData,
            'state' => ZWORKS_REQUEST_STATE_UNREAD,
        );
        $this->correlationId = $this->zworksRequestRepository->register($log);

        $logger->addDebug(sprintf('AMQP EXCHANGE: correlation_id: %d, %s', $this->correlationId, $json));

        $message = new AMQPMessage($json, array(
            'content_type' => 'application/json',
            'user_id'        => AMQP_USER,
            'delivery_mode' => 2,
            'correlation_id' => $this->correlationId,
        ));

        $this->channel->basic_publish($message, AMQP_EXCHANGE_NAME, '');
    }


    public function waitNotification($timeout=0) {
        $logger = Logger::get();

        $timeoutMsec = $timeout * 1000;
        if(!$timeoutMsec) {
            $timeoutMsec = AMQP_REQUEST_TIMEOUT * 1000;
        }

        for($i=0; $i < $timeoutMsec; $i+= AMQP_POLLING_INTERVAL) {
            $this->notification = $this->zworksNotificationRepository->findPeerNotification($this->correlationId);
            if($this->notification) {
                break;
            }
            usleep(AMQP_POLLING_INTERVAL * 1000);
        }

        if(!$this->notification) {
            return false;
        }

        $correlationId = $this->notification['correlation_id'];
        $logger->addDebug(sprintf('AMQP NOTIFICATION: correlation_id: %d, %s', $correlationId, var_export($this->notification, true)));
        return true;
    }


    public function getResult() {
        $logger = Logger::get();

        $result = json_decode($this->notification['data'], true);
        if(!$result) {
            $logger->addWarning(sprintf('Failed to decode notification.data into json. data: %s', var_export($this->notification, true)));
        }
        return $result;
    }

    public function complete() {
        $this->zworksRequestRepository->updateState($this->correlationId, ZWORKS_REQUEST_STATE_PROCESSED);
        $this->zworksNotificationRepository->updateStateByCorrelationId($this->correlationId, ZWORKS_LOG_STATE_PROCESSED);
    }

}
