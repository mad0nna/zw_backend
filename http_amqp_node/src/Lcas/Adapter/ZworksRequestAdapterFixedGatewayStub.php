<?php

/**
 * @file
 * @brief GATEWAY割当て時、mac_address毎に同じNode, Deviceを含んだGatewayを返すスタブ実装。
 *
 * ZworksRequestAdapterStubでは同じmac_addressでも割り当てる都度毎回新しいnode_id, device_idが
 * 払い出されるため、gatewayの再割り当て時の動作確認が出来ないことからこの実装を用意しています。
 *
 * このスタブを使用する際は、mac_addressは"1000000"以上の10進数、且つ、
 * 下位6桁は0の値をmac_addressとして使用する必要があります。
 *
 * (例：1000000, 2000000, 3000000)
 */

namespace Lcas\Adapter;

use Lcas\DB\DB;
use Lcas\Exception\IOException;
use Lcas\Log\Logger;
use Lcas\Repository\DeviceRepository;
use Lcas\Repository\NodeRepository;
use Lcas\Repository\ScenarioRepository;


/**
 * @class ZworksRequestAdapterFixedGatewayStub
 * @brief GATEWAY割当て時、mac_address毎に同じNode, Deviceを含んだGatewayを返すスタブ実装。
 *
 * ZworksRequestAdapterStubでは同じmac_addressでも割り当てる都度毎回新しいnode_id, device_idが
 * 払い出されるため、gatewayの再割り当て時の動作確認が出来ないことからこの実装を用意しています。
 *
 * このスタブを使用する際は、mac_addressは"1000000"以上の10進数、且つ、
 * 下位6桁は0の値をmac_addressとして使用する必要があります。
 *
 * (例：1000000, 2000000, 3000000)
 */
class ZworksRequestAdapterFixedGatewayStub extends ZworksRequestAdapterStub {

    /**
     * 毎回同じIDのNODE,DEVICEを含んだGATEWAYを返す。
     * gw_nameは数値で指定する必要があります。
     * @param $isSucceeded
     * @return array
     */
    protected function getGatewayAssociationNotification($isSucceeded) {

        $baseId = (int)$this->prevRequest['gw_name'];
        $baseNodeId = $baseId + 1000;
        $baseDeviceId = $baseId + 100;

        if($isSucceeded) {
            $gatewayData =  array(
                'command' => 'user_gw_assoc',
                'result_code' => '200',
                'username' => $this->prevRequest['username'],
                'gw_name' => $this->prevRequest['gw_name'],
                'manufacturer' => 'some_company',
                'fw_version' => '1.0.0',
                'capabilities' => array(
                    'zwave', 'ip_camera', 'bluetooth',
                ),
                'nodes' => array(
                    array(
                        'node_id' => $baseNodeId,
                        'devices' => array(
                            array('device_id' => $baseDeviceId++, 'device_type' => 'temperature'),
                            array('device_id' => $baseDeviceId++, 'device_type' => 'humidity'),
                            array('device_id' => $baseDeviceId++, 'device_type' => 'luminance'),
                            array('device_id' => $baseDeviceId++, 'device_type' => 'cover'),
                            array('device_id' => $baseDeviceId++, 'device_type' => 'battery'),
                        ),
                        'actions' => array(
                            array(
                                'type' => 'range',
                                'description' => 'description',
                            ),
                            array(
                                'type' => 'trigger',
                                'description' => 'description',
                            )
                        ),
                    ),
                    array(
                        'node_id' => $baseNodeId + 1000,
                        'devices' => array(
                            array('device_id' => $baseDeviceId++, 'device_type' => 'open_close'),
                            array('device_id' => $baseDeviceId++, 'device_type' => 'lock'),
                            array('device_id' => $baseDeviceId++, 'device_type' => 'cover'),
                            array('device_id' => $baseDeviceId++, 'device_type' => 'battery'),
                        ),
                        'actions' => array(
                            array(
                                'type' => 'binary',
                                'description' => 'description',
                            ),
                            array(
                                'type' => 'trigger',
                                'description' => 'description',
                            )
                        ),
                    ),
                    array(
                        'node_id' => $baseNodeId + 2000,
                        'devices' => array(
                            array('device_id' => $baseDeviceId++, 'device_type' => 'motion'),
                            array('device_id' => $baseDeviceId++, 'device_type' => 'cover'),
                            array('device_id' => $baseDeviceId++, 'device_type' => 'battery'),
                        ),
                        'actions' => array(
                            array(
                                'type' => 'range',
                                'description' => 'description',
                            ),
                            array(
                                'type' => 'trigger',
                                'description' => 'description',
                            )
                        ),
                    ),
                ),
            );

            //AMQPを使用しないダミーの環境の場合、
            //Gateway割当て時に自動でNode Inclusion Notificationが来ないため、
            //各Nodeにmanufacturer,product,serial_noを設定することが出来ない。
            //このため、各NodeがDBに登録されたあと、manufacturer等を設定する処理を実行する。
            //($this->pendingOperationsに登録しておく)
            $nodeRepository = new NodeRepository();
            foreach($gatewayData['nodes'] as $node) {
                $this->pendingOperations[] = function() use($nodeRepository, $node) {
                    $manufacturer = 'manufacturer.name';
                    $product = 'node.' . $node['node_id'];
                    $serialNo = $node['node_id'];
                    $nodeRepository->updateNodeNameElement($node['node_id'], $manufacturer, $product, $serialNo);
                };
            }

            return $gatewayData;
        } else {
            return array(
                'command' => 'user_gw_assoc',
                'result_code' => '400',
                'reason' => array('error_reason', 'error_reason2'),
            );
        }
    }


}
