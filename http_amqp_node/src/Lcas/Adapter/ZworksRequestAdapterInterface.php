<?php

/**
 * @file
 * @brief z-works.ioへのリクエストの送信/通知の取得処理を行うアダプタ
 */

namespace Lcas\Adapter;

/**
 * @interface ZworksRequestAdapterInterface
 * @brief z-works.ioへのリクエストの送信/通知の取得処理を行うアダプタ
 */
interface ZworksRequestAdapterInterface {

    /**
     * リクエストの送信を行う
     * @param array $request 送信するメッセージを含んだ連想配列
     * @param array $extraData AMQP通知には含めないが、保管する追加の情報
     * @return void
     */
    public function sendMessage($request, $extraData = null);


    /**
     * リクエストに対応する通知を受信するまで待機する
     * @param int $timeout タイムアウト。0=デフォルト値を使用。
     * @return boolean 時間内に応答を確認できたかどうか
     */
    public function waitNotification($timeout=0);


    /**
     * 通知内容を返す
     * @return array 受信したメッセージを含んだ連装配列
     */
    public function getResult();


    /**
     * 通知データの処理が完了し、通知を処理済に更新する際に呼ばれる処理。
     *
     * ここでは、getResultの時点では実行できず保留にしていた処理を実行する。
     *
     * 保留の対象となる処理は以下の通り。
     * - Nodeへのmanufacturer,product,serial_noの割り当て
     */
    public function complete();
}
