<?php

/**
 * @file
 * @brief 実際のAMQP送信を行う代わりに固定の連想配列を返却する実装
 *
 * AMQPサーバーが存在しない環境で使用することを想定しています。
 */

namespace Lcas\Adapter;

use Lcas\DB\DB;
use Lcas\Exception\IOException;
use Lcas\Log\Logger;
use Lcas\Repository\DeviceRepository;
use Lcas\Repository\NodeRepository;
use Lcas\Repository\ScenarioRepository;

/**
 * @class ZworksRequestAdapterStub
 * @brief 実際のAMQP送信を行う代わりに固定の連想配列を返却する実装
 *
 * AMQPサーバーが存在しない環境で使用することを想定しています。
 */
class ZworksRequestAdapterStub implements ZworksRequestAdapterInterface {

    /**
     * リクエストの種類(AMQPメッセージのBodyのcommandに相当)
     * @var string
     */
    protected $requestType;

    /**
     * リクエストの本体
     *
     * (Notificationの値を返す際、リクエスト時のパラメータを
     * 参照可能にするために保持)
     * @var array
     */
    protected $prevRequest;

    /**
     * AMQP通知には含めないが、保管する追加の情報
     *
     * 例： ScenarioRegistration Request時のuser_id, labelなど、
     * ZIOには送信しないが、LCAS内部では必要な情報
     *
     * @var array
     */
    protected $prevExtraData = null;

    /**
     * complete()呼び出し時に実行する追加の処理
     * (callableの配列)
     * @var callable[]
     */
    protected $pendingOperations = array();

    public function sendMessage($request, $extraData=null) {
        $this->requestType = $request['command'];
        $this->prevRequest = $request;
        $this->prevExtraData = $extraData;
        $logger = Logger::get();
        $logger->addDebug('DUMMY AMQP REQUEST: ' . var_export($request, true));

        return;
    }


    public function waitNotification($timeout=0) {
        return true;
    }

    public function getResult() {
        $isSucceeded = true;
        $result = array();
        switch($this->requestType) {
            case 'user_gw_assoc':  $result = $this->getGatewayAssociationNotification($isSucceeded); break;
            case 'user_gw_dissoc': $result = $this->getGatewayDissociationNotification($isSucceeded); break;
            case 'user_reg':       $result = $this->getUserRegistrationNotification($isSucceeded); break;
            case 'user_dereg':     $result = $this->getUserDeregistrationNotification($isSucceeded); break;
            case 'node_incl':     $result = $this->getGatewayStateChangeNotification($isSucceeded, 'inclusion'); break;
            case 'node_excl':     $result = $this->getGatewayStateChangeNotification($isSucceeded, 'exclusion'); break;
            case 'node_removal':    $result = $this->getNodeRemovalNotification($isSucceeded); break;
            case 'node_config_update': $result = $this->getNodeConfigUpdateNotification($isSucceeded); break;
            case 'dev_ctrl':        $result = $this->getDeviceControlNotification($isSucceeded); break;
            case 'scenario_reg':    $result = $this->getScenarioRegistrationNotification($isSucceeded); break;
            case 'scenario_mod':    $result = $this->getScenarioModificationNotification($isSucceeded); break;
            case 'scenario_dereg':    $result = $this->getScenarioDeregistrationNotification($isSucceeded); break;
        }
        $logger = Logger::get();
        $logger->addDebug('DUMMY AMQP NOTIFICATION: ' . var_export($result, true));
        return $result;
    }


    public function complete() {
        foreach($this->pendingOperations as $operation) {
            call_user_func($operation);
        }
    }

    /**
     * Gateway Association Notificationの結果を返す。
     * @param boolean $isSucceeded 成功した結果を返すかどうか
     * @return array Gateway Association Notificationの情報を含んだ連装配列
     */
    protected function getGatewayAssociationNotification($isSucceeded) {


        $db = DB::getMasterDb(true);
        $sql = "SELECT login_id FROM users LIMIT 10";
        $db->setSql($sql);
        $db->Query();
        $loginIds = array();
        while($row = $db->Fetch()) {
            $loginIds[] = $row['login_id'];
        }

        $sql = "SELECT max(id) AS max_id FROM nodes";
        $db->setSql($sql);
        if(!$db->Query()) {
            throw new IOException('nodes IDの最大値取得に失敗');
        }
        $row = $db->Fetch();
        $maxNodeId = $row['max_id'] + 1;

        $sql = "SELECT max(id) AS max_id FROM devices";
        $db->setSql($sql);
        if(!$db->Query()) {
            throw new IOException('devices IDの最大値取得に失敗');
        }
        $row = $db->Fetch();
        $maxDeviceId = $row['max_id'] + 1;

        if($isSucceeded) {
            if(preg_match('/^nodeless/i', $this->prevRequest['gw_name'])) {
                return $this->getNodelessGatewayAssociationResult();
            }

            $gatewayData =  array(
                'command' => 'user_gw_assoc',
                'result_code' => '200',
                'username' => $this->prevRequest['username'],
                'gw_name' => $this->prevRequest['gw_name'],
                'manufacturer' => 'some_company',
                'fw_version' => '1.0.0',
                'capabilities' => array(
                    'zwave', 'ip_camera', 'bluetooth',
                ),
                'nodes' => array(
                    array(
                        'node_id' => $maxNodeId++,
                        'devices' => array(
                            array('device_id' => $maxDeviceId++, 'device_type' => 'temperature'),
                            array('device_id' => $maxDeviceId++, 'device_type' => 'humidity'),
                            array('device_id' => $maxDeviceId++, 'device_type' => 'luminance'),
                            array('device_id' => $maxDeviceId++, 'device_type' => 'cover'),
                            array('device_id' => $maxDeviceId++, 'device_type' => 'battery'),
                        ),
                        'actions' => array(
                            array(
                                'type' => 'range',
                                'description' => 'description',
                            ),
                            array(
                                'type' => 'trigger',
                                'description' => 'description',
                            )
                        ),
                    ),
                    array(
                        'node_id' => $maxNodeId++,
                        'devices' => array(
                            array('device_id' => $maxDeviceId++, 'device_type' => 'open_close'),
                            array('device_id' => $maxDeviceId++, 'device_type' => 'lock'),
                            array('device_id' => $maxDeviceId++, 'device_type' => 'cover'),
                            array('device_id' => $maxDeviceId++, 'device_type' => 'battery'),
                        ),
                        'actions' => array(
                            array(
                                'type' => 'binary',
                                'description' => 'description',
                            ),
                            array(
                                'type' => 'trigger',
                                'description' => 'description',
                            )
                        ),
                    ),
                    array(
                        'node_id' => $maxNodeId++,
                        'devices' => array(
                            array('device_id' => $maxDeviceId++, 'device_type' => 'motion'),
                            array('device_id' => $maxDeviceId++, 'device_type' => 'cover'),
                            array('device_id' => $maxDeviceId++, 'device_type' => 'battery'),
                        ),
                        'actions' => array(
                            array(
                                'type' => 'range',
                                'description' => 'description',
                            ),
                            array(
                                'type' => 'trigger',
                                'description' => 'description',
                            )
                        ),
                    ),
                ),
            );

            //AMQPを使用しないダミーの環境の場合、
            //Gateway割当て時に自動でNode Inclusion Notificationが来ないため、
            //各Nodeにmanufacturer,product,serial_noを設定することが出来ない。
            //このため、各NodeがDBに登録されたあと、manufacturer等を設定する処理を実行する。
            //($this->pendingOperationsに登録しておく)
            $nodeRepository = new NodeRepository();
            $deviceRepository = new DeviceRepository();
            foreach($gatewayData['nodes'] as $node) {
                $this->pendingOperations[] = function() use($nodeRepository, $node) {
                    $manufacturer = 'manufacturer.name';
                    $product = 'node.' . $node['node_id'];
                    $serialNo = $node['node_id'];
                    $nodeRepository->updateNodeNameElement($node['node_id'], $manufacturer, $product, $serialNo);
                };
//                foreach($node['devices'] as $device) {
//                    $this->pendingOperations[] = function() use($deviceRepository, $device) {
//                        $initialData = $this->getInitialDeviceDataByType($device['device_type']);
//                        $data = array(
//                            'time_stamp' => time() * 1000,
//                            'value' => $initialData['value'],
//                            'unit' => $initialData['unit'],
//                        );
//                        $deviceRepository->registerDeviceData($device['device_id'], $data);
//                    };
//                }
            }

            return $gatewayData;
        } else {
            return array(
                'command' => 'user_gw_assoc',
                'result_code' => '400',
                'reason' => array('error_reason', 'error_reason2'),
            );
        }
    }


    /**
     * ゲートウェイ自身を表す1つのノードのみを含むゲートウェイを返す。
     */
    private function getNodelessGatewayAssociationResult() {
        $db = DB::getMasterDb(true);

        $sql = "SELECT max(id) AS max_id FROM nodes";
        $db->setSql($sql);
        if(!$db->Query()) {
            throw new IOException('nodes IDの最大値取得に失敗');
        }
        $row = $db->Fetch();
        $maxNodeId = $row['max_id'] + 1;
        $gatewayData =  array(
            'command' => 'user_gw_assoc',
            'result_code' => '200',
            'username' => $this->prevRequest['username'],
            'gw_name' => $this->prevRequest['gw_name'],
            'manufacturer' => 'some_company',
            'fw_version' => '1.0.0',
            'capabilities' => array(
                'zwave', 'ip_camera', 'bluetooth',
            ),
            'nodes' => array(
                //ゲートウェイを表すノード。デバイスを含まない。
                array(
                    'node_id' => $maxNodeId++,
                    'devices' => array(
                    ),
                    'actions' => array(
                        array(
                            'type' => 'range',
                            'description' => 'description',
                        ),
                        array(
                            'type' => 'trigger',
                            'description' => 'description',
                        )
                    ),
                ),
            )
        );
        return $gatewayData;
    }

    private function getGatewayDissociationNotification($isSucceeded) {
        if($isSucceeded) {
            return array(
                'command' => 'user_gw_dissoc',
                'result_code' => '200',
//             'reason' => array(''),
                'username' => 1,
                'gw_name' => $this->prevRequest['gw_name'],
            );
        } else {
            return array(
                'command' => 'user_gw_dissoc',
                'result_code' => '400',
                'reason' => array('error_reason'),
                'username' => 1,
                'gw_name' => $this->prevRequest['gw_name'],
            );
        }
    }


    private function getUserRegistrationNotification($isSucceeded) {
        if($isSucceeded) {
            return array(
                'command' => 'user_reg',
                'result_code' => '200',
//             'reason' => array(''),
                'username' => 1,
            );
        } else {
            return array(
                'command' => 'user_reg',
                'result_code' => '400',
                'reason' => array('error_reason'),
            );
        }
    }

    private function getUserDeregistrationNotification($isSucceeded) {
        if($isSucceeded) {
            return array(
                'command' => 'user_dereg',
                'result_code' => '200',
//             'reason' => array(''),
                'username' => $this->prevRequest['username'],
            );
        } else {
            return array(
                'command' => 'user_dereg',
                'result_code' => '400',
                'reason' => array('error_reason'),
            );
        }
    }

    private function getGatewayStateChangeNotification($isSucceeded, $state) {
        $newState = 'connected';
        if($this->requestType == 'node_incl') {
            $newState = $this->prevRequest['flag'] == 'start' ? 'inclusion' : 'connected';
        } else if($this->requestType == 'node_excl') {
            $newState = $this->prevRequest['flag'] == 'start' ? 'exclusion' : 'connected';
        }

        if($isSucceeded) {
            return array(
                'command' => 'gw_state_change',
                'result_code' => '200',
//             'reason' => array(''),
                'gw_name' => 1,
                'state' => $newState,
            );
        } else {
            return array(
                'command' => 'gw_state_change',
                'result_code' => '400',
                'reason' => array('error_reason'),
            );
        }
    }


    private function getNodeRemovalNotification($isSucceeded) {
        if($isSucceeded) {
            return array(
                'command' => 'node_removal',
                'result_code' => '200',
            );
        } else {
            return array(
                'command' => 'node_removal',
                'result_code' => '400',
                'reason' => array('error_reason'),
            );
        }
    }

    private function getNodeConfigUpdateNotification($isSucceeded) {

        if($isSucceeded) {

            $nodeRepository = new NodeRepository();
            $deviceRepository = new DeviceRepository();
            $originalNode = $nodeRepository->findById($this->prevRequest['node_id']);
            $originalDevices = $nodeRepository->findDevices($this->prevRequest['node_id']);
            $originalActions = $nodeRepository->findActions($this->prevRequest['node_id']);
            $originalUsers = $nodeRepository->findSharedUsers($this->prevRequest['node_id']);

            $devices = array();
            foreach($originalDevices as $device) {
                $devices[] = array(
                    'device_id' => $device['id'],
                    'device_type' => $deviceRepository->getDeviceTypeNameFromType($device['type']),
                );
            }

            $actions = array();
            foreach($originalActions as $action) {
                $actions[] = array(
                    'type' => $nodeRepository->getActionTypeFromName($action['type']),
                    'description' => $action['description'],
                );
            }

            $newSharingLevel = $nodeRepository->getSharingLevelNameFromType($originalNode['sharing_level']);
            if(isset($this->prevRequest['sharing_level'])){
                $newSharingLevel = $this->prevRequest['sharing_level'];
            }
            $userNames = array();
            if(isset($this->prevRequest['shared_with'])) {
                $userNames = $this->prevRequest['shared_with'];
            } else {
                foreach($originalUsers as $user) {
                    $userNames[] = $user['name'];
                }
            }

            $result = array(
                'command' => 'node_config_update',
                'result_code' => 200,
                'node_id' => $originalNode['id'],
                'devices' => $devices,
                'sharing_level' => $newSharingLevel,
                'shared_with' => $userNames,
                'actions' => $actions,
            );

            return $result;
        } else {
            return array(
                'command' => 'user_dereg',
                'result_code' => '400',
                'reason' => array('error_reason'),
            );
        }
    }

    private function getDeviceControlNotification($isSucceeded) {
        if($isSucceeded) {
            $result = array(
                'command' => 'dev_ctrl',
                'result_code' => 200,
            	'node_name' => $this->prevRequest['node_name'],
            	'device_type' => $this->prevRequest['device_type'],
            	'action_type' => $this->prevRequest['action_type'],
            	'action' => $this->prevRequest['action'],
            	'params' => $this->prevRequest['params'],
            );

            return $result;
        } else {
            return array(
                'command' => 'dev_ctrl',
                'result_code' => '400',
                'reason' => array('error_reason'),
            );
        }
    }

    private function getScenarioRegistrationNotification($isSucceeded) {

        if($isSucceeded) {
            $result = array(
                'command' => 'scenario_reg',
                'result_code' => 200,
                'scenario_id' => $this->prevRequest['scenario_id'],
                'scenario_type' => $this->prevRequest['scenario_type'],
                'condition_operator' => $this->prevRequest['condition_operator'],
                'conditions' => $this->prevRequest['conditions'],
                'trigger_after' => $this->prevRequest['trigger_after'],
                'actions' => $this->prevRequest['actions'],
            );

            return $result;
        } else {
            return array(
                'command' => 'scenario_reg',
                'result_code' => '400',
                'reason' => array('error_reason'),
            );
        }
    }

    private function getScenarioModificationNotification($isSucceeded) {

        if($isSucceeded) {
            $result = array(
                'command' => 'scenario_mod',
                'result_code' => 200,
                'scenario_id' => $this->prevRequest['scenario_id'],
                'scenario_type' => $this->prevRequest['scenario_type'],
                'condition_operator' => $this->prevRequest['condition_operator'],
                'conditions' => $this->prevRequest['conditions'],
                'trigger_after' => $this->prevRequest['trigger_after'],
                'actions' => $this->prevRequest['actions'],
            );

            return $result;
        } else {
            return array(
                'command' => 'scenario_mod',
                'result_code' => '400',
                'reason' => array('error_reason'),
            );
        }
    }

    private function getScenarioDeregistrationNotification($isSucceeded) {
        if($isSucceeded) {
            $result = array(
                'command' => 'scenario_dereg',
                'scenario_id' => $this->prevRequest['scenario_id'],
                'result_code' => 200,
            );

            return $result;
        } else {
            return array(
                'command' => 'scenario_dereg',
                'result_code' => '400',
                'reason' => array('error_reason'),
            );
        }
    }


    private function getInitialDeviceDataByType($type) {

        switch($type) {
            case DEVICE_TYPE_TEMPERATURE:
                return array('value' => mt_rand(10, 35), 'unit' => 'C');
            case DEVICE_TYPE_HUMIDITY:
                return array('value' => mt_rand(20, 80), 'unit' => '%');
            case DEVICE_TYPE_MOTION:
            case DEVICE_TYPE_OPEN_CLOSE:
            case DEVICE_TYPE_LOCK:
            case DEVICE_TYPE_COVER:
            case DEVICE_TYPE_CALL_BUTTON:
            case DEVICE_TYPE_SMART_LOCK:
            case DEVICE_TYPE_POWER_SWITCH:
            	return array('value' => mt_rand(0, 100) > 50 ? 0 : 255, 'unit' => null);
            case DEVICE_TYPE_BATTERY:
            case DEVICE_TYPE_LUMINANCE:
                return array('value' => mt_rand(0, 100), 'unit' => '%');
            case DEVICE_TYPE_POWER:
            	return array('value' => mt_rand(0, 100), 'unit' => 'W');
            case DEVICE_TYPE_ACC_ENERGY:
            	return array('value' => mt_rand(0, 100), 'unit' => 'kWh');
        }
        return null;
    }

}
