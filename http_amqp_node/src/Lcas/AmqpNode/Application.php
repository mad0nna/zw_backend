<?php

/**
 * @file
 * @brief AMQPクライアント(amqp-node)の処理本体
 */

namespace Lcas\AmqpNode;


use Lcas\DB\DB;
use Lcas\Exception\DBException;
use Lcas\Exception\IOException;
use Lcas\Exception\LcasException;
use Lcas\Log\Logger;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPSocketConnection;
use PhpAmqpLib\Connection\AMQPSSLConnection;
use PhpAmqpLib\Exception\AMQPIOException;
use PhpAmqpLib\Exception\AMQPProtocolConnectionException;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPRuntimeException;
use SebastianBergmann\Environment\Runtime;

/**
 * @class Application
 * @brief AMQPクライアント(amqp-node)の処理本体
 */
class Application {


    private $haveConnectionTried = true;

    /**
     * 再接続試行回数
     * (一度でも正常に接続できた場合、クリア)
     * @var int
     */
    private $retryCount = 0;

    /**
     * @var AMQPStreamConnection
     */
    private $amqpConnection = null;

    /**
     * @var AMQPChannel
     */
    private $channel = null;

    /**
     * コンストラクタ
     */
    public function __construct() {
        $this->haveConnectionTried = false;
        $this->retryCount = 0;
    }


    /**
     * アプリケーションのメインルーチン
     */
    public function run() {
        $logger = Logger::get();
        $logger->addInfo('[APPLICATION START]Application started.');
        register_shutdown_function(array($this, 'shutdown'));

        while(true) {
            try {
                $this->runInner();
            } catch(AMQPProtocolConnectionException $amqpConnectionException){
                $logger->addError('[AMQP Connection Error]' . $amqpConnectionException->getMessage());
            } catch(AMQPRuntimeException $amqpRuntimeException) {
                $logger->addError('[AMQP Runtime Error]' . $amqpRuntimeException->getMessage());
            } catch(\ErrorException $exception) {
                //接続断時にErrorExceptionがスローされることがあるため、ここでキャッチして祭接続を試みる。
                //それ以外のRuntimeExceptionやExceptionは特にハンドルせず、プロセス全体の再起動がかかることを期待する
                $logger->addError('[General Error]' . $exception->getMessage());
            }
        }
    }


    /**
     * 接続〜メッセージループの間の処理。
     * 接続エラー発生時、この処理全体をリトライする
     */
    private function runInner() {
        //接続が必要な場合は接続する。
        $this->initializeConnection();

        //メッセージループ開始
        $logger = Logger::get();
        try {
            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }
        } catch(AMQPIOException $amqpIoException) {
            $logger->addError('[AMQP IO Error]' . $amqpIoException->getMessage());
        }

    }

    /**
     * メッセージ受信時に実行する処理
     * @param \PhpAmqpLib\Message\AMQPMessage $message AMQPメッセージ
     */
    public function onMessage($message) {
        $db = DB::getMasterDb();
        $logger = \Lcas\Log\Logger::get();

        $body = $message->body;
        $properties = $message->get_properties();
        $json = json_decode($body, true);
        if(!isset($json['command'])) {
            //JSONデータとしてのデコードに失敗したものは、何度処理してもエラーとなるため、
            //エラーとして扱いつつACKを返す。
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            $logger->addCritical('[AMQP DECODE ERROR]AMQPメッセージのデコードに失敗: ' . $body . ', properties:' . var_export($properties, true));
            return;
        } else {
            $logger->addDebug('AMQP RECEIVED:' . var_export($json, true));
            //$logger->addDebug('AMQP RECEIVED on node '.WHICH_AMQP.':' . var_export($json, true));
        }

        $command = $json['command'];
        $correlationId = isset($properties['correlation_id']) ? $properties['correlation_id'] : 0;

        $entry = array(
            'type' => $command,
            'data' => $body,
            'state' => ZWORKS_LOG_STATE_UNREAD,
            'correlation_id' => $correlationId,
        );

        $zworksNotificationRepository = new \Lcas\Repository\ZworksNotificationRepository();
        try {
            $db->Begin();
        } catch(DBException $startTransactionException) {
            $errorMessage = $startTransactionException->getMessage();
            if(preg_match('/MySQL server has gone away/', $errorMessage)) {
                //長時間のアイドル状態による接続断が発生した場合は、再接続を試みる
                $logger->addWarning('DB connection lost detected. retry connection. Message:' . $errorMessage);
                $forceConnect = true;
                $db->Connect($forceConnect);
                $db->Begin();
            } else {
                //これ以外のエラーの場合はエラーとしてthrowする。
                throw $startTransactionException;
            }
        }

        try {
            $zworksNotificationRepository->register($entry);

            $delayedNotificationHandler = new \Lcas\AmqpNode\DelayedNotificationHandler();
            if ($delayedNotificationHandler->isDelayed($correlationId)) {
                $delayedNotificationHandler->handleDelayedNotification($entry);
            }

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            $db->Commit();
        } catch(LcasException $e) {
            //ここでエラーが発生した場合、トランザクションをロールバックしてから例外を再スローする。
            //(ロールバックしない場合、次に呼び出されるBegin()で暗黙のコミットが行われる)
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            $db->Rollback();

            //LCAS固有の例外(DBのクエリエラー等も含む)
            $errorMessage = '[AMQP MESSAGE PROCESS ERROR]次のメッセージの処理中にエラーが発生しました: ' . $body . ', properties:' . var_export($properties, true).", "
                          . 'エラー内容： ' . $e->getMessage() . "\n"
            ;

            $logger->addCritical($errorMessage);
            $logger->addError($e->getTraceAsString());

        } catch(\Exception $e) {
            //ここでエラーが発生した場合、トランザクションをロールバックしてから例外を再スローする。
            //(ロールバックしない場合、次に呼び出されるBegin()で暗黙のコミットが行われる)
            $db->Rollback();

            //LCASのロジックより上位のエラーであるため、AMQPのackは返さない
            //(再試行により解消する可能性がある)
            throw $e;
        }
    }


    /**
     * アプリケーションをシャットダウンする
     */
    public function shutdown() {
        $logger = \Lcas\Log\Logger::get();
        $logger->addInfo('[APPLICATION STOP] Application Stopped.');

        if($this->channel) $this->channel->close();
        if($this->amqpConnection) $this->amqpConnection->close();
    }

    /**
     * 接続の初期化処理。既に接続が確立できている場合は特に何も行わない。
     */
    private function initializeConnection() {
        $logger = \Lcas\Log\Logger::get();

        if($this->retryCount >= AMQP_MAX_RECONNECTION_TRY) {
            throw new \RuntimeException('[AMQP Reconnection Error]再試行回数が上限を超過しました。');
        }

        if($this->haveConnectionTried) {
            //一定時間置いて再接続
            sleep( $this->getRetryWaitTime($this->retryCount) );
        }
        $this->haveConnectionTried = true;
        $this->retryCount++;

        $this->realConnect();
        $logger->addInfo('[AMQP Connection]Established.');

        $this->channel = $this->amqpConnection->channel();

        $queueName = AMQP_QUEUE_NAME;
        $consumer_tag = AMQP_QUEUE_NAME;

        $noLocal = false;
        $noAck = false;
        $consumeExclusive = false;
        $noWait = false;
        $callbackFunc = array($this, 'onMessage');
        $this->channel->basic_consume($queueName, $consumer_tag, $noLocal, $noAck, $consumeExclusive, $noWait, $callbackFunc);

        //接続成功のため、カウントを戻す
        $this->retryCount = 0;
        $logger->addInfo('[AMQP Connection]Initialization Complete.');
    }


    /**
     * 実際の接続処理
     * @return void
     */
    private function realConnect() {
        if($this->amqpConnection) {
            //AMQPStreamConnection::isConnectedでは現在の接続状況の判定が出来ないことと、
            //接続に失敗すると"$this->amqpConnection"はオブジェクトとして初期化されないため、
            //"$this->amqpConnectionがオブジェクトとして初期化されている" = 1度でも接続が成功したことがあると判断する。
            //その場合は、単純に再接続する。
            $this->amqpConnection->reconnect();
            return;
        }

        //TODO: $this->amqpConnectionの実体はserviceContainerから取得する。
        if(ENV == ENV_PROD || ENV == 'staging') {
            $ssl_options = array(
                'local_cert'    => AMQP_TLS_CLIENT_CERT,
                'cafile'        => AMQP_TLS_CA_CERT,
                'verify_peer'   => true,
            );
            $options = array(
                'login_method' => 'EXTERNAL',
                'read_write_timeout' => AMQP_HEARTBEAT + 1,  //heartbeat以上の数値を指定しないとACK返送時に"Socket Connection Timeout"が発生するため、指定
                'heartbeat'    => AMQP_HEARTBEAT,
            );
            $this->amqpConnection = new AMQPSSLConnection(AMQP_HOST, AMQP_PORT, AMQP_USER, AMQP_PASS, AMQP_VHOST, $ssl_options, $options);
        } else {
            $this->amqpConnection = new AMQPStreamConnection(AMQP_HOST, AMQP_PORT, AMQP_USER, AMQP_PASS, AMQP_VHOST);
        }
    }


    /**
     * 接続の試行回数に応じた待ち時間を返す(最大でも60秒)。
     * @param int $count 接続試行回数(これまでに何回リトライしたか)
     * @return int 待ち時間(秒)
     */
    private function  getRetryWaitTime($count) {
        if($count < 1) {
            $count = 1;
        }
        return min(pow(2, $count - 1), 60);
    }

}
