<?php

/**
 * @file
 * @brief AMQPメッセージの遅延処理のインターフェース
 */

namespace Lcas\AmqpNode\DelayedNotification;


/**
 * @interface DelayedNotificationInterface
 * @brief AMQPメッセージの遅延処理のインターフェース
 */
interface DelayedNotificationInterface {

    /**
     * 遅延したメッセージをセットする
     * @param int $correlationId メッセージのCorellation-Id
     * @param array $notification zworks_notificationsテーブルのレコード
     * @return void
     */
    public function setNotification($correlationId, $notification);

    /**
     * 遅延処理を実行する。
     * @return void
     */
    public function process();

}
