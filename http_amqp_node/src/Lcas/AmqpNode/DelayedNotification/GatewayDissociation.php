<?php

/**
 * @file
 * @brief "Gateway Disassociation Notification"が遅延した場合に実行する処理
 */

namespace Lcas\AmqpNode\DelayedNotification;


use Lcas\Log\Logger;
use Lcas\Repository\GatewayRepository;
use Lcas\Service\GatewayService;

/**
 * @class GatewayDissociation
 * @brief "Gateway Disassociation Notification"が遅延した場合に実行する処理
 */
class GatewayDissociation implements DelayedNotificationInterface {

    private $notification;

    /**
     * @var GatewayService
     */
    private $gatewayService;

    /**
     * @var GatewayRepository
     */
    private $gatewayRepository;

    public function __construct() {
        $this->gatewayService = new GatewayService();
        $this->gatewayRepository = new GatewayRepository();
    }

    public function setNotification($correlationId, $notification) {
        $this->notification = $notification;
    }

    public function process() {
        $logger = Logger::get();
        if($this->notification['result_code'] != 200) {
            //レスポンスがOKでなければ処理しない
            $logger->addWarning('Delayed notification result_code: ' . $this->notification['result_code']);
            $logger->addWarning('Reason: ' . var_export($this->notification['reason'], true));
            return;
        }
        $gateway = $this->gatewayRepository->findByMacAddress($this->notification['gw_name'], array('with_associated' => true));
        if(!$gateway) {
            $logger->addWarning('No associated gateway found. gw_name:' . $this->notification['gw_name']);
            return;
        }
        $this->gatewayService->deregisterGateway($gateway['id']);
    }
}
