<?php

/**
 * @file
 * @brief "Gateway State Change Notification"が遅延した場合に実行する処理
 */

namespace Lcas\AmqpNode\DelayedNotification;


use Lcas\Log\Logger;
use Lcas\Service\GatewayService;
use Lcas\Repository\GatewayRepository;

/**
 * @class GatewayStateChange
 * @brief "Gateway State Change Notification"が遅延した場合に実行する処理
 */
class GatewayStateChange implements DelayedNotificationInterface {

    private $notification;

    /**
     * @var GatewayService
     */
    private $gatewayService;

    public function __construct() {
        $this->gatewayService = new GatewayService();
        $this->gatewayRepository = new GatewayRepository();
    }

    public function setNotification($correlationId, $notification) {
        $this->notification = $notification;
    }

    public function process() {
        $logger = Logger::get();
        if($this->notification['result_code'] != 200) {
            //レスポンスがOKでなければ処理しない
            $logger->addWarning('Delayed notification result_code: ' . $this->notification['result_code']);
            $logger->addWarning('Reason: ' . var_export($this->notification['reason'], true));
            return;
        }

        $gatewayId = $this->gatewayRepository->findByMacAddress($this->notification['gw_name'])['id'];
        $stateId = $this->gatewayRepository->getStateIdFromName($this->notification['state']);
        $this->gatewayRepository->updateState($gatewayId, $stateId);
    }
}
