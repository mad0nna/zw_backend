<?php

/**
 * @file
 * @brief "Scenario Deregistration Notification"が遅延した場合に実行する処理
 */

namespace Lcas\AmqpNode\DelayedNotification;


use Lcas\Exception\LcasException;
use Lcas\Log\Logger;
use Lcas\Repository\ZworksRequestRepository;
use Lcas\Service\ScenarioService;

/**
 * @class ScenarioDeregistration
 * @brief "Scenario Deregistration Notification"が遅延した場合に実行する処理
 */
class ScenarioDeregistration implements DelayedNotificationInterface {

    private $notification;

    private $correlationId;

    /**
     * @var ScenarioService
     */
    private $scenarioService;

    /**
     * @var ZworksRequestRepository
     */
    private $zworksRequestRepository;

    public function __construct() {
        $this->scenarioService = new ScenarioService();
        $this->zworksRequestRepository = new ZworksRequestRepository();
    }

    public function setNotification($correlationId, $notification) {
        $this->notification = $notification;
        $this->correlationId = $correlationId;
    }

    public function process() {
        $logger = Logger::get();
        if($this->notification['result_code'] != 200) {
            //レスポンスがOKでなければ処理しない
            $logger->addWarning('Delayed notification result_code: ' . $this->notification['result_code']);
            $logger->addWarning('Reason: ' . var_export($this->notification['reason'], true));
            return;
        }

        $amqpRequest = $this->zworksRequestRepository->find($this->correlationId);
        $extraDataJson = $amqpRequest['extra_data'];
        $extraData = json_decode($extraDataJson, true);

        $originalAction = $extraData['original_action'];

        if($originalAction == 'remove') {
            //シナリオの削除要求経由でscenario_delメッセージが送信されていた場合
            if(!isset($this->notification['scenario_id'])) {
                throw new LcasException('Scenario Deregistration Notificationで削除するシナリオの対象IDが指定されていません。');
            }
            $scenarioId = $this->notification['scenario_id'];

            $this->scenarioService->removeScenarioFromLcas($scenarioId);
        } else {
            //シナリオの削除意外の理由でメッセージが送信されていた場合（シナリオの一時停止など）
            if(!isset($extraData['original_data'])) {
                throw new LcasException('シナリオ情報を復元するための情報がzworks_requestsのレコードに含まれていません');
            }
            if(!isset($extraData['user_id'])) {
                throw new LcasException('シナリオ情報を復元する対象の情報(user_id)がzworks_requestのレコードに含まれていません');
            }

            $scenarioId = $this->notification['scenario_id'];
            $userId = $extraData['user_id'];

            $this->scenarioService->saveScenario($userId, $scenarioId, $extraData['original_data']);
        }
    }
}
