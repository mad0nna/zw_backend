<?php

/**
 * @file
 * @brief "Scenario Modification Notification"が遅延した場合に実行する処理
 */

namespace Lcas\AmqpNode\DelayedNotification;


use Lcas\Exception\LcasException;
use Lcas\Log\Logger;
use Lcas\Repository\ZworksRequestRepository;
use Lcas\Service\ScenarioService;

/**
 * @class ScenarioModification
 * @brief "Scenario Modification Notification"が遅延した場合に実行する処理
 */
class ScenarioModification implements DelayedNotificationInterface {

    private $notification;

    private $correlationId;

    /**
     * @var ScenarioService
     */
    private $scenarioService;

    /**
     * @var ZworksRequestRepository
     */
    private $zworksRequestRepository;

    public function __construct() {
        $this->scenarioService = new ScenarioService();
        $this->zworksRequestRepository = new ZworksRequestRepository();
    }

    public function setNotification($correlationId, $notification) {
        $this->notification = $notification;
        $this->correlationId = $correlationId;
    }

    public function process() {
        $logger = Logger::get();
        if($this->notification['result_code'] != 200) {
            //レスポンスがOKでなければ処理しない
            $logger->addWarning('Delayed notification result_code: ' . $this->notification['result_code']);
            $logger->addWarning('Reason: ' . var_export($this->notification['reason'], true));
            return;
        }

        $amqpRequest = $this->zworksRequestRepository->find($this->correlationId);
        //NotificationにはScenarioに対応するuser_idが含まれていないため、
        //zworks_requests.extra_dataから取得を行う。
        $extraDataJson = $amqpRequest['extra_data'];
        $extraData = json_decode($extraDataJson, true);
        if(!isset($extraData['user_id'])) {
            throw new LcasException('Scenario Modification Notificationのシナリオに対応するユーザーが見つかりません。');
        }

        //HTTPのPOST scenariosのパラメータを再現するため、
        //enabled, labelsを追加する。
        if(isset($extraData['labels'])) {
            $this->notification['labels'] = $extraData['labels'];
        }
        $this->notification['enabled'] = $extraData['enabled'];

        $userId = $extraData['user_id'];
        $scenarioId = $this->notification['scenario_id'];

        $this->scenarioService->saveScenario($userId, $scenarioId, $this->notification);
    }
}
