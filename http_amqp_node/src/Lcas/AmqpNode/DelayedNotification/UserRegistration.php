<?php

/**
 * @file
 * @brief "User Registration Notification"が遅延した場合に実行する処理
 */

namespace Lcas\AmqpNode\DelayedNotification;


use Lcas\Log\Logger;
use Lcas\Service\UserService;

/**
 * @class UserRegistration
 * @brief "User Registration Notification"が遅延した場合に実行する処理
 */
class UserRegistration implements DelayedNotificationInterface {

    private $notification;

    /**
     * @var UserService
     */
    private $userService;

    public function __construct() {
        $this->userService = new UserService();
    }

    public function setNotification($correlationId, $notification) {
        $this->notification = $notification;
    }

    public function process() {
        $logger = Logger::get();
        if($this->notification['result_code'] != 200) {
            //レスポンスがOKでなければ処理しない
            $logger->addWarning('Delayed notification result_code: ' . $this->notification['result_code']);
            $logger->addWarning('Reason: ' . var_export($this->notification['reason'], true));
            return;
        }
        $userName = $this->notification['username'];
        $this->userService->sendDeregisterUserRequest($userName);
    }
}
