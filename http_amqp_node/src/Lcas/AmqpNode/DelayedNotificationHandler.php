<?php

/**
 * @file
 * @brief AMQPメッセージの遅延処理の制御を行うクラス
 */

namespace Lcas\AmqpNode;



use Lcas\AmqpNode\DelayedNotification\DelayedNotificationInterface;
use Lcas\AmqpNode\DelayedNotification\GatewayAssociation;
use Lcas\AmqpNode\DelayedNotification\GatewayStateChange;
use Lcas\AmqpNode\DelayedNotification\GatewayDissociation;
use Lcas\AmqpNode\DelayedNotification\ScenarioDeregistration;
use Lcas\AmqpNode\DelayedNotification\ScenarioModification;
use Lcas\AmqpNode\DelayedNotification\ScenarioRegistration;
use Lcas\AmqpNode\DelayedNotification\UserRegistration;
use Lcas\Log\Logger;
use Lcas\Repository\ZworksNotificationRepository;
use Lcas\Repository\ZworksRequestRepository;

/**
 * @class DelayedNotificationHandler
 * @brief AMQPメッセージの遅延処理の制御を行うクラス
 */
class DelayedNotificationHandler {

    /**
     * @var ZworksRequestRepository
     */
    private $zworksRequestRepository;

    /**
     * @var ZworksNotificationRepository
     */
    private $zworksNotificationRepository;


    /**
     * コンストラクタ
     */
    public function __construct() {
        $this->zworksNotificationRepository = new ZworksNotificationRepository();
        $this->zworksRequestRepository = new ZworksRequestRepository();
    }

    /**
     * 通知が遅延しているかを判定して返す。
     * @param int $correlationId zworks_requests.id
     * @return boolean
     */
    public function isDelayed($correlationId) {
        $request = $this->zworksRequestRepository->find($correlationId);
        if(!$request) {
            return false;
        }
        $logCreatedTime = strtotime($request['created']);
        $currentTime = time();

        return $logCreatedTime < ($currentTime - AMQP_REQUEST_TIMEOUT);
    }


    /**
     * 遅延処理を実行する
     * @param array $notification zworks_notificationsテーブルのレコード
     */
    public function handleDelayedNotification($notification) {

        $logger = Logger::get();
        $logger->addWarning('DELAYED NOTIFICATION: Type: ' . $notification['type'] . ', Correlation-Id: ' . $notification['correlation_id']);
        $logger->addWarning(var_export($notification, true));

        $delayedNotification = $this->createDelayedNotification($notification);
        if(!$delayedNotification) {
            //未知の通知タイプを受診した場合、特にエラーにはせずスルーする
            //受け取った通知自体は処理済として更新するが、
            //zworks_requestsの方は、まだ処理は完了していないため更新しない
            $this->zworksNotificationRepository->updateStateByCorrelationId($notification['correlation_id'], ZWORKS_LOG_STATE_PROCESSED);
            return;
        }

        $data = json_decode($notification['data'], true);
        $delayedNotification->setNotification($notification['correlation_id'], $data);
        $delayedNotification->process();

        //処理の完了後、通知ログの状態を処理完了に更新する
        $this->zworksRequestRepository->updateState($notification['correlation_id'], ZWORKS_REQUEST_STATE_PROCESSED);
        $this->zworksNotificationRepository->updateStateByCorrelationId($notification['correlation_id'], ZWORKS_LOG_STATE_PROCESSED);
        $logger->addWarning('DELAYED NOTIFICATION COMPLETE: Type: ' . $notification['type'] . ', Correlation-Id: ' . $notification['correlation_id']);
    }


    /**
     * 受け取った通知の種類に応じてDelayedNotificationのインスタンスを生成して返す。
     * @param array $notification zworks_notificationsテーブルのレコード
     * @return DelayedNotificationInterface
     */
    private function createDelayedNotification($notification) {
        $logger = Logger::get();
        switch($notification['type']) {
            case 'user_gw_assoc': return new GatewayAssociation();
            case 'user_gw_dissoc': return new GatewayDissociation();
            case 'user_reg': return new UserRegistration();
            case 'scenario_reg': return new ScenarioRegistration();
            case 'scenario_mod': return new ScenarioModification();
            case 'scenario_dereg': return new ScenarioDeregistration();
            case 'gw_state_change': return new GatewayStateChange();
        }
        //未知の通知タイプを受診した場合、特にエラーにはせずログに記録だけしてスルーする
        $logger->addWarning('Unknown notification type: ' . $notification['type'] . ', detail: ' . var_export($notification, true));
        return false;
    }
}
