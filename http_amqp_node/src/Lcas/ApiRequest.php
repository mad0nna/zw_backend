<?php

/**
 * @file
 * @brief HTTP API用のHTTPリクエストオブジェクト
 */

namespace Lcas;


use Lcas\Exception\BadParameterException;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * @class ApiRequest
 * @brief HTTP API用のHTTPリクエストオブジェクト
 */
class ApiRequest extends \Symfony\Component\HttpFoundation\Request {

    /**
     * リクエストのコンテンツBODYをJSONとしてパースして返す。
     * @return ParameterBag
     * @throws Exception\BadParameterException
     */
    public function getContentAsJson() {
        $content = $this->getContent();
        $json = json_decode($content, true);
        if(!$json) {
            $head = substr($content, 0, 100);
            throw new BadParameterException('入力データが正しいJSONではありません。: ' . $head);
        }
        $bag = new ParameterBag($json);
        return $bag;
    }

}
