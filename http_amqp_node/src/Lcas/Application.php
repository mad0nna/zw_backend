<?php

/**
 * @file
 * @brief HTTP APIのリクエストを受け付け、処理の分岐を行うクラス。
 */

namespace Lcas;

use Lcas\Controller\ControllerInterface;
use Lcas\DB\DB;
use Lcas\Exception\ApiTimeoutException;
use Lcas\Exception\BadParameterException;
use Lcas\Exception\LcasException;
use Lcas\Exception\RouteNotFoundException;
use Lcas\Exception\ZworksApiException;
use Lcas\Exception\MailException;
use Lcas\HttpNode\JsonResponse;
use Lcas\Log\Logger;
use Symfony\Component\HttpFoundation\Response;
use Lcas\Exception\ApiNotSupportedException;


/**
 * @class Application
 * @brief HTTP APIのリクエストを受け付け、処理の分岐を行うクラス。
 */
class Application {

    /**
     * コンストラクタ
     */
    public function __construct() {

    }


    /**
     * HTTPメソッドやURLに応じた処理を実行する。
     * @param string $method HTTPメソッド
     * @param string $path リクエストのパス部分
     * @param ApiRequest $request HTTPリクエストのオブジェクト
     * @return Response HTTPレスポンスのオブジェクト
     * @throws Exception\RouteNotFoundException
     */
    private function dispatch($method, $path, $request) {
        $router = new Router();
        $route = $router->getRoute($method, $path);

        if(!$route) {
            throw new RouteNotFoundException("無効なURLです。 method:{$method}, path:{$path}");
        }

        $controllerName = "\\Lcas\\Controller\\" . $route['controller'];
        if(!class_exists($controllerName)) {
            throw new RouteNotFoundException("ルーティングに対応するクラスが存在しません。 method:{$method}, path:{$path}, class:{$controllerName}");
        }
        $actionName = $route['action'];
        /**
         * @var ControllerInterface $controller
         */
        $controller = new $controllerName();
        if(!method_exists($controller, $actionName)) {
            throw new RouteNotFoundException("ルーティングに対応するアクションが存在しません。 method:{$method}, path:{$path}, class:{$controllerName}, action: {$actionName}");
        }
        $controller->initialize();
        $authResult = $controller->authenticate($request, $actionName);
        switch($authResult) {
            case AUTH_RESULT_NOT_PROVIDED:
            case AUTH_RESULT_NOT_FOUND:
                $response = new Response('セッションIDが無効です', 401);
                break;
            default:
                $response = $controller->$actionName($request, $route['captured']);
        }
        return $response;

    }

    /**
     * 受け取ったリクエストに応じて処理を実行し、レスポンスの出力を行う。
     *
     * @param array $parameters 起動パラメータ。
     *
     * - $parameters['request']: URL、HTTPヘッダなどを含んだApiRequestオブジェクト。
     *
     */
    public function run($parameters) {
        /**
         * @var ApiRequest $request
         */
        $request = $parameters['request'];
        $this->logInput($request);


        $method = $request->getMethod();
        $path = parse_url($request->getRequestUri(), PHP_URL_PATH);
        try {
            $response = $this->dispatch($method, $path, $request);
            $this->logOutput($request, $response);

            $db = DB::getMasterDb();
            $db->Close();

            $response->sendHeaders();
            $response->sendContent();
        } catch(RouteNotFoundException $e) {
            header('HTTP/1.0 404 Not Found');
            $this->logError($request, '404 Not Found', null);
            exit;
        } catch(BadParameterException $e) {
            $r = new JsonResponse(array('errors' => array('入力データにエラーがありました。')), 400);
            $this->logError($request, $r, $e);
            $r->sendHeaders();
            $r->sendContent();
        } catch(ApiTimeoutException $e) {
            $r = new JsonResponse(array('errors' => array('サーバー内でエラーが発生しました。')), 504);
            $this->logError($request, $r, $e);
            $r->sendHeaders();
            $r->sendContent();
        } catch(ApiNotSupportedException $e) {
            $r = new JsonResponse(array('errors' => array('未対応エラーが発生しました。')), 403);
            $this->logError($request, $r, $e);
            $r->sendHeaders();
            $r->sendContent();
        } catch(ZworksApiException $e) {
            $r = new JsonResponse(array('errors' => array('サーバー内でエラーが発生しました。')), 400);
            $this->logError($request, $r, $e);
            $r->sendHeaders();
            $r->sendContent();
        } catch(MailException $e) {
            $r = new JsonResponse(array('errors' => array('メールの送信に失敗しました。')), 500);
            $this->logError($request, $r, $e);
            $r->sendHeaders();
            $r->sendContent();
        } catch(LcasException $e) {
            $r = new JsonResponse(array('errors' => array('システム内でエラーが発生しました。')), 500);
            $this->logError($request, $r, $e);
            $r->sendHeaders();
            $r->sendContent();
        }
    }


    /**
     * HTTPリクエストに関する情報をログに記録する
     * @param ApiRequest $request HTTPリクエスト
     */
    public function logInput(ApiRequest $request) {
        $logger = Logger::get();
        $content = sprintf('REQUEST: %s %s, QUERY: %s, BODY: %s, Headers: %s, IP: %s',
            $request->getMethod(), $request->getRequestUri(),
            $request->getQueryString(), $request->getContent(), var_export($request->headers->all(), true), $request->getClientIp());

        $logger->addDebug($content);
    }

    /**
     * HTTPレスポンスに関する情報をログに記録する
     * @param ApiRequest $request HTTPリクエスト
     * @param mixed $content HTTPレスポンス
     */
    public function logOutput(ApiRequest $request, $content) {
        $logger = Logger::get();
        if($content instanceof Response) {
            $logger->addDebug(sprintf('RESPONSE: %d, CONTENT: %s', $content->getStatusCode(), $content->getContent()));
        } else {
            $logger->addDebug(sprintf('RESPONSE: %s', $content));
        }
    }

    /**
     * エラー情報をログに記録する
     * @param ApiRequest $request HTTPリクエスト
     * @param mixed $content HTTPレスポンス
     * @param \Exception $e 例外オブジェクト
     */
    public function logError(ApiRequest $request, $content, $e=null) {
        $logger = Logger::get();
        $message = '';
        if($content instanceof Response) {
            $message = sprintf('RESPONSE: %d, CONTENT: %s', $content->getStatusCode(), $content->getContent());
        } else {
            $message = sprintf('RESPONSE: %s', $content);
        }
        if($e instanceof \Exception) {
            $message .= ', Exception: ' . $e->getMessage();
            $message .= ', Trace: ' . $e->getTraceAsString();
        }
        $logger->addInfo($message);
        if($e instanceof \Exception) {
            $logger->addInfo($e->getTraceAsString());
        }
    }

}

