<?php

/**
 * @file
 * @brief HTTP API用の認証処理を行う
 */

namespace Lcas\Auth;

use Lcas\Repository\AuthTokenRepository;
use Lcas\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * @class AuthHandler
 * @brief HTTP API用の認証処理を行う。
 */
class AuthHandler {

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var AuthTokenRepository
     */
    private $authTokenRepository;

    /**
     * @var array
     */
    private $user;

    /**
     * コンストラクタ
     */
    public function __construct() {
        $this->userRepository = new UserRepository();
        $this->authTokenRepository = new AuthTokenRepository();
        $this->user = null;
    }

    /**
     * 認証処理を実行する
     * @param Request $request HTTPリクエスト
     * @return int AUTH_RESULT_XXX定数
     */
    public function authenticate(Request $request) {
        $tokenKey = $request->headers->get(AUTH_TOKEN_HEADER_NAME);
        if(!$tokenKey) {
            $tokenKey = $request->cookies->get(AUTH_SESS_COOKIE_NAME);
            if(!$tokenKey) {
                return AUTH_RESULT_NOT_PROVIDED;
            }
        }

        //トークンが存在するか確認
        $token = $this->authTokenRepository->find($tokenKey);
        if(!$token) {
            return AUTH_RESULT_NOT_FOUND;
        }

        //有効期限の確認
        $tokenExpires = strtotime($token['expires']);
        if($tokenExpires <= time()) {
            return AUTH_RESULT_NOT_FOUND;
        }

        //有効期限の更新
        $newExpirationDate = date('Y-m-d H:i:s', time() + AUTH_TOKEN_LIFE_TIME);
        $this->authTokenRepository->updateExpirationDate($tokenKey, $newExpirationDate);

        //トークンに紐付くユーザーを確認
        $this->user = $this->userRepository->findById($token['user_id']);
        if(!$this->user) {
            return AUTH_RESULT_NOT_FOUND;
        }
        return AUTH_RESULT_OK;
    }


    /**
     * usersテーブルのエントリを返す
     * @return array
     */
    public function getUser() {
        return $this->user;
    }

}
