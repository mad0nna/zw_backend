<?php

/**
 * @file
 * @brief HTTP API用の認証処理を行うクラスのインターフェース
 *
 * 認証処理はこのインターフェースを使用することを想定していましたが、
 * 現在、このインターフェースは未使用です。
 */

namespace Lcas\Auth;

use Symfony\Component\HttpFoundation\Request;

/**
 * @interface AuthHandlerInterface
 * @brief HTTP API用の認証処理を行うクラスのインターフェース
 *
 * 認証処理はこのインターフェースを使用することを想定していましたが、
 * 現在、このインターフェースは未使用です。
 */
interface AuthHandlerInterface {

    /**
     * 認証処理を実行する
     * @param Request $request HTTPリクエスト
     * @return int AUTH_RESULT_XXX定数
     */
    public function authenticate(Request $request);


    /**
     * ユーザーID(users.id)を返す
     * @return int
     */
    public function getUserId();

}
