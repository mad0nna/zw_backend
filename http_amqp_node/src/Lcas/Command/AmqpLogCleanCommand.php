<?php

/**
 * @file
 * @brief AMQP関連の操作を実行するコマンド
 */

namespace Lcas\Command;

use Lcas\Log\Logger;
use Lcas\Repository\ZworksNotificationRepository;
use Lcas\Repository\ZworksRequestRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;


/**
 * @class AmqpLogCleanCommand
 * @brief AMQP関連の操作を実行するコマンド
 */
class AmqpLogCleanCommand extends Command {


    /**
     * @var ZworksNotificationRepository
     */
    private $zworksNotificationRepository;

    /**
     * @var ZworksRequestRepository
     */
    private $zworksRequestRepository;


    /**
     * コマンドの初期設定を実行する。
     */
    protected function configure() {
        $this
            ->setName('amqp:log:clean')
            ->setDescription('古いAMQPの通知ログの削除を実行します。')
            ->addOption(
                'batch',
                'b',
                InputOption::VALUE_NONE,
                '実行前に実行可否の入力を求めずに実行します。'
            )
        ;

        $this->zworksNotificationRepository = new ZworksNotificationRepository();
        $this->zworksRequestRepository = new ZworksRequestRepository();
    }

    /**
     * コマンドの処理本体を実行する。
     * @param InputInterface $input 入力パラメータ
     * @param OutputInterface $output 出力ストリーム
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $logger = Logger::get();

        $holdingPeriod = (int)HOLDING_PERIOD_DAYS_ZWORKS_NOTIFICATION;
        if($holdingPeriod < 1) {
            throw new RuntimeException('HOLDING_PERIOD_DAYS_ZWORKS_NOTIFICATION の設定値が不正です。');
        }

        $startDate = date('Y-m-d 00:00:00', strtotime("-{$holdingPeriod} days"));
        $limit = 100000;
        $sleepInterval = 1;

        $logger->addInfo('========================================================');
        $logger->addInfo('BATCH PROCESS START: amqp:log:clean');
        $logger->addInfo("削除条件：");
        $logger->addInfo("　・削除期間： {$startDate}以前");

        $output->writeln('BATCH PROCESS START: amqp:log:clean');
        $output->writeln("<info>削除条件：</info>");
        $output->writeln("<info>　・削除期間： {$startDate}以前</info>");
        if (!$input->getOption('batch')) {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('<question>以上の条件でAMQP通知ログ(zworks_notificaitons, zworks_requests)の削除を実行します。よろしいですか？</question>(y/n)', false);

            if (!$helper->ask($input, $output, $question)) {
                $logger->addInfo('User Canceled.');
                $output->writeln("実行を中止しました。");
                return;
            }
        }


        //10万件単位でデータを削除する
        //zworks_notifications
        $startTime = microtime(true);
        $rowsRemoved = 0;
        while(true) {
            $affectedRows = $this->zworksNotificationRepository->removeLog($startDate, $limit);
            $rowsRemoved += $affectedRows;
            if($affectedRows == 0) {
                break;
            }
            sleep($sleepInterval);
        }
        $endTime = microtime(true);
        $elapsedTime = round(($endTime - $startTime), 3);

        $logger->addInfo('');
        $logger->addInfo('zworks_notifications: Elapsed time: ' . $elapsedTime . ' sec');
        $logger->addInfo('zworks_notifications: Removed record count: ' . $rowsRemoved);

        $output->writeln('');
        $output->writeln('zworks_notifications: Elapsed time: ' . $elapsedTime . ' sec');
        $output->writeln('zworks_notifications: Removed record count: ' . $rowsRemoved);


        //zworks_requests
        $startTime = microtime(true);
        $rowsRemoved = 0;
        while(true) {
            $affectedRows = $this->zworksRequestRepository->removeLog($startDate, $limit);
            $rowsRemoved += $affectedRows;
            if($affectedRows == 0) {
                break;
            }
            sleep($sleepInterval);
        }
        $endTime = microtime(true);
        $elapsedTime = round(($endTime - $startTime), 3);

        $logger->addInfo('');
        $logger->addInfo('zworks_requests: Elapsed time: ' . $elapsedTime . ' sec');
        $logger->addInfo('zworks_requests: Removed record count: ' . $rowsRemoved);

        $output->writeln('');
        $output->writeln('zworks_requests: Elapsed time: ' . $elapsedTime . ' sec');
        $output->writeln('zworks_requests: Removed record count: ' . $rowsRemoved);

        $logger->addInfo('BATCH PROCESS END: amqp:log:clean');
        $output->writeln('BATCH PROCESS END: amqp:log:clean');
   }
}
