<?php

/**
 * @file
 * @brief デバイスデータの削除を行うコマンド
 */

namespace Lcas\Command;

use Lcas\Log\Logger;
use Lcas\Repository\ScenarioRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;


/**
 * @class ScenarioEventCleanCommand
 * @brief シナリオイベントの削除を行うコマンド
 */
class ScenarioEventCleanCommand extends Command {


    /**
     * @var ScenarioRepository
     */
    private $scenarioRepository;

    /**
     * コマンドの初期設定を実行する。
     */
    protected function configure() {
        $this
            ->setName('scenario:event:clean')
            ->setDescription('古いシナリオイベントの削除を実行します。')
            ->addOption(
                'batch',
                'b',
                InputOption::VALUE_NONE,
                '実行前に実行可否の入力を求めずに実行します。'
            )
        ;

        $this->scenarioRepository = new ScenarioRepository();
    }


    /**
     * コマンドの処理本体を実行する。
     * @param InputInterface $input 入力パラメータ
     * @param OutputInterface $output 出力ストリーム
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $logger = Logger::get();

        $holdingPeriod = (int)HOLDING_PERIOD_DAYS_SCENARIO_EVENT;
        if($holdingPeriod < 1) {
            throw new RuntimeException('HOLDING_PERIOD_DAYS_SCENARIO_EVENT の設定値が不正です。');
        }

        $startDate = date('Y-m-d 00:00:00', strtotime("-{$holdingPeriod} days"));
        $limit = 100000;
        $sleepInterval = 1;

        $logger->addInfo('========================================================');
        $logger->addInfo('BATCH PROCESS START: scenario:event:clean');
        $logger->addInfo("削除条件：");
        $logger->addInfo("　・削除期間： {$startDate}以前");

        $output->writeln('BATCH PROCESS START: scenario:event:clean');
        $output->writeln("<info>削除条件：</info>");
        $output->writeln("<info>　・削除期間： {$startDate}以前</info>");
        if (!$input->getOption('batch')) {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('<question>以上の条件でシナリオイベントの削除を実行します。よろしいですか？</question>(y/n)', false);

            if (!$helper->ask($input, $output, $question)) {
                $logger->addInfo('User Canceled.');
                $output->writeln("実行を中止しました。");
                return;
            }
        }


        //10万件単位でデータを削除する
        $startTime = microtime(true);
        $rowsRemoved = 0;
        while(true) {
            $affectedRows = $this->scenarioRepository->removeEventBefore($startDate, $limit);
            $rowsRemoved += $affectedRows;
            if($affectedRows == 0) {
                break;
            }
            sleep($sleepInterval);
        }
        $endTime = microtime(true);
        $elapsedTime = round(($endTime - $startTime), 3);

        $logger->addInfo('');
        $logger->addInfo('Elapsed time: ' . $elapsedTime . ' sec');
        $logger->addInfo('Removed record count: ' . $rowsRemoved);

        $output->writeln('');
        $output->writeln('Elapsed time: ' . $elapsedTime . ' sec');
        $output->writeln('Removed record count: ' . $rowsRemoved);


        $logger->addInfo('BATCH PROCESS END: scenario:event:clean');
        $output->writeln('BATCH PROCESS END: scenario:event:clean');
   }
}
