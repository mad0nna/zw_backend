<?php

/**
 * @file
 * @brief 無効になったSNS Endpointの削除を行うコマンド
 */

namespace Lcas\Command;

use Lcas\Adapter\MobilePushAdapterInterface;
use Lcas\Log\Logger;
use Lcas\ServiceContainer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;


/**
 * @class SnsEndpointCleanCommand
 * @brief 無効になったSNS Endpointの削除を行うコマンド
 */
class SnsEndpointCleanCommand extends Command {


    /**
     * @var MobilePushAdapterInterface
     */
    private $mobilePushAdapter;

    /**
     * コマンドの初期設定を実行する。
     */
    protected function configure() {
        $this
            ->setName('sns:endpoint:clean')
            ->setDescription('古いシナリオイベントの削除を実行します。')
            ->addOption(
                'batch',
                'b',
                InputOption::VALUE_NONE,
                '実行前に実行可否の入力を求めずに実行します。'
            )
        ;

        $this->mobilePushAdapter = ServiceContainer::getInstance()->get('mobile_push_adapter');
    }

    /**
     * コマンドの処理本体を実行する。
     * @param InputInterface $input 入力パラメータ
     * @param OutputInterface $output 出力ストリーム
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $logger = Logger::get();


        $logger->addInfo('========================================================');
        $logger->addInfo('BATCH PROCESS START: sns:endpoint:clean');

        $output->writeln('BATCH PROCESS START: sns:endpoint:clean');
        if (!$input->getOption('batch')) {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('<question>無効になったSNS Endpointの削除を実行します。よろしいですか？</question>(y/n)', false);

            if (!$helper->ask($input, $output, $question)) {
                $logger->addInfo('User Canceled.');
                $output->writeln("実行を中止しました。");
                return;
            }
        }

        //3件処理する毎に1秒sleepする
        //1ページ(100県単位)毎に3秒スリープする
        $sleepThreshold = 3;
        $sleepInterval = 1;
        $pageSleepInterval = 3;

        //Android版
        $startTime = microtime(true);
        $rowsRemoved = 0;
        $nextToken = null;
        while(true) {
            $result = $this->mobilePushAdapter->getEndpointList(PLATFORM_GCM, $nextToken);

            foreach($result['Endpoints'] as $endpoint) {
                if($endpoint['Attributes']['Enabled'] == 'true') {
                    //有効なEndpointは対象外
                    continue;
                }
                $output->writeln('Disabled Endpoint: ' . $endpoint['EndpointArn']);
                $this->mobilePushAdapter->deleteEndpoint($endpoint['EndpointArn']);
                $rowsRemoved++;

                //APIの高頻度な呼び出しを避けるため、数件削除した後sleepする。
                if($rowsRemoved % $sleepThreshold == 0) {
                    sleep($sleepInterval);
                }
            }
            if(!isset($result['NextToken'])) {
                break;
            }
            sleep($pageSleepInterval);
            $nextToken = $result['NextToken'];
        }
        $endTime = microtime(true);
        $elapsedTime = round(($endTime - $startTime), 3);

        $logger->addInfo('');
        $logger->addInfo('Elapsed time: ' . $elapsedTime . ' sec');
        $logger->addInfo('Removed record count: ' . $rowsRemoved);

        $output->writeln('');
        $output->writeln('Elapsed time: ' . $elapsedTime . ' sec');
        $output->writeln('Removed record count: ' . $rowsRemoved);


        $logger->addInfo('BATCH PROCESS END: sns:endpoint:clean');
        $output->writeln('BATCH PROCESS END: sns:endpoint:clean');
   }
}
