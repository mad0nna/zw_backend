<?php

/**
 * @file
 * @brief コントローラクラスの基底クラス
 *
 * 各コントローラ共通の処理を定義します。
 */


namespace Lcas\Controller;

use Lcas\ApiRequest;
use Lcas\Auth\AuthHandler;
use Lcas\HttpNode\JsonResponse;
use Lcas\Log\Logger;

/**
 * @class BaseController
 * @brief コントローラクラスの基底クラス
 *
 * 各コントローラ共通の処理を定義します。
 */
class BaseController implements ControllerInterface {

    /**
     * 認証処理を行うオブジェクト
     * @var AuthHandler
     */
    protected $authHandler;


    /**
     * コンストラクタ
     */
    public function __construct() {
        $this->authHandler = new AuthHandler();
    }

    public function initialize() {
    }


    public function authenticate(ApiRequest $request, $actionName) {
        return $this->authHandler->authenticate($request);
    }

    /**
     * ログイン中のユーザーに対応するusersテーブルのエントリを返す。
     * @return array
     */
    public function getUser() {
        return $this->authHandler->getUser();
    }

    public function getUserId() {
        $user = $this->getUser();
        return isset($user['id']) ? $user['id'] : false;
    }


    /**
     * 一般的なエラー用のレスポンスを返す
     * @param array $messages エラーメッセージを含んだ配列
     * @param string|string[] $detailedError ログにのみ記録する、エラーの詳細情報
     * @param int $statusCode ステータスコード
     * @return JsonResponse
     */
    public function onError($messages, $detailedError, $statusCode=400) {
        $logger = Logger::get();
        $userId = $this->getUserId();
        if(is_array($detailedError)) {
        	foreach($detailedError as $e) {
      		  	$logger->addDebug('user_id: ' . $userId . ', ' . $e);
        	}
        } else {
        	$logger->addDebug('user_id: ' . $userId . ', ' . $detailedError);
        }
        return new JsonResponse(array('errors' => $messages), $statusCode);
    }


    /**
     * ID指定系のAPIでデータの取得に失敗した場合のレスポンスを返す
     * @param array $messages エラーメッセージを含んだ配列
     * @param string $detailedError ログにのみ記録する、エラーの詳細情報
     * @return JsonResponse
     */
    public function onDataNotFound($messages, $detailedError) {
        return $this->onError($messages, $detailedError, 404);
    }


}

