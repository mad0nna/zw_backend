<?php

/**
 * @file
 * @brief コントローラクラスのインターフェース
 */

namespace Lcas\Controller;

use Lcas\ApiRequest;
use Psr\Log\LoggerInterface;

/**
 * @interface ControllerInterface
 * @brief コントローラクラスのインターフェース
 */
Interface ControllerInterface {

    /**
     * 認証フェーズの前に行う初期化処理を実行します。
     * @return mixed
     */
    public function initialize();

    /**
     * 認証処理を行います。
     * @param ApiRequest $request HTTPリクエスト
     * @param string $actionName アクション名
     * @return int 認証結果(AUTH_RESULT_X)
     *
     * 認証が不要なactionでは、このメソッドが AUTH_RESULT_OKを返す必要があります。
     */
    public function authenticate(ApiRequest $request, $actionName);


    /**
     * 認証したユーザーのユーザーIDを返す。
     * @return int
     */
    public function getUserId();

}

