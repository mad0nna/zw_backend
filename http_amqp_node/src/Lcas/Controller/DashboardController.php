<?php

/**
 * @file
 * @brief コントローラクラスの基底クラス
 *
 * 各コントローラ共通の処理を定義します。
 */


namespace Lcas\Controller;

use Lcas\ApiRequest;
use Lcas\HttpNode\JsonResponse;
use Lcas\Service\TileService;
use Lcas\Repository\TileRepository;

/**
 * @class DashboardController
 * @brief
 */
class DashboardController extends BaseController
{
	/**
	 * @var \Lcas\Service\TileService
	 */
	private $tileService;

	/**
	 * @var \Lcas\Repository\TileRepository
	 */
	private $tileRepository;

	/**
	 * コンストラクタ
	 */
	public function __construct() {
		parent::__construct();
	}


	/**
	 * @inheritdoc
	 */
	public function initialize() {
		$this->tileService = new TileService();
		$this->tileRepository = new TileRepository();
	}

	/**
	 * Updates the sequence of the tiles
	 * in the dashboard
	 *
	 * @param Lcas\ApiRequest $request
	 * @param array $capturedParams
	 * @return array
	 */
	public function updateTileAction(ApiRequest $request, $capturedParams) {
		// retrieve the post data
		$postData = $request->getContentAsJson();

		// set initial status code
		$statusCode = 200;

		// initialize response data
		$data = [];

		try {
			// get the tiles from the ajax request
			$tiles = $postData->get('tiles');

			// retrieve user id
			$userId = $this->getUserId();

			$data['results'] = true;

			if(count($tiles) > 1) {
				$data['results'] = $this->tileService->updateTiles($userId, $tiles);	
			}
		} catch(\Exception $e) {
			// set error message
			$data = [
				'error' => $e->getMessage()
			];

			// set error status code
			$statusCode = 500;
		}

		return new JsonResponse($data, $statusCode);
	}

	/**
	 * Updates the sequence of the tiles
	 * in the dashboard
	 *
	 * @param Lcas\ApiRequest $request
	 * @param array $capturedParams
	 * @return array
	 */
	public function getUserTileAction(ApiRequest $request, $capturedParams) {

			// set initial status code
			$statusCode = 200;

			// initialize response data
			$data = [];
	
			try {
	
				// retrieve user id
				$userId = $this->getUserId();
	
				$data['results'] = true;
				$data = $this->tileService->getUserTiles($userId);
	
			} catch(\Exception $e) {
				// set error message
				$data = [
					'error' => $e->getMessage()
				];
	
				// set error status code
				$statusCode = 500;
			}
			return new JsonResponse($data, $statusCode);
	}
}