<?php

/**
 * @file
 * @brief デバイス関連のAPIのコントローラ
 */

namespace Lcas\Controller;

use Lcas\ApiRequest;
use Lcas\Repository\DeviceRepository;
use Lcas\Service\DeviceService;
use Lcas\HttpNode\JsonResponse;
use Respect\Validation\Validator as v;
use Symfony\Component\HttpFoundation\Response;
use Lcas\DB\DB;
use Lcas\Repository\NodeRepository;

/**
 * @class DeviceController
 * @brief デバイス関連のAPIのコントローラ
 */
class DeviceController extends BaseController {

    /**
     * @var \Lcas\Service\DeviceService
     */
    private $deviceService;

    /**
     * @var \Lcas\Repository\DeviceRepository
     */
    private $deviceRepository;
    
    /**
     * @var \Lcas\Repository\NodeRepository
     */
    private $nodeRepository;

    /**
     * コンストラクタ
     */
    public function __construct() {
        parent::__construct();
    }


    public function initialize() {
        $this->deviceService = new DeviceService();
        $this->deviceRepository = new DeviceRepository();
        $this->nodeRepository = new NodeRepository();
    }

    /**
     * GET devices
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function listUserDeviceAction(ApiRequest $request, $capturedParams) {
        $userId = $this->getUserId();
        $label = $request->get('label', '');

        $result = $this->deviceService->getUserDeviceList($userId, $label);
        $statusCode = 200;
        $response = new JsonResponse($result, $statusCode);
        return $response;
    }


    /**
     * GET devices/:id
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function getDeviceDetailAction(ApiRequest $request, $capturedParams) {
        //capturedParamsに含まれるパラメータは、正規表現によるチェックを実施済
        $deviceId = $capturedParams['id'];

        $detailedErrors = array();
        if(!$this->deviceRepository->isGrantedUser($deviceId, $this->getUserId())) {
            $error = 'Deviceにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $error . ' device_id: ' . $deviceId);
        }

        $result = $this->deviceService->getDeviceDetail($deviceId);
        if(!$result) {
            $error = 'Deviceが見つかりません。';
            return $this->onDataNotFound(array($error), $error . ' id: ' . $deviceId);
        }
        $statusCode = 200;
        $response = new JsonResponse($result, $statusCode);
        return $response;
    }

    /**
     * GET devices/:id/values
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function listDeviceValueAction(ApiRequest $request, $capturedParams) {
        $deviceId = $capturedParams['id'];
        $from = $request->get('start', 0);
        $to = $request->get('end', 0);
        $latest = $request->query->has('latest');

        $detailedErrors = array();
        if(!v::optional(v::digit())->validate($from)) {
            $detailedErrors[] = "無効な検索期間(開始日)が指定されました。 from:" . $from;
        }
        if(!v::optional(v::digit())->validate($to)) {
            $detailedErrors[] = "無効な検索期間(終了日)が指定されました。 to:" . $to;
        }

        if(count($detailedErrors) > 0) {
            return $this->onError(array('入力データが無効です。'), $detailedErrors);
        }

        if(!$this->deviceRepository->isGrantedUser($deviceId, $this->getUserId())) {
            $error = 'Deviceにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $error . ' device_id:' . $deviceId);
        }

        if($latest) {
            $latestData = $this->deviceService->getLatestDeviceValue($deviceId);
            $result = (is_array($latestData)) ? array($latestData) : array();
        } else {
            $data = $this->deviceService->getDeviceValueList($deviceId, $from, $to);
            $result = (is_array($data)) ? $data : array();
        }

        $response = new JsonResponse();
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_BIGINT_AS_STRING);
        $response->setData($result);
        return $response;
    }

    /**
     * PUT devices/:id
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function updateLabelAction(ApiRequest $request, $capturedParams) {
        $deviceId = $capturedParams['id'];

        $postData = $request->getContentAsJson();
        $labels = $postData->get('labels', array());
        if(!is_array($labels)) {
        	$error = 'labelsが配列ではありません。';
        	return $this->onError(array($error), $error, 400);
        }
        $device = $this->deviceRepository->findById($deviceId);
        if(!$device) {
            $error = 'デバイスが見つかりません。';
            return $this->onDataNotFound(array($error), $error . ' device_id:' . $deviceId);
        }

        if(!$this->deviceRepository->isGrantedUser($deviceId, $this->getUserId(), array('owner_only' => true))) {
            $error = 'デバイスにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $error . ' device_id:' . $deviceId);
        }

        $db = DB::getMasterDb();
        $db->Begin();
        $this->deviceRepository->updateLabels($deviceId, $labels);
        $db->Commit();

        $response = new Response('', 204);
        return $response;
    }
    
    public function controlDeviceAction(ApiRequest $request, $capturedParams) {
    	$deviceId = $capturedParams['id'];
    	
    	$nodeId = $this->deviceRepository->findNodeById($deviceId);
    	if (!$this->nodeRepository->isGrantedUser($nodeId, $this->getUserId(), array('owner_only' => true))) {
    		$error = 'デバイスを操作する権限がありません。';
    		return $this->onDataNotFound(array($error), $error . ' device_id:' . $deviceId);
    	}
    	
    	$postData = $request->getContentAsJson();
    	$actionType = $postData->get('type', array());
    	$action = $postData->get('action', array());

    	$device = $this->deviceRepository->findById($deviceId);
    	if(!$device) {
    		$error = 'デバイスが見つかりません。';
    		return $this->onDataNotFound(array($error), $error . ' device_id:' . $deviceId);
    	}

    	$deviceType = $this->deviceRepository->getDeviceTypeNameFromType($device['type']);
    	if($deviceType === false) {
    		$error = 'デバイスの形式が不正です。';
    		return $this->onDataNotFound(array($error), $error . ' device_id:' . $deviceId);
    	}
    	
    	$nodeName = $this->deviceRepository->findNodeNameById($deviceId);
    	
    	$this->deviceService->controlDevice($nodeName, $deviceType, $actionType, $action);
   		$statusCode = 204;
   		$response = new JsonResponse('', $statusCode);
   		return $response;
   	}

}
