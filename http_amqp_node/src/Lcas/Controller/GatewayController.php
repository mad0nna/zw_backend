<?php

/**
 * @file
 * @brief ゲートウェイ関連のAPIのコントローラ
 */

namespace Lcas\Controller;

use Lcas\ApiRequest;
use Lcas\Exception\BadParameterException;
use Lcas\Exception\ZworksApiException;
use Lcas\HttpNode\JsonResponse;
use Lcas\Repository\GatewayRepository;
use Lcas\Service\GatewayService;
use Symfony\Component\HttpFoundation\Response;
use Lcas\DB\DB;

/**
 * @class GatewayController
 * @brief ゲートウェイ関連のAPIのコントローラ
 */
class GatewayController extends BaseController {

    /**
     * @var \Lcas\Service\GatewayService
     */
    private $gatewayService;

    /**
     * @var \Lcas\Repository\GatewayRepository
     */
    private $gatewayRepository;

    /**
     * コンストラクタ
     */
    public function __construct() {
        parent::__construct();
    }


    public function initialize() {
        $this->gatewayService = new GatewayService();
        $this->gatewayRepository = new GatewayRepository();
    }

    /**
     * GET gateways/:id
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function getGatewayDetailAction(ApiRequest $request, $capturedParams) {
        //$capturedParamsに含まれるパラメータは、正規表現によるチェックを実施済
        $gatewayId = $capturedParams['id'];
        $gateway = $this->gatewayRepository->findById($gatewayId);
        if(!$gateway) {
            $error = 'ゲートウェイが見つかりません。';
            $innerError = 'ゲートウェイにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId );
        }

        if(!$this->gatewayRepository->isGrantedUser($gatewayId, $this->getUserId())) {
            $error = '無効なゲートウェイが指定されました。';
            $innerError = 'ゲートウェイにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId );
        }

        $result = $this->gatewayService->getGatewayDetailedResponse($gateway['id']);
        $statusCode = 200;
        $response = new JsonResponse($result, $statusCode);
        return $response;
    }

    /**
     * GET gateways
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function listUserGatewaysAction(ApiRequest $request, $capturedParams) {
        $userId = $this->getUserId();
        $label = $request->get('label');
        $gateways = $this->gatewayRepository->findByUserId($userId);

        $filteredGatewayIds = array();
        if(!is_null($label)) {
            $filteredGatewayIds = $this->gatewayRepository->findGatewayIdListByLabel($label);
        }

        $results = array();
        foreach($gateways as $gateway) {
            if(!is_null($label) && !in_array($gateway['id'], $filteredGatewayIds)) {
                continue;
            }
            $results[] = $this->gatewayService->getGatewayDetailedResponse($gateway['id']);
        }

        $response = new JsonResponse($results);
        return $response;
    }

    /**
     * POST gateways
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function associateGatewayAction(ApiRequest $request, $capturedParams) {
        $user = $this->getUser();
        $postData = $request->getContentAsJson();

        $macAddr = $postData->get('mac_address');
        $key = $postData->get('key');

        if(strlen($macAddr) == 0) {
            $error = 'MACアドレスが指定されていません。';
            return $this->onError(array($error), $error);
        }

        try{
            $result = $this->gatewayService->associateGateway($user, $macAddr, $key);
            $response = new JsonResponse($result['response'], 200);
            return $response;
        } catch(BadParameterException $e) {
            //エラーの内容に応じて処理を振り分ける必要が発生した場合、
            //ここで例外処理を行う。
            return $this->onError(array('入力データにエラーがありました'), $e->getMessage());
        } catch(ZworksApiException $apiException) {
            return $this->onError(array('入力データにエラーがありました'), $apiException->getMessage());
        }
    }


    /**
     * DELETE gateways
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function dissociateGatewayAction(ApiRequest $request, $capturedParams) {
        $user = $this->getUser();
        $gatewayId = (int)$capturedParams['id'];

        if(!$this->gatewayRepository->isGrantedUser($gatewayId, $this->getUserId())) {
            $error = '無効なゲートウェイが指定されました。';
            $innerError = 'ゲートウェイにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId );
        }

        $this->gatewayService->disssociateGateway($user, $gatewayId);
        $response = new Response('', 204);
        return $response;
    }


    /**
     * PUT gateways/:id
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function updateLabelAction(ApiRequest $request, $capturedParams) {
        $gatewayId = (int)$capturedParams['id'];
        $postData = $request->getContentAsJson();
        $labels = $postData->get('labels', array());
        $dustIp = $postData->get('dust_ip');

        $gateway = $this->gatewayRepository->findById($gatewayId);
        if(!$gateway) {
            $error = '無効なゲートウェイが指定されました。';
            $innerError = 'ゲートウェイが見つかりません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId);
        }
        if(!$this->gatewayRepository->isGrantedUser($gatewayId, $this->getUserId())) {
            $error = '無効なゲートウェイが指定されました。';
            $innerError = 'ゲートウェイにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId );
        }

        // 入力値チェック
        $errorMessages = array();
        if (isset($dustIp)) {
	        if(!$this->validateDustIp($dustIp, $errorMessages)) {
	            return $this->onError(array($errorMessages), $errorMessages);
	        }
        }
        
	    $response_code = $this->gatewayService->updateLabelsAndDustIp($gatewayId, $labels, $dustIp);

        $response = new Response('', $response_code);
        return $response;
    }

    private function validateDustIp($dustIp, &$errorMessages) {
        $errorMessages = array();

        $requiredDustIpParameters = array(
            'network_id', 'acl'
        );

        foreach ($requiredDustIpParameters as $keyName) {
            if (!isset($dustIp[$keyName])) {
                $errorMessages[] = 'dust_ip.' . $keyName . 'が指定されていません。';
            }
        }

        $acl = $dustIp["acl"];
        if (is_array($acl) && count($acl) > 0) {

            $requiredAclParameters = array(
                'mac_addr', 'join_key'
            );

            foreach ($acl as $record) {
                foreach ($requiredAclParameters as $keyName) {
                    if (!isset($record[$keyName])) {
                        $errorMessages[] = 'dust_ip.acl.' . $keyName . 'が指定されていません。';
                    }
                }
            }
        } else if (!(is_array($acl) && count($acl) == 0)){
            $errorMessages[] = 'dust_ip.acl.mac_addrが指定されていません。';
            $errorMessages[] = 'dust_ip.acl.join_keyが指定されていません。';
        }

        return count($errorMessages) == 0;
    }


    /**
     * POST gateways/:id
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function updateGatewayStateAction(ApiRequest $request, $capturedParams) {
        $userId = $this->getUserId();
        $gatewayId = (int)$capturedParams['id'];
        $postData = $request->getContentAsJson();
        $mode = $postData->get('zwave[mode]', '', true);
        $flag = $postData->get('zwave[flag]', '', true);

        if(!$this->gatewayRepository->isGrantedUser($gatewayId, $this->getUserId())) {
            $error = '無効なゲートウェイが指定されました。';
            $innerError = 'ゲートウェイにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId );
        }

        if(strlen($mode) == 0) {
            return new Response('modeが指定されていません', 400);
        }
        if(strlen($flag) == 0) {
            return new Response('flagが指定されていません', 400);
        }

        $this->gatewayService->updateGatewayState($gatewayId, $mode, $flag);

        $response = new Response('', 204);
        return $response;
    }

    /**
     * @param ApiRequest $request
     * @param $capturedParams
     */
    public function updateFirmwareAction(ApiRequest $request, $capturedParams) {
        $gatewayId = (int)$capturedParams['id'];
        $postData = $request->getContentAsJson();
        $fwVersion = $postData->get('fw_version');
        
        if (strlen($fwVersion) === 0) {
        	return new Response('fw_versionが指定されていません', 400);
        }

        $gateway = $this->gatewayRepository->findById($gatewayId);
        if(!$gateway) {
            $error = '無効なゲートウェイが指定されました。';
            $innerError = 'ゲートウェイが見つかりません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId);
        }
        if(!$this->gatewayRepository->isGrantedUser($gatewayId, $this->getUserId())) {
            $error = '無効なゲートウェイが指定されました。';
            $innerError = 'ゲートウェイにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId );
        }

        $db = DB::getMasterDb();
        $db->Begin();
        $this->gatewayService->updateFirmware($gatewayId, $fwVersion);
        $db->Commit();

        $response = new Response('', 204);
        return $response;
    }


    public function getPublicKeyAction(ApiRequest $request, $capturedParams) {
        $gatewayId = (int)$capturedParams['id'];
        $gateway = $this->gatewayRepository->findById($gatewayId);

        if(!$gateway) {
            $error = 'ゲートウェイが見つかりません。';
            $innerError = 'ゲートウェイにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId );
        }

        if(!$this->gatewayRepository->isGrantedUser($gatewayId, $this->getUserId())) {
            $error = '無効なゲートウェイが指定されました。';
            $innerError = 'ゲートウェイにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId );
        }


        $publicKey = $gateway["public_key"];

        if (isset($publicKey)) {
            $result = Array (
                "public_key" => $publicKey
            );

            $response = new JsonResponse($result, 200);
        } else {
            $response = new Response('', 404);
        }
        return $response;
    }


    public function updatePublicKeyAction(ApiRequest $request, $capturedParams) {
        $gatewayId = (int)$capturedParams['id'];
        $gateway = $this->gatewayRepository->findById($gatewayId);

        if(!$gateway) {
            $error = 'ゲートウェイが見つかりません。';
            $innerError = 'ゲートウェイにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId );
        }

        if(!$this->gatewayRepository->isGrantedUser($gatewayId, $this->getUserId())) {
            $error = '無効なゲートウェイが指定されました。';
            $innerError = 'ゲートウェイにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $innerError . ' gateway_id:' . $gatewayId );
        }

        $result = $this->gatewayService->updatePublicKey($gatewayId);

        // todo: map amqp response code to http response code

        $response = new JsonResponse($result["response"], $result["result_code"]);
        return $response;
    }
}
