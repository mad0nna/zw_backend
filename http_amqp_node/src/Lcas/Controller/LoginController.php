<?php

/**
 * @file
 * @brief ログイン処理のコントローラ
 */

namespace Lcas\Controller;

use Lcas\ApiRequest;
use Lcas\HttpNode\JsonResponse;
use Lcas\Repository\AuthTokenRepository;
use Lcas\Repository\UserRepository;
use Lcas\Service\UserService;
use Symfony\Component\HttpFoundation\Cookie;
use Lcas\DB\DB;
use Symfony\Component\HttpFoundation\Response;


/**
 * @class LoginController
 * @brief ログイン処理のコントローラ
 */
class LoginController extends BaseController {

    /**
     * @var \Lcas\Service\UserService
     */
    private $userService;

    /**
     * @var \Lcas\Repository\UserRepository
     */
    private $userRepository;

    /**
     * @var \Lcas\Repository\AuthTokenRepository
     */
    private $authTokenRepository;

    /**
     * コンストラクタ
     */
    public function __construct() {
        parent::__construct();
    }


    public function initialize() {
        $this->userService = new UserService();
        $this->userRepository = new UserRepository();
        $this->authTokenRepository = new AuthTokenRepository();
    }

    public function authenticate(ApiRequest $request, $actionName) {
        //loginActionは認証が不要なため、無条件でAUTH_RESULT_OKを返す。
        $noAuthList = array('loginAction', 'loginStatusAction');
        if(in_array($actionName, $noAuthList)) {
            return AUTH_RESULT_OK;
        }
        return parent::authenticate($request, $actionName);
    }


    /**
     * POST login
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function loginAction(ApiRequest $request, $capturedParams) {
        $postData = $request->getContentAsJson();
        $email = $postData->get('email', '');
        $pass = $postData->get('password', '');

        $errorMessages = array();
        if(strlen($email) == 0) {
            $errorMessages[] = 'メールアドレスが指定されていません';
        }
        if(strlen($pass) == 0) {
            $errorMessages[] = 'パスワードが指定されていません';
        }

        if(count($errorMessages) > 0) {
            return $this->onError($errorMessages, $errorMessages, 400);
        }
        $user = $this->userRepository->findUserByLoginInfo($email, $pass);
        if(!$user) {
            $error = 'メールアドレス、またはパスワードが間違えています。';
            return $this->onError(array($error), $error, 401);
        }

        $token = $this->authTokenRepository->getNewUniqueTokenId(AUTH_TOKEN_LENGTH);

        $db = DB::getMasterDb();
        $db->Begin();
        $this->authTokenRepository->register($user['id'], $token);
        $db->Commit();
        $response = new Response('', 204);
        $response->headers->setCookie(new Cookie(AUTH_SESS_COOKIE_NAME, $token));
        
        if (defined('FORCE_CROSS_DOMAIN_COOKIE') && FORCE_CROSS_DOMAIN_COOKIE) {
	        // for Safari's cross-domain cookie bug.
	        $response->headers->set(AUTH_TOKEN_HEADER_NAME, $token);
        }
        
        return $response;
    }


    /**
     * GET login
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function loginStatusAction(ApiRequest $request, $capturedParams) {
        $loginStatus = $this->authHandler->authenticate($request);
        $statusString = ($loginStatus == AUTH_RESULT_OK) ? 'OK' : 'NG';

        $response = new JsonResponse(array('status' => $statusString), 200);
        return $response;
    }
}
