<?php

/**
 * @file
 * @brief ログイン処理のコントローラ
 */

namespace Lcas\Controller;

use Lcas\ApiRequest;
use Lcas\Log\Logger;
use Lcas\Repository\NodeRepository;
use Lcas\Service\NodeService;
use Lcas\HttpNode\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Respect\Validation\Validator as v;

/**
 * @class NodeController
 * @brief ログイン処理のコントローラ
 */
class NodeController extends BaseController {

    /**
     * @var \Lcas\Service\NodeService
     */
    private $nodeService;

    /**
     * @var \Lcas\Repository\NodeRepository
     */
    private $nodeRepository;


    /**
     * @var \MonoLog\Logger
     */
    private $logger;

    public function __construct() {
        parent::__construct();
    }


    /**
     * @inheritdoc
     */
    public function initialize() {
        $this->nodeService = new NodeService();
        $this->nodeRepository = new NodeRepository();
        $this->logger = Logger::get();
    }

    /**
     *
     */
    public function listUserNodeAction(ApiRequest $request, $capturedParams) {
        $userId = $this->getUserId();
        $label = $request->get('label');
        $target = $request->get('target');
        
        // target パラメータのチェック
        if (!is_null($target) && $target != 'all-nodes' && $target != 'failed-nodes') {
        	$error = '有効な target パラメータではありません。';
        	return $this->onError(array($error), $error, 400);
        }
        
        $result = $this->nodeService->getUserNodeList($userId, $label, $target);
        $statusCode = 200;
        $response = new JsonResponse($result, $statusCode);
        return $response;
    }


    /**
     *
     */
    public function getNodeDetailAction(ApiRequest $request, $capturedParams) {
        $nodeId = $capturedParams['id'];

        if(!$this->nodeRepository->isGrantedUser($nodeId, $this->getUserId())) {
            $error = 'Nodeにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $error . ' node_id:' . $nodeId);
        }

        $result = $this->nodeService->getNodeDetail($this->getUserId(), $nodeId);
        if(!$result) {
            $error = 'Nodeが見つかりません。';
            return $this->onDataNotFound(array($error), $error . ' node_id:' . $nodeId);
        }
        $statusCode = 200;
        $response = new JsonResponse($result, $statusCode);
        return $response;
    }

    /**
     *
     */
    public function getUserNodeDetailAction(ApiRequest $request, $capturedParams) {
    	$nodeId = $capturedParams['id'];
    	$userId = $capturedParams['user_id'];
    
    	if(!$this->nodeRepository->isGrantedUser($nodeId, $userId) || $this->getUserId() != $userId) {
    		$error = 'Nodeにアクセスする権限がありません。';
    		return $this->onDataNotFound(array($error), $error . ' node_id:' . $nodeId);
    	}
    
    	$result = array();
    	$result['info_pcs'] = $this->nodeService->getNodeLabels($userId, $nodeId);
    	$statusCode = 200;
    	$response = new JsonResponse($result, $statusCode);
    	return $response;
    }
    
    
    public function updateNodeAction(ApiRequest $request, $capturedParams) {
        $nodeId = (int)$capturedParams['id'];
        $putData = $request->getContentAsJson();
        $labels = $putData->get('labels', null);
        $sharingLevel = $putData->get('sharing_level');
        $sharedWith = $putData->get('shared_with');
        //add shima s
        // zwave_configurations , configurations 受け取り
        $zwaveConfig = $putData->get('zwave_configurations');
        $otherConfig = $putData->get('configurations');
        //add shima e
        $app = $putData->get('app');
 
        if(!$this->nodeRepository->isGrantedUser($nodeId, $this->getUserId(), array('owner_only' => true))) {
            $error = 'Nodeにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $error . ' node_id: ' . $nodeId);
        }
        
        //add shima s
        // 入力値チェック
        $errorMessages = array();
        if(!$this->validateNode($zwaveConfig, $otherConfig, $app, $errorMessages)) {
            return $this->onError(array($errorMessages), $errorMessages);
        }
        //add shima e

        //mod shima s
        // config 追加
        $returnJSON = array();
        $statusCode = $this->nodeService->updateNode($nodeId, null, $labels, $sharingLevel, $sharedWith ,
                                       $zwaveConfig, $otherConfig, $app, $this->getUserId(), $returnJSON);
        return new JsonResponse($returnJSON, $statusCode);
        //mod shima e
    }

    public function updateUserNodeAction(ApiRequest $request, $capturedParams) {
        $nodeId = (int)$capturedParams['id'];
        $userId = (int)$capturedParams['user_id'];
        $putData = $request->getContentAsJson();
        $labels = $putData->get('info_pcs', null);
        
        if(!$this->nodeRepository->isGrantedUser($nodeId, $userId, array('owner_only' => false))
        		|| $this->getUserId() != $userId) {
            $error = 'Nodeにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $error . ' node_id: ' . $nodeId);
        }

        $this->nodeService->updateNodeLabels($nodeId, $userId, $labels);
        return new Response('', 204);
    }
    
    
    public function deleteNodeAction(ApiRequest $request, $capturedParams) {
        $nodeId = $capturedParams['id'];

        if(!$this->nodeRepository->isGrantedUser($nodeId, $this->getUserId(), array('owner_only' => true))) {
            $error = 'Nodeにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $error . ' node_id: ' . $nodeId);
        }

        $this->nodeService->deleteNode($nodeId);
        return new Response('', 204);
    }

    /**
     * add shima
     * 確認 node更新時のバリデーション
     * @param ParameterBag $zwaveConfig, $otherConfig
     * @param array $errorMessages return error message
     * @return array
     */
    private function validateNode($zwaveConfig, $otherConfig, $app, &$errorMessages)
    {
        $errorMessages = array();
        $requiredZwaveParameters = array(
            'parameter_no', 'type', 'size', 'value'
        );
        $enumParameterSize = array(1, 2, 4);
        
        // zwave configurations validate
        if (is_array($zwaveConfig) && count($zwaveConfig) > 0) {
            foreach ($zwaveConfig as $record) {

                // require check(zwave)
                foreach ($requiredZwaveParameters as $keyName) {
                    if (!isset($record[$keyName])) {
                        $errorMessages[] = 'zwave_configuration.' . $keyName . 'が指定されていません。';
                    }
                }
                
                //check digit
                $parameterNo = $record['parameter_no'];
                if (!v::digit()->validate($parameterNo)) {
                    $errorMessages[] = 'zwave_configuration.parameter_noが数値ではありません。';
                }
                
                //check string
                $type = $record['type'];
                if (!($type == 'signed int' || $type == 'unsigned int')) {
                    $errorMessages[] = 'zwave_configuration.typeが signed int か unsigned int ではありません。';
                }

                //check digit
                $size = (int)$record['size'];
                if (!v::digit()->validate($size)) {
                    $errorMessages[] = 'zwave_configuration.sizeが数値ではありません。';
                } else {
	                $checkFlg = false;
	                foreach($enumParameterSize as $sample) {
	                	if ($size === $sample) {
	                		$checkFlg = true;
	                		break;
	                	}
	                }
	                if (!$checkFlg) {
	                	$errorMessages[] = 'zwave_configuration.sizeが有効な値ではありません。';
	                }
                }
                
                //check digit
                $value = $record['value'];
                if (!v::digit()->validate($value)) {
                    $errorMessages[] = 'zwave_configuration.valueが数値ではありません。';
                }
                
            }
        }

        $requiredOtherParameters = array(
            'type', 'parameter', 'value'
        );
        $enumPowerColPeriod = ['0', '1', '5', '15', '30', '60', '180', '360'];
        $enumEnergyGasWaterColPeriod = ['0', '60', '180', '360', '720', '1440'];
        $enumBodyMotionColPeriod = ['0', '30', '60', '180', '300', '600'];
        
        // other configurations validate
        if (is_array($otherConfig) && count($otherConfig) > 0) {
            foreach ($otherConfig as $record) {

                // require check(other)
                foreach ($requiredOtherParameters as $keyName) {
                    if (!isset($record[$keyName])) {
                        $errorMessages[] = 'configuration.' . $keyName . 'が指定されていません。';
                    }
                }
                
                //check string
                $type = $record['type'];
                if (!($type == 'unsigned int')) {
                    $errorMessages[] = 'configuration.typeが unsigned int ではありません。';
                }

                //check string
                $parameter = $record['parameter'];
                $availableParams = ['power_col_period', 'energy_consumption_col_period', 'gas_consumption_col_period', 'water_consumption_col_period', 'body_motion_col_period'];
                if (!in_array($parameter, $availableParams)) {
                    $errorMessages[] = 'configuration.parameterが有効なものではありません。';
                }else{
                    // check value
                    $value = $record['value'];
                    $checkFlg = false;
                    if ($parameter == 'power_col_period') {
                        $checkFlg = in_array($value, $enumPowerColPeriod);
                    } else if (in_array($parameter, ['energy_consumption_col_period', 'gas_consumption_col_period', 'water_consumption_col_period'])) {
                        $checkFlg = in_array($value, $enumEnergyGasWaterColPeriod);
                    } else if ($parameter == 'body_motion_col_period') {
                        $checkFlg = in_array($value, $enumBodyMotionColPeriod);
                    }
                    // check
                    if ($checkFlg == false){
                        $errorMessages[] = 'configuration.valueが 既定値 ではありません。';
                    }
                }
            }
        }

        $requiredAppParameters = array(
            'app_name', 'configurations'
        );
        
        // validate app field
        if (is_array($app) && count($app) > 0) {
            foreach ($app as $record) {

                // require check (app)
                foreach ($requiredAppParameters as $keyName) {
                    if (!isset($record[$keyName])) {
                        $errorMessages[] = 'app.' . $keyName . 'が指定されていません。';
                    }
                }

                $appConfigurations = $record["configurations"];
                if (is_array($appConfigurations) && count($appConfigurations) > 0) {

                    $requiredAppConfigParameters = array(
                        'param_name', 'param_value'
                    );

                    foreach ($appConfigurations as $subRecord) {
                        // require check (app.configurations)
                        foreach ($requiredAppConfigParameters as $subKeyName) {
                            if (!isset($subRecord[$subKeyName])) {
                                $errorMessages[] = 'app.configurations.' . $subKeyName . 'が指定されていません。';
                            }
                        }
                    }
                } else {
                    $errorMessages[] = 'app.configurations.param_nameが指定されていません。';
                    $errorMessages[] = 'app.configurations.param_valueが指定されていません。';
                }

            }
        }

        return count($errorMessages) == 0;
    }

    

}
