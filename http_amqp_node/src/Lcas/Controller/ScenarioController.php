<?php

/**
 * @file
 * @brief シナリオ関連のAPIのコントローラ
 */

namespace Lcas\Controller;

use Lcas\ApiRequest;
use Lcas\HttpNode\JsonResponse;
use Lcas\Repository\ScenarioRepository;
use Lcas\Repository\UserRepository;
use Lcas\Repository\DeviceRepository;
use Lcas\Service\ScenarioService;
use Respect\Validation\Validator as v;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Response;
use Lcas\Log\Logger;


/**
 * @class ScenarioController
 * @brief シナリオ関連のAPIのコントローラ
 */
class ScenarioController extends BaseController {

    /**
     * @var \Lcas\Service\ScenarioService
     */
    private $scenarioService;

    /**
     * @var ScenarioRepository
     */
    private $scenarioRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var DeviceRepository
     */
    private $deviceRepository;

    /**
     * コンストラクタ
     */
    public function __construct() {
        parent::__construct();
    }


    public function initialize() {
        $this->scenarioService = new ScenarioService();
        $this->scenarioRepository = new ScenarioRepository();
        $this->userRepository = new UserRepository();
        $this->deviceRepository = new DeviceRepository();
    }

    public function authenticate(ApiRequest $request, $actionName) {
        //ws-nodeからのシナリオ自動登録リクエストの場合には認証が不要
        if($actionName == 'registerAutoConfigScenarioAction'
        		|| $actionName == 'removeAutoConfigScenarioAction') {
        	return AUTH_RESULT_OK;
        }
        return parent::authenticate($request, $actionName);
    }


    /**
     * GET scenarios
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function listUserScenarioAction(ApiRequest $request, $capturedParams) {
        $user = $this->getUser();
        $label = $request->get('label', '');

        $result = $this->scenarioService->getUserScenarioList($user['id'], $label);
        $response = new JsonResponse(array(), 200);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_BIGINT_AS_STRING);
        $response->setData($result);
        return $response;
    }


    /**
     * GET scenarios/:id
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function getScenarioDetailAction(ApiRequest $request, $capturedParams) {
        $user = $this->getUser();
        $scenarioId = $capturedParams['id'];

        if(!$this->scenarioRepository->isGrantedUser($scenarioId, $this->getUserId())) {
            $error = 'データにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $error . ' scenario_id:' . $scenarioId);
        }

        $result = $this->scenarioService->getScenarioDetail($scenarioId);
        if(!$result) {
            $error = 'データが見つかりません。';
            return $this->onDataNotFound(array($error), $error . ' scenario_id:' . $scenarioId);
        }

        $response = new JsonResponse(array(), 200);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_BIGINT_AS_STRING);
        $response->setData($result);
        return $response;
    }


    /**
     * GET scenarios/all/events
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function listUserScenarioEventsAction(ApiRequest $request, $capturedParams) {
        $user = $this->getUser();
        $start = $request->get('start', 0);
        $end = $request->get('end', 0);
        $offset = $request->get('offset', null);
        $limit = $request->get('limit', null);

        $detailedErrors = array();
        if(!v::optional(v::digit())->validate($start)) {
            $detailedErrors[] = "無効な検索期間(開始日)が指定されました。 from:" . $start;
        }
        if(!v::optional(v::digit())->validate($end)) {
            $detailedErrors[] = "無効な検索期間(終了日)が指定されました。 to:" . $end;
        }

        if(!v::optional(v::digit())->validate($offset)) {
            $detailedErrors[] = "無効なoffsetが指定されました。 offset:" . $offset;
        }
        //limitは1以上の整数を期待する
        if(!v::optional(v::digit())->validate($limit)) {
            $detailedErrors[] = "無効なlimitが指定されました。 limit:" . $limit;
        }

        if(count($detailedErrors) > 0) {
            return $this->onError(array('入力データが無効です。'), $detailedErrors);
        }

        $result = $this->scenarioService->getUserEventList($user['id'], $start, $end, $offset, $limit);

        $response = new JsonResponse(array(), 200);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_BIGINT_AS_STRING);
        $response->setData($result);
        return $response;
    }


    /**
     * GET scenarios/:id/events
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function listScenarioEventsAction(ApiRequest $request, $capturedParams) {
        $user = $this->getUser();
        $scenarioId = $capturedParams['id'];
        $start = $request->get('start', 0);
        $end = $request->get('end', 0);
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 0);

        if(!$this->scenarioRepository->isGrantedUser($scenarioId, $this->getUserId())) {
            $error = 'データにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $error . ' scenario_id:' . $scenarioId);
        }

        $result = $this->scenarioService->getScenarioDetail($scenarioId);
        if(!$result) {
            $error = 'データが見つかりません。';
            return $this->onDataNotFound(array($error), $error . ' scenario_id:' . $scenarioId);
        }

        $detailedErrors = array();
        if(!v::optional(v::digit())->validate($start)) {
            $detailedErrors[] = "無効な検索期間(開始日)が指定されました。 from:" . $start;
        }
        if(!v::optional(v::digit())->validate($end)) {
            $detailedErrors[] = "無効な検索期間(終了日)が指定されました。 to:" . $end;
        }
        if(!v::optional(v::digit())->validate($offset)) {
            $detailedErrors[] = "無効なoffsetが指定されました。 offset:" . $offset;
        }
        if(!v::optional(v::digit())->validate($limit)) {
            $detailedErrors[] = "無効なlimitが指定されました。 limit:" . $limit;
        }

        if(count($detailedErrors) > 0) {
            return $this->onError(array('入力データが無効です。'), $detailedErrors);
        }

        $result = $this->scenarioService->getEventList($scenarioId, $start, $end, $offset, $limit);

        $response = new JsonResponse(array(), 200);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_BIGINT_AS_STRING);
        $response->setData($result);
        return $response;
    }


    /**
     * POST scenarios
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function registerScenarioAction(ApiRequest $request, $capturedParams) {
        $user = $this->getUser();
    	$postData = $request->getContentAsJson();
    	$scenario = $postData->all();
    	
        $errorMessages = array();
        if(!$this->validateScenario($postData, $errorMessages)) {
            return $this->onError(array($errorMessages), $errorMessages);
        }
        
        // 指定されたデバイスへのアクセス権限を現在のユーザーが持っているか？を確認します。
        $devices = array();
       	if (isset($scenario['conditions'])) {
        	foreach ($scenario['conditions'] as $condition) {
        		if (isset($condition['lhs']) && 0 < strlen($condition['lhs'])) {
        			array_push($devices, (int)$condition['lhs']);
        		}
        	}
       	}
       	if (isset($scenario['temperature_dev']) && !is_null($scenario['temperature_dev']) && gettype($scenario['temperature_dev']) === "integer") {
       		array_push($devices, (int)$scenario['temperature_dev']);
       	}
       	if (isset($scenario['humidity_dev']) && !is_null($scenario['humidity_dev']) && gettype($scenario['humidity_dev']) === "integer") {
			array_push($devices, (int)$scenario['humidity_dev']);
       	}
        if (isset($scenario['device_list']) && is_array($scenario['device_list'])) {
        	foreach ($scenario['device_list'] as $device) {
        		if (!is_null($device) && gettype($device) === "integer") {
        			array_push($devices, $device);
        		}
        	}
        }
        
        foreach ($devices as $device) {
        	if (!$this->deviceRepository->isGrantedUser($device, $user['id'])) {
        		$error = 'Deviceにアクセスする権限がありません。';
        		return $this->onDataNotFound(array($error), $error . ' device_id: ' . $device);
        	}
        }
        
        $result = $this->scenarioService->registerScenario($user, $scenario);
        $response = new JsonResponse(array(), 200);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_BIGINT_AS_STRING);
        $response->setData($result);
        return $response;
    }


    /**
     * PUT scenarios/:id
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function modifyScenarioAction(ApiRequest $request, $capturedParams) {
        $user = $this->getUser();
        $scenarioId = $capturedParams['id'];
        $postData = $request->getContentAsJson();
        $scenario = $postData->all();
        
        if(!$this->scenarioRepository->isGrantedUser($scenarioId, $this->getUserId())) {
            $error = 'データにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $error . ' scenario_id:' . $scenarioId);
        }

        $result = $this->scenarioService->getScenarioDetail($scenarioId);
        if(!$result) {
            $error = 'データが見つかりません。';
            return $this->onDataNotFound(array($error), $error . ' scenario_id:' . $scenarioId);
        }

        if ($this->scenarioRepository->isAutoConfiguration($scenarioId)) {
        	$error = '自動で登録されたシナリオは編集できません。';
        	return $this->onDataNotFound(array($error), $error . ' scenario_id:' . $scenarioId);
        }
        
        $errorMessages = array();
        if(!$this->validateScenario($postData, $errorMessages)) {
            return $this->onError(array($errorMessages), $errorMessages);
        }

        // 指定されたデバイスへのアクセス権限を現在のユーザーが持っているか？を確認します。
        $devices = array();
       	if (isset($scenario['conditions'])) {
        	foreach ($scenario['conditions'] as $condition) {
        		if (isset($condition['lhs']) && 0 < strlen($condition['lhs'])) {
        			array_push($devices, (int)$condition['lhs']);
        		}
        	}
       	}
        if (isset($scenario['temperature_dev']) && !is_null($scenario['temperature_dev']) && gettype($scenario['temperature_dev']) === "integer") {
       		array_push($devices, (int)$scenario['temperature_dev']);
       	}
       	if (isset($scenario['humidity_dev']) && !is_null($scenario['humidity_dev']) && gettype($scenario['humidity_dev']) === "integer") {
			array_push($devices, (int)$scenario['humidity_dev']);
       	}
        if (isset($scenario['device_list']) && is_array($scenario['device_list'])) {
        	foreach ($scenario['device_list'] as $device) {
        		if (!is_null($device) && gettype($device) === "integer") {
        			array_push($devices, $device);
        		}
        	}
        }
       	
        foreach ($devices as $device) {
        	if (!$this->deviceRepository->isGrantedUser($device, $user['id'])) {
        		$error = 'Deviceにアクセスする権限がありません。';
        		return $this->onDataNotFound(array($error), $error . ' device_id: ' . $device);
        	}
        }
        
        $this->scenarioService->modifyScenario($user, $scenarioId, $scenario);
        return new Response('', 204);

    }

    /**
     * DELETE scenarios/:id
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function removeScenarioAction(ApiRequest $request, $capturedParams) {
        $user = $this->getUser();
        $scenarioId = $capturedParams['id'];

        if(!$this->scenarioRepository->isGrantedUser($scenarioId, $this->getUserId())) {
            $error = 'データにアクセスする権限がありません。';
            return $this->onDataNotFound(array($error), $error . ' scenario_id:' . $scenarioId);
        }

        $scenario = $this->scenarioService->getScenarioDetail($scenarioId);
        if(!$scenario) {
            $error = 'データが見つかりません。';
            return $this->onDataNotFound(array($error), $error . ' scenario_id:' . $scenarioId);
        }
        
        if ($this->scenarioRepository->isAutoConfiguration($scenarioId)) {
        	$error = '自動で登録されたシナリオは削除できません。';
        	return $this->onDataNotFound(array($error), $error . ' scenario_id:' . $scenarioId);
        }

        $this->scenarioService->removeScenario($scenarioId);
        return new Response('', 204);
    }

    /**
     * シナリオデータの登録／更新時の共通のバリデーション
     * @param ParameterBag $postData
     * @param array $errorMessages
     * @return array
     */
    private function validateScenario($postData, &$errorMessages)
    {
        $errorMessages = array();
        $requiredParameters = array(
            'scenario_type', 'labels', 'actions'
        );
        $requiredParametersSimple = array(
            'condition_operator', 'conditions', 'trigger_after'
        );
        $requiredParametersWBGT = array(
            'temperature_dev', 'humidity_dev', 'wbgt_threshold'
        );
        $requiredParametersSCXML = array(
        	'scxml', 'device_list', 'action_state_list'
        );
        
        foreach ($requiredParameters as $keyName) {
            if (!$postData->has($keyName)) {
                $errorMessages[] = $keyName . 'が指定されていません。';
            }
        }

        $scenarioType = $postData->get('scenario_type');
        switch ($scenarioType) {
        case 'simple':
            foreach ($requiredParametersSimple as $keyName) {
                if (!$postData->has($keyName)) {
                    $errorMessages[] = $keyName . 'が指定されていません。';
                }
            }
            $conditions = $postData->get('conditions');
	        if (!is_array($conditions) || count($conditions) == 0) {
	            $errorMessages[] = 'conditionsが0件です。';
	        }
	
	        $triggerAfter = $postData->get('trigger_after');
	        if (!v::digit()->validate($triggerAfter)) {
	            $errorMessages[] = 'trigger_afterが数値ではありません。';
	        }
            break;
        case 'wbgt':
            foreach ($requiredParametersWBGT as $keyName) {
                if (!$postData->has($keyName)) {
                    $errorMessages[] = $keyName . 'が指定されていません。';
                }
            }
            break;
        case 'scxml':
            foreach ($requiredParametersSCXML as $keyName) {
                if (!$postData->has($keyName)) {
                    $errorMessages[] = $keyName . 'が指定されていません。';
                }
            }
            break;
        }
        
        if ($postData->has('auto_config')) {
	        $autoConfig = $postData->get('auto_config');
	        if (!is_bool($autoConfig)) {
	        	$errorMessages[] = 'auto_configが有効な値ではありません。 auto_config: ' . $autoConfig;
	        }
        }

        $actions = $postData->get('actions');
        if (!is_array($actions) || count($actions) == 0) {
            $errorMessages[] = 'actionsが0件です。';
            return $errorMessages;
        }
        return count($errorMessages) == 0;
    }

    public function registerAutoConfigScenarioAction(ApiRequest $request, $capturedParams) {
        	
    	$postData = $request->getContentAsJson();
    	$scenario = $postData->all();
    	
    	if (!isset($scenario['user_id'])) {
    		$message = 'user_idが指定されていません。';
    		return $this->onError($message, $message);
    	}
    	
    	$userId = (int)$scenario['user_id'];
    	$user = $this->userRepository->findById($userId);
    	if (!$user) {
            $error = 'ユーザーが見つかりません。';
            return $this->onDataNotFound(array($error), $error . ' user_id:' . $userId);
    	}
    	
        $errorMessages = array();
        if(!$this->validateScenario($postData, $errorMessages)) {
            return $this->onError(array($errorMessages), $errorMessages);
        }
        
        $scenario['auto_config'] = true;

        $result = $this->scenarioService->registerScenario($user, $scenario);
        $response = new JsonResponse(array(), 200);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_BIGINT_AS_STRING);
        $response->setData($result);
        return $response;
    }

    /**
     *
     */
    public function removeAutoConfigScenarioAction(ApiRequest $request, $capturedParams) {

    	$scenarioId = $capturedParams['id'];
    	$this->scenarioService->removeScenario($scenarioId);
    	return new Response('', 204);
    }

     /**
     *
     */
    public function hideScenarioTileAction(ApiRequest $request, $capturedParams) {

        $scenarioId = $capturedParams['id'];
        $value = $capturedParams['value'];
        $this->scenarioService->hideTileScenario($scenarioId, $value);
        return new Response('', 204);
    }

}
