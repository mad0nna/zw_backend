<?php

/**
 * @file
 * @brief ユーザー関連のAPIのコントローラ
 */

namespace Lcas\Controller;

use Lcas\ApiRequest;
use Lcas\Exception\BadParameterException;
use Lcas\Repository\NodeRepository;
use Lcas\Repository\UserRepository;
use Lcas\Repository\UserTokenRepository;
use Lcas\Service\UserService;
use Lcas\HttpNode\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Lcas\DB\DB;
use Lcas\Repository\GatewayRepository;
use Lcas\Service\ScenarioService;
use Lcas\Repository\ScenarioRepository;

/**
 * @class UserController
 * @brief ユーザー関連のAPIのコントローラ
 */
class UserController extends BaseController {

    /**
     * @var \Lcas\Service\UserService
     */
    private $userService;

    /**
     * @var UserRepository
     */
    private $userRepository;
    
    /**
     * @var GatewayRepository
     */
    private $gatewayRepository;

    /**
     * @var NodeRepository
     */
    private $nodeRepository;

    /**
     * @var ScenarioRepository
     */
    private $scenarioRepository;

    /**
     * @var ScenarioService
     */
    private $scenarioService;

    /**
     * @var UserConfirmTokenRepository
     */
    private $userTokenRepository;
    
    /**
     * コンストラクタ
     */
    public function __construct() {
        parent::__construct();
    }


    public function initialize() {
        $this->userService = new UserService();
        $this->userRepository = new UserRepository();
        $this->gatewayRepository = new GatewayRepository();
        $this->nodeRepository = new NodeRepository();
        $this->scenarioRepository = new ScenarioRepository();
        $this->scenarioService = new ScenarioService();
    }

    public function authenticate(ApiRequest $request, $actionName) {
        //registerUserActionは認証が不要なため、無条件でAUTH_RESULT_OKを返す。
        if($actionName == 'registerUserAction'
        		|| $actionName == 'confirmUserAction'
        		|| $actionName == 'resetUserAction'
        		) {
            return AUTH_RESULT_OK;
        } else if ($actionName === 'updateUserAction') {
        	// パスワードリセットへの対応の際は認証が不要
        	$postData = $request->getContentAsJson();
        	if (!is_null($postData->get('reset_token'))) {
        		return AUTH_RESULT_OK;
        	}
        }
        return parent::authenticate($request, $actionName);
    }


    /**
     * GET users
     *
     * 現在未使用です。
     *
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function getUserDetailAction(ApiRequest $request, $capturedParams) {
        $targetUserLoginId = $capturedParams['id'];

        $targetUser = $this->userRepository->findUserByLoginId($targetUserLoginId);
        if(!$targetUser) {
            $error = 'ユーザーが見つかりません。';
            return $this->onDataNotFound(array($error), $error . ' user_login_id:' . $targetUserLoginId);
        }

        $nodes = $this->nodeRepository->findByUserId($targetUser['id']);
        $hasNodeAssociated = (is_array($nodes) && count($nodes) > 0);

        $granted = $hasNodeAssociated || $targetUser['id'] == $this->getUserId();
        if(!$granted) {
            $error = '無効なユーザーIDが指定されました。';
            return $this->onDataNotFound(array($error), $error . ' user_login_id:' . $targetUserLoginId);
        }

        $result = array(
            'email' => $targetUser['email']
        );

        $response = new JsonResponse($result, 200);
        return $response;
    }


    /**
     * POST users
     *
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function registerUserAction(ApiRequest $request, $capturedParams) {
        $postData = $request->getContentAsJson();
        $email = $postData->get('email');
        $platform = $postData->get('platform');
        $token = $postData->get('token');
        
        $errorMessages = array();
        if(is_null($email) || strlen($email) == 0) {
            $errorMessages[] = 'emailが指定されていません。';
        }
        if(!is_null($platform) && 0 < strlen($platform)) {
            if(($platform !== 'none' && $platform !== 'sqs') && (is_null($token) || strlen($token) == 0)) {
                $errorMessages[] = 'tokenが指定されていません。';
            } else if ($platform === 'none' || $platform === 'sqs') {
            	$token = '';
            }
        } else {
        	$platform = 'none';
            $token = '';
        }
        $platform = UserRepository::getPlatformTypeFromName($platform);
        
        if(count($errorMessages) > 0) {
            return $this->onError($errorMessages, $errorMessages);
        }

        $result = $this->userService->sendRegistUserTokenMail($email, $platform, $token, $request->headers->get('Origin'));
        $response = new JsonResponse('', 204);
        return $response;
    }

    /**
     * GET users
     *
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function getUserAction(ApiRequest $request, $capturedParams) {
        $userId = (int)$this->getUserId();
        
        // 現在のユーザー情報を取得
        $user = $this->userRepository->findById($userId);
        if(!$user) {
            $error = '無効なユーザーIDが指定されました。';
            return $this->onDataNotFound(array($error), $error . ' id:' . $userId);
        }
        
        // ラベルを取得
        $labels = $this->userRepository->findLabels($userId);
        $labelArray = array();
        foreach ($labels as $label) {
        	array_push($labelArray, $label['label']);
        }

        $status = $this->userRepository->findDashboardStatus($userId);
        $dashboardStatusArray = array();
        foreach($status as $row) {
            array_push($dashboardStatusArray, $row['label']);
        }

        $result = array(
        		'id' => (int)$userId,
        		'email' => $user['email'],
        		'platform' => $this->userRepository->getPlatformNameFromType($user['platform']),
        		'token' => $user['token'],
                'labels' => $labelArray,
                'dashboard_status' => $dashboardStatusArray,
        );

        $response = new JsonResponse($result, 200);
        return $response;
    }
    
    /**
     * PUT users/self
     *
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function updateUserAction(ApiRequest $request, $capturedParams) {
        $userId = $this->getUserId();
        $postData = $request->getContentAsJson();
        $email = $postData->get('email');
        $password = $postData->get('password');
        $old_password = $postData->get('old_password');
        $platform = $postData->get('platform');
        $token = $postData->get('token');
        $reset_token = $postData->get('reset_token');
        $labels = $postData->get('labels', null);
        
        if (!is_null($reset_token) && 0 < strlen($reset_token)) {
        	
        	// パスワードリセットの処理
  		  	$this->userService->resetUser($password, $reset_token);
	 	   return new Response('', 204);
        	
        } else {
	        // 現在のユーザー情報を取得
	        $user = $this->userRepository->findById($userId);
	        if(!$user) {
	        	$error = '無効なユーザーIDが指定されました。';
	        	return $this->onDataNotFound(array($error), $error . ' id:' . $userId);
	        }
	        
	        if (strlen($email) > 0 || strlen($password) > 0 || strlen($platform) > 0) {
		        // old_password の確認
		        if(strlen($old_password) == 0) {
		        	$error = 'パスワードが指定されていません。';
		        	return $this->onError(array($error), $error, 400);
		        }
		        if (!$this->userRepository->findUserByLoginInfo($user['email'], $old_password)) {
		        	$error = 'パスワードが間違えています。';
		        	return $this->onError(array($error), $error, 401);
		        }
	        } else {
	        	if (strlen($old_password) > 0) {
		        	$error = 'email, password, platform の指定がない場合に old_password が指定されていてはいけません。';
	        		return $this->onError(array($error), $error);
	        	}
	        }
	
	        // そのほかのパラメータの妥当性の確認
	        $errorMessages = array();
	        if(strlen($platform) > 0) {
	            $platformType = $this->userRepository->getPlatformTypeFromName($platform);
	            if($platformType === false) {
	                $errorMessages[] = '無効なplatformです。: ' . $platform;
	            } else {
	                if(($platformType !== PLATFORM_NONE && $platformType !== PLATFORM_SQS) && is_null($token)) {
	                    $errorMessages[] = 'tokenが指定されていません。';
	                }
	            }
	        }
	
	        if(count($errorMessages) > 0) {
	            return $this->onError($errorMessages, $errorMessages);
	        }
	        
	        $response = new Response('', 204);
	        if (strlen($email) > 0 && strcmp($email, $user['email']) !== 0) {
		        // メールアドレス変更時の対応
		        
	        	if (is_null($platform) || strlen($platform) == 0) {
	        		$platform = (int)$user['platform'];
	        	}
	        	if (is_null($token) || strlen($token) == 0) {
	        		$token = (string)$user['token'];
	        	}
	        	
	        	try {
	        		// 確認メールを送信
	    	    	$this->userService->sendUpdateUserTokenMail($userId, $email, $platform, $token, $request->headers->get('Origin'));
	        	} catch(Exception $e) {
	        		throw $e;
	        	}
	        }
	        else {
	        	$db = DB::getMasterDB();
	        	$db->Begin();
		        try {
		            $this->userService->updateUser($userId, $email, $password, $platform, $token, $labels);
		        } catch(BadParameterException $e) {
		            //エラー内容に応じてレスポンス内容を振り分ける必要が発生した場合、
		            //ここで例外を処理する。
		            throw $e;
		        }
		        $db->Commit();
	        }
	        return $response;
        }
    }

    /**
     * POST users/confirmation
     * 
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function confirmUserAction(ApiRequest $request, $capturedParams) {
        $postData = $request->getContentAsJson();
    	$confirmation_token = $postData->get('confirmation_token');
    	$password = $postData->get('password');
    	$platform = $postData->get('platform');
    	$token = $postData->get('token');
    	$labels = $postData->get('labels', null);
    	
        if(is_null($confirmation_token) || strlen($confirmation_token) == 0) {
            $error = 'confirmation_token が指定されていません。';
            return $this->onError(array($error), $error);
        }

        if(is_null($password) || strlen($password) == 0) {
            $error = 'password が指定されていません。';
            return $this->onError(array($error), $error);
        }

        if(!is_null($platform) && strlen($platform) > 0) {
        	if(($platform == 'apns' || $platform == 'gcm') && (is_null($token) || strlen($token) == 0)) {
	            $error = 'token が指定されていません。';
	            return $this->onError(array($error), $error);
        	}
        }

        $response = new Response('', 204);
        try {
    		$this->userService->confirmUser($confirmation_token, $password, $platform, $token, $labels);
        } catch (Exception $e) {
        	throw $e;
        }
        return $response;
    }
    
    /**
     * POST users/resettoken
     * 
     * @param ApiRequest $request HTTPリクエスト
     * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
     * @return JsonResponse
     */
    public function resetUserAction(ApiRequest $request, $capturedParams) {
        $postData = $request->getContentAsJson();
        $email = $postData->get('email');
        
        // 現在のユーザー情報を取得
        $user = $this->userRepository->findUserByEmail($email);
        if(!$user) {
        	$error = '登録されていないメールアドレスが指定されました。';
        	return $this->onDataNotFound(array($error), $error . ' email:' . $email);
        }
        $userId = $user['id'];
        
        $response = new Response('', 204);
      	// 確認メールを送信
    	$this->userService->sendResetPasswordTokenMail($userId, $email, (int)$user['platform'], (string)$user['token'], $request->headers->get('Origin'));
        return $response;
    }
	
	/**
	 * DELETE users/self
	 *
	 * @param ApiRequest $request HTTPリクエスト
	 * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
	 * @return JsonResponse
	 */
	public function removeUserAction(ApiRequest $request, $capturedParams) {
		$postData = $request->getContentAsJson();
		$password = $postData->get('password');
		
		$user = $this->getUser();
		
		$response = new Response('', 204);
		
		// password のチェック
		if(strlen($password) == 0) {
			$error = 'パスワードが指定されていません。';
			return $this->onError(array($error), $error, 400);
	    }
		// パスワードが違う場合、403 を返す
        if (!$this->userRepository->findUserByLoginInfo($user['email'], $password)) {
        	$error = 'パスワードが間違えています。';
        	return $this->onError(array($error), $error, 403);
        }
    	
    	// gateway のチェック
        $gateways = $this->gatewayRepository->findByUserId($user['id']);
    	if (0 < count($gateways)) {
	    	// gateway の所有者の場合、400 を返す
        	$error = 'ゲートウェイの所有者は削除できません。';
        	return $this->onError(array($error), $error, 400);
    	}
    	
    	// ユーザーの削除をおこなう
    	$this->userService->removeUser($user);
    	
    	return $response;
    }

    /**
     * GET users/dashboard/status
     * 
     * @param ApiRequest $request HTTPリクエスト
	 * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
	 * @return JsonResponse
     */
    public function listUserDashboardStatusAction() {
        $status = 200;
        $result = [];

        try {
            $result['data'] = $this->userRepository->getDashboardStatus($this->getUserId());
        } catch(\Exception $e) {
            $result['error'] = $e->getMessage();
            $status = 500;
        }

        return new JsonResponse($result, $status);
    }

    /**
     * PUT users/dashboard/status
     * 
     * @param ApiRequest $request HTTPリクエスト
	 * @param array $capturedParams URL中の正規表現パターンからキャプチャした値を含んだ連想配列
	 * @return JsonResponse
     */
    public function updateUserDashboardStatusAction(ApiRequest $request, $capturedParams) {
        $postData = $request->getContentAsJson();

        $status = 200;
        $result = [];

        try {
            $labels = $postData->get('labels');

            $result['data'] = $this->userRepository->updateDashboardStatus($this->getUserId(), $labels);
        } catch(\Exception $e) {
            $result['error'] = $e->getMessage();
            $status = 500;
        }

        return new JsonResponse($result, $status);
    }
}
