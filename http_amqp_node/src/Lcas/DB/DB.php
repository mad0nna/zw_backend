<?php

/**
 * @file
 * @brief DBへの問い合わせを行うクラス
 */

namespace Lcas\DB;

/**
 * @class DB
 * @brief DBへの問い合わせを行うクラス
 */
class DB {
    /**
     * DBコネクションリソース
     * @var mixed
     */
    protected $Con;

    /**
     * DBホスト名
     * @var string
     */
    protected $Hos;

    /**
     * ポート番号
     * @var integer
     */
    protected $Port;

    /**
     * DBユーザー名
     * @var string
     */
    protected $Uid;

    /**
     * DBパスワード
     * @var string
     */
    protected $Pwd;

    /**
     * DBデータベース名
     * @var string
     */
    protected $Dbn;
    /**
     * 文字コード
     * @var string
     */
    protected $Jcd;

    /**
     * SQL文
     * @var string
     */
    protected $Sql;

    /**
     * 結果リソース
     * @var resource
     */
    protected $Row;

    /**
     * エラーメッセージ
     * @var string
     */
    protected $Err;

    /**
     * トランザクション開始フラグ
     * @var boolean
     */
    protected $Trn;

    /**
     * マスター側DBへの接続を保持したインスタンス
     * @var \Lcas\DB\Mysql
     */
    protected static $masterConnection = null;

    /**
     * マスターDBへのコネクションを返す
     * @param boolean $forceNew コネクションを新規に作成するかどうか
     * @return \Lcas\DB\Mysql
     */
    public static function getMasterDb($forceNew=false) {
        if($forceNew) {
            return new Mysql(DB_HOST, DB_PORT, DB_USER, DB_PASS, DB_NAME);
        }
        if(!is_null(self::$masterConnection)) {
            return self::$masterConnection;
        }
        self::$masterConnection = new Mysql(DB_HOST, DB_PORT, DB_USER, DB_PASS, DB_NAME);
        return self::$masterConnection;
    }

    /**
     * コンストラクター
     *
     * @since 1.0.0
     */
    function __construct() {
        $this->Con = "";
        $this->Hos = "";
        $this->Uid = "";
        $this->Pwd = "";
        $this->Dbn = "";
        $this->Jcd = "";
        $this->Sql = "";
        $this->Row = "";
        $this->Err = "";
        $this->Trn = 0;
    }

    /**
     * SQLを設定する
     * @param string $sql SQL
     * @since 1.0.0
     */
    function setSql($sql) {
        $this->Sql = $sql;
    }

    /**
     * 現在設定しているSQLを返す
     * @return string SQL
     * @since 1.0.0
     */
    function getSql() {
        return $this->Sql;
    }

    /**
     * エラーメッセージを返す。
     * @return string エラーメッセージ
     * @since 1.0.0
     */
    function getErr() {
        return $this->Err;
    }
}
