<?php

/**
 * @file
 * @brief DBへの問い合わせを行うクラス(MySQL版実装)
 */

namespace Lcas\DB;
use Lcas\Exception\DBException;
use Symfony\Bridge\Monolog\Logger;


/**
 * @class Mysql
 * @brief DBへの問い合わせを行うクラス(MySQL版実装)
 */
class Mysql extends DB {

    /**
     * mysqliのインスタンス
     * @var \mysqli
     */
    private $mysql;

    /**
     * 接続を確立したかどうか
     * @var bool
     */
    private $connected;

    /**
     * コンストラクター
     *
     * @since 1.0.0
     */
    function __construct($hos,$port,$uid,$pwd,$dbn) {
        parent::__construct();
        $this->Hos = $hos;
        $this->Port = $port;
        $this->Uid = $uid;
        $this->Pwd = $pwd;
        $this->Dbn = $dbn;
        $this->Jcd = 'utf8';
        $this->connected = false;
        $this->mysql = new \mysqli();
    }

    /**
     * 接続を実行する
     * @param bool $force
     * @return bool
     */
    function Connect($force=false) {
        if(!$force && $this->connected) {
            return true;
        }

        $connectResult = $this->mysql->real_connect($this->Hos,$this->Uid,$this->Pwd,$this->Dbn, $this->Port);
        if (!$connectResult) {
            throw new DBException('DB接続に失敗しました。errno=' . mysqli_connect_errno() . ', error=' . mysqli_connect_error());
        }
        $this->connected = true;
        $this->mysql->query("SET CHARACTER SET {$this->Jcd}");
        if ($this->mysql->error) {
            $this->Err = $this->Error();
            throw new DBException('DB接続に失敗しました。');
        }
        return true;
    }


    /**
     * クエリを実行する
     * @param int $val 未使用
     * @return mixed クエリが成功した場合、mysqli_resultのインスタンス、失敗した場合、false
     */
    function Query($val=0) {

        $logger = \Lcas\Log\Logger::get();
        if(defined('DEBUG_LOG_DB_QUERIES') && DEBUG_LOG_DB_QUERIES) {
            $logger->addDebug('QUERY: ' . $this->Sql);
        }

        if(!$this->connected) {
            if(!$this->Connect()) {
                return false;
            }
        }

        $this->Row = $this->mysql->query($this->Sql);

        $tmpThisSql = $this->Sql;
        $tmpThisRow = $this->Row;

        if ($this->mysql->error) {
            $this->Err = $this->Error();
            $logger->addDebug('QUERY ERROR: ' . $this->Err);
            return FALSE;
        } else {
            $this->Sql = $tmpThisSql;
            $this->Row = $tmpThisRow;
            return $this->Row;
        }
    }


    /**
     * 結果セット内のカーソルを移動する
     * @param integer $i ポジション
     * @return mixed
     */
    function Data_seek($i) {
        return $this->Row->data_seek($i);
    }

    /**
     * 結果セットから1行取得し、連想配列で返す
     * @return mixed 結果セットの行データを含んだ連装配列。最終行を取得済の場合、false
     */
    function Fetch() {
        return $this->Row->fetch_assoc();
    }

    /**
     * 結果セットから1行取得し、数字添字で返す。
     * @return mixed 結果セットの行データを含んだ配列。最終行を取得済の場合、false
     */
    function Fetch_row() {
        return $this->Row->fetch_row();
    }

    /**
     * mysqliから取得したmysqli_resultを直接返す
     * @return mysqli_result
     */
    function Get_result_set() {
        return $this->Row;
    }

    /**
     * 接続を閉じる
     * @return void
     */
    function Close() {

        if(!$this->connected) {
            return;
        }

        return $this->mysql->close();
    }

    /**
     * エラーメッセージを返す
     * @return string
     */
    function Error() {
        return $this->mysql->error;
    }

    /**
     * 結果セットを開放する
     * @return string
     */
    function Free_result() {
        return $this->Row->free_result();
    }

    /**
     * 結果セットの行数を返す
     * @return integer
     */
    function Num_rows() {
        return $this->Row->num_rows;
    }

    /**
     * UPDATE/DELETEオペレーションの影響した行数を返す。
     * @return integer
     */
    function Affected_rows() {
        return $this->mysql->affected_rows;
    }

    /**
     * INSERTによって挿入された行のauto_increment値を返す
     * @return integer
     */
    function Insert_id() {
        return $this->mysql->insert_id;
    }

    /**
     * 渡された値をエスケープする
     *
     * real_escape_stringを使用するため、呼び出し時に
     * DB接続が確立している必要があります。
     *
     * @param string $val エスケープ対象の値
     * @return string エスケープ後の文字列
     */
    function Escape($val) {
        if(!$this->connected) {
            $this->Connect();
        }
        return $this->mysql->real_escape_string($val);
    }


    /**
     * 渡された値をLIKE句用にエスケープする
     * @param string $str エスケープ対象
     * @return string エスケープした文字列
     */
    function LikeEscape($str) {
        $str = $this->Escape(
            str_replace(
                array('\\',   '%',  '_'),
                array('\\\\', '\%', '\_'),
                $str
            )
        );
        return $str;
    }

    /**
     * 直前のクエリで発生したWarningの件数を返す
     * @return int warningの発生件数
     */
    function getWarningCount() {
        return $this->mysql->warning_count;
    }

    /**
     * 直前のクエリで発生したwarningのメッセージを返す
     * @return string warningメッセージ
     */
    function getWarningMessage() {
        $this->setSql("SHOW WARNINGS");
        $this->Row = $this->mysql->query($this->Sql);

        $rows = $this->Row->fetch_row();
        return sprintf("%s (%d): %s\n", $rows[0], $rows[1], $rows[2]);
    }

    /**
     * トランザクションを開始する。
     * @param int $val 未使用
     */
    function Begin($val=0) {
        $this->setSql("START TRANSACTION");
        if (!$this->Query()) {
            throw new DBException('Start transaction failed. detail: ' . $this->getErr());
        }
        $this->Trn = 1;
    }

    /**
     * トランザクションをコミットする
     * @param int $val 未使用
     */
    function Commit($val=0) {
        $this->setSql("COMMIT");
        if (!$this->Query()) {
            throw new DBException('Commit failed. detail: ' . $this->getErr());
        }
        $this->Trn = 0;
    }

    /**
     * トランザクションをロールバックする
     */
    function Rollback() {
        $this->setSql("ROLLBACK");
        if (!$this->Query()) {
            throw new DBException('Rollback failed. detail: ' . $this->getErr());
        }
        $this->Trn = 0;
    }

    /**
     * 次のトランザクションでのみトランザクション分離レベルを指定する。
     * @param string $level トランザクション分離レベル。
     */
    function SetTransactionIsolationLevel($level) {
        //SET [SESSION|GLOBAL] のSESSION,GLOBALのどちらも指定しない場合、
        //次に実行されるトランザクションのみのトランザクション分離レベルを指定する。
        //https://dev.mysql.com/doc/refman/5.6/ja/set-transaction.html#set-transaction-scope
        $sql = "SET TRANSACTION ISOLATION LEVEL {$level}";
        $this->setSql($sql);
        if (!$this->Query()) {
            throw new DBException('Set transaction isolation level failed. SQL:' . $sql);
        }
        $this->Trn = 0;
    }

    /**
     * セッション内のトランザクションのトランザクション分離レベルを指定する。
     * @param string $level トランザクション分離レベル。
     */
    function SetSessionTransactionIsolationLevel($level) {
        $sql = "SET SESSION TRANSACTION ISOLATION LEVEL {$level}";
        $this->setSql($sql);
        if (!$this->Query()) {
            throw new DBException('Set transaction isolation level failed. SQL:' . $sql);
        }
        $this->Trn = 0;
    }
    
    function Quot($value) {
    	return sprintf('\'%s\'', $value);
    }

}

