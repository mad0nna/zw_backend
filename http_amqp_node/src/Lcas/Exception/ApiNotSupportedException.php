<?php

namespace Lcas\Exception;


/**
 * 未対応を表す例外
 * @package Lcas\Exception
 */
class ApiNotSupportedException extends ZworksApiException {

}
