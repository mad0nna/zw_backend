<?php

/**
 * @file
 * @brief z-works.ioとのAPIのタイムアウト時に発生する例外
 */


namespace Lcas\Exception;

/**
 * @class ApiTimeoutException
 * @brief z-works.ioとのAPIのタイムアウト時に発生する例外
 */
class ApiTimeoutException extends IOException {

}
