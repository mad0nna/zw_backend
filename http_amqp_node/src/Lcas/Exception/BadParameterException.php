<?php

/**
 * @file
 * @brief ユーザー起因のパラメータ異常を表す例外
 */

namespace Lcas\Exception;


/**
 * @class BadParameterException
 * @brief ユーザー起因のパラメータ異常を表す例外
 */
class BadParameterException extends LcasException {

}
