<?php

/**
 * @file
 * @brief DBクエリ、接続についての例外
 */

namespace Lcas\Exception;

/**
 * @class DBException
 * @brief DBクエリ、接続についての例外
 */
class DBException extends IOException {

}
