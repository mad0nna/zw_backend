<?php

/**
 * @file
 * @brief DB, HTTP, ファイル、AMQPなど各種入出力の失敗を表す例外
 */

namespace Lcas\Exception;

/**
 * @class IOException
 * @brief DB, HTTP, ファイル、AMQPなど各種入出力の失敗を表す例外
 */
class IOException extends LcasException {

}
