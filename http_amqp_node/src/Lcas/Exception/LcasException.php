<?php

/**
 * @file
 * @brief LCASの例外の基底クラス
 */

namespace Lcas\Exception;

/**
 * @class LcasException
 * @brief LCASの例外の基底クラス
 */
class LcasException extends \RuntimeException {

    /**
     * エラーメッセージを配列形式で返す。
     * @return array エラーメッセージを含んだ配列
     */
    public function getMessages() {
        $content = $this->getMessage();
        if(!is_array($content)) {
            return array($content);
        }
        return $content;
    }

}
