<?php

namespace Lcas\Exception;


/**
 * メール送信の失敗を表す例外
 * @package Lcas\Exception
 */
class MailException extends LcasException {

}
