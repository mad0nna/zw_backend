<?php

/**
 * @file
 * @brief ルーティング対象が存在しない場合に発生する例外
 */

namespace Lcas\Exception;


/**
 * @class RouteNotFoundException
 * @brief ルーティング対象が存在しない場合に発生する例外
 */
class RouteNotFoundException extends LcasException{



}
