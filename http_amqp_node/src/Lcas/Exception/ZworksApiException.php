<?php

/**
 * @file
 * @brief z-works.ioとの間のAPI通信で発生したエラーについての例外
 *
 * - APIがエラーを返した
 * - APIからのレスポンスに予期しない値が含まれていた
 * - APIがタイムアウトした(この場合はApiTimeoutExceptionを使用)
 */

namespace Lcas\Exception;

/**
 * @class ZworksApiException
 * @brief z-works.ioとの間のAPI通信で発生したエラーについての例外
 *
 * - APIがエラーを返した
 * - APIからのレスポンスに予期しない値が含まれていた
 * - APIがタイムアウトした(この場合はApiTimeoutExceptionを使用)
 */
class ZworksApiException extends IOException {

}
