<?php

/**
 * @file
 * @brief 日付を扱うユーティリティ関数の定義
 */

namespace Lcas\Helper;


/**
 * @class DateHelper
 * @brief 日付を扱うユーティリティ関数の定義
 */
class DateHelper {

    /**
     * マイクロ秒を含めた現在日時の文字列を取得する
     * @return string 'Y-m-d H:i:s.u' 形式の現在日時の文字列
     */
    public static function getDateStringWithMicrotime() {
        $now = microtime(true);
        $splitedTime = explode('.', $now);
        if(!isset($splitedTime[1])) {
            $splitedTime[1] = 0;
        }

        $dateTimePart = (int)$splitedTime[0];
        $microSecondPart = $splitedTime[1];
        return date('Y-m-d H:i:s', (int)$dateTimePart) . '.' .  $microSecondPart;
    }
    
    /**
     * 指定されたフォーマットに従い現在日時の文字列を取得する
     * @param string $format 日時フォーマット[Optional] 未指定の場合には 'Y-m-s H:i:s'
     * @param int $microtime Unix時刻[Optional] 未指定の場合には 現在時刻
     * @return string 現在日時の文字列
     */
    public static function getDateTimeString($format = 'Y-m-d H:i:s', $microtime = null) {
    	$dt = new \DateTime();
    	if (!is_null($microtime)) {
    		$dt->setTimestamp($microtime);
    	}
    	return $dt->format($format);
    }

    /**
     * 渡された値が有効な日付文字列かを確認する
     * @param string $dateTimeString 日付文字列(フォーマットはYYYY-mm-dd hh:ii:ss or YYYY-mm-dd hh:ii:ss.uuuuuu)
     * @return boolean
     */
    public static function isValidDateTimeString($dateTimeString) {
    	$matched = array();
        if(!preg_match('/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})([.]([0-9]+))*$/', $dateTimeString, $matched)) {
        	return false;
        }
        $year = $matched[1];
        $month = $matched[2];
        $day = $matched[3];
        $hour = $matched[4];
        $minute = $matched[5];
        $second = $matched[6];
        if (isset($matched[8])) {
	        $microsecond = $matched[8];
        } else {
        	$microsecond = 0;
        }

        if(!checkdate($month, $day, $year)) {
            return false;
        }

        if($hour < 0 || $hour > 24) {
        	return false;
        }
        if($minute < 0 || $minute > 59) {
        	return false;
        }
        if($second < 0 || $second > 59) {
        	return false;
        }

        return true;
    }
    
    /**
     * 単位を指定して日時の差分を取得する
     * 計算式の概要は次のとおり
     * result = dateTimtString2 - dateTimeString1
     * result に小数は含まれません（切り捨てられます）
     * @param string $unit 次のいずれかを指定する('d':日、'h':時、'i':分、's':秒)
     * @param string $dateTimeString1 要素1 'Y-m-d h:i:s' or 'Y-m-d H:i:s.u' 形式の日時文字列
     * @param string $dateTimeString2 要素2 'Y-m-d h:i:s' or 'Y-m-d H:i:s.u' 形式の日時文字列
     * @return mixed false の場合は失敗、それ以外は差分を int で返却
     */
    public static function getDateTimeDiff($unit, $dateTimeString1, $dateTimeString2 = null) {
    	if (	strcasecmp($unit, 'd') !== 0 
    			&& strcasecmp($unit, 'h') !== 0 
    			&& strcasecmp($unit, 'i') !== 0 
    			&& strcasecmp($unit, 's') !== 0 
    		) {
    		return false;
    	}
    	$unit = strtolower($unit);
    	
    	if (!DateHelper::isValidDateTimeString($dateTimeString1)) {
    		return false;
    	}
    	if (!is_null($dateTimeString2) && !DateHelper::isValidDateTimeString($dateTimeString2)) {
    		return false;
    	}
    	
    	$dt1 = strtotime($dateTimeString1);
    	$dt2 = is_null($dateTimeString2) ? microtime(true) : strtotime($dateTimeString2);
    	 
    	$diff = $dt2 - $dt1;
    	$result = 0;
    	switch ($unit) {
    		case 'd':
    			$result = $diff / 60 / 60 / 24;
    			break;
    			
    		case 'h':
    			$result = $diff / 60 / 60;
    			break;
    			
    		case 'i':
    			$result = $diff / 60;
    			break;
    			
    		case 's':
    			$result = $diff;
    			break;
    			
    		default:
    			return false;
    	}
    	return (int)floor($result);
    }

    /**
     * 単位を指定して日時を加算し、結果を日時文字列で返却する
     * @param string $unit 次のいずれかを指定する('d':日、'h':時、'i':分、's':秒)
     * @param int $elem 加算要素
     * @param string $dateTimeString 加算元[Optional] 'Y-m-d H:i:s' or 'Y-m-d H:i:s.u' 形式の日時文字列
     * @return mixed false の場合は失敗、それ以外は日時文字列を 'Y-m-d H:i:s' 形式で返却
     */
    public static function getDateTimeAdd($unit, $elem, $dateTimeString = null) {
    	if (	strcasecmp($unit, 'd') !== 0 
    			&& strcasecmp($unit, 'h') !== 0 
    			&& strcasecmp($unit, 'i') !== 0 
    			&& strcasecmp($unit, 's') !== 0 
    		) {
    		return false;
    	}
    	$unit = strtolower($unit);
    	
    	if (!is_null($dateTimeString) && !DateHelper::isValidDateTimeString($dateTimeString)) {
    		return false;
    	}
    	
    	$dt = is_null($dateTimeString) ? microtime(true) : strtotime($dateTimeString);
    	
    	$sec = 0;
    	switch ($unit) {
    		case 'd':
    			$sec = $elem * 24 * 60 * 60;
    			break;
    			
    		case 'h':
    			$sec = $elem * 60 * 60;
    			break;
    			
    		case 'i':
    			$sec = $elem * 60;
    			break;
    			
    		case 's':
    			$sec = $elem;
    			break;
    			
    		default:
    			return false;
    	}
    	return DateHelper::getDateTimeString('Y-m-d H:i:s', $dt + $sec);
    }

}
