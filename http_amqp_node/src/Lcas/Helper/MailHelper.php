<?php
namespace Lcas\Helper;


use Lcas\Exception\MailException;

class MailHelper {
	
	/**
	 * メールを送信します。
	 * @param string $from
	 * @param string $to
	 * @param string $subject
	 * @param string $body
	 * @throws MailException
	 */
	public static function sendMail($from, $to, $subject, $body, $contentType='text/plain') {
		$subject = mb_encode_mimeheader($subject);
		$from = mb_encode_mimeheader($from);
		$headers = sprintf("From: %s\r\nMIME-Version: 1.0\r\nContent-type: %s; charset=UTF-8\r\n", $from, $contentType);
		if (!mail($to, $subject, $body, $headers)) {
			throw new MailException();
		}
	}
}