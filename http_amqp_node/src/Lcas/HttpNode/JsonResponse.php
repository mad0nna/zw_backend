<?php

/**
 * @file
 * @brief LCAS内でJSONのレスポンスを返す際に使用するオブジェクト
 */

namespace Lcas\HttpNode;


/**
 * @class JsonResponse
 * @brief LCAS内でJSONのレスポンスを返す際に使用するオブジェクト
 */
class JsonResponse extends \Symfony\Component\HttpFoundation\JsonResponse {

    /**
     * コンストラクタ
     * @param array $data データ
     * @param int $status ステータスコード
     * @param array $headers HTTPヘッダを含んだ連想配列
     */
    public function __construct($data = null, $status = 200, $headers = array())
    {
        parent::__construct($data, $status, $headers);

        $this->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_BIGINT_AS_STRING);
    }
}

