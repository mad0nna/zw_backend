<?php

/**
 * @file
 * @brief ロギングを行うオブジェクト(MonoLog)を管理する
 */

namespace Lcas\Log;

/**
 * @class Logger
 * @brief ロギングを行うオブジェクト(MonoLog)を管理する
 */
class Logger {

    /**
     * @var \Monolog\Logger []
     */
    private static $logger;

    /**
     * 指定された名前のインスタンスを返す
     * @param string $name Loggerの名前
     * @return \MonoLog\Logger
     */
    public static function get($name='default') {
        if(isset(self::$logger[$name])) {
            return self::$logger[$name];
        }
        throw new \InvalidArgumentException('無効なloggerが指定されました。: ' . $name);
    }

    /**
     * Loggerをセットする
     * @param string $name Loggerの名前
     * @param \Monolog\Logger $logger
     */
    public static function setLogger($name, \Monolog\Logger $logger) {
        self::$logger[$name] = $logger;
    }
}
