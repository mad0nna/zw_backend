<?php

/**
 * @file
 * @brief 認証トークンのDBへのアクセスを行うオブジェクト
 */

namespace Lcas\Repository;

use Lcas\DB\DB;

/**
 * @class AuthTokenRepository
 * @brief 認証トークンのDBへのアクセスを行うオブジェクト
 */
class AuthTokenRepository extends BaseRepository {

    /**
     * コンストラクタ
     */
    public function __construct() {
        parent::__construct();
    }


    /**
     * 指定されたトークンのレコードを検索
     * @param string $token 認証トークン
     * @return mixed auth_tokensテーブルのレコード。見つからなかった場合false
     */
    public function find($token) {
        $token = $this->db->Escape($token);
        $sql = "SELECT token, user_id, expires, created, updated "
            . "  FROM auth_tokens "
            . " WHERE token='{$token}' ";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new \RuntimeException("トークンの検索に失敗しました。SQL:{$sql}, Error:{$error}");
        }
        $result = $this->db->Fetch();
        return $result;
    }

    /**
     * 指定されたユーザーIDのレコードを探す
     * @param int $userId ユーザーID(users.id)
     * @return mixed auth_tokensテーブルのレコード。見つからなかった場合false
     */
    public function findByUserId($userId) {
        $userId = (int)$userId;
        $sql = "SELECT token, user_id, expires, created, updated "
            . "  FROM auth_tokens "
            . " WHERE user_id='{$userId}' ";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new \RuntimeException("トークンの検索に失敗しました。SQL:{$sql}, Error:{$error}");
        }
        $result = $this->db->Fetch();
        return $result;
    }


    /**
     * 期限切れでないトークンを対象にトークンを検索して返す。
     * @param string $token トークン文字列
     * @return mixed auth_tokensテーブルのレコード。見つからなかった場合false
     */
    public function findValid($token) {
        $currentDate = date('Y-m-d H:i:s');

        $token = $this->db->Escape($token);
        $sql = "SELECT token, user_id, expires, created, updated "
             . "  FROM auth_token "
             . " WHERE token='{$token}' AND expires >= '{$currentDate}' ";
        $this->db->setSql($sql);
        if($this->db->Query()) {
            $error = $this->db->getErr();
            throw new \RuntimeException("トークンの検索に失敗しました。SQL:{$sql}, Error:{$error}");
        }
        $result = $this->db->Fetch();
        return $result;
    }

    /**
     * トークン情報を登録する
     * @param int $userId ユーザーID(users.id)
     * @param string $tokenKey トークン文字列
     */
    public function register($userId, $tokenKey) {
        $userId = (int)$userId;

        //既に登録済か確認する
        $token = $this->findByUserId($userId);

        $expires = date('Y-m-d H:i:s', time() + AUTH_TOKEN_LIFE_TIME);
        if(!$token) {
            //このユーザーでは登録されていない。新規。
            $tokenKey = $this->db->Escape($tokenKey);
            $created = date('Y-m-d H:i:s');
            $sql = "INSERT INTO auth_tokens SET token='{$tokenKey}', "
                . "  user_id={$userId}, expires='{$expires}', created='{$created}'";
        } else {
            //登録済のため、更新する
            $tokenKey = $this->db->Escape($tokenKey);
            $updated = date('Y-m-d H:i:s');
            $sql = "UPDATE auth_tokens SET token='{$tokenKey}', "
                . "        expires='{$expires}', updated='{$updated}' "
                . "  WHERE user_id={$userId}";
        }
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new \RuntimeException("トークンの登録に失敗しました。SQL:{$sql}, Error:{$error}");
        }
    }


    /**
     * トークンの有効期限を更新する
     * @param string $tokenKey トークン文字列
     * @param string $newExpirationDate 新しい有効期限
     */
    public function updateExpirationDate($tokenKey, $newExpirationDate) {
        $tokenKey = $this->db->Escape($tokenKey);
        $newExpirationDate = $this->db->Escape($newExpirationDate);

        $updated = date('Y-m-d H:i:s');
        $sql = "UPDATE auth_tokens SET expires='{$newExpirationDate}', updated='{$updated}' "
            . "  WHERE token='{$tokenKey}'";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new \RuntimeException("トークンの更新に失敗しました。SQL:{$sql}, Error:{$error}");
        }
    }


    /**
     * 新しいトークンIDを発行する
     * @param int $length 文字数
     * @return string
     */
    public function getNewUniqueTokenId($length) {

        $maxRetry = 100;
        for($i=0; $i<$maxRetry; $i++) {
            $candidate = $this->getNewTokenId($length);
            $sql = "SELECT token FROM auth_tokens WHERE token='{$candidate}'";
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new \RuntimeException("トークンIDの発行に失敗しました。SQL:{$sql}, Error:{$error}");
            }
            $result = $this->db->Fetch();
            if(!$result) {
                return $candidate;
            }
        }
        throw new \RuntimeException("トークンIDの発行に失敗しました。");
    }

    /**
     * 指定された長さのランダムな文字列を生成する
     * @param int $length 文字数
     * @return string
     */
    public function getNewTokenId($length) {
        $buffer = '';
        for($i=0; $i<$length; $i++) {
            $randInt = mt_rand(0, 61);
            $char = '';
            if($randInt >= 0 && $randInt <= 9) {
                $char = $randInt;
            } else if($randInt >= 10 && $randInt <= 35) {
                $char = chr(ord('A') + $randInt - 10);
            } else {
                $char = chr(ord('a') + $randInt - 36);
            }
            $buffer .= $char;
        }
        return $buffer;
    }

}

