<?php

/**
 * @file
 * @brief Repositoryの基底クラス
 */

namespace Lcas\Repository;



use Lcas\DB\DB;
use Lcas\DB\Mysql;

/**
 * @class BaseRepository
 * @brief Repositoryの基底クラス
 */
class BaseRepository {


    /**
     * DBコネクション
     * @var \Lcas\DB\Mysql
     */
    protected $db;

    /**
     * コンストラクタ
     */
    public function __construct() {
        $this->db = DB::getMasterDb();
    }


    /**
     * DB接続オブジェクトを変更する。
     * 通常とは異る接続を指定する場合に使用する。
     * @param Mysql $newDb DB接続オブジェクト
     */
    public function setConnection(Mysql $newDb) {
        $this->db = $newDb;
    }
}
