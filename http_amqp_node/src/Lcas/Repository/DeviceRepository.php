<?php

/**
 * @file
 * @brief デバイスのDBへのアクセスを行うオブジェクト
 */


namespace Lcas\Repository;



use Lcas\Exception\IOException;
use Lcas\DB\DB;
use Lcas\Helper\DateHelper;


/**
 * @class DeviceRepository
 * @brief デバイスのDBへのアクセスを行うオブジェクト
 */
class DeviceRepository extends BaseRepository {

    /**
     * コンストラクタ
     */
    public function __construct() {
        parent::__construct();
    }


    /**
     * デバイスへのアクセス権があるかを返す
     * @param int $deviceId デバイスID
     * @param int $userId ユーザーID(users.id)
     * @return bool
     */
    public function isGrantedUser($deviceId, $userId, $options=array()) {
        $deviceId = (int)$deviceId;
        $userId = (int)$userId;
        $ownerOnly = (isset($options['owner_only'])) ? $options['owner_only'] : 0;
        
        $sql = "SELECT nodes.id, nodes.sharing_level FROM devices "
            . " INNER JOIN nodes ON (nodes.id=devices.node_id) "
            . " WHERE devices.id={$deviceId} ";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("nodesのSELECT失敗。sql: {$sql}, Error: {$error}");
        }
        $node = $this->db->Fetch();
        if(!$node) {
            return false;
        }
        $nodeId = $node['id'];

        //Nodeが属するGatewayのユーザーと一致するか
        $sql = "SELECT nodes.id FROM nodes "
            . " INNER JOIN gateways ON (gateways.id=nodes.gateway_id) "
            . " WHERE nodes.id={$nodeId} AND gateways.user_id={$userId} ";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("gatewayのSELECT失敗。sql: {$sql}, Error: {$error}");
        }
        $row = $this->db->Fetch();
        if(isset($row['id'])) {
            return true;
        }

        if($ownerOnly) {
            //Nodeのオーナーのみの場合、この時点でfalseを返す。
            return false;
        }

        //Node共有しているユーザーに含まれるか
        $sharingLevel = NODE_SHARING_LEVEL_USERS;
        $sql = "SELECT node_id FROM nodes_users "
             . " INNER JOIN nodes ON (nodes.id=nodes_users.node_id) "
             . " WHERE node_id={$nodeId} AND user_id={$userId} AND nodes.sharing_level={$sharingLevel}";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("nodesのSELECT失敗。sql: {$sql}, Error: {$error}");
        }
        $row = $this->db->Fetch();
        return isset($row['node_id']);
    }


    /**
     * デバイスIDからデバイスを検索する
     * @param int $deviceId デバイスID
     * @return mixed
     */
    public function findById($deviceId) {
        $deviceId = (int)$deviceId;
        $sql = "SELECT id, type, node_id "
            . "  FROM devices WHERE id={$deviceId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("deviceのSELECT失敗。sql: {$sql}, Error: {$error}");
        }

        return $this->db->Fetch();
    }


    /**
     * ユーザーIDからデバイスを検索する
     * @param int $userId ユーザーID
     * @return array
     */
    public function findByUserId($userId) {
        $userId = (int)$userId;
        $sharingLevel = NODE_SHARING_LEVEL_USERS;
        $sql = "SELECT devices.id, devices.node_id, devices.type "
            . "   FROM devices "
            . "   LEFT OUTER JOIN nodes ON (nodes.id=devices.node_id) "
            . "   LEFT OUTER JOIN nodes_users ON (nodes_users.node_id=nodes.id) "
            . "  WHERE nodes_users.user_id={$userId} AND nodes.sharing_level={$sharingLevel}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("deviceのSELECT失敗。sql: {$sql}, Error: {$error}");
        }

        $devices = array();
        while($row = $this->db->Fetch()) {
            $devices[] = $row;
        }
        return $devices;
    }


    /**
     * ノードIDからデバイスを検索する
     * @param int $nodeId ノードID
     * @return array
     */
    public function findByNodeId($nodeId) {
        $nodeId = (int)$nodeId;
        $sql = "SELECT devices.id, devices.node_id, devices.type "
            . "   FROM devices "
            . "  WHERE node_id={$nodeId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("deviceのSELECT失敗。sql: {$sql}, Error: {$error}");
        }

        $devices = array();
        while($row = $this->db->Fetch()) {
            $devices[] = $row;
        }
        return $devices;
    }


    /**
     * 複数のノードIDからデバイスを検索する
     * @param array $nodeIdList ノードIDのリスト
     * @return array
     */
    public function findByNodeIdList($nodeIdList) {

        if(!is_array($nodeIdList) || count($nodeIdList) == 0) {
            return array();
        }

        foreach($nodeIdList as $idx=>$nodeId) {
            $nodeIdList[$idx] = (int)$nodeId;
        }

        $nodeIdListString = implode(',', $nodeIdList);
        $sql = "SELECT devices.id, devices.node_id, devices.type "
            . "   FROM devices "
            . "  WHERE node_id IN ({$nodeIdListString})";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("deviceのSELECT失敗。sql: {$sql}, Error: {$error}");
        }

        $devices = array();
        while($row = $this->db->Fetch()) {
            $devices[] = $row;
        }
        return $devices;
    }


    /**
     * 指定したデバイスのラベルを検索する
     * @param int $nodeId デバイスID
     * @return array
     */
    public function findLabels($nodeId) {
        $nodeId = (int)$nodeId;
        $labelType = LABEL_TYPE_DEVICE;
        $sql = "SELECT label FROM labels WHERE type={$labelType} AND entity_id={$nodeId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("labelのSELECT失敗。sql: {$sql}, Error: {$error}");
        }

        $labels = array();
        while($row = $this->db->Fetch()) {
            $labels[] = $row['label'];
        }
        return $labels;
    }


    /**
     * 指定されたデバイスのデバイスデータを返す
     *
     * DBから返されるtime_stamp列の値はMySQLが返すフォーマットのため、
     * "2015-01-01 00:00:00.123456"のようなフォーマットです。
     *
     * @param int $deviceId デバイスID
     * @param int $fromMilliSec 取得開始日時(ミリ秒精度の数値)。0=下限なし
     * @param int $toMilliSec 取得終了日時(ミリ秒精度の数値)。0=上限なし
     * @return array
     */
    public function findDeviceValues($deviceId, $fromMilliSec, $toMilliSec) {
        $deviceId = (int)$deviceId;
        $sql = "SELECT time_stamp, value, unit "
            . "   FROM device_data "
            . "  WHERE ";

        $conditions = array();
        $conditions[] = "device_id={$deviceId}";

        if($fromMilliSec > 0) {
            $fromTimeStamp = $this->convertMilliSecToMicroSecString($fromMilliSec);
            $conditions[] = " time_stamp >= '{$fromTimeStamp}' ";
        }
        if($toMilliSec > 0) {
            $toTimestamp = $this->convertMilliSecToMicroSecString($toMilliSec);
            $conditions[] = " time_stamp <= '{$toTimestamp}' ";
        }

        $sql .= implode(' AND ', $conditions);
        $sql .= " ORDER BY time_stamp ";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("device_valueのSELECT失敗。sql: {$sql}, Error: {$error}");
        }

        //TODO: この処理は大量のレコードを取得する可能性があるため、
        //TODO: 内部でクエリを分割して発行するか、結果セットから1行ずつフェッチするイテレータを返すことを検討する。
        $values = array();
        while($row = $this->db->Fetch()) {
            $values[] = $row;
        }
        return $values;
    }


    /**
     * 指定されたデバイスの最新のデバイスデータ1件を返す
     * @param int $deviceId デバイスID
     * @return mixed
     */
    public function findLatestDeviceValue($deviceId) {
        $deviceId = (int)$deviceId;
        $sql = "SELECT time_stamp, value, unit "
             . "  FROM device_data WHERE device_id={$deviceId} "
             . " ORDER BY time_stamp DESC "
             . " LIMIT 1";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("device_valueのSELECT失敗。sql: {$sql}, Error: {$error}");
        }

        return $this->db->Fetch();
    }


    /**
     * 指定されたノードのデバイスを置き換える
     * @param int $nodeId ノードID
     * @param array $devices 複数件のdevicesテーブルのレコードを含んだ配列
     */
    public function replaceDevies($nodeId, $devices) {

        $nodeId = (int)$nodeId;
        $deleteSql = "DELETE FROM devices WHERE node_id={$nodeId}";
        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("deviceの削除失敗。sql: {$deleteSql}, Error: {$error}");
        }

        if(!is_array($devices)) {
            throw new \InvalidArgumentException("devicesが配列ではありません。");
        }

        foreach($devices as $device) {
            $created = date('Y-m-d H:i:s');
            $deviceId = (int)$device['id'];
            $deviceType = (int)$device['type'];
            $sql = "INSERT INTO devices SET  "
                 . " id={$deviceId}, "
                 . " node_id={$nodeId}, "
                 . " type='{$deviceType}', "
                 . " created='{$created}'";
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("capabilitiesの登録失敗。sql: {$sql}, Error: {$error}");
            }
        }
    }


    /**
     * Deviceデータを登録する
     * @param int $deviceId デバイスID
     * @param array $data devicesテーブルのレコードを含んだ配列
     */
    public function registerDeviceData($deviceId, $data) {
        $deviceId = (int)$deviceId;
        $timeStamp = (int)$data['time_stamp'];
        $value = $this->db->Escape($data['value']);
        $unit = isset($data['unit']) ? $this->db->Escape($data['unit']) : '';

        $sql = "INSERT INTO device_data SET device_id={$deviceId}, "
             . "  time_stamp={$timeStamp}, value='{$value}', unit='{$unit}' ";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("capabilitiesの登録失敗。sql: {$sql}, Error: {$error}");
        }
    }

    /**
     * デバイスのラベルを更新する
     * @param int $deviceId デバイスID
     * @param array $labels ラベル文字列を含んだ配列
     */
    public function updateLabels($deviceId, $labels) {
        $deviceId = (int)$deviceId;

        $this->removeAllLabels($deviceId);

        $labelType = LABEL_TYPE_DEVICE;
        foreach((array)$labels as $label) {
            $created = date('Y-m-d H:i:s');
            $label = $this->db->Escape($label);
            $sql = "INSERT INTO labels SET type={$labelType}, entity_id={$deviceId}, label='{$label}', created='{$created}'";
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("labelの登録失敗。sql: {$sql}, Error: {$error}");
            }
        }
    }


    /**
     * 指定したデバイスのラベルを全削除する
     * @param int $deviceId デバイスID
     */
    public function removeAllLabels($deviceId) {
        $deviceId = (int)$deviceId;
        $labelType = LABEL_TYPE_DEVICE;
        $deleteSql = "DELETE FROM labels WHERE type={$labelType} AND entity_id={$deviceId}";

        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("labelの削除失敗。sql: {$deleteSql}, Error: {$error}");
        }
    }


    /**
     * 指定したデバイスのデバイスデータを全削除する
     * @param int $deviceId デバイスID
     */
    public function removeAllData($deviceId) {
        $deviceId = (int)$deviceId;
        $deleteSql = "DELETE FROM device_data WHERE device_id={$deviceId}";

        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$deleteSql}, Error: {$error}");
        }
    }


    /**
     * 指定された日付以前のデータを全削除する
     * @param string $startDate 日付
     * @param int $limit 1回のクエリで削除する件数を制限する場合、件数
     * @return int 削除した件数
     */
    public function removeDataBefore($startDate, $limit=0) {

        if(!DateHelper::isValidDateTimeString($startDate)) {
            throw new \InvalidArgumentException('無効な日付けが指定されました。 startDate: ' . $startDate);
        }

        $startDate = $this->db->Escape($startDate);

        $sql = "DELETE FROM device_data "
            . " WHERE time_stamp <= '{$startDate}' ";

        if($limit > 0) {
            $sql .= ' LIMIT ' . (int)$limit;
        }

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        return $this->db->Affected_rows();
    }


    /**
     * デバイス種別のタイプコードと名称の配列を返す
     * @return array
     */
    public static function getDeviceTypeMapping() {
        return array(
            DEVICE_TYPE_TEMPERATURE  => 'temperature',
            DEVICE_TYPE_HUMIDITY     => 'humidity',
            DEVICE_TYPE_MOTION       => 'motion',
            DEVICE_TYPE_OPEN_CLOSE   => 'open_close',
            DEVICE_TYPE_LOCK         => 'lock',
            DEVICE_TYPE_COVER        => 'cover',
            DEVICE_TYPE_BATTERY      => 'battery',
            DEVICE_TYPE_LUMINANCE    => 'luminance',
			DEVICE_TYPE_CALL_BUTTON  => 'call_button',
			DEVICE_TYPE_SMART_LOCK   => 'smart_lock',
			DEVICE_TYPE_POWER        => 'power',
			DEVICE_TYPE_ACC_ENERGY   => 'acc_energy',
            DEVICE_TYPE_POWER_SWITCH => 'power_switch',
        	DEVICE_TYPE_APPARENT_ENERGY => 'apparent_energy',
        	DEVICE_TYPE_ACC_GAS => 'acc_gas',
        	DEVICE_TYPE_ACC_WATER => 'acc_water',
        	DEVICE_TYPE_HEART_RATE => 'heart_rate',
        	DEVICE_TYPE_BREATHING_RATE => 'breathing_rate',
        	DEVICE_TYPE_BODY_MOTION => 'body_motion',
        	DEVICE_TYPE_SMOKE => 'smoke',
        	DEVICE_TYPE_PERSON_STATE => 'person_state',
        	DEVICE_TYPE_DISTANCE => 'distance',
        	DEVICE_TYPE_PARKERS_CAR => 'parkers_car',
        	DEVICE_TYPE_PARKERS_LED => 'parkers_led',
        	DEVICE_TYPE_PARKERS_MODE => 'parkers_mode',
        	DEVICE_TYPE_PARKERS_BATTERY => 'parkers_battery',
        );
    }

    /**
     * デバイス種別のタイプコード->名前変換を行う
     * @param int $type DEVICE_TYPE_XXX定数
     * @return mixed デバイス名、見つからない場合はfalse
     */
    public static function getDeviceTypeNameFromType($type) {
        $mapping = self::getDeviceTypeMapping();
        if(isset($mapping[$type])) {
            return $mapping[$type];
        }
        return false;
    }

    /**
     * デバイス種別の名前->タイプコード変換を行う
     * @param string $typeName デバイスタイプ名
     * @return mixed DEVICE_TYPE_XXX、見つからない場合はfalse
     */
    public static function getDeviceTypeFromName($typeName) {
        $mapping = self::getDeviceTypeMapping();
        return array_search($typeName, $mapping);
    }


    /**
     * DB内部で保持しているマイクロ秒精度の日付文字列をミリ秒精度の数値に変換する
     * @param string $microTime MySQLのミリ秒制度の日付を含んだ文字列
     * @return int ミリ秒精度の整数
     *
     * ex. 2015-10-01-01 00:00:00.12345789
     */
    public static function convertMicroTimeToMilliSec($microTime) {
        $split = explode('.', $microTime);
        $dateTime = $split[0];
        $microSec = isset($split[1]) ? $split[1] : 0;

        $milliSec = strtotime($dateTime) * 1000;
        $milliSec += (int)($microSec / 1000);
        return $milliSec;
    }

    /**
     * ミリ秒精度の数値をMySQLが使用するマイクロ秒精度の日付文字列に変換する
     * @param int $milliSec MySQLのミリ秒制度の日付を含んだ文字列
     * @return string マイクロ秒精度の整数
     */
    public static function convertMilliSecToMicroSecString($milliSec) {
        $seconds = (int)($milliSec / 1000);
        $microSeconds = ($milliSec % 1000);

        $microSecondString = sprintf('%s.%03d', date('Y-m-d H:i:s', $seconds), $microSeconds);
        return $microSecondString;
    }

	/**
	 * Device ID から属しているノードIDを取得します。
	 * @param integer $deviceId
	 * @throws IOException
	 */
	public function findNodeById($deviceId) {
		$deviceId = (int)$deviceId;

		$sql = "SELECT nodes.id "
				. " FROM nodes "
				. "   INNER JOIN devices ON (devices.node_id=nodes.id) "
				. " WHERE devices.id={$deviceId} ";
				
		$this->db->setSql($sql);
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException("nodesのSELECT失敗。sql: {$sql}, Error: {$error}");
		}
		
		$row = $this->db->Fetch();
		return (int)$row['id'];
	}
	
    /**
	 * Device ID から属しているノードの NodeName を取得します。
	 * @param integer $deviceId
	 * @throws IOException
	 */
	public function findNodeNameById($deviceId) {
		$deviceId = (int)$deviceId;

		$sql = "SELECT nodes.manufacturer, nodes.product, nodes.serial_no "
				. " FROM nodes "
				. "   INNER JOIN devices ON (devices.node_id=nodes.id) "
				. " WHERE devices.id={$deviceId} ";
				
		$this->db->setSql($sql);
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException("nodesのSELECT失敗。sql: {$sql}, Error: {$error}");
		}
		
		$row = $this->db->Fetch();
		
		$nodeName = sprintf('%s_%s_%s', $row['manufacturer'], $row['product'], $row['serial_no']);
		return $nodeName;
	}
	
}
