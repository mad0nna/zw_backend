<?php

/**
 * @file
 * @brief ゲートウェイのDBへのアクセスを行うオブジェクト
 */

namespace Lcas\Repository;
use Lcas\Exception\IOException;
use Lcas\DB\DB;
use Lcas\Log\Logger;

/**
 * @class GatewayRepository
 * @brief ゲートウェイのDBへのアクセスを行うオブジェクト
 */
class GatewayRepository extends BaseRepository {

    public function __construct() {
        parent::__construct();
    }

    /**
     * ゲートウェイへのアクセス権があるかを返す
     * @param int $gatewayId ゲートウェイID
     * @param int $userId ユーザーID(users.id)
     * @return bool
     */
    public function isGrantedUser($gatewayId, $userId) {
        $gatewayId = (int)$gatewayId;
        $userId = (int)$userId;

        $sql = "SELECT id FROM gateways WHERE id={$gatewayId} AND user_id={$userId} ";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("gatewayのSELECT失敗。sql: {$sql}, Error: {$error}");
        }
        $row = $this->db->Fetch();
        return isset($row['id']);
    }

    /**
     * ゲートウェイIDからゲートウェイを検索する
     * @param int $id ゲートウェイID
     * @return mixed
     */
    public function findById($id) {
        $gatewayId = (int)$id;

        $sql = "SELECT id, mac_address, fw_version, manufacturer, "
             . "       state, user_id, prev_user_id, created, updated, public_key, gw_name, status"
             . "  FROM gateways WHERE id={$gatewayId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("gatewayのSELECT失敗。sql: {$sql}, Error: {$error}");
        }
        $row = $this->db->Fetch();
        return $row;
    }

    /**
     * ユーザーIDからゲートウェイを検索する
     * @param int $userId ユーザーID(users.id)
     * @return mixed
     */
    public function findByUserId($userId) {
        $userId = (int)$userId;

        // todo: add new columns? public_key, gw_name

        $sql = "SELECT id, mac_address, fw_version, manufacturer, "
            . "        state, user_id, prev_user_id, created, updated "
            . "   FROM gateways "
            . "  WHERE user_id={$userId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("gatewayのSELECT失敗。sql: {$sql}, Error: {$error}");
        }
        $rows = array();
        while($row = $this->db->Fetch()) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * ノードIDからゲートウェイを検索する
     * @param int $nodeId ノードID
     * @return mixed
     */
    public function findByNodeId($nodeId) {
        $nodeId = (int)$nodeId;

        // todo: add new columns? public_key, gw_name

        $sql = "SELECT gateways.id, mac_address, fw_version, manufacturer, "
            . "        gateways.state, gateways.user_id, gateways.prev_user_id, gateways.created, gateways.updated "
            . "   FROM gateways "
            . "  INNER JOIN nodes ON (nodes.gateway_id=gateways.id) "
            . "  WHERE nodes.id={$nodeId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("gatewayのSELECT失敗。sql: {$sql}, Error: {$error}");
        }
        $row = $this->db->Fetch();
        return $row;
    }


    /**
     * MACアドレスからゲートウェイを検索する
     * @param string $name MACアドレス
     * @param array $options 検索オプション
     *
     * - with_associated: ユーザーに割り当てられたゲートウェイのみを対象とする場合、true(デフォルト:false)
     *
     * @return mixed
     */
    public function findByMacAddress($name, $options=array()) {
        $name = $this->db->Escape($name);

        // todo: add new columns? public_key, gw_name

        $sql = "SELECT gateways.id, mac_address, fw_version, manufacturer, "
            . "        gateways.state, gateways.user_id, gateways.prev_user_id, gateways.created, gateways.updated, gateways.public_key "
            . "   FROM gateways "
            . "  WHERE gateways.mac_address='{$name}' ";

        if(isset($options['with_associated']) && $options['with_associated']) {
            $sql .= " AND gateways.user_id != 0 ";
        }

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("gatewayのSELECT失敗。sql: {$sql}, Error: {$error}");
        }
        $row = $this->db->Fetch();
        return $row;
    }


    /**
     * capabilityの一覧を返す
     * @param int $gatewayId ゲートウェイID
     * @return array
     */
    public function findCapabilitiesById($gatewayId) {
        $gatewayId = (int)$gatewayId;
        $sql = "SELECT capability FROM capabilities WHERE gateway_id={$gatewayId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("capabilityのSELECT失敗。sql: {$sql}, Error: {$error}");
        }

        $capabilities = array();
        while($row = $this->db->Fetch()) {
            $capabilities[] = $row['capability'];
        }
        return $capabilities;
    }

    /**
     * ラベルの一覧を返す
     * @param int $gatewayId ゲートウェイID
     * @return array
     */
    public function findLabelsById($gatewayId) {
        $gatewayId = (int)$gatewayId;
        $labelType = LABEL_TYPE_GATEWAY;
        $sql = "SELECT label FROM labels WHERE type={$labelType} AND entity_id={$gatewayId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("labelのSELECT失敗。sql: {$sql}, Error: {$error}");
        }

        $labels = array();
        while($row = $this->db->Fetch()) {
            $labels[] = $row['label'];
        }
        return $labels;
    }


    /**
     * 指定したラベルに部分一致するGatewayのID一覧を返す
     * @param string $label
     * @return array
     */
    public function findGatewayIdListByLabel($label) {
        $label = $this->db->LikeEscape($label);

        $labelType = LABEL_TYPE_GATEWAY;
        $sql = "SELECT entity_id FROM labels WHERE type={$labelType} AND label LIKE '%{$label}%' ";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $rows = array();
        while($row = $this->db->Fetch()) {
            $rows[] = (int)$row['entity_id'];
        }
        return $rows;
    }


    /**
     * ゲートウェイを登録する
     * @param array $gateway gatewaysテーブルのカラムをキー名に持った配列
     * @return int INSERTしたgatewayのID
     */
    public function register($gateway) {
        $escapedMacAddr = $this->db->Escape($gateway['mac_address']);
        $escapedGwName = $this->db->Escape($gateway['gw_name']);
        $escapedFwVersion = $this->db->Escape($gateway['fw_version']);
        $escapedManufacturer = $this->db->Escape($gateway['manufacturer']);
        $escapedState = (int)$gateway['state'];
        $escapedUserId = (int)$gateway['user_id'];
        $created = date('Y-m-d H:i:s');

        $sql = "INSERT INTO gateways SET "
             . " mac_address='{$escapedMacAddr}', "
             . " gw_name='{$escapedGwName}', "
             . " fw_version='{$escapedFwVersion}', "
             . " manufacturer='{$escapedManufacturer}', "
             . " user_id={$escapedUserId}, "
             . " state={$escapedState}, "
             . " created='{$created}' "
        ;

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        return $this->db->Insert_id();
    }


    /**
     * ゲートウェイ情報を更新する
     * @param int $gatewayId ゲートウェイID
     * @param array $gateway gatewaysテーブルのカラムをキー名に持った配列
     * @return int 未使用
     */
    public function update($gatewayId, $gateway) {
        $gatewayId = (int)$gatewayId;
        $escapedMacAddr = $this->db->Escape($gateway['mac_address']);
        $escapedFwVersion = $this->db->Escape($gateway['fw_version']);
        $escapedManufacturer = $this->db->Escape($gateway['manufacturer']);
        $escapedState = (int)$gateway['state'];
        $escapedUserId = (int)$gateway['user_id'];
        $updated = date('Y-m-d H:i:s');

        $sql = "UPDATE gateways SET "
            . " mac_address='{$escapedMacAddr}', "
            . " fw_version='{$escapedFwVersion}', "
            . " manufacturer='{$escapedManufacturer}', "
            . " user_id={$escapedUserId}, "
            . " state={$escapedState}, "
            . " updated='{$updated}' "
            . " WHERE id={$gatewayId}"
        ;

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        return $this->db->Insert_id();
    }


    /**
     * ゲートウェイ割り当てを解除する
     * @param int $gatewayId ゲートウェイID
     */
    public function clearAssociation($gatewayId) {
        $gatewayId = (int)$gatewayId;
        $updated = date('Y-m-d H:i:s');

        $sql = "UPDATE gateways SET prev_user_id=user_id, user_id=0, updated='{$updated}' WHERE id={$gatewayId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }

    /**
     * ゲートウェイの状態を更新する
     * @param int $gatewayId ゲートウェイID
     * @param int $state GATEWAY_STATE_XXX定数
     */
    public function updateState($gatewayId, $state) {
        $gatewayId = (int)$gatewayId;
        $state = (int)$state;
        $updated = date('Y-m-d H:i:s');
        $sql = "UPDATE gateways SET state={$state}, updated='{$updated}' WHERE id={$gatewayId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("gatewayの更新失敗。sql: {$sql}, Error: {$error}");
        }
    }

    /**
     * ゲートウェイをDBから削除する(現在未使用)
     * @param int $gatewayId ゲートウェイID
     */
    public function remove($gatewayId) {
        $gatewayId = (int)$gatewayId;
        $deleteSql = "DELETE FROM gateways WHERE id={$gatewayId}";

        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("gatewayの削除失敗。sql: {$deleteSql}, Error: {$error}");
        }
    }


    /**
     * ラベルを更新する
     * @param int $gatewayId ゲートウェイID
     * @param array $labels ラベル文字列を含んだ配列
     */
    public function updateLabels($gatewayId, $labels) {
        $gatewayId = (int)$gatewayId;

        $this->removeAllLabels($gatewayId);

        $labelType = LABEL_TYPE_GATEWAY;
        foreach((array)$labels as $label) {
            $created = date('Y-m-d H:i:s');
            $label = $this->db->Escape($label);
            $sql = "INSERT INTO labels SET type={$labelType}, entity_id={$gatewayId}, label='{$label}', created='{$created}'";
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("labelの登録失敗。sql: {$sql}, Error: {$error}");
            }
        }

    }


    /**
     * ラベルを前削除する
     * @param int $gatewayId ゲートウェイID
     */
    public function removeAllLabels($gatewayId) {
        $gatewayId = (int)$gatewayId;
        $labelType = LABEL_TYPE_GATEWAY;
        $deleteSql = "DELETE FROM labels WHERE type={$labelType} AND entity_id={$gatewayId}";

        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("labelの削除失敗。sql: {$deleteSql}, Error: {$error}");
        }
    }


    /**
     * Capabilityを更新する
     * @param int $gatewayId ゲートウェイID
     * @param array $capabilityTypes capabilityを表す数値(CAPABILITY_X)の配列
     */
    public function replaceCapabilities($gatewayId, $capabilityTypes) {
        $this->removeCapabilities($gatewayId);

        if(!is_array($capabilityTypes)) {
            throw new \InvalidArgumentException("capabilityTypesが配列ではありません。");
        }

        foreach($capabilityTypes as $capabilityType) {
            $created = date('Y-m-d H:i:s');
            $sql = "INSERT INTO capabilities SET gateway_id={$gatewayId}, "
                . " capability={$capabilityType}, created='{$created}' ";
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("capabilitiesの登録失敗。sql: {$sql}, Error: {$error}");
            }
        }
    }


    /**
     * capabilityの一覧を全削除する
     * @param int $gatewayId ゲートウェイID
     */
    public function removeCapabilities($gatewayId) {
        $gatewayId = (int)$gatewayId;
        $deleteSql = "DELETE FROM capabilities WHERE gateway_id={$gatewayId}";
        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("capabilitiesnの削除失敗。sql: {$deleteSql}, Error: {$error}");
        }
    }

    /**
     * ゲートウェイの状態の名前<->タイプコードを提議した配列を返す
     * @return array
     */
    public static function getStateMapping() {
        return array(
            GATEWAY_STATE_CONNECTED => 'connected',
            GATEWAY_STATE_DISCONNECTED => 'disconnected',
            GATEWAY_STATE_INCLUSION => 'inclusion',
            GATEWAY_STATE_EXCLUSION => 'exclusion',
            GATEWAY_STATE_INITIAL => 'initial',
            GATEWAY_STATE_CONFIGURING => 'configuring',
            GATEWAY_STATE_JOINING => 'joining',
            GATEWAY_STATE_NORMAL => 'normal',
            GATEWAY_STATE_FAILED => 'failed',
        );
    }

    /**
     * ゲートウェイの状態のタイプコード->名前変換を行う
     * @param int $type GATEWAY_STATE_XXX定数
     * @return string
     */
    public static function getStateNameFromId($type) {
        $mapping = self::getStateMapping();
        if(isset($mapping[$type])) {
            return $mapping[$type];
        }
        return false;
    }

    /**
     * ゲートウェイの状態の名前->タイプコード変換を行う
     * @param string $name 状態名
     * @return string
     */
    public static function getStateIdFromName($name) {
        $mapping = self::getStateMapping();
        return array_search($name, $mapping);
    }


    /**
     * ゲートウェイのcapabilityの名前<->タイプコードを提議した配列を返す
     * @return array
     */
    public static function getCapabilityTypeMapping() {
        return array(
            CAPABILITY_ZWAVE     => 'zwave',
            CAPABILITY_IP_CAMERA => 'ip_camera',
            CAPABILITY_BLUETOOTH => 'bluetooth',
            CAPABILITY_DUST_IP => 'dust_ip',
        );
    }

    /**
     * ゲートウェイのcapabilityのタイプコード->名前変換を行う
     * @param int $type CAPABILITY_XXX定数
     * @return string
     */
    public static function getCapabilityNameFromType($type) {
        $mapping = self::getCapabilityTypeMapping();
        if(isset($mapping[$type])) {
            return $mapping[$type];
        }
        return false;
    }

    /**
     * ゲートウェイのcapabilityの名前->タイプコード変換を行う
     * @param string $typeName capability名
     * @return string
     */
    public static function getCapabilityTypeFromName($typeName) {
        $mapping = self::getCapabilityTypeMapping();
        return array_search($typeName, $mapping);
    }

    /**
     * getCapabilityTypeFromNameの複数形
     * @param array $typeNames capability名の配列
     * @return array
     */
    public static function getCapabilityTypesFromNames($typeNames) {
        $types = array();
        foreach((array)$typeNames as $typeName) {
            $types[] = self::getCapabilityTypeFromName($typeName);
        }
        return $types;
    }

    /**
     * getCapabilityNameFromTypeの複数形
     * @param array $types CAPABILITY_XXX定数を含んだ配列
     * @return array
     */
    public static function getCapabilityNamesFromTypes($types) {
        $names = array();
        foreach((array)$types as $type) {
            $names[] = self::getCapabilityNameFromType($type);
        }
        return $names;
    }

    public function updatePublicKey($gatewayId, $publicKey) {
        $gatewayId = (int)$gatewayId;
        $state = (string)$publicKey;
        $updated = date('Y-m-d H:i:s');
        $sql = "UPDATE gateways SET public_key='{$publicKey}', updated='{$updated}' WHERE id={$gatewayId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("gatewayの更新失敗。sql: {$sql}, Error: {$error}");
        }
    }

    public function dustIpAssociated($gatewayId) {
        $sql = "select exists (select * from dust_gateways where gateway_id={$gatewayId} ) as 'exists';";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("gatewayの更新失敗。sql: {$sql}, Error: {$error}");
        }
        $result = $this->db->Fetch();
        return $result['exists'] == 1;
    }

    public function getDustIp($gatewayId) {
        $sql = "SELECT network_id FROM dust_gateways where gateway_id='{$gatewayId}' ;";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("gatewayの更新失敗。sql: {$sql}, Error: {$error}");
        }
        $result = $this->db->Fetch();

        $dustIp = [];
        $dustIp['acl_set'] = isset($result['network_id']);
        if ($dustIp['acl_set']) {
            $dustIp['network_id'] = $result['network_id'];
        }

        return $dustIp;
    }

    public function reserveDustIp($gatewayId) {
        $created = date('Y-m-d H:i:s');

        $sql = "delete from dust_gateways where "
             . " gateway_id='{$gatewayId}' "
        ;

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $sql = "INSERT INTO dust_gateways SET "
             . " gateway_id='{$gatewayId}', "
             . " created='{$created}', "
             . " updated='{$created}' "
        ;

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }
}
