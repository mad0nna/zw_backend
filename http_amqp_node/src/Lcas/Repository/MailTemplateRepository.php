<?php

namespace Lcas\Repository;
use Lcas\Exception\IOException;
use Lcas\DB\DB;

class MailTemplateRepository extends BaseRepository {

	public function __construct() {
        parent::__construct();
    }
    
    /**
     * Origin からメールテンプレートを探します
     * @param string $origin Originヘッダの内容
     * @param int $platform Mobile Application Platform(null:default, 1:NONE, 2:APNS, 3:GCM, 4:SQS)
     * @param int $templateType テンプレートの種類(1:新規アカウント用、2:メールアドレス変更用、3:パスワードリセット用)
     * @param boolean $doGetDefault trueの場合には、DBから取得できなかった場合にデフォルトのテンプレートを取得する
     * @return mixed false の場合は失敗、それ以外は取得したメールテンプレートの情報の配列(配列要素:'sender':送信者、'subject':件名、'content':メール本文)
     * @throws IOException
     */
    public function findMailTemplateByOrigin($origin, $platform, $templateType, $doGetDefault = true) {
    	$templateType = (int)$templateType;
    	$platform = is_null($platform) ? 0 : (int)$platform;
    	
    	// Origin をキーとしてメールテンプレートを取得する
    	$sql = "SELECT sender, subject, content, `content-type` "
				. "  FROM mail_templates "
				. "  WHERE origin='{$origin}' "
				. "  AND platform={$platform}"
				. "  AND template_type={$templateType}";
		
		$this->db->setSql($sql);
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException("mail_templatesのSELECT失敗。sql: {$sql}, Error: {$error}");
		} else if (0 < $this->db->Num_rows()) {
			return $this->db->Fetch();
		}
    	// Originが NULL、または、取得できなかったらデフォルトのテンプレートを取得する
    	// デフォルトのテンプレートは config_http.php に定義されている
		else if ($doGetDefault) {
			return $this->getDefault($templateType);
		} else {
			return false;
		}
		return false;
    }
    
    /**
     * メールテンプレートのデフォルト値を取得
     * @param int $templateType メールテンプレートの種類(1:新規アカウント用、2:メールアドレス変更用、3:パスワードリセット用)
     * @return mixed false の場合は失敗、それ以外は取得したメールテンプレートの情報の配列(配列要素:'sender':送信者、'subject':件名、'content':メール本文)
     */
    private function getDefault($templateType) {
    	if ($templateType === MAIL_TEMPLATE_TYPE_NEW_USER) {
    		// アカウント新規作成
    		return array(
				'sender' => DEFAULT_MAIL_TEMPLATE_NEW_USER_CONFIRMATION_SENDER,
				'subject' => DEFAULT_MAIL_TEMPLATE_NEW_USER_CONFIRMATION_SUBJECT,
				'content' => DEFAULT_MAIL_TEMPLATE_NEW_USER_CONFIRMATION_CONTENT,
				'content-type' => DEFAULT_MAIL_TEMPLATE_NEW_USER_CONFIRMATION_CONTENT_TYPE,
    		);
		} else if ($templateType === MAIL_TEMPLATE_TYPE_CHANGE_EMAIL) {
			// メールアドレスの変更
			return array(
					'sender' => DEFAULT_MAIL_TEMPLATE_CHANGE_EMAIL_CONFIRMATION_SENDER,
					'subject' => DEFAULT_MAIL_TEMPLATE_CHANGE_EMAIL_CONFIRMATION_SUBJECT,
					'content' => DEFAULT_MAIL_TEMPLATE_CHANGE_EMAIL_CONFIRMATION_CONTENT,
					'content-type' => DEFAULT_MAIL_TEMPLATE_CHANGE_EMAIL_CONFIRMATION_CONTENT_TYPE,
			);
		} else if ($templateType === MAIL_TEMPLATE_TYPE_RESET_PASSWORD) {
			// パスワードリセット
			return array(
					'sender' => DEFAULT_MAIL_TEMPLATE_RESET_PASSWORD_CONFIRMATION_SENDER,
					'subject' => DEFAULT_MAIL_TEMPLATE_RESET_PASSWORD_CONFIRMATION_SUBJECT,
					'content' => DEFAULT_MAIL_TEMPLATE_RESET_PASSWORD_CONFIRMATION_CONTENT,
					'content-type' => DEFAULT_MAIL_TEMPLATE_RESET_PASSWORD_CONFIRMATION_CONTENT_TYPE,
			);
		}
		return false;
	}
    
    /**
     * メールテンプレートのメール本文('content')に含まれるプレースホルダーの置き換えをおこなう
     * @param string $bodyTemplate メールテンプレートのメール本文
     * @param string $email プレースホルダー{email_address}を置き換える文字列
     * @param sting $confirmationToken プレースホルダー{confirmation_token}を置き換える文字列
     * @param string $resetToken プレースホルダー{reset_token}を置き換える文字列
     * @return string 置き換え処理後の文字列
     */
    public static function formatBody($bodyTemplate, $email, $confirmationToken, $resetToken) {
    	$result = str_replace(
    			array(	MAIL_PLACEHOLDER_EMAIL,
    					MAIL_PLACEHOLDER_CONFIRMATION_TOKEN,
    					MAIL_PLACEHOLDER_RESET_TOKEN
    					),
    			array(	$email,
    					$confirmationToken,
    					$resetToken
    					),
    			$bodyTemplate
    			);
    	return $result;
    }
    
    /**
     * token の種類からメールテンプレートの種類に変換する
     * @param int $tokenType
     * @return mixed false の場合は失敗、それ以外は、メールテンプレートの種類(int)
     */
    public static function convertMailTemplateTypeFromTokenType($tokenType) {
    	$tokenType = (int)$tokenType;
    	switch ($tokenType) {
    		case USER_CONFIRM_TOKEN_TYPE_NEW_USER:
    			return MAIL_TEMPLATE_TYPE_NEW_USER;
    		case USER_CONFIRM_TOKEN_TYPE_CHANGE_EMAIL:
    			return MAIL_TEMPLATE_TYPE_CHANGE_EMAIL;
    		case USER_CONFIRM_TOKEN_TYPE_RESET_PASSWORD:
    			return MAIL_TEMPLATE_TYPE_RESET_PASSWORD;
    		default:
    			return false;
    	}
    	return false;
    }

}