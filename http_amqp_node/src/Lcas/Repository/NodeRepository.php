<?php

/**
 * @file
 * @brief ノードのDBへのアクセスを行うオブジェクト
 */

namespace Lcas\Repository;



use Lcas\Exception\IOException;
use Lcas\DB\DB;
use Lcas\Helper\DateHelper;

/**
 * @class NodeRepository
 * @brief ノードのDBへのアクセスを行うオブジェクト
 */
class NodeRepository extends BaseRepository {

    public function __construct() {
        parent::__construct();
    }

    /**
     * ノードへのアクセス権があるかを返す
     * @param int $nodeId ノードID
     * @param int $userId ユーザーID(users.id)
     * @param array $options 検索オプション
     *
     *  - owner_only: ノードの所有者のみ許可とするか(デフォルト: false)
     *
     * @return bool
     */
    public function isGrantedUser($nodeId, $userId, $options=array()) {
        $nodeId = (int)$nodeId;
        $userId = (int)$userId;
        $ownerOnly = (isset($options['owner_only'])) ? $options['owner_only'] : 0;

        //Nodeが属するGatewayのユーザーと一致するか
        $sql = "SELECT nodes.id FROM nodes "
             . " INNER JOIN gateways ON (gateways.id=nodes.gateway_id) "
             . " WHERE nodes.id={$nodeId} AND gateways.user_id={$userId} ";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        $row = $this->db->Fetch();
        if(isset($row['id'])) {
            return true;
        }

        if($ownerOnly) {
            //Nodeのオーナーのみの場合、この時点でfalseを返す。
            return false;
        }

        //Node共有しているユーザーに含まれるか
        $sharingLevel = NODE_SHARING_LEVEL_USERS;
        $sql = "SELECT node_id FROM nodes_users "
             . " INNER JOIN nodes ON (nodes.id=nodes_users.node_id) "
             . " INNER JOIN gateways ON (gateways.id=nodes.gateway_id) "
             . " WHERE nodes_users.node_id={$nodeId} AND nodes_users.user_id={$userId} "
             . "   AND nodes.sharing_level={$sharingLevel}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        $row = $this->db->Fetch();
        return isset($row['node_id']);
    }


    /**
     * ノードIDからノードを検索する
     * @param int $nodeId ノードID
     * @return mixed
     */
    public function findById($nodeId) {
        $nodeId = (int)$nodeId;
        $sql = "SELECT id, gateway_id, protocol, sharing_level, "
            . " location_name, location_type, "
            . " manufacturer, product, serial_no, prev_user_id, "
            . " state, failed_cmd, "
            . " created, updated, join_key_set, yet_another_state, node_type "
            . " FROM nodes WHERE id={$nodeId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        return $this->db->Fetch();
    }


    /**
     * ゲートウェイIDの配列からノードを検索する
     * @param array $gatewayIds ゲートウェイIDを含んだ配列
     * @param string $target 検索対象ノード
     * @return array
     */
    public function findByGatewayIds($gatewayIds, $target=null) {

        foreach($gatewayIds as $idx=>$gatewayId) {
            $gatewayIds[$idx] = (int)$gatewayId;
        }
        if(count($gatewayIds) == 0) {
            return array();
        }

        $joinedGatewayIds = implode(',', $gatewayIds);
        $sql = "SELECT nodes.id, gateway_id, protocol, sharing_level, "
			. " location_name, location_type, "
            . " manufacturer, product, serial_no, prev_user_id, "
            . " state, failed_cmd, "
            . " created, updated, join_key_set, yet_another_state, node_type "
            . " FROM nodes "
        	. " WHERE gateway_id IN ({$joinedGatewayIds})";
        if (!is_null($target)) {
        	if ($target == 'failed-nodes') {
        		$targetNode = NODE_STATE_FAILED;
	        	$sql .= " AND state={$targetNode} ";
        	}
        }
        $sql .= " ORDER BY id";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $nodes = array();
        while($row = $this->db->Fetch()) {
            $nodes[] = $row;
        }
        return $nodes;
    }

    /**
     * ユーザーIDからノードを検索する
     * @param int $userId ユーザーID(users.id)
     * @param string $target 検索対象ノード
     * @return array
     */
    public function findByUserId($userId, $target=null) {
        $userId = (int)$userId;
        $sharingLevel = NODE_SHARING_LEVEL_USERS;
        $sql = "SELECT nodes.id, nodes.gateway_id, nodes.protocol, nodes.sharing_level, "
            . "        nodes.location_name, nodes.location_type, nodes.manufacturer, "
            . "        nodes.product, nodes.serial_no, nodes.prev_user_id, "
            . "        nodes.state, nodes.failed_cmd, "
            . "        nodes.created, nodes.updated, nodes.join_key_set, nodes.yet_another_state, nodes.node_type "
            . "   FROM nodes "
            . "   LEFT OUTER JOIN nodes_users ON (nodes_users.node_id=nodes.id) "
            . "   INNER JOIN gateways ON (gateways.id=nodes.gateway_id) "
            . "  WHERE nodes_users.user_id={$userId} AND nodes.sharing_level={$sharingLevel} ";
        if (!is_null($target)) {
        	if ($target == 'failed-nodes') {
        		$targetNode = NODE_STATE_FAILED;
	        	$sql .= " AND nodes.state={$targetNode} ";
        	}
        }
        $sql .= " ORDER BY id";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $nodes = array();
        while($row = $this->db->Fetch()) {
            $nodes[] = $row;
        }
        return $nodes;
    }


    /**
     * ラベルの一覧を返す
     * @param int $nodeId ノードID
     * @param int $userId ユーザー単位設定を取得する場合に指定してください。ノード単位設定を取得する場合には null を指定してください。
     * @return array 取得したラベルの配列
     */
    public function findLabels($nodeId, $userId) {
        $nodeId = (int)$nodeId;
        $labelType = LABEL_TYPE_NODE;
        $sql = "SELECT label FROM labels WHERE type={$labelType} AND entity_id={$nodeId} ";
        if (is_null($userId)) {
        	$sql .= " AND user_id is NULL";
        } else {
       		$userId = (int)$userId;
        	$sql .= " AND user_id={$userId}";
        }

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $labels = array();
        while($row = $this->db->Fetch()) {
            $labels[] = $row['label'];
        }
        return $labels;
    }


    /**
     * デバイスの一覧を返す
     * @param int $nodeId ノードID
     * @return array
     */
    public function findDevices($nodeId) {
        $nodeId = (int)$nodeId;
        $sql = "SELECT id, type FROM devices WHERE node_id={$nodeId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $devices = array();
        while($row = $this->db->Fetch()) {
            $devices[] = $row;
        }
        return $devices;
    }


    /**
     * ノードのアクションの一覧を返す
     * @param int $nodeId ノードID
     * @return array
     */
    public function findActions($nodeId) {
        $nodeId = (int)$nodeId;
        $sql = "SELECT id, node_id, type, description FROM node_actions WHERE node_id={$nodeId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $actions = array();
        while($row = $this->db->Fetch()) {
            $actions[] = $row;
        }
        return $actions;
    }

    /**
     * ノード共有しているユーザーの一覧を返す
     * @param int $nodeId ノードID
     * @return array usersのレコードを含んだ配列
     */
    public function findSharedUsers($nodeId) {
        $nodeId = (int)$nodeId;
        $sql = "SELECT users.id, users.login_id, users.email,  "
            . "        users.name, users.platform, users.token, "
            . "        users.created, users.updated "
            . "   FROM nodes_users "
            . "  INNER JOIN users ON(users.id=nodes_users.user_id) "
            . "  WHERE node_id='{$nodeId}'";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $users = array();
        while($row = $this->db->Fetch()) {
            $users[] = $row;
        }
        return $users;
    }


    /**
     * Nodeが、指定したUserと共有されているかを返す。
     * @param int $nodeId ノードID
     * @param int $userId ユーザーID(users.id)
     * @return bool
     */
    public function isSharedUser($nodeId, $userId) {
        $nodeId = (int)$nodeId;
        $userId = (int)$userId;
        $sharingLevel = NODE_SHARING_LEVEL_USERS;
        $sql = "SELECT nodes_users.user_id "
            . "   FROM nodes_users "
            . "   LEFT OUTER JOIN nodes ON (nodes.id=nodes_users.node_id) "
            . "  WHERE node_id=$nodeId AND user_id={$userId} "
            . "    AND nodes.sharing_level={$sharingLevel}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        $found = $this->db->Fetch();
        return isset($found['user_id']);
    }


    /**
     * 指定したラベルに部分一致するNodeのID一覧を返す
     * @param string $label ラベル文字列
     * @return array
     */
    public function findNodeIdListByLabel($label) {
        $label = $this->db->LikeEscape($label);

        $labelType = LABEL_TYPE_NODE;
        $sql = "SELECT entity_id FROM labels WHERE type={$labelType} AND label LIKE '%{$label}%' ";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $rows = array();
        while($row = $this->db->Fetch()) {
            $rows[] = (int)$row['entity_id'];
        }
        return $rows;
    }



    /**
     * 指定されたNode ID一覧をラベルで曖昧検索し、マッチするNode IDの一覧を返す。
     * @param array $baseIdList 大元のnodes.idの一覧
     * @param string $label ラベル文字列
     * @return array nodes.idの一覧
     */
    public function filterNodeIdListByLabel($baseIdList, $label) {
        if(strlen($label) == 0) {
            return array();
        }
        $type = LABEL_TYPE_NODE;
        $escapedLabel = $this->db->LikeEscape($label);
        $sql = "SELECT entity_id "
            . "   FROM labels "
            . "  WHERE type={$type} AND label LIKE '%{$escapedLabel}%'";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        //IDは数値にキャストする
        foreach($baseIdList as $idx=>$value) {
            $baseIdList[$idx] = (int)$value;
        }

        $matchedIdList = array();
        while($row = $this->db->Fetch()) {
            $matchedIdList[] = (int)$row['entity_id'];
        }
        return array_intersect($baseIdList, $matchedIdList);
    }


    /**
     * ノード情報を登録/更新する
     * @param int $nodeId ノードID
     * @param array $node nodesテーブルのレコードを含んだ配列
     */
    public function save($nodeId, $node) {
        $nodeId = (int)$nodeId;
        $gatewayId = (int)$node['gateway_id'];
        $protocol = $this->db->Escape($node['protocol']);
        $sharingLevel = (int)$node['sharing_level'];
        $locationType = $this->db->Escape($node['location_type']);
        $locationName = $this->db->Escape($node['location_name']);
        $state = (int)$node['state'];
        $failed_cmd = $node['failed_cmd'];
        $reason = $node['reason'];
        $created = date('Y-m-d H:i:s');
        $join_key_set = !isset($node['join_key_set']) || !$node['join_key_set'] ? 'null' : $node['join_key_set'];

        $sql = "INSERT INTO nodes SET id={$nodeId}, "
             . " gateway_id={$gatewayId}, "
             . " protocol='{$protocol}', "
             . " sharing_level={$sharingLevel}, "
             . " location_type='{$locationType}', "
             . " location_name='{$locationName}', "
             . " state={$state}, "
             . " failed_cmd='{$failed_cmd}', "
             . " reason='{$reason}', "
             . " join_key_set={$join_key_set}, "
             . " created='{$created}' "
             . " ON DUPLICATE KEY UPDATE "
             . " gateway_id={$gatewayId}, "
             . " protocol='{$protocol}', "
             . " sharing_level={$sharingLevel}, "
             . " location_type='{$locationType}', "
             . " location_name='{$locationName}', "
             . " state={$state}, "
             . " failed_cmd='{$failed_cmd}', "
             . " reason='{$reason}', "
             . " join_key_set={$join_key_set}, "
             . " updated='{$created}' "
        ;
        $this->db->setSql($sql);

        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }

    /**
     * ノードのsharing_levelを更新する
     * @param int $nodeId ノードID
     * @param int $sharingLevel NODE_SHARING_LEVEL_XXX定数
     */
    public function updateSharingLevel($nodeId, $sharingLevel) {
        $nodeId = (int)$nodeId;
        $sharingLevel = (int)$sharingLevel;
        $updated = date('Y-m-d H:i:s');

        $sql = "UPDATE nodes SET sharing_level={$sharingLevel}, updated='{$updated}' "
             . " WHERE id={$nodeId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }

    /**
     * ノード共有の対象ユーザーを更新する
     * @param int $nodeId ノードID
     * @param array $userIds ユーザーID(users.id)の配列
     */
    public function updateSharedUsers($nodeId, $userIds) {
        $nodeId = (int)$nodeId;
        $deleteSql = "DELETE FROM nodes_users WHERE node_id={$nodeId}";
        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$deleteSql}, Error: {$error}");
        }

        $created = date('Y-m-d H:i:s');
        $insertValueList = array();
        foreach((array)$userIds as $userId) {
            $insertValueList[] = sprintf("(%d, %d, '%s')", $nodeId, $userId, $created);
        }

        if(count($insertValueList) == 0) {
            return;
        }

        $joinedInsertValues = implode(',', $insertValueList);
        $sql = "INSERT INTO nodes_users(node_id, user_id, created) "
             . " VALUES " . $joinedInsertValues;
        self::saveSharedUserNodeTiles($joinedInsertValues);
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }


    /**
     * node_nameを構成するmanufacturer,product,serial_noを更新する。
     * @param int $nodeId ノードID
     * @param string $manufacturer
     * @param string $product
     * @param string $serialNo
     */
    public function updateNodeNameElement($nodeId, $manufacturer, $product, $serialNo) {
        $nodeId = (int)$nodeId;
        $manufacturer = $this->db->Escape($manufacturer);
        $product = $this->db->Escape($product);
        $serialNo = $this->db->Escape($serialNo);
        $updated = date('Y-m-d H:i:s');

        $sql = "UPDATE nodes SET manufacturer='{$manufacturer}', product='{$product}', serial_no='{$serialNo}', updated='{$updated}' "
             . " WHERE id={$nodeId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }

    /**
     * ノードのactionを更新する
     * @param int $nodeId ノードID
     * @param array $actions node_actionsのレコードを含んだ配列
     */
    public function updateActions($nodeId, $actions) {
        $nodeId = (int)$nodeId;
        $deleteSql = "DELETE FROM node_actions WHERE node_id={$nodeId}";
        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$deleteSql}, Error: {$error}");
        }

        foreach((array)$actions as $action) {
            $created = date('Y-m-d H:i:s');
            $actionType = (int)$action['type'];
            $description = $this->db->Escape($action['description']);

            $sql = "INSERT INTO node_actions SET "
                 . " node_id={$nodeId}, "
                 . " type={$actionType}, "
                 . " description='{$description}', "
                 . " created='{$created}' ";

            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("sql: {$sql}, Error: {$error}");
            }
        }
    }

    /**
     * 指定されたゲートウェイ配下のノードを全て解除する(Node Exclusion)
     * @param int $gatewayId ゲートウェイID
     */
    public function clearGatewayAssociation($gatewayId) {
        $gatewayId = (int)$gatewayId;
        $updated = date('Y-m-d H:i:s');

        //再度Node inclusionが実行された場合に備え、各NodeのGatewayに紐付くUserのIDを保存しておく。
        //後でNode Inclusionが実行された際、Exclusion前のユーザーと同じユーザーか判定を可能にするため。
        $prevUserSql = "SELECT user_id FROM gateways "
                     . " WHERE id={$gatewayId}";

        $this->db->setSql($prevUserSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$prevUserSql}, Error: {$error}");
        }

        $prevUserInfo = $this->db->Fetch();
        $prevUserId = (isset($prevUserInfo['user_id'])) ? $prevUserInfo['user_id'] : 0;
        $reason = 'user_gw_dissoc';

        $sql = "UPDATE nodes SET prev_user_id={$prevUserId}, gateway_id=0, reason='{$reason}', updated='{$updated}' WHERE gateway_id={$gatewayId}";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }


    /**
     * 指定されたノードのNode exclusionを実行する。
     * @param int $nodeId ノードID
     */
    public function excludeNode($nodeId) {
        $nodeId = (int)$nodeId;
        $updated = date('Y-m-d H:i:s');

        //再度Node inclusionが実行された場合に備え、各NodeのGatewayに紐付くUserのIDを保存しておく。
        //後でNode Inclusionが実行された際、Exclusion前のユーザーと同じユーザーか判定を可能にするため。
        $prevUserSql = "SELECT gateways.user_id FROM nodes "
            . " INNER JOIN gateways ON (gateways.id=nodes.gateway_id)"
            . " WHERE nodes.id={$nodeId}";

        $this->db->setSql($prevUserSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$prevUserSql}, Error: {$error}");
        }

        $prevUserInfo = $this->db->Fetch();
        $prevUserId = (isset($prevUserInfo['user_id'])) ? $prevUserInfo['user_id'] : 0;
        $reason = 'node_excl';

        $sql = "UPDATE nodes SET prev_user_id={$prevUserId}, gateway_id=0, reason='{$reason}', updated='{$updated}' WHERE id={$nodeId}";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }


    /**
     * ラベルを更新する
     * @param int $nodeId ノードID
     * @param int $userId ユーザー単位設定ではなければ null を指定します。
     * @param array $labels ラベル文字列を含んだ配列
     */
    public function updateLabels($nodeId, $userId, $labels) {
        $nodeId = (int)$nodeId;
        $userId = is_null($userId) ? null : (int)$userId;

        $this->removeLabels($nodeId, $userId);

        $labelType = LABEL_TYPE_NODE;
        foreach((array)$labels as $label) {
            $created = date('Y-m-d H:i:s');
            $label = $this->db->Escape($label);
            if (is_null($userId)) {
            	$sql = "INSERT INTO labels SET type={$labelType}, entity_id={$nodeId}, label='{$label}', created='{$created}'";
            } else {
            	$sql = "INSERT INTO labels SET type={$labelType}, entity_id={$nodeId}, user_id={$userId}, label='{$label}', created='{$created}'";
            }
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("sql: {$sql}, Error: {$error}");
            }
        }

    }

    /**
     * ラベルを全て削除する
     * @param int $nodeId ノードID
     */
    public function removeAllLabels($nodeId) {
        $nodeId = (int)$nodeId;
        $labelType = LABEL_TYPE_NODE;
        $deleteSql = "DELETE FROM labels WHERE type={$labelType} AND entity_id={$nodeId} ";

        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$deleteSql}, Error: {$error}");
        }
    }

    public function removeLabels($nodeId, $userId = null) {
    	$nodeId = (int)$nodeId;
    	$labelType = LABEL_TYPE_NODE;
    	$deleteSql = "DELETE FROM labels WHERE type={$labelType} AND entity_id={$nodeId} ";
    	if (!is_null($userId)) {
    		$deleteSql .= " AND user_id={$userId}";
    	} else {
    		$deleteSql .= " AND user_id IS NULL";
    	}
    
    	$this->db->setSql($deleteSql);
    	if(!$this->db->Query()) {
    		$error = $this->db->getErr();
    		throw new IOException("sql: {$deleteSql}, Error: {$error}");
    	}
    }
    
    /**
     * ユーザーのラベルを全ての削除する
     * @param int $userId ユーザーID
     */
    public function removeAllUserLabels($userId) {
    	$userId = (int)$userId;
    	$labelType = LABEL_TYPE_NODE;
    	$deleteSql = "DELETE FROM labels WHERE type={$labelType} AND user_id={$userId} ";
    
    	$this->db->setSql($deleteSql);
    	if(!$this->db->Query()) {
    		$error = $this->db->getErr();
    		throw new IOException("sql: {$deleteSql}, Error: {$error}");
    	}
    }
    
    /**
     * NodeのActionのタイプコード、Action名を提議した配列を返す
     * @return array
     */
    public static function getActionTypeMapping() {
        return array(
            NODE_ACTION_TYPE_BINARY  => 'binary',
            NODE_ACTION_TYPE_RANGE   => 'range',
            NODE_ACTION_TYPE_TRIGGER => 'trigger',
        );
    }

    /**
     * NodeのActionのタイプコード->Action名の変換を行う
     * @param int $type NODE_ACTION_TYPE_XXX定数
     * @return mixed Action名、見つからない場合はfalse
     */
    public static function getActionNameFromType($type) {
        $mapping = self::getActionTypeMapping();
        if(isset($mapping[$type])) {
            return $mapping[$type];
        }
        return false;
    }

    /**
     * NodeのActionのAction名->タイプコードの変換を行う
     * @param string $typeName Action名
     * @return mixed NODE_ACTION_TYPE_XXX定数、見つからない場合はfalse
     */
    public static function getActionTypeFromName($typeName) {
        $mapping = self::getActionTypeMapping();
        return array_search($typeName, $mapping);
    }


    /**
     * Nodeのsharing_levelの名前、タイプコードを定義した配列を返す
     * @return array
     */
    public static function getSharingLevelTypeMapping() {
        return array(
            NODE_SHARING_LEVEL_PRIVATE => 'private',
            NODE_SHARING_LEVEL_USERS   => 'users',
        );
    }

    /**
     * Nodeのsharing_levelのタイプコード->名前の変換を行う
     * @param int $type NODE_SHARING_LEVEL_XXX定数
     * @return mixed 名前、見つからない場合はfalse
     */
    public static function getSharingLevelNameFromType($type) {
        $mapping = self::getSharingLevelTypeMapping();
        if(isset($mapping[$type])) {
            return $mapping[$type];
        }
        return false;
    }

    /**
     * Nodeのsharing_levelの名前->タイプコードの変換を行う
     * @param string $typeName 名前
     * @return mixed NODE_SHARING_LEVEL_XXX定数、見つからない場合はfalse
     */
    public static function getSharingLevelTypeFromName($typeName) {
        $mapping = self::getSharingLevelTypeMapping();
        return array_search($typeName, $mapping);
    }

    /**
     * Nodeの state の名前、タイプコードを定義した配列を返す
     * @return array
     */
    public static function getNodeStateTypeMapping() {
    	return array(
    			NODE_STATE_NOTINCLUDED   => 'notincluded',
    			NODE_STATE_INCLUDED      => 'included',
    			NODE_STATE_FAILED        => 'failed',
    	);
    }
    
    /**
     * Nodeの state のタイプコード->名前の変換を行う
     * @param int $type NODE_STATE_XXX定数
     * @return mixed 名前、見つからない場合はfalse
     */
    public static function getNodeStateNameFromType($type) {
    	$mapping = self::getNodeStateTypeMapping();
    	if(isset($mapping[$type])) {
    		return $mapping[$type];
    	}
    	return false;
    }
    
    /**
     * Nodeの state の名前->タイプコードの変換を行う
     * @param string $typeName 名前
     * @return mixed NODE_STATE_XXX定数、見つからない場合はfalse
     */
    public static function getNodeStateTypeFromName($typeName) {
    	$mapping = self::getNodeStateTypeMapping();
    	return array_search($typeName, $mapping);
    }

    /**
     * Nodeの state の名前、タイプコードを定義した配列を返す
     * @return array
     */
    public static function getNodeYetAnotherStateTypeMapping() {
    	return array(
    			NODE_YET_ANOTHER_STATE_UNKNOWN   => 'unknown',
    			NODE_YET_ANOTHER_STATE_OPERATIONAL   => 'operational',
    			NODE_YET_ANOTHER_STATE_LOST   => 'lost',
    			NODE_YET_ANOTHER_STATE_NEGOTIATING   => 'negotiating',
    	);
    }
    
    /**
     * Nodeの state のタイプコード->名前の変換を行う
     * @param int $type NODE_STATE_XXX定数
     * @return mixed 名前、見つからない場合はfalse
     */
    public static function getNodeYetAnotherStateNameFromType($type) {
    	$mapping = self::getNodeYetAnotherStateTypeMapping();
    	if(isset($mapping[$type])) {
    		return $mapping[$type];
    	}
    	return false;
    }
    
    /**
     * Nodeの state の名前->タイプコードの変換を行う
     * @param string $typeName 名前
     * @return mixed NODE_STATE_XXX定数、見つからない場合はfalse
     */
    public static function getNodeYetAnotherStateTypeFromName($typeName) {
    	$mapping = self::getNodeYetAnotherStateTypeMapping();
    	return array_search($typeName, $mapping);
    }
    
    /**
     * プロトコルのIDに対応する名前を返す
     * @param int $id プロトコルのタイプコード
     * @return mixed プロトコル名、見つからない場合はfalse
     */
    public static function getProtocolNameById($id) {
        switch($id) {
            case NODE_PROTOCOL_ZWAVE: return 'zwave';
        }
        return false;
    }
    
    public function saveNodeConfig($content, $statusCode) {
		$updatePending = 0;
		if (!is_null($statusCode) && $statusCode == 300) {
			$updatePending = 1;
		}
    	$statusCode = (int)$statusCode;
		$now = DateHelper::getDateStringWithMicrotime();
		
		// 既存の設定の存在確認
		$sql = "SELECT id FROM node_configs "
			. " WHERE "
			. " node_id='{$content['node_id']}' "
			. " AND config_type='{$content['config_type']}' "
			. " AND param_id='{$content['param_id']}' ";
		$this->db->setSql( $sql );
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException( "sql: {$sql}, Error: {$error}" );
		}
		$update = 0 < $this->db->Num_rows();
		if($update) {
			$targetRow = $this->db->Fetch();
			$id = (int)$targetRow['id'];
			
			$sql = "UPDATE node_configs SET "
				. " value='{$content['value']}', "
				. " update_pending={$updatePending}, "
				. " updated='{$now}' "
				. " WHERE id={$id} ";
		} else {
			$sql = "INSERT INTO node_configs SET "
				. " node_id='{$content['node_id']}', "
				. " config_type='{$content['config_type']}', "
				. " param_type='{$content['param_type']}', ";
			if (isset($content['param_size'])) {
				$sql .= " param_size={$content['param_size']}, ";
			}
			$sql .= " param_id='{$content['param_id']}', "
				. " value='{$content['value']}', "
				. " update_pending={$updatePending}, "
				. " created='{$now}' ";
		}
		$this->db->setSql( $sql );
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException( "sql: {$sql}, Error: {$error}" );
		}
    }


    public function wipePreviousAppSpecificNodeConfig($nodeId) {
        $sql = "delete from nodes_apps where node_id='{$nodeId}'";

 		$this->db->setSql( $sql );

		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException( "sql: {$sql}, Error: {$error}" );
		}
    }

    public function saveAppSpecificNodeConfig($nodeId, $config) {
        $this->db->Begin();

        // todo: should app names be checked so that there are no duplicate names?

        // update nodes_apps table first
        $now = DateHelper::getDateStringWithMicrotime();
        $sql = "insert into nodes_apps (node_id, app_name, created) values ("
            . "'{$nodeId}', "
            . "'{$config['app_name']}', "
            . "'{$now}');";

		$this->db->setSql( $sql );

		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException( "sql: {$sql}, Error: {$error}" );
		}

        // get the inserted apps id
        $sql = "SELECT LAST_INSERT_ID();";

		$this->db->setSql( $sql );
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException( "sql: {$sql}, Error: {$error}" );
		}
        $app_id = $this->db->Fetch()["LAST_INSERT_ID()"];

        // then update apps_params table
        foreach ($config['configurations'] as $param_pair) {

            // todo: should param names be checked so that there are no duplicate names?

            $now = DateHelper::getDateStringWithMicrotime();
            $param_value = json_encode($param_pair['param_value']);
            $sql = "insert into apps_params (app_id, param_name, param_value, created) values ('{$app_id}', '{$param_pair['param_name']}', '{$param_value}', '{$now}');";

            $this->db->setSql( $sql );

            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException( "sql: {$sql}, Error: {$error}" );
            }
        }

        $this->db->Commit();
    }
    
    public function findNodeConfigs($nodeId) {
    	$nodeId = (int)$nodeId;
    	
    	$zwave_configurations = array();
    	$configurations = array();
    	
		$sql = "SELECT "
			. " id, node_id, config_type, param_type, param_size, param_id, value, update_pending, created, updated "
			. " FROM "
			. " node_configs "
			. " WHERE "
			. " node_id={$nodeId} ";
		$this->db->setSql( $sql );
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException( "sql: {$sql}, Error: {$error}" );
		}
		while($row = $this->db->Fetch()) {
			$config = array();
			
			if (strcmp($row['config_type'], 'zwave') === 0) {
				
				$config['parameter_no'] = (int)$row['param_id'];
				$config['type'] = $row['param_type'];
				if(strcmp( $row['param_type'], 'unsigned int' ) === 0) {
					$config['value'] = (int)$row['value'];
				} else {
					$config['value'] = (string)$row['value'];
				}
				$config['param_size'] = (int)$row['param_size'];
				$config['value'] = (int)$row['value'];
				$config['update_pending'] = (int)$row['update_pending'] === 1 ? true : false;
				
				$zwave_configurations[] = $config;
			} else if(strcmp( $row['config_type'], 'other' ) === 0) {
				
				$config['type'] = $row['param_type'];
				$config['parameter'] = (string)$row['param_id'];
				if(strcmp( $row['param_type'], 'signed int' ) === 0) {
					$config['value'] = (int)$row['value'];
				} elseif(strcmp( $row['param_type'], 'unsigned int' ) === 0) {
					$config['value'] = (int)$row['value'];
				} else {
					$config['value'] = (string)$row['value'];
				}
				$config['update_pending'] = (int)$row['update_pending'] === 1 ? true : false;
				
				$configurations[] = $config;
			}
		}
		
		$result = array(
			'zwave_configurations' => $zwave_configurations,
			'configurations' => $configurations
		);
		return $result;
    }

    public function findNodeAppConfigs($nodeId) {
    	$nodeId = (int)$nodeId;
        
		$sql = "select nodes_apps.app_id, param_id, app_name, param_name, param_value from nodes_apps "
            ."inner join apps_params on nodes_apps.app_id = apps_params.app_id "
            ."where node_id = {$nodeId};";

		$this->db->setSql( $sql );
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException( "sql: {$sql}, Error: {$error}" );
		}

		$result = [];
        
		while($row = $this->db->Fetch()) {
            $app_id = $row['app_id'];

            // for the first time, save app name and build configurations skeleton
            if (!array_key_exists($app_id, $result)) {
                $result[$app_id] = [
                    "app_name" => $row["app_name"],
                    "configurations" => []
                ];
            } 

            // and then push parameter pairs into configurations
            $result[$app_id]["configurations"][] = [
                "param_name" => $row["param_name"],
                "param_value" => json_decode($row["param_value"], true),
            ];
		}

        $formatted_result = [];
        foreach ($result as $newRow) {
            $formatted_result[] = $newRow;
        }

		return $formatted_result;
    }

    public function saveUserNodeTiles($nodeId, $userId){
       
        $sql = "INSERT INTO node_tiles SET node_id={$nodeId},"
        . "user_id={$userId},"
        . "tile_status=0"
        ;
    
        $this->db->setSql( $sql );
		if(!$this->db->Query()) {
            $error = $this->db->getErr();
			throw new IOException( "sql: {$sql}, Error: {$error}" );
		}

    }

    public function saveSharedUserNodeTiles($nodeId){

        $sql = "INSERT INTO node_tiles(node_id, user_id) "
        . " VALUES " . $nodeId;

        $this->db->setSql( $sql );
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException( "sql: {$sql}, Error: {$error}" );
		}

    }

}
