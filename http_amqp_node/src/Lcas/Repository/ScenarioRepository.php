<?php

/**
 * @file
 * @brief シナリオのDBへのアクセスを行うオブジェクト
 */

namespace Lcas\Repository;



use Lcas\Exception\IOException;
use Lcas\Helper\DateHelper;

class ScenarioRepository extends BaseRepository {


    /**
     * 指定されたシナリオへのアクセス権があるかを返す。
     * @param int $scenarioId scenarios.id
     * @param int $userId users.id
     * @return bool
     */
    public function isGrantedUser($scenarioId, $userId) {
        $scenarioId = (int)$scenarioId;
        $userId = (int)$userId;

        $sql = "SELECT id FROM scenarios WHERE id={$scenarioId} AND user_id={$userId} ";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        $row = $this->db->Fetch();
        return isset($row['id']);
    }


    /**
     * 指定したユーザーIDのScenarioの一覧を返す。
     * @param int $scenarioId users.id
     * @return array
     */
    public function findById($scenarioId, $includeAutoConfig=false) {
        $sql = "SELECT id, user_id, scenario_type, condition_operator, "
             . "       temperature_dev, humidity_dev, wbgt_threshold, "
             . "       scxml, device_list, action_state_list, "
             . "       trigger_after, enabled, enabled_period, enabled_days, "
             . "       auto_config, "
             . "       created, updated "
             . "  FROM scenarios "
             . " WHERE id={$scenarioId} ";
        if (!$includeAutoConfig) {
        	$sql .= " AND auto_config=0 ";
        }
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        return $this->db->Fetch();
    }


    /**
     * 指定したユーザーIDのScenarioの一覧を返す。
     * @param int $userId users.id
     * @return array
     */
    public function findByUserId($userId, $includeAutoConfig=false) {
        $sql = "SELECT id, user_id, scenario_type, condition_operator, "
             . "       temperature_dev, humidity_dev, wbgt_threshold, "
             . "       scxml, device_list, action_state_list, "
             . "       trigger_after, enabled, enabled_period, enabled_days, "
             . "       auto_config, "
             . "       created, updated, tile_status "
             . "  FROM scenarios "
             . " WHERE user_id={$userId}";
		if(!$includeAutoConfig) {
			$sql .= " AND auto_config=0 ";
		}
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $rows = array();
        while($row = $this->db->Fetch()) {
            $rows[] = $row;
        }
        return $rows;
    }


    /**
     * 指定されたシナリオのシナリオ条件を返す
     * @param int $scenarioId シナリオID
     * @return array
     */
    public function findConditions($scenarioId) {
        $sql = "SELECT id, scenario_id, lhs, operator, rhs, unit, "
            . "        created "
            . "  FROM scenario_conditions "
            . " WHERE scenario_id={$scenarioId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $rows = array();
        while($row = $this->db->Fetch()) {
            $rows[] = $row;
        }
        return $rows;
    }


    public function findIdsByLhs($lhs) {
        $sql = "SELECT distinct scenario_id "
            . "  FROM scenario_conditions "
            . " WHERE lhs='{$lhs}'";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $rows = array();
        while($row = $this->db->Fetch()) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * 指定されたシナリオのアクションの情報を返す
     * @param int $scenarioId シナリオID
     * @return array
     */
    public function findActions($scenarioId) {
        $sql = "SELECT id, scenario_id, type, content, "
            . "        created "
            . "  FROM scenario_actions "
            . " WHERE scenario_id={$scenarioId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $rows = array();
        while($row = $this->db->Fetch()) {
            $rows[] = $row;
        }
        return $rows;
    }


    /**
     * ラベルの一覧を返す。
     * @param int $scenarioId シナリオID
     * @return array
     */
    public function findLabels($scenarioId) {
        $scenarioId = (int)$scenarioId;
        $labelType = LABEL_TYPE_SCENARIO;
        $sql = "SELECT label FROM labels WHERE type={$labelType} AND entity_id={$scenarioId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $labels = array();
        while($row = $this->db->Fetch()) {
            $labels[] = $row['label'];
        }
        return $labels;
    }


    /**
     * 指定したラベルに部分一致するシナリオのID一覧を返す
     * @param string $label ラベル文字列
     * @return array シナリオIDを含んだ配列
     */
    public function findScenarioIdListByLabel($label) {
        $label = $this->db->LikeEscape($label);

        $labelType = LABEL_TYPE_SCENARIO;
        $sql = "SELECT entity_id FROM labels WHERE type={$labelType} AND label LIKE '%{$label}%' ";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $rows = array();
        while($row = $this->db->Fetch()) {
            $rows[] = (int)$row['entity_id'];
        }
        return $rows;
    }


    /**
     * 指定されたシナリオのイベント一覧を返す。
     * @param int $scenarioId シナリオID
     * @param array $options 検索オプション
     */
    public function findEvents($scenarioId, $options=array()) {
        $scenarioId = (int)$scenarioId;

        $sql = "SELECT scenario_events.id, scenario_events.scenario_id, "
             . "       scenario_events.message, scenario_events.created "
             . "  FROM scenario_events "
             . " WHERE scenario_id={$scenarioId} ";

        $conditions = array();
        if(isset($options['from'])) {
            $from = $this->db->Escape($options['from']);
            $conditions[] = " scenario_events.created >= '{$from}' ";
        }
        if(isset($options['to'])) {
            $to = $this->db->Escape($options['to']);
            $conditions[] = " scenario_events.created <= '{$to}' ";
        }

        if(count($conditions) > 0) {
            $sql .= ' AND ' . implode(' AND ', $conditions);
        }

        $sql .= " ORDER BY created DESC ";

        if(isset($options['limit'])) {
            $sql .= " LIMIT " . $options['limit'] . " ";
        }
        //MySQLの場合、LIMIT句付きでなければOFFSET指定出来ない
        if(isset($options['limit']) && isset($options['offset'])) {
            $sql .= " OFFSET " . $options['offset'] . " ";
        }

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $rows = array();
        $rowCount = 0;
        while($row = $this->db->Fetch()) {
            //MySQLの場合、LIMIT句付きでなければOFFSET指定出来ないため、
            //LIMIT句なしでOFFSET指定された場合、結果セットの上位$offset件をスキップして対応する
            if(!isset($options['limit']) && isset($options['offset'])) {
                if($rowCount++ < $options['offset']) {
                    continue;
                }
            }
            $rows[] = $row;
        }
        return $rows;
    }


    /**
     * 指定されたユーザーのイベント一覧を返す。
     * @param int $userId users.id
     * @param array $options 検索オプション
     *
     *   - from: 開始日時
     *   - to: 終了日時
     *   - limit: 件数制限
     *   - offset: 取得開始位置
     */
    public function findEventsByUserId($userId, $options=array()) {
        $userId = (int)$userId;

        $sql = "SELECT scenario_events.id, scenario_events.scenario_id, "
             . "       scenario_events.message, scenario_events.created "
             . "  FROM scenario_events "
             . " INNER JOIN scenarios ON (scenarios.id=scenario_events.scenario_id) "
             . " WHERE scenarios.user_id={$userId} ";

        $conditions = array();

        if(isset($options['from'])) {
            $from = $this->db->Escape($options['from']);
            $conditions[] = " scenario_events.created >= '{$from}' ";
        }
        if(isset($options['to'])) {
            $to = $this->db->Escape($options['to']);
            $conditions[] = " scenario_events.created <= '{$to}' ";
        }

        if(count($conditions) > 0) {
            $sql .= ' AND ' . implode(' AND ', $conditions);
        }

        $sql .= " ORDER BY created DESC ";

        if(isset($options['limit'])) {
            $sql .= " LIMIT " . $options['limit'] . " ";
        }
        //MySQLの場合、LIMIT句付きでなければOFFSET指定出来ない
        if(isset($options['limit']) && isset($options['offset'])) {
            $sql .= " OFFSET " . $options['offset'] . " ";
        }

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $rows = array();
        $rowCount = 0;
        while($row = $this->db->Fetch()) {
            //MySQLの場合、LIMIT句付きでなければOFFSET指定出来ないため、
            //LIMIT句なしでOFFSET指定された場合、結果セットの上位$offset件をスキップして対応する
            if(!isset($options['limit']) && isset($options['offset'])) {
                if($rowCount++ < $options['offset']) {
                    continue;
                }
            }
            $rows[] = $row;
        }
        return $rows;
    }


    /**
     * Scenarioの登録を行う
     * @param array $scenario scenariosテーブルのレコードを含んだ連想配列
     * @return int 新規登録したScenarioのID
     */
    public function registerScenario($scenario) {

        $userId = (int)$scenario['user_id'];
        $scenarioType = (int)$scenario['scenario_type'];
        if (isset($scenario['condition_operator'])) {
        	$conditionOperator = (int)$scenario['condition_operator'];
        }
        if (isset($scenario['trigger_after'])) {
        	$triggerAfter = (int)$scenario['trigger_after'];
        }
        if (isset($scenario['temperature_dev'])) {
        	$temperatureDev = (int)$scenario['temperature_dev'];
        }
        if (isset($scenario['humidity_dev'])) {
        	$humidityDev = (int)$scenario['humidity_dev'];
        }
        if (isset($scenario['wbgt_threshold'])) {
        	$wbgtThreshold = (int)$scenario['wbgt_threshold'];
        }
        if (isset($scenario['scxml'])) {
        	$scxml = $scenario['scxml'];
        }
        if (isset($scenario['device_list'])) {
        	$deviceList = $scenario['device_list'];
        }
        if (isset($scenario['action_state_list'])) {
        	$actionStateList = $scenario['action_state_list'];
        }
        $enabled = (int)$scenario['enabled'];
        if (isset($scenario['enabled_period'])) {
        	$enabledPeriod = $scenario['enabled_period'];
        }
        if (isset($scenario['enabled_days'])) {
        	$enabledDays = $scenario['enabled_days'];
        }
        $autoConfig = (int)$scenario['auto_config'];
        $created = date('Y-m-d H:i:s');

        $sql = "INSERT INTO scenarios SET "
             . " user_id={$userId}, "
             . " scenario_type={$scenarioType}, ";
        if (isset($scenario['condition_operator'])) {
            $sql .= " condition_operator={$conditionOperator}, ";
        }
        if (isset($scenario['trigger_after'])) {
        	$sql .= " trigger_after={$triggerAfter}, ";
        }
        if (isset($scenario['temperature_dev'])) {
        	$sql .= " temperature_dev={$temperatureDev}, ";
        }
        if (isset($scenario['humidity_dev'])) {
        	$sql .= " humidity_dev={$humidityDev}, ";
        }
        if (isset($scenario['wbgt_threshold'])) {
        	$sql .= " wbgt_threshold={$wbgtThreshold}, ";
        }
        if (isset($scenario['scxml'])) {
        	$sql .= " scxml='{$scxml}', ";
        }
        if (isset($scenario['device_list'])) {
        	$sql .= " device_list='{$deviceList}', ";
        }
        if (isset($scenario['action_state_list'])) {
        	$sql .= " action_state_list='{$actionStateList}', ";
        }
        $sql .= " enabled={$enabled}, ";
        if (isset($scenario['enabled_period'])) {
            $sql .= " enabled_period='{$enabledPeriod}', ";
        }
        if (isset($scenario['enabled_days'])) {
        	$sql .= " enabled_days='{$enabledDays}', ";
        }
        $sql .= " auto_config={$autoConfig}, ";
        $sql .= " created='{$created}' ";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        return $this->db->Insert_id();
    }


    /**
     * Scenarioを更新する。
     * @param int $scenarioId シナリオID
     * @param array $scenario scenariosテーブルのレコードを含んだ連想配列
     * @return void
     */
    public function updateScenario($scenarioId, $scenario) {
        $scenarioId = (int)$scenarioId;
        $userId = (int)$scenario['user_id'];
        $scenarioType = (int)$scenario['scenario_type'];
        if (isset($scenario['condition_operator'])) {
        	$conditionOperator = (int)$scenario['condition_operator'];
        }
        if (isset($scenario['trigger_after'])) {
        	$triggerAfter = (int)$scenario['trigger_after'];
        }
        if (isset($scenario['temperature_dev'])) {
        	$temperatureDev = (int)$scenario['temperature_dev'];
        }
        if (isset($scenario['humidity_dev'])) {
        	$humidityDev = (int)$scenario['humidity_dev'];
        }
        if (isset($scenario['wbgt_threshold'])) {
        	$wbgtThreshold = (int)$scenario['wbgt_threshold'];
        }
        if (isset($scenario['scxml'])) {
        	$scxml = $scenario['scxml'];
        }
        if (isset($scenario['device_list'])) {
        	$deviceList = $scenario['device_list'];
        }
        if (isset($scenario['action_state_list'])) {
        	$actionStateList = $scenario['action_state_list'];
        }
        $enabled = (int)$scenario['enabled'];
        if (isset($scenario['enabled_period'])) {
        	$enabledPeriod = $scenario['enabled_period'];
        }
        if (isset($scenario['enabled_days'])) {
        	$enabledDays = $scenario['enabled_days'];
        }
        $autoConfig = (int)$scenario['auto_config'];
        $updated = date('Y-m-d H:i:s');

        $sql = "INSERT INTO scenarios SET "
             . " id={$scenarioId}, "
             . " user_id={$userId}, "
             . " scenario_type={$scenarioType}, ";
        if (isset($scenario['condition_operator'])) {
            $sql .= " condition_operator={$conditionOperator}, ";
        }
        if (isset($scenario['trigger_after'])) {
        	$sql .= " trigger_after={$triggerAfter}, ";
        }
        if (isset($scenario['temperature_dev'])) {
        	$sql .= " temperature_dev={$temperatureDev}, ";
        } else {
        	$sql .= " temperature_dev=null, ";
        }
        if (isset($scenario['humidity_dev'])) {
        	$sql .= " humidity_dev={$humidityDev}, ";
        } else {
        	$sql .= " humidity_dev=null, ";
        }
        if (isset($scenario['wbgt_threshold'])) {
        	$sql .= " wbgt_threshold={$wbgtThreshold}, ";
        } else {
        	$sql .= " wbgt_threshold=null, ";
        }
        if (isset($scenario['scxml'])) {
        	$sql .= " scxml='{$scxml}', ";
        } else {
        	$sql .= " scxml=null, ";
        }
        if (isset($scenario['device_list'])) {
        	$sql .= " device_list='{$deviceList}', ";
        } else {
        	$sql .= " device_list=null, ";
        }
        if (isset($scenario['action_state_list'])) {
        	$sql .= " action_state_list='{$actionStateList}', ";
        } else {
        	$sql .= " action_state_list=null, ";
        }
        if (isset($scenario['enabled_period'])) {
            $sql .= " enabled_period='{$enabledPeriod}', ";
        } else {
        	$sql .= " enabled_period=null, ";
        }
        if (isset($scenario['enabled_days'])) {
        	$sql .= " enabled_days='{$enabledDays}', ";
        } else {
        	$sql .= " enabled_days=null, ";
        }
        $sql .= " enabled={$enabled}, ";
        $sql .= " auto_config={$autoConfig} ";
        $sql .= " ON DUPLICATE KEY UPDATE "
             . " user_id={$userId}, "
             . " scenario_type={$scenarioType}, ";
		if(isset($scenario['condition_operator'])) {
			$sql .= " condition_operator={$conditionOperator}, ";
		}
		if(isset($scenario['trigger_after'])) {
			$sql .= " trigger_after={$triggerAfter}, ";
		}
		if(isset($scenario['temperature_dev'])) {
			$sql .= " temperature_dev={$temperatureDev}, ";
		} else {
			$sql .= " temperature_dev=null, ";
		}
		if(isset($scenario['humidity_dev'])) {
			$sql .= " humidity_dev={$humidityDev}, ";
		} else {
			$sql .= " humidity_dev=null, ";
		}
		if(isset($scenario['wbgt_threshold'])) {
			$sql .= " wbgt_threshold={$wbgtThreshold}, ";
		} else {
			$sql .= " wbgt_threshold=null, ";
		}
        if (isset($scenario['scxml'])) {
        	$sql .= " scxml='{$scxml}', ";
        } else {
        	$sql .= " scxml=null, ";
        }
        if (isset($scenario['device_list'])) {
        	$sql .= " device_list='{$deviceList}', ";
        } else {
        	$sql .= " device_list=null, ";
        }
        if (isset($scenario['action_state_list'])) {
        	$sql .= " action_state_list='{$actionStateList}', ";
        } else {
        	$sql .= " action_state_list=null, ";
        }
		if(isset($scenario['enabled_period'])) {
			$sql .= " enabled_period='{$enabledPeriod}', ";
		} else {
			$sql .= " enabled_period=null, ";
		}
		if(isset($scenario['enabled_days'])) {
			$sql .= " enabled_days='{$enabledDays}', ";
		} else {
			$sql .= " enabled_days=null, ";
		}
        $sql .= " enabled={$enabled}, "
             . " auto_config={$autoConfig}, "
             . " updated='{$updated}' ";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }


    /**
     * シナリオを削除する
     * @param int $scenarioId シナリオID
     */
    public function removeScenario($scenarioId) {
        $scenarioId = (int)$scenarioId;
        $sql = "DELETE FROM scenarios WHERE id={$scenarioId} ";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }

    /**
     * Scenarioの条件を更新する。
     * @param int $scenarioId シナリオID
     * @param array $conditions scenario_conditionsのレコードを含んだ連想配列
     */
    public function updateConditions($scenarioId, $conditions) {
        $scenarioId = (int)$scenarioId;

        $this->removeAllConditions($scenarioId);

        foreach((array)$conditions as $condition) {
            $lhs = (int)$condition['lhs'];
            $operator = (int)$condition['operator'];
            $rhs = (string)$condition['rhs'];
            $unit = $this->db->Escape($condition['unit']);
            $created = date('Y-m-d H:i:s');

            $sql = "INSERT INTO scenario_conditions SET "
                 . " scenario_id={$scenarioId}, "
                 . " lhs={$lhs}, "
                 . " operator={$operator}, "
                 . " rhs='{$rhs}', "
                 . " unit='{$unit}', "
                 . " created='{$created}' "
            ;
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("sql: {$sql}, Error: {$error}");
            }
        }
    }


    /**
     * 登録されている条件を削除する。
     * @param int $scenarioId シナリオID
     */
    public function removeAllConditions($scenarioId) {
        $scenarioId = (int)$scenarioId;
        $deleteSql = "DELETE FROM scenario_conditions WHERE scenario_id={$scenarioId}";

        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$deleteSql}, Error: {$error}");
        }
    }


    /**
     * Actionを更新する
     * @param int $scenarioId シナリオID
     * @param array $actions scenario_actionsのレコードを含んだ連想配列
     */
    public function updateActions($scenarioId, $actions) {
        $scenarioId = (int)$scenarioId;

        $this->removeAllActions($scenarioId);

        foreach((array)$actions as $action) {
            $type = (int)$action['type'];
            $content = $this->db->Escape($action['content']);
            $created = date('Y-m-d H:i:s');

            $sql = "INSERT INTO scenario_actions SET "
                . " scenario_id={$scenarioId}, "
                . " type={$type}, "
                . " content='{$content}', "
                . " created='{$created}' "
            ;
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("sql: {$sql}, Error: {$error}");
            }
        }
    }


    /**
     * Actionを削除する
     * @param int $scenarioId シナリオID
     */
    public function removeAllActions($scenarioId) {
        $scenarioId = (int)$scenarioId;
        $deleteSql = "DELETE FROM scenario_actions WHERE scenario_id={$scenarioId}";

        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$deleteSql}, Error: {$error}");
        }
    }


    /**
     * ラベルを更新する
     * @param int $scenarioId シナリオID
     * @param array $labels ラベル文字列を含んだ配列
     */
    public function updateLabels($scenarioId, $labels) {
        $scenarioId = (int)$scenarioId;

        $this->removeAllLabels($scenarioId);

        $labelType = LABEL_TYPE_SCENARIO;
        foreach((array)$labels as $label) {
            $created = date('Y-m-d H:i:s');
            $label = $this->db->Escape($label);
            $sql = "INSERT INTO labels SET type={$labelType}, entity_id={$scenarioId}, label='{$label}', created='{$created}'";
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("labelの登録失敗。sql: {$sql}, Error: {$error}");
            }
        }

    }


    /**
     * ラベルを削除する
     * @param int $scenarioId シナリオID
     */
    public function removeAllLabels($scenarioId) {
        $scenarioId = (int)$scenarioId;
        $labelType = LABEL_TYPE_SCENARIO;
        $deleteSql = "DELETE FROM labels WHERE type={$labelType} AND entity_id={$scenarioId}";

        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("labelの削除失敗。sql: {$deleteSql}, Error: {$error}");
        }
    }


    /**
     * 指定されたシナリオのシナリオイベント(お知らせ)を全て削除する
     * @param int $scenarioId シナリオID
     */
    public function removeAllEvents($scenarioId) {
        $scenarioId = (int)$scenarioId;
        $deleteSql = "DELETE FROM scenario_events WHERE scenario_id={$scenarioId}";

        $this->db->setSql($deleteSql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$deleteSql}, Error: {$error}");
        }
    }


    /**
     * 指定期間以前のシナリオイベント(お知らせ)を削除する
     * @param string $startDate 削除の基準日時
     * @param int $limit 削除する最大件数
     * @return int
     */
    public function removeEventBefore($startDate, $limit=0) {

        if(!DateHelper::isValidDateTimeString($startDate)) {
            throw new \InvalidArgumentException('無効な日付けが指定されました。 startDate: ' . $startDate);
        }

        $startDate = $this->db->Escape($startDate);

        $sql = "DELETE FROM scenario_events "
            . " WHERE created <= '{$startDate}' ";

        if($limit > 0) {
            $sql .= ' LIMIT ' . (int)$limit;
        }

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        return $this->db->Affected_rows();
    }


    /**
     * シナリオ種別の名前とタイプコードの定義を返す。
     * @return array
     */
    public static function getScenarioTypeMapping() {
        return array(
            SCENARIO_TYPE_SIMPLE => 'simple',
            SCENARIO_TYPE_WBGT => 'wbgt',
            SCENARIO_TYPE_SCXML => 'scxml'
        );
    }

    /**
     * シナリオ種別のタイプコード->種別名の変換を行う。
     * @param int $type タイプコード
     * @return mixed 種別名。見つからない場合はfalse
     */
    public function getScenarioTypeNameFromType($type) {
        $mapping = self::getScenarioTypeMapping();
        if(isset($mapping[$type])) {
            return $mapping[$type];
        }
        return false;
    }

    /**
     * シナリオ種別の種別名->タイプコードの変換を行う
     * @param string $name 種別名
     * @return mixed タイプコード。見つからない場合はfalse
     */
    public function getScenarioTypeFromName($name) {
        $mapping = self::getScenarioTypeMapping();
        return array_search($name, $mapping);
    }


    /**
     * シナリオ条件の連結方法のタイプコード、名前の定義を返す
     * @return array
     */
    public static function getConditionOperatorTypeMapping() {
        return array(
            SCENARIO_CONDITION_OPERATOR_AND => 'AND',
            SCENARIO_CONDITION_OPERATOR_OR => 'OR',
        );
    }

    /**
     * シナリオ条件の連結方法のタイプコード->名前の変換を行う
     * @param int $type タイプコード
     * @return mixed 名前。見つからない場合はfalse
     */
    public function getConditionOperatorTypeNameFromType($type) {
        $mapping = self::getConditionOperatorTypeMapping();
        if(isset($mapping[$type])) {
            return $mapping[$type];
        }
        return false;
    }

    /**
     * シナリオ条件の連結方法の名前->タイプコードの変換を行う。
     * @param string $name 名前
     * @return mixed タイプコード。見つからない場合はfalse
     */
    public function getConditionOperatorTypeFromName($name) {
        $mapping = self::getConditionOperatorTypeMapping();
        return array_search($name, $mapping);
    }


    /**
     * シナリオ条件の比較演算子の定義を返す
     * @return array
     */
    public static function getOperatorTypeMapping() {
        return array(
            SCENARIO_OPERATOR_LESS       => '<',
            SCENARIO_OPERATOR_LESS_EQ    => '<=',
            SCENARIO_OPERATOR_EQ         => '==',
            SCENARIO_OPERATOR_GREATER    => '>',
            SCENARIO_OPERATOR_GREATER_EQ => '>=',
            SCENARIO_OPERATOR_FREQ       => 'freq',
        );
    }

    /**
     * シナリオ条件の比較演算子のタイプコード->名前の変換を行う
     * @param int $type タイプコード
     * @return mixed 名前。見つからない場合はfalse
     */
    public static function getOperatorTypeNameFromType($type) {
        $mapping = self::getOperatorTypeMapping();
        if(isset($mapping[$type])) {
            return $mapping[$type];
        }
        return false;
    }

    /**
     * シナリオ条件の比較演算子の名前->タイプコードの変換を行う。
     * @param string $name 名前
     * @return mixed タイプコード。見つからない場合はfalse
     */
    public static function getOperatorTypeFromName($name) {
        $mapping = self::getOperatorTypeMapping();
        return array_search($name, $mapping);
    }


    /**
     * シナリオのactionの定義を返す
     * @return array
     */
    public static function getActionTypeMapping() {
        return array(
            SCENARIO_ACTION_TYPE_SEND_MESSAGE => 'send_message',
        );
    }

    /**
     * シナリオのactionのタイプコード->名前の変換を行う
     * @param int $type タイプコード
     * @return mixed 名前。見つからない場合はfalse
     */
    public static function getActionTypeNameFromType($type) {
        $mapping = self::getActionTypeMapping();
        if(isset($mapping[$type])) {
            return $mapping[$type];
        }
        return false;
    }

    /**
     * シナリオのactionの名前->タイプコードの変換を行う
     * @param string $name 名前
     * @return mixed タイプコード。見つからない場合はfalse
     */
    public function getActionTypeFromName($name) {
        $mapping = self::getActionTypeMapping();
        return array_search($name, $mapping);
    }

    /**
     * DB内部で保持しているマイクロ秒精度の日付文字列をミリ秒精度の数値に変換する
     * @param string $microTime MySQLのミリ秒制度の日付を含んだ文字列
     * @return int ミリ秒精度の整数
     *
     * ex. 2015-10-01-01 00:00:00.12345789
     */
    public static function convertMicroTimeToMilliSec($microTime) {
        $split = explode('.', $microTime);
        $dateTime = $split[0];
        $microSec = isset($split[1]) ? $split[1] : 0;

        $milliSec = strtotime($dateTime) * 1000;
        $milliSec += (int)($microSec / 1000);
        return $milliSec;
    }

    /**
     * ミリ秒精度の数値をMySQLが使用するマイクロ秒精度の日付文字列に変換する
     * @param int $milliSec MySQLのミリ秒制度の日付を含んだ文字列
     * @return string マイクロ秒精度の整数
     */
    public static function convertMilliSecToMicroSecString($milliSec) {
        $seconds = (int)($milliSec / 1000);
        $microSeconds = ($milliSec % 1000);

        $microSecondString = sprintf('%s.%03d', date('Y-m-d H:i:s', $seconds), $microSeconds);
        return $microSecondString;
    }
    
    public function isAutoConfiguration($scenarioId) {
    	$scenario = $this->findById($scenarioId, true);
    	if (!$scenario) {
    		throw new IOException('指定されたシナリオが見つかりません。 scenario_id: ' . $scenarioId);
    	}
    	return (int)$scenario['auto_config'] !== 0;
    }

    public function hideTile($scenarioId, $value) {

        $sql = "UPDATE scenarios"
        . " SET tile_status = {$value} "
        . "WHERE id = {$scenarioId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            var_dump($error);
            die;
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        return true;
    }
    
}
