<?php

/**
 * @file
 * @brief デバイスのDBへのアクセスを行うオブジェクト
 */


namespace Lcas\Repository;

use Lcas\DB\DB;
use Lcas\Exception\BadParameterException;
use Lcas\Exception\IOException;

/**
 * @class TileRepository
 * @brief
 */
class TileRepository extends BaseRepository
{
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * 
	 * @param array $tiles
	 * @return bool
	 */
	public function updateTiles($tiles) {
		if(is_array($tiles) !== true) {
			throw new BadParameterException('Invalid parameter tiles passed, must be a valid array.');
		}

		// remove old dashboard tile sequence
		$sql = "DELETE FROM dashboard_tiles WHERE user_id = {$tiles[0]['user_id']}";
		$this->db->setSql($sql);
		$this->db->Query();

		// initialize insert values
		$values = [];

		// build query...
		$sql  = 'INSERT INTO dashboard_tiles ';
		$sql .= sprintf( '(`%s`)', implode("`, `", array_keys($tiles[0])) );

		foreach($tiles as $tile) {
			$values[] = sprintf( "('%s')", implode("', '", $tile) );
		}

		$sql .= ' VALUES ' . implode(',', $values);

		$this->db->setSql($sql);
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException("dashboard_tilesの登録失敗。sql: {$sql}, Error: {$error}");
		}

		return true;
	}

	/**
	 * 
	 * @param array $userId
	 * @return bool
	 */
	public function findUserTiles($userId){

		$sql = "Select * from dashboard_tiles where user_id = {$userId} ORDER BY id ASC";

		$this->db->setSql($sql);
		 if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
		 }

		 $tiles = array();
		 while($row = $this->db->Fetch()) {
			 $tiles[] = $row;
		 }

        return $tiles;

	}

	/**
	 * 
	 * @param array $userId
	 * @return bool
	 */

	public function addUserTiles($userId, $entity_id, $type){

		$maxSequence = "Select MAX(sequence) as max from dashboard_tiles where user_id = {$userId}";
		$this->db->setSql($maxSequence);

		if(!$this->db->Query()) {
			$error = $this->db->getErr();	
            throw new IOException("sql: {$sql}, Error: {$error}");
		 }
		$sequence = (int)$this->db->Fetch()["max"] + 1;
		
		$saveSql = "Insert INTO dashboard_tiles set user_id={$userId},"
		. "entity_id={$entity_id},"
		. "sequence={$sequence},"
		. "type='{$type}'"
		;

		$this->db->setSql($saveSql);
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
		}
		
	}

}