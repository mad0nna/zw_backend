<?php

/**
 * @file
 * @brief ユーザーのDBへのアクセスを行うオブジェクト
 */

namespace Lcas\Repository;



use Lcas\Exception\IOException;
use Lcas\DB\DB;

/**
 * @class UserRepository
 * @brief ユーザーのDBへのアクセスを行うオブジェクト
 */
class UserRepository extends BaseRepository {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 指定されたIDのユーザーを返す
     * @param int $id users.id
     * @return mixed
     */
    public function findById($id) {
        $userId = (int)$id;
        $sql = "SELECT id, login_id, email, name, platform, token, push_endpoint_arn, origin, "
            . "        created, updated "
            . "  FROM users WHERE id={$userId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        return $this->db->Fetch();
    }

    /**
     * 指定された名前のユーザーを返す。
     *
     * ユーザー名(users.name)は、LCAS<->ZIO間で使用するユーザーIDを想定して追加したIDです。
     *
     * v1.0では、ユーザー名はLCAS<->アプリ間で使用しているusers.login_idと同じ値を使用しています。
     *
     * @param $name users.name
     * @return mixed
     */
    public function findByName($name) {
        $name = $this->db->Escape($name);
        $sql = "SELECT id, login_id, email, name, platform, token, push_endpoint_arn, origin, "
            . "        created, updated "
            . "  FROM users WHERE name='{$name}'";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        return $this->db->Fetch();
    }

    /**
     * URLからユーザーの検索を行う
     * @param string $url "/api/v3/users/%login_id%"というフォーマットの文字列
     *
     *   %login_id%: users.login_id(users.idではありません)。
     *
     * @return mixed
     */
    public function findByUrl($url) {
        $urlPart = explode('/', $url);
        $userId = (int)array_pop($urlPart);
        return $this->findUserByLoginId($userId);
    }


    /**
     * メールアドレスとパスワードが合致するユーザーを検索する
     * @param string $email メールアドレス
     * @param string $pass パスワード
     * @return mixed
     */
    public function findUserByLoginInfo($email, $pass) {
        $email = $this->db->Escape($email);

        $encryptedPass = sha1(SALT_PASSWORD . $pass);
        $escapedPass = $this->db->Escape($encryptedPass);

        $sql = "SELECT id, login_id, email, name, platform, token, push_endpoint_arn, origin, created, updated FROM users WHERE email='{$email}' AND pass='{$escapedPass}'";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        return $this->db->Fetch();
    }

    /**
     * login_id(LCAS<->アプリ間で使用するユーザーID)に対応するユーザーの情報を返す。
     * @param int $loginId users.login_id (LCAS<->アプリ間で使用するユーザーID)
     * @return mixed
     */
    public function findUserByLoginId($loginId) {
        $loginId = (int)$loginId;

        $sql = "SELECT id, login_id, email, name, platform, token, push_endpoint_arn, origin, created, updated FROM users WHERE login_id={$loginId}";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        return $this->db->Fetch();
    }

    /**
     * 指定したメールアドレスのユーザーを返す
     * @param string $email
     * @param array $options 検索オプション
     *
     * - exclude_id: 検索結果から除外するユーザーのID(users.id)。
     *               ログインしている自分自身を除外するなどの用途で使用します。
     *
     * @return mixed
     */
    public function findUserByEmail($email, $options=array()) {
        $email = $this->db->Escape($email);

        $sql = "SELECT id, login_id, email, name, platform, token, push_endpoint_arn, origin, created, updated FROM users WHERE email='{$email}'";

        if(isset($options['exclude_id'])) {
            $excludeId = (int)$options['exclude_id'];
            $sql .= " AND id != {$excludeId} ";
        }

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        return $this->db->Fetch();
    }

    /**
     * 指定されたノードを所有しているユーザーを返す。
     * @param int $nodeId ノードID
     * @return array
     */
    public function findUserByNodeId($nodeId) {
        $nodeId = (int)$nodeId;
        $sharingLevel = NODE_SHARING_LEVEL_USERS;

        $sql = "SELECT users.id, users.login_id, users.email, users.name, users.platform, users.token, "
            . "  users.push_endpoint_arn, users.origin, users.created, users.updated "
            . "  FROM users "
            . "  LEFT OUTER JOIN nodes_users ON (nodes_users.user_id=users.id) "
            . " WHERE nodes_users.node_id={$nodeId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $users = array();
        while($row = $this->db->Fetch()) {
            $users[] = $row;
        }

        return $users;
    }


    /**
     * ユーザーを登録する
     * @param array $user usersのレコードを含んだ連想配列
     * @return int
     */
    public function register($user) {
        $platformType = $this->getPlatformTypeFromName($user['platform']);
        if(!$platformType) {
            throw new \InvalidArgumentException('無効なplatformです。: ' . $user['platform']);
        }

        $loginId = (int)$user['login_id'];
        $email = $this->db->Escape($user['email']);
        $name = $this->db->Escape($user['name']);
        $encryptedPass = sha1(SALT_PASSWORD . $user['pass']);
        $escapedPass = $this->db->Escape($encryptedPass);
        $platform = (int)$platformType;
        $token = $this->db->Escape($user['token']);
        $origin = $this->db->Escape($user['origin']);
        $created = date('Y-m-d H:i:s');

        $sql = "INSERT INTO users SET login_id={$loginId}, "
             . " email='{$email}', "
             . " name='{$name}', "
             . " pass='{$escapedPass}', "
             . " platform={$platform}, "
             . " token='{$token}', "
             . " origin='{$origin}', "
             . " created='{$created}' "
        ;
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        return $this->db->Insert_id();
    }


    /**
     * ユーザーを削除する
     * @param int $userId ユーザーID
     */
    public function deregister($userId) {
        $userId = (int)$userId;

        $sql = "DELETE FROM users WHERE id={$userId}";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }


    /**
     * ユーザーの情報を更新する
     * @param int $userId ユーザーID
     * @param array $user usersのレコードを含んだ連想配列
     */
    public function update($userId, $user) {

        $updateFields = array();
        if(array_key_exists('email', $user)) {
            $updateFields[] = sprintf("email='%s'", $this->db->Escape($user['email']));
        }
        if(array_key_exists('pass', $user)) {
            $encryptedPassword = sha1(SALT_PASSWORD . $user['pass']);
            $updateFields[] = sprintf("pass='%s'", $this->db->Escape($encryptedPassword));
        }
        if(array_key_exists('platform', $user)) {
            $platformType = $this->getPlatformTypeFromName($user['platform']);
            if(!$platformType) {
                throw new \InvalidArgumentException('無効なplatformです。: ' . $user['platform']);
            }
            $updateFields[] = sprintf("platform=%d", $platformType);
        }
        if(array_key_exists('token', $user)) {
            $updateFields[] = sprintf("token='%s'", $this->db->Escape($user['token']));
        }
        if(array_key_exists('origin', $user)) {
            $updateFields[] = sprintf("origin='%s'", $this->db->Escape($user['origin']));
        }
        $updateLabels = false;
        if(array_key_exists('labels', $user)) {
            $updateLabels = true;
        }
        
        if(count($updateFields) == 0 && !$updateLabels) {
            return;
        }

        if(0 < count($updateFields)) {
            $updateExpression = implode(', ', $updateFields);
            $updated = date('Y-m-d H:i:s');
            $sql = "UPDATE users SET {$updateExpression}, updated='{$updated}' WHERE id={$userId}";
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("sql: {$sql}, Error: {$error}");
            }
        }
        
        // labels の更新
        if ($updateLabels) {
            $this->updateLabels($userId, $user['labels']);
        }
    }
    
    /**
     * ユーザーラベルの更新をおこなう
     * データベースから、一旦削除して追加する
     * @param int $userId
     * @param array $labels
     * @throws IOException
     */
    public function updateLabels($userId, $labels)
    {
        $userId = (int)$userId;
        
        $this->removeAllLabels($userId);
        
        $labelType = LABEL_TYPE_USER;
        foreach((array)$labels as $label) {
            $created = date('Y-m-d H:i:s');
            $label = $this->db->Escape($label);
            $sql = "INSERT INTO labels SET type={$labelType}, entity_id={$userId}, label='{$label}', created='{$created}'";
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("labelの登録失敗。sql: {$sql}, Error: {$error}");
            }
        }        
    }
    
    public function findLabels($userId) {
        $userId = (int)$userId;
        $sql = "SELECT * FROM labels WHERE type=5 AND entity_id={$userId}";
        
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        
        $result = array();
        while ($row = $this->db->Fetch()) {
            array_push($result, $row);
        }
        return $result;
    }

    /**
     * Retrieves the selected Dashboard scenario status
     * of the user
     * 
     * @param int $userId
     * @return array $result
     */
    public function findDashboardStatus($userId) {
        $userId = (int)$userId;
        $sql = "SELECT * FROM labels WHERE type=". LABEL_TYPE_DASHBOARD_SCENARIO_STATUS ." AND entity_id={$userId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        $result = array();
        while ($row = $this->db->Fetch()) {
            array_push($result, $row);
        }

        return $result;
    }

    /**
     * AWS SNSのエンドポイントARNの情報を更新する
     * @param int $userId users.id
     * @param string $endpointArn Endpoint ARN
     */
    public function updatePushEndpoint($userId, $endpointArn) {
        $userId = (int)$userId;
        $endpointArn = $this->db->Escape($endpointArn);
        $updated = date('Y-m-d H:i:s');

        $sql = "UPDATE users SET push_endpoint_arn='{$endpointArn}', updated='{$updated}' WHERE id={$userId}";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }

    /**
     * 指定したユーザーのノード共有を解除する
     * @param int $userId ユーザーID
     */
    public function dissociateNodes($userId) {
        $userId = (int)$userId;

        $sql = "DELETE FROM nodes_users WHERE user_id={$userId}";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }

    /**
     * 新しいログインID()を生成して返す
     * @return int
     */
    public function generateNewLoginId() {
        mt_srand(microtime(true));

        $i = 0;
        $maxRetries = 10;
        $randomId = 0;
        while($i++ < $maxRetries) {
            $randomId = mt_rand(100000000, 999999999);
            $user = $this->findUserByLoginId($randomId);
            if($user == false) {
                break;
            }
        }

        if($i == $maxRetries) {
            return false;
        }
        return $randomId;
    }

    /**
     * プラットフォーム種別の定義を返す
     * @return array
     */
    public static function getPlatformTypeMapping() {
        return array(
            PLATFORM_NONE => 'none',
            PLATFORM_APNS => 'apns',
            PLATFORM_GCM  => 'gcm',
            PLATFORM_SQS  => 'sqs',
        );
    }

    /**
     * プラットフォーム種別の名前->タイプコードの変換を行う
     * @param string $string 種別名
     * @return mixed タイプコード。見つからない場合はfalse
     */
    public static function getPlatformTypeFromName($string) {
        $mapping = self::getPlatformTypeMapping();
        return array_search($string, $mapping);
    }

    /**
     * プラットフォーム種別のタイプコード->種別名の変換を行う
     * @param int $type タイプコード
     * @return mixed 種別名。見つからない場合はfalse
     */
    public static function getPlatformNameFromType($type) {
        $mapping = self::getPlatformTypeMapping();
        if(isset($mapping[$type])) {
            return $mapping[$type];
        }
        return false;
    }

    /**
     * ユーザーのすべてのラベルを削除する
     * ノードに紐づいたユーザーのラベルの削除については、NodeRepository::removeAllLabels() を使用すること
     * @param int $userId
     * @throws IOException
     */
    public function removeAllLabels($userId) {
        $userId = (int)$userId;
        $labelType = LABEL_TYPE_USER;

        $sql = "DELETE FROM labels WHERE type={$labelType} AND entity_id={$userId}";
        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }

    /**
     * Stores the selected scenario status
     * to be displayed in the dashboard
     *
     * @param int $userId
     * @param array $labels
     * @throws IOException
     */
    public function updateDashboardStatus($userId, $labels) {
        if(!is_array($labels) || count($labels) < 1) {
            throw new IOException("Invalid parameter labels passed, must be a valid array.");
        }

        $userId = (int)$userId;

        $this->removeDashboardStatus($userId);

        $labelType = LABEL_TYPE_DASHBOARD_SCENARIO_STATUS;

        foreach((array)$labels as $label) {
            $created = date('Y-m-d H:i:s');
            $label = $this->db->Escape($label);
            $sql = "INSERT INTO labels SET type={$labelType}, entity_id={$userId}, label='{$label}', created='{$created}'";
            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("labelの登録失敗。sql: {$sql}, Error: {$error}");
            }
        }
    }

    /**
     * Removes user selected scenario status
     * that is displayed in the dashboard
     *
     * @param int $userId
     * @throws IOException
     */
    public function removeDashboardStatus($userId) {
        $userId = (int)$userId;
        $labelType = LABEL_TYPE_DASHBOARD_SCENARIO_STATUS;

        $sql = "DELETE FROM labels WHERE type={$labelType} AND entity_id={$userId}";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
    }

    /**
     * Retrives the dashboard status to display
     * 
     * @param int $userId
     * @throws IOException
     * @return stdClass $result
     */
    public function getDashboardStatus($userId) {
        $scenarioStatus = $this->findDashboardStatus((int) $userId);

        $result = null;
        $scenarioIds = [];

        foreach($scenarioStatus as $row) {
            $status = explode(':', str_replace('dashboard_scenario_status_', '', $row['label']) );

            if($status[1] === '1') {
                array_push($scenarioIds, $status[0]);
            }
        }

        if(count($scenarioIds) > 0) {
            $sql = "SELECT scenario_messages.* FROM scenario_messages
                    INNER JOIN scenario_events ON scenario_events.message = scenario_messages.timeline_message
                    WHERE scenario_id IN (" . join(',', $scenarioIds) . ") ORDER BY scenario_events.created DESC LIMIT 1;";

            $this->db->setSql($sql);
            if(!$this->db->Query()) {
                $error = $this->db->getErr();
                throw new IOException("sql: {$sql}, Error: {$error}");
            }

            $result = array();

            while ($row = $this->db->Fetch()) {
                array_push($result, $row);
            }
        }

        return reset($result);
    }
}
