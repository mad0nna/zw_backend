<?php


namespace Lcas\Repository;



use Lcas\Exception\IOException;
use Lcas\DB\DB;
use Lcas\Helper\DateHelper;

class UserTokenRepository extends BaseRepository {

	public function __construct() {
		parent::__construct();
	}
	
	public function findToken($token, $includeExpired = false) {
		$sql = "SELECT token, user_id, token_type, email, platform, platform_token, origin, expires, created "
				. "  FROM user_tokens WHERE token='{$token}' ";
		if (!$includeExpired) {
			$dtNow = date('Y-m-d H:i:s');
			$sql .= " AND expires >= '{$dtNow}'";
		}
		
		$this->db->setSql($sql);
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException("user_tokensのSELECT失敗。sql: {$sql}, Error: {$error}");
		}

		return $this->db->Fetch();
	}
	
	public function findTokenByEmail($email, $includeExpired = false) {
		$sql = "SELECT token, user_id, token_type, email, platform, platform_token, origin, expires, created "
				. "  FROM user_tokens WHERE email='{$email}' ";
		if (!$includeExpired) {
			$dtNow = DateHelper::getDateTimeString();
			$sql .= " AND expires >= '{$dtNow}'";
		}
		
		$this->db->setSql($sql);
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException("user_tokensのSELECT失敗。sql: {$sql}, Error: {$error}");
		}

		return $this->db->Fetch();
	}
	
	public function registerToken($token, $token_type, $email = null, $platform = null, $platform_token = null, $origin = null, $userId = null, $expires = 86400) {
		$token = $this->db->Quot($token);
		$token_type = (int)$token_type;
		$email = is_null($email) ? '\'\'' : $this->db->Quot($this->db->Escape($email));
		$platform = is_null($platform) ? 0 : (int)$platform;
		$platform_token = is_null($platform_token) ? '\'\'' : $this->db->Quot($this->db->Escape($platform_token));
		$origin = is_null($origin) ? '\'\'' : $this->db->Quot($this->db->Escape($origin));
		$userId = is_null($userId) ? 'NULL' : (int)$userId;
		$created = DateHelper::getDateTimeString();
		$expires = DateHelper::getDateTimeAdd('h', 24, $created);
		$created = $this->db->Quot($created);
		$expires = $this->db->Quot($expires);
		
		$sql = "INSERT INTO user_tokens "
				. " SET "
				. "  token={$token}, "
				. "  token_type={$token_type}, "
				. "  email={$email}, "
				. "  platform={$platform}, "
				. "  platform_token={$platform_token}, "
				. "  origin={$origin}, "
				. "  user_id={$userId}, "
				. "  created={$created}, "
				. "  expires={$expires} ";
				
		$this->db->setSql($sql);
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException("user_tokensの登録失敗。sql: {$sql}, Error: {$error}");
		}
	}
	
	public function deleteToken($token) {
		$token = $this->db->Quot($token);
		
		$sql = "DELETE FROM user_tokens "
			. " WHERE "
			. "  token={$token} ";
				
		$this->db->setSql($sql);
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException("user_tokensの削除失敗。sql: {$sql}, Error: {$error}");
		}
	}
	
	public function deleteExpiredToken() {
		$dtNow = DateHelper::getDateTimeString();
		$sql = "DELETE FROM user_tokens "
				. " WHERE expires < '{$dtNow}'";

		$this->db->setSql($sql);
		if(!$this->db->Query()) {
			$error = $this->db->getErr();
			throw new IOException("user_tokensの削除失敗。sql: {$sql}, Error: {$error}");
		}
	}

	/**
	 * 32桁の Token を生成します。
	 */
	public function generateToken() {
        $i = 0;
        $maxRetries = 10;
        $newToken = 0;
        while($i++ < $maxRetries) {
			$newToken = bin2hex(openssl_random_pseudo_bytes(16));
            $token = $this->findToken($newToken, true);
            if($token === false) {
                break;
            }
        }

        if($i == $maxRetries) {
            return false;
        }
        return $newToken;
	}
}