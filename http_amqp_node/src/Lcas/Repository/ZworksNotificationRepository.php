<?php

/**
 * @file
 * @brief AMQPの通知メッセージを記録するテーブルへのアクセスを行うオブジェクト
 */

namespace Lcas\Repository;



use Lcas\Exception\IOException;
use Lcas\DB\DB;
use Lcas\Helper\DateHelper;
use Lcas\Log\Logger;


/**
 * @class ZworksNotificationRepository
 * @brief AMQPの通知メッセージを記録するテーブルへのアクセスを行うオブジェクト
 */
class ZworksNotificationRepository extends BaseRepository {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 通知メッセージを記録する
     * @param array $log zworks_notificationsテーブルのレコードを含んだ配列
     * @return int 登録したレコードのID
     */
    public function register($log) {
        $escapedType = $this->db->Escape($log['type']);
        $escapedData = $this->db->Escape($log['data']);
        $escapedCollerationId = (int)$log['correlation_id'];
        $escapedState = (int)$log['state'];
        $created = DateHelper::getDateStringWithMicrotime();

        $sql = "INSERT INTO zworks_notifications SET "
            . " type='{$escapedType}', "
            . " data='{$escapedData}', "
            . " state={$escapedState}, "
            . " correlation_id={$escapedCollerationId}, "
            . " created='{$created}' "
        ;

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("zworks_notificationsのINSERT失敗。sql: {$sql}, Error: {$error}");
        }
        return $this->db->Insert_id();
    }


    /**
     * 指定したCorrelation-Id(Z-works.ioへのリクエストのID)に対応する未処理のログを探して返す。
     * @param int $correlationId 対応するzworks_requestsテーブルのid(zworks_notifications.id)
     * @return array
     */
    public function findPeerNotification($correlationId) {
        $correlationId = (int)$correlationId;
        $state = ZWORKS_LOG_STATE_UNREAD;
        $sql = "SELECT id, type, data, state, correlation_id, created, updated "
            . "  FROM zworks_notifications WHERE correlation_id={$correlationId} AND state={$state} "
            . " LIMIT 1";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("zworks_notificationsのSELECT失敗。sql: {$sql}, Error: {$error}");
        }

        return $this->db->Fetch();
    }

    public function getNewFirmwares() {
        $logger = Logger::get();

        $sql = "SELECT * FROM firmware_versions";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("firmware_versionsのSELECT失敗。sql: {$sql}, Error: {$error}");
        }

        $firmwares = [];

        while ($notification = $this->db->Fetch()) {
            if (!array_key_exists('version', $notification)) {
                $logger->addWarning(sprintf('No value in firmware_versions record. data: %s', var_export($notification, true)));
                continue;
            }
            
            array_push($firmwares, $notification['version']);
        }

        return $firmwares;
    }

    /**
     * AMQPリクエストに対応する通知の状態を更新する。
     * @param int $correlationId 対応するzworks_requestsテーブルのid(zworks_notifications.id)
     * @param int $newState 更新後の状態。 ZWORKS_LOG_X
     */
    public function updateStateByCorrelationId($correlationId, $newState) {
        $correlationId = (int)$correlationId;
        $newState = (int)$newState;
        $updated = date('Y-m-d H:i:s');

        $sql = "UPDATE zworks_notifications SET "
            . " state={$newState}, "
            . " updated='{$updated}' "
            . " WHERE correlation_id=" . $correlationId
        ;

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("zworks_notificationsのUPDATE失敗。sql: {$sql}, Error: {$error}");
        }
    }


    /**
     * ログの削除を行う
     * @param string $startDate 基準日時
     * @param int $limit 1度に削除する件数の上限
     * @return int 削除したレコード数
     */
    public function removeLog($startDate, $limit=0) {

        if(!DateHelper::isValidDateTimeString($startDate)) {
            throw new \InvalidArgumentException('無効な日付けが指定されました。 startDate: ' . $startDate);
        }

        $startDate = $this->db->Escape($startDate);

        $sql = "DELETE FROM zworks_notifications "
             . " WHERE created <= '{$startDate}' ";

        if($limit > 0) {
            $sql .= ' LIMIT ' . (int)$limit;
        }

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        return $this->db->Affected_rows();
    }

}
