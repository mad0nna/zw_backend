<?php

/**
 * @file
 * @brief ZIOに送信したメッセージを記録するテーブルへのアクセスを行うオブジェクト
 */

namespace Lcas\Repository;



use Lcas\Exception\IOException;
use Lcas\DB\DB;
use Lcas\Helper\DateHelper;


/**
 * @class ZworksRequestRepository
 * @brief ZIOに送信したメッセージを記録するテーブルへのアクセスを行うオブジェクト
 */
class ZworksRequestRepository extends BaseRepository {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 指定したIDのAMQPメッセージを検索する
     * @param int $id zworks_requests.id
     * @return mixed
     */
    public function find($id) {
        $id = (int)$id;
        $state = ZWORKS_REQUEST_STATE_UNREAD;
        $sql = "SELECT id, command, data, extra_data, state, created, updated "
            . "  FROM zworks_requests WHERE id={$id} AND state={$state} "
            . " LIMIT 1";

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        return $this->db->Fetch();
    }

    /**
     * 送信したAMQPメッセージの記録を行う
     * @param array $log zworks_requestsテーブルのレコードを含んだ配列
     * @return int ログID
     */
    public function register($log) {
        $escapedCommand = $this->db->Escape($log['command']);
        $escapedData = $this->db->Escape($log['data']);
        $escapedState = (int)$log['state'];
        $created = date('Y-m-d H:i:s');

        $extra = '';
        if(is_array($log['extra_data'])) {
            $extra = $this->db->Escape(json_encode($log['extra_data']));
        }

        $sql = "INSERT INTO zworks_requests SET "
            . " command='{$escapedCommand}', "
            . " data='{$escapedData}', "
            . " extra_data='{$extra}', "
            . " state={$escapedState}, "
            . " created='{$created}' "
        ;

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        return $this->db->Insert_id();
    }


    /**
     * ログの状態を更新する
     * @param int $id zworks_requests.id
     * @param int $newState ZWORKS_REQUEST_STATE_XXX
     * @return int 未使用
     */
    public function updateState($id, $newState) {
        $id = (int)$id;
        $newState = (int)$newState;
        $updated = date('Y-m-d H:i:s');

        $sql = "UPDATE zworks_requests SET "
            . " state={$newState}, "
            . " updated='{$updated}' "
            . " WHERE id=" . $id
        ;

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }
        return $this->db->Insert_id();
    }


    /**
     * 指定日時以前のログの削除を行う
     * @param string $startDate 基準日時
     * @param int $limit 1度に削除を行うレコード数
     * @return int 削除した件数
     */
    public function removeLog($startDate, $limit=0) {

        if(!DateHelper::isValidDateTimeString($startDate)) {
            throw new \InvalidArgumentException('無効な日付けが指定されました。 startDate: ' . $startDate);
        }

        $startDate = $this->db->Escape($startDate);

        $sql = "DELETE FROM zworks_requests "
            . " WHERE created <= '{$startDate}' ";

        if($limit > 0) {
            $sql .= ' LIMIT ' . (int)$limit;
        }

        $this->db->setSql($sql);
        if(!$this->db->Query()) {
            $error = $this->db->getErr();
            throw new IOException("sql: {$sql}, Error: {$error}");
        }

        return $this->db->Affected_rows();
    }

}
