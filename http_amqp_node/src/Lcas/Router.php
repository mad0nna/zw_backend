<?php

/**
 * @file
 * @brief HTTPメソッド、URLに応じて処理のルーティングを行う
 */

namespace Lcas;


/**
 * @class Router
 * @brief HTTPメソッド、URLに応じて処理のルーティングを行う
 */
class Router {

    /**
     * HTTPメソッド、URLに応じて実行する処理を決定する
     * @param string $method HTTPメソッド
     * @param string $path HTTPリクエストのパス部分
     * @return array|bool ルーティング情報を含んだ配列、該当無しの場合はfalse
     *
     * ```
     * [
     *   'controller' => 'コントローラ名',
     *   'action' => 'アクション名',
     *   'captured' => [URLのパス部分のうち、正規表現でキャプチャしたデータを含んだ連想配列],
     * ]
     * ```
     */
    public function getRoute($method, $path) {
        $mapping = $this->getRoutingMap();

        foreach($mapping as $map) {
            if($map['method'] != $method) {
                continue;
            }
            $matched = array();
            if(!preg_match($map['pattern'], $path, $matched)) {
                continue;
            }

            $route = array(
                'controller' => $map['controller'],
                'action' => $map['action'],
                'captured' => $matched,
            );
            return $route;
        }
        return false;
    }


    private function getRoutingMap() {

        $mapping = array();
        require CONFIG_DIR . '/routing.php';

        return $mapping;
    }

}
