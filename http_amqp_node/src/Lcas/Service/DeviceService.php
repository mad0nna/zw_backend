<?php

/**
 * @file
 * @brief デバイスに関するロジックを定義するクラス
 */

namespace Lcas\Service;

use Lcas\Exception\ApiTimeoutException;
use Lcas\Exception\BadParameterException;
use Lcas\Exception\ZworksApiException;
use Lcas\Repository\DeviceRepository;
use Lcas\Repository\GatewayRepository;
use Lcas\Repository\NodeRepository;
use Lcas\ServiceContainer;
use Lcas\DB\DB;


/**
 * @class DeviceService
 * @brief デバイスに関するロジックを定義するクラス
 */
class DeviceService {

    /**
     * @var DeviceRepository
     */
    private $deviceRepository;

    /**
     * @var GatewayRepository
     */
    private $gatewayRepository;

    /**
     * @var NodeRepository
     */
    private $nodeRepository;

    /**
     * @var \Lcas\Adapter\ZworksRequestAdapterInterface
     */
    private $zworksAmqpAdapter;

    
    /**
     * コンストラクタ
     */
    public function __construct() {
    	$this->deviceRepository = new DeviceRepository();
        $this->gatewayRepository = new GatewayRepository();
        $this->nodeRepository = new NodeRepository();

    	$serviceContainer = ServiceContainer::getInstance();
        $this->zworksAmqpAdapter = $serviceContainer->get('zworks_request_adapter');
    }


    /**
     * 指定されたユーザーIDに紐付くデバイスを列挙する
     * @param int $userId users.id
     * @param string $label デバイスに紐付くノードのラベル。部分一致で検索(未指定時は空文字列)
     * @return array HTTP APIのレスポンス用に整形した連想配列
     */
    public function getUserDeviceList($userId, $label) {
        //Gatewayを取得する
        $gateways = $this->gatewayRepository->findByUserId($userId);

        $gatewayIds = array();
        foreach($gateways as $gateway) {
            $gatewayIds[] = (int)$gateway['id'];
        }

        //Gatewayのownerに紐付くNode一覧を取得
        $nodes = $this->nodeRepository->findByGatewayIds($gatewayIds);

        //Node共有されているNodeの一覧を取得
        $sharedNodes = $this->nodeRepository->findByUserId($userId);

        $nodeIds = array();
        foreach(array_merge($nodes, $sharedNodes) as $node) {
            $nodeIds[] = $node['id'];
        }

        if(strlen($label) > 0) {
            //labelが指定されている場合は、デバイスに紐付くNodeのlabelを曖昧検索する。
            //その結果マッチしたNode IDのみ対象とする。
            $filteredNodeIds = $this->nodeRepository->filterNodeIdListByLabel($nodeIds, $label);
        } else {
            $filteredNodeIds = $nodeIds;
        }

        $devices = $this->deviceRepository->findByNodeIdList($filteredNodeIds);

        $result = array();
        foreach($devices as $device) {
            $labels = $this->deviceRepository->findLabels($device['id']);
            $result[] = $this->formatDeviceForApiResponce($device, $labels);
        }
        return $result;
    }


    /**
     * 指定されたデバイスの情報を返す
     * @param int $deviceId devices.id
     * @return array HTTP APIのレスポンス用に整形した連想配列
     */
    public function getDeviceDetail($deviceId) {

        $device = $this->deviceRepository->findById($deviceId);
        if(!$device) {
            return false;
        }

        $labels = $this->deviceRepository->findLabels($device['id']);
        return $this->formatDeviceForApiResponce($device, $labels);
    }


    /**
     * 指定されたデバイスのデータ一覧を返す
     * @param int $deviceId devices.device_id
     * @param int $fromMilliSec 取得開始日時。ミリ秒精度の時刻。0=無制限
     * @param int $toMilliSec 取得終了日時。ミリ秒精度の時刻。0=無制限
     * @return mixed HTTP APIのレスポンス用に整形した連想配列、返却するデータが存在しない場合、false
     */
    public function getDeviceValueList($deviceId, $fromMilliSec, $toMilliSec) {
        $device = $this->deviceRepository->findById($deviceId);
        if(!$device) {
            return false;
        }

        $values = $this->deviceRepository->findDeviceValues($device['id'], $fromMilliSec, $toMilliSec);
        //TODO: この結果セットは非常に大きくなる可能性があるため、一定量ずつデータを取得するイテレータを返し、
        //TODO: 出力時はSymfony\Component\HttpFoundation\StreamedResponseを使用することを検討する。
        $result = array();
        foreach($values as $value) {
            $result[] = $this->formatDeviceValueForApiResponce($value);
        }
        return $result;
    }


    /**
     * 指定したデバイスの最新のデータ1件を返す。
     * @param int $deviceId devices.device_id
     * @return mixed HTTP APIのレスポンス用に整形した連想配列
     */
    public function getLatestDeviceValue($deviceId) {
        $device = $this->deviceRepository->findById($deviceId);
        if(!$device) {
            return false;
        }

        $value = $this->deviceRepository->findLatestDeviceValue($device['id']);
        if(!$value || count($value) == 0) {
            return false;
        }
        return $this->formatDeviceValueForApiResponce($value);
    }


    /**
     * DBから取得したdevicesの情報を、APIのレスポンスの書式に整形して返す。
     * @param array $device devicesテーブルのレコード(1行分)
     * @param array $labels ラベル文字列の配列
     * @return array HTTP API のレスポンス用に整形した配列
     */
    public function formatDeviceForApiResponce($device, $labels) {
        $nodeUrl = sprintf('/api/v3/nodes/%d', $device['node_id']);
        return array(
            'id' => (int)$device['id'],
            'labels' => (array)$labels,
            'type' => $this->deviceRepository->getDeviceTypeNameFromType($device['type']),
            'node' => $nodeUrl,
        );
    }

    /**
     * DBから取得したdevice_dataの情報を、APIのレスポンスの書式に整形して返す。
     * @param array $deviceValue device_dataテーブルのレコード（1行分）
     * @return array HTTP API のレスポンス用に整形した配列
     */
    public function formatDeviceValueForApiResponce($deviceValue) {
        $data = array(
            'value' => $deviceValue['value'],
            'timestamp' => $this->deviceRepository->convertMicroTimeToMilliSec($deviceValue['time_stamp']),
        );
        if(isset($deviceValue['unit']) && strlen($deviceValue['unit']) > 0) {
            $data['unit'] = $deviceValue['unit'];
        }
        return $data;
    }
    
    /**
     * デバイスの制御をおこないます。
     * @param string $nodeName
     * @param string $deviceType
     * @param string $action
     * @param array $params
     * @throws BadParameterException
     * @throws ApiTimeoutException
     * @throws ZworksApiException
     * @return array
     */
    public function controlDevice($nodeName, $deviceType, $actionType, $action) {

    	$request = array(
    			'command'     => 'dev_ctrl',
    			'node_name'   => $nodeName,
    			'device_type' => $deviceType,
    			'action_type' => $actionType,
    			'action'      => $action,
    	);
    	
    	$db = DB::getMasterDb();

    	$db->Begin();
    	//Adapterに問い合わせる
    	$this->zworksAmqpAdapter->sendMessage($request);
    	
    	// 待っていても結果は返らないため、待ち受けはしない
    	$this->zworksAmqpAdapter->complete();
    	$db->Commit();
    }

}
