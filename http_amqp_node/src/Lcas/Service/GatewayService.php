<?php

/**
 * @file
 * @brief ゲートウェイに関するロジックを定義するクラス
 */

namespace Lcas\Service;

use Lcas\Adapter\ZworksRequestAdapterStub;
use Lcas\Exception\ApiTimeoutException;
use Lcas\Exception\BadParameterException;
use Lcas\Exception\DataNotFoundException;
use Lcas\Exception\ZworksApiException;
use Lcas\Repository\DeviceRepository;
use Lcas\Repository\GatewayRepository;
use Lcas\Repository\NodeRepository;
use Lcas\Repository\ZWorksNotificationRepository;
use Lcas\Repository\UserRepository;
use Lcas\DB\DB;
use Lcas\ServiceContainer;
use Lcas\Log\Logger;
use Lcas\Exception\ApiNotSupportedException;

/**
 * @class GatewayService
 * @brief ゲートウェイに関するロジックを定義するクラス
 */
class GatewayService {

    /**
     * @var GatewayRepository
     */
    private $gatewayRepository;

    /**
     * @var NodeRepository
     */
    private $nodeRepository;

    /**
     * @var DeviceRepository
     */
    private $deviceRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var \Lcas\Adapter\ZworksRequestAdapterInterface
     */
    private $zworksAmqpAdapter;

    /**
     * @var NodeService
     */
    private $nodeService;

    /**
     * コンストラクタ
     */
    public function __construct() {
        $serviceContainer = ServiceContainer::getInstance();

        $this->gatewayRepository = new GatewayRepository();
        $this->nodeRepository = new NodeRepository();
        $this->deviceRepository = new DeviceRepository();
        $this->userRepository = new UserRepository();
        $this->notificationRepository = new ZWorksNotificationRepository();
        $this->nodeService = new NodeService();
        $this->zworksAmqpAdapter = $serviceContainer->get('zworks_request_adapter');
    }


    /**
     * 指定されたGatewayへのアクセス権があるかを確認して返す
     * @param int $gatewayId ゲートウェイID
     * @param array $user usersのレコードを含んだ配列
     * @return boolean
     */
    public function isGrantedUser($gatewayId, $user) {
        $gateway = $this->gatewayRepository->findById($gatewayId);
        if(!$gateway) {
            throw new DataNotFoundException('ゲートウェイが見つかりません。');
        }
        return $gateway['user_id'] == $user['id'];
    }

    /**
     * ゲートウェイの割り当てを実行する
     *
     * @param array $user usersテーブルのエントリ
     * @param string $macAddr
     * @param string $key
     * @return array 結果コード、HTTPのレスポンス用の情報を含んだ配列
     */
    public function associateGateway($user, $macAddr, $key) {

        //ユーザーにGatewayが割当済みの場合、API呼出は失敗する
        $existGateway = $this->gatewayRepository->findByMacAddress($macAddr, array('with_associated' => true));
        if($existGateway) {
            throw new BadParameterException('無効なゲートウェイです。');
        }

        $request = array(
            'command' => 'user_gw_assoc',
            'username' => (string)$user['name'],
            'gw_name'  => $macAddr,
            'gw_key'   => $key,
        );

        $db = DB::getMasterDb();
        //トランザクション中でDELETE+INSERTしている箇所でデッドロックが発生する可能性があるため、
        //READ-COMMITTEDに変更する。
        $db->SetTransactionIsolationLevel('READ COMMITTED');
        $db->Begin();

        //Adapterに問い合わせる
        $this->zworksAmqpAdapter->sendMessage($request);

        //結果をポーリングする
        $received = $this->zworksAmqpAdapter->waitNotification();

        if(!$received) {
            throw new ApiTimeoutException('API timeout: Gateway Association Notification.');
        }
        $notification = $this->zworksAmqpAdapter->getResult();

        $nodeUrls = array();
        $resultCode = $notification['result_code'];
        if($resultCode != 200) {
            throw new ZworksApiException(var_export($notification['reason'], true));
        }
        foreach((array)$notification['nodes'] as $node) {
            $nodeUrls[] = sprintf('/api/v3/nodes/%d', $node['node_id']);
        }

        //Gatewayの登録処理を実行
        $gatewayId = $this->registerGateway($notification);


        $this->zworksAmqpAdapter->complete();

        $db->Commit();
        
        // 登録済みのラベルを取得する
        $labels = $this->gatewayRepository->findLabelsById($gatewayId);

        $response = array(
            'result_code' => $notification['result_code'],
            'response' => array(
                'id' => $gatewayId,
                'labels' => $labels,
                'mac_address' => $notification['gw_name'],
                'gw_name' => $notification['gw_name'],
                'fw_version' => $notification['fw_version'],
                'manufacturer' => $notification['manufacturer'],
                'nodes' => $nodeUrls,
                'capabilities' => $notification['capabilities'],
                'status' => $this->gatewayRepository->getStateNameFromId(GATEWAY_STATE_DISCONNECTED),  //新規登録時は常に"DISCONNECTED"
            )
        );
        return $response;
    }


    /**
     * AMQPからの通知の内容でGatewayの登録を行う
     * @param $notification "User Gateway Association Notification" を含んだ連装配列
     * @return int ゲートウェイID
     */
    public function registerGateway($notification)
    {
        $user = $this->userRepository->findByName($notification['username']);
        if(!$user) {
            throw new \RuntimeException('No user found. username: ' . $notification['username']);
        }

        //過去にLCASに登録済みのGatewayか確認する
        $oldGateway = $this->gatewayRepository->findByMacAddress($notification['gw_name']);
        $prevUserId = (isset($oldGateway['prev_user_id'])) ? $oldGateway['prev_user_id'] : 0;
        $isSameOwner = ($prevUserId == $user['id']);

        //gateway情報をDBに登録する
        $gateway = array(
            'mac_address' => $notification['gw_name'],
            'gw_name' => $notification['gw_name'],
            'fw_version' => $notification['fw_version'],
            'manufacturer' => $notification['manufacturer'],
            'state' => GATEWAY_STATE_DISCONNECTED, //Gateway割り当て直後は常に"DISCONNECTED"
            'user_id' => $user['id'],
        );

        $gatewayId = 0;
        if(isset($oldGateway['id'])) {
            $gatewayId = $oldGateway['id'];
            $this->gatewayRepository->update($gatewayId, $gateway);
        } else {
            $gatewayId = $this->gatewayRepository->register($gateway);
        }

        $capabilityTypes = $this->gatewayRepository->getCapabilityTypesFromNames($notification['capabilities']);
        $this->gatewayRepository->replaceCapabilities($gatewayId, $capabilityTypes);

        if(!$isSameOwner) {
            $this->gatewayRepository->removeAllLabels($gatewayId);
        }

        foreach ($notification['nodes'] as $nodeInfo) {
            $this->nodeService->registerNodeFromNotification('user_gw_assoc', $gatewayId, $nodeInfo);
        }
        return $gatewayId;
    }

    /**
     * ユーザーとゲートウェイの割当を解除する
     *
     * @param array $user usersテーブルのレコードを含んだ配列
     * @param int $gatewayId gateways.id
     * @return array 処理結果のステータスコードを含んだ配列
     */
    public function disssociateGateway($user, $gatewayId) {

        $gateway = $this->gatewayRepository->findById($gatewayId);
        if(!$gateway) {
            throw new BadParameterException("ゲートウェイが見つかりません。");
        }

        $request = array(
            'command' => 'user_gw_dissoc',
            'username' => (string)$user['name'],
            'gw_name'  => $gateway['mac_address'],
        );

        $db = DB::getMasterDb();
        $db->Begin();

        //Adapterに問い合わせる
        $this->zworksAmqpAdapter->sendMessage($request);

        //結果をポーリングする
        $received = $this->zworksAmqpAdapter->waitNotification();
        if(!$received) {
            throw new ApiTimeoutException('Gateway Dissociation Notificationの受信がタイムアウトしました.');
        }
        $notification = $this->zworksAmqpAdapter->getResult();

        $resultCode = $notification['result_code'];
        if($resultCode != 200) {
            throw new ZworksApiException(var_export($notification['reason'], true));
        }

        //gatewayに関する情報を削除する
        $this->deregisterGateway($gatewayId);

        $this->zworksAmqpAdapter->complete();

        $db->Commit();

        $response = array(
            'result_code' => $notification['result_code'],
        );
        return $response;
    }


    /**
     * LCAS内部でのゲートウェイの割り当て解除を行う
     *
     * (ゲートウェイをユーザーから切り離す、ノードをゲートウェイから切り離す)
     * @param int $gatewayId ゲートウェイID
     */
    public function deregisterGateway($gatewayId) {
        $this->nodeRepository->clearGatewayAssociation($gatewayId);
        $this->gatewayRepository->clearAssociation($gatewayId);
    }

    /**
     * Check if there are newer firmware versions available
     * @param string $currentFirmware The firmware to compare
     * @param array $firmwares The list of firmwares to compare to
     * @return string|null The new firmware version, or null if there is none
     */
    private function compareFirmwares($currentFirmware, $firmwares) {
        $fwRegex = '/([A-Z]+).(\d+).(\d+).(\d+)([a-z]*)/';

        preg_match($fwRegex, $currentFirmware, $output);
        if (count($output) != 6) {
            return null;
        }
        $currentProduct = $output[1];
        $currentMajor = $output[2];
        $currentMinor = $output[3];
        $currentPatch = $output[4];
        $currentLabel = $output[5];

        $currentVersionStr = $currentMajor . '.' . $currentMinor . '.' . $currentPatch;

        $newFirmware = null;

        foreach ($firmwares as $firmware) {
            preg_match($fwRegex, $firmware, $output);
            if (count($output) != 6) {
                continue;
            }
            $product = $output[1];
            $major = $output[2];
            $minor = $output[3];
            $patch = $output[4];
            $label = $output[5];

            if ($currentProduct !== $product || $currentLabel !== $label) {
                continue;
            }

            $versionStr = $major . '.' . $minor . '.' . $patch;
            if (version_compare($currentVersionStr, $versionStr) === -1) {
            	$newFirmware = $firmware;
            	
		        preg_match($fwRegex, $firmware, $output);
		        if (count($output) != 6) {
		            return null;
		        }
		        $currentProduct = $output[1];
		        $currentMajor = $output[2];
		        $currentMinor = $output[3];
		        $currentPatch = $output[4];
		        $currentLabel = $output[5];
		
		        $currentVersionStr = $currentMajor . '.' . $currentMinor . '.' . $currentPatch;
            }
        }

        return $newFirmware;
    }

    /**
     * 指定されたゲートウェイの詳細情報を返す
     * @param int $gatewayId gateways.id
     * @return array HTTP APIのレスポンス用の情報を含んだ配列
     */
    public function getGatewayDetailedResponse($gatewayId) {
        $gateway = $this->gatewayRepository->findById($gatewayId);
        $labels = $this->gatewayRepository->findLabelsById($gatewayId);
        $capabilityTypes = $this->gatewayRepository->findCapabilitiesById($gatewayId);
        $capabilities = $this->gatewayRepository->getCapabilityNamesFromTypes($capabilityTypes);
        $nodes = $this->nodeRepository->findByGatewayIds(array($gatewayId));
        $firmwares = $this->notificationRepository->getNewFirmwares();

        $nodeUrls = array();
        foreach($nodes as $node) {
            $nodeUrls[] = sprintf('/api/v3/nodes/%d', $node['id']);
        }

        $result = array(
            'id' => (int)$gateway['id'],
            'labels' => $labels,
            'mac_address' => $gateway['mac_address'],
            'gw_name' => $gateway['gw_name'],
            'fw_version' => $gateway['fw_version'],
            'manufacturer' => $gateway['manufacturer'],
            'gateway_tile_status' => $gateway['status'],
            'nodes' => $nodeUrls,
            'capabilities' => $capabilities,
            'status' => $this->gatewayRepository->getStateNameFromId($gateway['state']),
        );

        $newFirmware = $this->compareFirmwares($gateway['fw_version'], $firmwares);
        if (!is_null($newFirmware)) {
            $result['new_fw'] = $newFirmware;
        }

        if (in_array("dust_ip", $capabilities)) {
            $result['dust_ip'] = $this->gatewayRepository->getDustIp($gatewayId);
        }

        return $result;
    }


    /**
     * ゲートウェイの状態変更
     * @param int $gatewayId ゲートウェイID
     * @param string $mode 変更後のゲートウェイの状態名(include|exclude)
     * @param string $flag start|stopのいずれか
     * @return array 処理結果のステータスコードを含んだ配列
     */
    public function updateGatewayState($gatewayId, $mode, $flag) {

        $gateway = $this->gatewayRepository->findById($gatewayId);
        if(!$gateway) {
            throw new BadParameterException("gateway_idが不正です。:".$gatewayId);
        }

        $commandNames = array(
            'include' => 'node_incl',
            'exclude' => 'node_excl',
        );
        if(!isset($commandNames[$mode])) {
            throw new BadParameterException("modeの値が不正です。:".$flag);
        }
        $commandName = $commandNames[$mode];

        $possibleFlags = array('start', 'stop');
        if(!in_array($flag, $possibleFlags)) {
            throw new BadParameterException("flagの値が不正です。:".$flag);
        }

        $request = array(
            'command' => $commandName,
            'flag' => $flag,
            'gw_name'  => $gateway['mac_address'],
        );

        $db = DB::getMasterDb();
        $db->Begin();

        //Adapterに問い合わせる
        $this->zworksAmqpAdapter->sendMessage($request);

        //結果をポーリングする
        $received = $this->zworksAmqpAdapter->waitNotification();
        if(!$received) {
            throw new ApiTimeoutException('Gateway State Change Notificationの受信がタイムアウトしました.');
        }
        $notification = $this->zworksAmqpAdapter->getResult();

        $resultCode = $notification['result_code'];
        if($resultCode != 200) {
            throw new ZworksApiException(var_export($notification['reason'], true));
        }

        $stateId = $this->gatewayRepository->getStateIdFromName($notification['state']);
        $this->gatewayRepository->updateState($gatewayId, $stateId);

        $this->zworksAmqpAdapter->complete();
        $db->Commit();
        $response = array(
            'result_code' => $notification['result_code'],
        );
        return $response;
    }
    
    public function updateFirmware($gatewayId, $fwVersion) {
    	$gateway = $this->gatewayRepository->findById($gatewayId);
    	if(!$gateway) {
    		throw new BadParameterException("gateway_idが不正です。:".$gatewayId);
    	}
    	
    	$request = array(
    			'command' => 'gw_fw_update',
    			'gw_name'  => $gateway['mac_address'],
    			'fw_version' => $fwVersion,
    			'force' => false
    	);
    	
    	$db = DB::getMasterDb();
    	$db->Begin();
    	
    	//Adapterに問い合わせる
    	$this->zworksAmqpAdapter->sendMessage($request);
    	
    	// 待っていても結果は返らないため、待ち受けはしない
    	$this->zworksAmqpAdapter->complete();
    	$db->Commit();
    }

    public function updatePublicKey($gatewayId) {
    	$gateway = $this->gatewayRepository->findById($gatewayId);

        // todo: change mac_address to gw_name in the future?
        $gatewayName = $gateway["mac_address"];

        $request = array(
            "command" => "pubkey_update",
            "gw_name" => $gatewayName
        );

        $db = DB::getMasterDb();
        $db->Begin();

        $this->zworksAmqpAdapter->sendMessage($request);
        $received = $this->zworksAmqpAdapter->waitNotification();
        if(!$received) {
            throw new ApiTimeoutException('Public Key Update Notificationの受信がタイムアウトしました.');
        }
        $notification = $this->zworksAmqpAdapter->getResult();
        
        $resultCode = $notification['result_code'];
        if($resultCode != 200) {
            throw new ApiNotSupportedException();
        }
        
        $publicKey = $notification["public_key"];

        $this->gatewayRepository->updatePublicKey($gatewayId, $publicKey);

        $this->zworksAmqpAdapter->complete();
        $db->Commit();

        $result = Array (
            "result_code" => $notification["result_code"],
            "response"=> Array (
                "public_key" => $publicKey
            )
        );

        return $result;
    }

    public function updateLabelsAndDustIp($gatewayId, $labels, $dustIp) {
        // save labels to database directly
        $db = DB::getMasterDb();
        $db->Begin();
        $this->gatewayRepository->updateLabels($gatewayId, $labels);
        $db->Commit();
        
        if (empty($dustIp)) {
        	return 204;
        }

        $db = DB::getMasterDb();
        $db->Begin();

        // send set acl request to amqp
        $gwName = $this->gatewayRepository->findById($gatewayId)['gw_name'];

        // todo: is it necessary to check capability?

        $request = array(
            "command" => "set_acl",
            "gw_name" => $gwName,
            "network_id" => $dustIp['network_id'],
            "acl" => $dustIp['acl'],
        );

        $this->zworksAmqpAdapter->sendMessage($request);
        $received = $this->zworksAmqpAdapter->waitNotification();
        if(!$received) {
            throw new ApiTimeoutException('Set ACL Notificationの受信がタイムアウトしました.');
        }
        $notification = $this->zworksAmqpAdapter->getResult();

        $resultCode = $notification['result_code'];

        if($resultCode != 200 && $resultCode != 300) {
            throw new ZworksApiException(var_export($notification['reason'], true));
        }

        $this->gatewayRepository->reserveDustIp($gatewayId);

    	$db->Commit();

        return 202;
    }
}
