<?php

/**
 * @file
 * @brief ノードに関するロジックを定義するクラス
 */

namespace Lcas\Service;

use Lcas\Adapter\ZworksRequestAdapterInterface;
use Lcas\DB\DB;
use Lcas\Exception\ApiTimeoutException;
use Lcas\Exception\BadParameterException;
use Lcas\Exception\DataNotFoundException;
use Lcas\Exception\ZworksApiException;
use Lcas\Log\Logger;
use Lcas\Repository\DeviceRepository;
use Lcas\Repository\GatewayRepository;
use Lcas\Repository\NodeRepository;
use Lcas\Repository\UserRepository;
use Lcas\Repository\TileRepository;
use Lcas\ServiceContainer;


/**
 * @class NodeService
 * @brief ノードに関するロジックを定義するクラス
 */
class NodeService {

    /**
     * @var NodeRepository
     */
    private $nodeRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var DeviceRepository
     */
    private $deviceRepository;

    /**
     * @var GatewayRepository
     */
    private $gatewayRepository;

      /**
     * @var TileRepository
     */
    private $tileRepository;


    /**
     * @var ZworksRequestAdapterInterface
     */
    private $zworksRequestAdapter;

    /**
     * コンストラクタ
     */
    public function __construct() {
        $serviceContainer = ServiceContainer::getInstance();

        $this->nodeRepository = new NodeRepository();
        $this->userRepository = new UserRepository();
        $this->deviceRepository = new DeviceRepository();
        $this->gatewayRepository = new GatewayRepository();
        $this->tileRepository = new TileRepository();
        $this->zworksRequestAdapter = $serviceContainer->get('zworks_request_adapter');
    }

    /**
     * 指定されたユーザーに紐付くnodesの一覧を返す
     * @param int $userId users.id
     * @param string $label 絞り込みに使用するラベル。未指定時はnull
     * @param string $target 絞り込みに使用する対象ノード。未指定時はnull
     * @return array HTTP APIのレスポンス用の情報を含んだ配列
     */
    public function getUserNodeList($userId, $label=null, $target=null) {
        $gateways = $this->gatewayRepository->findByUserId($userId);

        $gatewayIds = array();
        foreach($gateways as $gateway) {
            $gatewayIds[] = (int)$gateway['id'];
        }

        $nodes = $this->nodeRepository->findByGatewayIds($gatewayIds, $target);
        $sharedNodes = $this->nodeRepository->findByUserId($userId, $target);
        $nodes = array_merge($nodes, $sharedNodes);

        uasort($nodes, function($a, $b){
            if($a['id'] == $b['id']) {
                return 0;
            }
            return ($a['id'] < $b['id']) ? -1 : 1;
        });

        $filteredNodeIds = array();
        if(!is_null($label)) {
            $filteredNodeIds = $this->nodeRepository->findNodeIdListByLabel($label);
        }

        $result = array();
        foreach($nodes as $node) {

            if(!is_null($label) && !in_array($node['id'], $filteredNodeIds)) {
                continue;
            }

           	$labels = $this->nodeRepository->findLabels($node['id'], null);
            $users = $this->userRepository->findUserByNodeId($node['id']);
            $devices = $this->nodeRepository->findDevices($node['id']);
            $configs = $this->nodeRepository->findNodeConfigs($node['id']);
            $zwaveConfigs = array();
            if (isset($configs['zwave_configurations'])) {
            	array_push($zwaveConfigs, $configs['zwave_configurations']);
            }
            $otherConfigs = array();
            if (isset($configs['configurations'])) {
            	array_push($otherConfigs, $configs['configurations']);
            }
            $owned = in_array($node['gateway_id'], $gatewayIds);
            
            $dustIp = array();
            if ($node['protocol'] == 'dust_ip') {
           		$dustIp['join_key_set'] = $node['join_key_set'];
            }
            $appConfigs = $this->nodeRepository->findNodeAppConfigs($node['id']);
            $result[] = $this->formatNodeForApiResponce($node, $labels, $users, $devices, $owned, $zwaveConfigs, $otherConfigs, $dustIp, $appConfigs);
        }
        return $result;
    }

    /**
     * 指定されたノードの情報を返す
     * @param int $userId users.id
     * @param int $nodeId nodes.id
     * @return mixed HTTP APIのレスポンス用の情報を含んだ配列。対象が存在しない場合、false
     */
    public function getNodeDetail($userId, $nodeId) {

        $node = $this->nodeRepository->findById($nodeId);
        if(!$node) {
            return false;
        }

        $gateways = $this->gatewayRepository->findByUserId($userId);
        $gatewayIds = array();
        foreach($gateways as $gateway) {
            $gatewayIds[] = (int)$gateway['id'];
        }

       	$labels = $this->nodeRepository->findLabels($node['id'], null);
        $users = $this->userRepository->findUserByNodeId($node['id']);
        $devices = $this->nodeRepository->findDevices($node['id']);
        $configs = $this->nodeRepository->findNodeConfigs($node['id']);
		$zwaveConfigs = array();
		if(isset($configs['zwave_configurations'])) {
			array_push($zwaveConfigs, $configs['zwave_configurations']);
		}
		$otherConfigs = array();
		if(isset($configs['configurations'])) {
			array_push($otherConfigs, $configs['configurations']);
		}
        $owned = in_array($node['gateway_id'], $gatewayIds);

        $dustIp = array();
        if ($node['protocol'] == 'dust_ip') {
        	$dustIp['join_key_set'] = $node['join_key_set'];
        }
        
        $appConfigs = $this->nodeRepository->findNodeAppConfigs($node['id']);

        return $this->formatNodeForApiResponce($node, $labels, $users, $devices, $owned, $zwaveConfigs, $otherConfigs, $dustIp, $appConfigs);
    }

    public function getNodeLabels($userId, $nodeId) {
       	return $this->nodeRepository->findLabels($nodeId, $userId);
    }

    /**
     * ノードの情報を更新する
     * @param int $nodeId ノードID
     * @param int $userId ラベルについてユーザーごとの設定を行う場合のみ指定します。ノード単位の設定の場合には、null を指定してください。
     * @param array $labels ラベル文字列を含んだ配列
     * @param int $sharingLevel NODE_SHARING_LEVEL_XXX定数
     * @param array $sharedUsers 共有対象のユーザーの情報を含んだ配列
     * @param array $zwaveConfig zwave関連の設定
     * @param array $otherConfig その他の設定
     * @param array $app appについてのの設定
     * @param int $userIdIn ユーザID
     * @param array $returnJSON 返却用のJSON
     * @return 結果のステータスコード 202 or 204 or 400(Exception)
     */
    public function updateNode($nodeId, $userId, $labels, $sharingLevel, $sharedUsers,
                               $zwaveConfig, $otherConfig, $app, $userIdIn, &$returnJSON) {

        //ノードの存在確認
        $node = $this->nodeRepository->findById($nodeId);
        if(!$node) {
            throw new BadParameterException('無効なnode_idです。: ' . $nodeId);
        }

        //ユーザーの存在確認
        if (!is_null($userId))
        {
	        $user = $this->userRepository->findById($userId);
	        if(!$user) {
	        	throw new BadParameterException('無効なuser_idです。: ' . $userId);
	        }
        }
        
        //sharing_levelのフラグ値の確認
        $sharingLevelType = 0;
        if(!is_null($sharingLevel)) {
            $sharingLevelType = $this->nodeRepository->getSharingLevelTypeFromName($sharingLevel);
            if(!$sharingLevelType) {
                throw new BadParameterException('無効なsharing_levelです。: ' . $sharingLevel);
            }
            if($sharingLevelType == NODE_SHARING_LEVEL_USERS && is_null($sharedUsers)) {
                throw new BadParameterException('shared_withを指定して下さい。');
            }
        }

        //shared_withの一覧からユーザー名の一覧を作成
        $userIds = array();
        if(!is_null($sharedUsers)) {
            foreach($sharedUsers as $user) {
                if(isset($user['email'])) {
                    $user = $this->userRepository->findUserByEmail($user['email']);
                    if($user) {
                        $userIds[] = $user['id'];
                    } else {
                    	throw new BadParameterException('無効なshared_userです。');
                    }
                } else if(isset($user['id'])) {
                    $user = $this->userRepository->findUserByLoginId($user['id']);
                    if($user) {
                        $userIds[] = $user['id'];
                    } else {
                    	throw new BadParameterException('無効なshared_userです。');
                    }
                } else if(isset($user['url'])) {
                    $user = $this->userRepository->findByUrl($user['url']);
                    if($user) {
                        $userIds[] = $user['id'];
                    } else {
                    	throw new BadParameterException('無効なshared_userです。');
                    }
                } else {
                	throw new BadParameterException('無効なshared_userです。');
                }
                
                // 自分自身は指定できない
                if ($user['id'] == $userIdIn) {
                	throw new BadParameterException('無効なshared_userです。');
                }
            }
        }

        $db = DB::getMasterDb();
        //nodes_usersテーブルの更新中にデッドロックが発生する可能性があるため、
        //READ-COMMITTEDに変更する。
        $db->SetTransactionIsolationLevel('READ COMMITTED');
        $db->Begin();

        //labelの更新
        if(!is_null($labels)) {
            $this->nodeRepository->updateLabels($nodeId, $userId, $labels);
        }

        //nodes_usersの更新
        if(!is_null($sharedUsers)) {
        	$this->nodeRepository->updateSharedUsers($nodeId, $userIds);
        }

        //更新結果のユーザー数を確認する。
        //ユーザー数が0の場合、sharering_levelをprivateに変更する。
        $userCount = count($this->nodeRepository->findSharedUsers($nodeId));
        if($userCount == 0) {
            $sharingLevelType = NODE_SHARING_LEVEL_PRIVATE;
        }

        if($sharingLevelType != 0) {
            $this->nodeRepository->updateSharingLevel($nodeId, $sharingLevelType);
        }

        $db->Commit();

        //mod shima s
        
        // 返却コード 初期化
        $returnCode = 204;
        $returnZwave = array();
        
        // AMQP で更新処理をサーバに投げる
        // zwave 関連
        if (is_array($zwaveConfig) && count($zwaveConfig) > 0) {
            // 配列分 クラウドに通知する
            foreach ((array)$zwaveConfig as $record) {
            	$config = array(
            			'command' => 'node_config_update',
            			'node_id' => (int)$nodeId,
            			'config_type' => 'zwave',
            			'param_type' => $record['type'],
            			'param_size' => (int)$record['size'],
            			'param_id' => $record['parameter_no'],
            			'value' => $record['value'],
            	);

            	$notification = $this->sendMessageToCloud($config);

                // 返却された処理結果は、200（正常）もしくは300（処理中）ならばOK
                $resultCode = $notification['result_code'];
                if($resultCode != 200 && $resultCode != 300) {
                    throw new ZworksApiException(var_export($notification['reason'], true));
                } else if($resultCode == 300) {
        			$returnCode = 202;
                	// create response body
                    $returnZwave[] = array(
                        'parameter_no' => $notification['param_id'],
                        'type' => $notification['param_type'],
                    	'size' => $notification['param_size'],
                        'value' => $notification['value'],
                        'update_pending' => ($resultCode == 300),
                    );
                }
            	
                // NodeConfig を DB に保存する
                $this->nodeRepository->saveNodeConfig($config, $resultCode);
            }
        }
        
        // other 関連
        $returnOther = array();
        if (is_array($otherConfig) && count($otherConfig) > 0) {
            // 配列分 クラウドに通知する
            foreach ((array)$otherConfig as $record) {
            	$config = array(
            		'command' => 'node_config_update',
            		'node_id' => (int)$nodeId,
            		'config_type' => 'other',
            		'param_type' => $record['type'],
            		'param_id' => $record['parameter'],
            		'value' => $record['value'],
            	);
            	
            	$notification = $this->sendMessageToCloud($config);

                // 返却された処理結果は、200（正常）もしくは300（処理中）ならばOK
                $resultCode = $notification['result_code'];
                if($resultCode != 200 && $resultCode != 300) {
                    throw new ZworksApiException(var_export($notification['reason'], true));
                } elseif($resultCode == 300) {
       			 	$returnCode = 202;
                	// create response body
                    $returnOther[] = array(
                        'type' => $notification['param_type'],
                        'parameter' => $notification['param_id'],
                        'value' => $notification['value'],
                        'update_pending' => ($resultCode == 300),
                    );
                }
                
                // NodeConfig を DB に保存する
                $this->nodeRepository->saveNodeConfig($config, $resultCode);
            }
        }

        // app 関連
        $returnApp = array();
        if (is_array($app) && count($app) > 0) {

            // wipe any earlier app fields saved in database
            $this->nodeRepository->wipePreviousAppSpecificNodeConfig($nodeId);

            // construct nodeName to fill in the amqp request
            $nodeName = sprintf('%s_%s_%s', $node['manufacturer'], $node['product'], $node['serial_no']);

            // 配列分 クラウドに通知する
            foreach ((array)$app as $record) {
            	$config = array(
            		'command' => 'app_sp_node_config_update',
            		'app_name' => $record['app_name'],
            		'node_name' => $nodeName,
            		'configurations' => $record['configurations'],
            	);
            	$notification = $this->sendMessageToCloud($config);
                $resultCode = $notification['result_code'];
                if($resultCode != 200) {
                    throw new ZworksApiException(var_export($notification['reason'], true));
                }

                // App specific config を DB に保存する
                $this->nodeRepository->saveAppSpecificNodeConfig($nodeId, $config);

                $returnApp[] = [
                    'app_name' => $config['app_name'],
                    'configurations' => $config['configurations'],
                ];
            }
        }
        
        //返却用のJSONデータの作成
        $users = $this->userRepository->findUserByNodeId($node['id']);
        $devices = $this->nodeRepository->findDevices($node['id']);
        $gateways = $this->gatewayRepository->findByUserId($userIdIn);
        $gatewayIds = array();
        foreach($gateways as $gateway) {
            $gatewayIds[] = (int)$gateway['id'];
        }
        $owned = in_array($node['gateway_id'], $gatewayIds);
        
        $returnJSON = $this->formatNodeForApiResponce(
                        $node, $labels, $users, $devices, $owned, $returnZwave, $returnOther, null, $returnApp);
        return $returnCode;

        //mod shima e
    }

    /**
     * shima add function
     * Node Update を クラウドに通知する
     * 当関数はvaridation check 済みのものが送られてくるものとする
     * @param array $request Cloudに送信する内容が含まれる配列
     */
    public function sendMessageToCloud($request) {

        //Adapterに問い合わせる
        $this->zworksRequestAdapter->sendMessage($request);

        //結果をポーリングする
        $received = $this->zworksRequestAdapter->waitNotification();
        if(!$received) {
            throw new ApiTimeoutException('サーバーからの応答の受信がタイムアウトしました.');
		}
		$notification = $this->zworksRequestAdapter->getResult();
		
		$this->zworksRequestAdapter->complete();
		
		return $notification;
	}
	
	/**
	 * ノードのラベル情報を更新する
	 * 
	 * @param int $nodeId        	
	 * @param int $userId ユーザーごとの設定を行う場合のみ指定します。ノード単位の設定の場合には、null を指定してください。
	 * @param array $labels        	
	 */
	public function updateNodeLabels($nodeId, $userId, $labels) {
		
		// ノードの存在確認
		$node = $this->nodeRepository->findById($nodeId);
		if(!$node) {
			throw new BadParameterException('無効なnode_idです。: ' . $nodeId);
		}
		
		// ユーザーの存在確認
		if(!is_null($userId)) {
			$user = $this->userRepository->findById($userId);
			if(!$user) {
				throw new BadParameterException('無効なuser_idです。: ' . $userId);
			}
		}
		$db = DB::getMasterDb();
		// nodes_usersテーブルの更新中にデッドロックが発生する可能性があるため、
		// READ-COMMITTEDに変更する。
		$db->SetTransactionIsolationLevel('READ COMMITTED');
		$db->Begin();
		
		// labelの更新
		if(!is_null($labels)) {
			$this->nodeRepository->updateLabels($nodeId, $userId, $labels);
		}
		
		$db->Commit();
    }
    
    /**
     * ノードの情報を削除する
     * @param int $nodeId ノードID
     */
    public function deleteNode($nodeId) {
    	$nodeId = (int)$nodeId;

        //ノードの存在確認
        $node = $this->nodeRepository->findById($nodeId);
        if(!$node) {
            throw new BadParameterException('無効なnode_idです。: ' . $nodeId);
        }

        $gateway = $this->gatewayRepository->findById($node['gateway_id']);

        $db = DB::getMasterDb();
        $db->Begin();
        
        $request = array(
            'command' => 'node_removal',
            'gw_name' => $gateway['mac_address'],
            'node_id' => (int)$nodeId,
            'replace' => false,
        );

        //Adapterに問い合わせる
        $this->zworksRequestAdapter->sendMessage($request);

        //結果をポーリングする
        $received = $this->zworksRequestAdapter->waitNotification();
        if(!$received) {
            throw new ApiTimeoutException('Node Removal Requestの応答の受信がタイムアウトしました.');
        }
        $notification = $this->zworksRequestAdapter->getResult();

        $resultCode = $notification['result_code'];
        if($resultCode != 200) {
            throw new ZworksApiException(var_export($notification['reason'], true));
        }

        $this->nodeRepository->excludeNode($nodeId);

        $db->Commit();
    }


    /**
     * AMQPの通知の内容からNodeの登録を行う。
     * @param string $type Notification Type
     * @param int $gatewayId ゲートウェイID
     * @param array $nodeInfo AMQPのGateway Association Notificationに含まれる
     *        ノード1件分の情報を含んだ配列
     */
    public function registerNodeFromNotification($type, $gatewayId, $nodeInfo) {
        $gateway = $this->gatewayRepository->findById($gatewayId);
        $oldNode = $this->nodeRepository->findById($nodeInfo['node_id']);
        $newUserId = $gateway['user_id'];
        $oldUserId = isset($oldNode['prev_user_id']) ? $oldNode['prev_user_id'] : 0;
        $failed_cmd = isset($nodeInfo['failed_cmd']) ? $nodeInfo['failed_cmd'] : array();
        $failed_cmd = json_encode($failed_cmd);
        $state = 0 < count($failed_cmd) ? NODE_STATE_FAILED : NODE_STATE_INCLUDED;
        $reason = (string)$type;
        $isSameOwner = ($newUserId == $oldUserId);

        //node_idが既に存在する場合は内容を更新する。
        $node = array(
            'id'            => $nodeInfo['node_id'],
            'gateway_id'    => $gatewayId,
            'protocol'      => NODE_PROTOCOL_ZWAVE, //現時点ではzwaveのみ
            'sharing_level' => NODE_SHARING_LEVEL_PRIVATE,
            'location_type' => '',
            'location_name' => '',
        	'state'         => $state,
        	'failed_cmd'    => $failed_cmd,
        	'reason'        => $reason,
        );

        $this->nodeRepository->save($nodeInfo['node_id'], $node);
       
        if(!$isSameOwner) {
            //所有者が以前の所有者と一致しない場合、ラベルやノード共有の設定を削除する。
            $this->nodeRepository->removeAllLabels($nodeInfo['node_id']);
            $this->nodeRepository->updateSharedUsers($nodeInfo['node_id'], array());
        }

        $devices = array();
        foreach ($nodeInfo['devices'] as $deviceInfo) {
            $devices[] = array(
                'id'   => $deviceInfo['device_id'],
                'type' => $this->deviceRepository->getDeviceTypeFromName($deviceInfo['device_type']),
            );
            $this->tileRepository->addUserTiles($newUserId, $deviceInfo['device_id'], 'device');
            //所有者が以前の所有者と一致しない場合、ラベルやノード共有の設定を削除する。
            if(!$isSameOwner) {
                $this->deviceRepository->removeAllLabels($deviceInfo['device_id']);
                $this->deviceRepository->removeAllData($deviceInfo['device_id']);
            }
        }
        $this->deviceRepository->replaceDevies($nodeInfo['node_id'], $devices);

        $actions = array();
        foreach ($nodeInfo['actions'] as $actionInfo) {
            $actions[] = array(
                'type'        => $this->nodeRepository->getActionTypeFromName($actionInfo['type']),
                'description' => $actionInfo['description'],
            );
        }
        $this->nodeRepository->updateActions($nodeInfo['node_id'], $actions);
    }


    /**
     * DBのレコード情報をAPIのレスポンス用に整形して返す。
     * @param array $node nodesテーブルのエントリ(1行)
     * @param array $labels ラベル情報の配列
     * @param array $users usersテーブルのエントリ(複数行)
     * @param array $devices devicesテーブルのエントリ(複数行)
     * @param boolean $owned ノードの所有者用の形式でレスポンスを返すかどうか
     * @return array HTTP APIのレスポンス用に整形した配列
     */
    public function formatNodeForApiResponce($node, $labels, $users, $devices, $owned, $zwaveConfig, $otherConfig, $dustIp, $returnApp) {
        $deviceUrls = array();
        foreach((array)$devices as $device) {
            $deviceUrls[] = sprintf('/api/v3/devices/%d', $device['id']);
        }
        $userUrls = array();
        $gatewayUrl = '';
        $nodeName = sprintf('%s_%s_%s', $node['manufacturer'], $node['product'], $node['serial_no']);
        
        if($owned) {
            foreach((array)$users as $user) {
                $userUrls[] = array(
                    'id' => (int)$user['login_id'],
                    'email' => $user['email'],
                    'url' => sprintf('/api/v3/users/%d', $user['login_id']),
                );
            }
            
        	$gatewayUrl = sprintf('/api/v3/gateways/%d', $node['gateway_id']);
        }
        
        $result = array(
            'id' => (int)$node['id'],
            'name' => $nodeName,
        	'labels' => (array)$labels,
            'state' => $this->nodeRepository->getNodeYetAnotherStateNameFromType($node['yet_another_state']),
            'gateway' => $gatewayUrl,
            'protocol' => (string)$node['node_type'],
            'sharing_level' => $this->nodeRepository->getSharingLevelNameFromType($node['sharing_level']),
            'devices' => $deviceUrls,
        	'shared_with' => $userUrls,
            'zwave' => array(
                'devices' => $deviceUrls,		// deprecated.
                'manufacturer' => (string)$node['manufacturer'],
                'product' => (string)$node['product'],
                'serial_no' => (string)$node['serial_no'],
                'state' => (string)$this->nodeRepository->getNodeStateNameFromType($node['state']),
                'failed_cmd' => (array)json_decode($node['failed_cmd']),
                'updated_at' => $node['updated'],
            ),
            'zwave_configurations' => $zwaveConfig,
			'configurations' => $otherConfig,
		);
        
        if ($dustIp != null && $result['protocol'] == 'dust_ip') {
        	$result['dust_ip'] = $dustIp;
        }

        if ($node['node_type'] == 'dust_ip') {
            $result['app'] = $returnApp;
        }
        
        return $result;
    }
}
