<?php

/**
 * @file
 * @brief シナリオについてのロジックを定義するクラス
 */

namespace Lcas\Service;


use Lcas\Adapter\ZworksRequestAdapterInterface;
use Lcas\DB\DB;
use Lcas\Exception\ApiTimeoutException;
use Lcas\Exception\BadParameterException;
use Lcas\Exception\ZworksApiException;
use Lcas\Repository\ScenarioRepository;
use Lcas\ServiceContainer;
use Lcas\Repository\TileRepository;


/**
 * @class ScenarioService
 * @brief シナリオについてのロジックを定義するクラス
 */
class ScenarioService {

    /**
     * @var ScenarioRepository
     */
    private $scenarioRepository;
    
     /**
     * @var TileRepository
     */
    private $tileRepository;

    /**
     * @var ZworksRequestAdapterInterface
     */
    private $zworksRequestAdapter;
    
    /**
     * コンストラクタ
     */
    public function __construct() {
        $serviceContainer = ServiceContainer::getInstance();
        $this->scenarioRepository = new ScenarioRepository();
        $this->tileRepository = new TileRepository();
        $this->zworksRequestAdapter = $serviceContainer->get('zworks_request_adapter');
    }

    /**
     * ユーザーが保持しているScenarioの一覧を返す。
     * @param int $userId users.id
     * @param string $label ラベルによる絞り込みを行う場合、キーワード
     * @return array HTTP APIのレスポンス用の情報を含んだ配列
     */
    public function getUserScenarioList($userId, $label='') {
        $scenarios = $this->scenarioRepository->findByUserId($userId);

        $filtered = false;
        $scenarioIds = array();
        if(strlen($label) > 0) {
            $filtered = true;
            $scenarioIds = $this->scenarioRepository->findScenarioIdListByLabel($label);
        }

        $scenarioList = array();
        foreach($scenarios as $scenario) {
            if($filtered && !in_array($scenario['id'], $scenarioIds)) {
                continue;
            }
            $scenarioId = $scenario['id'];
            $labels = $this->scenarioRepository->findLabels($scenarioId);
            $conditions = $this->scenarioRepository->findConditions($scenarioId);
            $actions = $this->scenarioRepository->findActions($scenarioId);

            $scenarioList[] = $this->formatScenarioForApiResponce($scenario, $labels, $conditions, $actions);
        }

        return $scenarioList;
    }


    /**
     * 指定されたシナリオの詳細情報を返す
     * @param int $scenarioId scenarios.id
     * @return array HTTP APIのレスポンス用の情報を含んだ配列
     */
    public function getScenarioDetail($scenarioId) {
        $scenario = $this->scenarioRepository->findById($scenarioId, true);
        if(!$scenario) {
            return false;
        }
        $labels = $this->scenarioRepository->findLabels($scenarioId);
        $conditions = $this->scenarioRepository->findConditions($scenarioId);
        $actions = $this->scenarioRepository->findActions($scenarioId);

        return $this->formatScenarioForApiResponce($scenario, $labels, $conditions, $actions);
    }


    /**
     * 指定されたシナリオのお知らせ(シナリオイベントの通知履歴)の一覧を返す
     * @param int $scenarioId scenarios.id
     * @param int $from 取得開始日時(ミリ秒単位のUNIXタイムスタンプ)
     * @param int $to 取得開始日時(ミリ秒単位のUNIXタイムスタンプ)
     * @param int $offset 取得開始位置
     * @param int $limit 取得件数
     * @return array HTTP APIのレスポンス用の情報を含んだ配列
     */
    public function getEventList($scenarioId, $from, $to, $offset, $limit) {
        $scenarioId = (int)$scenarioId;

        $options = array();
        if($from > 0) {
            $options['from'] = $this->scenarioRepository->convertMilliSecToMicroSecString($from);
        }
        if($to > 0) {
            $options['to'] = $this->scenarioRepository->convertMilliSecToMicroSecString($to);
        }
        if($offset > 0) {
            $options['offset'] = (int)$offset;
        }
        if($limit > 0) {
            $options['limit'] = (int)$limit;
        }

        $events = $this->scenarioRepository->findEvents($scenarioId, $options);

        $result = array();
        foreach($events as $event) {
            $result[] = $this->formatEventForApiResponse($event);
        }
        return $result;
    }


    /**
     * 指定されたユーザーのお知らせ(シナリオイベントの通知履歴)の一覧を返す
     * @param int $userId users.id
     * @param int $from 取得開始日時(ミリ秒単位のUNIXタイムスタンプ)
     * @param int $to 取得開始日時(ミリ秒単位のUNIXタイムスタンプ)
     * @param int $offset 取得開始位置
     * @param int $limit 取得件数
     * @return array HTTP APIのレスポンス用の情報を含んだ配列
     */
    public function getUserEventList($userId, $from, $to, $offset, $limit) {
        $userId = (int)$userId;

        $options = array();
        if($from > 0) {
            $options['from'] = $this->scenarioRepository->convertMilliSecToMicroSecString($from);
        }
        if($to > 0) {
            $options['to'] = $this->scenarioRepository->convertMilliSecToMicroSecString($to);
        }
        if($offset > 0) {
            $options['offset'] = (int)$offset;
        }
        if($limit > 0) {
            $options['limit'] = (int)$limit;
        }

        $events = $this->scenarioRepository->findEventsByUserId($userId, $options);

        $result = array();
        foreach($events as $event) {
            $result[] = $this->formatEventForApiResponse($event);
        }
        return $result;
    }


    /**
     * POSTデータからシナリオの登録を行う
     * @param array $user usersテーブルのレコード
     * @param array $newScenario POSTデータ
     * @return array HTTP APIのレスポンス用の情報を含んだ配列
     */
    public function registerScenario($user, $newScenario) {
        $db = DB::getMasterDb();
        //トランザクション中でDELETE+INSERTしている箇所でデッドロックが発生する可能性があるため、
        //READ-COMMITTEDに変更する。
        $db->SetTransactionIsolationLevel('READ COMMITTED');
        $db->Begin();

        //最初にLCAS内のDBにシナリオを保存する
        //入力値チェックも含む
        $scenarioId = $this->saveScenario($user['id'], 0, $newScenario);

        //enabled = falseで登録された場合、クラウドには通知せずに処理を返す。
        if(!$newScenario['enabled']) {
            $response = array(
                'id' => (string)$scenarioId
            );
            $db->Commit();
            return $response;
        }

        $newScenario['scenario_id'] = $scenarioId;
        $this->sendMessageToCloud('register', 'scenario_reg', $user['id'], $newScenario);

        $db->Commit();

        $response = array(
            'id' => (int)$scenarioId
        );
        return $response;
    }


    /**
     * HTTP API形式のシナリオデータをLCAS内のDBに保存(登録/更新)する
     * @param int $userId users.id
     * @param int $scenarioId シナリオID (新規シナリオの場合には、0 を指定する)
     * @param array $scenario HTTP API形式のデータを含んだ配列
     * @return int シナリオID
     */
    public function saveScenario($userId, $scenarioId, $scenario) {
        $scenarioType = $this->scenarioRepository->getScenarioTypeFromName($scenario['scenario_type']);
        if(!$scenarioType) {
            throw new BadParameterException('無効なシナリオタイプが指定されました。 scenario_type: ' . $scenario['scenario_type']);
        }
        if (!isset($scenario['labels']) || !is_array($scenario['labels']) || count($scenario['labels'])==0) {
            throw new BadParameterException('labelsが指定されていません。');
		}
       	if (!isset($scenario['actions']) || !is_array($scenario['actions']) || count($scenario['actions'])==0) {
            throw new BadParameterException('actionsが指定されていません。');
		}
        if (isset($scenario['condition_operator'])) {
	        $conditionOperator = $this->scenarioRepository->getConditionOperatorTypeFromName($scenario['condition_operator']);
	        if(!$conditionOperator) {
	            throw new BadParameterException('無効なcondition_operatorが指定されました。 scenario_type: ' . $scenario['condition_operator']);
	        }
        }
        if(!is_bool($scenario['enabled'])) {
            throw new BadParameterException('enabledに無効な値が指定されました。enabled: ' . $scenario['enabled']);
        }
        if(isset($scenario['auto_config'])) {
        	if(!is_bool($scenario['auto_config'])) {
       	 		throw new BadParameterException('無効なauto_configが指定されました。auto_config: ' . $scenario['auto_config']);
        	}
        } else {
        	$scenario['auto_config'] = false;
        }
        
        $errors = array();
        if (isset($scenario['conditions'])) {
	        foreach($scenario['conditions'] as $condition) {
	            if(!$this->validateCondition($condition, $errors)) {
	                throw new BadParameterException(implode("\n", $errors));
	            }
	        }
        }
        foreach($scenario['actions'] as $action) {
            if(!$this->validateAction($action, $errors)) {
                throw new BadParameterException(implode("\n", $errors));
            }
        }

        //scenario
        $data = array(
            'user_id' => $userId,
            'scenario_type' => $scenarioType,
            'enabled' => $scenario['enabled'],
        	'auto_config' => $scenario['auto_config']
        );
        
        if (isset($scenario['condition_operator'])) {
	        $data['condition_operator'] = $conditionOperator;
        }
        if (isset($scenario['trigger_after'])) {
	        $data['trigger_after'] = $scenario['trigger_after'];
        }
        if (isset($scenario['temperature_dev'])) {
			if (!is_numeric($scenario['temperature_dev'])) {
				throw new BadParameterException('無効なtemperature_devが指定されました。temperature_dev: ' . $scenario['temperature_dev']);
			}
	        $data['temperature_dev'] = $scenario['temperature_dev'];
        }
        if (isset($scenario['humidity_dev'])) {
			if (!is_numeric($scenario['humidity_dev'])) {
				throw new BadParameterException('無効なhumidity_devが指定されました。humidity_dev: ' . $scenario['humidity_dev']);
			}
	        $data['humidity_dev'] = $scenario['humidity_dev'];
        }
        if (isset($scenario['wbgt_threshold'])) {
	        if (!is_numeric($scenario['wbgt_threshold'])) {
				throw new BadParameterException('無効なwbgt_thresholdが指定されました。wbgt_threshold: ' . $scenario['wbgt_threshold']);
			}
	        $data['wbgt_threshold'] = $scenario['wbgt_threshold'];
        }
        if (isset($scenario['scxml'])) {
	        $data['scxml'] = $scenario['scxml'];
        }
        if (isset($scenario['device_list'])) {
        	if (!is_array($scenario['device_list'])) {
        		throw new BadParameterException('無効なdevice_listが指定されました。device_list: ' . $scenario['device_list']);
        	}
        	foreach ($scenario['device_list'] as $device) {
        		if (!is_numeric($device)) {
        			throw new BadParameterException('無効なdevice_listが指定されました。device_list: ' . $scenario['device_list']);
        		}
        	}
	        $data['device_list'] = implode(',', $scenario['device_list']);
        }
        if (isset($scenario['action_state_list'])) {
        	if (!is_array($scenario['action_state_list'])) {
        		throw new BadParameterException('無効なaction_state_listが指定されました。action_state_list: ' . $scenario['action_state_list']);
        	}
	        $data['action_state_list'] = implode(',', $scenario['action_state_list']);
        }
        if (isset($scenario['enabled_period'])) {
			if (preg_match("/^(\d{2}):(\d{2})-(\d{2}):(\d{2})$/", $scenario['enabled_period'], $matches)) {
				if (!is_numeric($matches[1]) || !is_numeric($matches[2]) || !is_numeric($matches[3]) || !is_numeric($matches[4])) {
					throw new BadParameterException('無効なenabled_periodが指定されました。enabled_period: ' . $scenario['enabled_period']);
				}
				$left = (int)$matches[1] * 60 + (int)$matches[2];
				$right = (int)$matches[3] * 60 + (int)$matches[4];
				if ($left == $right) {
					throw new BadParameterException('無効なenabled_periodが指定されました。enabled_period: ' . $scenario['enabled_period']);
				}
			} else {
				throw new BadParameterException('無効なenabled_periodが指定されました。enabled_period: ' . $scenario['enabled_period']);
			}
	        $data['enabled_period'] = $scenario['enabled_period'];
        }
        if (isset($scenario['enabled_days'])) {
        	$data['enabled_days'] = implode(',', $scenario['enabled_days']);
			if (!$this->validateEnableDays($scenario['enabled_days'])) {
				throw new BadParameterException('無効なenable_daysが指定されました。enabled_days: ' . $data['enabled_days']);
			}
        }
        if($scenarioId == 0) {
            $scenarioId = $this->scenarioRepository->registerScenario($data);
        } else {
            $this->scenarioRepository->updateScenario($scenarioId, $data);
        }

        //conditions
        if (isset($scenario['conditions'])) {
	        $newConditions = array();
	        foreach((array)$scenario['conditions'] as $condition) {
	            $newConditions[] = array(
	                'lhs' => $condition['lhs'],
	                'operator' => $this->scenarioRepository->getOperatorTypeFromName($condition['operator']),
	                'rhs' => $condition['rhs'],
	                'unit' => isset($condition['unit']) ? $condition['unit'] : '',
	            );
	        }
	        $this->scenarioRepository->updateConditions($scenarioId, $newConditions);
        }

        //actions
        $newActions = array();
        foreach((array)$scenario['actions'] as $action) {
            $newActions[] = array(
                'type' => $this->scenarioRepository->getActionTypeFromName($action['type']),
                'content' => $action['content'],
            );
        }
        $this->scenarioRepository->updateActions($scenarioId, $newActions);

        //labels
        $labels = array();
        if(isset($scenario['labels'])) {
        	$labels = $scenario['labels'];
        }
        if (!empty($labels)) {
        	$this->scenarioRepository->updateLabels($scenarioId, $labels);
        }
        $this->tileRepository->addUserTiles($userId, $scenarioId, 'scenario');
        return $scenarioId;
    }

    /**
     * POSTデータからシナリオの更新を行う
     * @param array $user usersテーブルのレコード
     * @param int $scenarioId scenarios.id
     * @param array $newScenario POSTデータ
     */
    public function modifyScenario($user, $scenarioId, $newScenario) {
        $oldScenario = $this->scenarioRepository->findById($scenarioId);
        $oldEnabled = $oldScenario['enabled'];


        $db = DB::getMasterDb();

        //トランザクション中でDELETE+INSERTしている箇所でデッドロックが発生する可能性があるため、
        //READ-COMMITTEDに変更する。
        $db->SetTransactionIsolationLevel('READ COMMITTED');
        $db->Begin();
        //結果をDBに取り込む
        $this->saveScenario($user['id'], $scenarioId, $newScenario);

        $messageMode = '';
        if($oldEnabled && $newScenario['enabled']) {
            //enabledがtrue -> true(変更前 -> 変更後)の場合、クラウドにはScenario Modificationを送信する
            $messageMode = 'scenario_mod';
        } else if($oldEnabled && !$newScenario['enabled']) {
            //enabledがtrue -> falseの場合、クラウドにはScenario Deregistrationを送信する
            $messageMode = 'scenario_dereg';
        } else if(!$oldEnabled && $newScenario['enabled']) {
            //enabledがfalse -> trueの場合、クラウドにはScenario Registrationを送信する
            $messageMode = 'scenario_reg';
        } else if(!$oldEnabled && !$newScenario['enabled']) {
            //enabledがfalse -> falseの場合、クラウドには何も通知を行わない。
        }
        
        if($messageMode != '') {
        	$newScenario['scenario_id'] = $scenarioId;
        	$this->sendMessageToCloud('modify', $messageMode, $user['id'], $newScenario);
        }

        $db->Commit();
    }


    /**
     * シナリオ関連データをクラウドに通知する
     * (シナリオの登録/更新/削除共通で使用する処理)
     *
     * @param string $originalAction 通知を送信するきっかけとなった操作の名前(register,modify,removeで区別)
     * @param string $mode AMQPのメッセージ種別(scenario_regなど)
     * @param int $userId ユーザーID(users.id)
     * @param array $scenario シナリオ情報(Scenario Registration Requestなどで使用する形式)
     */
    public function sendMessageToCloud($originalAction, $mode, $userId, $scenario) {
        $request = array();
        $extraData = array();

        switch($mode) {
            case 'scenario_reg':
            case 'scenario_mod':
                $request = array(
                    'command' => $mode,
                    'scenario_id' => (string)$scenario['scenario_id'],
                    'scenario_type' => $scenario['scenario_type'],
                    'actions' => $scenario['actions'],
                );
                if (isset($scenario['condition_operator'])) {
                	$request['condition_operator'] = $scenario['condition_operator'];
                }
                if (isset($scenario['conditions'])) {
                	$request['conditions'] = $scenario['conditions'];
                }
                if (isset($scenario['trigger_after'])) {
                	$request['trigger_after'] = $scenario['trigger_after'];
                }
                if (isset($scenario['temperature_dev'])) {
                	$request['temperature_dev'] = $scenario['temperature_dev'];
                }
                if (isset($scenario['humidity_dev'])) {
                	$request['humidity_dev'] = $scenario['humidity_dev'];
                }
                if (isset($scenario['wbgt_threshold'])) {
                	$request['wbgt_threshold'] = $scenario['wbgt_threshold'];
                }
                if (isset($scenario['scxml'])) {
                	$request['scxml'] = $scenario['scxml'];
                }
                if (isset($scenario['device_list'])) {
                	$request['device_list'] = $scenario['device_list'];
                }
                if (isset($scenario['action_state_list'])) {
                	$request['action_state_list'] = $scenario['action_state_list'];
                }
                if (isset($scenario['enabled_period'])) {
                	$request['enabled_period'] = $scenario['enabled_period'];
                }
                if (isset($scenario['enabled_days'])) {
                	$request['enabled_days'] = $scenario['enabled_days'];
                }
                
                if (isset($request['conditions'])) {
	                foreach($request['conditions'] as $idx=>$condition) {
	                    if($this->scenarioRepository->getOperatorTypeFromName($condition['operator']) == SCENARIO_OPERATOR_FREQ) {
	                        unset($request['conditions'][$idx]['unit']);
	                    }
	                }
                }
                //Scenario xxx NotificationはユーザーIDを含まないので、
                //extraDataとしてuser_idを記録しておく
                $extraData = array(
                    'user_id' => $userId,
                    'labels' => $scenario['labels'],
                    'enabled' => $scenario['enabled'],
                    'original_action' => $originalAction,
                );
                break;

            case 'scenario_dereg':
                $request = array(
                    'command' => 'scenario_dereg',
                    'scenario_id' => (string)$scenario['scenario_id'],
                );
                //Scenario xxx NotificationはユーザーIDを含まないので、
                //extraDataとしてuser_idを記録しておく
                $extraData = array(
                    'user_id' => $userId,
                    'original_action' => $originalAction,
                );

                //$originalActionがmodifyの場合、遅延メッセージ処理時に
                //シナリオ情報を丸ごと更新する必要があるため、もともとのシナリオ情報もextra_dataに持たせておく
                if($originalAction == 'modify') {
                    $extraData['original_data'] = $scenario;
                }
                break;
        }

        //Adapterに問い合わせる
        $this->zworksRequestAdapter->sendMessage($request, $extraData);

        //結果をポーリングする
        $received = $this->zworksRequestAdapter->waitNotification();
        if(!$received) {
            throw new ApiTimeoutException('サーバーからの応答の受信がタイムアウトしました.');
        }
        $notification = $this->zworksRequestAdapter->getResult();

        $resultCode = $notification['result_code'];
        if($resultCode != 200) {
            throw new ZworksApiException(var_export($notification['reason'], true));
        }

        $this->zworksRequestAdapter->complete();
    }


    /**
     * POSTデータからシナリオの削除を行う
     * @param int $scenarioId scenarios.id
     */
    public function removeScenario($scenarioId) {
        $db = DB::getMasterDb();
        $db->Begin();

        $scenario = $this->scenarioRepository->findById($scenarioId, true);

        //結果をDBに取り込む
        $this->removeScenarioFromLcas($scenarioId);
        
        if($scenario['enabled'] == 1) {
            //シナリオが有効だった場合のみクラウドにScenario Deregistrationのメッセージを送信する。
            
	        // sendMessageToCloud であつかえる形式にする必要があるため 'scenario_id' に シナリオIDを転記する。 
	        $scenario['scenario_id'] = $scenarioId;

        	$this->sendMessageToCloud('remove', 'scenario_dereg', (int)$scenario['user_id'], $scenario);
        }

        $db->Commit();
    }


    /**
     * 指定されたシナリオをLCAS上から削除する。
     * @param int $scenarioId
     */
    public function removeScenarioFromLcas($scenarioId) {
        $this->scenarioRepository->removeAllActions($scenarioId);
        $this->scenarioRepository->removeAllConditions($scenarioId);
        $this->scenarioRepository->removeAllLabels($scenarioId);
        $this->scenarioRepository->removeAllEvents($scenarioId);
        $this->scenarioRepository->removeScenario($scenarioId);
    }
    
    public function removeUserScenarios($userId) {
    	$scenarios = $this->scenarioRepository->findByUserId($userId, true);
    	foreach ($scenarios as $scenario) {
    		$this->removeScenario($scenario['id']);
    	}
    }

    public function hideTileScenario($scenarioId, $value) {
        $scenarios = $this->scenarioRepository->hideTile($scenarioId, $value);
    }


    /**
     * DBから取得したScenario関連のデータをAPIのレスポンス用に整形して返す。
     * @param array $scenario scenariosテーブルのレコード
     * @param array $labels ラベル文字列を含んだ配列
     * @param array $conditions 複数のscenario_conditionsテーブルのレコードを含んだ配列
     * @param array $actions 複数のscenario_actionsテーブルのレコードを含んだ配列
     * @return array
     */
    public function formatScenarioForApiResponce($scenario, $labels, $conditions, $actions) {
        $scenarioType = $this->scenarioRepository->getScenarioTypeNameFromType($scenario['scenario_type']);
        $conditionOperator = $this->scenarioRepository->getConditionOperatorTypeNameFromType($scenario['condition_operator']);

        $conditionList = array();
        foreach($conditions as $condition) {
            $c = array(
                'lhs' => (string)$condition['lhs'],
                'operator' => $this->scenarioRepository->getOperatorTypeNameFromType($condition['operator']),
                'rhs' => (string)$condition['rhs'],
            );
            if($c['operator'] != SCENARIO_OPERATOR_FREQ && isset($condition['unit']) && strlen($condition['unit']) > 0) {
                $c['unit'] = $condition['unit'];
            }
            $conditionList[] = $c;
        }

        $actionList = array();
        foreach($actions as $action) {
            $actionList[] = array(
                'type' => $this->scenarioRepository->getActionTypeNameFromType($action['type']),
                'content' => $action['content'],
            );
        }

        $data = array(
            'id' => (int)$scenario['id'],
            'scenario_type' => $scenarioType,
            'labels' => $labels,
            'condition_operator' => $conditionOperator,
            'conditions' => $conditionList,
            'trigger_after' => (int)$scenario['trigger_after'],
            'actions' => $actionList,
            'tile_status' => $scenario['tile_status'] ? true : false,
            'enabled' => $scenario['enabled'] ? true : false,
        );
        if (isset($scenario['temperature_dev'])) {
        	$data['temperature_dev'] = (int)$scenario['temperature_dev'];
        }
        if (isset($scenario['humidity_dev'])) {
        	$data['humidity_dev'] = (int)$scenario['humidity_dev'];
        }
        if (isset($scenario['wbgt_threshold'])) {
        	$data['wbgt_threshold'] = (int)$scenario['wbgt_threshold'];
        }
        if (isset($scenario['scxml'])) {
        	$data['scxml'] = $scenario['scxml'];
        }
        if (isset($scenario['device_list'])) {
        	$data['device_list'] = array();
        	$devices = explode(',', $scenario['device_list']);
        	foreach ($devices as $device) {
        		array_push($data['device_list'], (int)$device);
        	}
        }
        if (isset($scenario['action_state_list'])) {
        	$data['action_state_list'] = explode(',', $scenario['action_state_list']);
        }
        if (isset($scenario['enabled_period'])) {
        	$data['enabled_period'] = $scenario['enabled_period'];
        }
        if (isset($scenario['enabled_days'])) {
        	$data['enabled_days'] = explode(',', $scenario['enabled_days']);
        }
        
        return $data;
    }


    /**
     * DBから取得したシナリオのイベント(お知らせ)関連のデータをAPIのレスポンス用に整形して返す。
     * @param array $event scenario_eventsテーブルのレコード
     * @return array
     */
    public function formatEventForApiResponse($event) {
        $data = array(
            'id' => (int)$event['scenario_id'],
            'timestamp' => (int)$this->scenarioRepository->convertMicroTimeToMilliSec($event['created']),
        );
        return $data;
    }


    /**
     * シナリオ条件の配列のフォーマットが正しいかを検証する
     * @param array $condition 1件分のシナリオ条件の情報を含んだ配列
     * @param array &$errors エラーメッセージを格納する変数への参照
     * @return bool 検証結果OKの場合true、NGの場合false
     */
    public function validateCondition($condition, &$errors) {
        $errors = array();
        if(!isset($condition['lhs'])) {
            $errors[] = 'lhsが指定されていません。';
        }
        if(!isset($condition['rhs'])) {
            $errors[] = 'rhsが指定されていません。';
        }
        if(!isset($condition['operator'])) {
            $errors[] = 'operatorが指定されていません。';
        }
	if($condition['operator'] == 'freq') {
		if(preg_match("/(\d+)-(\d+)/", $condition['rhs'], $matches)) {
			if(!is_array($matches)) {
				$errors[] = '無効なrhsが指定されました。 rhs: ' . $condition['rhs'];
				return false;
			} else if(count($matches) != 3) {
				$errors[] = '無効なrhsが指定されました。 rhs: ' . $condition['rhs'];
				return false;
			} else if(!ctype_digit($matches[1]) || !ctype_digit($matches[2])) {
				$errors[] = '無効なrhsが指定されました。 rhs: ' . $condition['rhs'];
				return false;
			}
			$num1 = (int)$matches[1];
			$num2 = (int)$matches[2];
			if($num1 > $num2) {
				$errors[] = '無効なrhsが指定されました。 rhs: ' . $condition['rhs'];
				return false;
			}
		} else {
			if($condition['rhs'] != 'almost_none' &&
				$condition['rhs'] != 'low' &&
				$condition['rhs'] != 'med' &&
				$condition['rhs'] != 'high') {
				$errors[] = '無効なrhsが指定されました。 rhs: ' . $condition['rhs'];
				return false;
			}
		}
	} else if(!ctype_digit($condition['rhs'])) {
		$errors[] = '無効なrhsが指定されました。 rhs: ' . $condition['rhs'];
		return false;
	}
        $operator = $this->scenarioRepository->getOperatorTypeFromName($condition['operator']);
        if(!$operator) {
            $errors[] = '無効なoperatorが指定されました。 operator: ' . $condition['operator'];
        }
        return (count($errors) == 0) ? true : false;
    }

    /**
     * シナリオのactionのフォーマットが正しいかを検証する
     * @param array $action 1件分のactionの情報を含んだ配列
     * @param array &$errors エラーメッセージを格納する変数への参照
     * @return bool 検証結果OKの場合true、NGの場合false
     */
    public function validateAction($action, &$errors) {
        $errors = array();
        if(!isset($action['type'])) {
            $errors[] = 'typeが指定されていません。';
        }
        if(!isset($action['content'])) {
            $errors[] = 'contentが指定されていません。';
        }
        $actionType = $this->scenarioRepository->getActionTypeFromName($action['type']);
        if(!$actionType) {
            $errors[] = '無効なtypeが指定されました。 type: ' . $action['type'];
        }
        return (count($errors) == 0) ? true : false;
    }

    /**
     * シナリオのenable_daysのフォーマットが正しいかを検証する
     * @param array $enable_days 1件分のenable_dayの情報を含んだ配列
     * @return bool 検証結果OKの場合true、NGの場合false
     */
    public function validateEnableDays($enable_days) {
	    if (is_null($enable_days) || !is_array($enable_days)) {
		    return false;
	    }
	    $unique_array = array_unique($enable_days);
	    if (count($unique_array) != count($enable_days)) {
		    return false;
	    }
	    foreach($enable_days as $enable_day) {
		    if($enable_day != 'sun' &&
			    $enable_day != 'mon' &&
			    $enable_day != 'tue' &&
			    $enable_day != 'wed' &&
			    $enable_day != 'thu' &&
			    $enable_day != 'fri' &&
			    $enable_day != 'sat') {
			return false;
		    }
	    }
	    return true;
    }
}
