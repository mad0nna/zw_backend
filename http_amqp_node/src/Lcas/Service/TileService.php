<?php

/**
 * @file
 * @brief ユーザー関連のロジックを定義した配列
 */

namespace Lcas\Service;

use Lcas\Exception\BadParameterException;
use Lcas\Repository\TileRepository;

/**
 * @class TileService
 * @brief
 */
class TileService
{
	/**
	 * @var TileRepository
	 */
	private $tileRepository;

	/**
	 * コンストラクタ
	 */
	public function __construct() {

		$this->tileRepository = new TileRepository();
	}

	/**
	 * Updates the Dashboard tiles
	 * 
	 * @param int $userId
	 * @param array $tiles
	 * @return bool $result
	 */
	public function updateTiles($userId, $tiles) {
		if(intval($userId) === 0) {
			throw new BadParameterException('Invalid parameter userId passed, must be a valid integer.');
		}

		if(is_array($tiles) !== true) {
			throw new BadParameterException('Invalid parameter tiles passed, must be a valid array.');
		}

		$data = [];

		foreach($tiles as $tile) {
			$data[] = [
				'user_id' => $userId,
				'entity_id' => $tile['entity_id'],
				'type' => $tile['type'],
				'sequence' => $tile['sequence']
			];
		}

		$result = $this->tileRepository->updateTiles($data);

		return $result;
	}

	/**
	 * Get user tiles
	 * 
	 * @param int $userId
	 * @return bool $result
	 */
	public function getUserTiles($userId) {
		if(intval($userId) === 0) {
			throw new BadParameterException('Invalid parameter userId passed, must be a valid integer.');
		}

		$result = $this->tileRepository->findUserTiles($userId);
		return $result;
	}
}