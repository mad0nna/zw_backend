<?php

/**
 * @file
 * @brief ユーザー関連のロジックを定義した配列
 */

namespace Lcas\Service;

use Lcas\Adapter\MobilePushAdapterInterface;
use Lcas\Exception\ApiTimeoutException;
use Lcas\Exception\BadParameterException;
use Lcas\Exception\LcasException;
use Lcas\Exception\ZworksApiException;
use Lcas\Repository\UserRepository;
use Lcas\DB\DB;
use Lcas\ServiceContainer;
use Lcas\Repository\UserTokenRepository;
use Lcas\Helper\DateHelper;
use Lcas\Repository\MailTemplateRepository;
use Lcas\Helper\MailHelper;
use Lcas\Repository\NodeRepository;


/**
 * @class UserService
 * @brief ユーザー関連のロジックを定義した配列
 */
class UserService {

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var NodeRepository
     */
    private $nodeRepository;

    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;
    
    /**
     * @var MailTemplateRepository
     */
    private $mailTemplateRepository;
    
    /**
     * @var \Lcas\Adapter\ZworksRequestAdapterInterface
     */
    private $zworksRequestAdapter;
    
    /**
     * @var ScenarioService
     */
    private $scenarioService;


    /**
     * コンストラクタ
     */
    public function __construct() {
        $serviceContainer = ServiceContainer::getInstance();

        $this->userRepository = new UserRepository();
        $this->nodeRepository = new NodeRepository();
        $this->userTokenRepository = new UserTokenRepository();
        $this->mailTemplateRepository = new MailTemplateRepository();
        
        $this->scenarioService = new ScenarioService();
        
        $this->zworksRequestAdapter = $serviceContainer->get('zworks_request_adapter');
    }


    /**
     * ユーザー情報の更新を行う(指定された項目のみ)。
     * @param int $userId users.id
     * @param string $email
     * @param string $password
     * @param string $platform platformを表す文字列
     * @param string $token トークン文字列。$platformが"none"以外の場合は必須
     * @param array $labels ラベル
     */
    public function updateUser($userId, $email, $password, $platform, $token, $labels) {
        //user情報をDBに登録する
        $updateInfo = array();
        if(!is_null($email)) {
            //登録済メールアドレスかどうかを確認する。
            $duplicatedUser = $this->userRepository->findUserByEmail($email, array('exclude_id' => $userId));
            if(isset($duplicatedUser['id'])) {
                throw new BadParameterException('無効なメールアドレスです。');
            }
            $updateInfo['email'] = $email;
        }
        if(!is_null($password)) {
            $updateInfo['pass'] = $password;
            if(!defined('DEBUG_ALLOW_SIMPLE_PASSWORD') || !DEBUG_ALLOW_SIMPLE_PASSWORD) {
                if(!$this->verifyPasswordFormat($password)) {
                    throw new BadParameterException(sprintf('パスワードは%s〜%s文字以内で、英字・数字・記号(%s)のうち2種類以上を組み合わせてください。',
                        PASSWORD_LENGTH_MIN, PASSWORD_LENGTH_MAX, PASSWORD_SPECIAL_CHARS));
                }
            }
        }
        if(!is_null($platform)) {
            $updateInfo['platform'] = $platform;
            $platformType = $this->userRepository->getPlatformTypeFromName($platform);
            if(!$platform) {
                throw new BadParameterException('無効なプラットフォームです。');
            }
            if($platformType != PLATFORM_NONE && $platformType != PLATFORM_SQS && strlen($token) == 0) {
                throw new BadParameterException('tokenが指定されていません。');
            }
            if(!is_null($token)) {
                $updateInfo['token'] = $token;
            }
        }
        if(!is_null($labels)) {
        	$updateInfo['labels'] = $labels;
        }

        $this->userRepository->update($userId, $updateInfo);

        if(!is_null($token)) {
            $user = $this->userRepository->findById($userId);
            /**
             * @var MobilePushAdapterInterface $mobilePushAdapter
             */
            $mobilePushAdapter = ServiceContainer::getInstance()->get('mobile_push_adapter');
            $pushId = $mobilePushAdapter->registerToken($user['platform'], $token);
            $this->userRepository->updatePushEndpoint($userId, $pushId);
        }
    }


    /**
     * ユーザー登録解除のリクエストをz-works.ioへ送信する。
     * @param string $userName ユーザー名
     */
    public function sendDeregisterUserRequest($userName) {

        $request = array(
            'command' => 'user_dereg',
            'username' => $userName,
        );

        //Adapterに問い合わせる
        $db = DB::getMasterDb();
        $db->Begin();
        $this->zworksRequestAdapter->sendMessage($request);

        //結果をポーリングする
        $received = $this->zworksRequestAdapter->waitNotification();

        if(!$received) {
            throw new ApiTimeoutException('User Deregistration Notificationの受信がタイムアウトしました.');
        }
        $notification = $this->zworksRequestAdapter->getResult();

        $resultCode = $notification['result_code'];
        if($resultCode != 200) {
            throw new ZworksApiException(var_export($notification['reason'], true));
        }

        //この時点ではユーザー登録時のトランザクションはロールバックしているはずのため、
        //特に解約は行わない
        $this->zworksRequestAdapter->complete();
        $db->Commit();
        return;
    }


    /**
     * パスワードのフォーマットが正しいかを確認する
     * @param string $password
     * @return bool
     */
    public function verifyPasswordFormat($password) {
        $length = strlen($password);

        if($length < PASSWORD_LENGTH_MIN || $length > PASSWORD_LENGTH_MAX) {
            return false;
        }

        $variationCount = 0;
        if(preg_match('/[A-Za-z]/', $password)) {
            $variationCount++;
        }
        if(preg_match('/[0-9]/', $password)) {
            $variationCount++;
        }
        $passwordSpecialChars = preg_quote(PASSWORD_SPECIAL_CHARS, '/');
        if(preg_match("/[{$passwordSpecialChars}]/", $password)) {
            $variationCount++;
        }

        if($variationCount < 2) {
            return false;
        }

        return true;
    }
    
    /**
     * confirmation_token を使用してアカウント登録を完了させる
     * @param array $confTokenInfo confirmation_token の情報配列
     * @param string $password
     * @param string $platform
     * @param string $token
     * @param array $labels
     * @return array 処理結果
     * @throws BadParameterException
     * @throws LcasException
     * @throws ApiTimeoutException
     * @throws ZworksApiException
     */
    private function registerUserByToken($confTokenInfo, $password, $platform, $token, $labels) {
    	$confirmationToken = $confTokenInfo['token'];
    	$cTokenType = (int)$confTokenInfo['token_type'];
    	$email = $confTokenInfo['email'];
    	$origin = $confTokenInfo['origin'];
    	
    	$platform = (is_null($platform) || strlen($platform) == 0) ? UserRepository::getPlatformNameFromType($confTokenInfo['platform']) : $platform;
    	$token = (is_null($token) || strlen($token) == 0) ? $confTokenInfo['platform_token'] : $token;

    	if ($cTokenType !== USER_CONFIRM_TOKEN_TYPE_NEW_USER) {
    		throw new BadParameterException('無効なconfirmation_tokenです');
    	}
    	
        if(is_null($platform) || strlen($platform) == 0) {
            throw new BadParameterException('platform が指定されていません。');
        } else if(($platform == 'apns' || $platform == 'gcm') && (is_null($token) || strlen($token) == 0)) {
            throw new BadParameterException('token が指定されていません。');
        }

        if(!defined('DEBUG_ALLOW_SIMPLE_PASSWORD') || !DEBUG_ALLOW_SIMPLE_PASSWORD) {
            if(!$this->verifyPasswordFormat($password)) {
                throw new BadParameterException(sprintf('パスワードは%s〜%s文字以内で、英字・数字・記号(%s)のうち2種類以上を組み合わせてください。',
                      PASSWORD_LENGTH_MIN, PASSWORD_LENGTH_MAX, PASSWORD_SPECIAL_CHARS));
            }
        }

        $emailDuplicatedUser = $this->userRepository->findUserByEmail($email);
        if($emailDuplicatedUser) {
            //同じメールアドレスのユーザーが存在する場合、エラー
            throw new BadParameterException('このメールアドレスは既に登録されています。 :' . $email);
        }

        $newUserId = $this->userRepository->generateNewLoginId();
        if(!$newUserId) {
            throw new LcasException('ユーザーIDの発行に失敗しました。');
        }
        //AMQPの通信に使用するusernameは初期はusers.loginIdと同じ値を使用する。
        //usernameは仕様上文字列として定義されており、将来的にusernameを変更する
        //APIが追加される可能性がある。
        $newUserName = (string)$newUserId;

        $request = array(
            'command' => 'user_reg',
            'username' => $newUserName,
        );

        $db = DB::getMasterDb();
        $db->Begin();

        //Adapterに問い合わせる
        $this->zworksRequestAdapter->sendMessage($request);
        
        $db->Commit();

        //結果をポーリングする
        $received = $this->zworksRequestAdapter->waitNotification();

        if(!$received) {
            throw new ApiTimeoutException('User Registration Notificationの受信がタイムアウトしました.');
        }
        $notification = $this->zworksRequestAdapter->getResult();

        $resultCode = $notification['result_code'];
        if($resultCode != 200) {
            throw new ZworksApiException(var_export($notification['reason'], true));
        }
        
        $db->Begin();

        //user情報をDBに登録する
        $user = array(
            'email' => $email,
            'name' => $newUserName,
            'login_id' => $newUserId,
            'pass' => $password,
            'platform' => $platform,
            'token' => $token,
            'origin' => $origin,
        );

        $userId = $this->userRepository->register($user);
        
        $this->userRepository->updateLabels($userId, $labels);

        $platformType = $this->userRepository->getPlatformTypeFromName($platform);
        /**
         * @var MobilePushAdapterInterface $mobilePushAdapter
         */
        $mobilePushAdapter = ServiceContainer::getInstance()->get('mobile_push_adapter');
        $pushId = $mobilePushAdapter->registerToken($platformType, $token);
        $this->userRepository->updatePushEndpoint($userId, $pushId);


        $this->zworksRequestAdapter->complete();
        
        // Token を削除
        $this->userTokenRepository->deleteToken($confirmationToken);
        
        $db->Commit();

        $response = array(
            'result_code' => $resultCode,
            'response' => array(
                'id' => $newUserId,
            )
        );
        return $response;
    }
    
    /**
     * confirmation_token を使用してメールアドレスの変更を完了させる
     * @param array $confTokenInfo confirmation_token の情報配列
     * @param string $password
     * @param string $platform
     * @param string $token
     * @param array $labels
     * @throws BadParameterException
     */
    private function updateUserByToken($confTokenInfo, $password, $platform, $token, $labels) {
    	$confToken = $confTokenInfo['token'];
    	$email = $confTokenInfo['email'];
    	$userId = $confTokenInfo['user_id'];
    	
    	$tokenType = (int)$confTokenInfo['token_type'];
    	if ($tokenType !== USER_CONFIRM_TOKEN_TYPE_CHANGE_EMAIL) {
    		throw new BadParameterException('無効なconfirmation_tokenです');
    	}

    	$db = DB::getMasterDb();
    	$db->Begin();
    	
    	// token に含まれるメールアドレスをユーザー情報に転記する
    	$this->updateUser($userId, $email, $password, $platform, $token, $labels);
    	
    	// token を削除する
    	$this->userTokenRepository->deleteToken($confToken);
    	
    	$db->Commit();
    }

    /**
     * アカウント登録時の confirmation_token をメール送信する
     * @param sting $email 送信先メールアドレス
     * @param int $platform
     * @param string $platform_token
     * @param string $origin Originヘッダの内容
     */
    public function sendRegistUserTokenMail($email, $platform, $platform_token, $origin) {
    	$this->sendTokenMail(null, $email, $platform, $platform_token, $origin, USER_CONFIRM_TOKEN_TYPE_NEW_USER);
    }

    /**
     * メールアドレス変更時の confirmation_token をメール送信する
     * @param int $userId user_id
     * @param string $email 送信先メールアドレス
     * @param string $origin Originヘッダの内容
     */
    public function sendUpdateUserTokenMail($userId, $email, $platform, $platform_token, $origin) {
    	$this->sendTokenMail($userId, $email, $platform, $platform_token, $origin, USER_CONFIRM_TOKEN_TYPE_CHANGE_EMAIL);
    }

    /**
     * パスワードリセット時の reset_token をメール送信する
     * @param int $userId user_id
     * @param string $origin Originヘッダの内容
     */
    public function sendResetPasswordTokenMail($userId, $email, $platform, $platform_token, $origin) {
    	$this->sendTokenMail($userId, $email, $platform, $platform_token, $origin, USER_CONFIRM_TOKEN_TYPE_RESET_PASSWORD);
    }
    
    /**
     * token を含むメールを送信する
     * @param int $userId
     * @param string $email
     * @param int $platform
     * @param string $platform_token
     * @param string $origin
     * @param int $tokenType
     * @throws BadParameterException
     * @throws LcasException
     */
    private function sendTokenMail($userId, $email, $platform, $platform_token, $origin, $tokenType) {
    	$userId = (int)$userId;
    	$platform = (int)$platform;
    	$tokenType = (int)$tokenType;
    	
    	$mailTemplateType = MailTemplateRepository::convertMailTemplateTypeFromTokenType($tokenType);
    	if (!$mailTemplateType) {
    		throw new BadParameterException('送信しようとしているTokenの種別が正しくありません');
    	}
    	 
    	if ($tokenType !== USER_CONFIRM_TOKEN_TYPE_RESET_PASSWORD) {
	    	// 指定された email がほかのユーザーで登録済みであるかを確認する
	    	if ($this->userRepository->findUserByEmail($email)) {
	    		// 登録済みならば、この呼び出しは拒否する
	    		throw new BadParameterException('すでにこのメールアドレスは使用されています');
	    	}
    	}
    	
    	// 期限切れではない confirmation_token のうち token と共に記録されているメールアドレスと email が一致しているものがあることを確認する
    	if ($token = $this->userTokenRepository->findTokenByEmail($email)) {
			$tmpToken = $token['token'];
			$tmpTokenType = (int)$token['token_type'];
			$tmpCreated = $token['created'];
			
			// 一致している token が 引数で指定された token_type と同一ならば、生成されて 30 分以上経過していることを確認する
			if ($tmpTokenType === $tokenType) {
				
				$diff_minutes = DateHelper::getDateTimeDiff('i', $tmpCreated);
			
				// 30 分未満の場合、この呼び出しは拒否する
				if ($diff_minutes < 30) {
					throw new BadParameterException('このメールアドレスで有効な Token が存在しています');
				}
				// 30 分以上の場合、その token をDBから削除して、処理を続行する
				else {
					$db = DB::getMasterDb();
					$db->Begin();
					$this->userTokenRepository->deleteToken($tmpToken);
					$db->Commit();
				}
			}
			// 一致している token が引数で指定された token_type と異なるならば、この呼び出しは拒否する
			else {
				throw new BadParameterException('すでにこのメールアドレスは使用されています');
			}
    	}
    	
    	// 期限切れの Token を削除する
		$db = DB::getMasterDb();
		$db->Begin();
    	$this->userTokenRepository->deleteExpiredToken();
    	$db->Commit();
    	 
    	// confirmation_token を生成してDBに保存
    	$newToken = $this->userTokenRepository->generateToken();
    	if ($newToken === false) {
    		throw new LcasException('Tokenの発行に失敗しました');
    	}
    	$db = DB::getMasterDb();
    	$db->Begin();
    	$this->userTokenRepository->registerToken($newToken, $tokenType, $email, $platform, $platform_token, $origin, $userId);
    	$db->Commit();
    	 
    	// メールテンプレートを取得
    	$mailTemplate = $this->mailTemplateRepository->findMailTemplateByOrigin($origin, $platform, $mailTemplateType, false);
    	if ($mailTemplate === false) {
	    	$mailTemplate = $this->mailTemplateRepository->findMailTemplateByOrigin($origin, null, $mailTemplateType, true);
    	}
    	 
    	// メールの Body を作成
    	$mailBody = MailTemplateRepository::formatBody(
    			$mailTemplate['content'],
    			$email,
    			$newToken,
    			$newToken);
    			 
    	// メールを送信
    	MailHelper::sendMail(
    		$mailTemplate['sender'],
    		$email,
    		$mailTemplate['subject'],
    		$mailBody,
    		$mailTemplate['content-type']
    	);
    }

    /**
     * confirmation_token を確認し、要求されている処理を完了する
     * @param string $confToken confirmation_token
     * @param string $password
     * @param string $platform
     * @param string $token
     * @param array $labels
     * @throws BadParameterException
     */
    public function confirmUser($confToken, $password, $platform, $token, $labels) {
    	
    	// 期限切れの Token を削除する
		$db = DB::getMasterDb();
		$db->Begin();
    	$this->userTokenRepository->deleteExpiredToken();
    	$db->Commit();
    	
    	// confirmation_token の確認
    	$confTokenInfo = $this->userTokenRepository->findToken($confToken);
    	if (!$confTokenInfo) {
    		throw new BadParameterException('無効なconfirmation_tokenです');
    	}
    	$tokenType = (int)$confTokenInfo['token_type'];
    	if ($tokenType === USER_CONFIRM_TOKEN_TYPE_NEW_USER) {
    		$this->registerUserByToken($confTokenInfo, $password, $platform, $token, $labels);
    	} else if ($tokenType === USER_CONFIRM_TOKEN_TYPE_CHANGE_EMAIL) {
    		$this->updateUserByToken($confTokenInfo, $password, $platform, $token, $labels);
    	} else {
    		throw new BadParameterException('無効なconfirmation_tokenです');
    	}
    }
    
    /**
     * reset_token を検証して、パスワードの更新をおこなう
     * @param string $password
     * @param string $resetToken
     * @throws BadParameterException
     */
    public function resetUser($password, $resetToken) {

        // 現在の Token の情報を取得
    	$tokenInfo = $this->userTokenRepository->findToken($resetToken);
    	if(!$tokenInfo) {
    		throw new BadParameterException('無効なreset_tokenです。');
    	}
    	$userId = isset($tokenInfo['user_id']) ? (int)$tokenInfo['user_id'] : null;
    	if (is_null($userId)) {
    		throw new BadParameterException('無効なreset_tokenです。');
    	}
    	
    	$db = DB::getMasterDB();
        $db->Begin();
    	$this->updateUser($userId, null, $password, null, null);
        $this->userTokenRepository->deleteToken($resetToken);
        $db->Commit();
    }
    
    /**
     * ユーザーの削除をおこなう
     * @param array $user ユーザー情報
     */
    public function removeUser($user) {
    	
    	// scenarios の削除 (Cloud へ通知 -> 応答待ち)
    	try {
    	$this->scenarioService->removeUserScenarios($user['id']);
    	} catch (ZworksApiException $zworksApiException) {
    		// ZIO でシナリオの削除が失敗していてもLCAS上のユーザー削除処理は続ける
    	} catch (Exception $e) {
    		throw $e;
    	}
    		
    	$db = DB::getMasterDb();
    	$db->Begin();
    	
    	// nodes_users の削除
    	$this->userRepository->dissociateNodes($user['id']);
    		
    	// labels の削除
    	// node の user の label の削除
    	$this->nodeRepository->removeAllUserLabels($user['id']);
    	
    	// user の label の削除
    	$this->userRepository->removeAllLabels($user['id']);
    	 
    	// users の削除
    	$this->userRepository->deregister($user['id']);
    	 
	    // ZIO からユーザーを削除する
    	$this->sendDeregisterUserRequest($user['login_id']);

	    $db->Commit();
    }
}
