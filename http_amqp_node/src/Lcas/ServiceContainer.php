<?php

/**
 * @file
 * @brief サービスオブジェクトを管理するオブジェクト
 *
 * アプリケーションの実行環境に応じて切り替えが必要な
 * オブジェクトのインスタンスを設定段階で登録しておき、
 *
 * アプリケーション中で必要になった段階でサービスコンテナから
 * オブジェクトを取得することで、オブジェクトを利用する側が
 * オブジェクトの生成方法やクラス名に依存しないようにすることを意図しています。
 */

namespace Lcas;

/**
 * @class ServiceContainer
 * @brief サービスオブジェクトを管理するオブジェクト
 *
 * アプリケーションの実行環境に応じて切り替えが必要な
 * オブジェクトのインスタンスを設定段階で登録しておき、
 *
 * アプリケーション中で必要になった段階でサービスコンテナから
 * オブジェクトを取得することで、オブジェクトを利用する側が
 * オブジェクトの生成方法やクラス名に依存しないようにすることを意図しています。
 */
class ServiceContainer {

    private $services;

    /**
     * サービスコンテナのインスタンスを返す。
     * @return ServiceContainer
     */
    public static function getInstance() {
        static $instance = null;
        if(is_null($instance)) {
            $instance = new ServiceContainer();
        }
        return $instance;
    }


    /**
     * コンストラクタ
     */
    private function __construct() {

    }

    /**
     * 指定された名前に対応するクラスのインスタンスを返す
     * @param string $key サービス名
     * @return mixed 登録されたオブジェクト
     */
    public function get($key) {
        if(!isset($this->services[$key])) {
            throw new \RuntimeException('');
        }
        return $this->services[$key];
    }


    /**
     * サービスの登録を行う。
     * @param string $key サービス名
     * @param mixed $service サービスオブジェクト
     * @return void
     */
    public function set($key, $service) {
        return $this->services[$key] = $service;
    }

    /**
     * 指定された名前のサービスが登録されているかを返す。
     * @param string $key サービス名
     * @return boolean
     */
    public function have($key) {
        return isset($this->services[$key]);
    }
}
