LCAS Integration Test Tool
==========================================

## 概要
LCAS が仕様書通りに機能しているかを、各HTTP API, WebSocket APIを実際に実行して検証するツールです。

-----

## 前提条件

### ミドルウェア、RPMパッケージ
テストツールを動作させるために必要なミドルウェアやRPMパッケージは、
ｖ1.0納品物の"LCAS_新環境構築手順_v1.0.docx"をご参照ください。


### ディレクトリ構成
本ツールはLCAS本体の設定ファイルやライブラリの一部を参照しているため、
以下のディレクトリ構造で設置されることを想定しております。

```
/path/to/lcas/http_amqp_node/tests
```
(LCASの"http_amqp_node"ディレクトリ直下)


例：LCASが"/var/www/lcas"に設置されている場合
```
/var/www/lcas/http_amqp_node/tests
```

### AMQPサーバー
本ツールでは、テスト環境用のAMQPサーバー(RabbitMQなど)のインスタンスが必要です。
接続の際はSSLを使用せずに接続出来ることを想定しています。

-----

## インストール手順
以下のコマンドで、ツールの依存するライブラリをインストールします。

```
composer install
```

* PHPUnitは別途インストールをお願い致します。

-----

## 実行方法
本ツールは以下の手順で実行をお願い致します。

Jenkinsから実行して頂きたいコマンドは"テストの実行"セクションのコマンドです。

### テスト環境のLCAS本体の設定の確認
本ツールは、AMQPやDBなどの接続情報はLCAS本体の設定を参照しているため、
LCAS本体の設定に問題がないことを確認します。

例： PHP_ENV, NODE_ENVが"test"の場合、

    http_amqp_node/config/env/test/config_common.php
    ws_node/config/config_test.json

* DB接続先がテスト環境のものになっていること
* AMQPサーバーの接続先がテスト環境のものになっていること
  (非SSLで接続が可能なこと)
* AWS APIの設定が行われていること
* サービスの設定が、以下の通り設定されていること
  (実際にAMQPメッセージを発行し、AWS APIを実行する状態となっていること)
    
      $serviceContainer->set('zworks_request_adapter', new \Lcas\Adapter\ZworksRequestAdapterAmqp());

      $serviceContainer->set('mobile_push_adapter', new \Lcas\Adapter\MobilePushAdapterSNS());

* config_common.phpの"AMQP_POLLING_INTERVAL"が100(100ミリ秒)になっていること
  (テスト実行時間の短縮のためです)


### fake-zioプロセスの起動
テストツールを実行する前に、AMQP APIに対しダミーの応答を行う
"fake-zio"(ZIOの代わりにAMQP APIの応答を行うプロセス)を起動します。

以下のコマンドで起動します。

```
PHP_ENV=test nohup /usr/local/bin/php fake_zio.php > /dev/null &
```

* fake-zioの起動時に、必要なExchange、Queueの定義が行われます。
* nohupで実行する場合、何らかの理由によりプロセスが異常終了した場合に手動での再起動が必要になるため、

  可能であればsupervisordなどを使用しプロセスの監視をお願い致します。


### http-node, amqp-node, ws-nodeの起動
テスト環境のLCAS本体の各プロセスを起動させます。

### テストの実行
テスト本体は以下の方法で起動します。

* 通常時
テストツール本体は以下のコマンドで実行をお願い致します。

```
phpunit.phar -c /var/www/lcas/http_node/tests/phpunit.xml
```

* 実行環境名(PHP_ENV環境変数)が"test"以外の場合

```
PHP_ENV=xxx phpunit.phar -c /var/www/lcas/http_node/tests/phpunit.xml
```
