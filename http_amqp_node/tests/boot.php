<?php

$env = null;
if(isset($_SERVER['PHP_ENV'])) {
    $env = $_SERVER['PHP_ENV'];
} else {
    $env = 'test';
}
define('ENV', $env);

ini_set('date.timezone', 'Asia/Tokyo');

require_once __DIR__ . '/../config/config.php';
require_once __DIR__ . '/test_config.php';
