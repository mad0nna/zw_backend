<?php

/**
 * AMQPノードのエントリポイント。
 */

use Lcas\Test\Amqp\FakeZio;

ini_set('display_errors', 0);
ini_set('max_execution_time', 0);

if(!isset($_SERVER['PHP_ENV'])) {
    throw new RuntimeException('PHP_ENV 定数が定義されていません。');
}

//アプリケーションの環境を表す定数
$env = null;
if(isset($_SERVER['PHP_ENV'])) {
    $env = $_SERVER['PHP_ENV'];
} else {
    $env = 'test';
}
define('ENV', $env);

require_once __DIR__ . '/../config/config_amqp.php';
require_once __DIR__ . '/test_config.php';


$fakeZio = new FakeZio();
$fakeZio->run();
