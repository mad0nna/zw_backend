<?php


use Lcas\DB\DB;
use Lcas\Repository\DeviceRepository;
use Lcas\Test\Fixture\DeviceFixtureFactory;
use Lcas\Test\Fixture\GatewayFixtureFactory;
use Lcas\Test\Fixture\NodeFixtureFactory;
use Lcas\Test\Util\TestUtil;

class AbstractDeviceTestCase extends \Lcas\Test\IntegrationTestCase {


    /**
     * @var DeviceRepository
     */
    protected $deviceRepository;


    /**
     * @var GatewayFixtureFactory
     */
    protected $gatewayFactory;

    /**
     * @var DeviceFixtureFactory
     */
    protected $deviceFactory;

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->deviceRepository = new DeviceRepository();
        $this->gatewayFactory = new GatewayFixtureFactory();
        $this->deviceFactory = new DeviceFixtureFactory();
    }


    public function setUp() {
        parent::setUp();

        //テストデータを全削除
        TestUtil::clearDatabase();

        //デフォルトのユーザーを登録する
        $this->userUtil->registerDefaultUser();
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

    }


    protected function getDevices($label=null) {

        $queries = [];
        if(!is_null($label)) {
            $queries['label'] = $label;
        }

        return $this->httpClient->get('/api/v3/devices', ['query' => $queries]);
    }

    protected function getDeviceById($id) {
        return $this->httpClient->get('/api/v3/devices/' . $id);
    }


    protected function getDeviceValues($id, $options=[]) {

        return $this->httpClient->get("/api/v3/devices/{$id}/values", [
            'query' => $options,
        ]);
    }


    protected function updateDeviceLabels($deviceId, $labels=null) {

        $parameters = [];

        if(!is_null($labels)) {
            $parameters['labels'] = $labels;
        }

        return $this->httpClient->put('/api/v3/devices/' . $deviceId, [
            'body' => json_encode($parameters)
        ]);
    }

    protected function getDeviceData($deviceId, $from=null, $to=null, $isLatest=false) {

        $queries = [];
        if($isLatest) {
            $queries['latest'] = '';
        } else {
            if(!is_null($from)) {
                $queries['start'] = $from;
            }
            if(!is_null($to)) {
                $queries['end'] = $to;
            }
        }

        $this->httpClient->put('/api/v3/devices/' . $deviceId, [
            'query' => $queries
        ]);
    }

    protected function getNodeIdFromDeviceId($deviceId) {
        $db = DB::getMasterDb();

        $escapedDeviceId = $db->Escape($deviceId);

        $sql = "SELECT node_id FROM devices WHERE id='{$escapedDeviceId}'";
        $db->setSql($sql);

        if(!$db->query()) {
            throw new \Exception('クエリエラー SQL:' . $sql);
        }

        $result = $db->Fetch();
        return $result['node_id'];
    }

}
