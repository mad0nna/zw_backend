<?php


use Lcas\Test\Constraint\DeviceConstraint;

require_once __DIR__ . '/AbstractDeviceTestCase.php';

class GetDevicesIdTest extends AbstractDeviceTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @test
     */
    public function 指定デバイス参照_正常系_一般_自身のデバイスの参照() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $expectedDevices = $this->deviceFactory->getDeviceListFromMacAddress(TEST_DEFAULT_GATEWAY);
        $expectedDevice = $expectedDevices[1];
        $expectedDeviceId = $expectedDevice['device_id'];

        //ユーザーの全ノード一覧を取得
        $response = $this->getDeviceById($expectedDeviceId);
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //レスポンスのデバイス一覧が期待している内容か確認する
        $json = json_decode($response->getBody(), true);

        $expectedNodeId = $this->getNodeIdFromDeviceId($expectedDeviceId);
        $device = $json;

        $deviceConstraint = new DeviceConstraint($expectedDevice);
        $deviceConstraint->setNodeId($expectedNodeId);

        $this->assertThat($device, $deviceConstraint, "想定されたデバイス情報を含むこと");
    }


    /**
     * @test
     */
    public function 指定デバイス参照_正常系_ノード共有_ノード共有されたデバイスの参照() {

        //2人目のユーザーを登録してログイン
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $macAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($macAddress);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $expectedDevices = $sharedNode['devices'];
        $expectedDevice = $expectedDevices[1];
        $expectedDeviceId = $expectedDevice['device_id'];
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //ノード共有されたデバイスを参照する
        $response = $this->getDeviceById($expectedDeviceId);
        $this->assertEquals(200, $response->getStatusCode());

        $device = json_decode($response->getBody(), true);

        $deviceConstraint = new DeviceConstraint($expectedDevice);
        $deviceConstraint->setNodeId($sharedNodeId);

        $this->assertThat($device, $deviceConstraint, '想定されるデバイス情報を含んでいること');
    }


    /**
     * @test
     */
    public function 指定デバイス参照_正常系_ノード共有_ノード共有されたデバイスの参照_ゲートウェイ割り当てなし() {

        //2人目のユーザーを登録してログイン
        //このユーザーにはゲートウェイを割り当てない
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $expectedDevices = $sharedNode['devices'];
        $expectedDevice = $expectedDevices[1];
        $expectedDeviceId = $expectedDevice['device_id'];
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //ゲートウェイを割り当てていないユーザーでも
        //ノード共有されたゲートウェイが参照できることを確認する。
        $response = $this->getDeviceById($expectedDeviceId);
        $this->assertEquals(200, $response->getStatusCode());

        $device = json_decode($response->getBody(), true);

        $deviceConstraint = new DeviceConstraint($expectedDevice);
        $deviceConstraint->setNodeId($sharedNodeId);

        $this->assertThat($device, $deviceConstraint, '想定されるデバイス情報を含んでいること');
    }


    /**
     * @test
     */
    public function 指定デバイス参照_異常系_一般_他ユーザーのデバイスの参照() {

        //2人目のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $exampleGatewayMacAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($exampleGatewayMacAddress);

        $exampleGatewayData = $this->gatewayFactory->create($exampleGatewayMacAddress);
        $targetNode = $exampleGatewayData['nodes'][1];
        $targetDevice = $targetNode['devices'][1];

        //デフォルトのユーザーでログインし直す
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //ユーザーが所有していないノードを参照出来無いことを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use ($targetDevice){

            $this->getDeviceById($targetDevice['device_id']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     */
    public function 指定デバイス参照_異常系_一般_存在しないデバイスの参照() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //ユーザーが所有していないノードを参照出来無いことを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function(){

            $this->getDeviceById(1);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function 指定デバイス参照_異常系_ノード共有_ノード共有解除された状態でデバイスを参照() {
        //2人目のユーザーを登録してログイン
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $macAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($macAddress);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $sharedDevice = $sharedNode['devices'][1];
        $sharedDeviceId = $sharedDevice['device_id'];
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //ノード共有を解除する
        $this->nodeUtil->shareNodeWith($sharedNodeId, []);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //共有解除されたノードを参照出来ないこと
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($sharedDeviceId) {

            $this->getDeviceById($sharedDeviceId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }
}
