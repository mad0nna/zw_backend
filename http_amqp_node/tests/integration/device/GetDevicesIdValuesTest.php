<?php


use Lcas\DB\DB;
use Lcas\Test\Constraint\DeviceConstraint;
use Lcas\Test\Constraint\DeviceListConstraint;
use Lcas\Test\Constraint\DeviceValueConstraint;
use Lcas\Test\Constraint\NodeConstraint;
use Lcas\Test\Fixture\GatewayFixtureFactory;

require_once __DIR__ . '/AbstractDeviceTestCase.php';

class GetDevicesIdValuesTest extends AbstractDeviceTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @test
     */
    public function デバイスデータ一覧の参照_正常系_一般_全件取得() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $expectedDevices = $this->deviceFactory->getDeviceListFromMacAddress(TEST_DEFAULT_GATEWAY);
        $expectedDevice = $expectedDevices[1];
        $expectedDeviceId = $expectedDevice['device_id'];

        //Node Inclusionが取り込まれるまで2秒まつ
        sleep(4);

        //デバイスデータを数件送信する。
        $expectedDeviceData = [
            ['value' => '10.0', 'unit' => 'C'],
            ['value' => '10.5', 'unit' => 'C'],
            ['value' => '11.0', 'unit' => 'C'],
            ['value' => '11.5', 'unit' => 'C'],
            ['value' => '12.0', 'unit' => 'C'],
        ];
        foreach($expectedDeviceData as $deviceDataValue) {
            $this->sendDeviceDataNotificationByDeviceId($expectedDeviceId, $deviceDataValue['value'], $deviceDataValue['unit']);
            usleep(100 * 1000);
        }
        //Device Data Update Notificationが受信されるのを待つ
        sleep(1);

        //ユーザーの全デバイスデータ一覧を取得
        $response = $this->getDeviceValues($expectedDeviceId);
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //レスポンスのデバイス一覧が期待している内容か確認する
        $deviceDataList = json_decode($response->getBody(), true);

        foreach($expectedDeviceData as $idx=>$expectedValue) {
            $deviceValueConstraint = new DeviceValueConstraint($expectedValue);
            $this->assertThat($deviceDataList[$idx], $deviceValueConstraint, "想定されたデータを含んでいること({$idx})");
        }
    }


    /**
     * @test
     */
    public function デバイスデータ一覧の参照_正常系_一般_start指定あり() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $expectedDevices = $this->deviceFactory->getDeviceListFromMacAddress(TEST_DEFAULT_GATEWAY);
        $expectedDevice = $expectedDevices[1];
        $expectedDeviceId = $expectedDevice['device_id'];

        //Node Inclusionが取り込まれるまで2秒まつ
        sleep(4);

        //デバイスデータをタイムスタンプ指定付きで登録する
        $deviceDataFixture = [
            ['value' => '10.0', 'unit' => 'C', 'timestamp' => '2016-04-01 09:59:59.999'],
            ['value' => '10.5', 'unit' => 'C', 'timestamp' => '2016-04-01 10:00:00.000'],
            ['value' => '11.0', 'unit' => 'C', 'timestamp' => '2016-04-01 10:59:59.999'],
            ['value' => '11.5', 'unit' => 'C', 'timestamp' => '2016-04-01 11:00:00.000'],
            ['value' => '12.0', 'unit' => 'C', 'timestamp' => '2016-04-01 11:00:01.000'],
        ];
        foreach($deviceDataFixture as $deviceData) {
            $this->forceRegisterDeviceValue($expectedDeviceId, $deviceData['value'], $deviceData['unit'], $deviceData['timestamp']);
        }

        //ユーザーの全ノード一覧を取得(start指定あり)
        $startTime = strtotime('2016-04-01 10:00:00') * 1000;
        $expectedDeviceData = [  //GET devices/:id/valuesで取得できる想定のデバイスデータ
            ['value' => '10.5', 'unit' => 'C'],
            ['value' => '11.0', 'unit' => 'C'],
            ['value' => '11.5', 'unit' => 'C'],
            ['value' => '12.0', 'unit' => 'C'],
        ];
        $response = $this->getDeviceValues($expectedDeviceId, ['start' => $startTime]);
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //レスポンスのデバイス一覧が期待している内容か確認する
        $deviceDataList = json_decode($response->getBody(), true);

        foreach($expectedDeviceData as $idx=>$expectedValue) {
            $deviceValueConstraint = new DeviceValueConstraint($expectedValue);
            $this->assertThat($deviceDataList[$idx], $deviceValueConstraint, "想定されたデータを含んでいること({$idx})");
        }
    }


    /**
     * @test
     */
    public function デバイスデータ一覧の参照_正常系_一般_end指定あり() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $expectedDevices = $this->deviceFactory->getDeviceListFromMacAddress(TEST_DEFAULT_GATEWAY);
        $expectedDevice = $expectedDevices[1];
        $expectedDeviceId = $expectedDevice['device_id'];

        //Node Inclusionが取り込まれるまで2秒まつ
        sleep(4);

        //デバイスデータをタイムスタンプ指定付きで登録する
        $deviceDataFixture = [
            ['value' => '10.0', 'unit' => 'C', 'timestamp' => '2016-04-01 09:59:59.999'],
            ['value' => '10.5', 'unit' => 'C', 'timestamp' => '2016-04-01 10:00:00.000'],
            ['value' => '11.0', 'unit' => 'C', 'timestamp' => '2016-04-01 10:59:59.999'],
            ['value' => '11.5', 'unit' => 'C', 'timestamp' => '2016-04-01 11:00:00.000'],
            ['value' => '12.0', 'unit' => 'C', 'timestamp' => '2016-04-01 11:00:01.000'],
        ];
        foreach($deviceDataFixture as $deviceData) {
            $this->forceRegisterDeviceValue($expectedDeviceId, $deviceData['value'], $deviceData['unit'], $deviceData['timestamp']);
        }

        //ユーザーの全ノード一覧を取得(end指定あり)
        $endTime = strtotime('2016-04-01 11:00:00') * 1000;
        $expectedDeviceData = [  //GET devices/:id/valuesで取得できる想定のデバイスデータ
            ['value' => '10.0', 'unit' => 'C'],
            ['value' => '10.5', 'unit' => 'C'],
            ['value' => '11.0', 'unit' => 'C'],
            ['value' => '11.5', 'unit' => 'C'],
        ];
        $response = $this->getDeviceValues($expectedDeviceId, ['end' => $endTime]);
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //レスポンスのデバイス一覧が期待している内容か確認する
        $deviceDataList = json_decode($response->getBody(), true);

        foreach($expectedDeviceData as $idx=>$expectedValue) {
            $deviceValueConstraint = new DeviceValueConstraint($expectedValue);
            $this->assertThat($deviceDataList[$idx], $deviceValueConstraint, "想定されたデータを含んでいること({$idx})");
        }

        $this->assertCount(count($expectedDeviceData), $deviceDataList, '想定どおりの件数であること');
    }


    /**
     * @test
     */
    public function デバイスデータ一覧の参照_正常系_一般_start_end両方指定() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $expectedDevices = $this->deviceFactory->getDeviceListFromMacAddress(TEST_DEFAULT_GATEWAY);
        $expectedDevice = $expectedDevices[1];
        $expectedDeviceId = $expectedDevice['device_id'];

        //Node Inclusionが取り込まれるまで2秒まつ
        sleep(4);

        //デバイスデータをタイムスタンプ指定付きで登録する
        $deviceDataFixture = [
            ['value' => '10.0', 'unit' => 'C', 'timestamp' => '2016-04-01 09:59:59.999'],
            ['value' => '10.5', 'unit' => 'C', 'timestamp' => '2016-04-01 10:00:00.000'],
            ['value' => '11.0', 'unit' => 'C', 'timestamp' => '2016-04-01 10:59:59.999'],
            ['value' => '11.5', 'unit' => 'C', 'timestamp' => '2016-04-01 11:00:00.000'],
            ['value' => '12.0', 'unit' => 'C', 'timestamp' => '2016-04-01 11:00:01.000'],
        ];
        foreach($deviceDataFixture as $deviceData) {
            $this->forceRegisterDeviceValue($expectedDeviceId, $deviceData['value'], $deviceData['unit'], $deviceData['timestamp']);
        }

        //ユーザーの全ノード一覧を取得(start,end指定あり)
        $startTime = strtotime('2016-04-01 10:00:00') * 1000;
        $endTime = strtotime('2016-04-01 11:00:00') * 1000;
        $expectedDeviceData = [  //GET devices/:id/valuesで取得できる想定のデバイスデータ
            ['value' => '10.5', 'unit' => 'C'],
            ['value' => '11.0', 'unit' => 'C'],
            ['value' => '11.5', 'unit' => 'C'],
        ];
        $response = $this->getDeviceValues($expectedDeviceId, ['start' => $startTime, 'end' => $endTime]);
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //レスポンスのデバイス一覧が期待している内容か確認する
        $deviceDataList = json_decode($response->getBody(), true);

        foreach($expectedDeviceData as $idx=>$expectedValue) {
            $deviceValueConstraint = new DeviceValueConstraint($expectedValue);
            $this->assertThat($deviceDataList[$idx], $deviceValueConstraint, "想定されたデータを含んでいること({$idx})");
        }
        $this->assertCount(count($expectedDeviceData), $deviceDataList, '想定どおりの件数であること');
    }


    /**
     * @test
     */
    public function デバイスデータ一覧の参照_正常系_一般_最新値() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $expectedDevices = $this->deviceFactory->getDeviceListFromMacAddress(TEST_DEFAULT_GATEWAY);
        $expectedDevice = $expectedDevices[1];
        $expectedDeviceId = $expectedDevice['device_id'];

        //Node Inclusionが取り込まれるまで2秒まつ
        sleep(4);

        //デバイスデータをタイムスタンプ指定付きで登録する
        $deviceDataFixture = [
            ['value' => '10.0', 'unit' => 'C', 'timestamp' => '2016-04-01 09:59:59.999'],
            ['value' => '10.5', 'unit' => 'C', 'timestamp' => '2016-04-01 10:00:00.000'],
            ['value' => '11.0', 'unit' => 'C', 'timestamp' => '2016-04-01 10:59:59.999'],
            ['value' => '11.5', 'unit' => 'C', 'timestamp' => '2016-04-01 11:00:00.000'],
            ['value' => '12.0', 'unit' => 'C', 'timestamp' => '2016-04-01 11:00:01.000'],
        ];
        foreach($deviceDataFixture as $deviceData) {
            $this->forceRegisterDeviceValue($expectedDeviceId, $deviceData['value'], $deviceData['unit'], $deviceData['timestamp']);
        }

        //ユーザーの全ノード一覧を取得
        //(latestを指定、且つstart,end指定あり。この場合、latest指定が優先されること)
        $startTime = strtotime('2016-04-01 10:00:00') * 1000;
        $endTime = strtotime('2016-04-01 11:00:00') * 1000;
        $latest = true;
        $expectedDeviceData = ['value' => '12.0', 'unit' => 'C'];

        $response = $this->getDeviceValues($expectedDeviceId, [
            'start' => $startTime,
            'end' => $endTime,
            'latest' => true
        ]);
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //最新1件のみ取得できていることを確認する
        $deviceData = json_decode($response->getBody(), true);

        $deviceValueConstraint = new DeviceValueConstraint($expectedDeviceData);
        $this->assertThat($deviceData[0], $deviceValueConstraint, "想定されたデータを含んでいること");

        $this->assertCount(1, $deviceData, '想定どおりの件数であること');
    }


    /**
     * @test
     */
    public function デバイスデータ一覧の参照_正常系_ノード共有_ノード共有されたデバイスのデータの参照() {

        //2人目のユーザーを登録してログイン
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $macAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($macAddress);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $expectedDevices = $sharedNode['devices'];
        $expectedDevice = $expectedDevices[1];
        $expectedDeviceId = $expectedDevice['device_id'];
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //デバイスデータを数件送信する。
        $expectedDeviceData = [
            ['value' => '10.0', 'unit' => 'C'],
            ['value' => '10.5', 'unit' => 'C'],
            ['value' => '11.0', 'unit' => 'C'],
            ['value' => '11.5', 'unit' => 'C'],
            ['value' => '12.0', 'unit' => 'C'],
        ];
        foreach($expectedDeviceData as $deviceDataValue) {
            $this->sendDeviceDataNotificationByDeviceId($expectedDeviceId, $deviceDataValue['value'], $deviceDataValue['unit']);
            usleep(100 * 1000);
        }
        //Device Data Update Notificationが受信されるのを待つ
        sleep(1);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //ノード共有されたデバイスのデータを参照できること
        $response = $this->getDeviceValues($expectedDeviceId);
        $this->assertEquals(200, $response->getStatusCode());

        $deviceDataList = json_decode($response->getBody(), true);

        foreach($expectedDeviceData as $idx=>$expectedValue) {
            $deviceValueConstraint = new DeviceValueConstraint($expectedValue);
            $this->assertThat($deviceDataList[$idx], $deviceValueConstraint, "想定されたデータを含んでいること({$idx})");
        }
    }


    /**
     * @test
     */
    public function デバイスデータ一覧の参照_異常系_一般_他ユーザーのデバイスのデータの参照() {

        //2人目のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $exampleGatewayMacAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($exampleGatewayMacAddress);

        $exampleGatewayData = $this->gatewayFactory->create($exampleGatewayMacAddress);
        $targetNode = $exampleGatewayData['nodes'][1];
        $targetDevice = $targetNode['devices'][1];

        //デフォルトのユーザーでログインし直す
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //ユーザーが所有していないノードを参照出来無いことを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use ($targetDevice){

            $this->getDeviceValues($targetDevice['device_id']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     */
    public function デバイスデータ一覧の参照_異常系_一般_存在しないデバイスのデータの参照() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //ユーザーが所有していないノードを参照出来無いことを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function(){

            $this->getDeviceValues(1);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @dataProvider getDataFor_デバイスデータ一覧の参照_異常系_異常値指定
     */
    public function デバイスデータ一覧の参照_異常系_異常値指定($key, $value) {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $expectedDevices = $this->deviceFactory->getDeviceListFromMacAddress(TEST_DEFAULT_GATEWAY);
        $expectedDevice = $expectedDevices[1];
        $expectedDeviceId = $expectedDevice['device_id'];

        //Node Inclusionが取り込まれるまで2秒まつ
        sleep(4);

        //デバイスデータを数件送信する。
        $expectedDeviceData = [
            ['value' => '10.0', 'unit' => 'C'],
        ];
        foreach($expectedDeviceData as $deviceDataValue) {
            $this->sendDeviceDataNotificationByDeviceId($expectedDeviceId, $deviceDataValue['value'], $deviceDataValue['unit']);
            usleep(100 * 1000);
        }
        //Device Data Update Notificationが受信されるのを待つ
        sleep(1);

        //パラメータに異常な値を指定するとエラーが発生することを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedDeviceId, $key, $value){

            $this->getDeviceValues($expectedDeviceId, [$key => $value]);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    public function getDataFor_デバイスデータ一覧の参照_異常系_異常値指定() {
        return [
            'startの値が無効' => ['start', 'invalid-string'],
            'endの値が無効'   => ['end',   'invalid-string'],
        ];
    }

    private function forceRegisterDeviceValue($deviceId, $value, $unit, $timeStamp) {
        $db = DB::getMasterDb();

        $sql = "INSERT INTO device_data SET "
             . " device_id={$deviceId},"
             . " value='{$value}',"
             . " unit='{$unit}',"
             . " time_stamp='{$timeStamp}'"
        ;

        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception('SQLエラー: ' . $db->getErr() . ', SQL: ' . $sql);
        }
    }
}
