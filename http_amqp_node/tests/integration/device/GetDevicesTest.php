<?php


use Lcas\Test\Constraint\DeviceConstraint;
use Lcas\Test\Constraint\DeviceListConstraint;
use Lcas\Test\Constraint\NodeConstraint;
use Lcas\Test\Fixture\GatewayFixtureFactory;

require_once __DIR__ . '/AbstractDeviceTestCase.php';

class GetDevicesTest extends AbstractDeviceTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @test
     */
    public function デバイス一覧参照_正常系_一般_自身のデバイス一覧の参照() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //ユーザーの全ノード一覧を取得
        $response = $this->getDevices();
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //レスポンスのデバイス一覧が期待している内容か確認する
        $expectedDevices = $this->deviceFactory->getDeviceListFromMacAddress(TEST_DEFAULT_GATEWAY);
        $json = json_decode($response->getBody(), true);

        foreach($expectedDevices as $idx=>$expectedDevice) {
            $expectedNodeId = $this->getNodeIdFromDeviceId($expectedDevice['device_id']);
            $device = $json[$idx];

            $deviceConstraint = new DeviceConstraint($expectedDevice);
            $deviceConstraint->setNodeId($expectedNodeId);

            $this->assertThat($device, $deviceConstraint, "想定されたデバイス情報を含むこと({$idx})");
        }

    }


    /**
     * @test
     */
    public function デバイス一覧参照_正常系_一般_ラベル指定してデバイス一覧の参照() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
	sleep(5);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $targetNode = $gatewayData['nodes'][1];
        $targetNode2 = $gatewayData['nodes'][2];

        //ノードにラベルを設定する。
        //(GET devicesのラベルによる絞り込みはノードのラベルに対して行われるため)
        $this->nodeUtil->updateNodeLabels($targetNode['node_id'], ['test-label-test']);
	sleep(3);

        $this->nodeUtil->updateNodeLabels($targetNode2['node_id'], ['test']);
	sleep(3);

        //ラベル指定してノード一覧を取得
        $response = $this->getDevices('label');
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //ラベルが部分一致したノードに属するデバイスの一覧が含まれていることを確認する
        $json = json_decode($response->getBody(), true);

        $expectedDevices = $targetNode['devices'];
        foreach($expectedDevices as $idx=>$expectedDevice) {
            $expectedNodeId = $this->getNodeIdFromDeviceId($expectedDevice['device_id']);
            $device = $json[$idx];

            $deviceConstraint = new DeviceConstraint($expectedDevice);
            $deviceConstraint->setNodeId($expectedNodeId);

            $this->assertThat($device, $deviceConstraint, "想定されたデバイス情報を含むこと({$idx})");
        }

    }


    /**
     * @test
     */
    public function デバイス一覧参照_正常系_ノード共有_ノード共有された状態でデバイス一覧の参照() {

        //2人目のユーザーを登録してログイン
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $macAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($macAddress);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //デバイス一覧を参照する
        $response = $this->getDevices();
        $this->assertEquals(200, $response->getStatusCode());

        $devices = json_decode($response->getBody(), true);

        //ノード共有されたノードに属するデバイスが含まれていることを確認する。
        $expectedDevices = $sharedNode['devices'];

        $deviceListConstraint = new \Lcas\Test\Constraint\DeviceListConstraint($expectedDevices);
        $this->assertThat($devices, $deviceListConstraint, 'ノード共有されたデバイスが含まれていること');
    }

    /**
     * @test
     */
    public function デバイス一覧参照_正常系_ノード共有_ノード共有された状態でデバイス一覧の参照_ゲートウェイ割り当てなし() {

        //2人目のユーザーを登録してログイン
        //このユーザーにはゲートウェイを割り当てない
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //デバイス一覧を参照する
        $response = $this->getDevices();
        $this->assertEquals(200, $response->getStatusCode());

        $devices = json_decode($response->getBody(), true);

        //ゲートウェイを持っていないユーザーでも、
        //ノード共有されたノードを参照できることを確認する。
        $expectedDevices = $sharedNode['devices'];

        $deviceListConstraint = new \Lcas\Test\Constraint\DeviceListConstraint($expectedDevices);
        $this->assertThat($devices, $deviceListConstraint, 'ノード共有されたデバイスが含まれていること');
    }


    /**
     * @test
     */
    public function デバイス一覧参照_異常系_ノード共有_ノード共有解除された状態でデバイス一覧を参照() {
        //2人目のユーザーを登録してログイン
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $macAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($macAddress);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //ノード共有を解除する
        $this->nodeUtil->shareNodeWith($sharedNodeId, []);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //デバイス一覧を参照する
        $response = $this->getDevices();
        $this->assertEquals(200, $response->getStatusCode());

        $devices = json_decode($response->getBody(), true);

        //ノード共有されたノードに属するデバイスが含まれていないことを確認する。
        $sharedDevices = $sharedNode['devices'];

        $deviceListConstraint = new DeviceListConstraint($sharedDevices);
        $deviceListConstraint->setCompareType(DeviceListConstraint::COMPARE_TYPE_NOT_INCLUDE);

        $this->assertThat($devices, $deviceListConstraint, 'ノード共有されたデバイスが含まれていないこと');
    }
}
