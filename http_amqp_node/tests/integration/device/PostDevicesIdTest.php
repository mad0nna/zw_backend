<?php


use Lcas\Test\Fixture\NodeFixtureFactory;
use Lcas\Test\LcasSocketIoClient;

require_once __DIR__ . '/AbstractDeviceTestCase.php';

class PostDevicesIdTest extends AbstractDeviceTestCase {

    /**
     * @test
     * @dataProvider getDataFor_デバイスの操作_正常系_ドア操作
     */
    public function デバイスの操作_正常系_ドア操作($value){

        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //Danalockデバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'smart_lock');

        //デバイス操作を実行
        $response = $this->sendDeviceControlRequest($device['device_id'], 'door_lock', $value);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
    }

    public function getDataFor_デバイスの操作_正常系_ドア操作() {
        return [
            'ロック' => ['lock'],
            'オープン' => ['unlock'],
        ];
    }

    /**
     * @test
     * @dataProvider getDataFor_デバイスの操作_正常系_スマートタップ
     */
    public function デバイスの操作_正常系_スマートタップ($value){
        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //スマートタップデバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'power_switch');

        //デバイス操作を実行
        $response = $this->sendDeviceControlRequest($device['device_id'], 'door_lock', $value);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
    }

    public function getDataFor_デバイスの操作_正常系_スマートタップ() {
        return [
            '電源オン' => ['turn_on'],
            '電源オフ' => ['turn_off'],
        ];
    }

    /**
     * @test
     * @dataProvider getDataFor_デバイスの操作_正常系_LED操作
     */
    public function デバイスの操作_正常系_LED操作($value){

        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //Danalockデバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'luminance');

        //デバイス操作を実行
        $response = $this->sendDeviceControlRequest($device['device_id'], 'led', $value);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
    }

    public function getDataFor_デバイスの操作_正常系_LED操作() {
        return [
            'ロン' => ['turn_on'],
            'オフ' => ['turn_off'],
            '点滅' => ['blink'],
        ];
    }

    /**
     * @test
     * @dataProvider getDataFor_デバイスの操作_正常系_モード操作
     */
    public function デバイスの操作_正常系_モード操作($value){

        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //Danalockデバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'distance');

        //デバイス操作を実行
        $response = $this->sendDeviceControlRequest($device['device_id'], 'mode', $value);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
    }

    public function getDataFor_デバイスの操作_正常系_モード操作() {
        // Sorry these labels are bullshit
        return [
            'high-performance' => ['high_performance'],
            'balanced' => ['balanced'],
            'battery-saver' => ['battery_saver'],
        ];
    }

    /**
     * @test
     * @dataProvider getDataFor_デバイスの操作_正常系_アラーム操作
     */
    public function デバイスの操作_正常系_アラーム操作($value){

        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //Danalockデバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'heart_rate');

        //デバイス操作を実行
        $response = $this->sendDeviceControlRequest($device['device_id'], 'alarm', $value);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
    }

    public function getDataFor_デバイスの操作_正常系_アラーム操作() {
        return [
            'ロン' => ['turn_on'],
            'オフ' => ['turn_off'],
        ];
    }

    /**
     * @test
     */
    public function デバイスの操作_異常系_ノード共有(){
        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //デバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'smart_lock');

        //共有相手になるユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //ノード共有設定を行う。
        $this->nodeUtil->shareNodeWith($nodeData['node_id'], [
            $user2Email
        ]);


        //共有相手のユーザーでログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //共有相手からはデバイス操作を実行出来ないこと
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($device) {

            $this->sendDeviceControlRequest($device['device_id'], 'door_lock', 'lock');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function デバイスの操作_異常系_他ユーザーのデバイスを操作(){
        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //デバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'smart_lock');

        //別のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //別のユーザーでログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //他ユーザーからはデバイスの操作を実行できないこと
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($device) {

            $this->sendDeviceControlRequest($device['device_id'], 'door_lock', 'lock');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function デバイスの操作_異常系_存在しないデバイスを操作(){
        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //デバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'smart_lock');

        //別のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //別のユーザーでログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //他ユーザーからはデバイスの操作を実行できないこと
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($device) {

            $this->sendDeviceControlRequest($device['device_id'], 'door_lock', 'lock');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @dataProvider getDataFor_デバイスの操作_異常系_必須項目書略
     */
    public function デバイスの操作_異常系_必須項目書略($key){
        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //デバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'smart_lock');

        //別のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //別のユーザーでログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //他ユーザーからはデバイスの操作を実行できないこと
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($device, $key) {

            $params = [
                'type' => 'door_lock',
                'action' => 'lock',
            ];
            $this->sendDeviceControlRequest($device['device_id'], $params['type'], $params['action']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    public function getDataFor_デバイスの操作_異常系_必須項目書略() {

        return [
            'type' => ['type'],
            'action' => ['action'],
        ];

    }


    /**
     * @test
     * @dataProvider getDataFor_デバイスの操作_異常系_異常値
     */
    public function デバイスの操作_異常系_異常値($params){
        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //デバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'smart_lock');

        //別のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //別のユーザーでログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //他ユーザーからはデバイスの操作を実行できないこと
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($device, $params) {

            $this->sendDeviceControlRequest($device['device_id'], $params['type'], $params['action']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    public function getDataFor_デバイスの操作_異常系_異常値() {

        return [
            'type_無効な値' => [['type' => 'invalid-type', 'action' => 'lock']],
            'type_door_lock_action_無効な値' => [['type' => 'door_lock', 'action' => 'invalid-action']],
            'type_smart_tap_action_無効な値' => [['type' => 'power_switch', 'action' => 'invalid-action']],
        ];

    }


    /**
     * @test
     */
    public function デバイスの操作_異常系_AMQP_AMQPメッセージにてNG応答あり(){

        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //Danalockデバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'smart_lock');

        //AMQPメッセージで強制的にNG応答があるように設定する。
        $this->amqpClient->requestForceNgEnabled(true);

        //デバイス操作を実行
        $response = $this->sendDeviceControlRequest($device['device_id'], 'door_lock', 'lock');
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
    }


    /**
     * @test
     */
    public function デバイスの操作_異常系_AMQP_AMQPメッセージタイムアウト(){

        //デバイスデータ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $nodeData = $gatewayData['nodes'][0];

        //Danalockデバイスを取得
        $device = $this->deviceFactory->findDeviceFromNodeAndDeviceType($nodeData['node_id'], 'smart_lock');

        //AMQPメッセージでタイムアウトが発生するように設定する。
        $this->amqpClient->requestTimeout(true);

        //デバイス操作を実行
        $response = $this->sendDeviceControlRequest($device['device_id'], 'door_lock', 'lock');
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
    }



    private function sendDeviceControlRequest($deviceId, $type, $action) {
        $parameters = [];

        if(!is_null($type)) {
            $parameters['type'] = $type;
        }
        if(!is_null($action)) {
            $parameters['action'] = $action;
        }

        $response = $this->httpClient->post('/api/v3/devices/' . $deviceId, [
            'json' => $parameters
        ]);

        return $response;
    }

}
