<?php


use Lcas\Test\Constraint\DeviceConstraint;
use Lcas\Test\Constraint\NodeConstraint;

require_once __DIR__ . '/AbstractDeviceTestCase.php';

class PutDevicesIdTest extends AbstractDeviceTestCase {

    private $userRepository;

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->userRepository = new \Lcas\Repository\UserRepository();
    }

    /**
     * @test
     */
    public function デバイスのラベル設定_正常系_一般_ラベルの初回登録() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];
        $expectedDevice = $expectedNode['devices'][1];
        $expectedDeviceId = $expectedDevice['device_id'];

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        //ラベルの登録を行う。
        $expectedLabels = [
            'label1:abcd',
            'label2:123',
        ];
        $response = $this->updateDeviceLabels($expectedDeviceId, $expectedLabels);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //ノードの情報を参照する
        $response = $this->getDeviceById($expectedDeviceId);

        $json = json_decode($response->getBody(), true);

        //ラベルが登録されていること
        $deviceConstraint = new DeviceConstraint($expectedDevice);
        $deviceConstraint->setLabels($expectedLabels);
        $deviceConstraint->setNodeId($expectedNodeId);

        $this->assertThat($json, $deviceConstraint, 'labelsの値が更新されていること');
    }


    /**
     * @test
     */
    public function デバイスのラベル設定_正常系_一般_ラベルの更新() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];
        $expectedDevice = $expectedNode['devices'][1];
        $expectedDeviceId = $expectedDevice['device_id'];

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        $originalLabels = [
            'test',
            'test:123456',
        ];
        $this->updateDeviceLabels($expectedDeviceId, $originalLabels);

        //ラベルの登録を行う。
        $expectedLabels = [
            'label1:abcd',
            'label2:123',
        ];
        $response = $this->updateDeviceLabels($expectedDeviceId, $expectedLabels);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //ノードの情報を参照する
        $response = $this->getDeviceById($expectedDeviceId);

        $json = json_decode($response->getBody(), true);

        //ラベルが更新されていること
        //(以前登録されていたラベルが削除されていることの確認を含む)
        $deviceConstraint = new DeviceConstraint($expectedDevice);
        $deviceConstraint->setLabels($expectedLabels);
        $deviceConstraint->setNodeId($expectedNodeId);

        $this->assertThat($json, $deviceConstraint, 'labelsの値が更新されていること');

    }


    /**
     * @test
     */
    public function デバイスのラベル設定_異常系_一般_他ユーザーのデバイスのラベルを更新() {

        //デフォルト以外のユーザーを登録し、ゲートウェイを割り当てる
        $gatewayOwnerEmail = 'owner@example.com';
        $this->userUtil->registerUser($gatewayOwnerEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($gatewayOwnerEmail, TEST_DEFAULT_USER_PASSWORD);
        $exampleGateway = 'A00000001000';
        $this->gatewayUtil->registerGateway($exampleGateway);

        //Node Inclusionを待つため2秒sleepする
        sleep(4);

        //デフォルトのユーザーでログイン
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //他のユーザーが所有するデバイスに対しラベル設定を行おうとすると
        //エラーになることを確認する
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($exampleGateway){

            $exampleGatewayData = $this->gatewayFactory->create($exampleGateway);
            $targetNode = $exampleGatewayData['nodes'][1];
            $targetDevice = $targetNode['devices'][1];

            $this->updateDeviceLabels($targetDevice['device_id'], ['test']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function デバイスのラベル設定_異常系_一般_存在しないデバイスのラベルを更新() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //存在しないデバイスにラベルの設定を行おうとするとエラーになることを確認する
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() {

            $this->updateDeviceLabels(1, ['test2']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function デバイスのラベル設定_異常系_ノード共有_ノード共有されたデバイスのラベルの更新() {

        //デフォルト以外のユーザーを登録し、ゲートウェイを割り当てる
        $gatewayOwnerEmail = 'owner@example.com';
        $this->userUtil->registerUser($gatewayOwnerEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($gatewayOwnerEmail, TEST_DEFAULT_USER_PASSWORD);
        $exampleGateway = 'A00000001000';
        $this->gatewayUtil->registerGateway($exampleGateway);

        //Node Inclusionを待つため2秒sleepする
        sleep(4);

        //デフォルトのユーザーに対しノード共有を許可する
        $gatewayData = $this->gatewayFactory->create($exampleGateway);
        $sharedNode = $gatewayData['nodes'][1];
        $this->nodeUtil->shareNodeWith($sharedNode['node_id'], [
            TEST_DEFAULT_USER_EMAIL
        ]);

        //デフォルトのユーザーでログイン
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //ノード共有されたデバイスに対し共ラベル設定を行おうとするとエラーになることを確認する
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($sharedNode){

            $sharedDevice = $sharedNode['devices'][1];
            $this->updateDeviceLabels($sharedDevice['device_id'], ['test']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function デバイスのラベル設定_異常系_必須項目省略_labelsの省略() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedDevice = $expectedNode['devices'][1];

        //labelsを省略してデバイス設定を変更しようとすると
        //エラーになることを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedDevice){

            $this->updateDeviceLabels($expectedDevice['device_id'], null);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     */
    public function デバイスのラベル設定_異常系_異常値_labelsに無効な値を指定() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedDevice = $expectedNode['devices'][1];

        //labelsを省略してデバイス設定を変更しようとすると
        //エラーになることを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedDevice){

            $this->updateDeviceLabels($expectedDevice['device_id'], '配列ではなく文字列を指定');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

}
