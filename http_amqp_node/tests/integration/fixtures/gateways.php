<?php

$gateways = [];


//標準のゲートウェイ
//(通常のテストで使用するゲートウェイ)
$gateways['A00000000000'] = [
    'gw_name'      => 'A00000000000',
    'manufacturer' => 'fake factory',
    'fw_version'   => '1.0.0',
    'capabilities' => [
        'zwave', 'ip_camera', 'bluetooth',
    ],
    'nodes' => [
        [  //ゲートウェイ自身。デバイスを含まない
            'node_id' => 100,
            'devices' => [],
            'actions' => [
                ['type' => 'range', 'description' => 'aaa'],
                ['type' => 'trigger', 'description' => 'bbb'],
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node100',
            'serial_no' => '100',
            'node_type' => 'zwave',
        ],
        [  //マルチセンサー想定
            'node_id' => 200,
            'devices' => [
                ['device_id' => 200, 'device_type' => 'temperature'],
                ['device_id' => 201, 'device_type' => 'humidity'],
                ['device_id' => 202, 'device_type' => 'motion'],
                ['device_id' => 203, 'device_type' => 'cover'],
                ['device_id' => 204, 'device_type' => 'battery'],
            ],
            'actions' => [
                ['type' => 'range', 'description' => 'aaa'],
                ['type' => 'trigger', 'description' => 'bbb'],
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node200',
            'serial_no' => '200',
            'node_type' => 'zwave',
        ],
        [  //施錠センサー
            'node_id' => 300,
            'devices' => [
                ['device_id' => 300, 'device_type' => 'lock'],
                ['device_id' => 301, 'device_type' => 'cover'],
                ['device_id' => 302, 'device_type' => 'battery'],
            ],
            'actions' => [
                ['type' => 'range', 'description' => 'aaa'],
                ['type' => 'trigger', 'description' => 'bbb'],
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node300',
            'serial_no' => '300',
            'node_type' => 'zwave',
        ],
        [  //ドアセンサー
            'node_id' => 400,
            'devices' => [
                ['device_id' => 400, 'device_type' => 'open_close'],
                ['device_id' => 401, 'device_type' => 'humidity'],
                ['device_id' => 402, 'device_type' => 'motion'],
                ['device_id' => 403, 'device_type' => 'cover'],
                ['device_id' => 404, 'device_type' => 'battery'],
            ],
            'actions' => [
                ['type' => 'range', 'description' => 'aaa'],
                ['type' => 'trigger', 'description' => 'bbb'],
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node400',
            'serial_no' => '400',
            'node_type' => 'zwave',
        ],
    ]
];


//通常のゲートウェイ2つ目
$gateways['A00000001000'] = [
    'gw_name'      => 'A00000001000',
    'manufacturer' => 'fake factory',
    'fw_version'   => '1.0.0',
    'capabilities' => [
        'zwave', 'ip_camera', 'bluetooth',
    ],
    'nodes' => [
        [  //ゲートウェイ自身。デバイスを含まない
            'node_id' => 1100,
            'devices' => [],
            'actions' => [
                ['type' => 'range', 'description' => 'aaa'],
                ['type' => 'trigger', 'description' => 'bbb'],
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node1100',
            'serial_no' => '1100',
            'node_type' => 'zwave',
        ],
        [  //マルチセンサー想定
            'node_id' => 1200,
            'devices' => [
                ['device_id' => 1200, 'device_type' => 'temperature'],
                ['device_id' => 1201, 'device_type' => 'humidity'],
                ['device_id' => 1202, 'device_type' => 'motion'],
                ['device_id' => 1203, 'device_type' => 'cover'],
                ['device_id' => 1204, 'device_type' => 'battery'],
            ],
            'actions' => [
                ['type' => 'range', 'description' => 'aaa'],
                ['type' => 'trigger', 'description' => 'bbb'],
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node1200',
            'serial_no' => '1200',
            'node_type' => 'zwave',
        ],
    ]
];


//通常のゲートウェイ3つ目 - ゲートウェイ自身以外のノードを含まないゲートウェイ
$gateways['A00000002000'] = [
    'gw_name'      => 'A00000002000',
    'manufacturer' => 'fake factory',
    'fw_version'   => '1.0.0',
    'capabilities' => [
        'zwave', 'ip_camera', 'bluetooth',
    ],
    'nodes' => [
        [  //ゲートウェイ自身。デバイスを含まない
            'node_id' => 2100,
            'devices' => [],
            'actions' => [
                ['type' => 'range', 'description' => 'aaa'],
                ['type' => 'trigger', 'description' => 'bbb'],
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node2100',
            'serial_no' => '2100',
            'node_type' => 'zwave',
        ],
    ]
];


//device_typeのテスト用。
//(1つのノードに全てのdevice_typeが含まれています。)
$gateways['A00000003000'] = [
    'gw_name'      => 'A00000003000',
    'manufacturer' => 'fake factory',
    'fw_version'   => '1.0.0',
    'capabilities' => [
        'zwave', 'ip_camera', 'bluetooth',
    ],
    'nodes' => [
        [
            'node_id' => 3100,
            'devices' => [
                ['device_id' => 3100, 'device_type' => 'temperature'],
                ['device_id' => 3101, 'device_type' => 'humidity'],
                ['device_id' => 3102, 'device_type' => 'motion'],
                ['device_id' => 3103, 'device_type' => 'open_close'],
                ['device_id' => 3104, 'device_type' => 'lock'],
                ['device_id' => 3105, 'device_type' => 'cover'],
                ['device_id' => 3106, 'device_type' => 'battery'],
                ['device_id' => 3107, 'device_type' => 'luminance'],
                ['device_id' => 3108, 'device_type' => 'call_button'],
                ['device_id' => 3109, 'device_type' => 'smart_lock'],
                ['device_id' => 3110, 'device_type' => 'power'],
                ['device_id' => 3111, 'device_type' => 'acc_energy'],
                ['device_id' => 3112, 'device_type' => 'power_switch'],
	        	['device_id' => 3113, 'device_type' => 'apparent_energy'],
	        	['device_id' => 3114, 'device_type' => 'acc_gas'],
	        	['device_id' => 3115, 'device_type' => 'acc_water'],
	        	['device_id' => 3116, 'device_type' => 'heart_rate'],
	        	['device_id' => 3117, 'device_type' => 'breathing_rate'],
	        	['device_id' => 3118, 'device_type' => 'body_motion'],
	        	['device_id' => 3119, 'device_type' => 'smoke'],
	        	['device_id' => 3120, 'device_type' => 'person_state'],
	        	['device_id' => 3121, 'device_type' => 'distance'],
            ],
            'actions' => [
                ['type' => 'range', 'description' => 'aaa'],
                ['type' => 'trigger', 'description' => 'bbb'],
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node3100',
            'serial_no' => '3100',
            'state' => 'lost',
            'node_type' => 'zwave',
        ],
    ]
];


//自動登録シナリオの確認用
//(Vision : ZD 2201JP-5)
$gateways['A00000004000'] = [
    'gw_name'      => 'A00000004000',
    'manufacturer' => 'fake factory',
    'fw_version'   => '1.0.0',
    'capabilities' => [
        'zwave', 'ip_camera', 'bluetooth',
    ],
    'nodes' => [
        [
            'node_id' => 4100,
            'devices' => [
                ['device_id' => 4104, 'device_type' => 'lock'],
                ['device_id' => 4105, 'device_type' => 'cover'],
                ['device_id' => 4106, 'device_type' => 'battery'],
            ],
            'actions' => [
                ['type' => 'range', 'description' => 'aaa'],
                ['type' => 'trigger', 'description' => 'bbb'],
            ],
            'manufacturer' => '0109',  //Vision : ZD 2201JP-5
            'product' => '201f1f20',
            'serial_no' => '0109.201f1f20',
            'node_type' => 'zwave',
        ],
    ]
];

//インクルード失敗ノードを含むゲートウェイ
$gateways['A00000005000'] = [
    'gw_name'      => 'A00000005000',
    'manufacturer' => 'fake factory',
    'fw_version'   => '1.0.0',
    'capabilities' => [
        'zwave', 'ip_camera', 'bluetooth',
    ],
    'nodes' => [
        [  //ゲートウェイ自身。デバイスを含まない
            'node_id' => 1100,
            'devices' => [],
            'actions' => [
                ['type' => 'range', 'description' => 'aaa'],
                ['type' => 'trigger', 'description' => 'bbb'],
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node1100',
            'serial_no' => '1100',
        	'failed_cmd' => ["SECURITY", "MANUFACTURER_SPECIFIC"],
            'node_type' => 'zwave',
        ],
        [  //マルチセンサー想定
            'node_id' => 1200,
            'devices' => [
                ['device_id' => 1200, 'device_type' => 'temperature'],
                ['device_id' => 1201, 'device_type' => 'humidity'],
                ['device_id' => 1202, 'device_type' => 'motion'],
                ['device_id' => 1203, 'device_type' => 'cover'],
                ['device_id' => 1204, 'device_type' => 'battery'],
            ],
            'actions' => [
                ['type' => 'range', 'description' => 'aaa'],
                ['type' => 'trigger', 'description' => 'bbb'],
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node1200',
            'serial_no' => '1200',
            'node_type' => 'zwave',
        ],
    ]
];

//Parkersゲートウェイ
$gateways['A00000006000'] = [
    'gw_name'      => 'A00000006000',
    'manufacturer' => 'fake parkers factory',
    'fw_version'   => 'PK.1.0.0st',
    'capabilities' => [
        'dust_ip',
    ],
    'nodes' => [
        [
            'node_id' => 6100,
            'devices' => [
                ['device_id' => 6100, 'device_type' => 'parkers_car'],
                ['device_id' => 6101, 'device_type' => 'parkers_led'],
                ['device_id' => 6102, 'device_type' => 'parkers_mode'],
                ['device_id' => 6103, 'device_type' => 'parkers_battery'],
            ],
            'actions' => [
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node6100',
            'serial_no' => '6100',
            'node_type' => 'dust_ip',
            'app' => [
                [
                    'app_name' => 'yet another app',
                    'configurations' => [
                        [
                            'param_name' => 'integer',
                            'param_value' => 65535
                        ],
                        [
                            'param_name' => 'string',
                            'param_value' => '21474'
                        ],
                        [
                            'param_name' => 'json_object',
                            'param_value' => [
                                'key_with_string_value' => '4096',
                                'key_with_integer_value' => 1024
                            ]
                        ]
                    ]
                ]
            ]
        ],
        [
            'node_id' => 6200,
            'devices' => [
                ['device_id' => 6200, 'device_type' => 'parkers_car'],
                ['device_id' => 6201, 'device_type' => 'parkers_led'],
                ['device_id' => 6202, 'device_type' => 'parkers_mode'],
                ['device_id' => 6203, 'device_type' => 'parkers_battery'],
            ],
            'actions' => [
            ],
            'manufacturer' => 'fake-factory',
            'product' => 'node6200',
            'serial_no' => '6200',
            'node_type' => 'dust_ip',
        ],
    ]
];

return $gateways;
