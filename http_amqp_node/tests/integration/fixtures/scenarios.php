<?php

$scenarios = [];


//デフォルトのシナリオ
$scenarios[TEST_DEFAULT_SCENARIO] = [
    'scenario_type' => 'simple',
    'labels' => ['name:test-scenario1-test', 'color:red'],
    'condition_operator' => 'AND',
    'conditions' => [
        //ゲートウェイのフィクスチャ"TEST_DEFAULT_GATEWAY"に含まれるデバイスのIDを指定
        ['lhs' => '201', 'operator' => '==', 'rhs' => '20', 'unit' => '%']
    ],
    'trigger_after' => 10,
    'actions' => [
        ['type' => 'send_message', 'content' => 'default_scenario']
    ],
    'enabled' => true,
];

//通常のシナリオ2つ目。
//2つのシナリオを登録しなければならない場合用
$scenarios['second-scenario'] = [
    'scenario_type' => 'simple',
    'labels' => ['name:test-scenario2-test', 'color:white'],
    'condition_operator' => 'OR',
    'conditions' => [
        //ゲートウェイのフィクスチャ"TEST_DEFAULT_GATEWAY"に含まれるデバイスのIDを指定
        ['lhs' => '201', 'operator' => '>=', 'rhs' => '40', 'unit' => '%']
    ],
    'trigger_after' => 30,
    'actions' => [
        ['type' => 'send_message', 'content' => 'default_scenario2']
    ],
    'enabled' => true,
];


//scenario_type=wbgtのタイプのデフォルトのシナリオ
//2つのシナリオを登録しなければならない場合用
$scenarios[TEST_DEFAULT_SCENARIO_WBGT] = [
    'scenario_type' => 'wbgt',
    'labels' => ['name:test-wbgt_scenario-test', 'color:white'],
    'temperature_dev' => 0,  //実際のデバイスIDを指定する必要があります。
    'humidity_dev' => 0,     //実際のデバイスIDを指定する必要があります。
    'wbgt_threshold' => 10,
    'actions' => [
        ['type' => 'send_message', 'content' => 'wbgt_default_scenario']
    ],
    'enabled' => true,
];


//scenario_type=scxmlのタイプのデフォルトのシナリオ
$scenarios[TEST_DEFAULT_SCENARIO_SCXML] = [
    'scenario_type' => 'scxml',
    'labels' => ['name:test-scxml_scenario-test', 'color:white'],
	'scxml' => '<?xml version="1.0" encoding="UTF-8"?><scxml xmlns="http://www.w3.org/2005/07/scxml" version="1.0" initial="absent" datamodel="ecmascript"><datamodel><!-- 使用するセンサーリスト。全てID指定 --><!-- 入口のドアセンサー --><data id="entrance_door_device_id" expr="10" /><!-- トイレのドアセンサー --><data id="restroom_door_device_id" expr="11" /><!-- Vital Sensor心拍 --><data id="heartrate_device_id" expr="12" /><!-- Vital Sensor呼吸 --><data id="respiratoryrate_device_id" expr="13" /><!-- Vital Sensor BR比 --><data id="br_ratio_device_id" expr="14" /><!-- 人感センサー（任意の数） --><data id="motion_device_ids" expr="[15,16,17]" /><!-- 時間の単位はミリ秒。但し、時間精度はもっと粗い --><!-- 入口が閉まってから以下の時間内に人感センサが反応したら入室 --><data id="entrance_timer" expr="180000" /><!-- 人感センサが反応してから以下の時間内にドアが閉まったら退室 --><data id="maybe_leaving_timer" expr="180000" /><!-- 入室中、以下の時間人感反応がなかったらアラート（ベッド上除く） --><data id="room_alert_timer" expr="7200000" /><!-- ベッド上で以下の時間呼吸が観測されなかったらアラート --><data id="no_resp_timer" expr="600000" /><!-- タイマーID用の変数 --><data id="entrance_timer_id" /><data id="maybe_leaving_timer_id" /><data id="room_alert_timer_id" /><data id="no_resp_timer_id" /></datamodel><!-- 部屋に誰もいない状態 --><state id="absent"><initial><!-- 最初はドアも人感も反応していないことを想定 --><transition target="silent" /></initial><state id="silent"><!-- ドアが閉まったら遷移 --><transition event="devicedata.open_close" cond="_event.data.value == 0 &amp;&amp; _event.data.device_id == entrance_door_device_id" target="door_closed" /></state><state id="door_closed"><onentry><!-- タイマー起動 --><send idlocation="entrance_timer_id" event="entrance_timer.expired" delayexpr="entrance_timer" /></onentry><onexit><!-- タイマーキャンセル --><cancel sendid="entrance_timer_id" /></onexit><!-- 人感センサ反応 --><transition event="devicedata.motion" cond="_event.data.value == 255 &amp;&amp; motion_device_ids.indexOf(_event.data.device_id) != -1" target="present" /><!-- 反応がなく、タイマー満了 --><transition event="entrance_timer.expired" target="silent" /></state></state><!-- 部屋に誰かいる状態 --><state id="present"><initial><!-- 入ったばかりはすぐに出るかもしれない --><transition target="maybe_leaving" /></initial><!-- 入ったばかりか出る直前の状態 --><state id="maybe_leaving"><!-- タイマー起動 --><onentry><send idlocation="maybe_leaving_timer_id" event="maybe_leaving_timer.expired" delayexpr="maybe_leaving_timer" /></onentry><onexit><!-- タイマー停止 --><cancel sendid="maybe_leaving_timer_id" /></onexit><!-- 入り口ドアが閉まったら退室と判断 --><transition event="devicedata.open_close" cond="_event.data.value == 0 &amp;&amp; _event.data.device_id == entrance_door_device_id" target="door_closed" /><!-- 心拍センサーの呼吸が反応したらベッドに移動と判断 --><transition event="devicedata.breathing_rate" cond="_event.data.value &gt; 0 &amp;&amp; _event.data.device_id == respiratoryrate_device_id" target="on_bed" /><!-- 人感反応がしばらくなかったらベッド以外のどこかにいると判断 --><transition event="maybe_leaving_timer.expired" target="not_on_bed" /></state><!-- ベッド以外の場所にいる状態 --><state id="not_on_bed"><onentry><!-- タイマー起動 --><send idlocation="room_alert_timer_id" event="room_alert_timer.expired" delayexpr="room_alert_timer" /></onentry><onexit><cancel sendid="room_alert_timer_id" /></onexit><!-- 人感反応があればもうすぐ退室するかもと判断 --><transition event="devicedata.motion" cond="_event.data.value == 255 &amp;&amp; motion_device_ids.indexOf(_event.data.device_id) != -1" target="maybe_leaving" /><!-- 人感反応が一定時間なければアラート --><transition event="room_alert_timer.expired" target="alert" /><!-- 心拍センサーの呼吸が反応したらベッドに移動と判断 --><transition event="devicedata.breathing_rate" cond="_event.data.value &gt; 0 &amp;&amp; _event.data.device_id == respiratoryrate_device_id" target="on_bed" /></state><state id="on_bed"><initial><transition target="on_bed_normal" /></initial><state id="on_bed_normal"><!-- 呼吸がなくなったら遷移 --><transition event="devicedata.breathing_rate" cond="_event.data.value == 0 &amp;&amp; _event.data.device_id == respiratoryrate_device_id" target="on_bed_no_resp" /><!-- 人感反応があればもうすぐ退室するかもと判断 --><transition event="devicedata.motion" cond="_event.data.value == 255 &amp;&amp; motion_device_ids.indexOf(_event.data.device_id) != -1" target="maybe_leaving" /></state><state id="on_bed_no_resp"><!-- タイマー起動 --><onentry><send idlocation="no_resp_timer_id" event="no_resp_timer.expired" delayexpr="no_resp_timer" /></onentry><onexit><cancel sendid="no_resp_timer_id" /></onexit><!-- タイマーが満了したらアラート --><transition event="no_resp_timer.expired" target="alert" /><!-- 呼吸が観測されたら元に戻る --><transition event="devicedata.breathing_rate" cond="_event.data.value &gt; 0 &amp;&amp; _event.data.device_id == respiratoryrate_device_id" target="on_bed_normal" /><!-- 人感反応があればもうすぐ退室するかもと判断 --><transition event="devicedata.motion" cond="_event.data.value == 255 &amp;&amp; motion_device_ids.indexOf(_event.data.device_id) != -1" target="maybe_leaving" /></state></state><state id="alert"><!-- この状態に遷移したら通知を出す --><!-- 人感反応があれば抜ける --><transition event="devicedata.motion" cond="_event.data.value == 255 &amp;&amp; motion_device_ids.indexOf(_event.data.device_id) != -1" target="maybe_leaving" /><!-- 呼吸が反応しても抜ける --><transition event="devicedata.breathing_rate" cond="_event.data.value &gt; 0 &amp;&amp; _event.data.device_id == respiratoryrate_device_id" target="on_bed" /></state></state></scxml>',
	'device_list' => [0],  //実際のデバイスIDを指定する必要があります。
	'action_state_list' => ['absent', 'silent', 'door_closed', 'present', 'not_on_bed', 'on_bed', 'on_bed_normal', 'on_bed_no_resp'],
	'actions' => [
        ['type' => 'send_message', 'content' => 'scxml_default_scenario']
    ],
    'enabled' => true,
];


//バッテリ残量低下の自動登録シナリオ
$scenarios[TEST_AUTO_SCENARIO_BATTERY] = [
    'scenario_type' => 'simple',
    'labels' => ['電池残量低下の通知（自動登録シナリオ）'],
    'condition_operator' => 'AND',
    'conditions' => [
        //lhsは実際のデバイスIDを指定する必要があります。
        ['lhs' => '0', 'operator' => '==', 'rhs' => '255', 'unit' => '%']
    ],
    'trigger_after' => 0,
    'actions' => [
        ['type' => 'send_message', 'content' => '電池交換が必要なセンサーデバイスがあります']
    ],
    'enabled' => true,
];


//心拍異常シナリオ１
$scenarios['abnormal-heart-rate-scenario-1'] = [
		'scenario_type' => 'simple',
		'labels' => ['name:心拍異常シナリオ１'],
		'condition_operator' => 'AND',
		'conditions' => [
				//lhsは実際のデバイスIDを指定する必要があります。
				['lhs' => '0', 'operator' => '>', 'rhs' => '120']
		],
		'trigger_after' => 30,
		'actions' => [
				['type' => 'send_message', 'content' => '心拍数の異常を検知しました']
		],
		'enabled' => true,
];

//心拍異常シナリオ２
$scenarios['abnormal-heart-rate-scenario-2'] = [
		'scenario_type' => 'simple',
		'labels' => ['name:心拍異常シナリオ２'],
		'condition_operator' => 'AND',
		'conditions' => [
				//lhsは実際のデバイスIDを指定する必要があります。
				['lhs' => '0', 'operator' => '>', 'rhs' => '1'],
				['lhs' => '0', 'operator' => '<', 'rhs' => '120'],
		],
		'trigger_after' => 30,
		'actions' => [
				['type' => 'send_message', 'content' => '心拍数の異常を検知しました']
		],
		'enabled' => true,
];

//呼吸異常シナリオ１
$scenarios['abnormal-breath-rate-scenario-1'] = [
		'scenario_type' => 'simple',
		'labels' => ['name:呼吸異常シナリオ１'],
		'condition_operator' => 'AND',
		'conditions' => [
				//lhsは実際のデバイスIDを指定する必要があります。
				['lhs' => '0', 'operator' => '>', 'rhs' => '20']
		],
		'trigger_after' => 30,
		'actions' => [
				['type' => 'send_message', 'content' => '心拍数の異常を検知しました']
		],
		'enabled' => true,
];

//呼吸異常シナリオ２
$scenarios['abnormal-breath-rate-scenario-2'] = [
		'scenario_type' => 'simple',
		'labels' => ['name:呼吸異常シナリオ２'],
		'condition_operator' => 'AND',
		'conditions' => [
				//lhsは実際のデバイスIDを指定する必要があります。
				['lhs' => '0', 'operator' => '>', 'rhs' => '1'],
				['lhs' => '0', 'operator' => '<', 'rhs' => '20'],
		],
		'trigger_after' => 30,
		'actions' => [
				['type' => 'send_message', 'content' => '心拍数の異常を検知しました']
		],
		'enabled' => true,
];

//離床シナリオ
$scenarios['ambulation-scenario'] = [
		'scenario_type' => 'simple',
		'labels' => ['name:離床シナリオ'],
		'condition_operator' => 'AND',
		'conditions' => [
				//lhsは実際の心拍センサーのデバイスIDを指定する必要があります。
				['lhs' => '0', 'operator' => '==', 'rhs' => '0'],
				//lhsは実際の呼吸センサーのデバイスIDを指定する必要があります。
				['lhs' => '0', 'operator' => '==', 'rhs' => '0'],
		],
		'trigger_after' => 0,
		'actions' => [
				['type' => 'send_message', 'content' => '離床行動を検知しました']
		],
		'enabled' => true,
];


//火災警報シナリオ
$scenarios['fire-scenario'] = [
		'scenario_type' => 'simple',
		'labels' => ['name:火災警報シナリオ'],
		'condition_operator' => 'AND',
		'conditions' => [
				//lhsは実際のデバイスIDを指定する必要があります。
				['lhs' => '0', 'operator' => '==', 'rhs' => '255']
		],
		'trigger_after' => 0,
		'actions' => [
				['type' => 'send_message', 'content' => '火災警報を検知しました']
		],
		'enabled' => true,
];

//状態検知シナリオ_無人
$scenarios['person-state-scenario-0'] = [
		'scenario_type' => 'simple',
		'labels' => ['name:状態検知シナリオ'],
		'condition_operator' => 'AND',
		'conditions' => [
				//lhsは実際のデバイスIDを指定する必要があります。
				['lhs' => '0', 'operator' => '==', 'rhs' => '0']
		],
		'trigger_after' => 30,
		'actions' => [
				['type' => 'send_message', 'content' => '状態を検知しました']
		],
		'enabled' => true,
];

//状態検知シナリオ_在室
$scenarios['person-state-scenario-1'] = [
		'scenario_type' => 'simple',
		'labels' => ['name:状態検知シナリオ'],
		'condition_operator' => 'AND',
		'conditions' => [
				//lhsは実際のデバイスIDを指定する必要があります。
				['lhs' => '0', 'operator' => '==', 'rhs' => '1']
		],
		'trigger_after' => 60,
		'actions' => [
				['type' => 'send_message', 'content' => '状態を検知しました']
		],
		'enabled' => true,
];

//状態検知シナリオ_行動
$scenarios['person-state-scenario-2'] = [
		'scenario_type' => 'simple',
		'labels' => ['name:状態検知シナリオ'],
		'condition_operator' => 'AND',
		'conditions' => [
				//lhsは実際のデバイスIDを指定する必要があります。
				['lhs' => '0', 'operator' => '==', 'rhs' => '2']
		],
		'trigger_after' => 90,
		'actions' => [
				['type' => 'send_message', 'content' => '状態を検知しました']
		],
		'enabled' => true,
];

//距離測定シナリオ
$scenarios['distance-scenario'] = [
		'scenario_type' => 'simple',
		'labels' => ['name:距離測定シナリオ'],
		'condition_operator' => 'AND',
		'conditions' => [
				//lhsは実際のデバイスIDを指定する必要があります。
				['lhs' => '0', 'operator' => '<=', 'rhs' => '100', 'unit' => 'cm']
		],
		'trigger_after' => 0,
		'actions' => [
				['type' => 'send_message', 'content' => '接近を検知しました']
		],
		'enabled' => true,
];




return $scenarios;
