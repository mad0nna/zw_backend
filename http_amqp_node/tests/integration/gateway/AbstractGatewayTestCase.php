<?php


use Lcas\Test\Fixture\GatewayFixtureFactory;
use Lcas\Test\Util\TestUtil;

abstract class AbstractGatewayTestCase extends \Lcas\Test\IntegrationTestCase {

    /**
     * @var GatewayFixtureFactory
     */
    protected $gatewayFactory;


    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->gatewayFactory = new GatewayFixtureFactory();
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {

        parent::setUp();

        //テストデータを全削除
        TestUtil::clearDatabase();

        //デフォルトのユーザーを登録する
        $this->userUtil->registerDefaultUser();
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
    }

    /**
     * ゲートウェイの一覧を取得する(GET gateways)
     * @param string $label ラベル文字列
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function getGateways($label=null) {
        $options = [];
        if(!is_null($label)) {
            $options['query'] = ['label' => $label];
        }
        return $this->httpClient->get('/api/v3/gateways', $options);
    }

    /**
     * ゲートウェイの情報を取得する(GET gateways/:id)
     * @param int $gatewayId ゲートウェイID
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function getGateway($gatewayId) {
        return $this->httpClient->get('/api/v3/gateways/' . $gatewayId);
    }

    /**
     * ゲートウェイのラベルを更新する(PUT gateways/:id)
     * @param int $gatewayId
     * @param array $labels
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function updateGatewayLabels($gatewayId, $labels) {
        $json = json_encode(['labels' => $labels]);
        return $this->httpClient->put('/api/v3/gateways/' . $gatewayId, ['body' => $json]);
    }

    /**
     * ゲートウェイのdustipを更新する(PUT gateways/:id)
     * @param int $gatewayId
     * @param array $dustIp
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function updateGatewayDustIp($gatewayId, $dustIp) {
        $json = json_encode(['dust_ip' => $dustIp]);
        return $this->httpClient->put('/api/v3/gateways/' . $gatewayId, ['body' => $json]);
    }

    /**
     * ゲートウェイの状態を更新する(POST gateways/:id)
     * @param int $gatewayId
     * @param string $mode
     * @param string $flag
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function updateGatewayState($gatewayId, $mode, $flag) {
        $options = [];
        if(!is_null($mode)) {
            $options['mode'] = $mode;
        }
        if(!is_null($flag)) {
            $options['flag'] = $flag;
        }

        $json = json_encode(['zwave' => $options]);
        return $this->httpClient->post('/api/v3/gateways/' . $gatewayId, ['body' => $json]);
    }

}
