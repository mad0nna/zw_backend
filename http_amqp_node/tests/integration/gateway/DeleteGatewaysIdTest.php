<?php


use Lcas\Test\Constraint\GatewayConstraint;
use Lcas\Test\Fixture\GatewayFixtureFactory;

require_once __DIR__ . '/AbstractGatewayTestCase.php';

class DeleteGatewaysIdTest extends AbstractGatewayTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setup();
    }

    /**
     * @test
     */
    public function ゲートウェイ割り当て解除_正常系_解除成功() {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        $response = $this->gatewayUtil->deregisterGateway($gatewayId);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        $expectedStatusCode = 404;
        $this->assertHttpError(function() use ($gatewayId){

            $this->getGateway($gatewayId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ゲートウェイ割り当て解除_異常系_他ユーザーが所有() {
        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //別のユーザーでログイン
        $anotherUserEmail = 'test2@example.com';
        $this->userUtil->registerUser($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD);

        $expectedStatusCode = 404;
        $this->assertHttpError(function() use ($gatewayId){

            $this->gatewayUtil->deregisterGateway($gatewayId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     */
    public function ゲートウェイ割り当て解除_正常系_存在しないゲートウェイを指定() {
        $expectedStatusCode = 404;
        $this->assertHttpError(function(){

            $this->gatewayUtil->deregisterGateway(1);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ゲートウェイ割り当て解除_異常系_AMQP_NG応答() {
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //AMQPのNG応答が返るように設定する。
        $this->amqpClient->requestForceNgEnabled(true);

        $expectedStatusCode = 400;
        $this->assertHttpError(function() use($gatewayId){

            //エラーが返されることを確認する。
            $this->gatewayUtil->deregisterGateway($gatewayId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @group slow
     */
    public function ゲートウェイ割り当て解除_異常系_AMQP_タイムアウト() {
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //AMQPのレスポンスがタイムアウトするように設定する
        $this->amqpClient->requestSleep(11);

        $expectedStatusCode = 504;
        $this->assertHttpError(function() use($gatewayId){

            //エラーが返されることを確認する。
            $this->gatewayUtil->deregisterGateway($gatewayId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @group slow
     */
    public function ゲートウェイ割り当て解除_異常系_AMQP_タイムアウト_OK応答あり() {
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //AMQPのレスポンスがタイムアウトするように設定する
        $this->amqpClient->requestSleep(11);

        $expectedStatusCode = 504;
        $this->assertHttpError(function() use($gatewayId){

            //エラーが返されることを確認する。
            $this->gatewayUtil->deregisterGateway($gatewayId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

        sleep(1);

        $expectedStatusCode = 404;
        $this->assertHttpError(function() use($gatewayId){

            //ゲートウェイが解除され参照できなくなっていること
            $this->getgateway($gatewayId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @group slow
     */
    public function ゲートウェイ割り当て解除_異常系_AMQP_タイムアウト_NG応答あり() {
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //AMQPのレスポンスがタイムアウトし、その後NG応答するように設定する
        $this->amqpClient->requestSleep(11);
        $this->amqpClient->requestForceNgEnabled(true);

        $expectedStatusCode = 504;
        $this->assertHttpError(function() use($gatewayId){

            //エラーが返されることを確認する。
            $this->gatewayUtil->deregisterGateway($gatewayId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

        sleep(1);

        //ゲートウェイ情報を参照し、解除されていないことを確認する
        $response = $this->getgateway($gatewayId);
        $json = json_decode($response->getBody(), true);
        $gatewayFactory = new GatewayFixtureFactory();
        $expectedGatewayData = $gatewayFactory->create($defaultGateway);

        $gatewayConstraint = new GatewayConstraint($expectedGatewayData);
        $gatewayConstraint->setGatewayStatus('connected');

        $this->assertThat($json, $gatewayConstraint);
    }



}
