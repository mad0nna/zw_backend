<?php


use Lcas\Test\Constraint\WebSocket\GatewayStateChangeConstraint;
use Lcas\Test\Fixture\GatewayFixtureFactory;
use Lcas\Log\Logger;

require_once __DIR__ . '/AbstractGatewayTestCase.php';

class GetGatewaysIdPublickeyTest extends AbstractGatewayTestCase {

    /**
     * @test
     */
    public function Gateway_id_should_not_refer_to_a_non_existing_gateway() {
        $expectedStatusCode = 404;
        $this->assertHttpError(function(){

            $this->httpClient->get('/api/v3/gateways/1/publickey');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     */
    public function Gateway_id_should_not_refer_to_a_gateway_owned_by_other_user() {
        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //別のユーザーでログイン
        $anotherUserEmail = 'test2@example.com';
        $this->userUtil->registerUser($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD);

        $expectedStatusCode = 404;
        $this->assertHttpError(function() use ($gatewayId){

            $this->httpClient->get('/api/v3/gateways/'.$gatewayId.'/publickey');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     */
    public function A_404_response_should_be_returned_if_the_gateway_does_not_have_a_public_key() {
        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        $expectedStatusCode = 404;
        $this->assertHttpError(function() use ($gatewayId){

            $this->httpClient->get('/api/v3/gateways/'.$gatewayId.'/publickey');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     */
    public function Public_key_should_be_the_latest_updated_one() {

        //ユーザーにゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //PublicKeyUpdateNotificationのメッセージを送信する。
        $random_public_key = $this->sendPublicKeyUpdateNotification(TEST_DEFAULT_GATEWAY);
        sleep(4);

        $gatewayRepository = new \Lcas\Repository\GatewayRepository();
        $registeredGateway = $gatewayRepository->findByMacAddress(TEST_DEFAULT_GATEWAY);

        $this->assertSame($random_public_key, $registeredGateway["public_key"], json_encode($registeredGateway));
    }


    private function sendPublicKeyUpdateNotification($macAddress) {
        $public_key = strval(rand());
        $request = [
            'command' => 'pubkey_update',
            'result_code' => '200',
            'gw_name' => $macAddress,
            'public_key' => $public_key,
        ];
        $this->amqpClient->sendMessageToLcas($request);
        return $public_key;
    }

}
