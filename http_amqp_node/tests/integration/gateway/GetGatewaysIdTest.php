<?php


use Lcas\Test\Constraint\GatewayConstraint;

require_once __DIR__ . '/AbstractGatewayTestCase.php';


class GetGatewaysIdTest extends AbstractGatewayTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setUp();
    }

    /**
     * @test
     */
    public function ゲートウェイ情報取得_正常系_ID指定で取得() {
        //ゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //ラベルを指定してゲートウェイ一覧を取得
        $response = $this->getGateway($gatewayId);
        $this->assertEquals(200, $response->getStatusCode());

        //レスポンスの内容が正しいこと
        $responseJson = json_decode($response->getBody(), true);

        $expectedGatewayData = $this->gatewayFactory->create($defaultGateway);
        $gatewayConstraint = new GatewayConstraint($expectedGatewayData);
        $this->assertThat($responseJson, $gatewayConstraint, '想定するゲートウェイ情報が含まれていること');
    }


    /**
     * @test
     */
    public function ゲートウェイ情報取得_異常系_IDが数値でない() {

        $expectedStatusCode = 404;
        $this->assertHttpError(function(){

            $this->httpClient->get('/api/v3/gateways/invalid-id');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ゲートウェイ情報取得_異常系_他ユーザーが所有() {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //別のユーザーでログイン
        $anotherUserEmail = 'test2@example.com';
        $this->userUtil->registerUser($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD);

        $expectedStatusCode = 404;
        $this->assertHttpError(function() use ($gatewayId){

            $this->getGateway($gatewayId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ゲートウェイ情報取得_異常系_存在しないゲートウェイを指定() {

        $expectedStatusCode = 404;
        $this->assertHttpError(function(){

            $this->httpClient->get('/api/v3/gateways/1');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


}
