<?php


use Lcas\Test\Constraint\GatewayConstraint;

require_once __DIR__ . '/AbstractGatewayTestCase.php';


class GetGatewaysTest extends AbstractGatewayTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setUp();
    }

    /**
     * @test
     */
    public function ゲートウェイ一覧取得_正常系_全件取得() {

        //ゲートウェイを2つ割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $secondGateway = 'A00000001000';
        $this->gatewayUtil->registerGateway($defaultGateway);
        $this->gatewayUtil->registerGateway($secondGateway);

        // wait for 5 seconds to make sure that gw_state_change notifications have surely arrived.
        sleep(5);

        //ゲートウェイ一覧を取得
        $response = $this->getGateways();
        $this->assertEquals(200, $response->getStatusCode());

        //レスポンスの内容が正しいこと
        $responseJson = json_decode($response->getBody(), true);

        $expectedGatewayData = $this->gatewayFactory->create($defaultGateway);
        $gatewayConstraint = new GatewayConstraint($expectedGatewayData);
        $gatewayConstraint->setGatewayStatus('connected');
        $this->assertThat($responseJson[0], $gatewayConstraint, '1つ目のゲートウェイの内容が想定通りであること');

        $expectedGatewayData = $this->gatewayFactory->create($secondGateway);
        $gatewayConstraint = new GatewayConstraint($expectedGatewayData);
        $gatewayConstraint->setGatewayStatus('connected');
        $this->assertThat($responseJson[1], $gatewayConstraint, '2つ目のゲートウェイの内容が想定通りであること');
    }

    /**
     * @test
     */
    public function ゲートウェイ一覧取得_正常系_ラベル指定あり() {

        //ゲートウェイを2つ割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $secondGateway = 'A00000001000';
        $this->gatewayUtil->registerGateway($defaultGateway);
        $this->gatewayUtil->registerGateway($secondGateway);

        // wait for 5 seconds to make sure that gw_state_change notifications have surely arrived.
        sleep(5);

        //2つ目のゲートウェイにラベルを設定する
        $gatewayLabels = ['test gateway2 test'];
        $secondGatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($secondGateway);
        $this->updateGatewayLabels($secondGatewayId, $gatewayLabels);

        //ラベルを指定してゲートウェイ一覧を取得
        $response = $this->getGateways('gateway2');
        $this->assertEquals(200, $response->getStatusCode());
        $responseJson = json_decode($response->getBody(), true);

        //レスポンスの内容が正しいこと
        $expectedGatewayData = $this->gatewayFactory->create($secondGateway);
        $gatewayConstraint = new GatewayConstraint($expectedGatewayData);
        $gatewayConstraint->setGatewayLabels($gatewayLabels);
        $gatewayConstraint->setGatewayStatus('connected');

        $this->assertThat($responseJson[0], $gatewayConstraint, 'ラベルが部分一致するゲートウェイのみ含まれていること');
    }

}
