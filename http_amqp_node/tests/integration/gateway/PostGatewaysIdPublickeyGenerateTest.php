<?php


use Lcas\Test\Fixture\GatewayFixtureFactory;
use Lcas\Log\Logger;

require_once __DIR__ . '/AbstractGatewayTestCase.php';

class PostGatewaysIdPublickeyGenerateTest extends AbstractGatewayTestCase {

    /**
     * @test
     */
    public function Gateway_id_should_not_refer_to_a_non_existing_gateway() {
        $expectedStatusCode = 404;
        $this->assertHttpError(function(){

            $this->httpClient->post('/api/v3/gateways/1/publickey/generate');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     */
    public function Gateway_id_should_not_refer_to_a_gateway_owned_by_other_user() {
        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //別のユーザーでログイン
        $anotherUserEmail = 'test2@example.com';
        $this->userUtil->registerUser($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD);

        $expectedStatusCode = 404;
        $this->assertHttpError(function() use ($gatewayId){

            $this->httpClient->post('/api/v3/gateways/'.$gatewayId.'/publickey/generate');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     */
    public function Public_key_should_have_changed_after_a_valid_request() {
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        $gatewayRepository = new \Lcas\Repository\GatewayRepository();

        $original_key = $gatewayRepository->findByMacAddress($defaultGateway)["public_key"];
        $response = $this->httpClient->post('/api/v3/gateways/'.$gatewayId.'/publickey/generate');
        $modified_key = $gatewayRepository->findByMacAddress($defaultGateway)["public_key"];

        $this->assertNotSame($original_key, $modified_key, "Public key should have changed");
    }

    /**
     * @test
     */
    public function Public_key_should_remain_null_if_the_gateway_does_not_support_the_generation() {
        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //AMQP APIでNG応答が返るよう設定する。
        $this->amqpClient->requestForceNgEnabled(true);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 403;
        $this->assertGeneralErrorResponse(function() use($gatewayId){

        	$this->httpClient->post('/api/v3/gateways/'.$gatewayId.'/publickey/generate');
        	
        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

}
