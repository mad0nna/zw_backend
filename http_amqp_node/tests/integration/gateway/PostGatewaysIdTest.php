<?php


require_once __DIR__ . '/AbstractGatewayTestCase.php';

class PostGatewaysIdTest extends AbstractGatewayTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setup();
    }

    /**
     * @dataProvider getDataFor_ゲートウェイ状態変更_異常系_他ユーザーが所有
     * @test
     */
    public function ゲートウェイ状態変更_正常系($mode, $flag) {
        //

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        $response = $this->updateGatewayState($gatewayId, $mode, $flag);

        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
    }

    public static function getDataFor_ゲートウェイ状態変更_異常系_他ユーザーが所有() {
        //いずれのケースも実行すると204レスポンスが返ることを想定
        return [
            'include_start' => ['include', 'start'],
            'include_stop'  => ['include', 'stop'],
            'exclude_start' => ['exclude', 'start'],
            'exclude_stop'  => ['exclude', 'stop'],
        ];
    }

    /**
     * @test
     */
    public function ゲートウェイ状態変更_異常系_他ユーザーが所有() {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //別のユーザーでログイン
        $anotherUserEmail = 'test2@example.com';
        $this->userUtil->registerUser($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD);

        $expectedStatusCode = 404;
        $this->assertHttpError(function() use ($gatewayId){

            $this->updateGatewayState($gatewayId, 'include', 'start');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ゲートウェイ状態変更_異常系_存在しないゲートウェイを指定() {

        $expectedStatusCode = 404;
        $this->assertHttpError(function(){

            $this->httpClient->post('/api/v3/gateways/1', [
                'body' => json_encode([
                    'zwave' => [
                        'mode' => 'include',
                        'flag' => 'start'
                    ]
                ])
            ]);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @dataProvider getDataFor_ゲートウェイ状態変更_異常系_パラメータ
     */
    public function ゲートウェイ状態変更_異常系_パラメータ($mode, $flag) {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        $expectedStatusCode = 400;
        $this->assertHttpError(function() use($gatewayId, $mode, $flag){

            //無効な入力データを受け付けた場合、400が返ることを確認
            $this->updateGatewayState($gatewayId, $mode, $flag);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    public function getDataFor_ゲートウェイ状態変更_異常系_パラメータ() {
        //全てレスポンスが400となる想定
        return [
            'modeが存在しない' => [null, 'start'],
            'flagが存在しない' => ['exclude', null],
            'modeが無効な値'   => ['invalid', 'start'],
            'flagが無効な値'   => ['include', 'invalid'],
        ];
    }


    /**
     * @test
     */
    public function ゲートウェイ状態変更_異常系_AMQP_NG応答() {
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //AMQPのNG応答が返るように設定する。
        $this->amqpClient->requestForceNgEnabled(true);

        $expectedStatusCode = 400;
        $this->assertHttpError(function() use($gatewayId){

            //エラーが返されることを確認する。
            $this->updateGatewayState($gatewayId, 'exclude', 'start');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     * @group slow
     */
    public function ゲートウェイ状態変更_異常系_AMQP_タイムアウト() {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //AMQPのタイムアウトが発生するように設定する。
        $this->amqpClient->requestSleep(11);

        $expectedStatusCode = 504;
        $this->assertHttpError(function() use($gatewayId){

            //エラーが返されることを確認する。
            $this->updateGatewayState($gatewayId, 'exclude', 'start');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }



}
