<?php


use Lcas\Test\Constraint\GatewayConstraint;

require_once __DIR__ . '/AbstractGatewayTestCase.php';

class PostGatewaysTest extends AbstractGatewayTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setup();
    }

    /**
     * @test
     */
    public function ゲートウェイ割り当て_正常系_新規割り当て() {

        $macAddress = TEST_DEFAULT_GATEWAY;

        $expectedGatewayData = $this->gatewayFactory->create($macAddress);

        $response = $this->gatewayUtil->registerGateway($macAddress);
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        $gatewayConstraint = new GatewayConstraint($expectedGatewayData);
        $responseJson = json_decode($response->getBody(), true);
        $this->assertThat($responseJson, $gatewayConstraint, '想定されるゲートウェイ情報を含むこと');
    }


    /**
     * @test
     */
    public function ゲートウェイ割り当て_正常系_Parkers新規割り当て() {

        $macAddress = 'A00000006000';

        // the exact fixture will be returned
        $expectedGatewayData = $this->gatewayFactory->create($macAddress);

        $response = $this->gatewayUtil->registerGateway($macAddress);
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        sleep(4);

        $gatewayConstraint = new GatewayConstraint($expectedGatewayData);
        $responseJson = json_decode($response->getBody(), true);
        $this->assertThat($responseJson, $gatewayConstraint, '想定されるゲートウェイ情報を含むこと');

        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($macAddress);
        $response = $this->getGateway($gatewayId);
        $gatewayObj = json_decode($response->getBody(), true);

        $this->assertEquals(true, isset($gatewayObj['dust_ip']), 'dust_ipフィールドを含むこと');
        $this->assertEquals("joining", $gatewayObj['status'], 'statusがjoiningであること');
    }

    /**
     * @test
     */
    public function ゲートウェイ割り当て_異常系_他ユーザーに割当済() {
        //2人目のユーザーを登録してログイン、ゲートウェイを割り当てる
        $anotherUserEmail = 'test01@example.com';
        $this->userUtil->registerUser($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD);
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //1人目のユーザーでログインし直し、同じゲートウェイを割り当てる
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() {
            $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        }, $expectedStatusCode, '有効なエラーレスポンスであること');


    }

    /**
     * @test
     */
    public function ゲートウェイ割り当て_異常系_パラメータ不足_mac_address() {
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() {
            //mac_addressキーを省略
            $json = json_encode([
            ]);
            return $this->httpClient->post('/api/v3/gateways', ['body' => $json]);
        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ゲートウェイ割り当て_異常系_無効なmac_address() {
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() {
            //存在しないMACアドレスを指定
            $this->gatewayUtil->registerGateway('invalid');
        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ゲートウェイ割り当て_異常系_AMQPメッセージにてNG応答あり() {
        $expectedStatusCode = 400;

        //強制的にNGを返すモードを有効にする
        $this->amqpClient->requestForceNgEnabled(true);

        $this->assertGeneralErrorResponse(function() {
            //AMQPのNG応答が返ることを確認する
            $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @group slow
     */
    public function ゲートウェイ割り当て_異常系_AMQPメッセージタイムアウト() {
        $expectedStatusCode = 504;

        //AMQPメッセージの応答前に11秒以上sleepするように設定する
        $this->amqpClient->requestSleep(11);

        $this->assertGeneralErrorResponse(function() {
            //タイムアウトが発生することを確認する。
            $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @group slow
     */
    public function ゲートウェイ割り当て_異常系_AMQPメッセージタイムアウト後OK応答あり() {

        //AMQPメッセージの応答前に11秒以上sleepするように設定する
        $this->amqpClient->requestSleep(11);

        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() {
            //タイムアウトが発生することを確認する。
            $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        }, $expectedStatusCode, '有効なエラーレスポンスであること');
        sleep(4);

        $expectedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);

        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);
        $getApiResponse = $this->getGateway($gatewayId);
        $registeredGatewayJson = json_decode($getApiResponse->getBody(), true);

        $gatewayConstraint = new GatewayConstraint($expectedGatewayData);
        $gatewayConstraint->setGatewayStatus('connected');
        $this->assertThat($registeredGatewayJson, $gatewayConstraint, '想定されるゲートウェイ情報を含むこと');
    }


    /**
     * @test
     * @group slow
     */
    public function ゲートウェイ割り当て_異常系_AMQPメッセージタイムアウト後NG応答あり() {

        //AMQPメッセージの応答前に11秒以上sleepし、NGを応答するように設定する。
        $this->amqpClient->requestSleep(11);
        $this->amqpClient->requestForceNgEnabled(true);


        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() {
            //タイムアウトが発生することを確認する。
            $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        }, $expectedStatusCode, '有効なエラーレスポンスであること');
        sleep(4);

        //DB内に該当するmacアドレスのゲートウェイが存在しないこと
        $gatewayRepository = new \Lcas\Repository\GatewayRepository();
        $registeredGateway = $gatewayRepository->findByMacAddress(TEST_DEFAULT_GATEWAY);
        $this->assertEquals(false, $registeredGateway);
    }


    /**
     * @test
     */
    public function ゲートウェイ再割り当て_正常系_以前と同じ所有者() {

        //GatewayやNodeにラベルを登録する
        $macAddress = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($macAddress);

        $expectedGatewayData = $this->gatewayFactory->create($macAddress);
        $gatewayLabels = [
            'gateway-label1'
        ];
        $this->updateGatewayLabels($gatewayId, $gatewayLabels);

        $nodeLabels = [
            'node-label1'
        ];
        //Nodeのラベル登録は、
        //Gateway割り当て直後のNode Inclusionを受信した後行いたいため、
        //1秒程度sleepする
        sleep(1);
        $targetNodeId = $expectedGatewayData['nodes'][1]['node_id'];
        $this->nodeUtil->updateNodeLabels($targetNodeId, $nodeLabels);

        //デバイスデータも送信する
        $deviceId = $expectedGatewayData['nodes'][1]['devices'][0]['device_id'];
        $deviceType = $expectedGatewayData['nodes'][1]['devices'][0]['device_type'];
        $deviceDataValue = 123;
        $deviceDataUnit = '';
        $this->sendDeviceDataNotification($targetNodeId, $deviceType, $deviceDataValue, $deviceDataUnit);

        //ラベルやデバイスデータを登録した状態でゲートウェイの割当を解除する
        sleep(1);
        $this->gatewayUtil->deregisterGateway($gatewayId);

        //同じユーザーに再度割当を行う
        $response = $this->gatewayUtil->registerGateway($macAddress);

        //ゲートウェイの情報が、レスポンスに含まれていることを確認する。
        //この時、ラベルが維持されていることも確認する
        $gatewayConstraint = new GatewayConstraint($expectedGatewayData);
        $gatewayConstraint->setGatewayLabels($gatewayLabels);

        $responseJson = json_decode($response->getBody(), true);
        $this->assertThat($responseJson, $gatewayConstraint, '想定されるゲートウェイ情報を含むこと');

        //ノードに設定したラベルがクリアされていないことを確認する
        $nodeResponse = $this->httpClient->get('/api/v3/nodes/' . $targetNodeId);
        $nodeDetailJson = json_decode($nodeResponse->getBody(), true);

        $this->assertEquals($nodeLabels[0], $nodeDetailJson['labels'][0], 'ノードのラベルが保持されていること');

        //デバイスデータがクリアされていないことを確認する
        $deviceDataResponse = $this->httpClient->get('/api/v3/devices/' . $deviceId . '/values');
        $deviceDataJson = json_decode($deviceDataResponse->getBody(), true);
        $this->assertEquals($deviceDataValue, $deviceDataJson[0]['value'], 'デバイスデータがクリアされていないこと');
    }


    /**
     * @test
     */
    public function ゲートウェイ再割り当て_正常系_以前と異なる所有者() {

        //ゲートウェイを割り当てて解除する
        $macAddress = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($macAddress);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($macAddress);

        $expectedGatewayData = $this->gatewayFactory->create($macAddress);
        $gatewayLabels = [
            'gateway-label1'
        ];
        $this->updateGatewayLabels($gatewayId, $gatewayLabels);

        $nodeLabels = [
            'node-label1'
        ];
        //Nodeのラベル登録は、
        //Gateway割り当て直後のNode Inclusionを受信した後行いたいため、
        //1秒程度sleepする
        sleep(1);
        $targetNodeId = $expectedGatewayData['nodes'][1]['node_id'];
        $this->nodeUtil->updateNodeLabels($targetNodeId, $nodeLabels);

        //デバイスデータも送信する
        $deviceId = $expectedGatewayData['nodes'][1]['devices'][0]['device_id'];
        $deviceType = $expectedGatewayData['nodes'][1]['devices'][0]['device_type'];
        $deviceDataValue = 123;
        $deviceDataUnit = '';
        $this->sendDeviceDataNotification($targetNodeId, $deviceType, $deviceDataValue, $deviceDataUnit);

        //ラベルやデバイスデータを登録した状態でゲートウェイの割当を解除する
        sleep(1);
        $this->gatewayUtil->deregisterGateway($gatewayId);


        //新しいユーザーにゲートウェイを割り当てる
        $newUserEmail = 'test2@example.com';
        $this->userUtil->registerUser($newUserEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($newUserEmail, TEST_DEFAULT_USER_PASSWORD);
        $response = $this->gatewayUtil->registerGateway($macAddress);
        $expectedGatewayData = $this->gatewayFactory->create($macAddress);

        //Gatewayのラベルがクリアされていることを確認する
        $gatewayConstraint = new GatewayConstraint($expectedGatewayData);
        $gatewayConstraint->setGatewayLabels([]);
        $responseJson = json_decode($response->getBody(), true);
        $this->assertThat($responseJson, $gatewayConstraint, '想定されるゲートウェイ情報を含むこと');

        //ノードに設定したラベルがクリアされていることを確認する
        $nodeResponse = $this->httpClient->get('/api/v3/nodes/' . $targetNodeId);
        $nodeDetailJson = json_decode($nodeResponse->getBody(), true);

        $this->assertEquals([], $nodeDetailJson['labels'], 'ノードのラベルがクリアされていること');

        //デバイスデータがクリアされていないことを確認する
        $deviceDataResponse = $this->httpClient->get('/api/v3/devices/' . $deviceId . '/values');
        $deviceDataJson = json_decode($deviceDataResponse->getBody(), true);
        $this->assertEquals([], $deviceDataJson, 'デバイスデータがクリアされていること');
    }

}
