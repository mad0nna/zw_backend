<?php


require_once __DIR__ . '/AbstractGatewayTestCase.php';

class PutGatewaysIdTest extends AbstractGatewayTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setup();
    }

    /**
     * @test
     */
    public function ラベル更新_正常系_新規登録() {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        $response = $this->updateGatewayLabels($gatewayId, [
            'label1'
        ]);

        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        $response = $this->getGateway($gatewayId);
        $json = json_decode($response->getBody(), true);

        $labels = $json['labels'];
        $this->assertEquals('label1', $labels[0], 'ラベルが登録されていること');
    }


    /**
     * @test
     */
    public function ラベル更新_正常系_ラベルの変更() {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //ラベルの新規登録
        $this->updateGatewayLabels($gatewayId, [
            'label1'
        ]);

        //ラベルの更新
        $response = $this->updateGatewayLabels($gatewayId, [
            'label2'
        ]);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        $response = $this->getGateway($gatewayId);
        $json = json_decode($response->getBody(), true);

        $labels = $json['labels'];
        $this->assertEquals('label2', $labels[0], 'ラベルが更新されていること');

        $found = false;
        foreach($labels as $label) {
            if($label == 'label1') {
                $found = true;
            }
        }
        $this->assertFalse(false, $found, '以前登録されていたラベルが削除されていること');
    }


    /**
     * @test
     */
    public function ラベル更新_異常系_他ユーザーが所有() {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //別のユーザーでログイン
        $anotherUserEmail = 'test2@example.com';
        $this->userUtil->registerUser($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD);

        $expectedStatusCode = 404;
        $this->assertHttpError(function() use ($gatewayId){

            $this->updateGatewayLabels($gatewayId, [
                'label1'
            ]);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     */
    public function dustip更新_正常系_新規登録() {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = 'A00000006000';
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        $response = $this->updateGatewayDustIp($gatewayId, [
            "network_id" => 65535,
            "acl" => [
                [
                    "mac_addr" => "2147483647",
                    "join_key" => "joining!"
                ]
            ]
        ]);

        $this->assertEquals(202, $response->getStatusCode(), 'ステータスコードが204であること');

        // 300 response 
        $response = $this->getGateway($gatewayId);
        $json = json_decode($response->getBody(), true);
        $dust_ip = $json['dust_ip'];
        $this->assertEquals(false, $dust_ip['acl_set'], 'dust_ip.acl_setが登録されていないこと');

        sleep(5);

        // 200 response
        $response = $this->getGateway($gatewayId);
        $json = json_decode($response->getBody(), true);
        $dust_ip = $json['dust_ip'];
        $this->assertEquals(true, $dust_ip['acl_set'], 'dust_ip.acl_setが登録されていること');
        $this->assertEquals(65535, $dust_ip['network_id'], 'dust.network_idが登録されていること');
    }


    /**
     * @test
     */
    public function dustip更新_正常系_登録失敗() {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = 'A00000006000';
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);
        $this->amqpClient->requestForceNgEnabled(true);

        $response = $this->updateGatewayDustIp($gatewayId, [
            "network_id" => 65535,
            "acl" => [
                [
                    "mac_addr" => "2147483647",
                    "join_key" => "joining!"
                ]
            ]
        ]);

        $this->assertEquals(202, $response->getStatusCode(), 'ステータスコードが204であること');

        // 300 response 
        $response = $this->getGateway($gatewayId);
        $json = json_decode($response->getBody(), true);
        $dust_ip = $json['dust_ip'];
        $this->assertEquals(false, $dust_ip['acl_set'], 'dust_ip.acl_setが登録されていないこと');

        sleep(5);

        // 400 response
        $response = $this->getGateway($gatewayId);
        $json = json_decode($response->getBody(), true);
        $dust_ip = $json['dust_ip'];
        $this->assertEquals(false, $dust_ip['acl_set'], 'dust_ip.acl_setが登録されていること');

    }


    /**
     * @test
     */
    public function dustip更新_正常系_登録失敗後に再登録() {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = 'A00000006000';
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);
        $this->amqpClient->requestForceNgEnabled(true);

        $response = $this->updateGatewayDustIp($gatewayId, [
            "network_id" => 65535,
            "acl" => [
                [
                    "mac_addr" => "2147483647",
                    "join_key" => "joining!"
                ]
            ]
        ]);

        // 300 response 
        $response = $this->getGateway($gatewayId);
        $json = json_decode($response->getBody(), true);
        $dust_ip = $json['dust_ip'];
        $this->assertEquals(false, $dust_ip['acl_set'], 'dust_ip.acl_setが登録されていないこと');

        sleep(5);

        // 400 response
        $response = $this->getGateway($gatewayId);
        $json = json_decode($response->getBody(), true);
        $dust_ip = $json['dust_ip'];
        $this->assertEquals(false, $dust_ip['acl_set'], 'dust_ip.acl_setが登録されていること');

        $this->amqpClient->requestForceNgEnabled(false);

        $response = $this->updateGatewayDustIp($gatewayId, [
            "network_id" => 65535,
            "acl" => [
                [
                    "mac_addr" => "2147483647",
                    "join_key" => "joining!"
                ]
            ]
        ]);

        $this->assertEquals(202, $response->getStatusCode(), 'ステータスコードが204であること');

        // 300 response 
        $response = $this->getGateway($gatewayId);
        $json = json_decode($response->getBody(), true);
        $dust_ip = $json['dust_ip'];
        $this->assertEquals(false, $dust_ip['acl_set'], 'dust_ip.acl_setが登録されていないこと');

        sleep(5);

        // 200 response
        $response = $this->getGateway($gatewayId);
        $json = json_decode($response->getBody(), true);
        $dust_ip = $json['dust_ip'];
        $this->assertEquals(true, $dust_ip['acl_set'], 'dust_ip.acl_setが登録されていること');
        $this->assertEquals(65535, $dust_ip['network_id'], 'dust.network_idが登録されていること');
    }


    /**
     * @test
     */
    public function dustip更新_異常系_他ユーザーが所有() {

        //ユーザーにゲートウェイを割り当てる
        $defaultGateway = 'A00000006000';
        $this->gatewayUtil->registerGateway($defaultGateway);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);

        //別のユーザーでログイン
        $anotherUserEmail = 'test2@example.com';
        $this->userUtil->registerUser($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD);

        $expectedStatusCode = 404;
        $this->assertHttpError(function() use ($gatewayId){

            $this->updateGatewayDustIp($gatewayId, [
                "network_id" => "65535",
                "acl" => [
                    [
                        "mac_addr" => "2147483647",
                        "join_key" => "joining!"
                    ]
                ]
            ]);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }
}
