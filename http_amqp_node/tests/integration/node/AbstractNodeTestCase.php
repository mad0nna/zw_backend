<?php


use Lcas\Repository\GatewayRepository;
use Lcas\Repository\NodeRepository;
use Lcas\Test\Fixture\GatewayFixtureFactory;
use Lcas\Test\Fixture\NodeFixtureFactory;
use Lcas\Test\Util\NodeUtil;
use Lcas\Test\Util\TestUtil;

class AbstractNodeTestCase extends \Lcas\Test\IntegrationTestCase {


    /**
     * @var NodeRepository
     */
    protected $nodeRepository;


    /**
     * @var GatewayFixtureFactory
     */
    protected $gatewayFactory;


    /**
     * @var GatewayRepository
     */
    protected $gatewayRepository;

    /**
     * @var NodeFixtureFactory
     */
    protected $nodeFactory;

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->nodeRepository = new NodeRepository();
        $this->nodeFactory = new NodeFixtureFactory();
        $this->gatewayFactory = new GatewayFixtureFactory();
    }


    public function setUp() {
        parent::setUp();

        //テストデータを全削除
        TestUtil::clearDatabase();

        //デフォルトのユーザーを登録する
        $this->userUtil->registerDefaultUser();
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

    }


    protected function getNodes($label=null, $target=null) {

        $queries = [];
        if(!is_null($label)) {
            $queries['label'] = $label;
        }
        if(!is_null($target)) {
        	$queries['target'] = $target;
        }
        
        return $this->httpClient->get('/api/v3/nodes', ['query' => $queries]);
    }

    protected function getGatewayIdFromNodeId($nodeId) {
        $db = Lcas\DB\DB::getMasterDb();
        $escapedNodeId = $db->Escape($nodeId);

        $sql = "SELECT gateway_id FROM nodes WHERE id='{$escapedNodeId}'";
        $db->setSql($sql);

        if(!$db->Query()) {
            throw new \Exception('SQLエラー：' . $sql);
        }

        $result = $db->Fetch();
        return $result['gateway_id'];
    }

}
