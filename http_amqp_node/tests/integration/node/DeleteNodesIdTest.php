<?php


use Lcas\Test\Constraint\NodeConstraint;

require_once __DIR__ . '/AbstractNodeTestCase.php';

class DeleteNodesIdTest extends AbstractNodeTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @test
     */
    public function ノードの削除_正常系_一般_自身の所有するノードの削除() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //ユーザーが所有しているノードの1つをを参照
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $targetNode = $gatewayData['nodes'][1];
        $response = $this->deleteNode($targetNode['node_id']);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //削除後にノードを参照するとエラーになることを確認する
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($targetNode){

            $this->nodeUtil->getNodeById($targetNode['node_id']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     */
    public function ノードの削除_異常系_一般_他ユーザーのノードの削除() {

        //デフォルト以外のユーザーを登録してゲートウェイを割り当てる
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        $exampleGateway = 'A00000001000';
        $this->gatewayUtil->registerGateway($exampleGateway);

        //デフォルトのユーザーでログイン
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //user2が所有しているノードを削除しようとするとエラーになることを確認する
        $exampleGatewayData = $this->gatewayFactory->create($exampleGateway);
        $targetNode = $exampleGatewayData['nodes'][1];

        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($targetNode){

            $this->deleteNode($targetNode['node_id']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     */
    public function ノードの削除_異常系_一般_存在しないノードの削除() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //存在しないノードを削除しようとするとエラーになることを確認する
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function(){

            $this->deleteNode(1);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     */
    public function ノードの削除_異常系_ノード共有_ノード共有されたノードの削除() {

        //デフォルト以外のユーザーを登録してゲートウェイを割り当てる
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        $exampleGateway = 'A00000001000';
        $exampleGatewayData = $this->gatewayFactory->create($exampleGateway);
        $sharedNode = $exampleGatewayData['nodes'][1];
        $this->gatewayUtil->registerGateway($exampleGateway);

        //Node Inclusionが取り込まれるまで2秒まつ
        sleep(4);

        //デフォルトのユーザーに対しノード共有を許可する
        $this->nodeUtil->shareNodeWith($sharedNode['node_id'], [
            TEST_DEFAULT_USER_EMAIL
        ]);

        //デフォルトのユーザーでログイン
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //ノード共有されたノードを削除しようとするとエラーになることを確認する
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($sharedNode){

            $this->deleteNode($sharedNode['node_id']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }



    private function deleteNode($nodeId) {
        return $this->httpClient->delete('/api/v3/nodes/' . $nodeId);
    }
}
