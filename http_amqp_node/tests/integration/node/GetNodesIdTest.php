<?php


use Lcas\Test\Constraint\NodeConstraint;

require_once __DIR__ . '/AbstractNodeTestCase.php';

class GetNodesIdTest extends AbstractNodeTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @test
     */
    public function 指定ノード参照_正常系_一般_自身の所有するノードの参照() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        
        sleep(3);

        //ユーザーが所有しているノードの1つをを参照
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $response = $this->nodeUtil->getNodeById($expectedNode['node_id']);
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //レスポンスのノード一覧が期待している内容か確認する
        $node = json_decode($response->getBody(), true);
        $gatewayId = $this->getGatewayIdFromNodeId($expectedNode['node_id']);

        $nodeConstraint = new NodeConstraint($expectedNode);
        $nodeConstraint->setGatewayId($gatewayId);

        $this->assertThat($node, $nodeConstraint, "想定されたノード情報を含むこと");
    }


    /**
     * @test
     */
    public function 指定ノード参照_正常系_ノード共有_ノード共有されたノードの参照() {

        //2人目のユーザーを登録してログイン
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $macAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($macAddress);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($sharedNodeId);
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //ノード共有されたノードを参照する
        $response = $this->nodeUtil->getNodeById($sharedNodeId);
        $this->assertEquals(200, $response->getStatusCode());

        $node = json_decode($response->getBody(), true);

        //共有されたノードが参照できることを確認する
        $nodeConstraint = new NodeConstraint($sharedNode);
        $nodeConstraint->setSharingLevel('users');
        //Node共有された側のユーザーはノードのオーナーではないため、falseを指定する。
        $nodeConstraint->setIsOwner(false);

        $this->assertThat($node, $nodeConstraint, '想定されるノード情報を含んでいること');
    }


    /**
     * @test
     */
    public function 指定ノード参照_正常系_ノード共有_ノード共有されたノードの参照_ゲートウェイなし() {

        //2人目のユーザーを登録してログイン
        //(このユーザーはゲートウェイを持たない)
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($sharedNodeId);
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //共有されたノードを参照する
        $response = $this->nodeUtil->getNodeById($sharedNodeId);
        $this->assertEquals(200, $response->getStatusCode());

        $node = json_decode($response->getBody(), true);

        //共有されたノードを参照できることを確認する
        $nodeConstraint = new NodeConstraint($sharedNode);
        $nodeConstraint->setSharingLevel('users');
        //Node共有された側のユーザーはノードのオーナーではないため、falseを指定する。
        $nodeConstraint->setIsOwner(false);

        $this->assertThat($node, $nodeConstraint, '想定されるノード情報を含んでいること');
    }


    /**
     * @test
     */
    public function 指定ノード参照_異常系_一般_他ユーザーのノードの参照() {

        //2人目のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $exampleGatewayMacAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($exampleGatewayMacAddress);

        $exampleGatewayData = $this->gatewayFactory->create($exampleGatewayMacAddress);
        $targetNode = $exampleGatewayData['nodes'][1];

        //デフォルトのユーザーでログインし直す
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //ユーザーが所有していないノードを参照出来無いことを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use ($targetNode){

            $this->nodeUtil->getNodeById($targetNode['node_id']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function 指定ノード参照_異常系_一般_存在しないノードの参照() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //ユーザーが所有していないノードを参照出来無いことを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function(){

            $this->nodeUtil->getNodeById(1);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function 指定ノード参照_異常系_ノード共有_ノード共有解除された状態でノードを参照() {
        //2人目のユーザーを登録してログイン
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $macAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($macAddress);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //ノード共有を解除する
        $this->nodeUtil->shareNodeWith($sharedNodeId, []);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //共有解除されたノードを参照出来ないこと
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($sharedNodeId) {

            $this->nodeUtil->getNodeById($sharedNodeId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     */
    public function 指定ノード参照_異常系_一般_インクルード失敗ノードの参照() {
    	//デフォルトのゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway('A00000005000');
    	
    	sleep(4);
    
    	//ユーザーが所有しているノードの1つをを参照
    	$gatewayData = $this->gatewayFactory->create('A00000005000');
    	$expectedNode = $gatewayData['nodes'][0];
    	$response = $this->nodeUtil->getNodeById($expectedNode['node_id']);
    	$this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');
    
    	//レスポンスのノード一覧が期待している内容か確認する
    	$node = json_decode($response->getBody(), true);
    	$gatewayId = $this->getGatewayIdFromNodeId($expectedNode['node_id']);
    
    	$nodeConstraint = new NodeConstraint($expectedNode);
    	$nodeConstraint->setGatewayId($gatewayId);
    
    	$this->assertThat($node, $nodeConstraint, "想定されたノード情報を含むこと");
    }
    
    
    
}
