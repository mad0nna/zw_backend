<?php


use Lcas\Test\Constraint\NodeConstraint;

require_once __DIR__ . '/AbstractNodeTestCase.php';

class GetNodesTest extends AbstractNodeTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @test
     */
    public function ノード一覧参照_正常系_一般_自身のノード一覧の参照() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        
        sleep(3);

        //ユーザーの全ノード一覧を取得
        $response = $this->getNodes();
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //レスポンスのノード一覧が期待している内容か確認する
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNodes = $gatewayData['nodes'];
        $json = json_decode($response->getBody(), true);

        foreach($expectedNodes as $idx=>$expectedNode) {
            $node = $json[$idx];
            $gatewayId = $this->getGatewayIdFromNodeId($expectedNode['node_id']);

            $nodeConstraint = new NodeConstraint($expectedNode);
            $nodeConstraint->setGatewayId($gatewayId);

            $this->assertThat($node, $nodeConstraint, "想定されたノード情報を含むこと({$idx})");
        }

    }


    /**
     * @test
     */
    public function ノード一覧参照_正常系_ラベル指定してノード一覧を参照() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        sleep(3);
        
        //ノードにラベルを割り当てる
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNodes = $gatewayData['nodes'];
        $expectedNode = $expectedNodes[1];
        $nodeId = $expectedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($nodeId);
        $expectedLabels = ['test-label-test', 'aaa'];
        $this->nodeUtil->updateNodeLabels($nodeId, $expectedLabels);

        //無関係なノードにラベルを割り当てる
        $nodeId2 = $expectedNodes[2]['node_id'];
        $dummyLabels = ['test', 'bbb'];
        $this->nodeUtil->updateNodeLabels($nodeId2, $dummyLabels);


        //ラベルを指定してユーザーの全ノード一覧を取得
        $response = $this->getNodes('label');
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //"label"のキーワードに部分一致するノードのみ含まれていることを確認する
        $nodes = json_decode($response->getBody(), true);

        $this->assertCount(1, $nodes, '1件のノードのみ取得できること');

        $nodeConstraint = new NodeConstraint($expectedNode);
        $nodeConstraint->setLabels($expectedLabels);
        $nodeConstraint->setGatewayId($gatewayId);

        $this->assertThat($nodes[0], $nodeConstraint, "想定されたノード情報を含むこと");
    }


    /**
     * @test
     */
    public function ノード一覧参照_正常系_ノード共有_ノード共有された状態でノード一覧の参照() {

        //2人目のユーザーを登録してログイン
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $macAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($macAddress);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //ノード一覧を参照する
        $response = $this->getNodes();
        $this->assertEquals(200, $response->getStatusCode());

        $nodes = json_decode($response->getBody(), true);

        //ユーザーが保持しているノード + 共有されたノードが含まれていることを確認する
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $expectedNodes = [];
        $expectedNodes[] = $sharedNode;
        $expectedNodes = array_merge($expectedNodes, $gatewayData['nodes']);

        foreach($expectedNodes as $idx=>$expectedNode) {
            $nodeConstraint = new NodeConstraint($expectedNode);
            //Node共有された側のユーザーはノードのオーナーではないため、falseを指定する。
            $nodeConstraint->setIsOwner(false);

            if($expectedNode['node_id'] == $sharedNodeId) {
                $nodeConstraint->setSharingLevel('users');
                //Node共有された側に対しては"shared_with"は提供されないため、
                //空の配列であることを検証する。
                $nodeConstraint->setSharedWith([]);
            }

            $this->assertThat($nodes[$idx], $nodeConstraint, '想定されるノード情報を含んでいること');
        }
    }


    /**
     * @test
     */
    public function ノード一覧参照_正常系_ノード共有_ノード共有された状態でノード一覧の参照_ゲートウェイなし() {

        //2人目のユーザーを登録してログイン
        //(このユーザーはゲートウェイを持たない)
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //ノード一覧を参照する
        $response = $this->getNodes();
        $this->assertEquals(200, $response->getStatusCode());

        $nodes = json_decode($response->getBody(), true);

        //共有されたノードが含まれていることを確認する
        $nodeConstraint = new NodeConstraint($sharedNode);
        $nodeConstraint->setSharingLevel('users');
        //Node共有された側のユーザーはノードのオーナーではないため、falseを指定する。
        $nodeConstraint->setIsOwner(false);
        $this->assertThat($nodes[0], $nodeConstraint, '想定されるノード情報を含んでいること');
    }

    /**
     * @test
     */
    public function ノード一覧参照_正常系_インクルード失敗のみを参照(){
    	//ゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway('A00000005000');
    	$gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress('A00000005000');
    
    	//Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
    	sleep(4);
    
    	//ユーザーの失敗ノード一覧を取得
    	$response = $this->getNodes(null, 'failed-nodes');
    	$this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');
    
    	//レスポンスのノード一覧が期待している内容か確認する
    	$gatewayData = $this->gatewayFactory->create('A00000005000');
    	$expectedNode = $gatewayData['nodes'][0];
    	$json = json_decode($response->getBody(), true);
    
    	$this->assertEquals(1, count($json), "想定されたノード情報のみを含むこと");

    	$node = $json[0];
    	$gatewayId = $this->getGatewayIdFromNodeId($expectedNode['node_id']);
    
    	$nodeConstraint = new NodeConstraint($expectedNode);
    	$nodeConstraint->setGatewayId($gatewayId);
    
    	$this->assertThat($node, $nodeConstraint, "想定されたノード情報を含むこと");
    }
    
    
    /**
     * @test
     */
    public function ノード一覧参照_正常系_明示的にすべてを参照(){
    	//ゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway('A00000005000');
    	$gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress('A00000005000');
    
    	//Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
    	sleep(4);
    
    	//ユーザーの失敗ノード一覧を取得
    	$response = $this->getNodes(null, 'all-nodes');
    	$this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');
    
        //レスポンスのノード一覧が期待している内容か確認する
        $gatewayData = $this->gatewayFactory->create('A00000005000');
        $expectedNodes = $gatewayData['nodes'];
        $json = json_decode($response->getBody(), true);

    	$this->assertEquals(2, count($json), "すべてのノード情報を含むこと");

        foreach($expectedNodes as $idx=>$expectedNode) {
            $node = $json[$idx];
            $gatewayId = $this->getGatewayIdFromNodeId($expectedNode['node_id']);

            $nodeConstraint = new NodeConstraint($expectedNode);
            $nodeConstraint->setGatewayId($gatewayId);

            $this->assertThat($node, $nodeConstraint, "想定されたノード情報を含むこと({$idx})");
        }
    }
    
    
    /**
     * @test
     */
    public function ノード一覧参照_異常系_target値不正(){
    	//ゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway('A00000005000');
    	$gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress('A00000005000');
    
    	//Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
    	sleep(4);
    
    	//ユーザーの失敗ノード一覧を取得
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() {

            $this->getNodes(null, 'invalid');
            
        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }
    
    
    /**
     * @test
     */
    public function ノード一覧参照_異常系_ノード共有_ノード共有解除された状態でノード一覧を参照() {
        //2人目のユーザーを登録してログイン
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        //任意のゲートウェイを割り当てる
        $macAddress = 'A00000001000';
        $this->gatewayUtil->registerGateway($macAddress);

        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Node Inclusionが送信されるまで2秒待機する
        sleep(4);

        //user2に対しノード共有を許可する
        $sharedGatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $sharedNode = $sharedGatewayData['nodes'][1];
        $sharedNodeId = $sharedNode['node_id'];
        $this->nodeUtil->shareNodeWith($sharedNodeId, [$user2Email]);

        //ノード共有を解除する
        $this->nodeUtil->shareNodeWith($sharedNodeId, []);

        //user2でログインする
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //ノード一覧を参照する
        $response = $this->getNodes();
        $this->assertEquals(200, $response->getStatusCode());

        $nodes = json_decode($response->getBody(), true);

        //ユーザーが保持しているノードが含まれていることを確認する
        $gatewayData = $this->gatewayFactory->create($macAddress);
        $expectedNodes = $gatewayData['nodes'];

        foreach($expectedNodes as $idx=>$expectedNode) {
            $gatewayId = $this->getGatewayIdFromNodeId($expectedNode['node_id']);
            $nodeConstraint = new NodeConstraint($expectedNode);
            $nodeConstraint->setGatewayId($gatewayId);
            $this->assertThat($nodes[$idx], $nodeConstraint, '想定されるノード情報を含んでいること');
        }

        //その他のノードが含まれていないことを確認する
        $this->assertCount(count($expectedNodes), $nodes, 'ノード共有解除されたノードが含まれていないこと');
    }

    /**
     * @test
     */
    public function ノード一覧参照_異常系_インクルード失敗ノードがある(){
    	//ゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway('A00000005000');
    	$gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress('A00000005000');
    
    	//Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
    	sleep(4);
    
        //ユーザーの全ノード一覧を取得
        $response = $this->getNodes();
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        //レスポンスのノード一覧が期待している内容か確認する
        $gatewayData = $this->gatewayFactory->create('A00000005000');
        $expectedNodes = $gatewayData['nodes'];
        $json = json_decode($response->getBody(), true);

        foreach($expectedNodes as $idx=>$expectedNode) {
            $node = $json[$idx];
            $gatewayId = $this->getGatewayIdFromNodeId($expectedNode['node_id']);

            $nodeConstraint = new NodeConstraint($expectedNode);
            $nodeConstraint->setGatewayId($gatewayId);

            $this->assertThat($node, $nodeConstraint, "想定されたノード情報を含むこと({$idx})");
        }
    }
    
    
    
}
