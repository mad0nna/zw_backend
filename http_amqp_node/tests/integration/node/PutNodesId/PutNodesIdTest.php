<?php


use Lcas\Test\Constraint\NodeConstraint;

require_once __DIR__ . '/../AbstractNodeTestCase.php';

class PutNodesIdTest extends AbstractNodeTestCase {

    private $userRepository;

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->userRepository = new \Lcas\Repository\UserRepository();
    }

    /**
     * @test
     */
    public function ノード設定変更_正常系_共有設定変更_private_to_users() {

        //共有対象のユーザーを登録しておく
        $this->userUtil->registerUser('user2@example.com', TEST_DEFAULT_USER_PASSWORD, 'none');

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($expectedNodeId);

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        //ノードの共有設定をusersに変更する
        //usersに変更する場合、shared_withは必須で、1人以上を指定する必要がある
        $expectedSharedWith = [
            ['email' => 'user2@example.com']
        ];
        $response = $this->nodeUtil->updateNode($expectedNodeId, [
            'sharing_level' => 'users',
            'shared_with' => $expectedSharedWith
        ]);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //ノードの情報を参照する
        $response = $this->nodeUtil->getNodeById($expectedNodeId);

        $json = json_decode($response->getBody(), true);

        //sharing_levelが変化していること
        $nodeConstraint = new NodeConstraint($expectedNode);
        $nodeConstraint->setSharingLevel('users');
        $nodeConstraint->setSharedWith($expectedSharedWith);
        $nodeConstraint->setGatewayId($gatewayId);

        $this->assertThat($json, $nodeConstraint, 'sharing_levelの値が更新されていること');
    }


    /**
     * @test
     */
    public function ノード設定変更_正常系_共有設定変更_users_to_private() {

        //2人目のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($expectedNodeId);

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        //ノードの共有設定をusersに変更する
        //usersに変更する場合、shared_withは必須で、1人以上を指定する必要がある
        $this->nodeUtil->updateNode($expectedNodeId, [
            'sharing_level' => 'users',
            'shared_with' => [
                ['email' => $user2Email]
            ]
        ]);

        //privateに戻す
        $response = $this->nodeUtil->updateNode($expectedNodeId, [
            'sharing_level' => 'private'
        ]);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');


        //ノードの情報を参照する
        $response = $this->nodeUtil->getNodeById($expectedNodeId);

        $json = json_decode($response->getBody(), true);

        //sharing_levelが変化していること
        $nodeConstraint = new NodeConstraint($expectedNode);
        $nodeConstraint->setSharingLevel('private');
        $nodeConstraint->setSharedWith([
            ['email' => $user2Email]
        ]);
        $nodeConstraint->setGatewayId($gatewayId);

        $this->assertThat($json, $nodeConstraint, '想定されるノード情報を含んでいること');
    }


    /**
     * @test
     */
    public function ノード設定変更_正常系_共有設定変更_users_to_users() {

        //共有対象のユーザーを登録しておく
        $this->userUtil->registerUser('user2@example.com', TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->registerUser('user3@example.com', TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->registerUser('user4@example.com', TEST_DEFAULT_USER_PASSWORD, 'none');

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($expectedNodeId);

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        //ノードの共有設定をusersに変更する
        //usersに変更する場合、shared_withは必須で、1人以上を指定する必要がある
        $this->nodeUtil->updateNode($expectedNodeId, [
            'sharing_level' => 'users',
            'shared_with' => [
                ['email' => 'user2@example.com']
            ]
        ]);

        //共有対象ユーザーを変更して共有設定を変更する
        $expectedSharedWith = [
            ['email' => 'user3@example.com'],
            ['email' => 'user4@example.com'],
        ];
        $response = $this->nodeUtil->updateNode($expectedNodeId, [
            'sharing_level' => 'users',
            'shared_with' => $expectedSharedWith
        ]);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //ノードの情報を参照する
        $response = $this->nodeUtil->getNodeById($expectedNodeId);

        $json = json_decode($response->getBody(), true);

        //sharing_levelが変化していること
        $nodeConstraint = new NodeConstraint($expectedNode);
        $nodeConstraint->setSharingLevel('users');
        $nodeConstraint->setSharedWith($expectedSharedWith);
        $nodeConstraint->setGatewayId($gatewayId);

        $this->assertThat($json, $nodeConstraint, 'sharing_levelの値が更新されていること');
    }


    /**
     * @test
     */
    public function ノード設定変更_正常系_共有設定変更_private_to_private() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        //ノードの共有設定をprivateに変更する
        $response = $this->nodeUtil->updateNode($expectedNodeId, [
            'sharing_level' => 'private'
        ]);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
    }


    /**
     * @test
     */
    public function ノード設定変更_正常系_共有設定変更_share_by_id() {

        //共有対象のユーザーを登録しておく
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $user2 = $this->userRepository->findUserByEmail($user2Email);
        $user2Id = $user2['login_id'];  //users.idではなくusers.login_idを使用する

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($expectedNodeId);

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        //ID形式を使用してノード共有の設定を行う。
        $expectedSharedWith = [
            ['id' => (int)$user2Id]
        ];
        $response = $this->nodeUtil->updateNode($expectedNodeId, [
            'sharing_level' => 'users',
            'shared_with' => $expectedSharedWith
        ]);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //ノードの情報を参照する
        $response = $this->nodeUtil->getNodeById($expectedNodeId);

        $json = json_decode($response->getBody(), true);

        //sharing_levelが変化していること
        $nodeConstraint = new NodeConstraint($expectedNode);
        $nodeConstraint->setSharingLevel('users');
        $nodeConstraint->setSharedWith($expectedSharedWith);
        $nodeConstraint->setGatewayId($gatewayId);

        $this->assertThat($json, $nodeConstraint, 'shared_withの値が更新されていること');
    }


    /**
     * @test
     */
    public function ノード設定変更_正常系_共有設定変更_share_by_url() {

        //共有対象のユーザーを登録しておく
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $user2 = $this->userRepository->findUserByEmail($user2Email);
        $user2Id = $user2['login_id'];  //users.idではなくusers.login_idを使用する

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($expectedNodeId);

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        //ID形式を使用してノード共有の設定を行う。
        $expectedSharedWith = [
            ['url' => '/api/v3/users/' . $user2Id]
        ];
        $response = $this->nodeUtil->updateNode($expectedNodeId, [
            'sharing_level' => 'users',
            'shared_with' => $expectedSharedWith
        ]);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //ノードの情報を参照する
        $response = $this->nodeUtil->getNodeById($expectedNodeId);

        $json = json_decode($response->getBody(), true);

        //sharing_levelが変化していること
        $nodeConstraint = new NodeConstraint($expectedNode);
        $nodeConstraint->setSharingLevel('users');
        $nodeConstraint->setSharedWith($expectedSharedWith);
        $nodeConstraint->setGatewayId($gatewayId);

        $this->assertThat($json, $nodeConstraint, 'shared_withの値が更新されていること');
    }


    /**
     * @test
     */
    public function ノード設定変更_正常系_共有設定変更_share_by_email() {

        //共有対象のユーザーを登録しておく
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($expectedNodeId);

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        //ID形式を使用してノード共有の設定を行う。
        $expectedSharedWith = [
            ['email' => $user2Email]
        ];
        $response = $this->nodeUtil->updateNode($expectedNodeId, [
            'sharing_level' => 'users',
            'shared_with' => $expectedSharedWith
        ]);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //ノードの情報を参照する
        $response = $this->nodeUtil->getNodeById($expectedNodeId);

        $json = json_decode($response->getBody(), true);

        //sharing_levelが変化していること
        $nodeConstraint = new NodeConstraint($expectedNode);
        $nodeConstraint->setSharingLevel('users');
        $nodeConstraint->setSharedWith($expectedSharedWith);
        $nodeConstraint->setGatewayId($gatewayId);

        $this->assertThat($json, $nodeConstraint, 'shared_withの値が更新されていること');
    }


    /**
     * @test
     */
    public function ノード設定変更_正常系_共有設定変更_share_by_email_not_exist() {

        //デフォルトのゲートウェイを割り当てる
    	$exampleGateway = TEST_DEFAULT_GATEWAY;
        $this->gatewayUtil->registerGateway($exampleGateway);
        $gatewayData = $this->gatewayFactory->create($exampleGateway);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($expectedNodeId);

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($exampleGateway){

            $exampleGatewayData = $this->gatewayFactory->create($exampleGateway);
            $targetNode = $exampleGatewayData['nodes'][1];

	        //存在しないユーザーのメールアドレスを指定して登録を実行する
	        $sharedWith = [
	            ['email' => 'unknown@example.com']
	        ];
	            
            $this->nodeUtil->updateNode($targetNode['node_id'], [
                'sharing_level' => 'users',
                'shared_with' => $sharedWith
            ]);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ノード設定変更_正常系_ラベル変更_ラベルの初回登録() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($expectedNodeId);

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        //ラベルの登録を行う。
        $expectedLabels = [
            'label1:abcd',
            'label2:123',
        ];
        $response = $this->nodeUtil->updateNode($expectedNodeId, [
            'labels' => $expectedLabels
        ]);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //ノードの情報を参照する
        $response = $this->nodeUtil->getNodeById($expectedNodeId);

        $json = json_decode($response->getBody(), true);

        //ラベルが登録されていること
        $nodeConstraint = new NodeConstraint($expectedNode);
        $nodeConstraint->setLabels($expectedLabels);
        $nodeConstraint->setGatewayId($gatewayId);

        $this->assertThat($json, $nodeConstraint, 'labelsの値が更新されていること');
    }


    /**
     * @test
     */
    public function ノード設定変更_正常系_ラベル変更_ラベルの更新() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];
        $gatewayId = $this->getGatewayIdFromNodeId($expectedNodeId);

        //Node Inclusionの取り込みが完了するまで2秒まつ
        sleep(4);

        //ラベルの登録を行う。
        $originalLabels = [
            'label1:abcd',
            'label2:123',
        ];
        $this->nodeUtil->updateNode($expectedNodeId, [
            'labels' => $originalLabels
        ]);

        //ラベルの更新を行う。
        $expectedLabels = [
            'new-label',
        ];
        $response = $this->nodeUtil->updateNode($expectedNodeId, [
            'labels' => $expectedLabels
        ]);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //ノードの情報を参照する
        $response = $this->nodeUtil->getNodeById($expectedNodeId);

        $json = json_decode($response->getBody(), true);

        //ラベルが登録されていること
        $nodeConstraint = new NodeConstraint($expectedNode);
        $nodeConstraint->setLabels($expectedLabels);
        $nodeConstraint->setGatewayId($gatewayId);

        $this->assertThat($json, $nodeConstraint, 'labelsの値が更新されていること');
    }


    /**
     * @test
     */
    public function ノード設定変更_異常系_一般_他ユーザーのノードの共有設定変更() {

        //デフォルト以外のユーザーを登録し、ゲートウェイを割り当てる
        $gatewayOwnerEmail = 'owner@example.com';
        $this->userUtil->registerUser($gatewayOwnerEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($gatewayOwnerEmail, TEST_DEFAULT_USER_PASSWORD);
        $exampleGateway = 'A00000001000';
        $this->gatewayUtil->registerGateway($exampleGateway);

        //Node Inclusionを待つため2秒sleepする
        sleep(4);

        //デフォルトのユーザーでログイン
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //他のユーザーが所有するノードに対しノード共有設定を行おうとすると
        //エラーになることを確認する
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($exampleGateway){

            $exampleGatewayData = $this->gatewayFactory->create($exampleGateway);
            $targetNode = $exampleGatewayData['nodes'][1];

            $this->nodeUtil->updateNode($targetNode['node_id'], [
                'sharing_level' => 'users',
                'shared_with' => []
            ]);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ノード設定変更_異常系_一般_存在しないノードの共有設定変更() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //存在しないノードに共有設定変更を行おうとするとエラーになることを確認する
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() {

            $this->nodeUtil->updateNode(1, [
                'sharing_level' => 'users',
                'shared_with' => []
            ]);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ノード設定変更_異常系_ノード共有_ノード共有されたノードの共有設定変更() {

        //デフォルト以外のユーザーを登録し、ゲートウェイを割り当てる
        $gatewayOwnerEmail = 'owner@example.com';
        $this->userUtil->registerUser($gatewayOwnerEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($gatewayOwnerEmail, TEST_DEFAULT_USER_PASSWORD);
        $exampleGateway = 'A00000001000';
        $this->gatewayUtil->registerGateway($exampleGateway);

        //Node Inclusionを待つため2秒sleepする
        sleep(4);

        //デフォルトのユーザーに対しノード共有を許可する
        $gatewayData = $this->gatewayFactory->create($exampleGateway);
        $sharedNode = $gatewayData['nodes'][1];
        $this->nodeUtil->shareNodeWith($sharedNode['node_id'], [
            TEST_DEFAULT_USER_EMAIL
        ]);

        //デフォルトのユーザーでログイン
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //ノード共有されたノードに対し共有設定変更を行おうとするとエラーになることを確認する
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($sharedNode){

            $this->nodeUtil->updateNode($sharedNode['node_id'], [
                'sharing_level' => 'users',
                'shared_with' => [],
            ]);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ノード設定変更_異常系_必須項目省略_shared_withの省略() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //ノード共有設定変更時、sharing_level=usersの状態で
        //shared_withを省r略するとエラーになることを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId){

            $this->nodeUtil->updateNode($expectedNodeId, [
                'sharing_level' => 'users',
            ]);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    /**
     * @test
     */
    public function ノード設定変更_異常系_異常値指定_sharing_levelに無効な値を指定() {

        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //ノード共有設定変更時、sharing_levelに無効な値を指定すると
        //エラーになることを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId){

            $this->nodeUtil->updateNode($expectedNodeId, [
                'sharing_level' => 'invalid',
                'shared_with' => []
            ]);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

}
