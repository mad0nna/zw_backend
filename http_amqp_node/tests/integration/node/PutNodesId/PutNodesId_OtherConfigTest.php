<?php


use Lcas\Test\Constraint\NodeConstraint;

require_once __DIR__ .'/../AbstractNodeTestCase.php';

class PutNodesId_OtherConfigTest extends AbstractNodeTestCase {

    /**
     * @test
     * @dataProvider availableConfigs
     */
    public function ノード設定変更_その他_正常系_一般_設定変更_初回($config) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後のNode Inclusionが取り込まれるまで待つため、
        //一定時間sleepする
        sleep(4);

        //ノードの情報を取得する
        $nodeData = $this->nodeFactory->create(200);

        //ノードの情報を更新する
        $nodeData['configurations'] = [];
        $nodeData['configurations'][] = $config;
        $updateResponse = $this->nodeUtil->updateNode($nodeData['node_id'], $nodeData);
        $this->assertEquals(204, $updateResponse->getStatusCode(), 'ステータスコードが204であること');

        //設定内容が変化していることを確認する
        $updatedNodeResponse = $this->nodeUtil->getNodeById($nodeData['node_id']);
        $updatedNode = json_decode($updatedNodeResponse->getBody(), true);

        $nodeConstraint = new NodeConstraint($nodeData);
        $nodeConstraint->setGatewayId($gatewayId);
        $this->assertThat($updatedNode, $nodeConstraint, '想定されるノードの情報を含んでいること');

    }

    public function availableConfigs() {
        return [
            'power_col_period' => [
                [
                    'type' => 'unsigned int',
                    'parameter' => 'power_col_period',
                    'value' => 30
                ]
            ],
            'energy_consumption_col_period' => [
                [
                    'type' => 'unsigned int',
                    'parameter' => 'energy_consumption_col_period',
                    'value' => 60
                ]
            ],
            'gas_consumption_col_period' => [
                [
                    'type' => 'unsigned int',
                    'parameter' => 'gas_consumption_col_period',
                    'value' => 60
                ]
            ],
            'water_consumption_col_period' => [
                [
                    'type' => 'unsigned int',
                    'parameter' => 'water_consumption_col_period',
                    'value' => 60
                ]
            ],
            'body_motion_col_period' => [
                [
                    'type' => 'unsigned int',
                    'parameter' => 'body_motion_col_period',
                    'value' => 60
                ]
            ],
        ];
    }


    /**
     * @test
     */
    public function ノード設定変更_その他_正常系_一般_設定変更_2回目() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後のNode Inclusionが取り込まれるまで待つため、
        //一定時間sleepする
        sleep(4);

        //ノードの情報を取得する
        $nodeData = $this->nodeFactory->create(200);

        //ノードの情報を更新する
        $temporaryNodeData = $nodeData;
        $temporaryNodeData['configurations'] = [];
        $temporaryNodeData['configurations'][] = [
            'type' => 'unsigned int',
            'parameter' => 'power_col_period',
            'value' => 30
        ];
        $this->nodeUtil->updateNode($temporaryNodeData['node_id'], $temporaryNodeData);


        //2回目の設定変更を行う。
        //1回目とは異なるデータで更新する。
        $nodeData['configurations'] = [];
        $nodeData['configurations'][] = [
            'type' => 'unsigned int',
            'parameter' => 'power_col_period',
            'value' => 180
        ];

        $updateResponse = $this->nodeUtil->updateNode($nodeData['node_id'], $nodeData);
        $this->assertEquals(204, $updateResponse->getStatusCode(), 'ステータスコードが204であること');

        //設定内容が(1回目の更新に対する追加ではなく)変化していることを確認する
        $updatedNodeResponse = $this->nodeUtil->getNodeById($nodeData['node_id']);
        $updatedNode = json_decode($updatedNodeResponse->getBody(), true);

        $nodeConstraint = new NodeConstraint($nodeData);
        $nodeConstraint->setGatewayId($gatewayId);
        $this->assertThat($updatedNode, $nodeConstraint, '想定されるノードの情報を含んでいること');

    }


    /**
     * @test
     */
    public function ノード設定変更_その他_正常系_一般_設定変更_一部ペンディングの応答を含む() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後のNode Inclusionが取り込まれるまで待つため、
        //一定時間sleepする
        sleep(4);

        //ノードの情報を取得する
        $nodeData = $this->nodeFactory->create(200);

        //AMQPメッセージの応答がpendingで返るように設定する。
        $this->amqpClient->requestResultCode(300);

        //ノードの情報を更新する
        $temporaryNodeData = $nodeData;
        $temporaryNodeData['configurations'] = [];
        $temporaryNodeData['configurations'][] = [
            'type' => 'unsigned int',
            'parameter' => 'power_col_period',
            'value' => 30
        ];
        $this->nodeUtil->updateNode($temporaryNodeData['node_id'], $temporaryNodeData);

        $updateResponse = $this->nodeUtil->updateNode($nodeData['node_id'], $nodeData);
        $this->assertEquals(204, $updateResponse->getStatusCode(), 'ステータスコードが204であること');

        //設定内容が(1回目の更新に対する追加ではなく)変化していることを確認する
        $updatedNodeResponse = $this->nodeUtil->getNodeById($nodeData['node_id']);
        $updatedNode = json_decode($updatedNodeResponse->getBody(), true);

        $nodeConstraint = new NodeConstraint($nodeData);
        $nodeConstraint->setGatewayId($gatewayId);
        $this->assertThat($updatedNode, $nodeConstraint, '想定されるノードの情報を含んでいること');

    }


    /**
     * @test
     * @dataProvider getDataFor_ノード設定変更_その他_異常系_異常値
     */
    public function ノード設定変更_その他_異常系_異常値($key, $value) {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //ノードの設定変更時、無効な値を渡すと400エラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId, $expectedNode, $key, $value){

            $expectedNode['configurations'] = [];
            $expectedNode['configurations'][] = [
                'type' => 'unsigned int',
                'parameter' => 'power_col_period',
                'value' => 30
            ];
            $expectedNode['configurations'][$key] = $value;

            $this->nodeUtil->updateNode($expectedNodeId, $expectedNode);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    public function getDataFor_ノード設定変更_その他_異常系_異常値() {
        return [
            'type_無効な値'       => ['type', 'invalid-string'],
            'parameter_無効な値'  => ['parameter', 'invalid-string'],
            'value_無効な値'      => ['value', 'invalid-string'],
        ];
    }

    /**
     * @test
     * @dataProvider getDataFor_ノード設定変更_その他_異常系_異常power_col_period_value値
     */
    public function ノード設定変更_その他_異常系_異常power_col_period_value値($value) {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //ノードの設定変更時、無効な値を渡すと400エラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId, $expectedNode, $value){

            $expectedNode['configurations'] = [];
            $expectedNode['configurations'][] = [
                'type' => 'unsigned int',
                'parameter' => 'power_col_period',
                'value' => $value
            ];

            $this->nodeUtil->updateNode($expectedNodeId, $expectedNode);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    public function getDataFor_ノード設定変更_その他_異常系_異常power_col_period_value値() {
        return [
            '10'       => [10],
            '100'      => [100],
            '1000'     => [1000],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_ノード設定変更_その他_異常系_異常gas_consumption_col_period_value値
     */
    public function ノード設定変更_その他_異常系_異常gas_consumption_col_period_value値($value) {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //ノードの設定変更時、無効な値を渡すと400エラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId, $expectedNode, $value){

            $expectedNode['configurations'] = [];
            $expectedNode['configurations'][] = [
                'type' => 'unsigned int',
                'parameter' => 'gas_consumption_col_period',
                'value' => $value
            ];

            $this->nodeUtil->updateNode($expectedNodeId, $expectedNode);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    public function getDataFor_ノード設定変更_その他_異常系_異常gas_consumption_col_period_value値() {
        return [
            '10'       => [10],
            '100'      => [100],
            '1000'     => [1000],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_ノード設定変更_その他_異常系_異常water_consumption_col_period_value値
     */
    public function ノード設定変更_その他_異常系_異常water_consumption_col_period_value値($value) {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //ノードの設定変更時、無効な値を渡すと400エラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId, $expectedNode, $value){

            $expectedNode['configurations'] = [];
            $expectedNode['configurations'][] = [
                'type' => 'unsigned int',
                'parameter' => 'water_consumption_col_period',
                'value' => $value
            ];

            $this->nodeUtil->updateNode($expectedNodeId, $expectedNode);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    public function getDataFor_ノード設定変更_その他_異常系_異常water_consumption_col_period_value値() {
        return [
            '10'       => [10],
            '100'      => [100],
            '1000'     => [1000],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_ノード設定変更_その他_異常系_異常body_motion_col_period_value値
     */
    public function ノード設定変更_その他_異常系_異常body_motion_col_period_value値($value) {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //ノードの設定変更時、無効な値を渡すと400エラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId, $expectedNode, $value){

            $expectedNode['configurations'] = [];
            $expectedNode['configurations'][] = [
                'type' => 'unsigned int',
                'parameter' => 'body_motion_col_period',
                'value' => $value
            ];

            $this->nodeUtil->updateNode($expectedNodeId, $expectedNode);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }

    public function getDataFor_ノード設定変更_その他_異常系_異常body_motion_col_period_value値() {
        return [
            '10'       => [10],
            '100'      => [100],
            '1000'     => [1000],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_ノード設定変更_その他_異常系_必須項目省略
     */
    public function ノード設定変更_その他_異常系_必須項目省略($key) {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //ノードの設定変更時、必須項目を省略するとエラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId, $expectedNode, $key){

            $expectedNode['configurations'] = [];
            $expectedNode['configurations'][] = [
                'type' => 'unsigned int',
                'parameter' => 'power_col_period',
                'value' => 30
            ];
            $expectedNode['configurations'][$key] = null;
            $this->nodeUtil->updateNode($expectedNodeId, $expectedNode);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    public function getDataFor_ノード設定変更_その他_異常系_必須項目省略() {
        return [
            'type'      => ['type'],
            'parameter' => ['parameter'],
            'value'     => ['value'],
        ];
    }

    /**
     * @test
     */
    public function ノード設定変更_その他_AMQP_AMQPメッセージにてNG応答あり() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //AMQP APIの応答がNG応答となるように設定する
        $this->amqpClient->requestForceNgEnabled(true);

        //AMQP APIでNG応答があった場合、400エラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId, $expectedNode){

            $expectedNode['configurations'] = [];
            $expectedNode['configurations'][] = [
                'type' => 'unsigned int',
                'parameter' => 'power_col_period',
                'value' => 30
            ];
            $this->nodeUtil->updateNode($expectedNodeId, $expectedNode);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @group slow
     */
    public function ノード設定変更_その他_AMQP_AMQPメッセージタイムアウト() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //AMQP APIの応答がタイムアウトするように設定する
        $this->amqpClient->requestTimeout(true);

        //AMQP APIで応答のタイムアウトがあった場合、504エラーが発生することを確認する。
        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId, $expectedNode){

            $expectedNode['configurations'] = [];
            $expectedNode['configurations'][] = [
                'type' => 'unsigned int',
                'parameter' => 'power_col_period',
                'value' => 30
            ];
            $this->nodeUtil->updateNode($expectedNodeId, $expectedNode);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @group slow
     */
    public function ノード設定変更_その他_AMQP_AMQPメッセージタイムアウト後_OK応答あり() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //AMQP APIの応答が、HTTP APIのタイムアウト後に行われるように設定する。
        $this->amqpClient->requestSleep(11);

        //AMQP APIで応答のタイムアウトがあった場合、504エラーが発生することを確認する。
        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId, $expectedNode){

            $expectedNode['configurations'] = [];
            $expectedNode['configurations'][] = [
                'type' => 'unsigned int',
                'parameter' => 'power_col_period',
                'value' => 30
            ];
            $this->nodeUtil->updateNode($expectedNodeId, $expectedNode);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

        //HTTP APIのタイムアウト後、AMQP APIの要求が取り込まれるまでsleepする
        sleep(3);

        //設定変更が反映されていることを確認する。
        $nodeDetailResponse = $this->nodeUtil->getNodeById($expectedNodeId);
        $nodeDetail = json_decode($nodeDetailResponse->getBody(), true);

        $nodeConstraint = new NodeConstraint($expectedNode);
        $nodeConstraint->setGatewayId($gatewayId);
        $this->assertThat($nodeDetail, $nodeConstraint, '設定変更が反映されていること');
    }


    /**
     * @test
     * @group slow
     */
    public function ノード設定変更_その他_AMQP_AMQPメッセージタイムアウト後_NG応答あり() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayData = $this->gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $expectedNode = $gatewayData['nodes'][1];
        $expectedNodeId = $expectedNode['node_id'];

        //AMQP APIのNG応答が、HTTP APIのタイムアウト後に行われるように設定する。
        $this->amqpClient->requestSleep(11);
        $this->amqpClient->requestForceNgEnabled(true);

        //AMQP APIで応答のタイムアウトがあった場合、504エラーが発生することを確認する。
        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($expectedNodeId, $expectedNode){

            $expectedNode['configurations'] = [];
            $expectedNode['configurations'][] = [
                'type' => 'unsigned int',
                'parameter' => 'power_col_period',
                'value' => 30
            ];
            $this->nodeUtil->updateNode($expectedNodeId, $expectedNode);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


}
