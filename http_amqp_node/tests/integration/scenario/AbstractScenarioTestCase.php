<?php


use Lcas\DB\DB;
use Lcas\Repository\ScenarioRepository;
use Lcas\Test\Fixture\ScenarioFixtureFactory;
use Lcas\Test\Util\TestUtil;

class AbstractScenarioTestCase extends \Lcas\Test\IntegrationTestCase {


    /**
     * @var ScenarioRepository
     */
    protected $scenarioRepository;

    /**
     * @var ScenarioFixtureFactory
     */
    protected $scenarioFactory;

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->scenarioRepository = new ScenarioRepository();
        $this->scenarioFactory = new ScenarioFixtureFactory();
    }


    public function setUp() {
        parent::setUp();

        //テストデータを全削除
        TestUtil::clearDatabase();

        //デフォルトのユーザーを登録する
        $this->userUtil->registerDefaultUser();
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

    }


    protected function getScenarios($label=null) {

        $queries = [];
        if(!is_null($label)) {
            $queries['label'] = $label;
        }

        return $this->httpClient->get('/api/v3/scenarios', ['query' => $queries]);
    }

    protected function getScenarioById($id) {
        return $this->httpClient->get('/api/v3/scenarios/' . $id);
    }


    protected function updateScenarioLabels($id, $labels) {
        //ラベルのみを更新するAPIは存在しないため、直接テーブルを更新する。
        $this->scenarioRepository->updateLabels($id, $labels);
    }


    protected function registerScenario($scenarioData) {

        return $this->httpClient->post('/api/v3/scenarios', [
            'body' => json_encode($scenarioData)
        ]);

    }

    protected function updateScenario($id, $scenarioData) {

        return $this->httpClient->put('/api/v3/scenarios/' . $id, [
            'body' => json_encode($scenarioData)
        ]);

    }

    protected function deleteScenario($id) {
        return $this->httpClient->delete('/api/v3/scenarios/' . $id);
    }


    protected function getScenarioEvents($options=[]) {
        return $this->httpClient->get('/api/v3/scenarios/all/events', ['query' => $options]);
    }

    protected function getScenarioEventsById($scenarioId, $options=[]) {
        return $this->httpClient->get("/api/v3/scenarios/{$scenarioId}/events", ['query' => $options]);
    }

    protected function forceRegisterScenarioEvent($scenarioId, $message, $timeStamp) {
        $db = \Lcas\DB\DB::getMasterDb();

        $sql = "INSERT INTO scenario_events SET scenario_id={$scenarioId}, message='{$message}', "
             . " created='{$timeStamp}'";

        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception('SQLエラー: ' . $db->getErr() . ', ' . $sql);
        }
    }

    protected function findAutoRegisteredScenarioId($userId) {
        $sql = "SELECT id FROM scenarios WHERE user_id={$userId} AND auto_config=1";

        $db = DB::getMasterDb();
        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception("SQLエラー： {$db->getErr()}, SQL: {$sql}");
        }
        $result = $db->Fetch();

        if(!$result) {
            throw new \Exception('自動登録されたシナリオが見つかりません。user_id: ' . $userId);
        }

        return $result['id'];
    }


}
