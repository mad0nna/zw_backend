<?php


use Lcas\Test\Constraint\Scenario\SimpleScenarioConstraint;

require_once __DIR__ . '/AbstractScenarioTestCase.php';


class DeleteScenariosIdTest extends AbstractScenarioTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setUp();
    }

    /**
     * @test
     */
    public function シナリオの削除_正常系_自身のシナリオを削除() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオを削除する。
        $deleteResponse = $this->deleteScenario($scenarioId);
        $this->assertEquals(204, $deleteResponse->getStatusCode(), 'ステータスコードが204であること');

        //シナリオが削除され、取得できなくなっていること
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use ($scenarioId){

            $this->getScenarioById($scenarioId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }



    /**
     * @test
     */
    public function シナリオの削除_異常系_自動登録シナリオ_自動登録シナリオを削除() {
        //自動登録シナリオ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000004000';
        $this->gatewayUtil->registerGateway($macAddress);

        //Gateway割当直後のNode Inclusionが取り込まれ
        //シナリオの自動登録が行われるのを待つため、一定時間sleepする
        sleep(3);

        //自動登録シナリオのIDを取得
        $userId = $this->userUtil->getCurrentUserId();
        $autoRegisteredScenarioId = $this->findAutoRegisteredScenarioId($userId);

        //自動登録シナリオを更新するとエラーが発生することを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($autoRegisteredScenarioId){

            $this->deleteScenario($autoRegisteredScenarioId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function シナリオの削除_異常系_一般_他ユーザーのシナリオを削除() {
        //2人目のユーザーを登録してログインする
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

        //シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //デフォルトのユーザーでログインし直す
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($scenarioId){

            $this->deleteScenario($scenarioId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     */
    public function シナリオの削除_異常系_一般_存在しないシナリオを削除() {
        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() {

            $this->deleteScenario(1);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     */
    public function シナリオの削除_異常系_AMQP_AMQPメッセージにてNG応答あり() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //AMQP APIがNG応答を返すように設定する
        $this->amqpClient->requestForceNgEnabled(true);

        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use ($scenarioId){

            $this->deleteScenario($scenarioId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @group slow
     */
    public function シナリオの削除_異常系_AMQP_AMQPメッセージタイムアウト() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //AMQP APIがタイムアウトするように設定する。
        $this->amqpClient->requestTimeout(true);

        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use ($scenarioId){

            $this->deleteScenario($scenarioId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @group slow
     */
    public function シナリオの削除_異常系_AMQP_AMQPメッセージタイムアウト後OK応答あり() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //HTTP APIのタイムアウト後にOK応答が行われるように設定する
        $this->amqpClient->requestSleep(11);

        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use ($scenarioId){

            $this->deleteScenario($scenarioId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

        //HTTP APIのタイムアウト後、OK応答が行われるまでsleepする
        sleep(4);

        //シナリオが削除され参照できないこと
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use ($scenarioId){

            $this->getScenarioById($scenarioId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @group slow
     */
    public function シナリオの削除_異常系_AMQP_AMQPメッセージタイムアウト後NG応答あり() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //HTTP APIのタイムアウト後にOK応答が行われるように設定する
        $this->amqpClient->requestSleep(11);
        $this->amqpClient->requestForceNgEnabled(true);

        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use ($scenarioId){

            $this->deleteScenario($scenarioId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

        //HTTP APIのタイムアウト後、OK応答が行われるまでsleepする
        sleep(4);

        //シナリオが削除されず参照できること
        $response = $this->getScenarioById($scenarioId);
        $responseJson = json_decode($response->getBody(), true);

        //レスポンスの内容が正しいこと
        $simpleScenarioConstraint = new SimpleScenarioConstraint($registeredScenario);
        $this->assertThat($responseJson, $simpleScenarioConstraint, "シナリオの内容が想定通りであること");
    }


}
