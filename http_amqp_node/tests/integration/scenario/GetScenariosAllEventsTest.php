<?php


use Lcas\Test\Constraint\Scenario\ScenarioEventConstraint;

require_once __DIR__ . '/AbstractScenarioTestCase.php';


class GetScenariosAllEventsTest extends AbstractScenarioTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setUp();
    }

    /**
     * @test
     */
    public function シナリオイベント一覧の参照_正常系_一般_全件取得() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        $registeredScenario2 = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse2 = $this->registerScenario($registeredScenario2);
        $registerResponseJson2 = json_decode($registerResponse2->getBody(), true);
        $scenarioId2 = $registerResponseJson2['id'];

        //シナリオに対するイベントを登録する
        $this->forceRegisterScenarioEvent($scenarioId,  'message1', '2016-04-01 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId2, 'message1', '2016-04-02 00:00:00');
        $this->forceRegisterScenarioEvent($scenarioId,  'message1', '2016-04-02 09:59:59');
        $this->forceRegisterScenarioEvent($scenarioId2, 'message1', '2016-04-02 10:00:00');
        $this->forceRegisterScenarioEvent($scenarioId,  'message1', '2016-04-02 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId2, 'message1', '2016-04-03 00:00:00');

        //時間の降順で返却されることを確認する
        $expectedScenarioEvents = [
            ['id' => $scenarioId2, 'timestamp' => strtotime('2016-04-03 00:00:00') * 1000],
            ['id' => $scenarioId,  'timestamp' => strtotime('2016-04-02 23:59:59') * 1000],
            ['id' => $scenarioId2, 'timestamp' => strtotime('2016-04-02 10:00:00') * 1000],
            ['id' => $scenarioId,  'timestamp' => strtotime('2016-04-02 09:59:59') * 1000],
            ['id' => $scenarioId2, 'timestamp' => strtotime('2016-04-02 00:00:00') * 1000],
            ['id' => $scenarioId,  'timestamp' => strtotime('2016-04-01 23:59:59') * 1000],
        ];

        //イベントの一覧を取得する
        $scenarioEventResponse = $this->getScenarioEvents();
        $this->assertEquals(200, $scenarioEventResponse->getStatusCode(), 'ステータスコードが200であること');
        $scenarioEvents = json_decode($scenarioEventResponse->getBody(), true);

        foreach($expectedScenarioEvents as $idx=>$expectedEvent) {
            $scenarioEventConstraint = new ScenarioEventConstraint($expectedEvent);
            $this->assertThat($scenarioEvents[$idx], $scenarioEventConstraint, "想定されるイベント情報を含んでいること($idx)");
        }
    }


    /**
     * @test
     */
    public function シナリオイベント一覧の参照_正常系_一般_start指定あり() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオに対するイベントを登録する
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-01 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 00:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 09:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 10:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-03 00:00:00');

        //startを指定してイベントの一覧を取得する
        $scenarioEventResponse = $this->getScenarioEvents([
            'start' => strtotime('2016-04-02 00:00:00') * 1000
        ]);
        $this->assertEquals(200, $scenarioEventResponse->getStatusCode(), 'ステータスコードが200であること');
        $scenarioEvents = json_decode($scenarioEventResponse->getBody(), true);

        //期間指定した結果が反映されていることを確認する
        $expectedScenarioEvents = [
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-03 00:00:00') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 23:59:59') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 10:00:00') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 09:59:59') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 00:00:00') * 1000],
        ];

        foreach($expectedScenarioEvents as $idx=>$expectedEvent) {
            $scenarioEventConstraint = new ScenarioEventConstraint($expectedEvent);
            $this->assertThat($scenarioEvents[$idx], $scenarioEventConstraint, "想定されるイベント情報を含んでいること($idx)");
        }
    }


    /**
     * @test
     */
    public function シナリオイベント一覧の参照_正常系_一般_end指定あり() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオに対するイベントを登録する
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-01 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 00:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 09:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 10:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-03 00:00:00');

        //endを指定してイベントの一覧を取得する
        $scenarioEventResponse = $this->getScenarioEvents([
            'end' => strtotime('2016-04-02 23:59:59') * 1000
        ]);
        $this->assertEquals(200, $scenarioEventResponse->getStatusCode(), 'ステータスコードが200であること');
        $scenarioEvents = json_decode($scenarioEventResponse->getBody(), true);

        //期間指定した結果が反映されていることを確認する
        $expectedScenarioEvents = [
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 23:59:59') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 10:00:00') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 09:59:59') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 00:00:00') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-01 23:59:59') * 1000],
        ];

        foreach($expectedScenarioEvents as $idx=>$expectedEvent) {
            $scenarioEventConstraint = new ScenarioEventConstraint($expectedEvent);
            $this->assertThat($scenarioEvents[$idx], $scenarioEventConstraint, "想定されるイベント情報を含んでいること($idx)");
        }
    }


    /**
     * @test
     */
    public function シナリオイベント一覧の参照_正常系_一般_limit指定あり() {
        
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオに対するイベントを登録する
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-01 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 00:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 09:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 10:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-03 00:00:00');

        //limitを指定してイベントの一覧を取得する
        $scenarioEventResponse = $this->getScenarioEvents([
            'limit' => 3
        ]);
        $this->assertEquals(200, $scenarioEventResponse->getStatusCode(), 'ステータスコードが200であること');
        $scenarioEvents = json_decode($scenarioEventResponse->getBody(), true);

        //期間指定した結果が反映されていることを確認する
        $expectedScenarioEvents = [
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-03 00:00:00') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 23:59:59') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 10:00:00') * 1000],
        ];

        foreach($expectedScenarioEvents as $idx=>$expectedEvent) {
            $scenarioEventConstraint = new ScenarioEventConstraint($expectedEvent);
            $this->assertThat($scenarioEvents[$idx], $scenarioEventConstraint, "想定されるイベント情報を含んでいること($idx)");
        }
    }


    /**
     * @test
     */
    public function シナリオイベント一覧の参照_正常系_一般_offset指定あり() {
        
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオに対するイベントを登録する
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-01 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 00:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 09:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 10:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-03 00:00:00');

        //offsetを指定してイベントの一覧を取得する
        $scenarioEventResponse = $this->getScenarioEvents([
            'offset' => 3
        ]);
        $this->assertEquals(200, $scenarioEventResponse->getStatusCode(), 'ステータスコードが200であること');
        $scenarioEvents = json_decode($scenarioEventResponse->getBody(), true);

        //期間指定した結果が反映されていることを確認する
        $expectedScenarioEvents = [
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 09:59:59') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 00:00:00') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-01 23:59:59') * 1000],
        ];

        foreach($expectedScenarioEvents as $idx=>$expectedEvent) {
            $scenarioEventConstraint = new ScenarioEventConstraint($expectedEvent);
            $this->assertThat($scenarioEvents[$idx], $scenarioEventConstraint, "想定されるイベント情報を含んでいること($idx)");
        }
    }


    /**
     * @test
     */
    public function シナリオイベント一覧の参照_正常系_一般_start_end_limit_offset指定あり() {
        
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオに対するイベントを登録する
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-01 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 00:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 09:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 10:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-03 00:00:00');

        //offsetを指定してイベントの一覧を取得する
        $scenarioEventResponse = $this->getScenarioEvents([
            'start'  => strtotime('2016-04-02 00:00:00') * 1000,
            'end'    => strtotime('2016-04-02 23:59:59') * 1000,
            'limit'  => 2,
            'offset' => 1
        ]);
        $this->assertEquals(200, $scenarioEventResponse->getStatusCode(), 'ステータスコードが200であること');
        $scenarioEvents = json_decode($scenarioEventResponse->getBody(), true);

        //期間指定した結果が反映されていることを確認する
        $expectedScenarioEvents = [
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 10:00:00') * 1000],
            ['id' => $scenarioId, 'timestamp' => strtotime('2016-04-02 09:59:59') * 1000],
        ];

        foreach($expectedScenarioEvents as $idx=>$expectedEvent) {
            $scenarioEventConstraint = new ScenarioEventConstraint($expectedEvent);
            $this->assertThat($scenarioEvents[$idx], $scenarioEventConstraint, "想定されるイベント情報を含んでいること($idx)");
        }
    }


    /**
     * @test
     */
    public function シナリオイベント一覧の参照_正常系_自動登録シナリオ_自動登録シナリオのデータを参照() {
        //自動登録シナリオ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000004000';
        $this->gatewayUtil->registerGateway($macAddress);

        //Gateway割当直後のNode Inclusionが取り込まれ
        //シナリオの自動登録が行われるのを待つため、一定時間sleepする
        sleep(4);

        $userId = $this->userUtil->getCurrentUserId();
        $autoRegisteredScenarioId = $this->findAutoRegisteredScenarioId($userId);

        $this->forceRegisterScenarioEvent($autoRegisteredScenarioId, 'message1', '2016-04-01 23:59:59');

        $scenarioEventResponse = $this->getScenarioEvents();
        $this->assertEquals(200, $scenarioEventResponse->getStatusCode(), 'ステータスコードが200であること');
        $scenarioEvents = json_decode($scenarioEventResponse->getBody(), true);

        //自動登録シナリオのIDが含まれていること
        $this->assertEquals($autoRegisteredScenarioId, $scenarioEvents[0]['id'], '自動登録シナリオのIDが含まれていること');
    }



    /**
     * @test
     * @dataProvider getDataFor_シナリオイベント一覧の参照_異常系_異常値
     */
    public function シナリオイベント一覧の参照_異常系_異常値($key, $value) {
        
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオに対するイベントを登録する
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-01 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 00:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 09:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 10:00:00');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-02 23:59:59');
        $this->forceRegisterScenarioEvent($scenarioId, 'message1', '2016-04-03 00:00:00');

        //無効な値を指定するとエラーが発生することを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($key, $value){

            $this->getScenarioEvents([
                $key  => $value,
            ]);

        }, $expectedStatusCode, '');
    }


    public function getDataFor_シナリオイベント一覧の参照_異常系_異常値() {
        return [
            'startに無効な値'  => ['start', 'invalid-string'],
            'endに無効な値'    => ['end', 'invalid-string'],
            'limitに無効な値'  => ['limit', 'invalid-string'],
            'offsetに無効な値' => ['offset', 'invalid-string'],
        ];
    }

}
