<?php


use Lcas\Test\Constraint\GatewayConstraint;
use Lcas\Test\Constraint\Scenario\SimpleScenarioConstraint;

require_once __DIR__ . '/AbstractScenarioTestCase.php';


class GetScenariosIdTest extends AbstractScenarioTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setUp();
    }

    /**
     * @test
     */
    public function 指定シナリオの参照_正常系_自身のシナリオの参照() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($expectedScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオ一覧を取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $responseJson = json_decode($response->getBody(), true);

        $simpleScenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($responseJson, $simpleScenarioConstraint, "シナリオの内容が想定通りであること");
    }


    /**
     * @test
     */
    public function 指定シナリオの参照_異常系_他ユーザーのシナリオを参照() {
        //2人目のユーザーを登録してログインする
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

        //シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //デフォルトのユーザーでログインし直す
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($scenarioId){

            $this->getScenarioById($scenarioId);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     */
    public function 指定シナリオの参照_異常系_存在しないシナリオを参照() {

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function(){

            $this->getScenarioById(1);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     */
    public function 指定シナリオの参照_異常系_自動登録シナリオ_自動登録されたシナリオを参照() {
        //自動登録シナリオ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000004000';
        $this->gatewayUtil->registerGateway($macAddress);

        //Gateway割当直後のNode Inclusionが取り込まれ
        //シナリオの自動登録が行われるのを待つため、一定時間sleepする
        sleep(4);


        $userId = $this->userUtil->getCurrentUserId();
        $autoRegisteredScenarioId = $this->findAutoRegisteredScenarioId($userId);

        //自動登録されたシナリオを参照できることを確認する。
        $response = $this->getScenarioById($autoRegisteredScenarioId);

	//レスポンスの内容が正しいこと
        $responseJson = json_decode($response->getBody(), true);

	$this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');
    }


}
