<?php


use Lcas\Test\Constraint\GatewayConstraint;
use Lcas\Test\Constraint\Scenario\SimpleScenarioConstraint;

require_once __DIR__ . '/AbstractScenarioTestCase.php';


class GetScenariosTest extends AbstractScenarioTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setUp();
    }

    /**
     * @test
     */
    public function シナリオ一覧の参照_正常系_シナリオ一覧の参照() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $expectedScenarios = [];
        $expectedScenarios[] = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $expectedScenarios[] = $this->scenarioFactory->create('second-scenario');
        $this->registerScenario($expectedScenarios[0]);
        $this->registerScenario($expectedScenarios[1]);

        //シナリオ一覧を取得する
        $response = $this->getScenarios();

        //レスポンスの内容が正しいこと
        $responseJson = json_decode($response->getBody(), true);

        foreach($expectedScenarios as $idx=>$expectedScenario) {
            $simpleScenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
            $this->assertThat($responseJson[$idx], $simpleScenarioConstraint, "シナリオの内容が想定通りであること({$idx})");
        }
    }


    /**
     * @test
     */
    public function シナリオ一覧の参照_正常系_ラベル指定してシナリオを参照() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $dummyScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $expectedScenario = $this->scenarioFactory->create('second-scenario');
        $this->registerScenario($dummyScenario);
        $this->registerScenario($expectedScenario);


        //ラベルを指定してシナリオ一覧を取得する
        $response = $this->getScenarios('scenario2');

        //ラベルが部分一致したシナリオのみ返されることを確認する。
        $responseJson = json_decode($response->getBody(), true);

        $simpleScenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($responseJson[0], $simpleScenarioConstraint, "シナリオの内容が想定通りであること");

        $this->assertCount(1, $responseJson, 'シナリオ一覧の件数が想定どおりであること');
    }


    /**
     * @test
     */
    public function シナリオ一覧の参照_異常系_自動登録シナリオ_自動登録されたシナリオを含む一覧の参照() {
        //自動登録シナリオ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000004000';
        $this->gatewayUtil->registerGateway($macAddress);

        //Gateway割当直後のNode Inclusionが取り込まれ
        //シナリオの自動登録が行われるのを待つため、一定時間sleepする
        sleep(4);

        //自動登録シナリオがDBには登録されていることを確認する。
        $userId = $this->userUtil->getCurrentUserId();
        $this->findAutoRegisteredScenarioId($userId);

        //HTTP APIのレスポンスには、自動登録シナリオが含まれていないことを確認する。
        $scenarioListResponse = $this->getScenarios();
        $scenarioList = json_decode($scenarioListResponse->getBody(), true);
        $this->assertCount(0, $scenarioList, '自動登録シナリオが含まれていないこと');
    }

    protected function sendNodeInclusionNotification($gatewayMacAddress, $nodeData) {
        $user = $this->userUtil->getCurrentUser();

        $request = [
            'command' => 'node_incl',
            'result_code' => 200,
            'username' => $user['login_id'],
            'gw_name' => $gatewayMacAddress,
            'node_id' => $nodeData['node_id'],
            'manufacturer' => $nodeData['manufacturer'],
            'product' => $nodeData['product'],
            'serial_no' => $nodeData['serial_no'],
            'actions' => $nodeData['actions'],
            'devices' => $nodeData['devices'],
        ];
        $request = array_merge($request, $nodeData);
        $this->amqpClient->sendMessageToLcas($request);

    }

}
