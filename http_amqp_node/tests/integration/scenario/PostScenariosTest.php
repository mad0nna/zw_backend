<?php


use Lcas\Test\Constraint\Scenario\SimpleScenarioConstraint;

require_once __DIR__ . '/AbstractScenarioTestCase.php';


class PostScenariosTest extends AbstractScenarioTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setUp();
    }

    /**
     * @test
     */
    public function シナリオの登録_正常系_scenario_type_simple() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $expectedScenario['scenario_type'] = 'simple';
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        //POST scenariosのレスポンスのフォーマットが正しいことを確認する
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];
        $this->assertInternalType('integer', $registerResponseJson['id'], 'レスポンスのフォーマットが正しいこと');

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_正常系_condition_operator
     */
    public function シナリオの登録_正常系_condition_operator($operator) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $expectedScenario['condition_operator'] = $operator;
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    public function getDataFor_シナリオの登録_正常系_condition_operator() {
        return [
            'OR'  => ['OR'],
            'AND' => ['AND'],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_正常系_operator
     */
    public function シナリオの登録_正常系_operator($operator) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $expectedScenario['conditions'][0]['operator'] = $operator;
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    public function getDataFor_シナリオの登録_正常系_operator() {
        return [
            '<'    => ['<'],
            '=='   => ['=='],
            '>'    => ['>'],
//            'freq' => ['freq'],
        ];
    }

    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_正常系_operator_freq
     */
    public function シナリオの登録_正常系_operator_freq($rhs) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $expectedScenario['conditions'][0]['operator'] = 'freq';
        $expectedScenario['conditions'][0]['rhs'] = $rhs;
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    public function getDataFor_シナリオの登録_正常系_operator_freq() {
        return [
            'almost_none'    => ['almost_none'],
            'high'   => ['high'],
            'med'    => ['med'],
            'low' => ['low'],
            '1-999' => ['1-999'],
        ];
    }



    /**
     * @test
     */
    public function シナリオの登録_正常系_actions_send_message() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $expectedScenario['actions'][0] = [
            'type' => 'send_message',
            'content' => 'type = send_messageのテスト'
        ];
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_正常系_enabled
     */
    public function シナリオの登録_正常系_enabled($enabled) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $expectedScenario['enabled'] = $enabled;
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    public function getDataFor_シナリオの登録_正常系_enabled() {
        return [
            'true'  => [true],
            'false' => [false],
        ];
    }


    /**
     * @test
     */
    public function シナリオの登録_正常系_enabled_period() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $expectedScenario['enabled_period'] = '10:00-18:00';
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    /**
     * @test
     */
    public function シナリオの登録_正常系_enabled_days() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $expectedScenario['enabled_days'] = [
            'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'
        ];
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_異常系_必須項目省略
     */
    public function シナリオの登録_異常系_必須項目省略($key) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        unset($scenario[$key]);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenario){

            $this->registerScenario($scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    public function getDataFor_シナリオの登録_異常系_必須項目省略() {
        return [
            'scenario_type'      => ['scenario_type'],
            'labels'             => ['labels'],
            'condition_operator' => ['condition_operator'],
            'conditions'         => ['conditions'],
            'trigger_after'      => ['trigger_after'],
            'actions'            => ['actions'],
            'enabled'            => ['enabled'],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_異常系_必須項目省略_conditions
     */
    public function シナリオの登録_異常系_必須項目省略_conditions($key) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        unset($scenario['conditions'][0][$key]);

        //"unit"の省略を確認するため、"operator"は"=="などunitが必須なもので固定する。
        if($key != 'operator') {
            $scenario['conditions'][0]['operator'] = '==';
        }

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenario){

            $this->registerScenario($scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    public function getDataFor_シナリオの登録_異常系_必須項目省略_conditions() {
        return [
            'lhs'      => ['lhs'],
            'operator' => ['operator'],
            'rhs'      => ['rhs'],
        ];
    }

    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_異常系_必須項目省略_actions
     */
    public function シナリオの登録_異常系_必須項目省略_actions($action) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $scenario['actions'][0] = $action;

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenario){

            $this->registerScenario($scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    /**
     * @test
     */
    public function getDataFor_シナリオの登録_異常系_必須項目省略_actions() {
        return [
            'type省略' => [ [] ],
            'content省略' => [ ['type' => 'send_message'] ],  //type=send_messageの時、contentは省略不可
        ];

    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_異常系_異常値
     */
    public function シナリオの登録_異常系_異常値($key, $value) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $scenario[$key] = $value;

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenario){

            $this->registerScenario($scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    public function getDataFor_シナリオの登録_異常系_異常値() {
        return [
            'scenario_type'      => ['scenario_type', 'invalid_string' ],
            'labels'             => ['labels', 'invalid_string'],
            'condition_operator' => ['condition_operator', 'invalid_string'],
            'conditions'         => ['conditions', 'invalid_string'],
            'trigger_after'      => ['trigger_after', 'invalid_string'],
            'actions'            => ['actions', 'invalid_string'],
            'enabled'            => ['enabled', 'invalid_string'],
            'enabled_period'     => ['enabled_period', 'invalid_string'],
            'enabled_days_無効'  => ['enabled_days', 'invalid_string'],
            'enabled_days_重複'  => ['enabled_days', ['sun', 'mon', 'sun']],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_異常系_異常値_conditions
     */
    public function シナリオの登録_異常系_異常値_conditions($key, $value) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $scenario['conditions'][0][$key] = $value;

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenario){

            $this->registerScenario($scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    public function getDataFor_シナリオの登録_異常系_異常値_conditions() {
        return [
            'operator'            => ['operator', 'invalid_string'],
            'rhs(operator=equal)' => ['rhs', 'invalid_string'],
            'rhs(operator=freq)'  => ['rhs', 'invalid_string'],
        ];
    }


    /**
     * @test
     */
    public function シナリオの登録_異常系_AMQP_AMQPメッセージにてNG応答あり() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);

        //AMQP APIでNG応答が返るよう設定する。
        $this->amqpClient->requestForceNgEnabled(true);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenario){

            $this->registerScenario($scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     * @group slow
     */
    public function シナリオの登録_異常系_AMQP_AMQPメッセージタイムアウト() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);

        //AMQP APIでタイムアウトが発生するよう設定する。
        $this->amqpClient->requestTimeout(true);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($scenario){

            $this->registerScenario($scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     * @group slow
     */
    public function シナリオの登録_異常系_AMQP_AMQPメッセージタイムアウト後OK応答あり() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);

        //AMQP APIで遅れてOK応答が行われるよう設定する。
        $this->amqpClient->requestSleep(11);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($expectedScenario){

            $this->registerScenario($expectedScenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

        //HTTP APIのタイムアウト後、
        //OK応答が受信されるまで待機する
        sleep(4);

        //シナリオ一覧を取得する
        //HTTP APIは失敗しておりシナリオのIDはわからないため、
        //全件取得した最初の1件目を対象に判定する。
        $response = $this->getScenarios();

        //レスポンスの内容が正しいこと
        $registeredScenarios = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenarios[0], $scenarioConstraint, '想定されるシナリオ情報を含むこと');

    }

    /**
     * @test
     */
    public function シナリオの登録_異常系_AMQP_AMQPメッセージにてタイムアウト後NG応答あり() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

    	//シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);

        //AMQP APIでNG応答が返るよう設定する。
        $this->amqpClient->requestForceNgEnabled(true);
        $this->amqpClient->requestSleep(11);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($scenario){

            $this->registerScenario($scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    /**
     * @test
     */
    public function シナリオの登録_異常系_アクセス権限_共有() {
    	
    	//別ユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
    
    	//別ユーザーにゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
    	sleep(4);
    	
    	$this->nodeUtil->shareNodeWith(200, array(TEST_DEFAULT_USER_EMAIL));
    
        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
    	
    	//シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $expectedScenario['scenario_type'] = 'simple';
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        //POST scenariosのレスポンスのフォーマットが正しいことを確認する
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];
        $this->assertInternalType('integer', $registerResponseJson['id'], 'レスポンスのフォーマットが正しいこと');

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }
    
    /**
     * @test
     */
    public function シナリオの登録_異常系_アクセス権限_権限なし() {
    	
    	//別ユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
    
    	//別ユーザーにゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
    	sleep(4);
    
        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
    	
    	//シナリオを登録する。
    	$expectedStatusCode = 404;
    	$scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
    	$this->assertGeneralErrorResponse(function() use($scenario){
    	
    		$this->registerScenario($scenario);
    	
    	}, $expectedStatusCode, '有効なエラーレスポンスであること');
    }
    
}
