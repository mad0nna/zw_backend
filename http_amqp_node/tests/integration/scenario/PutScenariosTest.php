<?php


use Lcas\Test\Constraint\Scenario\SimpleScenarioConstraint;

require_once __DIR__ . '/AbstractScenarioTestCase.php';


class PutScenariosTest extends AbstractScenarioTestCase {

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * 各テスト毎に実行される処理
     */
    public function setUp() {
        parent::setUp();

        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
    }

    /**
     * @test
     */
    public function シナリオの更新_正常系_scenario_type_simple() {

        //シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($expectedScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //登録したシナリオを更新する。
        $expectedScenario['labels'][] = 'update-test';
        $updateResponse = $this->updateScenario($scenarioId, $expectedScenario);
        $this->assertEquals(204, $updateResponse->getStatusCode(), 'ステータスコードが204であること');

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの更新_正常系_condition_operator
     */
    public function シナリオの更新_正常系_condition_operator($operator) {

        //シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($expectedScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //登録したシナリオを更新する。
        $expectedScenario['condition_operator'] = $operator;
        $updateResponse = $this->updateScenario($scenarioId, $expectedScenario);
        $this->assertEquals(204, $updateResponse->getStatusCode(), 'ステータスコードが204であること');

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    public function getDataFor_シナリオの更新_正常系_condition_operator() {
        return [
            'OR'  => ['OR'],
            'AND' => ['AND'],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの更新_正常系_operator
     */
    public function シナリオの更新_正常系_operator($operator) {

        //シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($expectedScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //登録したシナリオを更新する。
        $expectedScenario['conditions'][0]['operator'] = $operator;
        $updateResponse = $this->updateScenario($scenarioId, $expectedScenario);
        $this->assertEquals(204, $updateResponse->getStatusCode(), 'ステータスコードが204であること');

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    public function getDataFor_シナリオの更新_正常系_operator() {
        return [
            '<'    => ['<'],
            '=='   => ['=='],
            '>'    => ['>'],
        ];
    }

    /**
     * @test
     * @dataProvider getDataFor_シナリオの更新_正常系_operator_freq
     */
    public function シナリオの更新_正常系_operator_freq($rhs) {

        //シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($expectedScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //登録したシナリオを更新する。
	$expectedScenario['conditions'][0]['operator'] = 'freq';
	$expectedScenario['conditions'][0]['rhs'] = $rhs;

        $updateResponse = $this->updateScenario($scenarioId, $expectedScenario);
        $this->assertEquals(204, $updateResponse->getStatusCode(), 'ステータスコードが204であること');

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    public function getDataFor_シナリオの更新_正常系_operator_freq() {
        return [
            '1-999'    => ['1-999'],
            'almost_none'   => ['almost_none'],
            'high'    => ['high'],
            'med' => ['med'],
            'low' => ['low'],
        ];
    }


    /**
     * @test
     */
    public function シナリオの更新_正常系_actions_send_message() {

        //シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($expectedScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //登録したシナリオを更新する。
        $expectedScenario['actions'][0] = [
            'type' => 'send_message',
            'content' => 'type = send_messageのテスト'
        ];
        $updateResponse = $this->updateScenario($scenarioId, $expectedScenario);
        $this->assertEquals(204, $updateResponse->getStatusCode(), 'ステータスコードが204であること');

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの更新_正常系_enabled
     */
    public function シナリオの更新_正常系_enabled($enabledFrom, $enabledTo) {
        //シナリオを登録する。
        //(enabledの値を$enabledFrom の状態で登録する)
        $registeredScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registeredScenario['enabled'] = $enabledFrom;
        $registerResponse = $this->registerScenario($registeredScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //登録したシナリオを更新する。
        //(enabledの値を$enabledTo の状態に変更する)
        $expectedScenario = $registeredScenario;
        $expectedScenario['enabled'] = $enabledTo;
        $updateResponse = $this->updateScenario($scenarioId, $expectedScenario);
        $this->assertEquals(204, $updateResponse->getStatusCode(), 'ステータスコードが204であること');

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    public function getDataFor_シナリオの更新_正常系_enabled() {
        return [
            'true  -> false'  => [true, false],
            'false -> flase'  => [false, false],
            'false -> true'   => [false, true],
            'true  -> true'   => [true, true],
        ];
    }



    /**
     * @test
     */
    public function シナリオの更新_正常系_enabled_period() {

        //シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($expectedScenario);

        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        $expectedScenario['enabled_period'] = '10:00-18:00';
        $updateResponse = $this->updateScenario($scenarioId, $expectedScenario);
        $this->assertEquals(204, $updateResponse->getStatusCode(), 'ステータスコードが204であること');

        //変更が反映されていることを確認する
        $response = $this->getScenarioById($scenarioId);
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    /**
     * @test
     */
    public function シナリオの更新_正常系_enabled_days() {

        //シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($expectedScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオを更新する。
        $expectedScenario['enabled_days'] = [
            'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'
        ];
        $updateResponse = $this->updateScenario($scenarioId, $expectedScenario);
        $this->assertEquals(204, $updateResponse->getStatusCode(), 'ステータスコードが204であること');

        //変更内容が反映されていることを確認する。
        $response = $this->getScenarioById($scenarioId);
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');
    }


    /**
     * @test
     */
    public function シナリオの更新_異常系_自動登録シナリオ_自動登録シナリオを更新() {
        //自動登録シナリオ確認用のゲートウェイを割り当てる
        $macAddress = 'A00000004000';
        $this->gatewayUtil->registerGateway($macAddress);

        //Gateway割当直後のNode Inclusionが取り込まれ
        //シナリオの自動登録が行われるのを待つため、一定時間sleepする
        sleep(4);

        //自動登録シナリオのIDを取得
        $userId = $this->userUtil->getCurrentUserId();
        $autoRegisteredScenarioId = $this->findAutoRegisteredScenarioId($userId);

        //自動登録シナリオを更新するとエラーが発生することを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use ($autoRegisteredScenarioId){

            $scenario = $this->scenarioFactory->create(TEST_AUTO_SCENARIO_BATTERY);
            $scenario['labels'][] = 'update-test';

            $this->updateScenario($autoRegisteredScenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }



    /**
     * @test
     */
    public function シナリオの更新_異常系_一般_他ユーザーのシナリオを更新() {

        //Setupで登録されたGatewayを登録解除する
    	$this->gatewayUtil->deregisterGateway($this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY));
        sleep(4);
        
    	//2人目のユーザーを登録してログインする
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);
        
        //シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //デフォルトのユーザーでログインし直す
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function() use($scenarioId, $scenario){

            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     */
    public function シナリオの更新_異常系_一般_存在しないシナリオを更新() {

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function(){

            $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
            $this->updateScenario(1, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの更新_異常系_必須項目省略
     */
    public function シナリオの更新_異常系_必須項目省略($key) {
        //シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenarioId, $scenario, $key){

            unset($scenario[$key]);

            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    public function getDataFor_シナリオの更新_異常系_必須項目省略() {
        return [
            'scenario_type'      => ['scenario_type'],
            'labels'             => ['labels'],
            'condition_operator' => ['condition_operator'],
            'conditions'         => ['conditions'],
            'trigger_after'      => ['trigger_after'],
            'actions'            => ['actions'],
            'enabled'            => ['enabled'],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの更新_異常系_必須項目省略_conditions
     */
    public function シナリオの更新_異常系_必須項目省略_conditions($key) {

        //シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenarioId, $scenario, $key){

            unset($scenario['conditions'][0][$key]);

            //"unit"の省略を確認するため、"operator"は"=="などunitが必須なもので固定する。
            if($key != 'operator') {
                $scenario['conditions'][0]['operator'] = '==';
            }


            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    public function getDataFor_シナリオの更新_異常系_必須項目省略_conditions() {
        return [
            'lhs'      => ['lhs'],
            'operator' => ['operator'],
            'rhs'      => ['rhs'],
        ];
    }

    /**
     * @test
     * @dataProvider getDataFor_シナリオの更新_異常系_必須項目省略_actions
     */
    public function シナリオの更新_異常系_必須項目省略_actions($action) {

        //シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenarioId, $scenario, $action){

            $scenario['actions'][0] = $action;

            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    /**
     * @test
     */
    public function getDataFor_シナリオの更新_異常系_必須項目省略_actions() {
        return [
            'type省略' => [ [] ],
            'content省略' => [ ['type' => 'send_message'] ],  //type=send_messageの時、contentは省略不可
        ];

    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの更新_異常系_異常値
     */
    public function シナリオの更新_異常系_異常値($key, $value) {

        //シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenarioId, $scenario, $key, $value){

            $scenario[$key] = $value;
            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    public function getDataFor_シナリオの更新_異常系_異常値() {
        return [
            'scenario_type'      => ['scenario_type', 'invalid_string' ],
            'labels'             => ['labels', 'invalid_string'],
            'condition_operator' => ['condition_operator', 'invalid_string'],
            'conditions'         => ['conditions', 'invalid_string'],
            'trigger_after'      => ['trigger_after', 'invalid_string'],
            'actions'            => ['actions', 'invalid_string'],
            'enabled'            => ['enabled', 'invalid_string'],
            'enabled_period'     => ['enabled_period', 'invalid_string'],
            'enabled_days_無効'  => ['enabled_days', 'invalid_string'],
            'enabled_days_重複'  => ['enabled_days', ['sun', 'mon', 'sun']],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの更新_異常系_異常値_conditions
     */
    public function シナリオの更新_異常系_異常値_conditions($key, $value) {

        //シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenarioId, $scenario, $key, $value){

            $scenario['conditions'][0][$key] = $value;
            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    public function getDataFor_シナリオの更新_異常系_異常値_conditions() {
        return [
            'operator'            => ['operator', 'invalid_string'],
            'rhs(operator=equal)' => ['rhs', 'invalid_string'],
            'rhs(operator=freq)'  => ['rhs', 'invalid_string'],
        ];
    }


    /**
     * @test
     */
    public function シナリオの更新_異常系_AMQP_AMQPメッセージにてNG応答あり() {

        //シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //AMQP APIでNG応答が返るよう設定する。
        $this->amqpClient->requestForceNgEnabled(true);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($scenarioId, $scenario){

            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     * @group slow
     */
    public function シナリオの登録_異常系_AMQP_AMQPメッセージタイムアウト() {

        //シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //AMQP APIでタイムアウトが発生するよう設定する。
        $this->amqpClient->requestTimeout(true);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($scenarioId, $scenario){

            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     * @group slow
     */
    public function シナリオの更新_異常系_AMQP_AMQPメッセージタイムアウト後OK応答あり() {

        //シナリオを登録する。
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($expectedScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //AMQP APIで遅れてOK応答が行われるよう設定する。
        $this->amqpClient->requestSleep(11);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($scenarioId, $expectedScenario){

            //更新が反映されたことが確認できるように、
            //シナリオの情報を一部変更してupdateを実行する。
            $expectedScenario['trigger_after'] += 1000;

            $this->updateScenario($scenarioId, $expectedScenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

        //HTTP APIのタイムアウト後、
        //OK応答が受信されるまで待機する
        sleep(4);

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenarios = json_decode($response->getBody(), true);
	$expectedScenario['trigger_after'] = 1010;
        $scenarioConstraint = new SimpleScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenarios, $scenarioConstraint, '想定されるシナリオ情報を含むこと');

    }

    /**
     * @test
     */
    public function シナリオの更新_異常系_AMQP_AMQPメッセージにてタイムアウト後NG応答あり() {

        //シナリオを登録する。
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //AMQP APIでNG応答が返るよう設定する。
        $this->amqpClient->requestForceNgEnabled(true);
        $this->amqpClient->requestSleep(11);

        //エラーのレスポンスが返却されることを確認する。
        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($scenarioId, $scenario){

            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


}
