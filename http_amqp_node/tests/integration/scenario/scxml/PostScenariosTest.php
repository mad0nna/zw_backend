<?php

use Lcas\Test\Constraint\Scenario\ScxmlScenarioConstraint;

require_once __DIR__ . '/../AbstractScenarioTestCase.php';


/**
 * scenario_type: scxml のシナリオのテスト
 */
class PostScenarios_ScxmlTest extends AbstractScenarioTestCase {

    /**
     * @test
     */
    public function シナリオの登録_type_scxml_正常系_scenario_type_scxml() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);


        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_SCXML);
        $expectedScenario['device_list'] = [200, 201];
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        //POST scenariosのレスポンスのフォーマットが正しいことを確認する
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];
        $this->assertInternalType('integer', $registerResponseJson['id'], 'レスポンスのフォーマットが正しいこと');

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new ScxmlScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');


    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_type_scxml_異常系_必須項目省略
     */
    public function シナリオの登録_type_scxml__異常系_必須項目省略($key) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //"TEST_DEFAULT_GATEWAY"のフィクスチャに含まれる温度系と湿度系を指定してシナリオを登録する。
        $temperatureDeviceId = 200;
        $humidityDeviceId = 201;
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_SCXML);
        $scenario['device_list'] = [$temperatureDeviceId, $humidityDeviceId];

        //必須項目を省略してシナリオの登録を実行するとエラーが発生することを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($key, $scenario){

            unset($scenario[$key]);
            $this->registerScenario($scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    public function getDataFor_シナリオの登録_type_scxml_異常系_必須項目省略() {

        return [
            'scxml' => ['scxml'],
            'device_list'    => ['device_list'],
            'action_state_list'  => ['action_state_list'],
        ];

    }

    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_type_scxml_異常系_異常値
     */
    public function シナリオの登録_type_scxml_異常系_異常系_異常値($key, $value) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //"TEST_DEFAULT_GATEWAY"のフィクスチャに含まれる温度系と湿度系を指定してシナリオを登録する。
        $temperatureDeviceId = 200;
        $humidityDeviceId = 201;
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_SCXML);
        $scenario['device_list'] = [$temperatureDeviceId, $humidityDeviceId];

        //不正なデータを渡すとエラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($key, $value, $scenario){

            $scenario[$key] = $value;
            $this->registerScenario($scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    public function getDataFor_シナリオの登録_type_scxml_異常系_異常値() {

        return [
            'device_list'    => ['device_list',    'invalid-string'],
            'action_state_list'  => ['action_state_list',  'invalid-string'],
        ];

    }

    /**
     * @test
     */
    public function シナリオの登録_type_scxml_異常系_アクセス権限_共有() {
    	
    	//別ユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
    
    	//別ユーザーにゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
    	sleep(4);
    	
    	$this->nodeUtil->shareNodeWith(200, array(TEST_DEFAULT_USER_EMAIL));
    
        //デフォルトのユーザーでログインしなおす
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
    	
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_SCXML);
        $expectedScenario['device_list'] = [200, 201];
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        //POST scenariosのレスポンスのフォーマットが正しいことを確認する
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];
        $this->assertInternalType('integer', $registerResponseJson['id'], 'レスポンスのフォーマットが正しいこと');

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new ScxmlScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');

    }
    
    /**
     * @test
     */
    public function シナリオの登録_type_scxml_異常系_アクセス権限_権限なし() {
    	 
    	//別ユーザーを登録する
    	$user2Email = 'user2@example.com';
    	$this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
    	$this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
    
    	//別ユーザーにゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
    	sleep(4);
    
    	//デフォルトのユーザーでログインしなおす
    	$this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
    	 
    	//シナリオを登録する。
    	$expectedStatusCode = 404;
    	$scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_SCXML);
    	$scenario['device_list'] = array(201, 202);
    	$this->assertGeneralErrorResponse(function() use($scenario){
    		 
    		$this->registerScenario($scenario);
    		 
    	}, $expectedStatusCode, '有効なエラーレスポンスであること');
    }
}
