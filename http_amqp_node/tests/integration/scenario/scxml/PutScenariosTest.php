<?php

use Lcas\Test\Constraint\Scenario\ScxmlScenarioConstraint;

require_once __DIR__ . '/../AbstractScenarioTestCase.php';


/**
 * scenario_type: scxml のシナリオのテスト
 */
class PutScenarios_ScxmlTest extends AbstractScenarioTestCase {

    /**
     * @test
     */
    public function シナリオの更新_type_scxml_正常系_scenario_type_scxml() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);


        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_SCXML);
        $expectedScenario['device_list'] = [200, 201];
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        //POST scenariosのレスポンスのフォーマットが正しいことを確認する
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];
        $this->assertInternalType('integer', $registerResponseJson['id'], 'レスポンスのフォーマットが正しいこと');


        //シナリオの更新を行う。
        $expectedScenario['labels'][] = 'updated';
        $expectedScenario['scxml'] = '<?xml version="1.0" encoding="UTF-8"?><scxml xmlns="http://www.w3.org/2005/07/scxml" version="1.0" initial="absent" datamodel="ecmascript"><datamodel><!-- 使用するセンサーリスト。全てID指定 --><!-- 入口のドアセンサー --><data id="entrance_door_device_id" expr="10" /><!-- トイレのドアセンサー --><data id="restroom_door_device_id" expr="11" /><!-- Vital Sensor心拍 --><data id="heartrate_device_id" expr="12" /><!-- Vital Sensor呼吸 --><data id="respiratoryrate_device_id" expr="13" /><!-- Vital Sensor BR比 --><data id="br_ratio_device_id" expr="14" /><!-- 人感センサー（任意の数） --><data id="motion_device_ids" expr="[15,16,17]" /><!-- 時間の単位はミリ秒。但し、時間精度はもっと粗い --><!-- 入口が閉まってから以下の時間内に人感センサが反応したら入室 --><data id="entrance_timer" expr="180000" /><!-- 人感センサが反応してから以下の時間内にドアが閉まったら退室 --><data id="maybe_leaving_timer" expr="180000" /><!-- 入室中、以下の時間人感反応がなかったらアラート（ベッド上除く） --><data id="room_alert_timer" expr="7200000" /><!-- ベッド上で以下の時間呼吸が観測されなかったらアラート --><data id="no_resp_timer" expr="600000" /><!-- タイマーID用の変数 --><data id="entrance_timer_id" /><data id="maybe_leaving_timer_id" /><data id="room_alert_timer_id" /><data id="no_resp_timer_id" /></datamodel></scxml>';
        $expectedScenario['device_list'] = [201];
        $expectedScenario['action_state_list'] = ['on_bed'];
        $this->updateScenario($scenarioId, $expectedScenario);

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //変更内容が反映されていることを確認する。
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new ScxmlScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');


    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_type_scxml_異常系_必須項目省略
     */
    public function シナリオの登録_type_scxml__異常系_必須項目省略($key) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //"TEST_DEFAULT_GATEWAY"のフィクスチャに含まれる温度系と湿度系を指定してシナリオを登録する。
        $temperatureDeviceId = 200;
        $humidityDeviceId = 201;
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_SCXML);
        $scenario['device_list'] = [$temperatureDeviceId, $humidityDeviceId];

        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //必須項目を省略してシナリオの登録を実行するとエラーが発生することを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($key, $scenarioId, $scenario){

            unset($scenario[$key]);
            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    public function getDataFor_シナリオの登録_type_scxml_異常系_必須項目省略() {

        return [
            'scxml' => ['scxml'],
            'device_list'    => ['device_list'],
            'action_state_list'  => ['action_state_list'],
        ];

    }

    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_type_scxml_異常系_異常値
     */
    public function シナリオの登録_type_scxml_異常系_異常系_異常値($key, $value) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //"TEST_DEFAULT_GATEWAY"のフィクスチャに含まれる温度系と湿度系を指定してシナリオを登録する。
        $temperatureDeviceId = 200;
        $humidityDeviceId = 201;
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_SCXML);
        $scenario['device_list'] = [$temperatureDeviceId, $humidityDeviceId];

        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //不正なデータを渡すとエラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($key, $value, $scenarioId, $scenario){

            $scenario[$key] = $value;
            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    public function getDataFor_シナリオの登録_type_scxml_異常系_異常値() {

        return [
            'device_list'    => ['device_list',    'invalid-string'],
            'action_state_list'  => ['action_state_list',  'invalid-string'],
        ];

    }

}
