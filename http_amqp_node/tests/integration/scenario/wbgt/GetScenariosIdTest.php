<?php

use Lcas\DB\DB;
use Lcas\Test\Constraint\Scenario\WbgtScenarioConstraint;

require_once __DIR__ . '/../AbstractScenarioTestCase.php';


/**
 * scenario_type: wbgt のシナリオのテスト
 */
class GetScenariosId_WbgtTest extends AbstractScenarioTestCase {

    /**
     * @test
     */
    public function 指定シナリオの参照_正常系_scenario_type_wbgt_自身のシナリオの参照() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        sleep(4);

        //"TEST_DEFAULT_GATEWAY"のフィクスチャに含まれる温度系と湿度系を指定してシナリオを登録する。
        $temperatureDeviceId = 200;
        $humidityDeviceId = 201;
        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_WBGT);
        $expectedScenario['temperature_dev'] = $temperatureDeviceId;
        $expectedScenario['humidity_dev'] = $humidityDeviceId;

        $registerResponse = $this->registerScenario($expectedScenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //シナリオ一覧を取得する
        $response = $this->getScenarioById($scenarioId);

        //レスポンスの内容が正しいこと
        $responseJson = json_decode($response->getBody(), true);

        $wbgtScenarioConstraint = new WbgtScenarioConstraint($expectedScenario);
        $this->assertThat($responseJson, $wbgtScenarioConstraint, "シナリオの内容が想定通りであること");
    }


};
