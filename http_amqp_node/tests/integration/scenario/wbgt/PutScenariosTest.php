<?php

use Lcas\Test\Constraint\Scenario\WbgtScenarioConstraint;

require_once __DIR__ . '/../AbstractScenarioTestCase.php';


/**
 * scenario_type: wbgt のシナリオのテスト
 */
class PutScenarios_WbgtTest extends AbstractScenarioTestCase {

    /**
     * @test
     */
    public function シナリオの更新_type_wbgt_正常系_scenario_type_wbgt() {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);


        //TEST_DEFAULT_GATEWAY フィクスチャの体温計と湿度計のデバイスを指定してシナリオを登録する。
        $temperatureDeviceId = 200;
        $humidityDeviceId = 201;

        $expectedScenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_WBGT);
        $expectedScenario['temperature_dev'] = $temperatureDeviceId;
        $expectedScenario['humidity_dev'] = $humidityDeviceId;
        $registerResponse = $this->registerScenario($expectedScenario);
        $this->assertEquals(200, $registerResponse->getStatusCode(), 'ステータスコードが200であること');

        //POST scenariosのレスポンスのフォーマットが正しいことを確認する
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];
        $this->assertInternalType('integer', $registerResponseJson['id'], 'レスポンスのフォーマットが正しいこと');


        //シナリオの更新を行う。
        $expectedScenario['labels'][] = 'updated';
        $expectedScenario['wbgt_threshold'] = 20;
        $this->updateScenario($scenarioId, $expectedScenario);

        //シナリオを取得する
        $response = $this->getScenarioById($scenarioId);

        //変更内容が反映されていることを確認する。
        $registeredScenario = json_decode($response->getBody(), true);
        $scenarioConstraint = new WbgtScenarioConstraint($expectedScenario);
        $this->assertThat($registeredScenario, $scenarioConstraint, '想定されるシナリオ情報を含むこと');


    }


    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_type_wbgt_異常系_必須項目省略
     */
    public function シナリオの登録_type_wbgt__異常系_必須項目省略($key) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //"TEST_DEFAULT_GATEWAY"のフィクスチャに含まれる温度系と湿度系を指定してシナリオを登録する。
        $temperatureDeviceId = 200;
        $humidityDeviceId = 201;
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_WBGT);
        $scenario['temperature_dev'] = $temperatureDeviceId;
        $scenario['humidity_dev'] = $humidityDeviceId;

        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //必須項目を省略してシナリオの登録を実行するとエラーが発生することを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($key, $scenarioId, $scenario){

            unset($scenario[$key]);
            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    public function getDataFor_シナリオの登録_type_wbgt_異常系_必須項目省略() {

        return [
            'temperature_dev' => ['temperature_dev'],
            'humidity_dev'    => ['humidity_dev'],
            'wbgt_threshold'  => ['wbgt_threshold'],
        ];

    }

    /**
     * @test
     * @dataProvider getDataFor_シナリオの登録_type_wbgt_異常系_異常値
     */
    public function シナリオの登録_type_wbgt_異常系_異常系_異常値($key, $value) {

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //"TEST_DEFAULT_GATEWAY"のフィクスチャに含まれる温度系と湿度系を指定してシナリオを登録する。
        $temperatureDeviceId = 200;
        $humidityDeviceId = 201;
        $scenario = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_WBGT);
        $scenario['temperature_dev'] = $temperatureDeviceId;
        $scenario['humidity_dev'] = $humidityDeviceId;

        $registerResponse = $this->registerScenario($scenario);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //不正なデータを渡すとエラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($key, $value, $scenarioId, $scenario){

            $scenario[$key] = $value;
            $this->updateScenario($scenarioId, $scenario);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    public function getDataFor_シナリオの登録_type_wbgt_異常系_異常値() {

        return [
            'temperature_dev' => ['temperature_dev', 'invalid-string'],
            'humidity_dev'    => ['humidity_dev',    'invalid-string'],
            'wbgt_threshold'  => ['wbgt_threshold',  'invalid-string'],
        ];

    }

}
