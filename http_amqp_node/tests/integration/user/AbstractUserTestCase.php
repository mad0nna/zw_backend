<?php


use GuzzleHttp\Cookie\SetCookie;
use Lcas\DB\DB;
use Lcas\Repository\UserRepository;
use Lcas\Test\Util\TestUtil;

class AbstractUserTestCase extends \Lcas\Test\IntegrationTestCase {

    /**
     * @var UserRepository
     */
    protected $userRepository;


    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->userRepository = new UserRepository();
    }



    public function setUp() {
        parent::setUp();

        //テストデータを全削除
        TestUtil::clearDatabase();

    }


    protected function getSessionIdFromResponse(\GuzzleHttp\Psr7\Response $response) {
        $cookieValue = '';
        if($response->hasHeader('Set-Cookie')) {
            //パースされていないCookieが返ってくる
            $headers = $response->getHeader('Set-Cookie');
            foreach($headers as $header) {
                $cookieParser = SetCookie::fromString($header);
                if($cookieParser->getName() != AUTH_SESS_COOKIE_NAME) {
                    continue;
                }
                $cookieValue = $cookieParser->getValue();
            }

        }
        return $cookieValue;
    }


    /**
     * user_tokensのレコード情報をemailから検索して返す。
     * @param $email
     * @return mixed
     */
    protected function findUserTokenByEmail($email) {
        $sql = "SELECT * FROM user_tokens WHERE email='{$email}'";
        $db = DB::getMasterDb();

        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception('SQLエラー： ' . $db->getErr() . 'SQL: ' . $sql);
        }

        $result = $db->Fetch();
        return $result;
    }


    /**
     * メールアドレスを基に、usersテーブルのレコードを取得して返す。
     *
     * UserRepositoryのメソッドはusers.passを返しませんが、このメソッドはパスワードも含めて返します。
     */
    protected function getUserRecordByEmail($email) {
        $sql = "SELECT * FROM users WHERE email='{$email}'";
        $db = DB::getMasterDb();

        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception('SQLエラー： ' . $db->getErr() . 'SQL: ' . $sql);
        }

        $result = $db->Fetch();
        return $result;
    }


    /**
     * メールアドレスを基に、labels テーブルのレコードを取得して返す。
     */
    protected function getUserLabelsByEmail($email) {
    	$user = $this->getUserRecordByEmail($email);
    	if (is_null($user)) {
    		return null;
    	}
    	
        $sql = "SELECT * FROM labels WHERE type=5 AND entity_id={$user['id']}";
        $db = DB::getMasterDb();

        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception('SQLエラー： ' . $db->getErr() . 'SQL: ' . $sql);
        }
        
        $result = array();
        while ($row = $db->Fetch()) {
        	array_push($result, $row);
        }
        return $result;
    }


    protected function forceExpireUserTokenByEmail($email) {
        //トークンの有効期限切れは24時間のため、24時間前に変更
        $newExpires = date('Y-m-d H:i:s', strtotime('-24 hours -1 seconds'));
        $sql = "UPDATE user_tokens SET expires='{$newExpires}' WHERE email='{$email}'";
        $db = DB::getMasterDb();

        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception('SQLエラー： ' . $db->getErr() . 'SQL: ' . $sql);
        }
    }
    
    /**
     * ユーザーのシナリオ一覧を取得する
     * @param int $userId
     */
    protected function findUserScenarios($userId) {
    	$userId = (int)$userId;
    	
    	$sql = "SELECT * FROM scenarios WHERE user_id={$userId}";
    	$db = DB::getMasterDb();
    	
    	$db->setSql($sql);
    	if(!$db->Query()) {
    		throw new \Exception('SQLエラー： ' . $db->getErr() . 'SQL: ' . $sql);
    	}
    	
    	$result = $db->Fetch();
    	return $result;
    }

    /**
     * ユーザーのラベル一覧を取得する
     * @param int $userId
     */
    protected function findUserLabels($userId) {
    	$userId = (int)$userId;
    	
    	$sql = "SELECT * FROM labels WHERE type=5 AND entity_id={$userId}";
    	$db = DB::getMasterDb();
    	
    	$db->setSql($sql);
    	if(!$db->Query()) {
    		throw new \Exception('SQLエラー： ' . $db->getErr() . 'SQL: ' . $sql);
    	}
    	
    	$result = $db->Fetch();
    	return $result;
    }

    /**
     * ユーザーのノードラベル一覧を取得する
     * @param int $userId
     */
    protected function findUserNodeLabels($userId) {
    	$userId = (int)$userId;
    	
    	$sql = "SELECT * FROM labels WHERE type=2 AND user_id={$userId}";
    	$db = DB::getMasterDb();
    	
    	$db->setSql($sql);
    	if(!$db->Query()) {
    		throw new \Exception('SQLエラー： ' . $db->getErr() . 'SQL: ' . $sql);
    	}
    	
    	$result = $db->Fetch();
    	return $result;
    }

    /**
     * ユーザーの共有ノード一覧を取得する
     * @param int $userId
     */
    protected function findUserSharedNodes($userId) {
    	$userId = (int)$userId;
    	
    	$sql = "SELECT * FROM nodes_shares WHERE user_id={$userId}";
    	$db = DB::getMasterDb();
    	
    	$db->setSql($sql);
    	if(!$db->Query()) {
    		throw new \Exception('SQLエラー： ' . $db->getErr() . 'SQL: ' . $sql);
    	}
    	
    	$result = $db->Fetch();
    	return $result;
    }

    protected function registMailTemplate($origin, $platform, $type, $sender, $subject, $content) {
        $sql = "INSERT INTO mail_templates "
        		. " ( "
        		. " origin, "
        		. " platform, "
        		. " template_type, "
        		. " sender, "
        		. " subject, "
        		. " content "
        		. " ) "
        		. " VALUES ( "
        		. " '$origin', "
        		. " $platform, "
        		. " $type, "
        		. " '$sender', "
        		. " '$subject', "
        		. " '$content') ";
        		$db = DB::getMasterDb();

        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception('SQLエラー： ' . $db->getErr() . 'SQL: ' . $sql);
        }
    }
}
