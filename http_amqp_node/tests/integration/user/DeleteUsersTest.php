<?php

use Lcas\Test\Fixture\GatewayFixtureFactory;

require_once __DIR__ . '/AbstractUserTestCase.php';

class DeleteUsersTest extends AbstractUserTestCase {

    /**
     * @var GatewayFixtureFactory
     */
    protected $gatewayFactory;

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->gatewayFactory = new GatewayFixtureFactory();
    }
    
    /**
     * @test
     * @dataProvider getDataFor_ユーザー削除_正常系_一般
     */
	public function ユーザー削除_正常系_一般($hasScenario, $hasSharedNode, $hasNodeLabel, $hasLabel) {
		
		// ゲートウェイの所有者を作成する
		$anotherUserEmail = 'test2@example.com';
		$this->userUtil->registerUser($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD, 'none');
		$owner = $this->userRepository->findUserByEmail($anotherUserEmail);
		$this->userUtil->login($anotherUserEmail, TEST_DEFAULT_USER_PASSWORD);
		
		//ユーザーにゲートウェイを割り当てる
		$defaultGateway = 'A00000001000';
		$this->gatewayUtil->registerGateway($defaultGateway);
		$gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress($defaultGateway);
		$gatewayData = $this->gatewayFactory->create($defaultGateway);
		$nodeId = $gatewayData['nodes'][1]['node_id'];
		
		// Node Inclusion 処理完了待ち
		sleep(5);
			
		// デフォルトのユーザーを登録する
		$this->userUtil->registerDefaultUser();
		$user = $this->userRepository->findUserByEmail(TEST_DEFAULT_USER_EMAIL);
		
		// ユーザーの存在確認
		$checkUser = $this->userUtil->findUser($user['id']);
		$this->assertNotNull($checkUser, 'ユーザーが存在していること');
		
		// 共有の有無
		if ($hasSharedNode || $hasNodeLabel) {
	        // 共有を登録する
	        $this->nodeUtil->shareNodeWith($nodeId, array(TEST_DEFAULT_USER_EMAIL));

			// 共有ノードの存在確認
			$checkSharedNodes = $this->nodeUtil->findUserSharedNodes($user['id']);
			$this->assertEquals(1, count($checkSharedNodes), '共有ノードが存在していること');
		}
		
		// ログイン
		$this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
		
		// ノードラベルの有無
		if ($hasNodeLabel) {
			// ノードラベルを登録する
	        $this->nodeUtil->updateNodeUserLabels($nodeId, $user['id'], array('ノードラベル1', 'ノードラベル2', 'ノードラベル3'));

			// ノードラベルの存在確認
			$checkNodeLabels = $this->nodeUtil->findUserNodeLabels($user['id']);
			$this->assertEquals(3, count($checkNodeLabels), 'ノードラベルが存在していること');
		}
		
		// ラベルの有無
		if ($hasLabel) {
			// ラベルを登録する
			$this->userUtil->updateLabels(array('ユーザーラベル1', 'ユーザーラベル2', 'ユーザーラベル3', 'ユーザーラベル4'));

			// ラベルの存在確認
			$checkLabels = $this->userUtil->findUserLabels($user['id']);
			$this->assertEquals(4, count($checkLabels), 'ユーザーラベルが存在していること');
		}
		
		// シナリオの有無
		if ($hasScenario) {
	
	        //ゲートウェイを割り当てる
    	    $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
      		sleep(4);

			// シナリオを登録する
			$this->scenarioUtil->registerDefaultScenario();

			// シナリオの存在確認
			$checkScenarios = $this->scenarioUtil->findUserScenarios($user['id']);
			$this->assertEquals(1, count($checkScenarios), 'シナリオが存在していること');

	        //ゲートウェイの登録解除
			$gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);
    	    $this->gatewayUtil->deregisterGateway($gatewayId);
      		sleep(4);

		}
		
		// ユーザーの削除をおこなう
		$this->deleteUser(TEST_DEFAULT_USER_PASSWORD);
		
		// ユーザーの存在確認
		$checkUser = $this->userUtil->findUser($user['id']);
		$this->assertNull($checkUser, 'ユーザーが削除されること');
		
		// シナリオの存在確認
		$checkScenarios = $this->scenarioUtil->findUserScenarios($user['id']);
		$this->assertEquals(0, count($checkScenarios), 'シナリオが削除されること');
		
		// 共有ノードの存在確認
		$checkSharedNodes = $this->nodeUtil->findUserSharedNodes($user['id']);
		$this->assertEquals(0, count($checkSharedNodes), '共有ノードが削除されること');
		
		// ノードラベルの存在確認
		$checkNodeLabels = $this->nodeUtil->findUserNodeLabels($user['id']);
		$this->assertEquals(0, count($checkNodeLabels), 'ノードラベルが削除されること');
		
		// ラベルの存在確認
		$checkLabels = $this->userUtil->findUserLabels($user['id']);
		$this->assertEquals(0, count($checkLabels), 'ユーザーラベルが削除されること');
		
    }

    public function getDataFor_ユーザー削除_正常系_一般() {
        return [
            'シナリオあり/共有あり/ノードラベルあり/ラベルあり' => [true, true, true, true],
            'シナリオなし/共有なし/ノードラベルなし/ラベルなし' => [false, false, false, false],
            'シナリオなし/共有なし/ノードラベルなし/ラベルあり' => [false, false, false, true],
            'シナリオなし/共有なし/ノードラベルあり/ラベルなし' => [false, false, true, false],
            'シナリオなし/共有あり/ノードラベルなし/ラベルなし' => [false, true, false, false],
            'シナリオあり/共有なし/ノードラベルなし/ラベルなし' => [true, false, false, false],
        ];
    }

    /**
     * @test
     */
	public function ユーザー削除_異常系_一般_パスワード未指定() {
			
		// デフォルトのユーザーを登録する
		$this->userUtil->registerDefaultUser();
		$user = $this->userRepository->findUserByEmail(TEST_DEFAULT_USER_EMAIL);
		
		// ユーザーの存在確認
		$checkUser = $this->userUtil->findUser($user['id']);
		$this->assertNotNull($checkUser, 'ユーザーが存在していること');
		
		// ログイン
		$this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
		
		// ユーザーの削除をおこなう
		$expectedStatusCode = 400;
		$this->assertGeneralErrorResponse(function() {
		
			$this->deleteUser('');
		
		}, $expectedStatusCode, '有効なエラーレスポンスであること');
    }
    
    /**
     * @test
     */
	public function ユーザー削除_異常系_一般_パスワード誤り() {
			
		// デフォルトのユーザーを登録する
		$this->userUtil->registerDefaultUser();
		$user = $this->userRepository->findUserByEmail(TEST_DEFAULT_USER_EMAIL);
		
		// ユーザーの存在確認
		$checkUser = $this->userUtil->findUser($user['id']);
		$this->assertNotNull($checkUser, 'ユーザーが存在していること');
		
		// ログイン
		$this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
		
		// ユーザーの削除をおこなう
		$expectedStatusCode = 403;
		$this->assertGeneralErrorResponse(function() {
		
			$this->deleteUser('drowssap!');
		
		}, $expectedStatusCode, '有効なエラーレスポンスであること');
    }
    
    /**
     * @test
     */
	public function ユーザー削除_異常系_一般_未ログイン() {
			
		try {
			// ユーザーの削除をおこなう
			$this->deleteUser(TEST_DEFAULT_USER_PASSWORD);
			$this->assertTrue(false, 'ステータスコードが401であること');
		} catch (Exception $e) {
			$this->assertEquals(401, $e->getCode(), 'ステータスコードが401であること');
	    }
    }
    
    /**
     * @test
     */
	public function ユーザー削除_異常系_一般_ゲートウェイの所有者() {
			
		// デフォルトのユーザーを登録する
		$this->userUtil->registerDefaultUser();
		$user = $this->userRepository->findUserByEmail(TEST_DEFAULT_USER_EMAIL);
		$this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
		
		// ユーザーにゲートウェイを割り当てる
		$defaultGateway = TEST_DEFAULT_GATEWAY;
		$this->gatewayUtil->registerGateway($defaultGateway);
		
		// Node Inclusion 処理完了待ち
		sleep(5);
		
		// ユーザーの存在確認
		$checkUser = $this->userUtil->findUser($user['id']);
		$this->assertNotNull($checkUser, 'ユーザーが存在していること');
		
		// ユーザーの削除をおこなう
		$expectedStatusCode = 400;
		$this->assertGeneralErrorResponse(function() {
		
			$this->deleteUser(TEST_DEFAULT_USER_PASSWORD);
		
		}, $expectedStatusCode, '有効なエラーレスポンスであること');
    }
    
    /**
     * @test
     * @dataProvider getDataFor_ユーザー削除_異常系_異常値
     */
    public function ユーザー削除_異常系_異常値($password) {
		// デフォルトのユーザーを登録する
		$this->userUtil->registerDefaultUser();
		$user = $this->userRepository->findUserByEmail(TEST_DEFAULT_USER_EMAIL);
		$this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
		
		// ユーザーの存在確認
		$checkUser = $this->userUtil->findUser($user['id']);
		$this->assertNotNull($checkUser, 'ユーザーが存在していること');
		
		try {
			// ユーザーの削除をおこなう
			$this->deleteUser($password);
			$this->assertTrue(false, 'ステータスコードが403であること');
		} catch (Exception $e) {
			$this->assertEquals(403, $e->getCode(), 'ステータスコードが403であること');
	    }
    }

    public function getDataFor_ユーザー削除_異常系_異常値() {

        return [
            '無効なパスワード' => ['invalid'],
            '日本語のパスワード' => ['パスワード'],
            '長いパスワード(500文字)' => ['sUdZFzTm2thgVMWJp5iwHRNXfaYYN6xM2NSwBKDUbTM7xGdkLQWiiuap2HRKu4x3QYFKa9yCky5TWzLbHX5ercAj8DuWrN2FY-rizM7wVHgBuLXsVixdg4XiANDgDW3j6YmLf9BjZku93ht_DDnXhJYYijs8xFmmfgEtsjCrJtPVKMDGb2DwmVp9cMnxFbiKV8K6Xydcm9jNB_-8ADNpzJF7uppcDjQ8uZdu_zge4jHV4Ax-kYjXBDppttEt98PEmm-hTub6PB73pGKUUdYGGZiSXxuVfsT5UcSUHW3hUckEtTFjRupibTXVFC9b8S7j5zzYAS5FaxmCPBMxJEpVJnHnhEyCmn6LDpdESdypuwnFf2pj6rbcLwXJXDNEnkhzVBi9AhYssDNecSyiGdYw24_cSXSkapLTSTET8zDW93RtXZ4S6FZugAjYyEUMTtAkY5ed9rxH7FMcRLimieGCYhjXxRQ4bRd8ipxRUCHWncRjF4GYbMpF'],
        ];

    }

    private function deleteUser($password) {
        $parameters = [
        		'password' => $password,
        ];

        $response = $this->httpClient->delete('/api/v3/users/self', [
            'json' => $parameters
        ]);
        return $response;
    }
}
