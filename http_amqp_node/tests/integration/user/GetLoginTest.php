<?php


use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Exception\ClientException;
use Lcas\DB\DB;
use Lcas\Repository\UserRepository;
use Lcas\Test\Util\TestUtil;

require_once __DIR__ . '/AbstractUserTestCase.php';

class GetLoginTest extends AbstractUserTestCase {


    public function setUp() {
        parent::setUp();

        //デフォルトのユーザーを登録する
        $platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
        $this->userUtil->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, $platform);
    }

    /**
     * @test
     */
    public function ログイン状態確認_正常系_成功() {
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        $response = $this->getLoginStatus();
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');
    }


    /**
     * @test
     */
    public function ログイン状態確認_異常系_一般_Cookieなし() {

        //認証Cookieなしでログイン状態を確認するとstatus:NGを含むレスポンスが
        //返されることを確認する
        $cookieJar = new CookieJar();
        $response = $this->httpClient->get('/api/v3/login', ['cookies' => $cookieJar]);

        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        $json = json_decode($response->getBody(), true);
        $this->assertEquals('NG', $json['status'], 'status:NGというレスポンスが返されること');
    }


    /**
     * @test
     */
    public function ログイン状態確認_異常系_一般_存在しないセッションIDを使用() {

        //認証Cookieなしでログイン状態を確認するとstatus:NGを含むレスポンスが
        //返されることを確認する

        $response = $this->getLoginStatusWithSessionId('invalid-session-id');

        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        $json = json_decode($response->getBody(), true);
        $this->assertEquals('NG', $json['status'], 'status:NGというレスポンスが返されること');
    }


    /**
     * @test
     */
    public function ログイン状態確認_異常系_一般_新しいログインで無効化されたセッションIDを使用() {

        //ログイン実行後、セッションIDを控える
        $response = $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        $oldSessionId = $this->getSessionIdFromResponse($response);

        //同じユーザーで再度ログインを行う
        //(古いセッションIDは無効になっているはず)
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        //古いセッションIDでログイン状態を確認するとNGであることを確認する
        $response = $this->getLoginStatusWithSessionId($oldSessionId);
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        $json = json_decode($response->getBody(), true);
        $this->assertEquals('NG', $json['status'], 'status:NGというレスポンスが返されること');
    }


    /**
     * @test
     */
    public function ログイン状態確認_異常系_一般_有効期限切れセッションIDを使用() {

        //通常通りログインする
        $loginResponse = $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        $sessionId = $this->getSessionIdFromResponse($loginResponse);

        //対象のセッションIDを強制的に有効期限切れにする
        $this->forceExpireSessionId($sessionId);

        $response = $this->getLoginStatus();
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        $json = json_decode($response->getBody(), true);
        $this->assertEquals('NG', $json['status'], 'status:NGというレスポンスが返されること');
    }


    private function getLoginStatus() {
        return $this->httpClient->get('/api/v3/login');
    }

    private function getLoginStatusWithSessionId($sessionId) {

        $cookie = new SetCookie([
            'Name'  => 'SESSID',
            'Value' => $sessionId,
            'Domain' => '0',  //Domainがnullや空白の場合、setCookieに失敗するためダミーの値をセットする
        ]);

        $cookieJar = new CookieJar();
        $cookieJar->setCookie($cookie);

        return $this->httpClient->get('/api/v3/login', ['cookies' => $cookieJar]);
    }


    private function forceExpireSessionId($sessionId) {

        $db = DB::getMasterDb();
        $escapedSessionId = $db->Escape($sessionId);
        $sql = "UPDATE auth_tokens SET expires='2000-01-01 00:00:00' WHERE token='{$escapedSessionId}'";
        $db->setSql($sql);

        if(!$db->Query()) {
            throw new \Exception('SQLの実行に失敗しました。' . $sql);
        }
    }

}
