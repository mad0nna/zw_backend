<?php


use Lcas\Repository\UserRepository;
use Lcas\Test\Constraint\UserConstraint;

require_once __DIR__ . '/AbstractUserTestCase.php';

class GetUsersSelfTest extends AbstractUserTestCase {


    /**
     * @test
     */
    public function ユーザー情報取得_正常系_一般_成功() {

        $deviceToken = 'abc';
        $this->userUtil->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, 'none', $deviceToken);
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        
        $labels = array("ユーザーラベル1", "ユーザーラベル2", "ユーザーラベル3", "ユーザーラベル4");
        $this->userUtil->updateLabels($labels);

        //ユーザー情報を取得する
        $response = $this->httpClient->get('/api/v3/users/self');
        $this->assertEquals(200, $response->getStatusCode(), 'ステータスコードが200であること');

        $userInfo = json_decode($response->getBody(), true);

        $expectedUser = $this->getUserByEmail(TEST_DEFAULT_USER_EMAIL);
        $expectedInfo = [
            'id' => $expectedUser['id'],
            'email' => TEST_DEFAULT_USER_EMAIL,
            'platform' => 'none',
            'token' => $deviceToken,
        	'labels' => $labels
        ];
        $userConstraint = new UserConstraint($expectedInfo);
        $this->assertThat($userInfo, $userConstraint, '想定されるユーザーの情報を含んでいること');
    }


    private function getUserByEmail($email) {

        $userRepository = new UserRepository();
        $user = $userRepository->findUserByEmail($email);

        return $user;
    }

}
