<?php


use GuzzleHttp\Exception\ClientException;
use Lcas\Repository\UserRepository;
use Lcas\Test\Util\TestUtil;

require_once __DIR__ . '/AbstractUserTestCase.php';

class PostLoginTest extends AbstractUserTestCase {

    /**
     * @test
     */
    public function ログイン_正常系_成功() {
        $platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
        $this->userUtil->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, $platform);

        $response = $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        $cookie = '';
        if($response->hasHeader('Set-Cookie')) {
            $cookieData = $response->getHeader('Set-Cookie');
            $cookie = array_pop($cookieData);
        }

        $this->assertRegExp('/^SESSID=[A-Za-z0-9]+;/', $cookie, '認証用Cookieがセットされていること');
    }


    /**
     * @test
     * @dataProvider getDataFor_ログイン_異常系_異常値
     */
    public function ログイン_異常系_異常値($email, $password) {
        $platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
        $this->userUtil->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, $platform);

        $expectedStatusCode = 401;
        $this->assertGeneralErrorResponse(function() use($email, $password){
            $this->userUtil->login($email, $password);
        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    public function getDataFor_ログイン_異常系_異常値() {

        return [
            '無効なメールアドレス' => ['invalid', TEST_DEFAULT_USER_PASSWORD],
            '無効なパスワード' => [TEST_DEFAULT_USER_EMAIL, 'invalid'],
        ];

    }


    /**
     * @test
     * @dataProvider getDataFor_ログイン_異常系_必須項目省略
     */
    public function ログイン_異常系_必須項目省略($email, $password) {
        $platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
        $this->userUtil->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, $platform);

        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($email, $password){
            $this->userUtil->login($email, $password);
        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    public function getDataFor_ログイン_異常系_必須項目省略() {

        return [
            'email' => [null, TEST_DEFAULT_USER_PASSWORD],
            'password' => [TEST_DEFAULT_USER_EMAIL, null],
        ];

    }
}
