<?php


use GuzzleHttp\Exception\ClientException;
use Lcas\DB\DB;
use Lcas\Repository\UserRepository;

require_once __DIR__ . '/../AbstractUserTestCase.php';

class PostUsersConfirmation_RegistrationTest extends AbstractUserTestCase {

    /**
     * @test
     */
    public function ユーザー本登録_正常系_一般_ユーザー登録成功() {
        $email = 'test@example.com';
        $platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
        $response = $this->userUtil->registerUser($email, TEST_DEFAULT_USER_PASSWORD, $platform);

        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        $user = $this->userRepository->findUserByEmail($email);
        $this->assertEquals($email, $user['email'], 'userテーブルに指定したメールアドレスのユーザーが存在すること');
    }

    /**
     * @test
     */
    public function ユーザー本登録_正常系_一般_ユーザー登録成功_origin() {
    	$email = 'test@example.com';
    	$origin = 'https://app.example.com';
    	$platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
    	$response = $this->userUtil->registerUser($email, TEST_DEFAULT_USER_PASSWORD, $platform, $origin);
    
    	$this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
    
    	$user = $this->userRepository->findUserByEmail($email);
    	$this->assertEquals($email, $user['email'], 'userテーブルに指定したメールアドレスのユーザーが存在すること');
    }
    
    /**
     * @test
     * @dataProvider getDataFor_ユーザー本登録_正常系_platform
     */
    public function ユーザー本登録_正常系_platform($platform) {
        $email = 'test@example.com';
        $platformType = UserRepository::getPlatformTypeFromName($platform);

        //iOSの場合、SNS API呼び出し時に指定するトークンが"64文字の16進"という制約があるため、
        //この制約に合わせる
        $token = '1234567890123456789012345678901234567890123456789012345678901234';

        $response = $this->userUtil->registerUser($email, TEST_DEFAULT_USER_PASSWORD, $platform, $token);

        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        $user = $this->userRepository->findUserByEmail($email);
        $this->assertEquals($email, $user['email'], 'userテーブルに指定したメールアドレスのユーザーが存在すること');
        $this->assertEquals($platformType, $user['platform'], 'platformが一致すること');
    }


    public function getDataFor_ユーザー本登録_正常系_platform() {
        return [
            'gcm'  => ['gcm'],
            'apns' => ['apns'],
            'sqs' => ['sqs'],
        	'none' => ['none']
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_ユーザー本登録_正常系_一般_confirmationでplatformを省略
     */
    public function ユーザー本登録_正常系_一般_confirmationでplatformを省略($platform, $platform_type, $token) {

        //仮登録を行う
        $this->temporaryUserRegistration(TEST_DEFAULT_USER_EMAIL, $platform, $token);

        //confirmation_tokenを調べる
        $confirmationToken = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        //本登録を行う
        $registerParameters = [
            'confirmation_token' => $confirmationToken['token'],
            'password' => TEST_DEFAULT_USER_PASSWORD
        ];
        $this->registerUserForTest($registerParameters);

        $user = $this->userRepository->findUserByEmail(TEST_DEFAULT_USER_EMAIL);
        $this->assertEquals($platform_type, $user['platform'], 'platformが一致すること');
        $this->assertEquals($token, $user['token'], 'tokenが一致すること');
    }


    public function getDataFor_ユーザー本登録_正常系_一般_confirmationでplatformを省略() {
        return [
//            'gcm'  => ['gcm', 2, '1234567890123456789012345678901234567890123456789012345678901234'],
//            'apns' => ['apns', 3, '2345678901234567890123456789012345678901234567890123456789012345'],
            'sqs'  => ['sqs', 4, null],
        	'none' => ['none', 1, null]
        ];
    }


    /**
     * @test
     */
    public function ユーザー本登録_異常系_一般_使用済confirmation_tokenで再リクエスト() {

        //仮登録を行う
        $this->temporaryUserRegistration(TEST_DEFAULT_USER_EMAIL, null, null);

        //confirmation_tokenを調べる
        $confirmationToken = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        //本登録を行う
        $registerParameters = [
            'confirmation_token' => $confirmationToken['token'],
            'password' => TEST_DEFAULT_USER_PASSWORD,
            'platform' => 'none',
        ];
        $this->registerUserForTest($registerParameters);

        //同じconfirmationTokenを使用してもう一度リクエストすると、
        //エラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($registerParameters) {

            $this->registerUserForTest($registerParameters);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     */
    public function ユーザー本登録_異常系_一般_存在しないconfirmation_tokenを使用してリクエスト() {

        //存在しないconfirmationTokenを指定するとエラーが発生することを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() {

            //本登録を行う
            $registerParameters = [
                'confirmation_token' => 'unknown-token',
                'password' => TEST_DEFAULT_USER_PASSWORD,
                'platform' => 'none',
            ];
            $this->registerUserForTest($registerParameters);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }

    /**
     * @test
     */
    public function ユーザー本登録_異常系_一般_有効期限切れのトークンを使用してリクエスト() {

        //仮登録を行う
        $this->temporaryUserRegistration(TEST_DEFAULT_USER_EMAIL, null, null);

        //confirmation_tokenを調べる
        $confirmationToken = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        //トークンを有効期限切れにする
        $this->forceExpireUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        //存在しないconfirmationTokenを指定するとエラーが発生することを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($confirmationToken) {

            //本登録を行う
            $registerParameters = [
                'confirmation_token' => $confirmationToken['token'],
                'password' => TEST_DEFAULT_USER_PASSWORD,
                'platform' => 'none',
            ];
            $this->registerUserForTest($registerParameters);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    /**
     * @test
     * @dataProvider getDataFor_ユーザー本登録_異常系_異常値
     */
    public function ユーザー本登録_異常系_異常値($key, $value) {

        //仮登録を行う
        $this->temporaryUserRegistration(TEST_DEFAULT_USER_EMAIL, null, null);

        //confirmation_tokenを調べる
        $confirmationToken = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        //存在しないconfirmationTokenを指定するとエラーが発生することを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use ($key, $value, $confirmationToken) {

            //本登録を行う
            $registerParameters = [
                'confirmation_token' => $confirmationToken,
                'password' => TEST_DEFAULT_USER_PASSWORD,
                'platform' => 'none',
                'token'    => '1234567890123456789012345678901234567890123456789012345678901234',
            ];
            $registerParameters[$key] = $value;

            $this->registerUserForTest($registerParameters);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    public function getDataFor_ユーザー本登録_異常系_異常値() {
        return [
            'confirmation_token_空文字' => ['confirmation_token', ''],
            'password_英字のみ' => ['password', 'abcdefghij'],
            'password_数字のみ' => ['password', '1234567890'],
            'password_記号のみ' => ['password', "@%+\/’!#$^?:,(){}[]~-_"],
            'password_使用不可な文字の使用' => ['password', '=========='],
            'password_長さ不足' => ['password', 'abc12'],
            'password_長さ超過' => ['password', 'test1234567890123'],
            'password_空文字'   => ['password', ''],
            'platform_無効なplatform' => ['platform', 'invalid'],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_ユーザー本登録_異常系_必須項目省略
     */
    public function ユーザー本登録_異常系_必須項目省略($key) {

        //仮登録を行う
        $this->temporaryUserRegistration(TEST_DEFAULT_USER_EMAIL, null, null);

        //confirmation_tokenを調べる
        $confirmationToken = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        //存在しないconfirmationTokenを指定するとエラーが発生することを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use ($key, $confirmationToken) {

            //本登録を行う
            $registerParameters = [
                'confirmation_token' => $confirmationToken,
                'password' => TEST_DEFAULT_USER_PASSWORD,
                'platform' => 'gcm',
                'token'    => '1234567890123456789012345678901234567890123456789012345678901234',
            ];

            //必須項目を省略する
            unset($registerParameters[$key]);

            $this->registerUserForTest($registerParameters);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    public function getDataFor_ユーザー本登録_異常系_必須項目省略() {
        return [
            'confirmation_token' => ['confirmation_token'],
            'password' => ['password'],
            'platform' => ['platform'],
            'token' => ['token'],
        ];
    }


    /**
     * @test
     */
    public function ユーザー本登録_異常系_AMQP_AMQPメッセージにてNG応答あり() {
        $this->temporaryUserRegistration(TEST_DEFAULT_USER_EMAIL, null, null);
        $confirmationToken = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        $this->amqpClient->requestForceNgEnabled(true);

        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($confirmationToken){

            $registerParam = [
                'confirmation_token' => $confirmationToken['token'],
                'password' => TEST_DEFAULT_USER_PASSWORD,
                'platform' => 'none'
            ];

            $this->registerUserForTest($registerParam);
        }, $expectedStatusCode, 'AMQPメッセージでNG王頭があった場合エラーが発生する');

    }

    /**
     * @test
     * @group slow
     */
    public function ユーザー登録_異常系_AMQP_AMQPメッセージタイムアウト() {
        $this->temporaryUserRegistration(TEST_DEFAULT_USER_EMAIL, null, null);
        $confirmationToken = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        $this->amqpClient->requestTimeout(true);

        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($confirmationToken){

            $registerParam = [
                'confirmation_token' => $confirmationToken['token'],
                'password' => TEST_DEFAULT_USER_PASSWORD,
                'platform' => 'none'
            ];

            $this->registerUserForTest($registerParam);


        }, $expectedStatusCode, 'AMQPメッセージがタイムアウトした場合エラーが発生する');

    }


    /**
     * @test
     * @group slow
     */
    public function ユーザー登録_異常系_AMQP_AMQPメッセージタイムアウト後_OK応答あり() {
        $this->temporaryUserRegistration(TEST_DEFAULT_USER_EMAIL, null, null);
        $confirmationToken = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        $this->amqpClient->requestSleep(11);

        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($confirmationToken){

            $registerParam = [
                'confirmation_token' => $confirmationToken['token'],
                'password' => TEST_DEFAULT_USER_PASSWORD,
                'platform' => 'none'
            ];

            $this->registerUserForTest($registerParam);

        }, $expectedStatusCode, 'AMQPメッセージがタイムアウトした場合エラーが発生する');

        sleep(4);

        //ログインが出来ないこと
        $expectedStatusCode = 401;
        $this->assertGeneralErrorResponse(function() {

            $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        }, $expectedStatusCode, 'HTTP APIのタイムアウト後、ログインができること');
    }


    /**
     * @test
     * @group slow
     */
    public function ユーザー登録_異常系_AMQP_AMQPメッセージタイムアウト後_NG応答あり() {
        $this->temporaryUserRegistration(TEST_DEFAULT_USER_EMAIL, null, null);
        $confirmationToken = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        $this->amqpClient->requestSleep(11);
        $this->amqpClient->requestForceNgEnabled(true);

        $expectedStatusCode = 504;
        $this->assertGeneralErrorResponse(function() use($confirmationToken){

            $registerParam = [
                'confirmation_token' => $confirmationToken['token'],
                'password' => TEST_DEFAULT_USER_PASSWORD,
                'platform' => 'none'
            ];

            $this->registerUserForTest($registerParam);

        }, $expectedStatusCode, 'AMQPメッセージがタイムアウトした場合エラーが発生する');

        sleep(4);

        //ログインが出来ないこと
        $expectedStatusCode = 401;
        $this->assertGeneralErrorResponse(function() {

            $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);

        }, $expectedStatusCode, 'HTTP APIのタイムアウト後、ログインができること');
    }


    /**
     * 試験用に、POST users/confirmation を単体で実行する
     */
    private function registerUserForTest($parameters) {

        //有効なパラメータは
        // confirmation_token
        // password
        // platform
        // token

        $response = $this->httpClient->post('/api/v3/users/confirmation', [
            'json' => $parameters
        ]);

        return $response;
    }


    private function temporaryUserRegistration($email, $platform, $platform_token) {
        $parameters = [
        ];

        if(!is_null($email)) {
            $parameters['email'] = $email;
        }
        if(!is_null($platform)) {
            $parameters['platform'] = $platform;
        }
        if(!is_null($platform_token)) {
        	$parameters['token'] = $platform_token;
        }
        $response = $this->httpClient->post('/api/v3/users', [
            'json' => $parameters
        ]);
        return $response;
    }

}
