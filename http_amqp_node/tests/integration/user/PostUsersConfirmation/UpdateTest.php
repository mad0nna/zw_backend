<?php


use GuzzleHttp\Exception\ClientException;
use Lcas\DB\DB;
use Lcas\Repository\UserRepository;
use Lcas\Test\Util\TestUtil;

require_once __DIR__ . '/../AbstractUserTestCase.php';

/**
 * POST users/confirmation の機能のうち、ユーザー情報更新適用 に関するテスト
 */
class PostUsersConfirmation_UpdateTest extends AbstractUserTestCase {

    public function setUp() {
        parent::setUp();

        //デフォルトのユーザーを登録する
        $platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
        $this->userUtil->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, $platform);
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
    }


    /**
     * @test
     */
    public function ユーザー情報更新適用_正常系_一般_email更新() {

        //メアド変更をリクエストする
        $newEmail = 'new@example.com';
        $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null);

        //トークンを調べる
        $confirmationToken = $this->findUserTokenByEmail($newEmail);

        //ユーザー情報更新を適用する
	$response = $this->updateUserForTest($confirmationToken['token'], TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //usersテーブルのemailが変更されていること
        $user = $this->userUtil->getCurrentUser();
        $this->assertEquals($newEmail, $user['email'], 'usersテーブルのメールアドレスが更新されていること');
    }


    /**
     * @test
     */
    public function ユーザー情報更新適用_異常系_一般_使用済トークンで再リクエスト() {

        //メアド変更をリクエストする
        $newEmail = 'new@example.com';
        $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null);

        //トークンを調べる
        $confirmationToken = $this->findUserTokenByEmail($newEmail);

        //ユーザー情報更新を適用する
        $this->updateUserForTest($confirmationToken['token'], TEST_DEFAULT_USER_PASSWORD, 'none');

        //同じconfirmationTokenを使用して再度リクエストを行う
        //既に使用済のトークンを使用してリクエストするとエラーが発生すること
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($confirmationToken) {

            $this->updateUserForTest($confirmationToken['token'], TEST_DEFAULT_USER_PASSWORD, 'none');

        }, $expectedStatusCode, '');

    }


    /**
     * @test
     */
    public function ユーザー情報更新適用_異常系_一般_有効期限切れのトークンを使用してリクエスト() {

        //メアド変更をリクエストする
        $newEmail = 'new@example.com';
        $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null);

        //トークンを調べる
        $confirmationToken = $this->findUserTokenByEmail($newEmail);

        //トークンを有効期限切れにする
        $this->forceExpireUserTokenByEmail($newEmail);

        //有効期限が切れたトークンでは更新ができないことを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($confirmationToken) {

            $this->updateUserForTest($confirmationToken['token'], null, null);

        }, $expectedStatusCode, '');

    }


    /**
     * @test
     */
    public function ユーザー情報更新適用_異常系_一般_存在しないトークンを使用してリクエスト() {

        //存在しないトークンを指定してリクエストするとエラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() {

            $this->updateUserForTest('invalid-token', null, null);

        }, $expectedStatusCode, '');

    }


    /**
     * @test
     * @dataProvider getDataFor_ユーザー情報更新適用_異常系_異常値
     */
    public function ユーザー情報更新適用_異常系_異常値($key, $value) {

        //メアド変更をリクエストする
        $newEmail = 'new@example.com';
        $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null);

        //トークンを調べる
        $confirmationToken = $this->findUserTokenByEmail($newEmail);

        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($confirmationToken, $key, $value) {

            //リクエストの一部を異常値に置き換えてAPIを実行する。
            $params = [
                'confirmation_token' => $confirmationToken['token'],
                'platform' => 'none',
                'password' => TEST_DEFAULT_USER_PASSWORD,
            ];
            $params[$key] = $value;

            $this->updateUserForTest($params['confirmation_token'], $params['password'], $params['platform']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    public function getDataFor_ユーザー情報更新適用_異常系_異常値() {
        return [
            'confirmation_tokenが空文字'      => ['confirmation_token', ''],
            'platformが無効な値'              => ['platform', 'invalid-platform'],
        ];
    }


    /**
     * @test
     * @dataProvider getDataFor_ユーザー情報更新適用_異常系_必須項目省略
     */
    public function ユーザー情報更新適用_異常系_必須項目省略($key) {

        //メアド変更をリクエストする
        $newEmail = 'new@example.com';
        $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null);

        //トークンを調べる
        $confirmationToken = $this->findUserTokenByEmail($newEmail);

        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($confirmationToken, $key) {

            $params = [
                'confirmation_token' => $confirmationToken['token'],
                'platform' => 'gcm',
                'password' => TEST_DEFAULT_USER_PASSWORD,
                'token'    => 'abcdefghijkl',
            ];
            //一部のパラメータを省略してAPIを実行する。
            $params[$key] = null;

            $this->updateUserForTest($params['confirmation_token'], $params['password'], $params['platform'], $params['token']);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');

    }


    public function getDataFor_ユーザー情報更新適用_異常系_必須項目省略() {
        return [
            'confirmation_token' => ['confirmation_token'],
//            'platform'           => ['platform'],
            'password'           => ['password'],
            'token'              => ['token'],
        ];
    }


    /**
     * @test
     */
    public function ユーザー情報更新適用_異常系_AMQP_AMQPメッセージにてNG応答あり() {

        //メアド変更をリクエストする
        $newEmail = 'new@example.com';
        $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null);

        //トークンを調べる
        $confirmationToken = $this->findUserTokenByEmail($newEmail);

        //AMQPメッセージがNG応答するように設定する
        $this->amqpClient->requestForceNgEnabled(true);

        //AMQPメッセージでNG応答があると、エラーが発生することを確認する
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($confirmationToken){

            $this->updateUserForTest($confirmationToken['token'], null, null);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }




    /**
     * @param $email
     * @param $password
     * @param $platform
     * @param $token
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function temporaryUserUpdate($email, $password, $oldPassword, $platform, $token) {
        $parameters = [];
        if(!is_null($email)) {
            $parameters['email'] = $email;
        }
        if(!is_null($password)) {
            $parameters['password'] = $password;
        }
        if(!is_null($oldPassword)) {
            $parameters['old_password'] = $oldPassword;
        }
        if(!is_null($platform)) {
            $parameters['platform'] = $platform;
        }
        if(!is_null($token)) {
            $parameters['token'] = $token;
        }

        return $this->httpClient->put('/api/v3/users/self', ['body' => json_encode($parameters)]);
    }


    private function updateUserForTest($confirmationToken, $password, $platform, $token=null) {
        $parameters = [];
        if(!is_null($confirmationToken)) {
            $parameters['confirmation_token'] = $confirmationToken;
        }
        if(!is_null($password)) {
            $parameters['password'] = $password;
        }
        if(!is_null($platform)) {
            $parameters['platform'] = $platform;
        }
        if(!is_null($token)) {
            $parameters['token'] = $token;
        }

        return $this->httpClient->post('/api/v3/users/confirmation', ['body' => json_encode($parameters)]);
    }



}
