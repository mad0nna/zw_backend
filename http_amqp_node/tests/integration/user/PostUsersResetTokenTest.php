<?php


use Lcas\Repository\UserRepository;

require_once __DIR__ . '/AbstractUserTestCase.php';

class PostUsersResetTokenTest extends AbstractUserTestCase {

    public function setUp() {
        parent::setUp();

        //デフォルトのユーザーを登録する
        $platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
        $this->userUtil->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, $platform);
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
    }


    /**
     * @test
     */
    public function パスワードリセットの要求_正常系_一般_パスワードをリセット() {

        $response = $this->requestPasswordReset(TEST_DEFAULT_USER_EMAIL);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        $userTokenRecord = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);
        $this->assertEquals(TEST_DEFAULT_USER_EMAIL, $userTokenRecord['email'], 'user_tokensテーブルに指定したレコードが記録されていること');
    }


    /**
     * @test
     */
    public function パスワードリセットの要求_異常系_一般_存在しないメールアドレスを使用() {

        $expectedStatusCode = 404;
        $this->assertGeneralErrorResponse(function(){

            $this->requestPasswordReset('unknown@example.com');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function パスワードリセットの要求_異常系_必須項目省略_email() {

        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function(){

            $this->requestPasswordReset(null);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    private function requestPasswordReset($email) {
        $parameters = [
        ];

        if(!is_null($email)) {
            $parameters['email'] = $email;
        }
        $response = $this->httpClient->post('/api/v3/users/resettoken', [
            'json' => $parameters
        ]);
        return $response;

    }

}
