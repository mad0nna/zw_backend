<?php


use Lcas\DB\DB;
use Lcas\Repository\UserRepository;

require_once __DIR__ . '/AbstractUserTestCase.php';

class PostUsersTest extends AbstractUserTestCase {

    /**
     * @test
     */
    public function ユーザー仮登録_正常系_一般_仮登録成功() {
        $email = 'test@example.com';
        $platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
        $response = $this->temporaryUserRegistration($email);

        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        $userToken = $this->findUserTokenByEmail($email);

        $this->assertEquals($email, $userToken['email'], 'user_tokensテーブルに指定したメールアドレスのレコードが存在すること');
    }


    /**
     * @test
     */
    public function ユーザー仮登録_正常系_一般_一定時間置いて同じメールアドレスで登録() {
        $email = 'test@example.com';
        $this->temporaryUserRegistration($email);
        $oldUserTokenRecord = $this->findUserTokenByEmail($email);

        //同じメールアドレスでの仮登録が受け付けられるよう、
        //トークンの作成時刻を現在時刻から30分前に変更する。
        $newCreatedTime = date('Y-m-d H:i:s', strtotime('-30 min -1 seconds'));
        $this->forceModifyUserTokenCreatedTimeByEmail($email, $newCreatedTime);

        //同じメールアドレスで2回目のリクエストを行う。
        $response = $this->temporaryUserRegistration($email);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        $userTokenRecord = $this->findUserTokenByEmail($email);

        //2回目のリクエストにより、expiresが新しくなっていること
        $this->assertNotEquals($userTokenRecord['token'], $oldUserTokenRecord['token'], 'トークンが更新されていること');
    }
    
    /**
     * @test
     * @dataProvider getDataFor_ユーザー仮登録_正常系_一般_OriginとPlatformの指定
     */
    public function ユーザー仮登録_正常系_一般_OriginとPlatformの指定($platform) {
    	
    	$this->registMailTemplate(
    			'http://test.example.com',
    			PLATFORM_NONE,
    			MAIL_TEMPLATE_TYPE_NEW_USER,
    			'test@example.com',
    			'platform:none,type:new,from:test@example.com',
    			'content');

    	$this->registMailTemplate(
    			'http://www.example.com',
    			0,
    			MAIL_TEMPLATE_TYPE_NEW_USER,
    			'www@example.com',
    			'platform:default,type:new,from:www@example.com',
    			'content');

    	$this->registMailTemplate(
    			'http://www.example.com',
    			PLATFORM_NONE,
    			MAIL_TEMPLATE_TYPE_NEW_USER,
    			'www@example.com',
    			'platform:none,type:new,from:www@example.com',
    			'content');
    	
    	$this->registMailTemplate(
    			'http://www.example.com',
    			PLATFORM_NONE,
    			MAIL_TEMPLATE_TYPE_RESET_PASSWORD,
    			'www@example.com',
    			'platform:none,type:reset,from:www@example.com',
    			'content');
    	
    	$this->registMailTemplate(
    			'http://www.example.com',
    			PLATFORM_NONE,
    			MAIL_TEMPLATE_TYPE_CHANGE_EMAIL,
    			'www@example.com',
    			'platform:none,type:update,from:www@example.com',
    			'content');
    	
    	$email = 'test@example.com';
    	$platform = UserRepository::getPlatformNameFromType($platform);
    	$response = $this->temporaryUserRegistrationWithOrigin($email, 'http://www.example.com');
    	
    	$this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
    	
    	$userToken = $this->findUserTokenByEmail($email);
    	
    	$this->assertEquals($email, $userToken['email'], 'user_tokensテーブルに指定したメールアドレスのレコードが存在すること');
    }

    public function getDataFor_ユーザー仮登録_正常系_一般_OriginとPlatformの指定() {
    	return [
    			'gcm'  => [PLATFORM_GCM],
    			'apns' => [PLATFORM_APNS],
    			'sqs' => [PLATFORM_SQS],
    			'none' => [PLATFORM_NONE]
    	];
    }
    
    
    /**
     * @test
     */
    public function ユーザー仮登録_異常系_一般_email_正規ユーザーと重複() {
        $email = TEST_DEFAULT_USER_EMAIL;
        $this->userUtil->registerUser($email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $oldUserToken = $this->findUserTokenByEmail($email);

        //本登録済ユーザーと同じメールアドレスで再度登録を行う。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($email){

            $this->temporaryUserRegistration($email);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ユーザー仮登録_異常系_一般_email_仮登録中ユーザーと重複() {
        $email = 'test@example.com';
        $this->temporaryUserRegistration($email);

        //仮登録中ユーザーと同じメールアドレスで再度登録を行う。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($email){

            $this->temporaryUserRegistration($email);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ユーザー仮登録_異常系_異常値_email_空文字() {
        //空のメールアドレスで登録するとエラーになることを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function(){

            $email = '';
            $this->temporaryUserRegistration($email);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ユーザー仮登録_異常系_必須項目省略_email() {
        //emailパラメータを省略するとエラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function(){

            $email = null;
            $this->temporaryUserRegistration($email);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    private function temporaryUserRegistration($email) {
        $parameters = [
        ];

        if(!is_null($email)) {
            $parameters['email'] = $email;
        }
        $response = $this->httpClient->post('/api/v3/users', [
            'json' => $parameters
        ]);
        return $response;
    }


    private function temporaryUserRegistrationWithOrigin($email, $origin) {
        $parameters = [
        ];

        if(!is_null($email)) {
            $parameters['email'] = $email;
        }
        $response = $this->httpClient->post('/api/v3/users', [
            'json' => $parameters,
        	'headers' => [
        			'origin' => $origin
        	]
        ]);
        return $response;
    }


    private function forceModifyUserTokenCreatedTimeByEmail($email, $newCreatedTime) {
        $sql = "UPDATE user_tokens SET created='{$newCreatedTime}' WHERE email='{$email}'";
        $db = DB::getMasterDb();

        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception('SQLエラー： ' . $db->getErr() . 'SQL: ' . $sql);
        }
    }


}
