<?php


use Lcas\Repository\UserRepository;

require_once __DIR__ . '/../AbstractUserTestCase.php';


/**
 * PUT users/self のうち、パスワードリセット実行についての試験を定義する。
 */
class PutUsersSelf_ResetPasswordTest extends AbstractUserTestCase {

    public function setUp() {
        parent::setUp();

        //デフォルトのユーザーを登録する
        $platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
        $this->userUtil->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, $platform);
        //このテストではユーザーの登録のみ行い、ログインは行いません。
    }


    /**
     * @test
     */
    public function パスワードリセット実行_正常系_一般_成功() {

        //変更前のパスワードのハッシュを取得する。
        $originalUser = $this->getUserRecordByEmail(TEST_DEFAULT_USER_EMAIL);
        $originalPassword = $originalUser['pass'];

        //リセットのリクエスト
        $this->requestPasswordReset(TEST_DEFAULT_USER_EMAIL);

        //reset_tokenを調べる
        $resetTokenRecord = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        //リセットの実行
        $newPassword = 'new-password';
        $response = $this->resetPassword($resetTokenRecord['token'], $newPassword);
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //パスワード(hash)が変化していること
        $user = $this->getUserRecordByEmail(TEST_DEFAULT_USER_EMAIL);
        $this->assertNotEquals($user['pass'], $originalPassword, 'usersテーブルのパスワードが更新されていること');
    }

    /**
     * @test
     */
    public function パスワードリセット実行_正常系_一般_Origin指定() {
		$this->registMailTemplate('http://test.example.com', PLATFORM_NONE, MAIL_TEMPLATE_TYPE_NEW_USER, 'test@example.com', 'platform:none,type:new,from:test@example.com', 'content');
		
		$this->registMailTemplate('http://www.example.com', 0, MAIL_TEMPLATE_TYPE_NEW_USER, 'www@example.com', 'platform:default,type:new,from:www@example.com', 'content');
		
		$this->registMailTemplate('http://www.example.com', PLATFORM_NONE, MAIL_TEMPLATE_TYPE_NEW_USER, 'www@example.com', 'platform:none,type:new,from:www@example.com', 'content');
		
		$this->registMailTemplate('http://www.example.com', PLATFORM_NONE, MAIL_TEMPLATE_TYPE_RESET_PASSWORD, 'www@example.com', 'platform:none,type:reset,from:www@example.com', 'content');
		
		$this->registMailTemplate('http://www.example.com', PLATFORM_NONE, MAIL_TEMPLATE_TYPE_CHANGE_EMAIL, 'www@example.com', 'platform:none,type:update,from:www@example.com', 'content');
		
        $email = 'abcdefg@example.com';
		
        $platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
        $this->userUtil->registerUser($email, '!abcdefg!', $platform, 'test-token');
		
		$platform = UserRepository::getPlatformNameFromType($platform);
		$response = $this->requestPasswordResetWithOrigin($email, 'http://www.example.com');
		
		$this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');
		
		$userToken = $this->findUserTokenByEmail($email);
		
		$this->assertEquals($email, $userToken['email'], 'user_tokensテーブルに指定したメールアドレスのレコードが存在すること');
    }
   
    /**
     * @test
     */
    public function パスワードリセット実行_正常系_一般_セッションCookieとreset_tokenの両方を指定() {

        //デフォルトとは別のユーザーを登録してログインする。
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //変更前のパスワードのハッシュを取得する。
        $originalUser = $this->getUserRecordByEmail(TEST_DEFAULT_USER_EMAIL);
        $originalPassword = $originalUser['pass'];

        //リセットのリクエスト
        $this->requestPasswordReset(TEST_DEFAULT_USER_EMAIL);

        //reset_tokenを調べる
        $resetTokenRecord = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        //リセットの実行
        //別のユーザーのセッションIDがCookieに含まれている状態でも、
        //reset_tokenの値に応じてパスワードが更新されることを確認する。
        $response = $this->resetPassword($resetTokenRecord['token'], 'new-password');
        $this->assertEquals(204, $response->getStatusCode(), 'ステータスコードが204であること');

        //デフォルトユーザーのパスワード(hash)が変化していることを確認する。
        $user = $this->getUserRecordByEmail(TEST_DEFAULT_USER_EMAIL);
        $this->assertNotEquals($user['pass'], $originalPassword, 'usersテーブルのパスワードが更新されていること');
    }


    /**
     * @test
     */
    public function パスワードリセット実行_異常系_一般_使用済のトークンを使用() {

        //リセットのリクエスト
        $this->requestPasswordReset(TEST_DEFAULT_USER_EMAIL);

        //reset_tokenを調べる
        $originalResetTokenRecord = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);
        $originalResetToken = $originalResetTokenRecord['token'];

        //リセットの実行
        $this->resetPassword($originalResetToken, 'new-password');

        //同じトークンを指定して再度リクエストすると、
        //エラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($originalResetToken){

            $this->resetPassword($originalResetToken, 'new-password');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function パスワードリセット実行_異常系_一般_有効期限切れのトークンを使用() {

        //リセットのリクエスト
        $this->requestPasswordReset(TEST_DEFAULT_USER_EMAIL);

        //reset_tokenを調べる
        $originalResetTokenRecord = $this->findUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);
        $originalResetToken = $originalResetTokenRecord['token'];

        //トークンを有効期限切れにする
        $this->forceExpireUserTokenByEmail(TEST_DEFAULT_USER_EMAIL);

        //有効期限切れのトークンを使用してリクエストを実行すると、
        //エラーが発生することを確認する。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($originalResetToken){

            $this->resetPassword($originalResetToken, 'new-password');

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function パスワードリセット実行_異常系_必須項目省略() {

        //特定のパラメータを省略した状態でリクエストを実行すると、
        //エラーが発生することを確認する。
	try {
		$this->resetPassword(null, 'new-password');
		$this->assertTrue(false, 'ステータスコードが401であること');
	} catch (Exception $e) {
		$this->assertEquals(401, $e->getCode(), 'ステータスコードが401であること');
    }
    }


    private function requestPasswordReset($email) {
        $parameters = [
        ];

        if(!is_null($email)) {
            $parameters['email'] = $email;
        }
        $response = $this->httpClient->post('/api/v3/users/resettoken', [
            'json' => $parameters
        ]);
        return $response;

    }


    private function requestPasswordResetWithOrigin($email, $origin) {
        $parameters = [
        ];

        if(!is_null($email)) {
            $parameters['email'] = $email;
        }
        $response = $this->httpClient->post('/api/v3/users/resettoken', [
            'json' => $parameters,
        	'headers' => [
        		'origin' => $origin
        	]
        ]);
        return $response;

    }


    private function resetPassword($resetTokenRecord, $password) {
        $parameters = [
        ];

        if(!is_null($resetTokenRecord)) {
            $parameters['reset_token'] = $resetTokenRecord;
        }
        if(!is_null($password)) {
            $parameters['password'] = $password;
        }
        $response = $this->httpClient->put('/api/v3/users/self', [
            'json' => $parameters
        ]);
        return $response;
    }
}
