<?php


use Lcas\DB\DB;
use Lcas\Repository\UserRepository;

require_once __DIR__ . '/../AbstractUserTestCase.php';

/**
 * PUT users/self のうち、会員情報変更申請についての試験を定義する。
 */
class PutUsersSelf_UpdateTest extends AbstractUserTestCase {

    public function setUp() {
        parent::setUp();

        //デフォルトのユーザーを登録する
        $platform = UserRepository::getPlatformNameFromType(PLATFORM_NONE);
        $this->userUtil->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, $platform);
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
    }


    /**
     * @test
     */
    public function ユーザー情報更新申請_正常系_一般_email更新() {
        $newEmail = 'new@example.com';
        $requestResult = $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null, null);
        $this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');

        //user_tokensテーブルにレコードが存在すること
        $temporaryRequest = $this->findUserTokenByEmail($newEmail);

        $this->assertEquals($newEmail, $temporaryRequest['email'], 'user_tokensテーブルにレコードが存在すること');
    }

    /**
     * @test
     */
    public function ユーザー情報更新申請_正常系_一般_email更新_Origin指定() {
		$this->registMailTemplate('http://test.example.com', PLATFORM_NONE, MAIL_TEMPLATE_TYPE_NEW_USER, 'test@example.com', 'platform:none,type:new,from:test@example.com', 'content');
		
		$this->registMailTemplate('http://www.example.com', 0, MAIL_TEMPLATE_TYPE_NEW_USER, 'www@example.com', 'platform:default,type:new,from:www@example.com', 'content');
		
		$this->registMailTemplate('http://www.example.com', PLATFORM_NONE, MAIL_TEMPLATE_TYPE_NEW_USER, 'www@example.com', 'platform:none,type:new,from:www@example.com', 'content');
		
		$this->registMailTemplate('http://www.example.com', PLATFORM_NONE, MAIL_TEMPLATE_TYPE_RESET_PASSWORD, 'www@example.com', 'platform:none,type:reset,from:www@example.com', 'content');
		
		$this->registMailTemplate('http://www.example.com', PLATFORM_NONE, MAIL_TEMPLATE_TYPE_CHANGE_EMAIL, 'www@example.com', 'platform:none,type:update,from:www@example.com', 'content');
    	
		$newEmail = 'new@example.com';
        $requestResult = $this->temporaryUserUpdateWithOrigin($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null, null, 'http://www.example.com');
        $this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');

        //user_tokensテーブルにレコードが存在すること
        $temporaryRequest = $this->findUserTokenByEmail($newEmail);

        $this->assertEquals($newEmail, $temporaryRequest['email'], 'user_tokensテーブルにレコードが存在すること');
    }

    /**
     * @test
     */
    public function ユーザー情報更新申請_正常系_一般_一定時間置いて同じメールアドレスで更新() {
        $newEmail = 'new@example.com';
        $requestResult = $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null, null);
        $this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');

        //初回のリクエストで発行されたconfirmationTokenを確認する。
        $initialRequest = $this->findUserTokenByEmail($newEmail);
        $initialConfirmationToken = $initialRequest['token'];

        //同一メールアドレスでの再申請が可能になるよう、
        //トークンの作成時刻を30分前に設定する
        $newCreatedTime = date('Y-m-d H:i:s', strtotime('-30 min -1 seconds'));
        $this->forceModifyUserTokenCreatedTimeByEmail($newEmail, $newCreatedTime);

        //再び同じメールアドレスに変更する要求を行う。
        $requestResult = $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null, null);

        $this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');
        $secondRequest = $this->findUserTokenByEmail($newEmail);
        $secondConfirmationToken = $secondRequest['token'];

        //2回目のリクエストにより、confirmationTokenが更新されることを確認する。
        $this->assertNotEquals($initialConfirmationToken, $secondConfirmationToken, 'confirmation_tokenが更新されていること');
    }


    /**
     * @test
     */
    public function ユーザー情報更新申請_正常系_一般_password更新() {

        $oldUser = $this->getUserRecordByEmail(TEST_DEFAULT_USER_EMAIL);
        $oldHashedPassword = $oldUser['pass'];

        $newPassword = 'new_password';
        $requestResult = $this->temporaryUserUpdate(null, $newPassword, TEST_DEFAULT_USER_PASSWORD, null, null, null);
        $this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');

        //usersテーブルのパスワードが更新されていること
        $user = $this->getUserRecordByEmail(TEST_DEFAULT_USER_EMAIL);
        $newHashedPassword = $user['pass'];

        $this->assertNotEquals($oldHashedPassword, $newHashedPassword, 'userテーブルのパスワードが更新されていること');
    }


    /**
     * @test
     */
    public function ユーザー情報更新申請_正常系_一般_platform_token更新() {

        $oldUser = $this->getUserRecordByEmail(TEST_DEFAULT_USER_EMAIL);
        $oldHashedPassword = $oldUser['pass'];

        $expectedPlatform = 'gcm';
        $expectedToken = 'new_token';
        $requestResult = $this->temporaryUserUpdate(null, null, TEST_DEFAULT_USER_PASSWORD, $expectedPlatform, $expectedToken, null);
        $this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');

        //usersテーブルのplatform、デバイストークンが更新されていること
        $user = $this->getUserRecordByEmail(TEST_DEFAULT_USER_EMAIL);
        $newPlatform = UserRepository::getPlatformNameFromType($user['platform']);
        $newToken = $user['token'];

        $this->assertEquals($expectedPlatform, $newPlatform, 'userテーブルのplatformが更新されていること');
        $this->assertEquals($expectedToken, $newToken, 'userテーブルのデバイストークンが更新されていること');
    }

    
    /**
     * @test
     */
    public function ユーザー情報更新申請_正常系_一般_ラベル更新() {
    
    	$expectedLabels = array('ラベル1', 'ラベル2', 'ラベル3');
    	$requestResult = $this->temporaryUserUpdate(null, null, null, null, null, $expectedLabels);
    	$this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');
    
    	// labels テーブルの label が更新されていること
    	$response = $this->temporaryUserInfo();
    	$user = json_decode($response->getBody(), true);
    	$this->assertEquals($expectedLabels, $user['labels'], 'labels テーブルの  label が更新されていること');
    }
    
    /**
     * @test
     */
    public function ユーザー情報更新申請_正常系_一般_ラベル更新をしない() {
    
    	// ラベルを更新
    	$expectedLabels = array('ラベル1', 'ラベル2', 'ラベル3');
    	$requestResult = $this->temporaryUserUpdate(null, null, null, null, null, $expectedLabels);
    	$this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');
    
    	// labels テーブルの label が更新されていること
    	$response = $this->temporaryUserInfo();
    	$user = json_decode($response->getBody(), true);
    	$this->assertEquals($expectedLabels, $user['labels'], 'labels テーブルの  label が更新されていること');
    	
    	// ラベル以外を更新
    	$requestResult = $this->temporaryUserUpdate(null, null, TEST_DEFAULT_USER_PASSWORD, 'none', null, null);
    	$this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');
    
    	// labels テーブルの label が更新されていないこと（以前の期待値と一致していること）
    	$response = $this->temporaryUserInfo();
    	$user = json_decode($response->getBody(), true);
    	$this->assertEquals($expectedLabels, $user['labels'], 'labels テーブルの  label が更新されていないこと');
    }
    
    
    /**
     * @test
     */
    public function ユーザー情報更新申請_正常系_一般_ラベルをクリア() {
    
    	// ラベルを更新
    	$expectedLabels = array('ラベル1', 'ラベル2', 'ラベル3');
    	$requestResult = $this->temporaryUserUpdate(null, null, null, null, null, $expectedLabels);
    	$this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');
    
    	// labels テーブルの label が更新されていること
    	$response = $this->temporaryUserInfo();
    	$user = json_decode($response->getBody(), true);
    	$this->assertEquals($expectedLabels, $user['labels'], 'labels テーブルの  label が更新されていること');
    	
    	// ラベルに空の配列を指定する
    	$requestResult = $this->temporaryUserUpdate(null, null, TEST_DEFAULT_USER_PASSWORD, 'none', null, array());
    	$this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');
    
    	// labels テーブルの label がクリアされていること
    	$response = $this->temporaryUserInfo();
    	$user = json_decode($response->getBody(), true);
    	$this->assertEquals(array(), $user['labels'], 'labels テーブルの  label がクリアされていること');
    }
    
    
    /**
     * @test
     */
    public function ユーザー情報更新申請_正常系_一般_email_password_platform_token_全て更新() {

        $oldUser = $this->getUserRecordByEmail(TEST_DEFAULT_USER_EMAIL);

        $expectedEmail = 'new@example.com';
        $newPassword = 'new_password';
        $expectedPlatform = 'gcm';
        $expectedToken = 'new_token';
        $requestResult = $this->temporaryUserUpdate($expectedEmail, $newPassword, TEST_DEFAULT_USER_PASSWORD, $expectedPlatform, $expectedToken, null);
        $this->assertEquals(204, $requestResult->getStatusCode(), 'ステータスコードが204であること');

        //usersテーブルのplatform, デバイストークンが更新されていること
        $user = $this->getUserRecordByEmail(TEST_DEFAULT_USER_EMAIL);
        $newPlatform = UserRepository::getPlatformNameFromType($user['platform']);
        $newToken = $user['token'];

        $this->assertNotEquals($expectedPlatform, $newPlatform, 'userテーブルのplatformが更新されていないこと');
        $this->assertNotEquals($expectedToken, $newToken, 'userテーブルのデバイストークンが更新されていないこと');

        //メールアドレスはuser_tokensテーブルに記録されていること
        $temporaryRequest = $this->findUserTokenByEmail($expectedEmail);
        $this->assertEquals($expectedEmail, $temporaryRequest['email'], 'user_tokensテーブルにレコードが存在すること');
    }
    
    
    /**
     * @test
     */
    public function ユーザー情報更新申請_異常系_一般_email_正規ユーザーと重複() {
        //新しいユーザーを登録する
        $newEmail = 'new@example.com';
        $this->userUtil->registerUser($newEmail, TEST_DEFAULT_USER_PASSWORD, 'none');

        //新しいユーザーと同じメールアドレスに更新しようとする
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($newEmail) {

            $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null, null);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     */
    public function ユーザー情報更新申請_異常系_一般_email_メールアドレス変更申請中ユーザーと重複() {
        //メールアドレスの変更申請を行う
        $newEmail = 'new@example.com';
        $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null, null);

        //申請中のメールアドレスと同じアドレスに変更しようとする。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($newEmail) {

            $this->temporaryUserUpdate($newEmail, null, TEST_DEFAULT_USER_PASSWORD, null, null, null);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    /**
     * @test
     * @dataProvider getDataFor_ユーザー情報更新申請_異常系_異常値
     */
    public function ユーザー情報更新申請_異常系_異常値($key, $value) {
        //メールアドレスの変更申請を行う
        $params = [
            'email'    => TEST_DEFAULT_USER_EMAIL,
            'password' => 'test1234',
            'old_password' => TEST_DEFAULT_USER_PASSWORD,
            'platform' => 'gcm',
            'token'    => 'token1234',
        ];
        $params[$key] = $value;

        //申請中のメールアドレスと同じアドレスに変更しようとする。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($params) {

            $this->temporaryUserUpdate($params['email'], $params['password'], $params['old_password'], $params['platform'], $params['token'], null);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    public function getDataFor_ユーザー情報更新申請_異常系_異常値() {
        return [
            'password_英字のみ' => ['password', 'abcdefghij'],
            'password_数字のみ' => ['password', '1234567890'],
            'password_記号のみ' => ['password', "@%+\/’!#$^?:,(){}[]~-_"],
            'password_使用不可な文字の使用' => ['password', '=========='],
            'password_長さ不足' => ['password', 'abc12'],
            'password_長さ超過' => ['password', 'test1234567890123'],
            'password_空文字'   => ['password', ''],
            'platform_無効なplatform' => ['platform', 'invalid'],
        ];
    }

    /**
     * @test
     */
    public function ユーザー情報更新申請_異常系_パスワード不一致() {
        //メールアドレスの変更申請を行う
        $params = [
            'email'    => TEST_DEFAULT_USER_EMAIL,
            'password' => 'test1234',
            'old_password' => 'invalid',
            'platform' => 'gcm',
            'token'    => 'token1234',
        ];

        //申請中のメールアドレスと同じアドレスに変更しようとする。
        $expectedStatusCode = 401;
        $this->assertGeneralErrorResponse(function() use($params) {

            $this->temporaryUserUpdate($params['email'], $params['password'], $params['old_password'], $params['platform'], $params['token'], null);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }



    /**
     * @test
     * @dataProvider getDataFor_ユーザー情報更新申請_異常系_必須項目省略
     */
    public function ユーザー情報更新申請_異常系_必須項目省略($key) {
        //メールアドレスの変更申請を行う
        $params = [
            'email'    => 'new@example.com',
            'password' => 'test1234',
            'old_password' => TEST_DEFAULT_USER_PASSWORD,
            'platform' => 'gcm',
            'token'    => 'token1234',
        ];
        $params[$key] = null;

        //申請中のメールアドレスと同じアドレスに変更しようとする。
        $expectedStatusCode = 400;
        $this->assertGeneralErrorResponse(function() use($params) {

            $this->temporaryUserUpdate($params['email'], $params['password'], $params['old_password'], $params['platform'], $params['token'], null);

        }, $expectedStatusCode, '有効なエラーレスポンスであること');
    }


    public function getDataFor_ユーザー情報更新申請_異常系_必須項目省略() {
        return [
            'token' => ['token'],
            'old_password' => ['old_password'],
        ];
    }




    /**
     * @param $email
     * @param $password
     * @param $platform
     * @param $token
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function temporaryUserUpdate($email, $password, $oldPassword, $platform, $token, $labels) {
        $parameters = [];
        if(!is_null($email)) {
            $parameters['email'] = $email;
        }
        if(!is_null($password)) {
            $parameters['password'] = $password;
        }
        if(!is_null($oldPassword)) {
            $parameters['old_password'] = $oldPassword;
        }
        if(!is_null($platform)) {
            $parameters['platform'] = $platform;
        }
        if(!is_null($token)) {
            $parameters['token'] = $token;
        }
        if(!is_null($labels) && is_array($labels)) {
        	$parameters['labels'] = $labels;
        }
        
        return $this->httpClient->put('/api/v3/users/self', ['body' => json_encode($parameters)]);
    }

    /**
     * @param $email
     * @param $password
     * @param $platform
     * @param $token
     * @param $origin
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function temporaryUserUpdateWithOrigin($email, $password, $oldPassword, $platform, $token, $labels, $origin) {
        $parameters = [];
        if(!is_null($email)) {
            $parameters['email'] = $email;
        }
        if(!is_null($password)) {
            $parameters['password'] = $password;
        }
        if(!is_null($oldPassword)) {
            $parameters['old_password'] = $oldPassword;
        }
        if(!is_null($platform)) {
            $parameters['platform'] = $platform;
        }
        if(!is_null($token)) {
            $parameters['token'] = $token;
        }
        if(!is_null($labels) && is_array($labels)) {
        	$parameters['labels'] = $labels;
        }
        
        return $this->httpClient->put('/api/v3/users/self', [
        		'body' => json_encode($parameters),
        		'headers' => [
        				'origin' => $origin
        		]
        ]);
    }

    private function temporaryUserInfo() {
        return $this->httpClient->get('/api/v3/users/self');
    }

    private function forceModifyUserTokenCreatedTimeByEmail($email, $newCreatedTime) {
        $sql = "UPDATE user_tokens SET created='{$newCreatedTime}' WHERE email='{$email}'";
        $db = DB::getMasterDb();

        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception('SQLエラー： ' . $db->getErr() . 'SQL: ' . $sql);
        }
    }


}
