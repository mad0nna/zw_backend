<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use GuzzleHttp\Psr7\Response;
use Lcas\DB\DB;
use Lcas\Test\LcasSocketIoClient;
use Lcas\Test\Util\TestUtil;
use WebSocket\Client as WSClient;

abstract class AbstractSocketIoTestCase extends \Lcas\Test\IntegrationTestCase {

    /**
     * @var LcasSocketIoClient
     */
    protected $socketIoClient;

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->socketIoClient = new LcasSocketIoClient();
    }


    public function setUp() {
        parent::setUp();

        //テストデータを全削除
        TestUtil::clearDatabase();

        $this->userUtil->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, 'none');
        $response = $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        $defaultSessionId = $this->userUtil->getAuthTokenFromEmail(TEST_DEFAULT_USER_EMAIL);

        $this->socketIoClient->setOrigin(null);
        $this->socketIoClient->setAuthToken($defaultSessionId);
    }


    protected function sendNodeInclusionNotification($gatewayMacAddress, $nodeData) {
        $user = $this->userUtil->getCurrentUser();

        $request = [
            'command' => 'node_incl',
            'result_code' => 200,
            'username' => $user['login_id'],
            'gw_name' => $gatewayMacAddress,
            'node_id' => $nodeData['node_id'],
            'manufacturer' => $nodeData['manufacturer'],
            'product' => $nodeData['product'],
            'serial_no' => $nodeData['serial_no'],
            'actions' => $nodeData['actions'],
            'devices' => $nodeData['devices'],
            'state' => isset($nodeData['state']) ? $nodeData['state'] : null
        ];
        $request = array_merge($request, $nodeData);
        $this->amqpClient->sendMessageToLcas($request);

    }


    protected function sendNodeExclusionNotification($gatewayMacAddress, $nodeId) {
        $user = $this->userUtil->getCurrentUser();

        $request = [
            'command' => 'node_excl',
            'result_code' => 200,
            'username' => $user['login_id'],
            'gw_name' => $gatewayMacAddress,
            'node_id' => $nodeId,
        ];
        $this->amqpClient->sendMessageToLcas($request);
    }



}
