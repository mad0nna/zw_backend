<?php

use GuzzleHttp\Psr7\Response;
use Lcas\Test\LcasSocketIoClient;
use Lcas\Test\LcasWebSocketClient;
use Lcas\Test\Util\TestUtil;
use WebSocket\Client as WSClient;

abstract class AbstractWebSocketTestCase extends \Lcas\Test\IntegrationTestCase {

    /**
     * @var LcasWebSocketClient
     */
    protected $webSocketClient;

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->webSocketClient = new LcasWebSocketClient();

    }


    public function setUp() {
        parent::setUp();

        //テストデータを全削除
        TestUtil::clearDatabase();

        $this->userUtil->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
        $defaultSessionId = $this->userUtil->getAuthTokenFromEmail(TEST_DEFAULT_USER_EMAIL);

        $this->webSocketClient->setOrigin(null);
        $this->webSocketClient->setAuthToken($defaultSessionId);
    }


}
