<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use Lcas\DB\DB;
use Lcas\Repository\DeviceRepository;
use Lcas\Test\Constraint\WebSocket\DeviceDataUpdateConstraint;
use Lcas\Test\Constraint\WebSocket\NodeInclusionConstraint;
use Lcas\Test\Fixture\GatewayFixtureFactory;
use Lcas\Test\Fixture\NodeFixtureFactory;
use Lcas\Test\Util\TestUtil;
use WebSocket\Client as WSClient;

require_once __DIR__ . '/AbstractSocketIoTestCase.php';

class DeviceDataUpdateTest extends AbstractSocketIoTestCase {

    /**
     * @dataProvider getDataFor_Device_Data_Update_デバイス種別
     * @test
     */
    public function Device_Data_Update_デバイス種別($deviceType, $value, $unit){

        //デバイスデータのテスト用ゲートウェイを割り当てる
        //(1つのノードの中に全てのdevice_typeを含んでいる)
        $macAddress = 'A00000003000';
        $this->gatewayUtil->registerGateway($macAddress);

        sleep(4);
        
        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create($macAddress);
        $node = $gatewayData['nodes'][0];

        //Gateway割り当て直後のNode Inclusionを受信することを避けるため、一定時間sleepする
        sleep(4);

        //WebSocket接続する
        $this->socketIoClient->connect();

        $timestampInMilliSec = (int)(microtime(true) * 1000);
        $this->sendDeviceDataNotification($node['node_id'], $deviceType, $value, $unit, $timestampInMilliSec);

        $notification = $this->socketIoClient->waitNotification();

        $deviceId = $this->getDeviceIdFromDeviceType($node['node_id'], $deviceType);
        $expectedDeviceValue = [
            'id' => $deviceId,
            'device_type' => $deviceType,
            'value' => $value,
            'unit' => $unit,
            'timestamp' => $timestampInMilliSec
        ];
        $deviceDataUpdateConstraint = new DeviceDataUpdateConstraint($expectedDeviceValue);

        $this->assertThat($notification, $deviceDataUpdateConstraint, '想定されるDevice Data Updateの情報を含んでいること');
    }


    public function getDataFor_Device_Data_Update_デバイス種別(){

        return [
            'temperature'  => ['temperature', '10.5', 'C', ],
            'humidity'     => ['humidity', '11.1', '%'],
            'motion'       => ['motion', '255', null],
            'open_close'   => ['open_close', '0', null],
            'lock'         => ['lock', '255', null],
            'cover'        => ['cover', '0', null],
            'battery'      => ['battery', '50', '%'],
            'luminance'    => ['luminance', '60.1', 'lux'],
            'call_button'  => ['call_button', '255', null],
            'smart_lock'   => ['smart_lock', '0', null],
            'power'        => ['power', '60.1', 'W'],
            'acc_energy'   => ['acc_energy', '100.1', 'kWh'],
            'power_switch' => ['power_switch', '255', null],
	        'apparent_energy' => ['apparent_energy', '0.123', 'kVAh'],
	        'acc_gas' => ['acc_gas', '123.4', 'Pulse'],
	        'acc_water' => ['acc_water', '123.4', 'Pulse'],
	        'heart_rate' => ['heart_rate', '100', 'bpm'],
	        'breathing_rate' => ['breathing_rate', '50', 'bpm'],
	        'body_motion' => ['body_motion', '1234', null],
	        'smoke' => ['smoke', '255', null],
	        'person_state_0' => ['person_state', '0', null],
	        'person_state_1' => ['person_state', '1', null],
	        'person_state_2' => ['person_state', '2', null],
	        'person_state_3' => ['person_state', '3', null],
	        'person_state_255' => ['person_state', '255', null],
	        'distance_0' => ['distance', '0', 'cm'],
        	'distance_100' => ['distance', '100', 'cm'],
        	'distance_no_unit' => ['distance', '100', null],
        ];
    }


    /**
     * @test
     */
    public function Device_Data_Update_ノード共有_共有相手への通知() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後にノード共有を行った場合Gateway割り当て直後のNode Inclusionと衝突することと、
        //Gateway割り当て直後のNode Inclusionが処理されたあとWebSocket接続を開始したいため
        //一定時間sleepする。
        sleep(4);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $nodeData = $gatewayData['nodes'][1];
        $nodeId = $nodeData['node_id'];

        //共有相手のユーザーを追加する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //ノード共有対象に設定する。
        $this->nodeUtil->shareNodeWith($nodeId, [
            $user2Email
        ]);


        //共有相手のユーザーでWebSocket接続する
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        $user2AuthToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($user2AuthToken);
        $this->socketIoClient->connect();

        $deviceType = $nodeData['devices'][0]['device_type'];
        $value = '0';
        $unit = null;
        $timestampInMilliSec = (int)(microtime(true) * 1000);
        $this->sendDeviceDataNotification($nodeId, $deviceType, $value, $unit, $timestampInMilliSec);

        //ノード共有相手にも通知が行われることを確認する。
        $notification = $this->socketIoClient->waitNotification();

        $deviceId = $this->getDeviceIdFromDeviceType($nodeId, $deviceType);
        $expectedDeviceValue = [
            'id' => $deviceId,
            'device_type' => $deviceType,
            'value' => $value,
            'unit' => $unit,
            'timestamp' => $timestampInMilliSec
        ];
        $deviceDataUpdateConstraint = new DeviceDataUpdateConstraint($expectedDeviceValue);

        $this->assertThat($notification, $deviceDataUpdateConstraint, 'Node共有対象にも通知が行われること');

    }


    /**
     * @test
     */
    public function Device_Data_Update_ノード共有_共有解除後にDevice_Data_Update発生() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後にノード共有を行った場合Gateway割り当て直後のNode Inclusionと衝突することと、
        //Gateway割り当て直後のNode Inclusionが処理されたあとWebSocket接続を開始したいため
        //一定時間sleepする。
        sleep(4);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $nodeData = $gatewayData['nodes'][1];
        $nodeId = $nodeData['node_id'];

        //共有相手のユーザーを追加する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //ノード共有対象に設定する。
        $this->nodeUtil->shareNodeWith($nodeId, [
            $user2Email
        ]);

        //ノード共有を解除する
        $this->nodeUtil->shareNodeWith($nodeId, [
        ]);

        //共有相手のユーザーでWebSocket接続する
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        $user2AuthToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($user2AuthToken);
        $this->socketIoClient->connect();

        $deviceType = $nodeData['devices'][0]['device_type'];
        $value = '0';
        $unit = null;
        $timestampInMilliSec = (int)(microtime(true) * 1000);
        $this->sendDeviceDataNotification($nodeId, $deviceType, $value, $unit, $timestampInMilliSec);

        //ノード共有解除した場合は通知が行われないことを確認する。
        $notification = $this->socketIoClient->waitNotification();
        $this->assertNull($notification, '通知が行われないこと');
    }


    /**
     * @test
     */
    public function Device_Data_Update_Node_Exclusion_Exclusion後にDevice_Data_Update発生() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後にNode Exclusionを行った場合Gateway割り当て直後のNode Inclusionと衝突することと、
        //Gateway割り当て直後のNode Inclusionが処理されたあとWebSocket接続を開始したいため
        //一定時間sleepする。
        sleep(4);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $nodeData = $gatewayData['nodes'][1];
        $nodeId = $nodeData['node_id'];

        //Node Exclusionを実行する
        $this->sendNodeExclusionNotification(TEST_DEFAULT_GATEWAY, $nodeId);
        sleep(4);

        //Node Exclusion後にDevice Data Updateが行われても、通知が行われないことを確認する。
        $this->socketIoClient->connect();

        $deviceType = $nodeData['devices'][0]['device_type'];
        $value = '0';
        $unit = null;
        $timestampInMilliSec = (int)(microtime(true) * 1000);
        $this->sendDeviceDataNotification($nodeId, $deviceType, $value, $unit, $timestampInMilliSec);

        $notification = $this->socketIoClient->waitNotification();
        $this->assertNull($notification, '通知が行われないこと');
    }

    /**
     * @test
     */
    public function Device_Data_Update_Gateway割り当て解除_解除後にDevice_Data_Update発生() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後のNode Inclusionが処理されたあとWebSocket接続を開始したいため
        //一定時間sleepする。
        sleep(4);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $nodeData = $gatewayData['nodes'][1];
        $nodeId = $nodeData['node_id'];

        //Gatewayの割り当てを解除する
        $this->gatewayUtil->deregisterGateway($gatewayId);

        //Gatewayの割り当て解除後にDevice Data Updateが行われても、通知が行われないことを確認する。
        $this->socketIoClient->connect();

        $deviceType = $nodeData['devices'][0]['device_type'];
        $value = '0';
        $unit = null;
        $timestampInMilliSec = (int)(microtime(true) * 1000);
        $this->sendDeviceDataNotification($nodeId, $deviceType, $value, $unit, $timestampInMilliSec);

        $notification = $this->socketIoClient->waitNotification();
        $this->assertNull($notification, '通知が行われないこと');
    }


    /**
     * @test
     */
    public function Device_Data_Update_第3者_第3者がメッセージを待ち受け() {
        //デフォルトのゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後のNode Inclusionが処理されたあとWebSocket接続を開始したいため
        //一定時間sleepする。
        sleep(4);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $nodeData = $gatewayData['nodes'][1];
        $nodeId = $nodeData['node_id'];

        //第3者のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //第3者のユーザーでWebSocket接続を行う。
        //このユーザーに対しては通知が行われないことを確認する。
        $authToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($authToken);
        $this->socketIoClient->connect();

        $deviceType = $nodeData['devices'][0]['device_type'];
        $value = '0';
        $unit = null;
        $timestampInMilliSec = (int)(microtime(true) * 1000);
        $this->sendDeviceDataNotification($nodeId, $deviceType, $value, $unit, $timestampInMilliSec);

        $notification = $this->socketIoClient->waitNotification();
        $this->assertNull($notification, '通知が行われないこと');
    }


    private function getDeviceIdFromDeviceType($nodeId, $deviceType) {
        $db = DB::getMasterDb();

        $deviceRepository = new DeviceRepository();
        $deviceTypeId = $deviceRepository->getDeviceTypeFromName($deviceType);

        $sql = "SELECT id FROM devices WHERE node_id={$nodeId} AND type={$deviceTypeId}";
        $db->setSql($sql);

        if(!$db->Query()) {
            throw new \Exception('SQLエラー: ' . $db->getErr() . ', SQL: ' . $sql);
        }

        $result = $db->Fetch();
        return (int)$result['id'];
    }
}
