<?php


use Lcas\Test\Constraint\WebSocket\GatewayStateChangeConstraint;
use Lcas\Test\Fixture\GatewayFixtureFactory;

require_once __DIR__ . '/AbstractSocketIoTestCase.php';

class GatewayStateChangeTest extends AbstractSocketIoTestCase {


    /**
     * @test
     * @dataProvider getDataFor_Gateway_State_Change_Notification_状態変更
     */
    public function Gateway_State_Change_Notification_状態変更($state) {

        //ユーザーにゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //ゲートウェイ割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする。
        sleep(4);

        //WebSocket接続を開始する
        $this->socketIoClient->connect();

        //GatewayStateChangeNotificationのメッセージを送信する。
        $this->sendGatewayStateChangeNotification(TEST_DEFAULT_GATEWAY, $state);

        $notification = $this->socketIoClient->waitNotification();

        $gatewayStateChangeConstraint = new GatewayStateChangeConstraint($gatewayId, $state);
        $this->assertThat($notification, $gatewayStateChangeConstraint, '想定されるGatewayStateChangeの情報を含むこと');
    }


    /**
     * @test
     * @dataProvider getDataFor_Gateway_State_Change_Notification_状態変更
     */
    public function Gateway_State_Change_Notification_状態変更_No_Result_Code($state) {

        //ユーザーにゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //ゲートウェイ割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする。
        sleep(4);

        //WebSocket接続を開始する
        $this->socketIoClient->connect();

        //GatewayStateChangeNotificationのメッセージを送信する。
        $this->sendGatewayStateChangeNotificationNoResultCode(TEST_DEFAULT_GATEWAY, $state);

        $notification = $this->socketIoClient->waitNotification();

        $gatewayStateChangeConstraint = new GatewayStateChangeConstraint($gatewayId, $state);
        $this->assertThat($notification, $gatewayStateChangeConstraint, '想定されるGatewayStateChangeの情報を含むこと');
    }


    /**
     * @test
     * @dataProvider getDataFor_Gateway_State_Change_Notification_状態変更
     */
    public function Gateway_State_Change_Notification_状態変更_異常_Error_Result_Code($state) {

        //ユーザーにゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //ゲートウェイ割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする。
        sleep(4);

        //WebSocket接続を開始する
        $this->socketIoClient->connect();

        //GatewayStateChangeNotificationのメッセージを送信する。
        $this->sendGatewayStateChangeNotificationErrorResultCode(TEST_DEFAULT_GATEWAY, $state);

        $this->assertNUll($this->socketIoClient->waitNotification(), '通知が行われないこと');
    }


    public function getDataFor_Gateway_State_Change_Notification_状態変更() {
        return [
            'connected'    => ['connected'],
            'disconnected' => ['disconnected'],
            'inclusion'    => ['inclusion'],
            'exclusion'    => ['exclusion'],
            'initial'      => ['initial'],
            'configuring'  => ['configuring'],
            'joining'      => ['joining'],
            'normal'       => ['normal'],
            'failed'       => ['failed'],
        ];
    }


    /**
     * @test
     */
    public function Gateway_State_Change_Notification_Gateway割り当て解除_解除後にGateway_State_Change発生() {

        //ユーザーにゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //ゲートウェイ割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする。
        sleep(4);

        //割り当てを解除する
        $this->gatewayUtil->deregisterGateway($gatewayId);

        //WebSocket接続を開始する
        $this->socketIoClient->connect();

        //GatewayStateChangeNotificationのメッセージを送信する。
        $this->sendGatewayStateChangeNotification(TEST_DEFAULT_GATEWAY, 'connected');

        $this->assertNUll($this->socketIoClient->waitNotification(), '通知が行われないこと');
    }

    /**
     * @test
     */
    public function Gateway_State_Change_Notification_第3者_第3者がメッセージを待ち受け(){
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //第3者のユーザーを登録する。
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        //WebSocket接続時に必要になるため、セッションIDを控える
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        $gateway2MacAddress = 'A00000002000';
        $this->gatewayUtil->registerGateway($gateway2MacAddress);

        //Gateway割り当て直後のNode Exclusionを受信しないよう、一定時間sleepする
        sleep(4);

        //第3者のユーザー側でWebSocket接続する
        $user2AuthToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($user2AuthToken);
        $this->socketIoClient->connect();

        //デフォルトのゲートウェイに対しGateway State Changeを実行する
        $this->sendGatewayStateChangeNotification(TEST_DEFAULT_GATEWAY, 'connected');

        //第3者のユーザーにはGateway State Changeが通知されないことを確認する。
        $this->assertNull($this->socketIoClient->waitNotification(), '通知が行われないこと');
    }


    /**
     * @test
     */
    public function Gateway_State_Change_Notification_dust_gateway_状態変更_connected() {

        //ユーザーにゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway('A00000006000');
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress('A00000006000');

        //ゲートウェイ割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする。
        sleep(4);

        //WebSocket接続を開始する
        $this->socketIoClient->connect();

        //GatewayStateChangeNotificationのメッセージを送信する。
        $this->sendGatewayStateChangeNotification('A00000006000', 'connected');

        $notification = $this->socketIoClient->waitNotification();

        $gatewayStateChangeConstraint = new GatewayStateChangeConstraint($gatewayId, 'joining');
        $this->assertThat($notification, $gatewayStateChangeConstraint, '想定されるGatewayStateChangeの情報を含むこと');
    }


    /**
     * @test
     * @dataProvider getDataFor_Gateway_State_Change_Notification_dust_gateway_状態変更_connected以外
     */
    public function Gateway_State_Change_Notification_dust_gateway_状態変更_connected以外($state) {

        //ユーザーにゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway('A00000006000');
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress('A00000006000');

        //ゲートウェイ割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする。
        sleep(4);

        //WebSocket接続を開始する
        $this->socketIoClient->connect();

        //GatewayStateChangeNotificationのメッセージを送信する。
        $this->sendGatewayStateChangeNotification('A00000006000', $state);

        $notification = $this->socketIoClient->waitNotification();

        $gatewayStateChangeConstraint = new GatewayStateChangeConstraint($gatewayId, $state);
        $this->assertThat($notification, $gatewayStateChangeConstraint, '想定されるGatewayStateChangeの情報を含むこと');
    }


    public function getDataFor_Gateway_State_Change_Notification_dust_gateway_状態変更_connected以外() {
        return [
            'initial'      => ['initial'],
            'configuring'  => ['configuring'],
            'normal'       => ['normal'],
            'failed'       => ['failed'],
        ];
    }


    private function sendGatewayStateChangeNotification($macAddress, $state) {
        $request = [
            'command' => 'gw_state_change',
            'result_code' => 200,
            'gw_name' => $macAddress,
            'state' => $state,
        ];
        $this->amqpClient->sendMessageToLcas($request);

    }

    private function sendGatewayStateChangeNotificationNoResultCode($macAddress, $state) {
        $request = [
            'command' => 'gw_state_change',
            'gw_name' => $macAddress,
            'state' => $state,
        ];
        $this->amqpClient->sendMessageToLcas($request);

    }

    private function sendGatewayStateChangeNotificationErrorResultCode($macAddress, $state) {
    	$request = [
    		'command' => 'gw_state_change',
            'result_code' => 400,
    		'gw_name' => $macAddress,
    		'state' => $state,
    	];
    	$this->amqpClient->sendMessageToLcas($request);
    
    }
    
}
