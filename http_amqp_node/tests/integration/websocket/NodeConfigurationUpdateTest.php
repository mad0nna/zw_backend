<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use Lcas\Test\Constraint\WebSocket\NodeConfigurationUpdateConstraint;
use Lcas\Test\Constraint\WebSocket\NodeInclusionConstraint;
use Lcas\Test\Fixture\GatewayFixtureFactory;
use WebSocket\Client as WSClient;

require_once __DIR__ . '/AbstractSocketIoTestCase.php';

class NodeConfigurationUpdateTest extends AbstractSocketIoTestCase {

    /**
     * @test
     */
    public function Node_Configuration_Update_設定変更_config_type_zwave() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $node = $gatewayData['nodes'][1];

        //Gateway割り当て直後のNode Inclusionが実行されたあとNode共有設定を行うため、
        //一定時間sleepする
        sleep(4);

        $this->socketIoClient->connect();

        //Node Config Updateを実行する
        $nodeConfig = [
            'config_type' => 'zwave',
            'param_type' => 'unsigned int',
            'param_size' => 1,
            'param_id' => 100,
            'value' => 20
        ];
        $this->sendNodeConfigUpdateNotification($node['node_id'], $nodeConfig);
	$nodeConfig['node_id'] = $node['node_id'];

        //共有相手にも通知が行われることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $nodeConfigurationUpdateConstraint = new NodeConfigurationUpdateConstraint($nodeConfig);
        $this->assertThat($notification, $nodeConfigurationUpdateConstraint, '想定されるノード設定情報を含んでいること');
    }




    /**
     * @test
     */
    public function Node_Configuration_Update_設定変更_config_type_other() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $node = $gatewayData['nodes'][1];

        //Gateway割り当て直後のNode Inclusionが実行されたあとNode共有設定を行うため、
        //一定時間sleepする
        sleep(4);

        $this->socketIoClient->connect();

        //Node Config Updateを実行する
        $nodeConfig = [
            'config_type' => 'other',
            'param_type' => 'unsigned int',
            'param_size' => 1,
            'param_id' => 'power_col_period',
            'value' => 30
        ];
        $this->sendNodeConfigUpdateNotification($node['node_id'], $nodeConfig);
	$nodeConfig['node_id'] = $node['node_id'];

        //共有相手にも通知が行われることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $nodeConfigurationUpdateConstraint = new NodeConfigurationUpdateConstraint($nodeConfig);
        $this->assertThat($notification, $nodeConfigurationUpdateConstraint, '想定されるノード設定情報を含んでいること');
    }


    /**
     * @test
     */
    public function Node_Configuration_Update_ノード共有_更新されたことの通知() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $node = $gatewayData['nodes'][1];

        //Gateway割り当て直後のNode Inclusionが実行されたあとNode共有設定を行うため、
        //一定時間sleepする
        sleep(4);

        //共有相手のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //Node共有設定を行う
        $this->nodeUtil->shareNodeWith($node['node_id'], [
            $user2Email
        ]);

        //共有相手のユーザーでWebSocket接続する
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        $authToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($authToken);
        $this->socketIoClient->connect();

        //Node Config Updateを実行する
        $nodeConfig = [
            'config_type' => 'zwave',
            'param_type' => 'unsigned int',
            'param_size' => 1,
            'param_id' => 100,
            'value' => 20
        ];

        $this->sendNodeConfigUpdateNotification($node['node_id'], $nodeConfig);
	$nodeConfig['node_id'] = $node['node_id'];

        //共有相手にも通知が行われることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $nodeConfigurationUpdateConstraint = new NodeConfigurationUpdateConstraint($nodeConfig);
        $this->assertThat($notification, $nodeConfigurationUpdateConstraint, '想定されるノード設定情報を含んでいること');
    }


    /**
     * @test
     */
    public function Node_Configuration_Update_ノード共有_共有解除後にNode_Configuration_Update発生() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $node = $gatewayData['nodes'][1];

        //Gateway割り当て直後のNode Inclusionが実行されたあとNode共有設定を行うため、
        //一定時間sleepする
        sleep(4);

        //共有相手のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //Node共有設定を行う
        $this->nodeUtil->shareNodeWith($node['node_id'], [
            $user2Email
        ]);

        //Node共有設定を解除する
        $this->nodeUtil->shareNodeWith($node['node_id'], [
        ]);

        //共有相手のユーザーでWebSocket接続する
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        $authToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($authToken);
        $this->socketIoClient->connect();

        //Node Config Updateを実行する
        $nodeConfig = [
            'config_type' => 'zwave',
            'param_type' => 'unsigned int',
            'param_size' => 1,
            'param_id' => 100,
            'value' => 20
        ];

        $this->sendNodeConfigUpdateNotification($node['node_id'], $nodeConfig);

        //共有相手にも通知が行われることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $this->assertNull($notification, '通知が行われないこと');
    }


    /**
     * @test
     */
    public function Node_Configuration_Update_Node_Exclusion_Exclusion後にNode_Configuration_Update発生() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $node = $gatewayData['nodes'][1];

        //Gateway割り当て直後のNode Inclusionが実行されたあと
        //Node Exclusionを実行するため、一定時間sleepする
        sleep(4);

        //NodeをExcludeする
        $this->sendNodeExclusionNotification(TEST_DEFAULT_GATEWAY, $node['node_id']);
        sleep(4);

        $this->socketIoClient->connect();

        //Node Config Updateを実行する
        $nodeConfig = [
            'config_type' => 'zwave',
            'param_type' => 'unsigned int',
            'param_size' => 1,
            'param_id' => 100,
            'value' => 20
        ];
        $this->sendNodeConfigUpdateNotification($node['node_id'], $nodeConfig);

        //Node Exclusion後にNOde Config Updateが実行されても通知されないことを確認する
        $notification = $this->socketIoClient->waitNotification();

        $this->assertNull($notification, '通知が行われないこと');
    }


    /**
     * @test
     */
    public function Node_Configuration_Update_Gateway割り当て解除_解除後にNode_Configuration_Update発生() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $node = $gatewayData['nodes'][1];

        //Gateway割り当て直後のNode Inclusionが実行されたあと
        //Node Exclusionを実行するため、一定時間sleepする
        sleep(4);

        //ゲートウェイを解除する
        $this->gatewayUtil->deregisterGateway($gatewayId);

        $this->socketIoClient->connect();

        //Node Config Updateを実行する
        $nodeConfig = [
            'config_type' => 'zwave',
            'param_type' => 'unsigned int',
            'param_size' => 1,
            'param_id' => 100,
            'value' => 20
        ];
        $this->sendNodeConfigUpdateNotification($node['node_id'], $nodeConfig);

        //ゲートウェイの割り当て解除後にNode Config Updateが実行されても通知されないことを確認する
        $notification = $this->socketIoClient->waitNotification();

        $this->assertNull($notification, '通知が行われないこと');
    }


    /**
     * @test
     */
    public function Node_Configuration_Update_第3者_第3者がメッセージを待ち受け() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $node = $gatewayData['nodes'][1];

        //Gateway割り当て直後のNode Inclusionが実行されたあと
        //Node Exclusionを実行するため、一定時間sleepする
        sleep(4);

        //第3者のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //第3者のユーザーでWebSocket接続する
        $authToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($authToken);
        $this->socketIoClient->connect();

        //Node Config Updateを実行する
        $nodeConfig = [
            'config_type' => 'zwave',
            'param_type' => 'unsigned int',
            'param_size' => 1,
            'param_id' => 100,
            'value' => 20
        ];
        $this->sendNodeConfigUpdateNotification($node['node_id'], $nodeConfig);

        //第3者には通知されないことを確認する
        $notification = $this->socketIoClient->waitNotification();

        $this->assertNull($notification, '通知が行われないこと');
    }


    public function sendNodeConfigUpdateNotification($nodeId, $nodeConfig) {
        $request = [
            'command' => 'node_config_update',
            'result_code' => 200,
            'node_id' => $nodeId,
            'config_type' => $nodeConfig['config_type'],
            'param_type' => $nodeConfig['param_type'],
            'param_size' => $nodeConfig['param_size'],
            'param_id' => $nodeConfig['param_id'],
            'value' => $nodeConfig['value']
        ];
        $this->amqpClient->sendMessageToLcas($request);

    }
}
