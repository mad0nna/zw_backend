<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use Lcas\DB\DB;
use Lcas\Test\Constraint\WebSocket\NodeExclusionConstraint;
use Lcas\Test\Constraint\WebSocket\NodeInclusionConstraint;
use Lcas\Test\Fixture\GatewayFixtureFactory;
use WebSocket\Client as WSClient;

require_once __DIR__ . '/AbstractSocketIoTestCase.php';

class NodeExclusionTest extends AbstractSocketIoTestCase {

    public function setUp() {
        parent::setUp();
    }

    /**
     * @test
     */
    public function Node_Exclusion_ノードの除外_通常の除外() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $excludeNodeId = $gatewayData['nodes'][1]['node_id'];

        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        $this->socketIoClient->connect();

        //Node Inclusionを実行する
        $this->sendNodeExclusionNotification(TEST_DEFAULT_GATEWAY, $excludeNodeId);

        //想定される通知が行われることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $nodeExclusionConstraint = new NodeExclusionConstraint($excludeNodeId);

        $this->assertThat($notification, $nodeExclusionConstraint, '想定されるNode Exclusionの情報を含んでいること');
    }


    /**
     * @test
     * @dataProvider getDataFor_Node_Exclusion_シナリオの自動登録_自動登録解除
     */
    public function Node_Exclusion_シナリオの自動登録_自動登録解除($manufacturer, $product) {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //Nodeのmanufacturerとproductを、自動登録シナリオに対応したものの名前に置き換えてノードを登録する
        $newNode['manufacturer'] = '0109';
        $newNode['product'] = '201f1f20';

        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        $this->socketIoClient->connect();

        //Node Inclusionを実行する
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);
        $this->socketIoClient->waitNotification();

        //この時点では自動登録シナリオが存在すること
        $userId = $this->userUtil->getCurrentUserId();
        $scenarioCount = $this->getAutoRegisteredScenarioCount($userId);
        $this->assertEquals(1, $scenarioCount);

        //Node Exclusionを実行する
        $this->sendNodeExclusionNotification(TEST_DEFAULT_GATEWAY, $newNode['node_id']);
        $this->socketIoClient->waitNotification();

        //自動登録シナリオが解除されていることを家訓する。
        $scenarioCount = $this->getAutoRegisteredScenarioCount($userId);
        $this->assertEquals(0, $scenarioCount, '自動登録シナリオが削除されていること');
    }
    
    
    public function getDataFor_Node_Exclusion_シナリオの自動登録_自動登録解除() {
    	return [
            'Vision       : ZD 2201JP-5(1)' => ['0109', '201f1f10'],	
            'Vision       : ZD 2201JP-5(2)' => ['0109', '201f1f20'],	
            'Vision       : ZP 3111JP-5'    => ['0109', '20212101'],	
            'Philio       : PST02-A'        => ['013c', '0002000c'],	
            'Sharp(Zworks): DN3G6JA062'     => ['024d', '47610001'],	
            'Danalock     : ZD-DN150'       => ['010e', '00080002'],	
            'Sharp        : DN3G6JA069'     => ['010b', '47610069'],	
            '???          : ??? (Smoke sensor)' => ['0123', '0a010001'],	
    	];
    }


    /**
     * @test
     */
    public function Node_Exclusion_ノード共有_解除されたことの通知() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $excludeNodeId = $gatewayData['nodes'][1]['node_id'];

        //共有相手のユーザーを追加する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //共有相手のユーザーをノード共有対象に追加する。
        $this->nodeUtil->shareNodeWith($excludeNodeId, [
            $user2Email
        ]);

        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        //共有対象のユーザー側でWebSocket接続する
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        $user2AuthToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($user2AuthToken);
        $this->socketIoClient->connect();

        //Node Exclusionを実行する
        $this->sendNodeExclusionNotification(TEST_DEFAULT_GATEWAY, $excludeNodeId);

        //共有相手のユーザーにも通知が行われることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $nodeExclusionConstraint = new NodeExclusionConstraint($excludeNodeId);

        $this->assertThat($notification, $nodeExclusionConstraint, '想定されるNode Exclusionの情報を含んでいること');
    }


    /**
     * @test
     */
    public function Node_Exclusion_ノード共有_共有解除後にNode_Exclusion発生() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $excludeNodeId = $gatewayData['nodes'][1]['node_id'];

        //共有相手のユーザーを追加する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');

        //共有相手のユーザーをノード共有対象に追加する。
        $this->nodeUtil->shareNodeWith($excludeNodeId, [
            $user2Email
        ]);

        //共有相手のユーザーをノード共有対象から外す。
        $this->nodeUtil->shareNodeWith($excludeNodeId, [
        ]);


        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        //共有対象のユーザー側でWebSocket接続する
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        $user2AuthToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($user2AuthToken);
        $this->socketIoClient->connect();

        //Node Exclusionを実行する
        $this->sendNodeExclusionNotification(TEST_DEFAULT_GATEWAY, $excludeNodeId);

        //共有相手のユーザーにも通知が行われることを確認する
        $this->assertNull($this->socketIoClient->waitNotification(), '通知が行われないこと');
    }


    /**
     * @test
     */
    public function Node_Exclusion_ノード設定_設定の削除() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $excludeNode = $gatewayData['nodes'][1];
        $excludeNodeId = $gatewayData['nodes'][1]['node_id'];


        //ノードに設定情報を追加する
        $excludeNode['zwave_configurations'] = [];
        $excludeNode['zwave_configurations'][] = [
            'parameter_no' => 100,
            'type' => 'unsigned int',
            'size' => 1,
            'value' => 20
        ];
        $this->nodeUtil->updateNode($excludeNodeId, $excludeNode);

        $this->socketIoClient->connect();

        //Node Exclusionを実行する
        $this->sendNodeExclusionNotification(TEST_DEFAULT_GATEWAY, $excludeNodeId);

        $this->socketIoClient->waitNotification();

        //再度、同じユーザーに登録する
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $excludeNode);

        //Node Inclusionが取り込まれるまでsleepする
        sleep(4);

        //設定上法が削除されていることを確認する
        $nodeDetailResponse = $this->nodeUtil->getNodeById($excludeNodeId);
        $nodeDetail = json_decode($nodeDetailResponse->getBody(), true);

        $this->assertNotContains('zwave_configurations', $nodeDetail, '設定情報が削除されていること');
    }


    /**
     * @test
     */
    public function Node_Exclusion_Gateway割り当て解除_解除後にNode_Exclusion発生() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $excludeNodeId = $gatewayData['nodes'][1]['node_id'];

        //ゲートウェイの割当を解除する
        $this->gatewayUtil->deregisterGateway($gatewayId);


        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        //WebSocket接続する
        $this->socketIoClient->connect();

        //Node Exclusionを実行する
        $this->sendNodeExclusionNotification(TEST_DEFAULT_GATEWAY, $excludeNodeId);

        //共有相手のユーザーにも通知が行われることを確認する
        $this->assertNull($this->socketIoClient->waitNotification(), '通知が行われないこと');
    }


    /**
     * @test
     */
    public function Node_Exclusion_第3者_第3者がメッセージを待ち受け(){
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        $gatewayFactory = new GatewayFixtureFactory();
        $gatewayData = $gatewayFactory->create(TEST_DEFAULT_GATEWAY);
        $excludeNodeId = $gatewayData['nodes'][1]['node_id'];

        //第3者のユーザーを登録する。
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        //WebSocket接続時に必要になるため、セッションIDを控える
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        $gateway2MacAddress = 'A00000002000';
        $this->gatewayUtil->registerGateway($gateway2MacAddress);

        //Gateway割り当て直後のNode Exclusionを受信しないよう、一定時間sleepする
        sleep(4);

        //第3者のユーザー側でWebSocket接続する
        $user2AuthToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($user2AuthToken);
        $this->socketIoClient->connect();

        //デフォルトのユーザーに対しNode Exclusionを実行する
        $this->sendNodeExclusionNotification(TEST_DEFAULT_GATEWAY, $excludeNodeId);

        //第3者のユーザーにはNode Exclusionが通知されないことを確認する。
        $this->assertNull($this->socketIoClient->waitNotification(), '通知が行われないこと');
    }


    protected function getAutoRegisteredScenarioCount($userId) {
        $sql = "SELECT count(*) AS c FROM scenarios WHERE user_id={$userId} AND auto_config=1";

        $db = DB::getMasterDb();
        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception("SQLエラー： {$db->getErr()}, SQL: {$sql}");
        }
        $result = $db->Fetch();

        if(!$result) {
            throw new \Exception('自動登録されたシナリオが見つかりません。user_id: ' . $userId);
        }

        return $result['c'];
    }

}
