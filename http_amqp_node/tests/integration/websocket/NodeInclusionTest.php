<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use Lcas\DB\DB;
use Lcas\Test\Constraint\WebSocket\NodeInclusionConstraint;
use Lcas\Test\Util\TestUtil;
use WebSocket\Client as WSClient;
use Lcas\Test\Constraint\WebSocket\GeneralErrorConstraint;
use Lcas\Log\Logger;

require_once __DIR__ . '/AbstractSocketIoTestCase.php';

class NodeInclusionTest extends AbstractSocketIoTestCase {

    public function setUp() {
        parent::setUp();
    }

    /**
     * @test
     */
    public function Node_Inclusion_ノードの追加_新規() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        $this->socketIoClient->connect();

        //Node Inclusionを実行する
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);


        //想定される通知が行われることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $nodeInclusionConstraint = new NodeInclusionConstraint($newNode);
        $nodeInclusionConstraint->setGatewayId($gatewayId);

        $this->assertThat($notification, $nodeInclusionConstraint, '想定されるノード情報を含んでいること');
    }


    /**
     * @test
     */
    public function Node_Inclusion_ノードの追加_以前と同じユーザー() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //Gateway割り当て直後のNode Inclusionの受信を待つため、一定時間sleepする
        sleep(4);

        //新しくノードを追加し、ラベルを登録する。
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);
        sleep(4);

        $expectedLabels = [
            'test-new-node-test'
        ];
        $this->nodeUtil->updateNodeLabels($exampleNodeId, $expectedLabels);

        //一度Excludeする
        $this->sendNodeExclusionNotification(TEST_DEFAULT_GATEWAY, $exampleNodeId);
        sleep(4);

        //WebSocketの接続を開始し、
        //再度Node Inclusionを行う。
        $this->socketIoClient->connect();
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);


        //想定される通知が行われることを確認する
        //ノードのラベルが削除されずに保持されていること
        $notification = $this->socketIoClient->waitNotification();

        $nodeInclusionConstraint = new NodeInclusionConstraint($newNode);
        $nodeInclusionConstraint->setGatewayId($gatewayId);
        $nodeInclusionConstraint->setLabels($expectedLabels);


        $this->assertThat($notification, $nodeInclusionConstraint, '想定されるノード情報を含んでいること');
    }


    /**
     * @test
     */
    public function Node_Inclusion_ノードの追加_以前と異なるユーザー() {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //Gateway割り当て直後のNode Inclusionの受信を待つため、一定時間sleepする
        sleep(4);

        //新しくノードを追加し、ラベルを登録する。
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);
        sleep(4);

        $expectedLabels = [
            'test-new-node-test'
        ];
        $this->nodeUtil->updateNodeLabels($exampleNodeId, $expectedLabels);

        //一度Excludeする
        $this->sendNodeExclusionNotification(TEST_DEFAULT_GATEWAY, $exampleNodeId);
        sleep(4);

        //別のユーザーを登録し、ゲートウェイを割り当てる
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        //WebSocket接続時に必要になるため、セッションIDを控える
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        $gateway2MacAddress = 'A00000002000';
        $this->gatewayUtil->registerGateway($gateway2MacAddress);
        $gateway2Id = $this->gatewayUtil->getGatewayIdFromMacAddress($gateway2MacAddress);

        //Gateway割り当て直後のNode Inclusionの取り込みを待つため、一定時間sleepする
        sleep(4);

        //WebSocketの接続を開始し、
        //再度Node Inclusionを行う。
        $user2AuthToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($user2AuthToken);
        $this->socketIoClient->connect();
        $this->sendNodeInclusionNotification($gateway2MacAddress, $newNode);

        //想定される通知が行われることを確認する
        //(以前と所有者が異なるためノードのラベルがクリアされていること)
        $notification = $this->socketIoClient->waitNotification();

        $nodeInclusionConstraint = new NodeInclusionConstraint($newNode);
        $nodeInclusionConstraint->setGatewayId($gateway2Id);
        $nodeInclusionConstraint->setLabels([]);

        $this->assertThat($notification, $nodeInclusionConstraint, '想定されるノード情報を含んでいること');
    }


    /**
     * @dataProvider getDataFor_Node_Inclusion_デバイス種別
     * @test
     */
    public function Node_Inclusion_デバイス種別($deviceId, $deviceType){
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //テスト用のデバイスを追加する。
        $newNode['devices'] = [];
        $newNode['devices'][] = [
            'device_id' => $deviceId,
            'device_type' => $deviceType,
        ];

        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        $this->socketIoClient->connect();

        //Node Inclusionを実行する
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);


        //想定される通知が行われることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $nodeInclusionConstraint = new NodeInclusionConstraint($newNode);
        $nodeInclusionConstraint->setGatewayId($gatewayId);

        $this->assertThat($notification, $nodeInclusionConstraint, '想定されるノード情報を含んでいること');
    }

    public function getDataFor_Node_Inclusion_デバイス種別() {
        //デバイスIDの8000は、フィクスチャのデータ内でIDが重複しないと思われる
        //値として指定しています。
        return [
            'temperature'  => ['device_id' => 8000, 'device_type' => 'temperature'],
            'humidity'     => ['device_id' => 8010, 'device_type' => 'humidity'],
            'motion'       => ['device_id' => 8020, 'device_type' => 'motion'],
            'open_close'   => ['device_id' => 8030, 'device_type' => 'open_close'],
            'lock'         => ['device_id' => 8040, 'device_type' => 'lock'],
            'cover'        => ['device_id' => 8050, 'device_type' => 'cover'],
            'battery'      => ['device_id' => 8060, 'device_type' => 'battery'],
            'luminance'    => ['device_id' => 8070, 'device_type' => 'luminance'],
            'call_button'  => ['device_id' => 8080, 'device_type' => 'call_button'],
            'smart_lock'   => ['device_id' => 8090, 'device_type' => 'smart_lock'],
            'power'        => ['device_id' => 8100, 'device_type' => 'power'],
            'acc_energy'   => ['device_id' => 8110, 'device_type' => 'acc_energy'],
            'power_switch' => ['device_id' => 8120, 'device_type' => 'power_switch'],
        ];
    }

    /**
     * @dataProvider getDataFor_Node_Inclusion_タイプ種別
     * @test
     */
    public function Node_Inclusion_タイプ種別($nodeType){
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //テスト用のデバイスを追加する。
        $newNode['node_type'] = $nodeType;

        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        $this->socketIoClient->connect();

        //Node Inclusionを実行する
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);


        //想定される通知が行われることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $nodeInclusionConstraint = new NodeInclusionConstraint($newNode);
        $nodeInclusionConstraint->setGatewayId($gatewayId);

        $this->assertThat($notification, $nodeInclusionConstraint, '想定されるノード情報を含んでいること');
    }

    public function getDataFor_Node_Inclusion_タイプ種別() {
        //デバイスIDの8000は、フィクスチャのデータ内でIDが重複しないと思われる
        //値として指定しています。
        return [
            'zwave'     => ['zwave'],
            'ip_camera' => ['ip_camera'],
            'dust_ip'   => ['dust_ip'],
        ];
    }

    /**
     * @test
     * @dataProvider getDataFor_Node_Inclusion_シナリオの自動登録
     */
    public function Node_Inclusion_シナリオの自動登録($nodeInfo) {
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //Nodeのmanufacturerとproductを、自動登録シナリオに対応したものの名前に置き換えてノードを登録する
        $newNode['manufacturer'] = $nodeInfo['manufacturer'];
        $newNode['product'] = $nodeInfo['product'];

        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        $this->socketIoClient->connect();

        //Node Inclusionを実行する
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);
        $this->socketIoClient->waitNotification();

        //シナリオが自動登録されていることを家訓する。
        $userId = $this->userUtil->getCurrentUserId();
        $scenarioCount = $this->getAutoRegisteredScenarioCount($userId);
        $this->assertEquals(1, $scenarioCount, 'シナリオが登録されていること');
    }


    public function getDataFor_Node_Inclusion_シナリオの自動登録() {
        return[
            'Vision_ZD_2201JP_5'      => [['manufacturer' => '0109', 'product' => '201f1f20']],
            'Vision_ZD_2201JP_5_201f1f10' => [['manufacturer' => '0109', 'product' => '201f1f10']],
            'Vision_ZP_3111JP_5'      => [['manufacturer' => '0109', 'product' => '20212101']],
            'Philio_PST02_A'          => [['manufacturer' => '013c', 'product' => '0002000c']],
            'Sharp_Zworks_DN3G6JA062' => [['manufacturer' => '024d', 'product' => '47610001']],
            'Danalock_ZD_DN150'       => [['manufacturer' => '010e', 'product' => '00080002']],
            'Sharp_DN3G6JA069'        => [['manufacturer' => '010b', 'product' => '47610069']],	
            '???_??? (Smoke sensor)'  => [['manufacturer' => '0123', 'product' => '0a010001']],	
        ];
    }

    /**
     * @test
     */
    public function Node_Inclusion_Gateway割り当て解除_解除後にNode_Inclusion発生(){
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //ゲートウェイ割当を解除する
        $this->gatewayUtil->deregisterGateway($gatewayId);

        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        $this->socketIoClient->connect();

        //Node Inclusionを実行する
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);


        //通知が行われないことを確認する。
        $this->assertNull($this->socketIoClient->waitNotification(), '通知が行われないこと');
    }


    /**
     * @test
     */
    public function Node_Inclusion_第3者_第3者がメッセージを待ち受け(){
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //第3者のユーザーを登録する。
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
        $user2AuthToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $gateway2MacAddress = 'A00000002000';
        $this->gatewayUtil->registerGateway($gateway2MacAddress);


        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        //第3者のユーザー側でWebSocket接続する
        $this->socketIoClient->setAuthToken($user2AuthToken);
        $this->socketIoClient->connect();

        //デフォルトのユーザーに対しNode Inclusionを実行する
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);

        //第3者のユーザーにはNOde Inclusionが通知されないことを確認する。
        $this->assertNull($this->socketIoClient->waitNotification(), '通知が行われないこと');
    }


    /**
     * @test
     */
    public function Node_Inclusion_failed_cmd(){
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        $this->socketIoClient->connect();

        //Node Inclusionを実行する
        $newNode['failed_cmd'] = array("SECURITY", "MANUFACTURER_SPECIFIC");
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);


        //想定される通知が行われることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $nodeInclusionConstraint = new NodeInclusionConstraint($newNode);
        $nodeInclusionConstraint->setGatewayId($gatewayId);

        $this->assertThat($notification, $nodeInclusionConstraint, '想定されるノード情報を含んでいること');
    }


    /**
     * @test
     */
    public function Node_Inclusion_インクルードの失敗(){
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);
        
    	$this->socketIoClient->connect();

        //Node Inclusionを実行する
        $nodeIncl = [
        		'command' => 'node_incl',
        		'result_code' => 400,
        		'reason' => ['error1', 'error2'],
        		'gw_name' => TEST_DEFAULT_GATEWAY
        ];
        $this->amqpClient->sendMessageToLcas($nodeIncl);
        
        // General Error が通知されることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $error = [
        		'notification_type' => 'general_error',
        		'content' => [
        				'error_id' => 1,
        				'detail' => TEST_DEFAULT_GATEWAY
        		]
        ];
        $generalErrorConstraint = new GeneralErrorConstraint($error);
        $this->assertThat($notification, $generalErrorConstraint, '想定されるエラー情報を含んでいること');
    }


    /**
     * @test
     */
    public function Node_Inclusion_異常系_インクルード済みのノードのインクルード(){
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //Gateway割り当て直後のNode Inclusionの受信を待つため、一定時間sleepする
        sleep(4);

        //新しくノードを追加する。
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);
        sleep(4);

        //WebSocketの接続を開始し、
        //再度Node Inclusionを行う。
        $this->socketIoClient->connect();
        $updatedNode = $newNode;
        $updatedNode['manufacturer'] = 'new-fake-factory';
        $updatedNode['product'] = 'node1201';
        $updatedNode['serial_no'] = '1201';
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $updatedNode);


        // General Error が通知されることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $error = [
        		'notification_type' => 'general_error',
        		'content' => [
        				'error_id' => 1,
        				'detail' => TEST_DEFAULT_GATEWAY
        		]
        ];
        $generalErrorConstraint = new GeneralErrorConstraint($error);
        $this->assertThat($notification, $generalErrorConstraint, '想定されるエラー情報を含んでいること');
    }


    protected function getAutoRegisteredScenarioCount($userId) {
        $sql = "SELECT count(*) AS c FROM scenarios WHERE user_id={$userId} AND auto_config=1";

        $db = DB::getMasterDb();
        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception("SQLエラー： {$db->getErr()}, SQL: {$sql}");
        }
        $result = $db->Fetch();

        if(!$result) {
            throw new \Exception('自動登録されたシナリオが見つかりません。user_id: ' . $userId);
        }

        return $result['c'];
    }


}
