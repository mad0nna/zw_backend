<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use Lcas\DB\DB;
use Lcas\Test\Constraint\WebSocket\NodeInclusionConstraint;
use Lcas\Test\Util\TestUtil;
use WebSocket\Client as WSClient;
use Lcas\Test\Constraint\WebSocket\GeneralErrorConstraint;
use Lcas\Log\Logger;

require_once __DIR__ . '/AbstractSocketIoTestCase.php';

class NodeInclusionWithAppTest extends AbstractSocketIoTestCase {

    public function setUp() {
        parent::setUp();
    }

    /**
     * @dataProvider getDataFor_Node_Inclusion_with_app_field
     * @test
     */
    public function Node_Inclusion_with_app_field($appConfigs, $joinKeySet){
        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        //デフォルトのゲートウェイには存在しないノードを取得する
        $exampleNodeId = 1200;
        $nodeFactory = new \Lcas\Test\Fixture\NodeFixtureFactory();
        $newNode = $nodeFactory->create($exampleNodeId);

        //テスト用のデバイスを追加する。
        $newNode['dust_ip']['join_key_set'] = $joinKeySet;
        $newNode['app'] = [];
        foreach ($appConfigs as $appConfig) {
            $newNode['app'][] = [
                'app_name' => $appConfig['app_name'],
                'configurations' => $appConfig['configurations'],
            ];
        }

        //Gateway割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする
        sleep(4);

        $this->socketIoClient->connect();

        //Node Inclusionを実行する
        $this->sendNodeInclusionNotification(TEST_DEFAULT_GATEWAY, $newNode);


        //想定される通知が行われることを確認する
        $notification = $this->socketIoClient->waitNotification();

        $nodeInclusionConstraint = new NodeInclusionConstraint($newNode);
        $nodeInclusionConstraint->setGatewayId($gatewayId);

        $this->assertThat($notification, $nodeInclusionConstraint, '想定されるノード情報を含んでいること');
    }

    public function getDataFor_Node_Inclusion_with_app_field() {

        return [
            'fake_app_with_join_key_set_is_true'  =>
       		[
           		[
           			[
           				'app_name' => "some_app",
           				'configurations' =>
           				[
           					[
								'param_name' => "fake_name1",
			                	'param_value' => "fake_value1",
           					],
                   			[
                       			'param_name' => "fake_name2",
                       			'param_value' => "fake_value2",
                   			]
           				]
           			]
           		],
       			true
           	],
            'fake_app_with_join_key_set_is_false'  =>
       		[
           		[
           			[
           				'app_name' => "some_app",
           				'configurations' =>
           				[
           					[
								'param_name' => "fake_name1",
			                	'param_value' => "fake_value1",
           					],
                   			[
                       			'param_name' => "fake_name2",
                       			'param_value' => "fake_value2",
                   			]
           				]
           			]
           		],
       			false
           	],
        ];
    }

}
