<?php

use Lcas\Log\Logger;
use Lcas\Repository\NodeRepository;
use Lcas\Test\Constraint\WebSocket\NodeStateChangeConstraint;

require_once __DIR__ . '/AbstractSocketIoTestCase.php';

class NodeStateChangeNotificationTest extends AbstractSocketIoTestCase {

    /**
     * @test
     */
    public function New_state_should_be_saved_to_database_and_notification_should_be_forwarded_to_mobile_side() {
        // load initial nodes along with gateways
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);

        $nodeRepository = new NodeRepository();
        $nodes = $nodeRepository->findByGatewayIds([$gatewayId]);

        $first_node = $nodes[0];

        //ゲートウェイ割り当て直後のNode Inclusionを受信しないよう、一定時間sleepする。
        sleep(4);

        //WebSocket接続を開始する
        $this->socketIoClient->connect();

        // Try to set state to negotiating and confirm that it is saved to the database
        $TEST_STATE = "negotiating";
        $this->sendNodeStateChangeNotification($first_node['id'], $TEST_STATE);
        sleep(4);
        $updated_first_node = $nodeRepository->findById($first_node['id']);

        $this->assertSame((int)($updated_first_node['yet_another_state']), $nodeRepository->getNodeYetAnotherStateTypeFromName($TEST_STATE), "States before and after change should be identical");

        // Check whether notification is forwarded to mobile
        $notification = $this->socketIoClient->waitNotification();
        $nodeStateChangeConstraint = new NodeStateChangeConstraint($first_node['id'], $TEST_STATE);

        $this->assertThat($notification, $nodeStateChangeConstraint, '想定されるNode State Updateの情報を含んでいること');
    }


    private function sendNodeStateChangeNotification($node_id, $state) {
        $request = [
            'command' => 'node_state_change',
            'node_id' => $node_id,
            'state' => $state,
        ];
        $this->amqpClient->sendMessageToLcas($request);
    }

}
