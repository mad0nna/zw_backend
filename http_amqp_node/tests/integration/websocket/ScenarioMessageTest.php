<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use Lcas\Test\Constraint\WebSocket\ScenarioMessageConstraint;
use Lcas\Test\Fixture\ScenarioFixtureFactory;
use WebSocket\Client as WSClient;

require_once __DIR__ . '/AbstractSocketIoTestCase.php';

class ScenarioMessageTest extends AbstractSocketIoTestCase {

    /**
     * @var ScenarioFixtureFactory
     */
    private $scenarioFactory;

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->scenarioFactory = new ScenarioFixtureFactory();
    }




    /**
     * @dataProvider getDataFor_Scenario_Message_イベント通知_ユーザーのplatform
     * @test
     */
    public function Scenario_Message_イベント通知_ユーザーのplatform($platform, $token){

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後のNode Inclusionを受信することを避けるため、一定時間sleepする
        sleep(4);

        //platformをgcmに変更
        $this->updateUserPlatform($platform, $token);

        //シナリオを登録する
        $scenarioData = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenarioData);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];


        //WebSocket接続する
        $this->socketIoClient->connect();

        //シナリオイベントの通知を実行する
        $message = 'Scenario Event通知(WebSocket)';
        $this->sendScenarioMessageNotification($scenarioId, $message);

        $notification = $this->socketIoClient->waitNotification();

        $timestampInMilliSec = (int)(microtime(true) * 1000);
        $expectedScenarioMessage = [
            'id' => $scenarioId,
            'message' => $message,
            'timestamp' => $timestampInMilliSec
        ];
        $scenarioMessageConstraint = new ScenarioMessageConstraint($expectedScenarioMessage);

        $this->assertThat($notification, $scenarioMessageConstraint, '想定されるScenario Messageの情報を含んでいること');
    }


    public function getDataFor_Scenario_Message_イベント通知_ユーザーのplatform() {
        return[
            'gcm'  => ['gcm', 'test-token'],
            //iOSの場合、トークンは64文字の16進である必要がある
            'apns' => ['apns', '1234567890123456789012345678901234567890123456789012345678901234'],
            'sqs' => ['sqs', ''],
        	'none' => ['none', ''],
        ];
    }


    /**
     * @dataProvider getDataFor_Scenario_Message_様々なイベント通知
     * @test
     */
    public function Scenario_Message_様々なイベント通知($scenarioName, $device1, $device2){

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway('A00000003000');

        //Gateway割り当て直後のNode Inclusionを受信することを避けるため、一定時間sleepする
        sleep(4);

        //シナリオを登録する
        $scenarioData = $this->scenarioFactory->create($scenarioName);
        if (!is_null($device1)) {
        	$scenarioData['conditions'][0]['lhs'] = $device1;
        }
        if (!is_null($device2)) {
        	$scenarioData['conditions'][1]['lhs'] = $device2;
        }
        $registerResponse = $this->registerScenario($scenarioData);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];


        //WebSocket接続する
        $this->socketIoClient->connect();

        //シナリオイベントの通知を実行する
        $message = 'Scenario Event通知(WebSocket)';
        $this->sendScenarioMessageNotification($scenarioId, $message);

        $notification = $this->socketIoClient->waitNotification();

        $timestampInMilliSec = (int)(microtime(true) * 1000);
        $expectedScenarioMessage = [
            'id' => $scenarioId,
            'message' => $message,
            'timestamp' => $timestampInMilliSec
        ];
        $scenarioMessageConstraint = new ScenarioMessageConstraint($expectedScenarioMessage);

        $this->assertThat($notification, $scenarioMessageConstraint, '想定されるScenario Messageの情報を含んでいること');
    }


    public function getDataFor_Scenario_Message_様々なイベント通知() {
        return[
			'abnormal-heart-rate-scenario-1' => ['abnormal-heart-rate-scenario-1', 3116, null],
			'abnormal-heart-rate-scenario-2' => ['abnormal-heart-rate-scenario-2', 3116, 3116],
			'abnormal-breath-rate-scenario-1' => ['abnormal-breath-rate-scenario-1', 3117, null],
			'abnormal-breath-rate-scenario-2' => ['abnormal-breath-rate-scenario-2', 3117, 3117],
			'ambulation-scenario' => ['ambulation-scenario', 3116, 3117],
			'fire-scenario' => ['fire-scenario', 3119, null],
			'person-state-scenario-0' => ['person-state-scenario-0', 3120, null],
			'person-state-scenario-1' => ['person-state-scenario-1', 3120, null],
			'person-state-scenario-2' => ['person-state-scenario-2', 3120, null],
			'distance-scenario' => ['distance-scenario', 3121, null],
        ];
    }
    
    /**
     * @test
     */
    public function Scenario_Message_第3者_第3者がメッセージを待ち受け(){

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後のNode Inclusionを受信することを避けるため、一定時間sleepする
        sleep(4);

        //シナリオを登録する
        $scenarioData = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenarioData);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //第3者のユーザーを登録する
        $user2Email = 'user2@example.com';
        $this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
        $this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);

        //第3者のユーザーでWebSocket接続する
        $authToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
        $this->socketIoClient->setAuthToken($authToken);
        $this->socketIoClient->connect();

        //シナリオイベントの通知を実行する
        $message = 'Scenario Event通知(WebSocket)';
        $this->sendScenarioMessageNotification($scenarioId, $message);

        //第3者には通知が行われないことを確認する
        $notification = $this->socketIoClient->waitNotification();
        $this->assertNull($notification, '通知が行われないことを確認する');
    }

    /**
     * @test
     */
    public function Scenario_Message_デバイスへのアクセス権なし_GWなし(){

        //ゲートウェイを割り当てる
        $this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);

        //Gateway割り当て直後のNode Inclusionを受信することを避けるため、一定時間sleepする
        sleep(4);

        //シナリオを登録する
        $scenarioData = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
        $registerResponse = $this->registerScenario($scenarioData);
        $registerResponseJson = json_decode($registerResponse->getBody(), true);
        $scenarioId = $registerResponseJson['id'];

        //WebSocket接続する
        $authToken = $this->userUtil->getAuthTokenFromEmail(TEST_DEFAULT_USER_EMAIL);
        $this->socketIoClient->setAuthToken($authToken);
        $this->socketIoClient->connect();

        //シナリオイベントの通知を実行する
        $message = 'Scenario Event通知(WebSocket)';
        $this->sendScenarioMessageNotification($scenarioId, $message);

        $notification = $this->socketIoClient->waitNotification();
        
        $timestampInMilliSec = (int)(microtime(true) * 1000);
        $expectedScenarioMessage = [
        		'id' => $scenarioId,
        		'message' => $message,
        		'timestamp' => $timestampInMilliSec
        ];
        $scenarioMessageConstraint = new ScenarioMessageConstraint($expectedScenarioMessage);
         
        $this->assertThat($notification, $scenarioMessageConstraint, '想定されるScenario Messageの情報を含んでいること');
        
        //ゲートウェイの登録解除
        $gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);
        $this->gatewayUtil->deregisterGateway($gatewayId);
        sleep(4);
        
        //WebSocket接続する
        $authToken = $this->userUtil->getAuthTokenFromEmail(TEST_DEFAULT_USER_EMAIL);
        $this->socketIoClient->setAuthToken($authToken);
        $this->socketIoClient->connect();

        //シナリオイベントの通知を実行する
        $message = 'Scenario Event通知(WebSocket)';
        $this->sendScenarioMessageNotification($scenarioId, $message);

        //通知が行われないことを確認する
        $notification = $this->socketIoClient->waitNotification();
        $this->assertNull($notification, '通知が行われないことを確認する');
    }

    /**
     * @test
     */
    public function Scenario_Message_デバイスへのアクセス権なし_GWなし_wbgt(){
    
    	//ゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
    
    	//Gateway割り当て直後のNode Inclusionを受信することを避けるため、一定時間sleepする
    	sleep(4);
    
    	//シナリオを登録する
    	$temperatureDeviceId = 200;
    	$humidityDeviceId = 201;
    	$scenarioData = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_WBGT);
    	$scenarioData['temperature_dev'] = $temperatureDeviceId;
    	$scenarioData['humidity_dev'] = $humidityDeviceId;
    	$registerResponse = $this->registerScenario($scenarioData);
    	$registerResponseJson = json_decode($registerResponse->getBody(), true);
    	$scenarioId = $registerResponseJson['id'];
    
    	//WebSocket接続する
    	$authToken = $this->userUtil->getAuthTokenFromEmail(TEST_DEFAULT_USER_EMAIL);
    	$this->socketIoClient->setAuthToken($authToken);
    	$this->socketIoClient->connect();
    
    	//シナリオイベントの通知を実行する
    	$message = 'Scenario Event通知(WebSocket)';
    	$this->sendScenarioMessageNotification($scenarioId, $message);
    
    	$notification = $this->socketIoClient->waitNotification();
    
    	$timestampInMilliSec = (int)(microtime(true) * 1000);
    	$expectedScenarioMessage = [
    			'id' => $scenarioId,
    			'message' => $message,
    			'timestamp' => $timestampInMilliSec
    	];
    	$scenarioMessageConstraint = new ScenarioMessageConstraint($expectedScenarioMessage);
    	 
    	$this->assertThat($notification, $scenarioMessageConstraint, '想定されるScenario Messageの情報を含んでいること');
    
    	//ゲートウェイの登録解除
    	$gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);
    	$this->gatewayUtil->deregisterGateway($gatewayId);
    	sleep(4);
    
    	//WebSocket接続する
    	$authToken = $this->userUtil->getAuthTokenFromEmail(TEST_DEFAULT_USER_EMAIL);
    	$this->socketIoClient->setAuthToken($authToken);
    	$this->socketIoClient->connect();
    
    	//シナリオイベントの通知を実行する
    	$message = 'Scenario Event通知(WebSocket)';
    	$this->sendScenarioMessageNotification($scenarioId, $message);
    
    	//通知が行われないことを確認する
    	$notification = $this->socketIoClient->waitNotification();
    	$this->assertNull($notification, '通知が行われないことを確認する');
    }
    
    
    /**
     * @test
     */
    public function Scenario_Message_デバイスへのアクセス権なし_GWなし_scxml(){
    
    	//ゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
    
    	//Gateway割り当て直後のNode Inclusionを受信することを避けるため、一定時間sleepする
    	sleep(4);
    
    	//シナリオを登録する
    	$temperatureDeviceId = 200;
    	$humidityDeviceId = 201;
    	$scenarioData = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_SCXML);
    	$scenarioData['device_list'] = [$temperatureDeviceId, $humidityDeviceId];
    	$registerResponse = $this->registerScenario($scenarioData);
    	$registerResponseJson = json_decode($registerResponse->getBody(), true);
    	$scenarioId = $registerResponseJson['id'];
    
    	//WebSocket接続する
    	$authToken = $this->userUtil->getAuthTokenFromEmail(TEST_DEFAULT_USER_EMAIL);
    	$this->socketIoClient->setAuthToken($authToken);
    	$this->socketIoClient->connect();
    
    	//シナリオイベントの通知を実行する
    	$message = 'Scenario Event通知(WebSocket)';
    	$this->sendScenarioMessageNotification($scenarioId, $message);
    
    	$notification = $this->socketIoClient->waitNotification();
    
    	$timestampInMilliSec = (int)(microtime(true) * 1000);
    	$expectedScenarioMessage = [
    			'id' => $scenarioId,
    			'message' => $message,
    			'timestamp' => $timestampInMilliSec
    	];
    	$scenarioMessageConstraint = new ScenarioMessageConstraint($expectedScenarioMessage);
    
    	$this->assertThat($notification, $scenarioMessageConstraint, '想定されるScenario Messageの情報を含んでいること');
    
    	//ゲートウェイの登録解除
    	$gatewayId = $this->gatewayUtil->getGatewayIdFromMacAddress(TEST_DEFAULT_GATEWAY);
    	$this->gatewayUtil->deregisterGateway($gatewayId);
    	sleep(4);
    
    	//WebSocket接続する
    	$authToken = $this->userUtil->getAuthTokenFromEmail(TEST_DEFAULT_USER_EMAIL);
    	$this->socketIoClient->setAuthToken($authToken);
    	$this->socketIoClient->connect();
    
    	//シナリオイベントの通知を実行する
    	$message = 'Scenario Event通知(WebSocket)';
    	$this->sendScenarioMessageNotification($scenarioId, $message);
    
    	//通知が行われないことを確認する
    	$notification = $this->socketIoClient->waitNotification();
    	$this->assertNull($notification, '通知が行われないことを確認する');
    }
    
    
    /**
     * @test
     */
    public function Scenario_Message_デバイスへのアクセス権なし_共有解除(){

    	//ゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
    	
    	//Gateway割り当て直後のNode Inclusionを受信することを避けるため、一定時間sleepする
    	sleep(4);
    	
    	//第3者のユーザーを登録する
    	$user2Email = 'user2@example.com';
    	$this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
    	 
    	//ノードを共有する
    	$this->nodeUtil->shareNodeWith(200, [$user2Email]);
    	
    	//第3者ログイン
    	$this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
    	
    	//シナリオを登録する
    	$scenarioData = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO);
    	$registerResponse = $this->registerScenario($scenarioData);
    	$registerResponseJson = json_decode($registerResponse->getBody(), true);
    	$scenarioId = $registerResponseJson['id'];
    	
    	//第3者のユーザーでWebSocket接続する
    	$authToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
    	$this->socketIoClient->setAuthToken($authToken);
    	$this->socketIoClient->connect();
    	
    	//シナリオイベントの通知を実行する
    	$message = 'Scenario Event通知(WebSocket)';
    	$this->sendScenarioMessageNotification($scenarioId, $message);
    	
    	//第3者に通知が行われることを確認する
    	$notification = $this->socketIoClient->waitNotification();

    	$timestampInMilliSec = (int)(microtime(true) * 1000);
    	$expectedScenarioMessage = [
    			'id' => $scenarioId,
    			'message' => $message,
    			'timestamp' => $timestampInMilliSec
    	];
    	$scenarioMessageConstraint = new ScenarioMessageConstraint($expectedScenarioMessage);
    	
    	$this->assertThat($notification, $scenarioMessageConstraint, '想定されるScenario Messageの情報を含んでいること');
    	
    	//デフォルトユーザー（gateway所有者）ログイン
    	$this->userUtil->loginWithDefaultUser();
    	
    	//ノードを共有解除
    	$this->nodeUtil->shareNodeWith(200, []);
    	
    	//第3者ログイン
    	$this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
    	
    	//第3者のユーザーでWebSocket接続する
    	$authToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
    	$this->socketIoClient->setAuthToken($authToken);
    	$this->socketIoClient->connect();
    	
    	//シナリオイベントの通知を実行する
    	$message = 'Scenario Event通知(WebSocket)';
    	$this->sendScenarioMessageNotification($scenarioId, $message);
    	
        //通知が行われないことを確認する
        $notification = $this->socketIoClient->waitNotification();
        $this->assertNull($notification, '通知が行われないことを確認する');
    }
    
    /**
     * @test
     */
    public function Scenario_Message_デバイスへのアクセス権なし_共有解除_wbgt(){
    
    	//ゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
    	 
    	//Gateway割り当て直後のNode Inclusionを受信することを避けるため、一定時間sleepする
    	sleep(4);
    	 
    	//第3者のユーザーを登録する
    	$user2Email = 'user2@example.com';
    	$this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
    
    	//ノードを共有する
    	$this->nodeUtil->shareNodeWith(200, [$user2Email]);
    	 
    	//第3者ログイン
    	$this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
    	 
    	//シナリオを登録する
    	$temperatureDeviceId = 200;
    	$humidityDeviceId = 201;
    	$scenarioData = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_WBGT);
    	$scenarioData['temperature_dev'] = $temperatureDeviceId;
    	$scenarioData['humidity_dev'] = $humidityDeviceId;
    	$registerResponse = $this->registerScenario($scenarioData);
    	$registerResponseJson = json_decode($registerResponse->getBody(), true);
    	$scenarioId = $registerResponseJson['id'];
    	 
    	//第3者のユーザーでWebSocket接続する
    	$authToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
    	$this->socketIoClient->setAuthToken($authToken);
    	$this->socketIoClient->connect();
    	 
    	//シナリオイベントの通知を実行する
    	$message = 'Scenario Event通知(WebSocket)';
    	$this->sendScenarioMessageNotification($scenarioId, $message);
    	 
    	//第3者に通知が行われることを確認する
    	$notification = $this->socketIoClient->waitNotification();
    
    	$timestampInMilliSec = (int)(microtime(true) * 1000);
    	$expectedScenarioMessage = [
    			'id' => $scenarioId,
    			'message' => $message,
    			'timestamp' => $timestampInMilliSec
    	];
    	$scenarioMessageConstraint = new ScenarioMessageConstraint($expectedScenarioMessage);
    	 
    	$this->assertThat($notification, $scenarioMessageConstraint, '想定されるScenario Messageの情報を含んでいること');
    	 
    	//デフォルトユーザー（gateway所有者）ログイン
    	$this->userUtil->loginWithDefaultUser();
    	 
    	//ノードを共有解除
    	$this->nodeUtil->shareNodeWith(200, []);
    	 
    	//第3者ログイン
    	$this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
    	 
    	//第3者のユーザーでWebSocket接続する
    	$authToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
    	$this->socketIoClient->setAuthToken($authToken);
    	$this->socketIoClient->connect();
    	 
    	//シナリオイベントの通知を実行する
    	$message = 'Scenario Event通知(WebSocket)';
    	$this->sendScenarioMessageNotification($scenarioId, $message);
    	 
    	//通知が行われないことを確認する
    	$notification = $this->socketIoClient->waitNotification();
    	$this->assertNull($notification, '通知が行われないことを確認する');
    }
    
    /**
     * @test
     */
    public function Scenario_Message_デバイスへのアクセス権なし_共有解除_scxml(){
    
    	//ゲートウェイを割り当てる
    	$this->gatewayUtil->registerGateway(TEST_DEFAULT_GATEWAY);
    	 
    	//Gateway割り当て直後のNode Inclusionを受信することを避けるため、一定時間sleepする
    	sleep(4);
    	 
    	//第3者のユーザーを登録する
    	$user2Email = 'user2@example.com';
    	$this->userUtil->registerUser($user2Email, TEST_DEFAULT_USER_PASSWORD, 'none');
    
    	//ノードを共有する
    	$this->nodeUtil->shareNodeWith(200, [$user2Email]);
    	 
    	//第3者ログイン
    	$this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
    	 
    	//シナリオを登録する
    	$temperatureDeviceId = 200;
    	$humidityDeviceId = 201;
    	$scenarioData = $this->scenarioFactory->create(TEST_DEFAULT_SCENARIO_SCXML);
    	$scenarioData['device_list'] = [$temperatureDeviceId, $humidityDeviceId];
    	$registerResponse = $this->registerScenario($scenarioData);
    	$registerResponseJson = json_decode($registerResponse->getBody(), true);
    	$scenarioId = $registerResponseJson['id'];
    	 
    	//第3者のユーザーでWebSocket接続する
    	$authToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
    	$this->socketIoClient->setAuthToken($authToken);
    	$this->socketIoClient->connect();
    	 
    	//シナリオイベントの通知を実行する
    	$message = 'Scenario Event通知(WebSocket)';
    	$this->sendScenarioMessageNotification($scenarioId, $message);
    	 
    	//第3者に通知が行われることを確認する
    	$notification = $this->socketIoClient->waitNotification();
    
    	$timestampInMilliSec = (int)(microtime(true) * 1000);
    	$expectedScenarioMessage = [
    			'id' => $scenarioId,
    			'message' => $message,
    			'timestamp' => $timestampInMilliSec
    	];
    	$scenarioMessageConstraint = new ScenarioMessageConstraint($expectedScenarioMessage);
    	 
    	$this->assertThat($notification, $scenarioMessageConstraint, '想定されるScenario Messageの情報を含んでいること');
    	 
    	//デフォルトユーザー（gateway所有者）ログイン
    	$this->userUtil->loginWithDefaultUser();
    	 
    	//ノードを共有解除
    	$this->nodeUtil->shareNodeWith(200, []);
    	 
    	//第3者ログイン
    	$this->userUtil->login($user2Email, TEST_DEFAULT_USER_PASSWORD);
    	 
    	//第3者のユーザーでWebSocket接続する
    	$authToken = $this->userUtil->getAuthTokenFromEmail($user2Email);
    	$this->socketIoClient->setAuthToken($authToken);
    	$this->socketIoClient->connect();
    	 
    	//シナリオイベントの通知を実行する
    	$message = 'Scenario Event通知(WebSocket)';
    	$this->sendScenarioMessageNotification($scenarioId, $message);
    	 
    	//通知が行われないことを確認する
    	$notification = $this->socketIoClient->waitNotification();
    	$this->assertNull($notification, '通知が行われないことを確認する');
    }
    
    /**
     * @param $platform
     * @param $token
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function updateUserPlatform($platform, $token) {
        $parameters = [
            'old_password' => TEST_DEFAULT_USER_PASSWORD,
        ];
        if(!is_null($platform)) {
            $parameters['platform'] = $platform;
        }
        if(!is_null($token)) {
            $parameters['token'] = $token;
        }

        return $this->httpClient->put('/api/v3/users/self', ['body' => json_encode($parameters)]);
    }

    private function registerScenario($scenarioData) {

        return $this->httpClient->post('/api/v3/scenarios', [
            'body' => json_encode($scenarioData)
        ]);

    }


    private function sendScenarioMessageNotification($scenarioId, $message) {
        $request = [
            'command' => 'scenario_event',
            'scenario_id' => $scenarioId,
            'content' => $message,
        ];
        $this->amqpClient->sendMessageToLcas($request);
    }


}
