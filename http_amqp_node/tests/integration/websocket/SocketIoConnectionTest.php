<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use Lcas\Test\Util\TestUtil;
use WebSocket\Client as WSClient;

require_once __DIR__ . '/AbstractSocketIoTestCase.php';

class SocketIoConnectionTest extends AbstractSocketIoTestCase {

    public function setUp() {
        parent::setUp();
    }

    /**
     * @test
     * @group run
     */
    public function SocketIO_正常系_正規のネームスペースで接続() {
        $this->socketIoClient->setOrigin(TEST_WEBSOCKET_ORIGIN);
        $this->socketIoClient->connect();

        //接続の3秒後に切断されていないことを確認するため、一定時間sleepする
        //(Socket.IO版APIでは、認証は通ってもネームスペース指定がない場合に接続を切断する仕様があるため)
        sleep(4);

        $connected = $this->socketIoClient->isConnected();
        $this->assertTrue($connected, '接続に成功すること');
    }


    /**
     * @test
     * @group run
     */
    public function SocketIO_正常系_Origin指定なしで接続() {
        $this->socketIoClient->connect();

        //接続の3秒後に切断されていないことを確認するため、一定時間sleepする
        //(Socket.IO版APIでは、認証は通ってもネームスペース指定がない場合に接続を切断する仕様があるため)
        sleep(4);

        $connected = $this->socketIoClient->isConnected();
        $this->assertTrue($connected, '接続に成功すること');
    }


    /**
     * @test
     */
    public function SocketIO_異常系_ネームスペース指定なし() {
        $this->socketIoClient->setNameSpace('');
        $this->socketIoClient->connect();

        //ネームスペースの指定をしないことで接続語3秒で切断されることを確認するため、
        //3秒以上sleepする
        sleep(4);

        $connected = $this->socketIoClient->isConnected();
        $this->assertFalse($connected, '3秒経過後に切断されること');
    }


    /**
     * @test
     */
    public function SocketIO_異常系_ネームスペース間違い() {
        $this->socketIoClient->setNameSpace('invalid');
        $this->socketIoClient->connect();

        //即座に切断されることを確認する

        $connected = $this->socketIoClient->isConnected();
        $this->assertFalse($connected, '即座に切断されること');
    }


    /**
     * @test
     */
    public function SocketIO_異常系_認証Cookieなし() {
        $this->socketIoClient->setAuthToken(null);
        $this->socketIoClient->connect();

        $connected = $this->socketIoClient->isConnected();
        $this->assertFalse($connected, '即座に切断されること');
    }



}
