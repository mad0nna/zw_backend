<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use WebSocket\Client as WSClient;

require_once __DIR__ . '/AbstractWebSocketTestCase.php';


class WebSocketTest extends AbstractWebSocketTestCase {

    /**
     * @test
     * @group run
     */
    public function WebSocket_正常系_正規のエンドポイントに接続() {
        $this->webSocketClient->setOrigin(TEST_WEBSOCKET_ORIGIN);
        $this->webSocketClient->connect();

        $this->assertTrue($this->webSocketClient->isConnected(), '接続に成功すること');
    }


    /**
     * @test
     * @group run
     */
    public function WebSocket_正常系_Origin指定なしで接続() {
        $this->webSocketClient->connect();

        $this->assertFalse($this->webSocketClient->isConnected(), '接続できないこと');
    }

    /**
     * @test
     */
    public function WebSocket_異常系_誤ったエンドポイントに接続() {
        $this->webSocketClient->setEndpoint('/ws/v3/invalid');
        $this->webSocketClient->connect();

        $this->assertFalse($this->webSocketClient->isConnected(), '即座に切断されること');
    }

    /**
     * @test
     */
    public function WebSocket_異常系_認証Cookieなし() {
        $this->webSocketClient->setAuthToken(null);
        $this->webSocketClient->connect();

        $this->assertFalse($this->webSocketClient->isConnected(), '即座に切断されること');
    }


    /**
     * @test
     * @group run
     */
    public function WebSocket_異常系_不正なOriginで接続() {
        $this->webSocketClient->setOrigin('http://invalid.example.com/');
        $this->webSocketClient->connect();

        $this->assertFalse($this->webSocketClient->isConnected(), '接続に失敗すること');

    }

}
