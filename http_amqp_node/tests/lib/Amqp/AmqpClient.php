<?php


namespace Lcas\Test\Amqp;


use Lcas\Log\Logger;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class AmqpClient {

    /**
     * @var AMQPStreamConnection
     */
    private static $connection = null;

    /**
     * @var AMQPChannel
     */
    private $channel;

    /**
     * @var string
     */
    private $routingKey;

    public function __construct() {
        $this->routingKey = '';
    }


    public function initConnection() {
        if(self::$connection) {
            return;
        }

        self::$connection = new AMQPStreamConnection(AMQP_HOST, AMQP_PORT, AMQP_USER, AMQP_PASS, AMQP_VHOST);
    }


    public function setRoutingKey($routingKey) {
        $this->routingKey = $routingKey;
    }

    /**
     * リクエストの送信を行う
     * @param array $request 送信するメッセージを含んだ連想配列
     * @param array $additionalProperties AMQPメッセージのプロパティ
     * @return mixed
     */
    public function sendMessage($request, $additionalProperties=[], $routingKey=null) {
        if(!self::$connection) {
            $this->initConnection();
        }
        if(!$this->channel) {
            $this->channel = self::$connection->channel();
        }
        $baseProperties = array(
            'content_type' => 'application/json',
            'user_id'        => AMQP_USER,
            'delivery_mode' => 2,
        );

        $encodedRequest = json_encode($request);
        $logger = Logger::get();

        $properties = array_merge($baseProperties, $additionalProperties);
        $message = new AMQPMessage($encodedRequest, $properties);

        $logger->addDebug('Test AMQP Client: AMQP Message sent: ' . $encodedRequest . ', properties: ' . var_export($properties, true));

        if(is_null($routingKey)) {
            $routingKey = $this->routingKey;
        }
        $this->channel->basic_publish($message, AMQP_EXCHANGE_NAME, $routingKey);
    }


    /**
     * LCAS宛てにAMQPメッセージの送信を行う。
     * @param array $request 送信するメッセージを含んだ連想配列
     * @param array $additionalProperties AMQPメッセージのプロパティ
     * @return mixed
     */
    public function sendMessageToLcas($request, $additionalProperties=[]) {
        $this->sendMessage($request, $additionalProperties, AMQP_QUEUE_NAME);
    }


    /**
     * ZIO側で強制的にNGを返すかどうかを指定するメッセージを送信する
     * @param boolean $forceNgEnabled NGメッセージを返すかどうか
     */
    public function requestForceNgEnabled($forceNgEnabled) {
        $this->sendMessage([
            'command' => '__force_ng',
            'enabled'   => $forceNgEnabled
        ]);
    }


    /**
     * 応答時のresult_codeの値を指定する
     * @param int $code 応答コード値
     */
    public function requestResultCode($code) {
        $this->sendMessage([
            'command' => '__result_code',
            'code'   => $code
        ]);
    }


    /**
     * ZIO側で強制的にsleepするかどうかを指定するメッセージを送信する
     * @param int $sleepSeconds 待ち時間(単位：秒)
     */
    public function requestSleep($sleepSeconds) {
        $this->sendMessage([
            'command' => '__sleep',
            'sleep_seconds'   => $sleepSeconds
        ]);
    }


    /**
     * ZIO側でレスポンスを返さないよう指定するメッセージを送信する
     * (HTTP APIのタイムアウトを発生させるため)
     *
     * @param boolean $enabled 有効にするかどうか
     */
    public function requestTimeout($enabled) {
        $this->sendMessage([
            'command' => '__timeout',
            'enabled' => $enabled
        ]);
    }

}
