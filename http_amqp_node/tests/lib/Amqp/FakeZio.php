<?php

namespace Lcas\Test\Amqp;

use Lcas\Log\Logger;
use Lcas\Test\Amqp\Handler\DeviceControlHandler;
use Lcas\Test\Amqp\Handler\PublicKeyUpdateHandler;
use Lcas\Test\Amqp\Handler\SetAclHandler;
use Lcas\Test\Amqp\Handler\ApplicationSpecificNodeConfigUpdateHandler;
use Lcas\Test\Amqp\Handler\GatewayAssociationHandler;
use Lcas\Test\Amqp\Handler\GatewayDissociationHandler;
use Lcas\Test\Amqp\Handler\NodeConfigUpdateHandler;
use Lcas\Test\Amqp\Handler\NodeExclusionHandler;
use Lcas\Test\Amqp\Handler\NodeInclusionHandler;
use Lcas\Test\Amqp\Handler\NodeRemovalHandler;
use Lcas\Test\Amqp\Handler\ScenarioDeregistrationHandler;
use Lcas\Test\Amqp\Handler\ScenarioModificationHandler;
use Lcas\Test\Amqp\Handler\ScenarioRegistrationHandler;
use Lcas\Test\Amqp\Handler\UserDeregistrationHandler;
use Lcas\Test\Amqp\Handler\UserRegistrationHandler;
use PhpAmqpLib\Exception\AMQPIOException;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class FakeZio{

    private $amqpConnection;

    private $channel = null;

    private $forceNgEnabled = false;

    private $resultCode = 200;

    private $timeoutEnabled = false;

    private $sleepSeconds = 0;


    public function run() {
        $this->amqpConnection = new AMQPStreamConnection(AMQP_HOST, AMQP_PORT, AMQP_USER, AMQP_PASS, AMQP_VHOST);
        $this->channel = $this->amqpConnection->channel();
        $logger = Logger::get();
        $logger->addInfo('Fake ZIO: AMQP 接続完了');

        $this->defineQueue();
        $logger->addInfo('Fake ZIO: キュー定義完了');

        $consumer_tag = 'fake_zio_queue';

        $noLocal = false;
        $noAck = false;
        $consumeExclusive = false;
        $noWait = false;
        $callbackFunc = array($this, 'onMessage');
        $this->channel->basic_consume('', $consumer_tag, $noLocal, $noAck, $consumeExclusive, $noWait, $callbackFunc);
        $logger->addInfo('Fake ZIO: メッセージ受付完了');

        try {
            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }
        } catch(AMQPIOException $amqpIoException) {
            $logger->addError('[Fake ZIO Error]' . $amqpIoException->getMessage());
        }
    }



    public function onMessage($message) {
        $logger = Logger::get();

        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);

        $body = $message->body;
        $properties = $message->get_properties();
        $json = json_decode($body, true);

        if(!isset($json['command'])) {
            //JSONデータとしてのデコードに失敗したものは、何度処理してもエラーとなるため、
            //エラーとして扱いつつACKを返す。
            $logger->addCritical('[Fake ZIO: AMQP DECODE ERROR]AMQPメッセージのデコードに失敗: ' . $body . ', properties:' . var_export($properties, true));
            return;
        } else {
            $logger->addDebug('Fake ZIO: AMQP RECEIVED:' . var_export($json, true));
        }
        $this->dispatch($json, $properties);

   }

    private function dispatch($json, $properties) {
        $messageHandler = null;
        switch($json['command']) {
            case 'user_gw_assoc':   $messageHandler = new GatewayAssociationHandler(); break;
            case 'user_gw_dissoc':  $messageHandler = new GatewayDissociationHandler(); break;
            case 'node_config_update': $messageHandler = new NodeConfigUpdateHandler(); break;
            case 'node_incl':       $messageHandler = new NodeInclusionHandler(); break;
            case 'node_excl':       $messageHandler = new NodeExclusionHandler(); break;
            case 'node_removal':    $messageHandler = new NodeRemovalHandler(); break;
            case 'user_reg':        $messageHandler = new UserRegistrationHandler(); break;
            case 'user_dereg':      $messageHandler = new UserDeregistrationHandler(); break;
            case 'scenario_reg':    $messageHandler = new ScenarioRegistrationHandler(); break;
            case 'scenario_mod':    $messageHandler = new ScenarioModificationHandler(); break;
            case 'scenario_dereg':  $messageHandler = new ScenarioDeregistrationHandler(); break;
            case 'dev_ctrl':        $messageHandler = new DeviceControlHandler(); break;
            case 'pubkey_update':   $messageHandler = new PublicKeyUpdateHandler(); break;
            case 'app_sp_node_config_update':   $messageHandler = new ApplicationSpecificNodeConfigUpdateHandler(); break;
            case 'set_acl':         $messageHandler = new SetAclHandler(); break;

            case '__force_ng':
                $this->forceNgEnabled = $json['enabled'];
                break;
            case '__result_code':
                $this->resultCode = $json['code'];
                break;
            case '__sleep':
                $this->sleepSeconds = $json['sleep_seconds'];
                break;
            case '__timeout':
                $this->timeoutEnabled = $json['enabled'];
                break;

            default:
                throw new \RuntimeException('対応するメッセージハンドラが存在しません。: ' . $json['command']);
        }

        if(is_null($messageHandler)) {
            return;
        }

        $messageHandler->setForceNgEnabled($this->forceNgEnabled);
        $messageHandler->setResultCode($this->resultCode);
        $messageHandler->setSleepSeconds($this->sleepSeconds);
        $messageHandler->setTimeoutEnabled($this->timeoutEnabled);
        $messageHandler->handle($json, $properties);
    }


    private function defineQueue() {
        $queueName = AMQP_QUEUE_NAME;
        $exchangeName = AMQP_EXCHANGE_NAME;

        $passive = false;
        $durable = true;
        $exclusive = false;
        $autoDelete = false;
        $this->channel->queue_declare($queueName, $passive, $durable, $exclusive, $autoDelete);
        $this->channel->queue_declare('fake_zio_queue', $passive, $durable, $exclusive, $autoDelete);

        $exchangeType = 'direct';
        $exchangePassive = false;
        $exchangeDurable = true;
        $exchangeAutoDelete = false;
        $this->channel->exchange_declare($exchangeName, $exchangeType, $exchangePassive, $exchangeDurable, $exchangeAutoDelete);

        $routingKey = $queueName;
        $this->channel->queue_bind($queueName, $exchangeName, $routingKey);
        $this->channel->queue_bind('fake_zio_queue', $exchangeName, '');

    }
}

