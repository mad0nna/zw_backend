<?php

namespace Lcas\Test\Amqp\Handler;



use Lcas\Test\Amqp\AmqpClient;

abstract class AbstractHandler implements HandlerInterface {

    protected $client;

    /**
     * 強制的にNGを返すかどうか
     * @var boolean
     */
    protected $forceNgEnabled;


    /**
     * 応答時のresult_code
     * @var int
     */
    protected $resultCode;

    /**
     * レスポンスを返す前にsleepする時間(単位：秒)
     * @var int
     */
    protected $sleepSeconds;


    /**
     * メッセージを返さずに応答をタイムアウトさせるかどうか
     * @var boolean
     */
    protected $timeoutEnabled;

    public function __construct() {
        $this->client = new AmqpClient();
        $this->client->setRoutingKey(AMQP_QUEUE_NAME);
    }

    abstract public function handle($request, $properties);


    public function setForceNgEnabled($forceNgEnabled) {
        $this->forceNgEnabled = $forceNgEnabled;
    }


    public function setResultCode($code) {
        $this->resultCode = $code;
    }


    public function setSleepSeconds($forceSeconds) {
        $this->sleepSeconds = $forceSeconds;
    }

    public function setTimeoutEnabled($enabled) {
        $this->timeoutEnabled = $enabled;
    }

}
