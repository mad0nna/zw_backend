<?php

namespace Lcas\Test\Amqp\Handler;


class ApplicationSpecificNodeConfigUpdateHandler extends AbstractHandler {

    public function handle($request, $properties) {

        if($this->timeoutEnabled) {
            return;
        }
        if($this->sleepSeconds > 0) {
            sleep($this->sleepSeconds);
        }

        $correlationId = isset($properties['correlation_id']) ? $properties['correlation_id'] : 0;

        $notification = [];
        if(!$this->forceNgEnabled) {
            $notification = [
                'command' => 'app_sp_node_config_update',
                'result_code' => (string)$this->resultCode,
                'app_name' => $request["app_name"],
                'node_name' => $request["node_name"],
                'configurations' => $request["configurations"],
            ];
        } else {
            $notification = [
                'command' => 'app_sp_node_config_update',
                'result_code' => '400',
                'reason' => ['Force error.'],
            ];
        }

        $notificationProperty = [
            'correlation_id' => $correlationId,
        ];
        $this->client->sendMessage($notification, $notificationProperty);
    }


}
