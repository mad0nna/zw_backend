<?php

namespace Lcas\Test\Amqp\Handler;


class DeviceControlHandler extends AbstractHandler {

    public function handle($request, $properties) {

        if($this->timeoutEnabled) {
            return;
        }
        if($this->sleepSeconds > 0) {
            sleep($this->sleepSeconds);
        }

        $correlationId = isset($properties['correlation_id']) ? $properties['correlation_id'] : 0;

        $notification = [];
        if(!$this->forceNgEnabled) {
            $notification = [
                'command' => 'dev_ctrl',
                'result_code' => (string)$this->resultCode,
                'node_name' => $request['node_name'],
                'device_type' => $request['device_type'],
                'action_type' => $request['action_type'],
                'action' => $request['action'],
            ];
        } else {
            $notification = [
                'command' => 'dev_ctrl',
                'result_code' => 400,
                'reason' => ['Force error.'],
            ];
        }

        $notificationProperty = [
            'correlation_id' => $correlationId,
        ];
        $this->client->sendMessage($notification, $notificationProperty);

        usleep(100 * 1000);

        $this->sendDeviceDataUpdate($request['node_name'], $request['device_type'], $request['action_type'], $request['action']);
    }



    private function sendDeviceDataUpdate($nodeName, $deviceType, $actionType, $action) {

        $value = null;
        if($actionType == 'door_lock') {
            switch($action) {
                case 'lock':   $value = '0'; break;
                case 'unlock': $value = '255'; break;
            }
        }
        if($actionType == 'smart_tap') {
            switch($action) {
                case 'turn_on':  $value = '255'; break;
                case 'turn_off': $value = '0'; break;
            }
        }

        $timestamp = (int)(microtime(true) * 1000);
        $notification = [
            'command' => 'device_data',
            'node_name' => $nodeName,
            'device_type' => $deviceType,
            'value' => $value,
            'timestamp' => $timestamp,
        ];
        $this->client->sendMessageToLcas($notification);
    }

}
