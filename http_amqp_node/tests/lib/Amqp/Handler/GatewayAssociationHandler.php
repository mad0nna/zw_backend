<?php

namespace Lcas\Test\Amqp\Handler;

use Lcas\Test\Fixture\GatewayFixtureFactory;
use Psr\Log\InvalidArgumentException;

class GatewayAssociationHandler extends AbstractHandler {

    public function handle($request, $properties) {

        $gatewayFixtureFactory = new GatewayFixtureFactory();

        $notification = [];
        $gatewayData = [];
        $correlationId = isset($properties['correlation_id']) ? $properties['correlation_id'] : 0;
        try {

            if(!isset($request['gw_name'])) {
                throw new \InvalidArgumentException('NG応答のテスト用です。');
            }

            if($this->timeoutEnabled) {
                return;
            }
            if($this->sleepSeconds > 0) {
                sleep($this->sleepSeconds);
            }
            if($this->forceNgEnabled) {
                throw new \RuntimeException('Fake ZIO: 無効なゲートウェイが指定されました。');
            }

            //フィクスチャが存在するか確認する
            $gatewayData = $gatewayFixtureFactory->create($request['gw_name']);

            $notification = $this->convertGatewayDataToAmqpResponse($gatewayData, $request);

        } catch(\Exception $e) {
            $notification = [
                'command' => 'user_gw_assoc',
                'result_code' => 400,
                'reason' => [$e->getMessage()],
            ];
        }
        $notificationProperty = [
            'correlation_id' => $correlationId,
        ];

        $this->sendGatewayStateChange($request["gw_name"]);
        sleep(1);

        $this->client->sendMessage($notification, $notificationProperty);

        //Node Inclusionを送信する
        foreach($gatewayData['nodes'] as $node) {
            $this->sendNodeInclusion($request, $node);
        }
    }


    /**
     * GatewayのフィクスチャのデータをAMQPレスポンス用の値に変換する
     * @param array $gatewayData
     * @param array $request
     */
    private function convertGatewayDataToAmqpResponse($gatewayData, $request){

        //基本はほぼ同じ
        $result = $gatewayData;

        //manufacturer等の情報を抜く
        foreach($gatewayData['nodes'] as $idx=>$node) {
            unset($gatewayData['nodes'][$idx]['manufacturer']);
            unset($gatewayData['nodes'][$idx]['product']);
            unset($gatewayData['nodes'][$idx]['serial_no']);
        }

        //command, usernameとresult_codeの追加
        $result['command'] = 'user_gw_assoc';
        $result['username'] = $request['username'];
        $result['result_code'] = (string)$this->resultCode;
        return $result;
    }


    private function sendNodeInclusion($request, $node) {
        $notification = [
            'command'      => 'node_incl',
            'node_id'      => $node['node_id'],
            'username'     => $request['username'],
            'gw_name'      => $request['gw_name'],
            'manufacturer' => $node['manufacturer'],
            'product' => $node['product'],
            'serial_no'    => $node['serial_no'],
        	'failed_cmd'   => isset($node['failed_cmd']) ? $node['failed_cmd'] : array(),
            'devices'      => $node['devices'],
            'actions'      => $node['actions'],
            'state'        => isset($node['state']) ? $node['state'] : null,
            'app'          => isset($node['app']) ? $node['app'] : null,
            'node_type'    => isset($node['node_type']) ? $node['node_type'] : null
        ];
        $this->client->sendMessageToLcas($notification);
    }

    private function sendGatewayStateChange($gw_name) {
        $notification = [
            'command' => 'gw_state_change',
            'gw_name' => $gw_name,
            'state' => 'connected',
        ];
        $this->client->sendMessageToLcas($notification);
    }
}
