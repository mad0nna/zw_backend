<?php

namespace Lcas\Test\Amqp\Handler;


class GatewayDissociationHandler extends AbstractHandler {

    public function handle($request, $properties) {


        if($this->timeoutEnabled) {
            return;
        }
        if($this->sleepSeconds > 0) {
            sleep($this->sleepSeconds);
        }

        $correlationId = isset($properties['correlation_id']) ? $properties['correlation_id'] : 0;
        if(!$this->forceNgEnabled) {
            $notification = [
                'command' => 'user_gw_dissoc',
                'result_code' => (string)$this->resultCode,
                'username' => $request['username'],
                'gw_name' => $request['gw_name'],
            ];
        } else {
            $notification = [
                'command' => 'user_gw_dissoc',
                'result_code' => '400',
                'reason' => ['NG応答の確認用レスポンスです'],
            ];
        }
        $notificationProperty = [
            'correlation_id' => $correlationId,
        ];
        $this->client->sendMessage($notification, $notificationProperty);
    }

}
