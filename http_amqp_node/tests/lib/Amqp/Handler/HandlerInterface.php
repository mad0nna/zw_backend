<?php

namespace Lcas\Test\Amqp\Handler;



interface HandlerInterface {

    /**
     * メッセージに対する応答(Notification)を実行する。
     *
     * (AMQPメッセージの送信も含みます)
     * @param array $request AMQPメッセージ
     * @param array $request AMQPメッセージのプロパティ(Correlation-Idなど)
     * @return void
     */
    public function handle($request, $properties);


    /**
     * 強制的にNGを返すかどうかを設定する
     * @param boolean $forceNgEnabled 強制的にNGを返すかどうか
     */
    public function setForceNgEnabled($forceNgEnabled);


    /**
     * 応答時のresult_codeの値を指定する(NG時以外)
     * @param int $code
     */
    public function setResultCode($code);

    /**
     * 強制的にsleepするかどうかを設定する
     * @param int $sleepSeconds sleepする時間(単位：秒)
     */
    public function setSleepSeconds($sleepSeconds);

    /**
     * 応答レスポンスを返さず処理を抜けるかを設定する
     * (HTTP APIでタイムアウトを発生させるため)
     *
     * @param boolean $enabled 有効にするかどうか
     */
    public function setTimeoutEnabled($enabled);
}
