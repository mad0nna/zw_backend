<?php

namespace Lcas\Test\Amqp\Handler;


class NodeConfigUpdateHandler extends AbstractHandler {

    public function handle($request, $properties) {

        if($this->timeoutEnabled) {
            return;
        }
        if($this->sleepSeconds > 0) {
            sleep($this->sleepSeconds);
        }

        $correlationId = isset($properties['correlation_id']) ? $properties['correlation_id'] : 0;
        if(!$this->forceNgEnabled) {

            $notification = [
                'command'     => 'node_config_update',
                'result_code' => (string)$this->resultCode,
                'node_id'     => $request['node_id'],
                'config_type' => $request['config_type'],
                'param_type'  => $request['param_type'],
                'param_size'  => $request['param_size'],
                'param_id'    => $request['param_id'],
                'value'       => $request['value'],
            ];
        } else {
            $notification = [
                'command'     => 'node_config_update',
                'result_code' => '400',
                'reason'      => ['NG応答の確認用です'],
            ];
        }
        $notificationProperty = [
            'correlation_id' => $correlationId,
        ];
        $this->client->sendMessage($notification, $notificationProperty);
    }

}
