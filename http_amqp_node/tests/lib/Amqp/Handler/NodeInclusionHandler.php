<?php

namespace Lcas\Test\Amqp\Handler;


class NodeInclusionHandler extends AbstractHandler {

    public function handle($request, $properties) {

        if($this->timeoutEnabled) {
            return;
        }

        $newState = $request['flag'] == 'start' ? 'inclusion' : 'connected';

        $correlationId = isset($properties['correlation_id']) ? $properties['correlation_id'] : 0;

        if(!$this->forceNgEnabled) {
            $notification = [
                'command' => 'gw_state_change',
                'result_code' => (string)$this->resultCode,
                'gw_name' => $request['gw_name'],
                'state' => $newState,
            ];
        } else {
            $notification = [
                'command' => 'gw_state_change',
                'result_code' => '400',
                'reason' => ['NG応答の確認用です'],
            ];
        }
        $notificationProperty = [
            'correlation_id' => $correlationId,
        ];

        if($this->sleepSeconds > 0) {
            sleep($this->sleepSeconds);
        }

        $this->client->sendMessage($notification, $notificationProperty);
    }

}
