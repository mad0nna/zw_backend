<?php

namespace Lcas\Test\Amqp\Handler;


class PublicKeyUpdateHandler extends AbstractHandler {

    public function handle($request, $properties) {

        if($this->timeoutEnabled) {
            return;
        }
        if($this->sleepSeconds > 0) {
            sleep($this->sleepSeconds);
        }

        $correlationId = isset($properties['correlation_id']) ? $properties['correlation_id'] : 0;

        $notification = [];
        if(!$this->forceNgEnabled) {
            $notification = [
                'command' => 'pubkey_update',
                'result_code' => (string)$this->resultCode,
                'gw_name' => $request["gw_name"],
                'public_key' => strval(rand()),
                // todo: maybe there is a better way to create fake public keys.
            ];
        } else {
            $notification = [
                'command' => 'pubkey_update',
                'result_code' => '400',
                'reason' => ['Force error.'],
            ];
        }

        $notificationProperty = [
            'correlation_id' => $correlationId,
        ];
        $this->client->sendMessage($notification, $notificationProperty);
    }


}
