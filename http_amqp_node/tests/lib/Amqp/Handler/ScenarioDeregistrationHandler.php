<?php

namespace Lcas\Test\Amqp\Handler;


class ScenarioDeregistrationHandler extends AbstractHandler {

    public function handle($request, $properties) {

        if($this->timeoutEnabled) {
            return;
        }
        if($this->sleepSeconds > 0) {
            sleep($this->sleepSeconds);
        }

        $correlationId = isset($properties['correlation_id']) ? $properties['correlation_id'] : 0;

        $notification = [];
        if(!$this->forceNgEnabled) {
            $notification = [
                'command' => 'scenario_dereg',
                'result_code' => (string)$this->resultCode,
                'scenario_id' => $request['scenario_id']
            ];
            $notification = array_merge($notification, $request);
        } else {
            $notification = [
                'command' => 'scenario_dereg',
                'result_code' => '400',
                'reason' => ['Force error.'],
            ];
        }

        $notificationProperty = [
            'correlation_id' => $correlationId,
        ];
        $this->client->sendMessage($notification, $notificationProperty);
    }


}
