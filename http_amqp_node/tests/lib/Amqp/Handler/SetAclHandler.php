<?php

namespace Lcas\Test\Amqp\Handler;


class SetAclHandler extends AbstractHandler {

    public function handle($request, $properties) {

        if($this->timeoutEnabled) {
            return;
        }
        if($this->sleepSeconds > 0) {
            sleep($this->sleepSeconds);
        }

        $correlationId = isset($properties['correlation_id']) ? $properties['correlation_id'] : 0;

        $notification = [
            'command' => 'set_acl',
            'result_code' => 300,
            'reason' => ['forced pending'],
        ];

        $notificationProperty = [
            'correlation_id' => $correlationId,
        ];
        $this->client->sendMessage($notification, $notificationProperty);

        sleep(4);

        if(!$this->forceNgEnabled) {
            $acl = $request["acl"];
            foreach ($acl as &$macKeyPair) {
                unset($macKeyPair["join_key"]);
            }

            $notification = [
                'command' => 'set_acl',
                'result_code' => 200,
                'gw_name' => $request["gw_name"],
                'network_id' => $request["network_id"],
                'acl' => $acl,
            ];

            $this->client->sendMessageToLcas($notification);
        } else {
            $notification = [
                'command' => 'set_acl',
                'result_code' => '400',
                'reason' => ['Force error.'],
            ];

            $this->client->sendMessageToLcas($notification);
        }

    }


}
