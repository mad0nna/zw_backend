<?php

namespace Lcas\Test\Constraint;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;
use Respect\Validation\Validator as v;


class DeviceConstraint extends PHPUnit_Framework_Constraint {

    private $expected;

    private $errors;

    public function __construct($deviceData) {
        parent::__construct();

        //フィクスチャのデータとHTTPレスポンスのデータはフォーマットが異なるので、
        //変換を行う。
        $this->expected = $this->convertNodeDataToHttpResponse($deviceData);
        $this->errors = [];
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other GET gateways/:id のレスポンス相当のゲートウェイの情報を含んだ配列
     */
    public function matches($other) {
        return $this->validateNode($other);
    }

    /**
     * @param array $other 検証対象のゲートウェイ場法を含んだ配列
     */
    private function validateNode($other) {
        $errors = [];
        $requiredParams = [
            'id', 'labels', 'type', 'node',
        ];
        foreach($requiredParams as $key) {
            if(!isset($other[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        $arrayCompareErrors = [];
        if(!CompareUtil::compareArray($this->expected['labels'], $other['labels'], $arrayCompareErrors)) {
            foreach($arrayCompareErrors as $e) {
                $errors[] = 'labels:' . $e;
            }
        }

        $simpleCompareparams = [
            'id', 'type', 'node'
        ];
        foreach($simpleCompareparams as $key) {
            //型も含めて一致するか確認する
            CompareUtil::verifyEquals($this->expected[$key], $other[$key], $key, $errors);
        }

        $this->errors = $errors;
        return (count($errors) == 0) ? true : false;
    }


    /**
     * ラベルを設定する
     * @param array $labels ラベル情報
     */
    public function setLabels($labels) {
        $this->expected['labels'] = $labels;
    }

    public function setNodeId($nodeId) {
        $this->expected['node'] = '/api/v3/nodes/' . $nodeId;
    }

    /**
     * ゲートウェイのフィクスチャデータをHTTPのレスポンス形式に変換する
     * @param array $deviceData ゲートウェイのフィクスチャ情報
     * @return array HTTPレスポンス用のフォーマット
     */
    private function convertNodeDataToHttpResponse($deviceData) {

        $expected = [
            'id' => (int)$deviceData['device_id'],
            'type' => (string)$deviceData['device_type'],

            //ラベルの値は初期値は空とする
            //ラベルの値を検証する場合は、setLabels()を使用する
            'labels' => [],

            //初期値は空白とする。
            //検証する場合はsetNodeId()を使用する
            'node' => '',
        ];

        return $expected;
    }


    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid device data.';
    }
}
