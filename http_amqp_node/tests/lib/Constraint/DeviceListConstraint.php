<?php

namespace Lcas\Test\Constraint;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;
use Respect\Validation\Validator as v;


/**
 * @class DeviceListConstraint
 * @brief 比較対照のデバイス一覧が、特定のデバイスの一覧を含むかどうかを検証する
 */
class DeviceListConstraint extends PHPUnit_Framework_Constraint {


    const COMPARE_TYPE_INCLUDE     = 1;  //期待されるデバイス一覧を含んでいることを確認
    const COMPARE_TYPE_MATCH       = 2;  //期待されるデバイス一覧と一致することを確認(IDが一致)
    const COMPARE_TYPE_NOT_INCLUDE = 3;  //期待されるデバイス一覧を含んでいないことを確認

    private $compareType;

    private $expectedDeviceList;

    private $errors;

    /**
     * @param array $expectedDeviceList ゲートウェイのフィクスチャなどに含まれるフォーマットのデバイスの配列(device_id, device_typeの一覧)
     */
    public function __construct($expectedDeviceList) {
        parent::__construct();

        $this->compareType = self::COMPARE_TYPE_INCLUDE;
        $this->expectedDeviceList = $expectedDeviceList;
        $this->errors = [];
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 期待値のデバイス一覧と比較対象のデバイス一覧の比較を行う
     *
     * @param array $other デバイスの一覧(GET devicesのレスポンス)を含んだ配列
     */
    public function matches($other)
    {
        $this->errors = [];

        switch($this->compareType) {
            case self::COMPARE_TYPE_INCLUDE:
                $this->verifyInclude($other);
                break;
            case self::COMPARE_TYPE_MATCH:
                $this->verifyMatch($other);
                break;
            case self::COMPARE_TYPE_NOT_INCLUDE:
                $this->verifyNotInclude($other);
                break;
        }

        if(count($this->errors) > 0) {

            $expectedIdList = array_map(function($a){
                return $a['device_id'];
            }, $this->expectedDeviceList);
            $targetIdList = array_map(function($a){
                return $a['id'];
            }, $other);

            $this->errors[] = '期待値：' . var_export($expectedIdList, true);
            $this->errors[] = '比較対象：' . var_export($targetIdList, true);
        }

        return (count($this->errors) == 0) ? true : false;
    }


    public function setCompareType($compareType) {
        $this->compareType = $compareType;
    }


    private function verifyInclude($other) {
        foreach ($this->expectedDeviceList as $idx=>$expectedDevice) {
            $found = false;
            $expectedDeviceId = $expectedDevice['device_id'];
            foreach ($other as $device) {
                if($expectedDevice['device_id'] == $device['id']) {
                    $found = true;
                    break;
                }
            }
            if(!$found) {
                $this->errors[] = "デバイスID: {$expectedDeviceId}が含まれていません。";
            }
        }
    }

    private function verifyNotInclude($other) {
        foreach ($this->expectedDeviceList as $idx=>$expectedDevice) {
            $found = false;
            $expectedDeviceId = $expectedDevice['device_id'];
            foreach ($other as $device) {
                if($expectedDevice['device_id'] == $device['id']) {
                    $found = true;
                    break;
                }
            }
            if($found) {
                $this->errors[] = "含まれないはずのデバイスID: {$expectedDeviceId}が含まれています。";
            }
        }
    }

    private function verifyMatch($other) {
        throw new \Exception('使用するケースがまだないため未実装です。');
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is device list includes expected devices.';
    }
}
