<?php

namespace Lcas\Test\Constraint;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;
use Respect\Validation\Validator as v;


class DeviceValueConstraint extends PHPUnit_Framework_Constraint {

    private $expected;

    private $errors;

    public function __construct($deviceValue) {
        parent::__construct();

        $this->expected = [
            'value' => (string)$deviceValue['value'],
            //timestampについては、2016年1月1日以降のUNIXタイムスタンプであることを検証する
            'timestamp' => (int)(strtotime('2016-01-01 00:00:00') * 1000),
        ];

        if(isset($deviceValue['unit'])) {
            $this->expected['unit'] = $deviceValue['unit'];
        }

        $this->errors = [];
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other GET devices/:id/values のレスポンスのデータ1件分を含んだ配列
     */
    public function matches($other) {
        $errors = [];
        $requiredParams = [
            'value', 'timestamp',
        ];
        foreach($requiredParams as $key) {
            if(!isset($other[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        CompareUtil::verifyEquals($this->expected['value'], $other['value'], 'value', $errors);

        //timestampについては、2016年1月1日以降のUNIXタイムスタンプであることを検証する
        if(!v::intType()->validate($other['timestamp'])) {
            $errors[] = 'timestamp: int型ではありません。';
        }
        if($this->expected['timestamp'] > $other['timestamp']) {
            $errors[] = sprintf('タイムスタンプが%sよりも古いため、異常な値の可能性があります(%s)。',
                date('Y-m-d H:i:s', $this->expected['timestamp'] / 1000),
                date('Y-m-d H:i:s', $other['timestamp'] / 1000)
            );
        }

        if(array_key_exists('unit', $this->expected)) {
            CompareUtil::verifyEquals($this->expected['unit'], $other['unit'], 'unit', $errors);
        }

        $this->errors = $errors;
        return (count($errors) == 0) ? true : false;
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid device value.';
    }
}
