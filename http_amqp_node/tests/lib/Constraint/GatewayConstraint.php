<?php

namespace Lcas\Test\Constraint;


use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;
use Respect\Validation\Validator as v;
use Symfony\Component\HttpFoundation\ParameterBag;


class GatewayConstraint extends PHPUnit_Framework_Constraint {

    private $expected;

    private $errors;

    public function __construct($gatewayData) {
        parent::__construct();

        //フィクスチャのデータとHTTPレスポンスのデータはフォーマットが異なるので、
        //変換を行う。
        $this->expected = $this->convertGatewayDataToHttpResponse($gatewayData);
        $this->errors = [];
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other GET gateways/:id のレスポンス相当のゲートウェイの情報を含んだ配列
     */
    public function matches($other) {
        return $this->validateGateway($other);
    }

    /**
     * @param array $other 検証対象のゲートウェイ場法を含んだ配列
     */
    private function validateGateway($other) {
        $errors = [];
        $requiredParams = [
            'id', 'labels', 'mac_address', 'gw_name', 'fw_version', 'manufacturer',
            'nodes', 'capabilities', 'status'
        ];
        foreach($requiredParams as $key) {
            if(!isset($other[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        if(!ctype_digit((string)$other['id'])) {
            $errors[] = 'idが数値ではありません。';
        }

        $arrayCompareErrors = [];
        if(!$this->arrayCompare($this->expected['labels'], $other['labels'], $arrayCompareErrors)) {
            foreach($arrayCompareErrors as $e) {
                $errors[] = 'labels:' . $e;
            }
        }
        $arrayCompareErrors = [];
        if(!$this->arrayCompare($this->expected['nodes'], $other['nodes'], $arrayCompareErrors)) {
            foreach($arrayCompareErrors as $e) {
                $errors[] = 'nodes:' . $e;
            }
        }
        $arrayCompareErrors = [];
        if(!$this->arrayCompare($this->expected['capabilities'], $other['capabilities'], $arrayCompareErrors)) {
            foreach($arrayCompareErrors as $e) {
                $errors[] = 'capabilities:' . $e;
            }
        }

        $simpleCompareparams = [
            'mac_address', 'fw_version', 'status', 'gw_name'
        ];
        foreach($simpleCompareparams as $key) {
            $expectedType = gettype($this->expected[$key]);
            $targetType = gettype($other[$key]);
            if($this->expected[$key] != $other[$key]) {
                $expectedValue = $this->expected[$key];
                $targetValue = $other[$key];
                $errors[] = "{$key}: 値が一致しません. 期待値:{$expectedValue}, 比較対象:{$targetValue}";
            } else if($expectedType != $targetType) {
                $errors[] = "{$key}: 型が一致しません. 期待値:{$expectedType}, 比較対象:{$targetType}";
            }
        }

        $this->errors = $errors;
        return (count($errors) == 0) ? true : false;
    }


    /**
     * ゲートウェイのラベルを設定する
     * @param array $labels ラベル情報
     */
    public function setGatewayLabels($labels) {
        $this->expected['labels'] = $labels;
    }

    /**
     * ゲートウェイのステータスを設定する
     * @param string $status ステータス情報
     */
    public function setGatewayStatus($status) {
        $this->expected['status'] = $status;
    }


    /**
     * ノードのラベルを設定する
     * @param integer $nodeId ノードID
     * @param array $labels ラベル情報
     */
    public function setNodeLabels($nodeId, $labels) {
        $found = false;
        foreach($this->expected['nodes'] as $idx=>$node) {
            if($node['node_id'] == $nodeId) {
                $this->expected['nodes'][$idx]['labels'] = $labels;
                $found = true;
                break;
            }
        }

        if(!$found) {
            throw new \InvalidArgumentException('ラベルを設定する対象のノードが見つかりません. node_id:' . $nodeId);
        }
    }


    /**
     * ゲートウェイのフィクスチャデータをHTTPのレスポンス形式に変換する
     * @param array $gatewayData ゲートウェイのフィクスチャ情報
     * @return array HTTPレスポンス用のフォーマット
     */
    private function convertGatewayDataToHttpResponse($gatewayData) {

        $expected = [
            //IDは数値であることを条件とする
            'id' => v::intVal(),
            //ラベルの値は初期値は空とする
            //ラベルの値を顕彰する場合は、setGatewayLabels()を使用する
            'labels' => [],
            'mac_address' => (string)$gatewayData['gw_name'],
            'gw_name' => (string)$gatewayData['gw_name'],
            'fw_version' => (string)$gatewayData['fw_version'],
            'manufacturer' => (string)$gatewayData['manufacturer'],
            'nodes' => [],
            'capabilities' => $gatewayData['capabilities'],

            //異なる状態を試験する場合はsetGatewayStatus()で期待値を変更する。
            'status' => 'disconnected',
        ];

        foreach($gatewayData['nodes'] as $node) {
            //HTTPレスポンスではURLを記載する
            $expected['nodes'][] = '/api/v3/nodes/' . $node['node_id'];
        }

        return $expected;
    }

    private function arrayCompare($a, $b, &$errors) {
        $errors = [];
        if(!is_array($a) || !is_array($b)) {
            $errors[] = '期待値、または比較対象が配列ではありません。';
            return false;
        }
        $expectedCount = count($a);
        if(count($b) != $expectedCount) {
            $errors[] = "件数が一致しません。期待値:{$expectedCount}, 比較対象:" . count($b);
        }
        foreach($a as $key=>$value) {
            if(!isset($b[$key])) {
                $errors[] = "比較対照にキーが存在しません:{$key}";
            } else if($a[$key] != $b[$key]) {
                $errors[] = "期待値と値が一致しません. 期待値:{$a[$key]}, 比較対象: {$b[$key]}";
            } else if(gettype($a[$key]) != gettype($b[$key])) {
                $errors[] = '期待値と型が一致しません。期待値:' . gettype($a) . ', 比較対象:' . gettype($b);
            }
        }
        return (count($errors) == 0) ? true : false;
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid gateway data.';
    }
}
