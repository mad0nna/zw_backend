<?php

namespace Lcas\Test\Constraint;


use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;
use Respect\Validation\Validator as v;
use Symfony\Component\HttpFoundation\ParameterBag;


class GeneralErrorResponseConstraint extends PHPUnit_Framework_Constraint {

    private $errors = [];

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other GET gateways/:id のレスポンス相当のゲートウェイの情報を含んだ配列
     */
    public function matches($other) {

        if(!isset($other['errors'])) {
            $this->errors[] = 'errors キーが含まれていません';
        }
        if(!is_array($other['errors'])) {
            $this->errors[] = 'errorsが配列ではありません';
        }

        return (count($this->errors) == 0) ? true : false;
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid general error response.';
    }
}
