<?php

namespace Lcas\Test\Constraint;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;
use Respect\Validation\Validator as v;


class NodeConstraint extends PHPUnit_Framework_Constraint {

    private $expected;

    private $errors;


    /**
     * ノードのオーナー用のレスポンスとして評価するかどうか
     * @var bool
     */
    private $isOwner;

    public function __construct($deviceData) {
        parent::__construct();

        //フィクスチャのデータとHTTPレスポンスのデータはフォーマットが異なるので、
        //変換を行う。
        $this->expected = $this->convertNodeDataToHttpResponse($deviceData);
        $this->errors = [];
        $this->isOwner = true;
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other GET gateways/:id のレスポンス相当のゲートウェイの情報を含んだ配列
     */
    public function matches($other) {
        return $this->validateNode($other);
    }

    /**
     * @param array $other 検証対象のゲートウェイ情報を含んだ配列
     */
    private function validateNode($other) {
        $errors = [];
        $requiredParams = [
            'id', 'name', 'labels', 'protocol', 'sharing_level', 'devices', 'zwave', 'state',
        ];
        if($this->isOwner) {
            $requiredParams[] = 'gateway';
        }
        foreach($requiredParams as $key) {
            if(!isset($other[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        $arrayCompareErrors = [];
        if(!CompareUtil::compareArray($this->expected['labels'], $other['labels'], $arrayCompareErrors)) {
            foreach($arrayCompareErrors as $e) {
                $errors[] = 'labels:' . $e;
            }
        }

        $simpleCompareparams = [
            'id', 'name', 'protocol', 'sharing_level'
        ];
        if($this->isOwner) {
            //ノードのオーナーの場合、gatewayのキーが含まれることを確認する。
            $simpleCompareparams[] = 'gateway';
        }
        foreach($simpleCompareparams as $key) {
            //型も含めて一致するか確認する
            CompareUtil::verifyEquals($this->expected[$key], $other[$key], $key, $errors);
        }

        if($this->isOwner) {
            //ノードのオーナーの場合、shared_withを検証する
            foreach($this->expected['shared_with'] as $idx=>$shared) {
                if(isset($shared['id'])) {
                    CompareUtil::verifyEquals($shared['id'], $other['shared_with'][$idx]['id'], 'shared_with.'.$idx.'.id', $errors);
                }
                if(isset($shared['url'])) {
                    CompareUtil::verifyEquals($shared['url'], $other['shared_with'][$idx]['url'], 'shared_with.'.$idx.'.url', $errors);
                }
                if(isset($shared['email'])) {
                    CompareUtil::verifyEquals($shared['email'], $other['shared_with'][$idx]['email'], 'shared_with.'.$idx.'.email', $errors);
                }
            }
            $expectedSharedWithCount = count($this->expected['shared_with']);
            $actualSharedWithCount = count($other['shared_with']);
            if($expectedSharedWithCount != $actualSharedWithCount) {
                $errors[] = "shared_with: 件数が一致しません。期待値:({$expectedSharedWithCount}), 比較対象:({$actualSharedWithCount})";
            }
        } else {
            //ノードのオーナーでない場合、shared_withが空であることを検証する
            $shareCount = count($other['shared_with']);
            if(count($other['shared_with']) != 0) {
                $errors[] = "shared_with: 件数が一致しません。期待値:(0), 比較対象:({$shareCount})";
            }
        }

        $arrayComparator = CompareUtil::getRecursiveComparator();
        if(!$arrayComparator->compare($this->expected['devices'], $other['devices'])) {
        	$errors = array_merge($errors, $arrayComparator->getErrors());
        }
        
        if(!$arrayComparator->compare($this->expected['zwave'], $other['zwave'])) {
            $errors = array_merge($errors, $arrayComparator->getErrors());
        }

        if(isset($this->expected['zwave_configurations'])) {
            $this->validateZwaveConfigurations($other['zwave_configurations'], $errors);
        }
        if(isset($this->expected['configurations'])) {
            $this->validateConfigurations($other['configurations'], $errors);
        }
        if(isset($this->expected['app'])) {
            $this->validateConfigurations($other['app'], $errors);
        }

        $this->errors = $errors;
        return (count($errors) == 0) ? true : false;
    }


    /**
     * ラベルを設定する
     * @param array $labels ラベル情報
     */
    public function setLabels($labels) {
        $this->expected['labels'] = $labels;
    }

    public function setSharingLevel($sharingLevel) {
        $this->expected['sharing_level'] = $sharingLevel;
    }

    public function setSharedWith($sharedWith) {
        $this->expected['shared_with'] = $sharedWith;
    }

    public function setGatewayId($gatewayId) {
        $this->expected['gateway'] = '/api/v3/gateways/' . $gatewayId;
    }


    public function setIsOwner($isOwner) {
        $this->isOwner = $isOwner;
    }


    private function validateZwaveConfigurations($otherZwaveConfigurations, &$error) {
        $localErrors = [];

        foreach($this->expected['zwave_configurations'] as $idx=>$expectedConfig) {
            $arrayCompareErrors = [];
            if(!CompareUtil::compareArray($expectedConfig, $otherZwaveConfigurations[$idx], $arrayCompareErrors)) {
                foreach($arrayCompareErrors as $e) {
                    $errors[] = "zwave_configurations.{$idx}:{$e}";
                }
            }
        }

        $error = array_merge($error, $localErrors);
        return (count($localErrors) == 0) ? true : false;
    }


    private function validateConfigurations($otherConfigurations, &$error) {
        $localErrors = [];

        foreach($this->expected['configurations'] as $idx=>$expectedConfig) {
            $arrayCompareErrors = [];
            if(!CompareUtil::compareArray($expectedConfig, $otherConfigurations[$idx], $arrayCompareErrors)) {
                foreach($arrayCompareErrors as $e) {
                    $errors[] = "configurations.{$idx}:{$e}";
                }
            }
        }

        $error = array_merge($error, $localErrors);
        return (count($localErrors) == 0) ? true : false;
    }

    private function validateAppConfigurations($appConfigurations, &$error) {
        $localErrors = [];

        foreach($this->expected['app'] as $idx=>$expectedConfig) {
            $arrayCompareErrors = [];
            if(!CompareUtil::compareArray($expectedConfig, $appConfigurations[$idx], $arrayCompareErrors)) {
                foreach($arrayCompareErrors as $e) {
                    $errors[] = "app.{$idx}:{$e}";
                }
            }
        }

        $error = array_merge($error, $localErrors);
        return (count($localErrors) == 0) ? true : false;
    }

    /**
     * ゲートウェイのフィクスチャデータをHTTPのレスポンス形式に変換する
     * @param array $nodeData ゲートウェイのフィクスチャ情報
     * @return array HTTPレスポンス用のフォーマット
     */
    private function convertNodeDataToHttpResponse($nodeData) {

        $expected = [
            'id' => (int)$nodeData['node_id'],
        		
        	'name' => sprintf('%s_%s_%s', $nodeData['manufacturer'], $nodeData['product'], $nodeData['serial_no']),
        
            //ラベルの値は初期値は空とする
            //ラベルの値を検証する場合は、setLabels()を使用する
            'labels' => [],

            //初期値は空白とする。
            //検証する場合はsetgateway()を使用する
            'gateway' => '',

            'protocol' => 'zwave',
            'sharing_level' => 'private',
            'devices' => [],
            'shared_with' => [],
            'zwave' => [
                'devices' => [],	// deprecated.
                'manufacturer' => (string)$nodeData['manufacturer'],
                'product' => (string)$nodeData['product'],
                'serial_no' => (string)$nodeData['serial_no'],
            ]
        ];

        foreach($nodeData['devices'] as $device) {
            $expected['devices'][] = '/api/v3/devices/' . $device['device_id'];
        	$expected['zwave']['devices'][] = '/api/v3/devices/' . $device['device_id'];	// deprecated.
        }

        if(isset($nodeData['zwave_configurations'])) {
            $expected['zwave_configurations'] = [];
            foreach($nodeData['zwave_configurations'] as $configuration) {
                $configEntry = [
                    'parameter_no' => (int)$configuration['parameter_no'],
                    'type'         => (string)$configuration['type'],
                    'size'         => (int)$configuration['size'],
                    'value'        => (int)$configuration['value'],
                ];

                if(isset($configuration['update_pending'])) {
                    $configEntry['update_pending'] = (bool)$configuration['update_pending'];
                }

                $expected['zwave_configurations'][] = $configEntry;
            }
        }
        if(isset($nodeData['configurations'])) {
            $expected['configurations'] = [];
            foreach($nodeData['configurations'] as $configuration) {
                $configEntry = [
                    'type'      => (int)$configuration['type'],
                    'parameter' => (string)$configuration['parameter'],
                    'value'     => (string)$configuration['value'],
                ];

                if(isset($configuration['update_pending'])) {
                    $configEntry['update_pending'] = (bool)$configuration['update_pending'];
                }
                $expected['configurations'][] = $configEntry;
            }
        }

        return $expected;
    }


    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid node data.';
    }
}
