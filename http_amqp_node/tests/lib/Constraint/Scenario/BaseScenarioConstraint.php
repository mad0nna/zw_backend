<?php

namespace Lcas\Test\Constraint\Scenario;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;
use Respect\Validation\Validator as v;


/**
 * scenario_typeを問わず、共通して行われるバリデーションの内容を定義
 */
class BaseScenarioConstraint extends PHPUnit_Framework_Constraint {

    /**
     * POST gatewaysなどで使用されている形式のデータを含んだ配列
     * @var array
     */
    protected $expected;

    protected $errors;

    /**
     * @param array $data  POST scenariosなどで使用されている形式のデータを含んだ配列
     */
    public function __construct($data) {
        parent::__construct();

        //入力データを初期化する
        //scenario_typeによらず共通した項目を初期化する。
        //scenario_type別の項目は各サブクラスで初期化する。
        $this->expected = [
            'scenario_type'   => (string)$data['scenario_type'],
            'labels'          => [],
            'actions'         => [],
            'enabled'         => (bool)$data['enabled'],
        ];

        foreach($data['labels'] as $label) {
            $this->expected['labels'][] = (string)$label;
        }
        foreach($data['actions'] as $action) {
            $this->expected['actions'][] = [
                'type'    => (string)$action['type'],
                'content' => (string)$action['content'],
            ];
        }

        if(isset($data['enabled_period'])) {
            $this->expected['enabled_period'] = $data['enabled_period'];
        }
        if(isset($data['enabled_days'])) {
            $this->expected['enabled_days'] = $data['enabled_days'];
        }
        $this->errors = [];
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other GET gateways/:id のレスポンス相当のゲートウェイの情報を含んだ配列
     */
    public function matches($other) {
        throw new \Exception('実装されていません。');
    }


    /**
     * 各処理で共通して実装されている処理
     * @param $other
     * @param $errors
     * @return bool
     */
    public function validateCommonFields($other, &$errors) {
        $localErrors = [];
        $requiredParams = [
            'scenario_type', 'labels', 'actions', 'enabled'
        ];
        foreach($requiredParams as $key) {
            if(!isset($other[$key])) {
                $localErrors[] = $key . 'が含まれていません。';
            }
        }

        $arrayCompareErrors = [];
        if(!CompareUtil::compareArray($this->expected['labels'], $other['labels'], $arrayCompareErrors)) {
            foreach($arrayCompareErrors as $e) {
                $localErrors[] = 'labels:' . $e;
            }
        }
        $arrayCompareErrors = [];
        if(!CompareUtil::compareArray($this->expected['actions'], $other['actions'], $arrayCompareErrors)) {
            foreach($arrayCompareErrors as $e) {
                $localErrors[] = 'actions:' . $e;
            }
        }
        if(isset($this->expected['enabled_days'])) {
            $arrayCompareErrors = [];
            if(!CompareUtil::compareArray($this->expected['enabled_days'], $other['enabled_days'], $arrayCompareErrors)) {
                foreach($arrayCompareErrors as $e) {
                    $localErrors[] = 'enabled_days:' . $e;
                }
            }
        }

        $simpleCompareparams = [
            'scenario_type',
            'enabled'
        ];

        if(isset($this->expected['enabled_period'])) {
            $simpleCompareparams[] = 'enabled_period';
        }
        foreach($simpleCompareparams as $key) {
            $expectedType = gettype($this->expected[$key]);
            $targetType = gettype($other[$key]);
            if($this->expected[$key] != $other[$key]) {
                $expectedValue = $this->expected[$key];
                $targetValue = $other[$key];
                $localErrors[] = "{$key}: 値が一致しません. 期待値:{$expectedValue}, 比較対象:{$targetValue}";
            } else if($expectedType != $targetType) {
                $localErrors[] = "{$key}: 型が一致しません. 期待値:{$expectedType}, 比較対象:{$targetType}";
            }
        }

        $errors = array_merge($errors, $localErrors);

        return (count($localErrors) == 0) ? true : false;
    }


    /**
     * ゲートウェイのラベルを設定する
     * @param array $labels ラベル情報
     */
    public function setLabels($labels) {
        $this->expected['labels'] = $labels;
    }


    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        throw new \Exception('実装されていません。');
    }
}
