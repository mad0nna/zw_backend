<?php

namespace Lcas\Test\Constraint\Scenario;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;

class ScenarioEventConstraint extends PHPUnit_Framework_Constraint {

    /**
     * GET scenarios/all/eventsなどで使用されているデータ1件分を含んだ配列
     * @var array
     */
    private $expected;

    private $errors;

    /**
     * @param array $data  POST gatewaysなどで使用されている形式のデータを含んだ配列
     */
    public function __construct($data) {
        parent::__construct();

        $this->expected = [
            'id' => (int)$data['id'],
            'timestamp' => (int)$data['timestamp'],
        ];
        $this->errors = [];
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other GET scenarios/all/events の1件分のデータを含んだ配列
     */
    public function matches($other) {

        $errors = [];
        $requiredParams = [
            'id', 'timestamp'
        ];
        foreach($requiredParams as $key) {
            if(!isset($other[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        CompareUtil::compareArray($this->expected, $other, $errors);

        $this->errors = $errors;
        return (count($errors) == 0) ? true : false;

    }


    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid scenario event.';
    }
}
