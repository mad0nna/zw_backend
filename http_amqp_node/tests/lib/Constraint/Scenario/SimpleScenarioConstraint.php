<?php

namespace Lcas\Test\Constraint\Scenario;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;
use Respect\Validation\Validator as v;


class SimpleScenarioConstraint extends BaseScenarioConstraint {

    /**
     * @param array $data  POST scenariosなどで使用されている形式のデータを含んだ配列
     */
    public function __construct($data) {

        //共通の項目の初期化処理を実行する
        parent::__construct($data);

        //simple固有の初期化処理
        $expectedSimpleScenario = [
            'condition_operator' => (string)$data['condition_operator'],
            'conditions' => [],
            'trigger_after' => (int)$data['trigger_after'],
        ];

        foreach($data['conditions'] as $condition) {
            $expectedCondition = [
                'lhs' => (string)$condition['lhs'],
                'operator' => (string)$condition['operator'],
                'rhs' => (string)$condition['rhs'],
            ];
            if(isset($condition['unit'])) {
                $expectedCondition['unit'] = (string)$condition['unit'];
            }
            $expectedSimpleScenario['conditions'][] = $expectedCondition;
        }

        //共通の項目とマージする
        $this->expected = array_merge($this->expected, $expectedSimpleScenario);
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other GET gateways/:id のレスポンス相当のゲートウェイの情報を含んだ配列
     */
    public function matches($other) {
        $errors = [];

        //共通の項目のバリデーションを行う。
        $this->validateCommonFields($other, $errors);

        //simple固有のバリデーションを行う。
        $requiredParams = [
            'condition_operator','conditions', 'trigger_after'
        ];
        foreach($requiredParams as $key) {
            if(!isset($other[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        //conditionsの各要素が一致するかを確認する
        $recursiveComparator = CompareUtil::getRecursiveComparator();
        if(!$recursiveComparator->compare($this->expected['conditions'], $other['conditions'])) {
            $errors = array_merge($errors, $recursiveComparator->getErrors());
        }

        $simpleCompareparams = [
            'condition_operator',
            'trigger_after'
        ];
        foreach($simpleCompareparams as $key) {
            $expectedType = gettype($this->expected[$key]);
            $targetType = gettype($other[$key]);
            if($this->expected[$key] != $other[$key]) {
                $expectedValue = $this->expected[$key];
                $targetValue = $other[$key];
                $errors[] = "{$key}: 値が一致しません. 期待値:{$expectedValue}, 比較対象:{$targetValue}";
            } else if($expectedType != $targetType) {
                $errors[] = "{$key}: 型が一致しません. 期待値:{$expectedType}, 比較対象:{$targetType}";
            }
        }

        $this->errors = $errors;

        return (count($this->errors) == 0) ? true : false;
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid simple scenario data.';
    }
}
