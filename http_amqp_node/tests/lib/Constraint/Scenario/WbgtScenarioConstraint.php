<?php

namespace Lcas\Test\Constraint\Scenario;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;
use Respect\Validation\Validator as v;


class WbgtScenarioConstraint extends BaseScenarioConstraint {

    /**
     * @param array $data  POST scenariosなどで使用されている形式のデータを含んだ配列
     */
    public function __construct($data) {

        //共通の項目の初期化処理を実行する
        parent::__construct($data);

        //wbgt固有の初期化処理
        $expectedWbgtScenario = [
            'temperature_dev' => (int)$data['temperature_dev'],
            'humidity_dev'    => (int)$data['humidity_dev'],
            'wbgt_threshold'  => (int)$data['wbgt_threshold'],
        ];

        //共通の項目とマージする
        $this->expected = array_merge($this->expected, $expectedWbgtScenario);
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other GET gateways/:id のレスポンス相当のゲートウェイの情報を含んだ配列
     */
    public function matches($other) {
        $errors = [];

        //共通の項目のバリデーションを行う。
        $this->validateCommonFields($other, $errors);

        //wbgt固有のバリデーションを行う。
        $requiredParams = [
            'temperature_dev', 'humidity_dev', 'wbgt_threshold',
        ];
        foreach($requiredParams as $key) {
            if(!isset($other[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        $simpleCompareparams = [
            'temperature_dev', 'temperature_dev', 'humidity_dev', 'wbgt_threshold',
        ];
        foreach($simpleCompareparams as $key) {
            $expectedType = gettype($this->expected[$key]);
            $targetType = gettype($other[$key]);
            if($this->expected[$key] != $other[$key]) {
                $expectedValue = $this->expected[$key];
                $targetValue = $other[$key];
                $errors[] = "{$key}: 値が一致しません. 期待値:{$expectedValue}, 比較対象:{$targetValue}";
            } else if($expectedType != $targetType) {
                $errors[] = "{$key}: 型が一致しません. 期待値:{$expectedType}, 比較対象:{$targetType}";
            }
        }

        $this->errors = $errors;
        return (count($this->errors) == 0) ? true : false;
    }


    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid wbgt scenario data.';
    }
}
