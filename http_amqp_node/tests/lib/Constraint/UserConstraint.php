<?php

namespace Lcas\Test\Constraint;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;
use Respect\Validation\Validator as v;


class UserConstraint extends PHPUnit_Framework_Constraint {

    private $expected;

    private $errors;

    public function __construct($user) {
        parent::__construct();

        $this->expected = [
            'id'       => (int)$user['id'],
            'email'    => (string)$user['email'],
            'platform' => (string)$user['platform'],
            'token'    => (isset($user['token'])) ? (string)$user['token'] : '',
        ];
        $this->errors = [];
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other GET users/self のレスポンスのデータを含んだ配列
     */
    public function matches($other) {
        $errors = [];
        $requiredParams = [
            'id', 'email', 'platform', 'token'
        ];
        foreach($requiredParams as $key) {
            if(!isset($other[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        $simpleCompareparams = [
            'id', 'email', 'platform', 'token'
        ];
        foreach($simpleCompareparams as $key) {
            $expectedType = gettype($this->expected[$key]);
            $targetType = gettype($other[$key]);
            if($this->expected[$key] != $other[$key]) {
                $expectedValue = $this->expected[$key];
                $targetValue = $other[$key];
                $errors[] = "{$key}: 値が一致しません. 期待値:{$expectedValue}, 比較対象:{$targetValue}";
            } else if($expectedType != $targetType) {
                $errors[] = "{$key}: 型が一致しません. 期待値:{$expectedType}, 比較対象:{$targetType}";
            }
        }

        $this->errors = $errors;
        return (count($errors) == 0) ? true : false;
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid user information.';
    }
}
