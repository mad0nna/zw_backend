<?php

namespace Lcas\Test\Constraint\WebSocket;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;


class DeviceDataUpdateConstraint extends PHPUnit_Framework_Constraint {

    private $expected;

    private $errors;

    public function __construct($deviceValue) {
        parent::__construct();

        //フィクスチャのデータをWebSocket APIの通知のフォーマットに変換する
        $this->expected = [
            'id'        => (int)$deviceValue['id'],
            'value'     => (string)$deviceValue['value'],
            'unit'      => (isset($deviceValue['unit'])) ? (string)$deviceValue['unit'] : null,
            'timestamp' => (int)$deviceValue['timestamp'],
        ];

        $this->errors = [];
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other Node Inclusion Notificationの情報を含んだ配列
     */
    public function matches($other) {
        $errors = [];

        $this->verifyNotificationType($other, $errors);

        $otherContent = $other['content'];
        $requiredParams = [
            'id', 'value', 'timestamp',
        ];
        foreach($requiredParams as $key) {
            if(!isset($otherContent[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        $simpleCompareParams = [
            'id', 'value', 'timestamp'
        ];

        if(!is_null($this->expected['unit'])) {
            $simpleCompareParams[] = 'unit';
        }

        foreach($simpleCompareParams as $key) {
            //型も含めて一致するか確認する
            CompareUtil::verifyEquals($this->expected[$key], $otherContent[$key], $key, $errors);
        }

        $this->errors = $errors;
        return (count($errors) == 0) ? true : false;

    }


    private function verifyNotificationType($other, &$errors) {
        $localErrors = [];
        if(!isset($other['notification_type'])) {
            $localErrors[] = 'notification_typeが存在しません';
        }

        if($other['notification_type'] != 'device_data_update') {
            $localErrors[] = 'notification_typeが一致しません。: ' . $other['notification_type'];
        }

        $errors = array_merge($errors, $localErrors);
        return (count($localErrors) == 0) ? true : false;
    }


    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid device data update notification.';
    }
}
