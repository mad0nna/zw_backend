<?php

namespace Lcas\Test\Constraint\WebSocket;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;


class GeneralErrorConstraint extends PHPUnit_Framework_Constraint {

    private $expected;

    private $errors;

    public function __construct($error) {
        parent::__construct();

        //フィクスチャのデータをWebSocket APIの通知のフォーマットに変換する
        $this->expected = $this->convertNodeFixtureDataToNotification($error);
        $this->errors = [];
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other General Error Notificationの情報を含んだ配列
     */
    public function matches($other) {
        $errors = [];

        $this->verifyNotificationType($other, $errors);

        // Notification に含まれるべき情報の確認
        $otherContent = $other['content'];
        $requiredParams = [
            'error_id', 'detail'
        ];
        foreach($requiredParams as $key) {
            if(!isset($otherContent[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        $arrayCompareErrors = [];
        
        $simpleCompareParams = [
            'error_id', 'detail'
        ];
        foreach($simpleCompareParams as $key) {
            //型も含めて一致するか確認する
            CompareUtil::verifyEquals($this->expected['content'][$key], $otherContent[$key], $key, $errors);
        }

        $this->errors = $errors;
        return (count($errors) == 0) ? true : false;

    }


    private function verifyNotificationType($other, &$errors) {
        $localErrors = [];
        if(!isset($other['notification_type'])) {
            $localErrors[] = 'notification_typeが存在しません';
        }

        if($other['notification_type'] != 'general_error') {
            $localErrors[] = 'notification_typeが一致しません。: ' . $other['notification_type'];
        }

        $errors = array_merge($errors, $localErrors);
        return (count($localErrors) == 0) ? true : false;
    }


    /**
     * フィクスチャのデータをWebSocketの通知形式のフォーマットに変換する
     * @param array $nodeData
     * @return array
     */
    private function convertNodeFixtureDataToNotification($error) {
    	
        $notification = [
        	'notification_type' => (string)$error['notification_type'],
        	'content' => [
	            'error_id' => (int)$error['content']['error_id'],
	        	'detail'   => $error['content']['detail'],
    		]
        ];
        
        return $notification;
    }


    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid general error notification.';
    }
}
