<?php

namespace Lcas\Test\Constraint\WebSocket;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;


class NodeInclusionConstraint extends PHPUnit_Framework_Constraint {

    private $expected;

    private $errors;

    public function __construct($nodeData) {
        parent::__construct();

        //フィクスチャのデータをWebSocket APIの通知のフォーマットに変換する
        $this->expected = $this->convertNodeFixtureDataToNotification($nodeData);
        $this->errors = [];
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other Node Inclusion Notificationの情報を含んだ配列
     */
    public function matches($other) {
        $errors = [];

        $this->verifyNotificationType($other, $errors);

        // Notification に含まれるべき情報の確認
        $otherContent = $other['content'];
        $requiredParams = [
            'id', 'labels', 'gateway', 'protocol', 'sharing_level', 'shared_with',
            'zwave',
        ];
        foreach($requiredParams as $key) {
            if(!isset($otherContent[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        $arrayCompareErrors = [];
        
        // ラベルの一致
        if(!CompareUtil::compareArray($this->expected['labels'], $otherContent['labels'], $arrayCompareErrors)) {
            foreach($arrayCompareErrors as $e) {
                $errors[] = 'labels:' . $e;
            }
        }

        $simpleCompareParams = [
            'id', 'gateway', 'sharing_level'
        ];
        foreach($simpleCompareParams as $key) {
            //型も含めて一致するか確認する
            CompareUtil::verifyEquals($this->expected[$key], $otherContent[$key], $key, $errors);
        }

        //Node Inclusion時点では、shared_withは空である事を検証する
        if(!isset($otherContent['shared_with'])) {
            $errors[] = 'shared_withが含まれていません';
        }
        if(!is_array($otherContent['shared_with'])) {
            $errors[] = 'shared_withが配列ではありません';
        }
        if(count($otherContent['shared_with']) != 0) {
            $errors[] = 'shared_withが0件ではありません';
        }

        //"zwave"フィールド内の検証
        $this->verifyZwaveField($otherContent['zwave'], $errors);

        $this->errors = $errors;
        return (count($errors) == 0) ? true : false;

    }


    private function verifyNotificationType($other, &$errors) {
        $localErrors = [];
        if(!isset($other['notification_type'])) {
            $localErrors[] = 'notification_typeが存在しません';
        }

        if($other['notification_type'] != 'node_inclusion') {
            $localErrors[] = 'notification_typeが一致しません。: ' . $other['notification_type'];
        }

        $errors = array_merge($errors, $localErrors);
        return (count($localErrors) == 0) ? true : false;
    }


    private function verifyZwaveField($otherZwaveField, &$errors) {
        $localErrors = [];
        $expectedZwaveField = $this->expected['zwave'];

        $simpleCompareParams = [
            'manufacturer', 'product', 'serial_no'
        ];

        if(isset($this->expected['is_repeater'])) {
            //必須項目ではない、is_repeaterのチェック
            $simpleCompareParams[] = 'is_repeater';
        }
        foreach($simpleCompareParams as $key) {
            //型も含めて一致するか確認する
            CompareUtil::verifyEquals($expectedZwaveField[$key], $otherZwaveField[$key], 'zwave.'.$key, $localErrors);
        }

        // failed_cmdの一致
        $arrayCompareErrors = [];
        if (!isset($otherZwaveField['failed_cmd'])) {
        	if (!CompareUtil::compareArray($expectedZwaveField['failed_cmd'], array(), $arrayCompareErrors)) {        	
        		foreach($arrayCompareErrors as $e) {
            		$errors[] = 'failed_cmd:' . $e;
            	}
        	}
        } else if(!CompareUtil::compareArray($expectedZwaveField['failed_cmd'], $otherZwaveField['failed_cmd'], $arrayCompareErrors)) {
            foreach($arrayCompareErrors as $e) {
                $errors[] = 'failed_cmd:' . $e;
            }
        }

        //デバイス一覧の検証(デバイスIDのURLを含んだ一覧が一致するか)
        if(!CompareUtil::compareArray($expectedZwaveField['devices'], $otherZwaveField['devices'], $arrayCompareErrors)) {
            foreach($arrayCompareErrors as $e) {
                $localErrors[] = 'devices:' . $e;
            }
        }

        $errors = array_merge($errors, $localErrors);
        return (count($localErrors) == 0) ? true : false;
    }

    /**
     * ラベルを設定する
     * @param array $labels ラベル情報
     */
    public function setLabels($labels) {
        $this->expected['labels'] = $labels;
    }

    public function setSharingLevel($sharingLevel) {
        $this->expected['sharing_level'] = $sharingLevel;
    }

    public function setSharedWith($sharedWith) {
        $this->expected['shared_with'] = $sharedWith;
    }

    public function setGatewayId($gatewayId) {
        $this->expected['gateway'] = '/api/v3/gateways/' . $gatewayId;
    }

    public function setIsRepeater($isRepeater) {
        if(!$isRepeater) {
            unset($this->expected['is_repeater']);
        } else {
            $this->expected['is_repeater'] = true;
        }
    }


    /**
     * フィクスチャのデータをWebSocketの通知形式のフォーマットに変換する
     * @param array $nodeData
     * @return array
     */
    private function convertNodeFixtureDataToNotification($nodeData) {
    	
    	$state = isset($nodeData['failed_cmd']) && is_array($nodeData['failed_cmd']) && 0 < count($nodeData['failed_cmd']) ?
    		'failed' : 'included';

        $notification = [
            'id' => (int)$nodeData['node_id'],
            'labels' => [],
            'gateway' => '',
            'protocol' => 'zwave',
            'sharing_level' => 'private',
            'shared_with' => [],
            'zwave' => [
                'devices' => [],
                'manufacturer' => (string)$nodeData['manufacturer'],
                'product' => (string)$nodeData['product'],
                'serial_no' => (string)$nodeData['serial_no'],
                //'is_repeater' => false,
                'state' => $state,
            	'failed_cmd' => isset($nodeData['failed_cmd']) ? $nodeData['failed_cmd'] : array(),
            ],
            'zwave_configurations' => [],
            'configurations' => [],
        ];

        foreach($nodeData['devices'] as $device) {
            $url = '/api/v3/devices/' . $device['device_id'];
            $notification['zwave']['devices'][] = $url;
        }

        return $notification;
    }


    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid node inclusion notification.';
    }
}
