<?php

namespace Lcas\Test\Constraint\WebSocket;


use Lcas\Test\Util\CompareUtil;
use PHPUnit_Framework_Constraint;
use PHPUnit_Framework_ExpectationFailedException;
use SebastianBergmann\Comparator\ComparisonFailure;


class ScenarioMessageConstraint extends PHPUnit_Framework_Constraint {

    private $expected;

    private $errors;

    public function __construct($scenarioMessage) {
        parent::__construct();

        //フィクスチャのデータをWebSocket APIの通知のフォーマットに変換する
        $this->expected = [
            'id'        => (int)$scenarioMessage['id'],
            'message'   => (string)$scenarioMessage['message'],
            'timestamp' => (int)$scenarioMessage['timestamp'],
        ];

        $this->errors = [];
    }

    /**
     * エラー時に出力する情報を返す。
     * @param mixed $other
     * @param string $description
     * @param ComparisonFailure $comparisonFailure
     */
    protected function fail($other, $description, ComparisonFailure $comparisonFailure = null) {
        throw new PHPUnit_Framework_ExpectationFailedException(
            implode("\n", $this->errors),
            $comparisonFailure
        );
    }

    /**
     * 想定どおりの情報を含んでいるかの確認を行う。
     * @param array $other Node Inclusion Notificationの情報を含んだ配列
     */
    public function matches($other) {
        $errors = [];

        $this->verifyNotificationType($other, $errors);

        $otherContent = $other['content'];
        $requiredParams = [
            'id', 'message', 'timestamp',
        ];
        foreach($requiredParams as $key) {
            if(!isset($otherContent[$key])) {
                $errors[] = $key . 'が含まれていません。';
            }
        }

        $simpleCompareParams = [
            'id', 'message'
        ];

        foreach($simpleCompareParams as $key) {
            //型も含めて一致するか確認する
            CompareUtil::verifyEquals($this->expected[$key], $otherContent[$key], $key, $errors);
        }

        //timestampはテスト側からは値を制御出来ないので、
        //直近60秒以内のミリ秒精度の時間かどうかで判定する
        $currentTimeInMilliSec = (int)(microtime(true) * 1000);
        $difference = $otherContent['timestamp'] - $currentTimeInMilliSec;
        if($difference >= (60 * 1000)) {
            $errors[] = 'timestampの値が無効です。';
        }

        $this->errors = $errors;
        return (count($errors) == 0) ? true : false;

    }


    private function verifyNotificationType($other, &$errors) {
        $localErrors = [];
        if(!isset($other['notification_type'])) {
            $localErrors[] = 'notification_typeが存在しません';
        }

        if($other['notification_type'] != 'scenario_message') {
            $localErrors[] = 'notification_typeが一致しません。: ' . $other['notification_type'];
        }

        $errors = array_merge($errors, $localErrors);
        return (count($localErrors) == 0) ? true : false;
    }


    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return 'is valid scenario message notification.';
    }
}
