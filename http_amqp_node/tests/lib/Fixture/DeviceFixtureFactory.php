<?php

namespace Lcas\Test\Fixture;



class DeviceFixtureFactory {

    private $devices;


    private $devicesPerGateway;

    public function __construct() {
        $this->devicesPerGateway = [];
        $this->loadFixture();
    }

    public function loadFixture() {
        $gatewayFactory = new GatewayFixtureFactory();

        $gateways = $gatewayFactory->getAllGateways();
        foreach($gateways as $gateway) {
            $macAddress = $gateway['gw_name'];
            $this->devicesPerGateway[$macAddress] = [];
            foreach($gateway['nodes'] as $node) {

                foreach($node['devices'] as $device) {
                    $deviceId = $device['device_id'];
                    $this->devices[$deviceId] = $device;
                    $this->devicesPerGateway[$macAddress][] = $device;

                }
            }
        }
    }

    public function create($deviceId) {
        if(!array_key_exists($deviceId, $this->devices)) {
            throw new \Exception('テスト用デバイスが存在しません: ' . $deviceId);
        }

        return $this->devices[$deviceId];
    }


    public function getDeviceListFromMacAddress($mac) {
        if(!array_key_exists($mac, $this->devicesPerGateway)) {
            throw new \Exception('テスト用ゲートウェイが存在しません。 mac:' . $mac);
        }

        return $this->devicesPerGateway[$mac];
    }


    /**
     * ノードIDとデバイス種別を基にフィクスチャを検索し、該当するデバイスの情報を返す。
     * @param int $nodeId
     * @param string $deviceType
     */
    public function findDeviceFromNodeAndDeviceType($nodeId, $deviceType) {
        $nodeFactory = new NodeFixtureFactory();
        $node = $nodeFactory->create($nodeId);

        foreach($node['devices'] as $device) {
            if($device['device_type'] == $deviceType) {
                return $device;
            }
        }
        throw new \Exception("対応するデバイスが見つかりません。node_id={$nodeId}, device_type={$deviceType}");
    }
}
