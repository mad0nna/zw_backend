<?php

namespace Lcas\Test\Fixture;



class GatewayFixtureFactory {

    private $gateways;

    public function __construct() {
        $this->loadFixture();
    }

    public function loadFixture() {
        $fixturePath = TEST_FIXTURE_DIR . '/gateways.php';
        $this->gateways = require($fixturePath);
    }

    public function create($macAddress) {
        if(!array_key_exists($macAddress, $this->gateways)) {
            throw new \Exception('テスト用ゲートウェイが存在しません: ' . $macAddress);
        }

        return $this->gateways[$macAddress];
    }


    public function getAllGateways() {
        return $this->gateways;
    }

}
