<?php

namespace Lcas\Test\Fixture;



class NodeFixtureFactory {

    private $nodes;


    private $nodesPergateway;

    public function __construct() {
        $this->loadFixture();
    }

    public function loadFixture() {
        $gatewayFactory = new GatewayFixtureFactory();

        $gateways = $gatewayFactory->getAllGateways();
        foreach($gateways as $gateway) {
            $macAddress = $gateway['gw_name'];
            $this->nodesPergateway[$macAddress] = [];
            foreach($gateway['nodes'] as $node) {
                $nodeId = $node['node_id'];
                $this->nodes[$nodeId] = $node;
                $this->nodesPergateway[$macAddress][] = $node;
            }
        }
    }

    public function create($nodeId) {
        if(!array_key_exists($nodeId, $this->nodes)) {
            throw new \Exception('テスト用ノードが存在しません: ' . $nodeId);
        }

        return $this->nodes[$nodeId];
    }


    public function getNodeListFromMacAddress($mac) {
        if(!array_key_exists($mac, $this->$this->nodesPergateway)) {
            throw new \Exception('テスト用ゲートウェイが存在しません。 mac:' . $mac);
        }

        return $this->nodesPergateway[$mac];
    }
}
