<?php

namespace Lcas\Test\Fixture;



class ScenarioFixtureFactory {

    private $scenarios;

    public function __construct() {
        $this->loadFixture();
    }

    public function loadFixture() {
        $fixturePath = TEST_FIXTURE_DIR . '/scenarios.php';
        $this->scenarios = require($fixturePath);
    }

    public function create($scenarioName) {
        if(!array_key_exists($scenarioName, $this->scenarios)) {
            throw new \Exception('テスト用シナリオが存在しません: ' . $scenarioName);
        }

        return $this->scenarios[$scenarioName];
    }


    public function getAllScenarios() {
        return $this->scenarios;
    }

}
