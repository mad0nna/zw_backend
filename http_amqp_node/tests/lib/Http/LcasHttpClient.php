<?php

namespace Lcas\Test\Http;



use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;

class LcasHttpClient extends Client{

    public function __construct($config=[]) {
        if(!isset($config['base_uri'])) {
            $config['base_uri'] = TEST_HTTP_API_BASE_URL;
        }
        if(!isset($config['cookies'])) {
            $config['cookies'] = true;
        }
        parent::__construct($config);
    }



    public function login($email, $pass) {
        $json = json_encode(['email' => $email, 'password' => $pass]);
        $response = $this->request('POST', '/api/v3/login', [
            'body' => $json
        ]);

        sleep(4);

        return $response;
    }

}
