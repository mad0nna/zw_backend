<?php

namespace Lcas\Test;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;
use Lcas\DB\DB;
use Lcas\Log\Logger;
use Lcas\Repository\DeviceRepository;
use Lcas\Repository\NodeRepository;
use Lcas\Test\Amqp\AmqpClient;
use Lcas\Test\Constraint\GeneralErrorResponseConstraint;
use Lcas\Test\Util\GatewayUtil;
use Lcas\Test\Util\NodeUtil;
use Lcas\Test\Util\UserUtil;
use Lcas\Test\Util\ScenarioUtil;


class IntegrationTestCase extends \PHPUnit_Framework_TestCase {

    /**
     * @var Client
     */
    protected $httpClient;

    protected $amqpClient;

    /**
     * @var UserUtil
     */
    protected $userUtil;

    /**
     * @var NodeUtil
     */
    protected $nodeUtil;

    /**
     * @var GatewayUtil
     */
    protected $gatewayUtil;
    
    /**
     * @var ScenarioUtil
     */
    protected $scenarioUtil;

    /**
     * コンストラクタ
     *
     * パラメータはdataProviderを使用したテストのために必要。
     *
     * 併せて、親クラスの__construct()を呼び出すことも必要です。
     *
     * @param mixed $name
     * @param mixed $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->httpClient = new Client([
            'base_uri' => TEST_HTTP_API_BASE_URL,
            'cookies'  => true,
        ]);

        $this->amqpClient = new AmqpClient();
        $this->userUtil = new UserUtil($this->httpClient);
        $this->nodeUtil = new NodeUtil($this->httpClient);
        $this->gatewayUtil = new GatewayUtil($this->httpClient);
        $this->scenarioUtil = new ScenarioUtil($this->httpClient);
    }


    /**
     * 各テスト項目開始時に必ず実行する処理
     */
    public function setUp() {
        $logger = Logger::get();
        $logger->addDebug('');
        $logger->addDebug('=== START TEST: ' .  $this->getName() . " ======================");
        $logger->addDebug('');

        //ダミーのAMQPサーバーの設定を初期化する
        $this->amqpClient->requestForceNgEnabled(false);
        $this->amqpClient->requestResultCode(200);
        $this->amqpClient->requestSleep(0);
        $this->amqpClient->requestTimeout(false);
    }


    /**
     * Device Data Update Notificationを送信する
     * @param $nodeId
     * @param $deviceType
     * @param $value
     * @param $unit
     * @param $timestamp
     * @param array $options
     */
    protected function sendDeviceDataNotification($nodeId, $deviceType, $value, $unit, $timestamp=null, $options=[]) {
        $nodeRepository = new NodeRepository();
        $node = $nodeRepository->findById($nodeId);
        if(!$node) {
            throw new \InvalidArgumentException('ノードIDが存在しません. node_id:' . $nodeId);
        }

        if(is_null($timestamp)) {
            $timestamp = (int)(microtime(true) * 1000);
        }

        $nodeName = sprintf('%s_%s_%s', $node['manufacturer'], $node['product'], $node['serial_no']);
        $notification = [
            'command' => 'device_data',
            'node_name' => $nodeName,
            'device_type' => $deviceType,
            'value' => $value,
            'timestamp' => (int)$timestamp,
        ];

        if(!is_null($unit)) {
            $notification['unit'] = $unit;
        }
        $this->amqpClient->sendMessageToLcas($notification);
    }


    /**
     * デバイスIDを使用してDevice Data Update Notificationを送信する
     * @param $deviceId
     * @param $value
     * @param $unit
     * @param array $options
     */
    protected function sendDeviceDataNotificationByDeviceId($deviceId, $value, $unit, $options=[]) {
        $deviceRepository = new DeviceRepository();
        $device = $deviceRepository->findById($deviceId);
        if(!$device) {
            throw new \Exception('デバイスが存在しません。device_id: ' . $deviceId);
        }

        $deviceTypeName = $deviceRepository->getDeviceTypeNameFromType($device['type']);
        $this->sendDeviceDataNotification($device['node_id'], $deviceTypeName, $value, $unit, null, $options);
    }


    /**
     * 一般的なエラーのレスポンスかを検証する
     * @param array $other 検証対象のデータを含んだ連想配列
     * @param $message
     */
    protected function isValidGeneralErrorResponse($other, $message) {
        $this->assertThat($other, new GeneralErrorResponseConstraint(), $message);
    }

    /**
     * closure内でエラーが発生し、それが一般的なエラーのレスポンスであることを検証する
     * @param callable $closure クロージャ
     * @param int $expectedStatusCode 期待するステータスコード
     * @param string $message 検証内容を説明するメッセージ
     */
    protected function assertGeneralErrorResponse($closure, $expectedStatusCode, $message) {
        $catched = false;
        try{
            $closure();
        } catch(BadResponseException $e) {
            if($expectedStatusCode != 0 && $e->getResponse()->getStatusCode() != $expectedStatusCode) {
                throw $e;
            }
            $body = $e->getResponse()->getBody();
            $json = json_decode($body, true);
            $this->assertThat($json, new GeneralErrorResponseConstraint(), $message);

            $catched = true;
        }
        $this->assertTrue($catched, "ステータスコードが{$expectedStatusCode}であること");
    }


    /**
     * closure内でエラーが発生することを検証する
     * @param callable $closure クロージャ
     * @param int $expectedStatusCode 期待するステータスコード
     * @param string $message 検証内容を説明するメッセージ
     */
    protected function assertHttpError(callable $closure, $expectedStatusCode=0, $message='') {
        $catched = false;
        try{
            $closure();
        } catch(BadResponseException $e) {
            if($expectedStatusCode != 0 && $e->getResponse()->getStatusCode() != $expectedStatusCode) {
                throw $e;
            }
            $catched = true;
        }
        $this->assertTrue($catched, "ステータスコードが{$expectedStatusCode}であること");
    }

}
