<?php

namespace Lcas\Test;


use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Client;
use ElephantIO\Exception\ServerConnectionFailureException;
use Lcas\Log\Logger;

class LcasSocketIoClient {

    private $host=null;
    private $nameSpace=null;
    private $authToken=null;
    private $origin=null;

    /**
     * @var Client
     */
    private $connection = null;

    const SOCKET_IO_PACKET_TYPE_CONNECT = 0;
    const SOCKET_IO_PACKET_TYPE_DISCONNECT = 1;
    const SOCKET_IO_PACKET_TYPE_EVENT = 2;

    public function setHost($host) {
        $this->host = $host;
    }

    public function setNameSpace($nameSpace) {
        $this->nameSpace = $nameSpace;
    }

    public function changeNameSpace($nameSpace) {
        $this->setNameSpace($nameSpace);
        return $this->connection->of($nameSpace);
    }

    public function setAuthToken($authToken) {
        $this->authToken = $authToken;
    }

    public function setOrigin($origin) {
        $this->origin = $origin;
    }

    public function connect($authToken=null) {

        if(is_null($this->host)) {
            $this->host = TEST_SOCKET_IO_API_BASE_URL;
        }
        if(is_null($this->nameSpace)) {
            $this->nameSpace = TEST_WEBSOCKET_API_ENDPOINT_PATH;
        }
        if(!is_null($authToken)) {
            $this->authToken = $authToken;
        }
        $options = [
            'context' => [
                'http' => [
                    'header' => [],
                    'timeout' => 2,  //読み込みタイムアウト
                ]
            ],
            'timeout' => 5,  //接続時のタイムアウト
            'wait' => 10 * 1000,  //WebSocketメッセージ送信直後の待ち時間(マイクロ秒)
        ];

        if(!is_null($this->authToken)) {
            $options['context']['http']['header'][] = "Cookie: SESSID={$this->authToken}";
        }
        if(!is_null($this->origin)) {
            $options['context']['http']['header'][] = "Origin: {$this->origin}";
        }

        $this->connection = new Client(new Version1X($this->host, $options));
        $this->connection->initialize();
        if($this->nameSpace != '') {
            $this->connection->of($this->nameSpace);
            //ネームスペースの変更に対する応答がくるので、1回readを行う。
            $this->connection->read();
        }

    }

    /**
     * LCASのWebSocket API形式の通知を待ち受けて返す。
     * (それ以外のメッセージは読み飛ばす)
     * @return array
     */
    public function waitNotification() {

        $notification = null;
        while($payload = @$this->connection->read()) {
            $parsedPayload = $this->parsePayload($payload);

	        $logger = Logger::get();
	        $logger->addDebug('############' + json_encode($parsedPayload));
        
            //Socket.IOの通知はtype==event以外にもconnect,disconnect,errorなどが存在する。
            //eventと合わせてerrorなども同時に送信されるケースが存在するため、
            //event以外のメッセージをスキップする。
            if($parsedPayload['type'] != 'event') {
                continue;
            }

            $notification = json_decode($parsedPayload['data'], true);
            break;
        }
        
        return $notification;
    }


    public function isConnected() {
        //接続が維持されているかを確認する。
        //ネームスペースの切り替えリクエストを行うと(接続されていれば)即座に応答があることから、
        //ネームスペース切り替えに対する応答があるかで判断する。

        $this->changeNameSpace(TEST_WEBSOCKET_API_ENDPOINT_PATH);

        $nameSpaceResponseFound = false;

        while($payload = @$this->connection->read()) {
            $parsedPayload = $this->parsePayload($payload);
            //typeがconnectで、正規のネームスペースを含むレスポンスが
            //返ってくるかどうかを確認する
            if($parsedPayload['type'] == 'connect' && $parsedPayload['data'] == TEST_WEBSOCKET_API_ENDPOINT_PATH) {
                $nameSpaceResponseFound = true;
            }
        }

        return $nameSpaceResponseFound;
    }

    /**
     * Socket.IOのパケットをパースする。
     * @param string $payload 受信したパケットデータ
     * @return array
     *
     * [
     *   'type' => パケットの種類(connect,disconnect,event,ack,error),
     *   'nasmespace' => ネームスペース,
     *   'event' => イベント名(type=event時のみ),
     *   'data' => レスポンスの文字列(JSONも文字列状態で格納)
     * ]
     */
    private function parsePayload($payload) {
        //2バイト目がパケットタイプ
        $packetType = substr($payload, 1, 1);
        $captured = [];
        $nameSpace = '';
        $parsedPayload = '';

        //Socket.IOのパケットは、タイプコードのあとは
        //ネームスペースがある場合は"ネームスペース,データ"
        //ネームスペース指定がない場合は"データ"という形式。
        if(preg_match('#[0-9]{2}(.+?),(.*?)$#', $payload, $captured)) {
            //ネームスペース指定あり
            $nameSpace = $captured[1];
            $parsedPayload = $captured[2];
        } else if(preg_match('#[0-9]{2}(.*?)$#', $payload, $captured)) {
            //ネームスペース指定なし
            $nameSpace = '';
            $parsedPayload = $captured[1];
        } else {
            return false;
        }

        //ws_node/node_modules/socket.io/node_modules/socket.io-parser/index.js
        $type = '';
        $event = '';
        switch($packetType) {
            case 0:  //CONNECT
                $type = 'connect';
                break;
            case 1:  //DISCONNECT
                $type = 'disconnect';
                break;
            case 2:  //EVENT
                //イベントメッセージは"[eventName,{メッセージ内容のJSON}]"というフォーマットのJSONで
                //通知されるため、イベント名、メッセージ本体に分割する。
                $type = 'event';
                $tmpPayload = json_decode($parsedPayload, true);
                $event = $tmpPayload[0];
                $parsedPayload = json_encode($tmpPayload[1]);
                break;
            case 3:  //ACK
                $type = 'ack';
                break;
            case 4:  //ERROR
                $type = 'error';
                break;
        }

        $returnData = [
            'type' => $type,
            'namespace' => $nameSpace,
            'event' => $event,
            'data' => $parsedPayload
        ];

        return $returnData;
    }

}
