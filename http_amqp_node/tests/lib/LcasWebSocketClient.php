<?php

namespace Lcas\Test;

use WebSocket\Client as WSClient;
use WebSocket\ConnectionException;

class LcasWebSocketClient {

    private $host=null;
    private $endPoint=null;
    private $authToken=null;
    private $origin=null;

    /**
     * @var WSClient
     */
    private $connection = null;

    public function setHost($host) {
        $this->host = $host;
    }

    public function setEndpoint($endPoint) {
        $this->endPoint = $endPoint;
    }

    public function setAuthToken($authToken) {
        $this->authToken = $authToken;
    }

    public function setOrigin($origin) {
        $this->origin = $origin;
    }

    public function connect($authToken=null) {

        if(is_null($this->host)) {
            $this->host = TEST_WEBSOCKET_API_BASE_URL;
        }
        if(is_null($this->endPoint)) {
            $this->endPoint = TEST_WEBSOCKET_API_ENDPOINT_PATH;
        }
        if(!is_null($authToken)) {
            $this->authToken = $authToken;
        }
        $options = [
            'headers' => [],
            'timeout' => 1,  //読み込みタイムアウト
        ];

        if(!is_null($this->authToken)) {
            $options['headers']['Cookie'] = " SESSID={$this->authToken}";
        }
        if(!is_null($this->origin)) {
            $options['headers']['Origin'] = $this->origin;
        }

        try {
            $this->connection = new WSClient($this->host . $this->endPoint, $options);
        } catch(ConnectionException $connectionException) {
            //接続失敗のテストを可能にするため、ここではエラー処理を行わない
        }


    }

    /**
     * サーバーからの通知を待ち、パースして返す
     * @return array|bool
     */
    public function wait() {
        $payload = @$this->connection->receive();

        return $payload;
    }


    public function isConnected() {
        $message = 'check-connection';

        //サーバーに対しpingを送信し、応答があるかを確認
        //(応答がない場合は例外が発生する)

        $readData = '';
        try {
            $this->connection->send('check-connection', 'ping');
            $readData = $this->connection->receive();
        } catch(ConnectionException $connectionException) {
        }

        return ($readData == $message) ? true : false;

    }

}
