<?php

namespace Lcas\Test\Util;

class CompareUtil {


    /**
     *
     * @param mixed $expected 期待値
     * @param mixed $target 比較対照の値
     * @param string $errorPrefix エラーメッセージに付加する接頭辞
     * @param array &$errors エラーを格納する変数への参照
     */
    public static function verifyEquals($expected, $target, $errorPrefix, &$errors) {
        $localErrors = [];
        if($expected != $target) {
            $localErrors[] = "{$errorPrefix}:期待値と値が一致しません. 期待値:{$expected}, 比較対象: {$target}";
        } else if(gettype($expected) != gettype($target)) {
            $localErrors[] = "{$errorPrefix}:期待値と型が一致しません。期待値:" . gettype($expected) . ', 比較対象:' . gettype($target);
        }
        $errors = array_merge($errors, $localErrors);
        return (count($localErrors) == 0) ? true : false;
    }


    /**
     * 配列内の各要素を比較し、期待値通りかを検証する
     * @param mixed $a
     * @param mixed $b
     * @param array &$errors
     * @return bool
     */
    public static function compareArray($a, $b, &$errors) {
        $errors = [];
        if(!is_array($a) || !is_array($b)) {
            $errors[] = '期待値、または比較対象が配列ではありません。';
            return false;
        }
        $expectedCount = count($a);
        if(count($b) != $expectedCount) {
            $errors[] = "件数が一致しません。期待値:{$expectedCount}, 比較対象:" . count($b);
        }
        foreach($a as $key=>$value) {
            if(!isset($b[$key])) {
                $errors[] = "比較対照にキーが存在しません:{$key}";
            } else if($a[$key] != $b[$key]) {
                $errors[] = "{$key}: 期待値と値が一致しません. 期待値:{$a[$key]}, 比較対象: {$b[$key]}";
            } else if(gettype($a[$key]) != gettype($b[$key])) {
                $errors[] = "{$key}: 期待値と型が一致しません。期待値:" . gettype($a[$key]) . ', 比較対象:' . gettype($b[$key]);
            }
        }
        return (count($errors) == 0) ? true : false;
    }


    /**
     * 連想配列を再帰的に検証するオブジェクトを生成して返す
     * @param array $options 初期化オプション
     * @return RecursiveComparator
     */
    public static function getRecursiveComparator($options=[]) {
        return new RecursiveComparator($options);
    }

}
