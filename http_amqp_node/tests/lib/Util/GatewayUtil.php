<?php

namespace Lcas\Test\Util;

use GuzzleHttp\Client;

class GatewayUtil {

    /**
     * @var Client
     */
    private $httpClient;


    /**
     * @param Client $httpClient
     */
    public function __construct($httpClient)
    {
        $this->httpClient = $httpClient;
    }


    /**
     * macアドレスからGatewayのIDを特定して返す。
     * @param string $macAddress MACアドレス
     * @return mixed
     */
    public function getGatewayIdFromMacAddress($macAddress) {
        $gatewayRepository = new \Lcas\Repository\GatewayRepository();
        $gateway = $gatewayRepository->findByMacAddress($macAddress);
        if(!$gateway) {
            throw new \InvalidArgumentException('ゲートウェイが見つかりません。mac_address=' . $macAddress);
        }
        return $gateway['id'];
    }


    /**
     * ゲートウェイの割当を行う(POST gateways)
     * @param string $macAddress
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function registerGateway($macAddress) {
        $json = json_encode([
            'mac_address' => $macAddress
        ]);
        return $this->httpClient->post('/api/v3/gateways', ['body' => $json]);
    }


    /**
     * ゲートウェイ割り当ての解除を行う
     * @param int $gatewayId ゲートウェイID
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function deregisterGateway($gatewayId) {
        return $this->httpClient->delete('/api/v3/gateways/' . $gatewayId);
    }


}
