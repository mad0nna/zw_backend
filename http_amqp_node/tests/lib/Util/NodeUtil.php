<?php

namespace Lcas\Test\Util;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Lcas\DB\DB;

class NodeUtil {


    /**
     * @var Client
     */
    private $httpClient;


    /**
     * @param Client $httpClient
     */
    public function __construct($httpClient)
    {
        $this->httpClient = $httpClient;
    }


    public function getNodeById($id) {
        return $this->httpClient->get('/api/v3/nodes/' . $id);
    }


    public function updateNode($nodeId, $parameters) {

        $json = json_encode($parameters);
        $response = $this->httpClient->request('PUT', '/api/v3/nodes/' . $nodeId, [
            'body' => $json
        ]);

        return $response;
    }

    /**
     * ノードのラベルを更新する(PUT nodes/:id)
     * @param int $nodeId ノードID
     * @param array $labels ラベル
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function updateNodeLabels($nodeId, $labels) {
        $json = json_encode(['labels' => $labels]);
        return $this->httpClient->put('/api/v3/nodes/' . $nodeId, ['body' => $json]);
    }

    /**
     * ノードのユーザーラベルを更新する(PUT nodes/:id/users/:user_id)
     * @param int $nodeId ノードID
     * @param int $userId ユーザーID
     * @param array $labels ラベル
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function updateNodeUserLabels($nodeId, $userId, $labels) {
    	$json = json_encode(['info_pcs' => $labels]);
    	return $this->httpClient->put('/api/v3/nodes/' . $nodeId . '/users/' . $userId, ['body' => $json]);
    }
    
    
    public function shareNodeWith($nodeId, $emails) {

        $parameters = [
            'sharing_level' => 'users',
            'shared_with' => [],
        ];

        foreach($emails as $email) {
            $parameters['shared_with'][] = [
                'email' => $email
            ];
        }

        $this->httpClient->put('/api/v3/nodes/' . $nodeId, [
            'body' => json_encode($parameters)
        ]);

    }
    
    /**
     * データベースから共有ノード一覧を取得する
     * @param int $userId
     * @throws \Exception
     */
    public function findSharedNodes($userId) {
    	$sql = "SELECT * FROM nodes_users WHERE user_id={$userId} ";
    	$db = DB::getMasterDb();
    	$db->setSql($sql);
    	if(!$db->Query()) {
    		throw new \Exception('SQLの実行に失敗しました。ERROR: ' . $db->getErr() . ',' . $sql);
    	}
		$sharedNodes = array();
		while($row = $db->Fetch()) {
			array_push($sharedNodes, $row);
		}
		return $sharedNodes;
    }

    /**
     * データベースからユーザーの共有ノード一覧を取得する
     * @param int $userId
     * @throws \Exception
     */
    public function findUserSharedNodes($userId) {
    	$sql = "SELECT * FROM nodes_users WHERE user_id={$userId} ";
    	$db = DB::getMasterDb();
    	$db->setSql($sql);
    	if(!$db->Query()) {
    		throw new \Exception('SQLの実行に失敗しました。ERROR: ' . $db->getErr() . ',' . $sql);
    	}
		$sharedNodes = array();
		while($row = $db->Fetch()) {
			array_push($sharedNodes, $row);
		}
		return $sharedNodes;
    }

    /**
     * データベースからユーザーのノードラベル一覧を取得する
     * @param int $userId
     * @throws \Exception
     */
    public function findUserNodeLabels($userId) {
    	$sql = "SELECT * FROM labels WHERE type=2 AND user_id={$userId} ";
    	$db = DB::getMasterDb();
    	$db->setSql($sql);
    	if(!$db->Query()) {
    		throw new \Exception('SQLの実行に失敗しました。ERROR: ' . $db->getErr() . ',' . $sql);
    	}
		$sharedNodes = array();
		while($row = $db->Fetch()) {
			array_push($sharedNodes, $row);
		}
		return $sharedNodes;
    }

}
