<?php


namespace Lcas\Test\Util;


use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

class RecursiveComparator {

    private $options;

    private $errors;

    public function __construct($options = []) {
        $this->options = $options;
        $this->errors = [];
    }

    public function compare($expected, $target) {
        return $this->compareRecursive($expected, $target);
    }

    private function compareRecursive($expected, $target, $prefix=[]) {
        $errors = [];
        foreach($expected as $key=>$value ) {
            if(!array_key_exists($key, $target)) {
                $errors[] = implode('.', $prefix) . '.' . $key . 'が存在しません';
                continue;
            }
            if(is_array($expected[$key])) {
                $prefix []= $key;
                return $this->compareRecursive($expected[$key], $target[$key], $prefix);
            }
            if($expected[$key] instanceof Validator) {
                //expected側の値がRespectライブラリのValidatorのインスタンスだった場合は
                //バリデーションを行う。
                try {
                    $expected[$key]->validate($target[$key]);
                } catch(NestedValidationException $e) {
                    $errors[] = $e->getFullMessage();
                }
            } else if($target[$key] != $expected[$key]) {
                $errors[] = implode('.', $prefix) . '.'  . "{$key}の値({$target[$key]})が期待値({$expected[$key]})と異なります";
            } else if(gettype($target[$key]) != gettype($expected[$key])) {
                $targetType = gettype($target[$key]);
                $expectedType = gettype($expected[$key]);
                $errors[] = implode('.', $prefix) . '.'  . "{$key}の型({$targetType})が期待値の型({$expectedType})と異なります";
            }
        }
        $this->errors = array_merge($this->errors, $errors);
        return (count($errors) == 0) ? true : false;
    }


    public function getErrors() {
        return $this->errors;
    }


}