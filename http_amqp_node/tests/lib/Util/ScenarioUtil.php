<?php

namespace Lcas\Test\Util;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Lcas\DB\DB;

class ScenarioUtil {


	/**
	 * @var Client
	 */
	private $httpClient;


	/**
	 * @param Client $httpClient
	 */
	public function __construct($httpClient) {
		$this->httpClient = $httpClient;
	}

	/**
	 * シナリオの登録を行う(POST scenarios)
	 * @return array
	 */
	public function registerScenario(array $scenario) {

		$response = $this->httpClient->post('/api/v3/scenarios', [
				'json' => $scenario
		]);

		return $response;
	}

	/**
	 * デフォルトのシナリオの登録を行う(POST scenarios)
	 * @return array
	 */
	public function registerDefaultScenario() {
		
		$labels = array("ラベル1", "ラベル2", "ラベル3", "ラベル4", "ラベル5");
		$condition = array(
				'lhs' => '200',
				'operator' => '==',
				'rhs' => '100',
				'unit' => '%'
		);
		$action = array(
				'type' => 'send_message',
				'content' => 'シナリオのイベントを通知します'
		);
		$enable_days = array('mon', 'tue', 'wed', 'thu', 'fri');
		
		$scenario = array(
				'scenario_type' => 'simple',
				'labels' => $labels,
				'condition_operator' => 'AND',
				'conditions' => array( $condition ),
				'trigger_after' => 5,
				'actions' => array( $action ),
				'enabled_period' => '10:00-20:00',
				'enabled_days' => $enable_days,
				'enabled' => true
		);
		
		return $this->registerScenario($scenario);
	}
	
	/**
	 * データベースからユーザーのシナリオ一覧を取得する
	 * @param int $userId
	 * @throws \Exception
	 */
	public function findUserScenarios($userId) {
		$sql = "SELECT * FROM scenarios WHERE user_id={$userId} ORDER BY id ";
		$db = DB::getMasterDb();
		$db->setSql($sql);
		if(!$db->Query()) {
			throw new \Exception('SQLの実行に失敗しました。ERROR: ' . $db->getErr() . ',' . $sql);
		}
		$scenarios = array();
		while($row = $db->Fetch()) {
			array_push($scenarios, $row);
		}
		return $scenarios;
	}
}