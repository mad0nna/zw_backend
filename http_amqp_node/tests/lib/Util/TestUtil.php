<?php


namespace Lcas\Test\Util;


use Lcas\DB\DB;

class TestUtil {

    public static function clearDatabase() {
        //self::runSqlScript(TEST_FIXTURE_DIR . '/reset.sql');
        $db = DB::getMasterDb();

        $tables = [
            'apps_params',
            'auth_tokens',
            'capabilities',
            'device_data',
            'devices',
            'firmware_versions',
            'gateways',
            'labels',
            'mail_templates',
            'node_actions',
            'node_configs',
            'nodes',
            'nodes_apps',
            'nodes_users',
            'scenario_actions',
            'scenario_conditions',
            'scenario_events',
            'scenarios',
            'user_tokens',
            'users',
            'zworks_notifications',
            'zworks_requests',
            'dust_gateways',
        ];
        foreach($tables as $table) {
            $db->setSql("DELETE FROM {$table}");
            $db->Query();
        }
    }

    public static function runSqlScript($scriptPath) {
//        if(!preg_match('/test/', DB_NAME)) {
//            throw new \Exception('テスト用DBではない可能性があります。: ' . DB_NAME);
//        }

        if(!is_file($scriptPath)) {
            throw new \Exception('SQLファイルが見つかりません: ' . $scriptPath);
        }

        $return = 0;
        $output = "";
        $command = sprintf('mysql -u %s -p%s -h %s -P %s %s < %s', DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME, $scriptPath);
        exec($command, $output, $return);
        if($return != 0) {
            throw new \Exception('コマンドの実行に失敗しました.: ' . $command . ', status: ' . $return);
        }
    }

}
