<?php

namespace Lcas\Test\Util;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Lcas\DB\DB;

class UserUtil {


    /**
     * @var Client
     */
    private $httpClient;


    /**
     * @param Client $httpClient
     */
    public function __construct($httpClient) {
        $this->httpClient = $httpClient;
    }

    /**
     * ユーザーの登録を行う(POST users)
     * @param $email
     * @param $pass
     * @param $platform
     * @param $token
     * @return array
     */
    public function registerUser($email, $pass, $platform, $token=null, $origin=null) {

        //仮登録リクエストを行った後、本登録リクエストを行う。
        $params = array(
        	'json' => [
        		'email' => $email
        	]
        );
        if (!is_null($origin)) {
        	array_push($params,
        		array(
        			'headers' => [
        				'origin' => $origin
        			]
        		)
        	);
        }
        $this->httpClient->post('/api/v3/users', $params);

        //仮登録のトークンを調べる
        $sql = "SELECT * FROM user_tokens WHERE email='{$email}'";
        $db = DB::getMasterDb();
        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception("SQLエラー: {$db->getErr()}, SQL: {$sql}");
        }
        $temporaryTokenRow = $db->Fetch();

        $parameters = [
        ];

        $parameters['confirmation_token'] = $temporaryTokenRow['token'];
        if(!is_null($pass)) {
            $parameters['password'] = $pass;
        }
        if(!is_null($platform)) {
            $parameters['platform'] = $platform;
        }
        if(!is_null($token)) {
            $parameters['token'] = $token;
        }

        $response = $this->httpClient->post('/api/v3/users/confirmation', [
            'json' => $parameters
        ]);

        return $response;
    }


    /**
     * デフォルトのユーザーを登録するショートカット
     * @return array
     */
    public function registerDefaultUser() {
        return $this->registerUser(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD, 'none');
    }

    /**
     * 指定したユーザーでログインする(POST login)
     * @param $email
     * @param $pass
     * @return Response
     */
    public function login($email, $pass) {
        $json = json_encode(['email' => $email, 'password' => $pass]);

        $response = $this->httpClient->request('POST', '/api/v3/login', [
            'body' => $json
        ]);

        return $response;
    }

    /**
     * デフォルトのユーザーでログインするショートカット
     * @return mixed
     */
    public function loginWithDefaultUser() {
        return $this->login(TEST_DEFAULT_USER_EMAIL, TEST_DEFAULT_USER_PASSWORD);
    }


    /**
     * HTTPレスポンスから、COOKIEに含まれるセッションIDの値を取得する
     * @param Response $response
     * @return string セッションID
     */
    public function getSessionIdFromResponse(Response $response) {
        $cookieHeaders = $response->getHeader('Set-Cookie');
        $cookieHeader = $cookieHeaders[0];

        $matched = [];
        if(!preg_match('/SESSID=([A-Za-z0-9]+);/', $cookieHeader, $matched)) {
            throw new \Exception('セッションIDの取得に失敗しました。HEADER: ' . $cookieHeader);
        }

        return $matched[1];
    }


    /**
     * 現在ログインしているユーザーの情報(usersのレコード)を返す。
     * @return array
     */
    public function getCurrentUser() {
        $cookieJar = $this->httpClient->getConfig('cookies');
        $cookies = $cookieJar->toArray();

        $sessionId = null;
        foreach($cookies as $cookie) {
            if($cookie['Name'] == 'SESSID') {
                $sessionId = $cookie['Value'];
            }
        }

        if(!$sessionId) {
            throw new \Exception('認証Cookieが見つかりません');
        }

        $sql = "SELECT users.* FROM users "
            . " INNER JOIN auth_tokens ON (auth_tokens.user_id=users.id) "
            . " WHERE auth_tokens.token='{$sessionId}' ";

        $db = DB::getMasterDb();
        $db->setSql($sql);
        if(!$db->Query()) {
            throw new \Exception('SQLの実行に失敗しました。ERROR: ' . $db->getErr() . ',' . $sql);
        }

        $result = $db->Fetch();
        return $result;
    }


    /**
     * 現在ログインしているユーザーのユーザーIDを返す
     * @return int
     */
    public function getCurrentUserId() {
        $user = $this->getCurrentUser();
        return $user['id'];
    }


    /**
     * メールアドレスから、認証トークン(セッションID)を取得する
     * @param string $email
     */
    public function getAuthTokenFromEmail($email) {
        $sql = "SELECT auth_tokens.token FROM auth_tokens "
            . " INNER JOIN users ON (auth_tokens.user_id=users.id) "
            . " WHERE users.email='{$email}' ";

        $db = DB::getMasterDb();
        $db->setSql($sql);

        if(!$db->Query()) {
            throw new \Exception('SQLエラー：' . $db->Error() . 'SQL: ' . $sql);
        }

        $result = $db->Fetch();
        return $result['token'];
    }

    /**
     * 指定したユーザーのラベルを登録する(POST users/self)
     * @param $labels
     * @return Response
     */
    public function updateLabels($labels) {
        $json = json_encode(['labels' => $labels]);

        $response = $this->httpClient->request('PUT', '/api/v3/users/self', [
            'body' => $json
        ]);

        return $response;
    }
    
    /**
     * データベースからユーザー情報を取得する
     * @param int $userId
     * @throws \Exception
     */
    public function findUser($userId) {
    	$sql = "SELECT * FROM users WHERE id={$userId} ";
    	$db = DB::getMasterDb();
    	$db->setSql($sql);
    	if(!$db->Query()) {
    		throw new \Exception('SQLの実行に失敗しました。ERROR: ' . $db->getErr() . ',' . $sql);
    	}
    	$result = $db->Fetch();
    	return $result;
    }
    
    /**
     * データベースからユーザーラベル一覧を取得する
     * @param int $userId
     * @throws \Exception
     */
    public function findUserLabels($userId) {
    	$sql = "SELECT * FROM labels WHERE type=5 AND entity_id={$userId} ";
    	$db = DB::getMasterDb();
    	$db->setSql($sql);
    	if(!$db->Query()) {
    		throw new \Exception('SQLの実行に失敗しました。ERROR: ' . $db->getErr() . ',' . $sql);
    	}
    	$labels = array();
    	while ($row = $db->Fetch()) {
    		array_push($labels, $row);
    	}
    	return $labels;
    }

}