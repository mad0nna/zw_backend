<?php

require_once __DIR__ . '/lib/Psr4ClassLoader.php';


define('TEST_DIR', __DIR__);
define('TEST_FIXTURE_DIR', TEST_DIR . '/integration/fixtures');

define('TEST_HTTP_API_BASE_URL', 'http://127.0.0.1');
define('TEST_WEBSOCKET_API_BASE_URL', 'ws://127.0.0.1:8090');
define('TEST_SOCKET_IO_API_BASE_URL', 'http://127.0.0.1');

define('TEST_WEBSOCKET_ORIGIN', 'https://example.com');
define('TEST_WEBSOCKET_API_ENDPOINT_PATH', '/ws/v3/main');


define('TEST_DEFAULT_USER_EMAIL', 'test@example.com');
define('TEST_DEFAULT_USER_PASSWORD', 'test1234');

define('TEST_DEFAULT_GATEWAY',  'A00000000000');
define('TEST_DEFAULT_SCENARIO', 'default-scenario');
define('TEST_DEFAULT_SCENARIO_WBGT', 'default-scenario-wbgt');
define('TEST_DEFAULT_SCENARIO_SCXML', 'default-scenario-scxml');

//バッテリの残量通知(自動登録シナリオのシナリオ名)
define('TEST_AUTO_SCENARIO_BATTERY', 'auto-scenario-battery');


define('TEST_FAKE_ZIO_QUEUE_NAME', '');

$classLoader = new Lcas\Test\Psr4ClassLoader();
$classLoader->register();
$classLoader->addNamespace('Lcas\Test', __DIR__ . '/lib');
