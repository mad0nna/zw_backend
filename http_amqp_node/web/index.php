<?php

/**
 * HTTP APIのエントリポイント。
 */

use Lcas\ApiRequest;

ini_set('display_errors', 0);

//アプリケーションの環境を表す定数
//例：production, staging, dev
define('ENV', 'dev');

require_once __DIR__ . '/../config/config.php';

$app = new \Lcas\Application();
//デバッグ用途で以下も可能
//$request = ApiRequest::create($uri, $method, $parameters, $cookies, $files, $servers, $rawPostData);
$parameters = array(
    'request' => ApiRequest::createFromGlobals(),
);
$app->run($parameters);