# Description

This program retrieves the scenarios registered in the `LCAS` `MySQL` database
and sends a *Scenario Registration Request* to the `LifeEngine` for each
scenario.


# Installation

To run these scripts, the `php` and `php-mysql` are required.
In order to install the PHP dependencies, `composer` is also required.
To install these packages on CentOS 7, run the following commands:

```
# yum install epel-release
# yum install composer php php-mysql
```

To install the script dependencies, run the following command:

```
$ composer install
```


# Usage

Edit the `config.ini` file with the `MySQL` and `AMQP` connection information.
Then, run the program:

```
$ php scenario_recovery.php
```

You can use the `--dry-run` flag to list the requests to be sent, without
actually sending any data:

```
$ php scenario_recovery.php --dry-run
```

If you use the `--write` flag and want to make sure you don't lose any data,
you can create a copy of the `LCAS` database using:

```
$ mysqldbcopy --force --source='root@localhost' --destination='root@localhost' lcas:lcas_bak
```
