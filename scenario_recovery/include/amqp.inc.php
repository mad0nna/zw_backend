e?php
/**
 * This file contains utility functions related to AMQP.
 *
 * @author Alexis Jeandeau <alexis@z-works.co.jp>
 */

require_once __DIR__ . '/../vendor/autoload.php';

use PhpAmqpLib\Message\AMQPMessage;


/**
 * Send a Scenario Registration AMQP command to the cloud.
 */
function register_scenario($json, $scenario, $channel, $config_amqp,
                           $dry = false, $wait = true, $timeout = 30) {
    if ($dry) {
        echo "\nScenario Registration Request:\n" . $json . "\n";
        return;
    }

    echo "\nSending:\n" . $json . "\n";

    $message = new AMQPMessage(
        $json,
        array(
            'content_type'   => 'application/json',
            'user_id'        => $config_amqp['user'],
            'delivery_mode'  => 2,
            'correlation_id' => $scenario['scenario_id']
        )
    );

    $channel->basic_publish($message, $config_amqp['exchange_name']);

    if ($wait) {
        return wait_for_scenario_reg_notif(
            $scenario,
            $channel,
            $config_amqp,
            $timeout
        );
    } else {
        return true;
    }
}

function wait_for_scenario_reg_notif($scenario, $channel, $config_amqp
                                     $timeout) {
    $queue = 'intermediate_' . $config_amqp['user'];
    $consumer_tag = $channel->basic_consume(
        $queue,
        '',
        false,
        false,
        false,
        false,
        function ($msg) use ($scenario) {
            // Send back ACK
            $msg->delivery_info['channel']->
                basic_ack($msg->delivery_info['delivery_tag']);

            if ($msg == null) {
                echo "\nReceived response is null\n";
                return;
            }
            $body = json_decode($msg->body, true);
            if ($body['command'] != 'scenario_reg') {
                echo "\nReceived response is " . $body['command']
                    . " instead of scenario_reg\n";
                return;
            }

            // Stop waiting for messages (quit callback loop)
            $delivery_info = $msg->delivery_info;
            $consumer_tag = $delivery_info['consumer_tag'];
            $delivery_info['channel']->basic_cancel($consumer_tag);

            if ($body['result_code'] != 200) {
                echo "\nReceived error code " . $body['result_code'] . "\n";
                echo "Reason: " . implode(', ', $body['reason']) . "\n";
                return;
            }

            echo "\nRegistered scenario ID {$scenario['scenario_id']}\n";
        }
    );

    try {
        while (count($channel->callbacks)) {
            $channel->wait(null, false, $timeout);
        }
    } catch (AMQPTimeoutException $e) {
        // Stop waiting for messages (quit callback loop)
        $channel->basic_cancel($consumer_tag);
        return false;
    }

    return true;
}


/**
 * Send a Scenario Deregistration AMQP command to the cloud.
 */
function deregister_scenario($scenario_id, $channel, $config_amqp,
                             $dry = false, $wait = true, $timeout = 30) {
    $scenario = array(
        'command' => 'scenario_dereg',
        'scenario_id' => $scenario_id
    );

    $json = json_encode($scenario);
    if (!$json) {
        throw new \RuntimeException(
            'Failed to encode scenario into JSON: ' . $scenario
        );
    }

    if ($dry) {
        echo "\nScenario Deregistration Request:\n" . $json . "\n";
        return;
    }

    echo "\nSending:\n" . $json . "\n";

    $message = new AMQPMessage(
        $json,
        array(
            'content_type'   => 'application/json',
            'user_id'        => $config_amqp['user'],
            'delivery_mode'  => 2,
            'correlation_id' => $scenario['scenario_id']
        )
    );

    $channel->basic_publish($message, $config_amqp['exchange_name']);

    if ($wait) {
        return wait_for_scenario_dereg_notif(
            $scenario,
            $channel,
            $config_amqp,
            $timeout
        );
    } else {
        return true;
    }
}

function wait_for_scenario_dereg_notif($scenario, $channel, $config_amqp,
                                       $timeout) {
    $queue = 'intermediate_' . $config_amqp['user'];
    $consumer_tag = $channel->basic_consume(
        $queue,
        '',
        false,
        false,
        false,
        false,
        function ($msg) use ($scenario) {
            // Send back ACK
            $msg->delivery_info['channel']->
                basic_ack($msg->delivery_info['delivery_tag']);

            if ($msg == null) {
                echo "\nReceived response is null\n";
                return;
            }
            $body = json_decode($msg->body, true);
            if ($body['command'] != 'scenario_dereg') {
                echo "\nReceived response is " . $body['command']
                    . " instead of scenario_dereg\n";
                return;
            }

            // Stop waiting for messages (quit callback loop)
            $delivery_info = $msg->delivery_info;
            $consumer_tag = $delivery_info['consumer_tag'];
            $delivery_info['channel']->basic_cancel($consumer_tag);

            if ($body['result_code'] != 200) {
                echo "\nReceived error code " . $body['result_code'] . "\n";
                echo "Reason: " . implode(', ', $body['reason']) . "\n";
                return;
            }

            echo "\nDeregistered scenario ID {$scenario['scenario_id']}\n";
        }
    );

    try {
        while (count($channel->callbacks)) {
            $channel->wait(null, false, $timeout);
        }
    } catch (AMQPTimeoutException $e) {
        // Stop waiting for messages (quit callback loop)
        $channel->basic_cancel($consumer_tag);
        return false;
    }

    return true;
}

/**
 * Send a User Gateway Association Request AMQP command to the cloud.
 */
function associate_gateway($username, $gw_name, $gw_key, $channel, $config_amqp,
                           $dry = false, $wait = true, $timeout = 30) {
    $payload = array(
        'command' => 'user_gw_assoc',
        'username' => $username,
        'gw_name' => $gw_name,
        'gw_key' => $gw_key
    );

    $json = json_encode($payload);
    if (!$json) {
        throw new \RuntimeException(
            'Failed to encode payload into JSON: ' . $payload
        );
    }

    if ($dry) {
        echo "\User Gateway Association Request:\n" . $json . "\n";
        return;
    }

    echo "\nSending:\n" . $json . "\n";

    $message = new AMQPMessage(
        $json,
        array(
            'content_type'   => 'application/json',
            'user_id'        => $config_amqp['user'],
            'delivery_mode'  => 2
        )
    );

    $channel->basic_publish($message, $config_amqp['exchange_name']);

    if ($wait) {
        return wait_for_user_gw_assoc_notif(
            $username,
            $gw_name,
            $channel,
            $config_amqp,
            $timeout
        );
    } else {
        return true;
    }
}

function wait_for_user_gw_assoc_notif($username, $gw_name,
                                      $channel, $config_amqp, $timeout) {
    $queue = 'intermediate_' . $config_amqp['user'];
    $consumer_tag = $channel->basic_consume(
        $queue,
        '',
        false,
        false,
        false,
        false,
        function ($msg) use ($username, $gw_name) {
            // Send back ACK
            $msg->delivery_info['channel']->
                basic_ack($msg->delivery_info['delivery_tag']);

            if ($msg == null) {
                echo "\nReceived response is null\n";
                return;
            }
            $body = json_decode($msg->body, true);
            if ($body['command'] != 'user_gw_assoc') {
                echo "\nReceived response is " . $body['command']
                    . " instead of scenario_dereg\n";
                return;
            }

            // Stop waiting for messages (quit callback loop)
            $delivery_info = $msg->delivery_info;
            $consumer_tag = $delivery_info['consumer_tag'];
            $delivery_info['channel']->basic_cancel($consumer_tag);

            if ($body['result_code'] != 200) {
                echo "\nReceived error code " . $body['result_code'] . "\n";
                echo "Reason: " . implode(', ', $body['reason']) . "\n";
                return;
            }

            echo "\nAssociated user {$username} from gateway {$gw_name}\n";
        }
    );

    try {
        while (count($channel->callbacks)) {
            $channel->wait(null, false, $timeout);
        }
    } catch (AMQPTimeoutException $e) {
        // Stop waiting for messages (quit callback loop)
        $channel->basic_cancel($consumer_tag);
        return false;
    }

    return true;
}

/**
 * Send a User Gateway Dissociation Request AMQP command to the cloud.
 */
function dissociate_gateway($username, $gw_name, $channel, $config_amqp,
                            $dry = false, $wait = true, $timeout = 30) {
    $payload = array(
        'command' => 'user_gw_dissoc',
        'username' => $username,
        'gw_name' => $gw_name
    );

    $json = json_encode($payload);
    if (!$json) {
        throw new \RuntimeException(
            'Failed to encode payload into JSON: ' . $payload
        );
    }

    if ($dry) {
        echo "\User Gateway Dissociation Request:\n" . $json . "\n";
        return;
    }

    echo "\nSending:\n" . $json . "\n";

    $message = new AMQPMessage(
        $json,
        array(
            'content_type'   => 'application/json',
            'user_id'        => $config_amqp['user'],
            'delivery_mode'  => 2
        )
    );

    $channel->basic_publish($message, $config_amqp['exchange_name']);

    if ($wait) {
        return wait_for_user_gw_dissoc_notif(
            $username,
            $gw_name,
            $channel,
            $config_amqp,
            $timeout
        );
    } else {
        return true;
    }
}

function wait_for_user_gw_dissoc_notif($username, $gw_name,
                                       $channel, $config_amqp, $timeout) {
    $queue = 'intermediate_' . $config_amqp['user'];
    $consumer_tag = $channel->basic_consume(
        $queue,
        '',
        false,
        false,
        false,
        false,
        function ($msg) use ($username, $gw_name) {
            // Send back ACK
            $msg->delivery_info['channel']->
                basic_ack($msg->delivery_info['delivery_tag']);

            if ($msg == null) {
                echo "\nReceived response is null\n";
                return;
            }
            $body = json_decode($msg->body, true);
            if ($body['command'] != 'user_gw_dissoc') {
                echo "\nReceived response is " . $body['command']
                    . " instead of scenario_dereg\n";
                return;
            }

            // Stop waiting for messages (quit callback loop)
            $delivery_info = $msg->delivery_info;
            $consumer_tag = $delivery_info['consumer_tag'];
            $delivery_info['channel']->basic_cancel($consumer_tag);

            if ($body['result_code'] != 200) {
                echo "\nReceived error code " . $body['result_code'] . "\n";
                echo "Reason: " . implode(', ', $body['reason']) . "\n";
                return;
            }

            echo "\nDissociated user {$username} from gateway {$gw_name}\n";
        }
    );

    try {
        while (count($channel->callbacks)) {
            $channel->wait(null, false, $timeout);
        }
    } catch (AMQPTimeoutException $e) {
        // Stop waiting for messages (quit callback loop)
        $channel->basic_cancel($consumer_tag);
        return false;
    }

    return true;
}

?>
