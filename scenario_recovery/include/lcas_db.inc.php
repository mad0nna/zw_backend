<?php
/**
 * This file contains utility functions related to the LCAS MySQL database.
 *
 * @author Alexis Jeandeau <alexis@z-works.co.jp>
 */

require_once __DIR__ . '/utils.inc.php';


function delete_for_scenario($conn, $scenario_id, $dry, $table_name, $key_name) {
    $sql = "DELETE FROM `{$table_name}` "
         . "WHERE `{$key_name}` = ?";

    if ($dry) {
        echo str_replace_last('?', $scenario_id, $sql) . "\n";
        return;
    }

    $stmt = $conn->prepare($sql);

    if (!$stmt) {
        fwrite(STDERR, "Couldn't prepare SQL statement: ({$conn->errno}) {$conn->error}\n");
        return;
    }

    $stmt->bind_param('i', $scenario_id);

    if ($stmt->execute()) {
        echo "Successfully deleted {$stmt->affected_rows} row(s) "
            . "from table {$table_name} "
            . "where scenario ID is {$scenario_id}\n";
    } else {
        fwrite(STDERR,
            "Error deleting scenario: ({$conn->errno}) {$conn->error}\n");
    }

    $stmt->close();
}

function delete_scenario($conn, $scenario_id, $dry) {
    return delete_for_scenario($conn, $scenario_id, $dry,
        'scenarios', 'id');
}

function delete_scenario_conditions($conn, $scenario_id, $dry) {
    return delete_for_scenario($conn, $scenario_id, $dry,
        'scenario_conditions', 'scenario_id');
}

function delete_scenario_actions($conn, $scenario_id, $dry) {
    return delete_for_scenario($conn, $scenario_id, $dry,
        'scenario_actions', 'scenario_id');
}

function delete_scenario_cascade($conn, $scenario_id, $dry) {
    if (!$dry) {
        $conn->autocommit(false);
    }

    delete_scenario($conn, $scenario_id, $dry);
    delete_scenario_conditions($conn, $scenario_id, $dry);
    delete_scenario_actions($conn, $scenario_id, $dry);

    if (!$dry) {
        $conn->commit();
    }
}

function find_scenarios_no_data_or_no_battery($conn, $dry) {
    $scenarios = [];

    $conn->autocommit(false);

    // Ensure the data is read as UTF-8
    $conn->query('SET NAMES utf8');

    // Only simple scenarios are supported
    $sql = 'SELECT DISTINCT(s.id) '
        . 'FROM scenarios s '
        . 'INNER JOIN scenario_actions sa ON sa.scenario_id = s.id '
        . 'WHERE sa.content = \'長期データなし\' '
        . 'OR sa.content = \'電池交換が必要なセンサーデバイスがあります\'';

    if ($dry) {
        echo "{$sql}\n";
    }

    $result = $conn->query($sql);
    if ($result && $result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($scenarios, $row['id']);
        }
    } else {
        echo "No scenario met the criteria\n";
    }

    $conn->rollback();

    return $scenarios;
}

?>
