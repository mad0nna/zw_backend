<?php
/**
 * This file contains utility functions related to the LCC MySQL database.
 *
 * @author Alexis Jeandeau <alexis@z-works.co.jp>
 */

require_once __DIR__ . '/utils.inc.php';


function delete_for_lc_scenario($conn, $scenario_id, $dry, $table_name, $key_name) {
    $sql = "DELETE FROM `{$table_name}` "
         . "WHERE `{$key_name}` = ?";

    if ($dry) {
        echo str_replace_last('?', $scenario_id, $sql) . "\n";
        return;
    }

    $stmt = $conn->prepare($sql);

    if (!$stmt) {
        fwrite(STDERR, "Couldn't prepare SQL statement: ({$conn->errno}) {$conn->error}\n");
        return;
    }

    $stmt->bind_param('i', $scenario_id);

    if ($stmt->execute()) {
        echo "Successfully deleted {$stmt->affected_rows} row(s) "
            . "from table {$table_name} "
            . "where scenario ID is {$scenario_id}\n";
    } else {
        fwrite(STDERR,
            "Error deleting scenario: ({$conn->errno}) {$conn->error}\n");
    }

    $stmt->close();
}

function delete_lcc_scenario_cascade($conn, $lc_scenario_id, $dry) {
    $conn->autocommit(false);

    // Ensure the data is read as UTF-8
    $conn->query('SET NAMES utf8');

    // Only simple scenarios are supported
    $sql = "SELECT scenario_id "
        . "FROM scenario "
        . "WHERE lc_scenario_id = {$lc_scenario_id}";

    if ($dry) {
        echo "{$sql}\n";
    }

    $result = $conn->query($sql);
    if ($result && $result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $scenario_id = $row['scenario_id'];
            echo "Found scenario_id {$scenario_id} "
                . "for lc_scenario_id ${lc_scenario_id}\n";
            delete_for_lc_scenario(
                $conn,
                $scenario_id,
                $dry,
                'scenario',
                'scenario_id'
            );
            delete_for_lc_scenario(
                $conn,
                $scenario_id,
                $dry,
                'scenario_notice',
                'scenario_id'
            );
        }
    } else {
        echo "No scenario found for lc_scenario_id={$lc_scenario_id}\n";
    }

    if ($dry) {
        $conn->rollback();
    } else {
        $conn->commit();
    }
}

?>
