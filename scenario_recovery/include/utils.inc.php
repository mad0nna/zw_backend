<?php
/**
 * This file contains generic utility functions.
 *
 * @author Alexis Jeandeau <alexis@z-works.co.jp>
 */

function str_replace_last($search, $replace, $str) {
    if (($pos = strrpos($str, $search)) !== false) {
        $search_length = strlen($search);
        $str = substr_replace($str, $replace, $pos, $search_length);
    }
    return $str;
}

?>
