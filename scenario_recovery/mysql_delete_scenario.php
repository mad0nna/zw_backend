#!/usr/bin/env php
<?php
/**
 * This program deletes a scenario from the LCAS MySQL database
 * along with its conditions and actions.
 *
 * @author Alexis Jeandeau <alexis@z-works.co.jp>
 */

require_once __DIR__ . '/include/lcas_db.inc.php';


function usage($prog_name) {
    echo "Usage: {$prog_name} SCENARIO_ID\n";
    echo "Delete the corresponding scenario from the LCAS database, " .
         "along with its conditions and actions.\n\n";
    echo "  -h, --help  display this help and exit\n";
    echo "  --dry-run   don't delete the scenarios, just print the queries\n";
}


if ($argc > 3) {
    fwrite(STDERR, "Too many arguments\n\n");
    usage($argv[0]);
    exit(1);
}

$scenario_id = null;
$dry = false;

while (count($argv) > 1) {
    if ($argv[1] === '-h' || $argv[1] === '--help') {
        usage($argv[0]);
        exit(0);
    } else if ($argv[1] === '--dry-run') {
        $dry = true;
        array_splice($argv, 1, 1);
    } else if (ctype_digit($argv[1])) {
        $scenario_id = intval($argv[1]);
        array_splice($argv, 1, 1);
    } else {
        fwrite(STDERR, "Invalid option -- {$argv[1]}\n\n");
        usage($argv[0]);
        exit(1);
    }
}

if ($scenario_id === null) {
    fwrite(STDERR, "No scenario ID provided\n");
    exit(1);
}

if ($dry) {
    delete_scenario_cascade(null, $scenario_id, $dry);
    exit(0);
}

$config_lcas = parse_ini_file('config_lcas.ini', true);

if (!array_key_exists('host', $config_lcas)
    || !array_key_exists('username', $config_lcas)
    || !array_key_exists('password', $config_lcas)
    || !array_key_exists('database', $config_lcas)
    || !array_key_exists('port', $config_lcas)
) {
    fwrite(STDERR, "Invalid configuration\n");
    exit(1);
}

$conn = new mysqli(
    $config_lcas['host'],
    $config_lcas['username'],
    $config_lcas['password'],
    $config_lcas['database'],
    $config_lcas['port']
);
if ($conn->connect_errno) {
    fwrite(STDERR, "Failed to connect to MySQL: ({$conn->connect_errno}) {$conn->connect_error}\n");
    exit(1);
}
echo "Connected to MySQL server {$conn->host_info}\n";

delete_scenario_cascade($conn, $scenario_id, $dry);

$conn->close();

?>
