#!/usr/bin/env php
<?php
/**
 * This program deletes the scenarios from the LCAS MySQL database
 * that meets one of the following conditions:
 *
 * - scenario_actions.content = '長期データなし'
 * - scenario_actions.content = '電池交換が必要なセンサーがあります'
 *
 * @author Alexis Jeandeau <alexis@z-works.co.jp>
 */

require_once __DIR__ . '/include/lcas_db.inc.php';
require_once __DIR__ . '/include/lcc_db.inc.php';


function usage($prog_name) {
    echo "Usage: {$prog_name}\n";
    echo "  -h, --help  display this help and exit\n";
    echo "  --dry-run   don't delete the scenarios, just print the queries\n";
}


if ($argc > 2) {
    fwrite(STDERR, "Too many arguments\n\n");
    usage($argv[0]);
    exit(1);
}

$dry = false;

if ($argc === 2 ) {
    if ($argv[1] === '-h' || $argv[1] === '--help') {
        usage($argv[0]);
        exit(0);
    } else if ($argv[1] === '--dry-run') {
        $dry = true;
    }
}

$lcas_config = parse_ini_file('config.ini', true);
$lcc_config = parse_ini_file('config_lcc.ini', true);

if (!array_key_exists('host', $lcas_config)
    || !array_key_exists('username', $lcas_config)
    || !array_key_exists('password', $lcas_config)
    || !array_key_exists('database', $lcas_config)
    || !array_key_exists('port', $lcas_config)
    || !array_key_exists('host', $lcc_config)
    || !array_key_exists('username', $lcc_config)
    || !array_key_exists('password', $lcc_config)
    || !array_key_exists('database', $lcc_config)
    || !array_key_exists('port', $lcc_config)
) {
    exit('Invalid configuration');
}

$lcas_conn = new mysqli(
    $lcas_config['host'],
    $lcas_config['username'],
    $lcas_config['password'],
    $lcas_config['database'],
    $lcas_config['port']
);
if ($lcas_conn->connect_errno) {
    fwrite(STDERR, "Failed to connect to MySQL: ({$lcas_conn->connect_errno}) {$lcas_conn->connect_error}\n");
    exit(1);
}
echo "Connected to LCAS MySQL server {$lcas_conn->host_info}\n";

$lcc_conn = new mysqli(
    $lcc_config['host'],
    $lcc_config['username'],
    $lcc_config['password'],
    $lcc_config['database'],
    $lcc_config['port']
);
if ($lcc_conn->connect_errno) {
    fwrite(STDERR, "Failed to connect to MySQL: ({$lcc_conn->connect_errno}) {$lcc_conn->connect_error}\n");
}
echo "Connected to LCC MySQL server {$lcc_conn->host_info}\n";

$scenario_ids = find_scenarios_no_data_or_no_battery($lcas_conn, $dry);
$count = count($scenario_ids);

if ($count == 0) {
    exit;
}

if (!$dry) {
    echo "Deleting {$count} LCAS and LCC scenario(s). Type 'yes' to continue: ";
    $handle = fopen('php://stdin', 'r');
    $line = fgets($handle);
    if (trim($line) != 'yes') {
        exit;
    }
    fclose($handle);
}

foreach ($scenario_ids as $scenario_id) {
    delete_scenario_cascade($lcas_conn, $scenario_id, $dry);
}

if (!$dry) {
    echo "LCAS scenario deletion done\n";
}

$lcas_conn->close();

foreach ($scenario_ids as $scenario_id) {
    delete_lcc_scenario_cascade($lcc_conn, $scenario_id, $dry);
}

if (!$dry) {
    echo "LCC scenario deletion done\n";
}

$lcc_conn->close();

?>
