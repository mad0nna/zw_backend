#!/usr/bin/env php
<?php
/**
 * This program sends a "Scenario Deregistration Request" to ZIO for the
 * specified scenario ID.
 *
 * @author Alexis Jeandeau <alexis@z-works.co.jp>
 */

require_once __DIR__ . '/include/amqp.inc.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Connection\AMQPSSLConnection;


function usage($prog_name) {
    echo "Usage: {$prog_name} SCENARIO_ID\n";
    echo "Delete the corresponding scenario from the LCAS database, " .
         "along with its conditions and actions.\n\n";
    echo "  -h, --help  display this help and exit\n";
    echo "  --dry-run   don't send the scenarios, just print them\n";
}


if ($argc > 3) {
    fwrite(STDERR, "Too many arguments\n\n");
    usage($argv[0]);
    exit(1);
} else if ($argc < 2) {
    usage($argv[0]);
    exit(1);
}

$dry = false;
$scenario_id = null;

while (count($argv) > 1) {
    if ($argv[1] === '-h' || $argv[1] === '--help') {
        usage($argv[0]);
        exit(0);
    } else if ($argv[1] === '--dry-run') {
        $dry = true;
        array_splice($argv, 1, 1);
    } else if (ctype_digit($argv[1])) {
        $scenario_id = intval($argv[1]);
        array_splice($argv, 1, 1);
    } else {
        fwrite(STDERR, "Invalid option -- {$argv[1]}\n\n");
        usage($argv[0]);
        exit(1);
    }
}

if (is_null($scenario_id)) {
    fwrite(STDERR, "SCENARIO_ID is missing\n\n");
    usage($argv[0]);
    exit(1);
}

$config_amqp = parse_ini_file('config_amqp.ini', true);

if (!array_key_exists('host', $config_amqp)
    || !array_key_exists('port', $config_amqp)
    || !array_key_exists('user', $config_amqp)
    || !array_key_exists('pass', $config_amqp)
    || !array_key_exists('vhost', $config_amqp)
    || !array_key_exists('exchange_name', $config_amqp)
) {
    exit('Invalid configuration');
}

if (!$dry) {
    $options = array(
        'login_method' => 'EXTERNAL',
        'connection_timeout' => 10,
    );

    if (array_key_exists('tls_client_cert', $config_amqp)
        && array_key_exists('tls_ca_cert', $config_amqp)) {

        $ssl_options = array(
            'local_cert'    => $config_amqp['tls_client_cert'],
            'cafile'        => $config_amqp['tls_ca_cert'],
            'verify_peer'   => true,
        );

        $amqp_conn = new AMQPSSLConnection(
            $config_amqp['host'],
            $config_amqp['port'],
            $config_amqp['user'],
            $config_amqp['pass'],
            $config_amqp['vhost'],
            $ssl_options,
            $options
        );
    } else {
        $amqp_conn = new AMQPConnection(
            $config_amqp['host'],
            $config_amqp['port'],
            $config_amqp['user'],
            $config_amqp['pass'],
            $config_amqp['vhost'],
            $options
        );
    }

    $channel = $amqp_conn->channel();
} else {
    $amqp_conn = null;
    $channel = null;
}

echo "Connected to RabbitMQ server {$config_amqp['host']}\n";

deregister_scenario($scenario_id, $channel, $config_amqp, $dry);

?>
