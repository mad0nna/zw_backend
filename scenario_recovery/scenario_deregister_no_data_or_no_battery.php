#!/usr/bin/env php
<?php
/**
 * This program deletes the scenarios from the LCAS MySQL database
 * that meet one of the following conditions:
 *
 * - scenario_actions.content = '長期データなし'
 * - scenario_actions.content = '電池交換が必要なセンサーがあります'
 *
 * @author Alexis Jeandeau <alexis@z-works.co.jp>
 */

require_once __DIR__ . '/include/amqp.inc.php';
require_once __DIR__ . '/include/lcas_db.inc.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Connection\AMQPSSLConnection;


function usage($prog_name) {
    echo "Usage: {$prog_name}\n";
    echo "  -h, --help      display this help and exit\n";
    echo "  --dry-run       don't send the scenarios, just print them\n";
    echo "  --amqp-timeout  AMQP response timeout (default: don't wait)\n";
}


if ($argc > 2) {
    fwrite(STDERR, "Too many arguments\n\n");
    usage($argv[0]);
    exit(1);
}

$dry = false;
$scenario_id = null;

while (count($argv) > 1) {
    if ($argv[1] === '-h' || $argv[1] === '--help') {
        usage($argv[0]);
        exit(0);
    } else if ($argv[1] === '--dry-run') {
        $dry = true;
        array_splice($argv, 1, 1);
    } else if ($argv[1] === '--dry-run') {
        $dry = true;
        array_splice($argv, 1, 1);
    } else {
        fwrite(STDERR, "Invalid option -- {$argv[1]}\n\n");
        usage($argv[0]);
        exit(1);
    }
}


$config_amqp = parse_ini_file('config_amqp.ini', true);
$config_lcas = parse_ini_file('config_lcas.ini', true);

if (!array_key_exists('host', $config_lcas)
    || !array_key_exists('username', $config_lcas)
    || !array_key_exists('password', $config_lcas)
    || !array_key_exists('database', $config_lcas)
    || !array_key_exists('port', $config_lcas)
    || !array_key_exists('amqp_host', $config_amqp)
    || !array_key_exists('amqp_port', $config_amqp)
    || !array_key_exists('amqp_user', $config_amqp)
    || !array_key_exists('amqp_pass', $config_amqp)
    || !array_key_exists('amqp_vhost', $config_amqp)
    || !array_key_exists('amqp_exchange_name', $config_amqp)
) {
    exit('Invalid configuration');
}


$conn = new mysqli(
    $config_lcas['host'],
    $config_lcas['username'],
    $config_lcas['password'],
    $config_lcas['database'],
    $config_lcas['port']
);
if ($conn->connect_errno) {
    echo "Failed to connect to MySQL: ({$conn->connect_errno}) "
        . $conn->connect_error;
}
echo "Connected to MySQL server {$conn->host_info}\n";


if (!$dry) {
    $options = array(
        'login_method' => 'EXTERNAL',
        'connection_timeout' => 10,
    );

    if (array_key_exists('amqp_tls_client_cert', $config_amqp)
        && array_key_exists('amqp_tls_ca_cert', $config_amqp)) {

        $ssl_options = array(
            'local_cert'    => $config_amqp['amqp_tls_client_cert'],
            'cafile'        => $config_amqp['amqp_tls_ca_cert'],
            'verify_peer'   => true,
        );

        $amqp_conn = new AMQPSSLConnection(
            $config_amqp['amqp_host'],
            $config_amqp['amqp_port'],
            $config_amqp['amqp_user'],
            $config_amqp['amqp_pass'],
            $config_amqp['amqp_vhost'],
            $ssl_options,
            $options
        );
    } else {
        $amqp_conn = new AMQPConnection(
            $config_amqp['amqp_host'],
            $config_amqp['amqp_port'],
            $config_amqp['amqp_user'],
            $config_amqp['amqp_pass'],
            $config_amqp['amqp_vhost'],
            $options
        );
    }

    $channel = $amqp_conn->channel();
} else {
    $amqp_conn = null;
    $channel = null;
}

echo "Connected to RabbitMQ server {$config_amqp['amqp_host']}\n";


$scenario_ids = find_scenarios_no_data_or_no_battery($conn, false);
$count = count($scenario_ids);

$conn->close();

if ($count == 0) {
    exit;
}

echo "Sending {$count} Scenario Deregistration Request(s).";
echo "Type 'yes' to continue: ";

$handle = fopen('php://stdin', 'r');
$line = fgets($handle);
if (trim($line) != 'yes') {
    exit;
}
fclose($handle);

foreach ($scenario_ids as $scenario_id) {
    deregister_scenario($scenario_id, $channel, $config_amqp, $dry, false);
}

echo "{$count} scenario(s) deleted\n";

?>
