#!/usr/bin/env php
<?php
/**
 * This program retrieves the scenarios registered in the LCAS MySQL database
 * and sends a "Scenario Registration Request" to the LifeEngine for each
 * scenario.
 *
 * @author Alexis Jeandeau <alexis@z-works.co.jp>
 */

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Connection\AMQPSSLConnection;
use PhpAmqpLib\Message\AMQPMessage;

define('SCENARIO_ACTION_TYPE_SEND_MESSAGE', 1);
define('SCENARIO_CONDITION_OPERATOR_AND', 1);
define('SCENARIO_CONDITION_OPERATOR_OR', 2);
define('SCENARIO_OPERATOR_LESS', 1);
define('SCENARIO_OPERATOR_LESS_EQ', 2);
define('SCENARIO_OPERATOR_EQ', 3);
define('SCENARIO_OPERATOR_GREATER', 4);
define('SCENARIO_OPERATOR_GREATER_EQ', 5);
define('SCENARIO_OPERATOR_FREQ', 6);
define('SCENARIO_TYPE_SIMPLE', 1);
define('SCENARIO_TYPE_WBGT', 2);
define('SCENARIO_TYPE_SCXML', 3);

define('DEVICE_TYPE_TEMPERATURE', 1);
define('DEVICE_TYPE_HUMIDITY', 2);
define('DEVICE_TYPE_BATTERY', 7);
define('DEVICE_TYPE_LUMINANCE', 8);


/**
 * Insert a key => val at the beginning of an associative array.
 *
 * @return array
 */
function array_unshift_assoc(&$arr, $key, $val) {
    $arr = array_reverse($arr, true);
    $arr[$key] = $val;
    return array_reverse($arr, true);
}

/**
 * Mapping between the database integer scenario type and its string
 * description.
 *
 * @return array
 */
function scenario_type_mapping() {
    return array(
        SCENARIO_TYPE_SIMPLE => 'simple',
        SCENARIO_TYPE_WBGT   => 'wbgt',
        SCENARIO_TYPE_SCXML  => 'scxml'
    );
}

function get_scenario_type_name($type) {
    $mapping = scenario_type_mapping();
    if (isset($mapping[$type])) {
        return $mapping[$type];
    }
    return false;
}

function get_scenario_type($type_str) {
    $mapping = scenario_type_mapping();
    return array_search($type_str, $mapping);
}

function scenario_action_type_mapping() {
    return array(
        SCENARIO_ACTION_TYPE_SEND_MESSAGE => 'send_message'
    );
}

function get_scenario_action_type_name($type) {
    $mapping = scenario_action_type_mapping();
    if (isset($mapping[$type])) {
        return $mapping[$type];
    }
    return false;
}

function get_scenario_action_type($type_str) {
    $mapping = scenario_action_type_mapping();
    return array_search($type_str, $mapping);
}

function condition_operator_type_mapping() {
    return array(
        SCENARIO_CONDITION_OPERATOR_AND => 'AND',
        SCENARIO_CONDITION_OPERATOR_OR  => 'OR',
    );
}

function get_condition_operator_type_name($type) {
    $mapping = condition_operator_type_mapping();
    if (isset($mapping[$type])) {
        return $mapping[$type];
    }
    return false;
}

function get_condition_operator_type($type_str) {
    $mapping = condition_operator_type_mapping();
    return array_search($type_str, $mapping);
}

function operator_type_mapping() {
    return array(
        SCENARIO_OPERATOR_LESS       => '<',
        SCENARIO_OPERATOR_LESS_EQ    => '<=',
        SCENARIO_OPERATOR_EQ         => '==',
        SCENARIO_OPERATOR_GREATER    => '>',
        SCENARIO_OPERATOR_GREATER_EQ => '>=',
        SCENARIO_OPERATOR_FREQ       => 'freq',
    );
}

function get_operator_type_name($type) {
    $mapping = operator_type_mapping();
    if (isset($mapping[$type])) {
        return $mapping[$type];
    }
    return false;
}

function get_operator_type($type_str) {
    $mapping = operator_type_mapping();
    return array_search($type_str, $mapping);
}

function get_scenarios($conn) {
    $scenarios = [];

    // Ensure the data is read as UTF-8
    $conn->query('SET NAMES utf8');

    // Only simple scenarios are supported
    $sql = 'SELECT id, scenario_type, '
         . '       condition_operator, trigger_after, '
         . '       temperature_dev, humidity_dev, wbgt_threshold, '
         . '       enabled_days, enabled_period, '
         . '       scxml, device_list, action_state_list '
         . 'FROM scenarios '
         . 'WHERE enabled = 1 AND '
         . '(scenario_type = ' . SCENARIO_TYPE_SIMPLE
         . ' OR scenario_type = ' . SCENARIO_TYPE_WBGT
         . ' OR scenario_type = ' . SCENARIO_TYPE_SCXML . ')';

    $result = $conn->query($sql);
    if ($result && $result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $scenario_type = get_scenario_type_name($row['scenario_type']);
            if ($scenario_type === false) {
                echo "Invalid scenario_type: {$row['scenario_type']}\n";
                continue;
            }
            $row['scenario_type'] = $scenario_type;

            $row = create_scenario_reg($row, $conn);

            array_push($scenarios, $row);
        }
    } else {
        echo "No scenario found\n";
        printf("Error: %s\n", $conn->error);
    }

    return $scenarios;
}

function create_scenario_reg($row, $conn) {
    // The API needs a "scenario_id" field
    $scenario_id = $row['id'];
    unset($row['id']);
    $row = array_unshift_assoc($row, 'scenario_id', $scenario_id);

    switch ($row['scenario_type']) {
    case 'simple':
        $condition_op = get_condition_operator_type_name(
            $row['condition_operator']
        );
        if ($condition_op === false) {
            echo "Invalid condition_operator: {$row['condition_operator']}\n";
            continue;
        }
        $row['condition_operator'] = $condition_op;

        // Convert "trigger_after" to an integer
        $row['trigger_after'] = intval($row['trigger_after']);

        $conditions = get_conditions($conn, $scenario_id);
        $row['conditions'] = $conditions;

        /* Remove the fields related to WBGT scenarios */
        unset($row['temperature_dev']);
        unset($row['humidity_dev']);
        unset($row['wbgt_threshold']);

        /* Remove the fields related to SCXML scenarios */
        unset($row['scxml']);
        unset($row['device_list']);
        unset($row['action_state_list']);

        break;
    case 'wbgt':
        $row['temperature_dev'] = intval($row['temperature_dev']);
        $row['humidity_dev'] = intval($row['humidity_dev']);
        $row['wbgt_threshold'] = intval($row['wbgt_threshold']);

        /* Remove the fields related to simple scenarios */
        unset($row['condition_operator']);
        unset($row['conditions']);
        unset($row['trigger_after']);

        /* Remove the fields related to SCXML scenarios */
        unset($row['scxml']);
        unset($row['device_list']);
        unset($row['action_state_list']);

        break;
    case 'scxml':
        /* Remove the fields related to simple scenarios */
        unset($row['condition_operator']);
        unset($row['conditions']);
        unset($row['trigger_after']);

        /* Remove the fields related to WBGT scenarios */
        unset($row['temperature_dev']);
        unset($row['humidity_dev']);
        unset($row['wbgt_threshold']);

        break;
    }

    $actions = get_actions($conn, $scenario_id);
    $row['actions'] = $actions;

    if (isset($row['enabled_days'])) {
        /* 'mon,tue,fri' => ['mon', 'tue', 'fri'] */
        $row['enabled_days'] = explode(',', $row['enabled_days']);
    } else {
        unset($row['enabled_days']);
    }

    if (!isset($row['enabled_period'])) {
        unset($row['enabled_period']);
    }

    if (isset($row['device_list'])) {
        /* 12,25,78 => [12, 25, 78] */
        $row['device_list'] = array_map(
            'intval',
            explode(',', $row['device_list'])
        );
    } else {
        unset($row['device_list']);
    }

    if (isset($row['action_state_list'])) {
        /* 'start,event' => ['start', 'event'] */
        $row['action_state_list'] = explode(',', $row['action_state_list']);
    } else {
        unset($row['action_state_list']);
    }

    return $row;
}

function insert_scenario($conn, $scenario) {
    $sql = "INSERT INTO scenarios (id, scenario_type, condition_operator, "
         .     "trigger_after, enabled) "
         . "VALUES (?, ?, ?, ?, 1)";

    $scenario_type = get_scenario_type($scenario['scenario_type']);
    if ($scenario_type === false) {
        echo "Invalid scenario_type: {$scenario['scenario_type']}\n";
        continue;
    }
    $scenario['scenario_type'] = $scenario_type;

    $condition_op = get_condition_operator_type($scenario['condition_operator']);
    if ($condition_op === false) {
        echo "Invalid condition_operator: {$scenario['condition_operator']}\n";
        continue;
    }
    $scenario['condition_operator'] = $condition_op;

    $stmt = $conn->prepare($sql);

    $stmt->bind_param(
        'iiii',
        $scenario['scenario_id'],
        $scenario['scenario_type'],
        $scenario['condition_operator'],
        $scenario['trigger_after']
    );

    if ($stmt->execute()) {
        echo "Successfully inserted scenario {$scenario['scenario_id']}\n";
    } else {
        fwrite(STDERR, "Error inserting scenario: ({$conn->errno}) {$conn->error}\n");
    }

    $stmt->close();
}

function get_device_type($conn, $device_id) {
    $sql = "SELECT type FROM devices WHERE id = {$device_id} LIMIT 1";

    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // Output data of each row
        while($row = $result->fetch_assoc()) {
            return intval($row['type']);
        }
    }

    return null;
}

function fix_condition($conn, &$row) {
    if ($row['unit'] === null ||
        preg_replace('/\s+/', '', $row['unit']) === '') {

        // Check the device type and add a unit if it shouldn't be
        // empty
        $device_type = get_device_type($conn, intval($row['lhs']));
        switch ($device_type) {
        case DEVICE_TYPE_TEMPERATURE:
            $row['unit'] = 'C';
            break;
        case DEVICE_TYPE_HUMIDITY:
        case DEVICE_TYPE_BATTERY:
        case DEVICE_TYPE_LUMINANCE:
            $row['unit'] = '%';
            break;
        default:
            unset($row['unit']);
            break;
        }
    }

    if ($row['operator'] == SCENARIO_OPERATOR_FREQ && $row['rhs'] == '0') {
        // Fix invalid RHS for freq operator
        $row['rhs'] = 'almost_none';
    }
}

function get_conditions($conn, $scenario_id) {
    $conditions = [];

    $sql = "SELECT lhs, operator, rhs, unit "
         . "FROM scenario_conditions "
         . "WHERE scenario_id={$scenario_id}";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // Output data of each row
        while($row = $result->fetch_assoc()) {
            fix_condition($conn, $row);

            $operator = get_operator_type_name($row['operator']);
            if ($operator === false) {
                echo "Invalid operator: {$row['operator']}\n";
                continue;
            }
            $row['operator'] = $operator;


            array_push($conditions, $row);
        }
    }

    return $conditions;
}

function insert_condition($conn, $condition, $scenario_id) {
    $sql = "INSERT INTO scenario_conditions (scenario_id, lhs, operator, rhs, unit) "
         . "VALUES (?, ?, ?, ?, ?)";

    $operator = get_operator_type($condition['operator']);
    if ($operator === false) {
        echo "Invalid operator: {$condition['operator']}\n";
        continue;
    }
    $condition['operator'] = $operator;

    if (!array_key_exists('unit', $condition)) {
        $condition['unit'] = '';
    }

    $stmt = $conn->prepare($sql);

    $stmt->bind_param(
        'issss',
        $scenario_id,
        $condition['lhs'],
        $condition['operator'],
        $condition['rhs'],
        $condition['unit']
    );

    if ($stmt->execute()) {
        echo "Successfully inserted condition for scenario {$scenario_id}\n";
    } else {
        fwrite(STDERR, "Error inserting condition: ({$conn->errno}) {$conn->error}\n");
    }

    $stmt->close();
}

function get_actions($conn, $scenario_id) {
    $actions = [];

    $sql = "SELECT type, content "
         . "FROM scenario_actions "
         . "WHERE scenario_id={$scenario_id}";

    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $type = get_scenario_action_type_name($row['type']);
            if ($type === false) {
                echo "Invalid action type: {$row['type']}\n";
                continue;
            }
            $row['type'] = $type;

            array_push($actions, $row);
        }
    }

    return $actions;
}

function insert_action($conn, $action, $scenario_id) {
    $sql = "INSERT INTO scenario_actions (scenario_id, type, content) "
         . "VALUES (?, ?, ?)";

    $type = get_scenario_action_type($action['type']);
    if ($type === false) {
        echo "Invalid action type: {$action['type']}\n";
        continue;
    }
    $action['type'] = $type;

    $stmt = $conn->prepare($sql);
    $stmt->bind_param('iis', $scenario_id, $action['type'], $action['content']);

    if ($stmt->execute()) {
        echo "Successfully inserted action for scenario {$scenario_id}\n";
    } else {
        fwrite(STDERR, "Error inserting action: ({$conn->errno}) {$conn->error}\n");
    }

    $stmt->close();
}

function delete_scenarios($conn) {
    if ($conn->query("DELETE FROM scenario_actions")) {
        echo "Deleted {$conn->affected_rows} actions\n";
    } else {
        fwrite(STDERR, "Error deleting actions: ({$conn->errno}) {$conn->error}\n");
    }
    if ($conn->query("DELETE FROM scenario_conditions")) {
        echo "Deleted {$conn->affected_rows} conditions\n";
    } else {
        fwrite(STDERR, "Error deleting conditions: ({$conn->errno}) {$conn->error}\n");
    }
    if ($conn->query("DELETE FROM scenarios")) {
        echo "Deleted {$conn->affected_rows} scenarios\n";
    } else {
        fwrite(STDERR, "Error deleting scenarios: ({$conn->errno}) {$conn->error}\n");
    }
}

function wait_for_response($scenario, $channel, $config_amqp, $write, $sql) {
    $queue = 'intermediate_' . $config_amqp['amqp_user'];
    $consumer_tag = $channel->basic_consume(
        $queue,
        '',
        false,
        false,
        false,
        false,
        function ($msg) use ($scenario, $write, $sql) {
            // Send back ACK
            $msg->delivery_info['channel']->
                basic_ack($msg->delivery_info['delivery_tag']);

            if ($msg == null) {
                echo "\nReceived response is null\n";
                return;
            }
            $body = json_decode($msg->body, true);
            if ($body['command'] != 'scenario_reg') {
                echo "\nReceived response is " . $body['command']
                    . " instead of scenario_reg\n";
                return;
            }

            // Stop waiting for messages (quit callback loop)
            $delivery_info = $msg->delivery_info;
            $consumer_tag = $delivery_info['consumer_tag'];
            $delivery_info['channel']->basic_cancel($consumer_tag);

            if ($body['result_code'] != 200) {
                echo "\nReceived error code " . $body['result_code'] . "\n";
                echo "Reason: " . implode(', ', $body['reason']) . "\n";
                return;
            }

            $newID = $body['scenario_id'];
            echo "\nRegistered scenario ID ${newID}\n";

            $scenario['scenario_id'] = $newID;

            if ($write) {
                insert_new_scenario($sql, $scenario);
            }
        }
    );

    try {
        while (count($channel->callbacks)) {
            $channel->wait(null, false, 5);
        }
    } catch (AMQPTimeoutException $e) {
        // Stop waiting for messages (quit callback loop)
        $channel->basic_cancel($consumer_tag);
        return false;
    }

    return true;
}

/**
 * Send a Scenario Registration AMQP command to the cloud.
 */
function register_scenario($scenario, $channel, $config_amqp, $dry, $write,
        $wait, $sql) {

    if (!array_key_exists('command', $scenario)) {
        $scenario = array_unshift_assoc($scenario, 'command', 'scenario_reg');
    }

    $json = json_encode($scenario);
    if (!$json) {
        throw new \RuntimeException(
            'Failed to encode scenario into JSON: ' . $scenario
        );
    }

    if ($dry) {
        echo "\nScenario Registration Request:\n" . $json . "\n";
        if ($write) {
            insert_new_scenario($sql, $scenario);
        }
        return;
    }

    echo "\nSending:\n" . $json . "\n";

    $message = new AMQPMessage(
        $json,
        array(
            'content_type'   => 'application/json',
            'user_id'        => $config_amqp['amqp_user'],
            'delivery_mode'  => 2,
            'correlation_id' => $scenario['scenario_id']
        )
    );

    $channel->basic_publish($message, $config_amqp['amqp_exchange_name']);

    if ($wait) {
        return wait_for_response(
            $scenario,
            $channel,
            $config_amqp,
            $write,
            $sql
        );
    } else {
        return true;
    }
}

function insert_new_scenario($conn, $scenario) {
    insert_scenario($conn, $scenario);

    foreach ($scenario['actions'] as $action) {
        insert_action($conn, $action, $scenario['scenario_id']);
    }

    foreach ($scenario['conditions'] as $condition) {
        insert_condition($conn, $condition, $scenario['scenario_id']);
    }
}

/**
 * Parse the input file and return an array of scenario IDs.
 *
 * @return int[]
 */
function parse_scenario_list($filename) {
    return file($filename, FILE_IGNORE_NEW_LINES);
}

function usage($prog_name) {
    echo "Usage: {$prog_name} [OPTION]...\n";
    echo "Send Scenario Registration Requests to LifeEngine according to " .
         "the database contents.\n\n";
    echo "  -h, --help  display this help and exit\n";
    echo "  --dry-run   don't send the scenarios, just print them\n";
    echo "  --nowait    don't wait for the response notification\n";
    echo "  --write     rewrite the scenario in the database\n";
    echo "  --count     shows the number of registered scenarios then quit\n";
}


if ($argc > 4) {
    fwrite(STDERR, "Too many arguments\n\n");
    usage($argv[0]);
    exit(1);
}

$count = false;
$dry = false;
$scenarioIds = null;
$wait = true;
$write = false;

while (count($argv) > 1) {
    switch ($argv[1]) {
    case '--count':
        $count = true;
        array_splice($argv, 1, 1);
        break;
    case '--dry-run':
        $dry = true;
        array_splice($argv, 1, 1);
        break;
    case '-h':
    case '--help':
        usage($argv[0]);
        exit(0);
    case '--input-file':
        if (count($argv) < 2) {
            fwrite(STDERR, "No input file specified\n\n");
            usage($argv[0]);
            exit(1);
        }
        $input = $argv[2];
        if (!file_exists($input)) {
            fwrite(STDERR, "File not found: {$input}\n\n");
            exit(1);
        }
        $scenarioIds = parse_scenario_list($input);
        if ($scenarioIds === false) {
            fwrite(STDERR, "Couldn't read lines from file: {$filename}\n\n");
            exit(1);
        }
        array_splice($argv, 1, 2);
        break;
    case '--nowait':
        $wait = false;
        array_splice($argv, 1, 1);
        break;
    case '--write':
        $write = true;
        array_splice($argv, 1, 1);
        break;
    default:
        fwrite(STDERR, "Invalid option -- {$argv[1]}\n\n");
        usage($argv[0]);
        exit(1);
    }
}

if ($write) {
    $question = "This will overwrite the database. Are you sure? (y/N) ";
    if (readline($question) != 'y') {
        exit(0);
    }
}

$config_amqp = parse_ini_file('config_amqp.ini', true);
$config_lcas = parse_ini_file('config_lcas.ini', true);

if (!array_key_exists('host', $config_lcas)
    || !array_key_exists('username', $config_lcas)
    || !array_key_exists('password', $config_lcas)
    || !array_key_exists('database', $config_lcas)
    || !array_key_exists('port', $config_lcas)
    || !array_key_exists('host', $config_amqp)
    || !array_key_exists('port', $config_amqp)
    || !array_key_exists('user', $config_amqp)
    || !array_key_exists('pass', $config_amqp)
    || !array_key_exists('vhost', $config_amqp)
    || !array_key_exists('exchange_name', $config_amqp)
) {
    exit('Invalid configuration');
}

$sql = new mysqli(
    $config_lcas['host'],
    $config_lcas['username'],
    $config_lcas['password'],
    $config_lcas['database'],
    $config_lcas['port']
);
if ($sql->connect_errno) {
    echo "Failed to connect to MySQL: ({$sql->connect_errno}) "
        . $sql->connect_error;
}
echo "Connected to MySQL server {$sql->host_info}\n";

$sql->autocommit(false);

$scenarios = get_scenarios($sql);

if ($count) {
    $scenario_count = count($scenarios);
    echo "Found {$scenario_count} scenario(s)\n";
    exit(0);
}

if (!$dry) {
    $options = array(
        'login_method' => 'EXTERNAL',
        'connection_timeout' => 10,
    );

    if (array_key_exists('tls_client_cert', $config_amqp)
        && array_key_exists('tls_ca_cert', $config_amqp)) {

        $ssl_options = array(
            'local_cert'    => $config_amqp['tls_client_cert'],
            'cafile'        => $config_amqp['tls_ca_cert'],
            'verify_peer'   => true,
        );

        $amqp_conn = new AMQPSSLConnection(
            $config_amqp['host'],
            $config_amqp['port'],
            $config_amqp['user'],
            $config_amqp['pass'],
            $config_amqp['vhost'],
            $ssl_options,
            $options
        );
    } else {
        $amqp_conn = new AMQPConnection(
            $config_amqp['host'],
            $config_amqp['port'],
            $config_amqp['user'],
            $config_amqp['pass'],
            $config_amqp['vhost'],
            $options
        );
    }

    $channel = $amqp_conn->channel();
} else {
    $amqp_conn = null;
    $channel = null;
}

if (is_null($scenarioIds)) {
    if ($write) {
        delete_scenarios($sql);
    }

    foreach ($scenarios as $scenario) {
        register_scenario(
            $scenario,
            $channel,
            $config_amqp,
            $dry,
            $write,
            $wait,
            $sql
        );
    }
} else {
    // Filter the scenarios to register
    $toRegister = array_filter($scenarios, function($scenario) use ($scenarioIds) {
        return in_array($scenario['scenario_id'], $scenarioIds);
    });

    foreach ($toRegister as $scenario) {
        register_scenario(
            $scenario,
            $channel,
            $config_amqp,
            $dry,
            $write,
            $wait,
            $sql
        );
    }
}

if ($dry) {
    $sql->rollback();
} else {
    $sql->commit();
}

$sql->close();

?>
