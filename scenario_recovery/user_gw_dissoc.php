#!/usr/bin/env php
<?php
/**
 * This program sends a "User Gateway Dissociation Request" to ZIO.
 *
 * @author Alexis Jeandeau <alexis@z-works.co.jp>
 */

require_once __DIR__ . '/include/amqp.inc.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Connection\AMQPSSLConnection;


function usage($prog_name) {
    echo "Usage: {$prog_name} USERNAME GW_NAME\n";
    echo "Send a User Gateway Dissociation Request\n\n";
    echo "  -h, --help  display this help and exit\n";
    echo "  --dry-run   don't send the scenarios, just print them\n";
}


if ($argc > 3) {
    fwrite(STDERR, "Too many arguments\n\n");
    usage($argv[0]);
    exit(1);
} else if ($argc < 3) {
    usage($argv[0]);
    exit(1);
}

$dry = false;
$gw_name = null;
$username = null;

while (count($argv) > 1) {
    if ($argv[1] === '-h' || $argv[1] === '--help') {
        usage($argv[0]);
        exit(0);
    } else if ($argv[1] === '--dry-run') {
        $dry = true;
        array_splice($argv, 1, 1);
    } else if (is_null($gw_name)) {
        $gw_name = $argv[1];
    } else {
        $username = $argv[1];
    }
}

if (is_null($gw_name)) {
    fwrite(STDERR, "GW_NAME is missing\n\n");
    usage($argv[0]);
    exit(1);
}

if (is_null($username)) {
    fwrite(STDERR, "USERNAME is missing\n\n");
    usage($argv[0]);
    exit(1);
}

$config = parse_ini_file('config.ini', true);

if (!array_key_exists('amqp_host', $config)
    || !array_key_exists('amqp_port', $config)
    || !array_key_exists('amqp_user', $config)
    || !array_key_exists('amqp_pass', $config)
    || !array_key_exists('amqp_vhost', $config)
    || !array_key_exists('amqp_exchange_name', $config)
) {
    exit('Invalid configuration');
}

if (!$dry) {
    $options = array(
        'login_method' => 'EXTERNAL',
        'connection_timeout' => 10,
    );

    if (array_key_exists('amqp_tls_client_cert', $config)
        && array_key_exists('amqp_tls_ca_cert', $config)) {

        $ssl_options = array(
            'local_cert'    => $config['amqp_tls_client_cert'],
            'cafile'        => $config['amqp_tls_ca_cert'],
            'verify_peer'   => true,
        );

        $amqp_conn = new AMQPSSLConnection(
            $config['amqp_host'],
            $config['amqp_port'],
            $config['amqp_user'],
            $config['amqp_pass'],
            $config['amqp_vhost'],
            $ssl_options,
            $options
        );
    } else {
        $amqp_conn = new AMQPConnection(
            $config['amqp_host'],
            $config['amqp_port'],
            $config['amqp_user'],
            $config['amqp_pass'],
            $config['amqp_vhost'],
            $options
        );
    }

    $channel = $amqp_conn->channel();
} else {
    $amqp_conn = null;
    $channel = null;
}

echo "Connected to RabbitMQ server {$config['amqp_host']}\n";

dissociate_gateway($username, $gw_name, $channel, $config, $dry);

?>
