// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var dateFormat = require('dateformat');
var fs = require('fs');
var nconf = require('nconf');
var DB = require('./lib/db/db');
var logger = require('./lib/logger');
var DeviceRepository = require('./lib/repository/device_repository');
var DeviceDataRepository = require('./lib/repository/device_data_repository');
var NodeRepository = require('./lib/repository/node_repository');
var ZworksNotificationRepository = require('./lib/repository/zworks_notification_repository');

var deviceList = {};

//---------------------------------
// 設定情報のロード
nconf.file('./config/config.json');
var env_config_path = './config/config_development.json';
if(fs.statSync(env_config_path).isFile()) {
    nconf.file(env_config_path);
}

DB.init({
    host: nconf.get('db:host'),
    port: nconf.get('db:port'),
    user: nconf.get('db:user'),
    pass: nconf.get('db:pass'),
    name: nconf.get('db:name')
});


function startUpdate(types, interval) {
    setTimeout(function onUpdate(){

        var self = this;
        var connection;
        co(function *(){
            connection = yield DB.getConnection();
            var deviceRepository = new DeviceRepository(connection);
            var devices = yield deviceRepository.findByTypes(types);

            for(var i in devices) {
                switch(devices[i]['type']) {
                    case 1:
                    case 2:
                    case 7:
                    case 8:
                        yield updateByRange(connection, devices[i]['id'], devices[i]['type']);
                        break;
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        yield updateByFlag(connection, devices[i]['id'], devices[i]['type']);
                        break;
                }
            }
            connection.release();
            setTimeout(onUpdate, interval * (0.5 + (Math.random() * 0.5)));
        }).catch(function(err){
            logger.addError(err);
            connection.destroy();
            setTimeout(onUpdate, interval * (0.5 + (Math.random() * 0.5)));
        });
    }, 1000);
}

startUpdate([1], 5 * 60 * 1000);
startUpdate([7], 5 * 60 * 1000);
startUpdate([2,8], 5 * 60 * 1000);
startUpdate([3,4,5,6], 5 * 60 * 1000);

/*
startUpdate([1], 5 * 10 * 1000);
startUpdate([7], 5 * 10 * 1000);
startUpdate([2,8], 5 * 10 * 1000);
startUpdate([3,4,5,6], 5 * 10 * 1000);
*/

function updateByRange(connection, deviceId, type) {

    var patterns = {
        1: {type: 'temperature', start: 15, min: 15, max: 30, step:  1, unit: 'C', is_int: false},  //温度
        2: {type: 'humidity',    start: 50, min: 30, max: 70, step:  1, unit: '%', is_int: false},  //湿度
        7: {type: 'battery',     start: 100, min: 0, max: 100, step: 2, unit: '%', is_int: true},   //バッテリー
        8: {type: 'luminance',   start: 50, min:  0, max: 100, step: 1, unit: '%', is_int: false}   //照度
    };

    var typeName = patterns[type]['type'];
    var start = patterns[type]['start'];
    var min = patterns[type]['min'];
    var max = patterns[type]['max'];
    var step = patterns[type]['step'];
    var unit = patterns[type]['unit'];
    var isInt = patterns[type]['is_int'];

    var promise = co(function *(){
        var zworksNotificationRepository = new ZworksNotificationRepository(connection);
        var deviceDataRepository = new DeviceDataRepository(connection);
        var nodeRepository = new NodeRepository(connection);

        var node = yield nodeRepository.findByDeviceId(deviceId);

        //現在の値を取る
        var latestData = yield deviceDataRepository.findCurrentValue(deviceId);
        var currentValue;
        if(latestData.length > 0) {
            currentValue = parseFloat(latestData[0]['value']);
        } else {
           currentValue = patterns[type]['start'];
        }
        var oldValue = currentValue;

//        //上下させる
//        var increase = 0;
//        if(Math.random() > 0.5) {
//            increase = Math.random() * step;
//        } else {
//            increase = Math.random() * step * -1;
//        }
//
//        currentValue += increase;
//
//        if(isInt) {
//            currentValue = parseInt(currentValue);
//        } else {
//            currentValue = Math.round(currentValue * 10) / 10;
//        }
//        //min,max内におさめる
//        currentValue = Math.max(currentValue, min);
//        currentValue = Math.min(currentValue, max);

        currentValue = ((max-min) * Math.random()) + min;
        if(isInt) {
            currentValue = parseInt(currentValue);
        } else {
            currentValue = Math.round(currentValue * 10) / 10;
        }

        //更新する
        var time = new Date();
//        var data = {
//            device_id: deviceId,
//            timestamp: time,
//            old_value: oldValue,
//            value: currentValue
//        };
//        logger.addDebug(data);
//        yield deviceDataRepository.save(data);


        var data = {
            command: 'device_data',
            node_name: node['manufacturer'] + '_' + node['product'] + '_' + node['serial_no'],
            device_type: typeName,
            value: currentValue,
            timestamp: +time,
            unit: unit
        };
        var notification = {
            type: 'device_data',
            data: JSON.stringify(data),
            state: 0
        };
        logger.addDebug(notification);
        yield zworksNotificationRepository.register(notification);
    });
    return promise;
}


function updateByFlag(connection, deviceId, type) {

    var promise = co(function *(){
        var zworksNotificationRepository = new ZworksNotificationRepository(connection);
        var nodeRepository = new NodeRepository(connection);

        var node = yield nodeRepository.findByDeviceId(deviceId);

        var typeNames = {
            3: 'motion',
            4: 'open_close',
            5: 'lock',
            6: 'cover'
        };
        var typeName = typeNames[type];

        var currentValue = 0;
        if(Math.random() > 0.5) {
            currentValue = 255;
        } else {
            currentValue = 0;
        }

        //更新する
        var time = new Date();
//        var data = {
//            device_id: deviceId,
//            timestamp: time,
//            old_value: oldValue,
//            value: currentValue
//        };
//        logger.addDebug(data);
//        yield deviceDataRepository.save(data);


        var data = {
            command: 'device_data',
            node_name: node['manufacturer'] + '_' + node['product'] + '_' + node['serial_no'],
            device_type: typeName,
            value: currentValue,
            timestamp: +time,
            unit: ''
        };
        var notification = {
            type: 'device_data',
            data: JSON.stringify(data),
            state: 0
        };
        logger.addDebug(notification);
        yield zworksNotificationRepository.register(notification);
    });
    return promise;
}
