// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var cookie = require('cookie');
var DB = require('./db/db');
var logger = require('./logger');
var nconf = require('nconf');
var UserRepository = require('./repository/user_repository');
var url = require('url');
var querystring = require('querystring');

function Authenticator() {
    if (!(this instanceof Authenticator)) return new Authenticator();
}


Authenticator.prototype.loginId = 0;

Authenticator.prototype.authenticateFromRequest = function(request) {
    var self = this;

    var promise = co(function *(){
        self.loginId = 0;
        self.error = '';

        var token = '';
        var rawCookie = request.headers.cookie || '';
        if(rawCookie != '') {
            var cookieData = cookie.parse(rawCookie) || {};
            token = cookieData['SESSID'];
        }else{
            token = querystring.parse(url.parse(request.url).query).token;
        }
        logger.addDebug('auth_token: ' + token);
        if(token == '' || token == undefined) {
            return Promise.reject(Error('AUTH ERROR: Authorization failed. Token not found.'));
        }
        yield self.authenticate(token);
        return Promise.resolve();
    }).catch(function(err){
        var detailedMessage = err + JSON.stringify(request.rawHeaders);
        return Promise.reject(Error(detailedMessage));
    });

    return promise;
};


Authenticator.prototype.authenticate = function(cookie) {
    var self = this;

    var connection = null;
    var promise = co(function *(){
        self.loginId = 0;
        self.error = '';
        connection = yield DB.getConnection();
        var userRepository = new UserRepository(connection);
        var user = yield userRepository.findByAuthToken(cookie);
        if(!user) {
            connection.release();
            return Promise.reject(Error('AUTH ERROR: Authorization failed. Client cookie: ' + cookie));
        }
        connection.release();

        self.loginId = user['login_id'];

        return Promise.resolve(true);
    }).catch(function(err){
        connection.destroy();
        return Promise.reject(logger.makeError(err));
    });

    return promise;
};


Authenticator.prototype.getLoginId = function() {
    return this.loginId;
};

module.exports = Authenticator;
