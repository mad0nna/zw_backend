// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

module.exports = {
	// デバイスタイプ
    DEVICE_TYPE_TEMPERATURE: 1,
    DEVICE_TYPE_HUMIDITY: 2,
    DEVICE_TYPE_MOTION: 3,
    DEVICE_TYPE_OPEN_CLOSE: 4,
    DEVICE_TYPE_LOCK: 5,
    DEVICE_TYPE_COVER: 6,
    DEVICE_TYPE_BATTERY: 7,
    DEVICE_TYPE_LUMINANCE: 8,
    DEVICE_TYPE_CALL_BUTTON: 9,
    DEVICE_TYPE_SMART_LOCK: 10,
    DEVICE_TYPE_POWER: 11,
    DEVICE_TYPE_ACC_ENERGY: 12,
    DEVICE_TYPE_POWER_SWITCH: 13,
    DEVICE_TYPE_APPARENT_ENERGY: 14,
    DEVICE_TYPE_ACC_GAS: 15,
    DEVICE_TYPE_ACC_WATER: 16,
    DEVICE_TYPE_HEART_RATE: 17,
    DEVICE_TYPE_BREATHING_RATE: 18,
    DEVICE_TYPE_BODY_MOTION: 19,
    DEVICE_TYPE_SMOKE: 20,
    DEVICE_TYPE_PERSON_STATE: 21,
    DEVICE_TYPE_DISTANCE: 22,
    DEVICE_TYPE_PARKERS_CAR: 23,
    DEVICE_TYPE_PARKERS_LED: 24,
    DEVICE_TYPE_PARKERS_MODE: 25,
    DEVICE_TYPE_PARKERS_BATTERY: 26,

    // ノードの共有レベル
    NODE_SHARING_LEVEL_PRIVATE: 1,
    NODE_SHARING_LEVEL_USERS: 2,
    
    // ノードの状態
    NODE_STATE_NOTINCLUDED: 0,
    NODE_STATE_INCLUDED: 1,
    NODE_STATE_FAILED: 2,

    // ノードの状態（こちらの方が本物）
    NODE_YET_ANOTHER_STATE_UNKNOWN: 0,
    NODE_YET_ANOTHER_STATE_OPERATIONAL: 1,
    NODE_YET_ANOTHER_STATE_LOST: 2,
    NODE_YET_ANOTHER_STATE_NEGOTIATING: 3,

    // シナリオ条件の閾値への演算子
    SCENARIO_OPERATOR_LESS: 1,
    SCENARIO_OPERATOR_LESS_EQ: 2,
    SCENARIO_OPERATOR_EQ: 3,
    SCENARIO_OPERATOR_GREATER: 4,
    SCENARIO_OPERATOR_GREATER_EQ: 5,
    SCENARIO_OPERATOR_FREQ: 6,

    // シナリオ条件同士を繋ぐ演算子
    SCENARIO_CONDITION_OPERATOR_AND: 1,
    SCENARIO_CONDITION_OPERATOR_OR: 2,

    // シナリオの種類
    SCENARIO_TYPE_SIMPLE: 1,
    SCENARIO_TYPE_WBGT: 2,
    SCENARIO_TYPE_SCXML: 3,

    WS_HANDLER_EVENT_CONNECT: 'ws_event_connect',
    WS_HANDLER_EVENT_DUMP_CONNECTIONS: 'ws_event_dump',

    // エラー
    GENERAL_ERROR_NODE_INCLUSION: 1,
};
