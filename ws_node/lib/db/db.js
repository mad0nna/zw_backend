// vim: ts=4 sw=4 et ff=unix fenc=utf8
var mysql = require('mysql');
var co = require('co');
var logger = require('../logger');
var nconf = require('nconf');
var pool = null;

var DB = {};

DB.logQueries = false;

DB.init = function(config) {
    this.config = config;
    if(pool == null) {
        pool = mysql.createPool({
            host     : config['host'],
            port     : config['port'],
            user     : config['user'],
            password : config['pass'],
            database : config['name']
        });
    }
    DB.logQueries = nconf.get('log:log_db_queries');
};

/**
 * コネクションとしてPoolそのものを返す。
 * (クエリの都度使用される接続が変化し得るため、)
 * @returns {Pool}
 */
DB.getPool = function() {
    return pool;
};


/**
 * Promise化済みのコネクションを返す。
 * @returns {Promise}
 *
 * 複数のクエリを同じコネクションで発行したい場合はこちらを使用します。
 * 使用後に明示的にcon.release()を行う必要があります。
 */
DB.getConnection = function() {
    return new Promise(function(resolve, reject) {
        pool.getConnection(function(err, con){
            if(err) {
                reject(Error(err));
                return;
            }
            resolve(con);
        });
    });
};


/**
 *
 * @param con
 * @param sql
 * @returns {Promise}
 */
DB.query = function(con, sql, options) {
    var warnings = null;
    var result = null;
    var logQuery = DB.logQueries;
    if(typeof options == 'object' && options['logging'] != 'undefined') {
        logQuery = options['logging'];
    }

    return new Promise(function(resolve, reject){
        if(logQuery) {
            logger.addDebug('QUERY: ' + sql);
        }
        con.query(sql, function(err, rows){
            if(err) {
                reject(err + 'SQL: ' + sql);
                return;
            }
            resolve(rows);
        });
    });
};


module.exports = DB;
