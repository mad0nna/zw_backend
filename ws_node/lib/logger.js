// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict';

var co = require('co');
var fs = require('fs');
var dateFormat = require('dateformat');
var nconf = require('nconf');

var SEVERITY_LEVELS = {
    ALL:   0,
    DEBUG: 10,
    INFO:  20,
    WARN:  30,
    ERROR: 40,
    FATAL: 50,
    LOG:   9999
};

var severityLevel = 0;
var logPath = null;
var logFd = null;

exports.log = function(message) {
    writeLog('LOG', message);
};

exports.addDebug = function(message) {
    var severity = 'DEBUG';
    writeLog(severity, message);
};

exports.addInfo = function(message) {
    var severity = 'INFO';
    writeLog(severity, message);
};

exports.addWarn = function(message) {
    var severity = 'WARN';
    writeLog(severity, message);
};

exports.addError = function(message) {
    var severity = 'ERROR';
    writeLog(severity, message);
};

exports.addFatal = function(message) {
    var severity = 'FATAL';
    writeLog(severity, message);
};



exports.makeError = function(message) {
    return (message instanceof Error) ? message : Error(message);
};

exports.setLogLevel = function(newLevel) {
    if(typeof SEVERITY_LEVELS[newLevel] == 'undefined') {
        exports.addError(Error('Invalid log level specified: ' + newLevel));
    } else {
        severityLevel = SEVERITY_LEVELS[newLevel];
    }
};


exports.reopen = function(){
    getFd(nconf.get('log:path')).then(function(fd){
    	
        //ログファイルのオープンに成功した場合、
        //以前のログのファイルディスクリプタをクローズする。
        if(logFd != null) {
            fs.closeSync(logFd);
        }
        
        logFd = fd;
        writeLog('LOG', 'Log file reopened.');
    });
};


function writeLog(severity, message) {
    if(severityLevel > SEVERITY_LEVELS[severity]) {
        return;
    }
    co(function *(){
        if(!logFd) {
            logFd = yield getFd(nconf.get('log:path'));
        }

        var formattedMessage = formatMessage(severity, message);
        fs.write(logFd, formattedMessage + "\n", function(err, written, string){
            if(err){
                console.log(err);
            }
        });
    }).catch(function(err){
        throw makeError(err);
    });
//    fs.appendFile(logPath, formattedMessage, function(err){
//        throw Error(err);
//    });
}

function formatMessage(severity, message) {
    var now = new Date();
    var nowString = dateFormat(now, 'yyyy-mm-dd HH:MM:ss');

    if(message instanceof Error) {
        message = message.stack;
    } else if (message instanceof Object) {
        message = JSON.stringify(message);
    }
    return '[' + nowString + '][' + severity + ']' + message;
}

function getFd(path) {
    var promise = new Promise(function (resolve, reject){

        var ws = fs.createWriteStream(path, {flags:'a'});
        ws.on('open', function(fd){
            return resolve(fd);
        });
        ws.on('error', function(err){
            return reject(err);
        });

    }).catch(function(err){
        return Promise.reject(makeError(err));
    });
    return promise;
}
