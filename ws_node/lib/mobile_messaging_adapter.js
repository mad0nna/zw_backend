// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var logger = require('./logger');

module.exports = MobileMessagingAdapter;

/**
 * WebSocketによる通知を実行するオブジェクト
 * @returns {MobileMessagingAdapter}
 * @constructor
 */
function MobileMessagingAdapter() {
    if (!(this instanceof MobileMessagingAdapter)) return new MobileMessagingAdapter();

}


MobileMessagingAdapter.prototype.handlers = [];

MobileMessagingAdapter.prototype.init = function() {
    this.handlers = [];
};

MobileMessagingAdapter.prototype.addHandler = function(handler) {
    handler.setMobileMessagingAdapter(this);
    this.handlers.push(handler);
};


/**
 * 登録されている各ハンドラにイベントの発生を通知する
 * @param {string} type イベント名
 * @param {object} data イベントデータを含んだJSON
 * @param {string} origin イベントの発生元の名前
 */
MobileMessagingAdapter.prototype.triggerHandlerEvent = function(type, data, origin) {
    for(var i in this.handlers) {
        this.handlers[i].processHandlerEvent(type, data, origin);
    }
};


/**
 * WebSocketでの通知を行う。
 * @param {int} userId
 * @param {string} notificationType
 * @param {object} data
 */
MobileMessagingAdapter.prototype.send = function(userId, notificationType, data) {
    var notified = false;
    for(var i in this.handlers) {
        notified = this.handlers[i].send(userId, notificationType, data);
        if(notified) {
            break;
        }
    }
    if(!notified) {
        logger.addDebug('WEBSOCKET MESSAGE: ' + userId + '(NO SOCKET), message: ' + JSON.stringify(data));
    }
};


/**
 * WebSocketでのエラー通知を行う。
 * @param {int} userId
 * @param {object} data
 */
MobileMessagingAdapter.prototype.sendError = function(userId, error_id, data) {

    var error = {error_id: error_id, detail: data};
    this.send(userId, 'general_error', error);
};


