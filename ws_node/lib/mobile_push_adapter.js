// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var ServiceContainer = require('./service_container');

function MobilePushAdapter() {

}


/**
 * 指定したユーザーに対しplatformを意識してPUSH通知を実行する。
 * @param {object} context DBコネクションなどを含んだオブジェクト
 * @param {object} user userssのレコード
 * @param {object} data scenario_eventsのレコード
 * @return {Promise}
 */
MobilePushAdapter.prototype.publish = function(context, user, data) {
    var transport = this.createTransport(user['platform']);

    //このメソッドはPromiseを返します。
    return transport.publish(context, user, data);
};


/**
 * ユーザーのPlatformに応じてTransportを生成する。
 *
 * @param {integer} platform
 */
MobilePushAdapter.prototype.createTransport = function(platform) {
    var transport = null;
    switch(platform) {
        case 1:
            transport = ServiceContainer.get('push_transport_email');
            break;
        case 2:
        case 3:
            transport = ServiceContainer.get('push_transport_sns');
            break;
        case 4:
            transport = ServiceContainer.get('push_transport_sqs');
            break;
    }
    return transport;
};


module.exports = MobilePushAdapter;
