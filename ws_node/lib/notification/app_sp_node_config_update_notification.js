// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var logger = require('../logger');
var NodeRepository = require('../repository/node_repository');

module.exports = ApplicationSpecificNodeConfigUpdateNotification;

function ApplicationSpecificNodeConfigUpdateNotification() {
    if (!(this instanceof ApplicationSpecificNodeConfigUpdateNotification)) return new ApplicationSpecificNodeConfigUpdateNotification();
}

ApplicationSpecificNodeConfigUpdateNotification.prototype.connection = null;
ApplicationSpecificNodeConfigUpdateNotification.prototype.data = null;

ApplicationSpecificNodeConfigUpdateNotification.prototype.init = function(context, data) {
    this.connection = context['connection'];
    this.data = data;
};

ApplicationSpecificNodeConfigUpdateNotification.prototype.execute = function() {
    var self = this;
    return co(function *(){
        yield self.updateAppConfig(self.connection, self.data);
    }.bind(this)).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
};

ApplicationSpecificNodeConfigUpdateNotification.prototype.updateAppConfig = function(connection, data) {
    var promise = co(function *(){
        var nodeRepository = new NodeRepository(connection);
        var dirtyNodeId = yield nodeRepository.getIdByName(data['node_name']);
        var nodeId = dirtyNodeId['id'];
        var appName = data["app_name"];
        var config = data["configurations"];
        yield nodeRepository.saveAppSpecificNodeConfig(nodeId, appName, config);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};

