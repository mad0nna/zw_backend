// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var dateFormat = require('dateformat');
var transform = require('lodash.transform');
var uniq = require('lodash.uniq');
var db = require('../db/db');
var DeviceRepository = require('../repository/device_repository');
var DeviceDataRepository = require('../repository/device_data_repository');
var logger = require('../logger');
var NodeRepository = require('../repository/node_repository');
var UserRepository = require('../repository/user_repository');
var serviceContainer = require('../service_container');

module.exports = DeviceDataNotification;


function DeviceDataNotification() {
    if (!(this instanceof DeviceDataNotification)) return new DeviceDataNotification();
}

DeviceDataNotification.prototype.connection = null;
DeviceDataNotification.prototype.data = null;

DeviceDataNotification.prototype.init = function(context, data) {
    this.connection = context['connection'];
    this.data = data;
};

DeviceDataNotification.prototype.execute = function() {
    var self = this;
    var mobileMessagingAdapter = serviceContainer.get('mobile_messaging_adapter');
    var i = 0;
    return co(function *(){
        //デバイスデータを保存する。
        var deviceRepository = new DeviceRepository(self.connection);
        var deviceDataRepository = new DeviceDataRepository(self.connection);

        //node_name(manufacturer,product,serial_noを連結したもの), device_typeからdevice_idを特定する。
        var deviceType = deviceRepository.getDeviceTypeFromName(self.data['device_type']);

        //ゲートウェイの割当て直後など、登録されたGateway, Nodeがコミットされる前に
        //通知を受けるとNodeが見つからないため、1秒間隔で3回まで確認を行う。
        var device = null;
        device = yield deviceRepository.findByNodeName(self.data['node_name'], deviceType);
        if(!device) {
            return Promise.reject(Error('Failed to select device. node_name: ' + self.data['node_name'] + 'device_type: ' + self.data['device_type']));
        }

        //device_dataの保存
        var deviceData = self.data;
        deviceData['device_id'] = device['id'];
        yield deviceDataRepository.save(deviceData);

        //通知対象のユーザーの一覧を取得する
        var users = yield self.getTargetUserList(self.connection, self.data, device);

        var loginIdList = transform(users, function(result, item){
            result.push(item['login_id']);
        });
        var uniqueLoginIdList = uniq(loginIdList);

        //通知の実行
        var userId = 0;
        var data = {};
        for(i in uniqueLoginIdList) {
            data = {
                id: device['id'],
                value: String(self.data['value']),
                timestamp: self.data['timestamp'],
                unit: self.data['unit']
            };
            mobileMessagingAdapter.send(uniqueLoginIdList[i], 'device_data_update', data);
        }
    }.bind(this)).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
};


DeviceDataNotification.prototype.getTargetUserList = function(connection, data, device) {
    var self = this;
    var promise = co(function *(){
        //デバイスデータを保存する。
        var userRepository = new UserRepository(connection);
        var nodeRepository = new NodeRepository(connection);

        //デバイスに紐づくNodeの情報を取得する
        var node = yield nodeRepository.findByDeviceId(device['id']);
        if(!node) {
            return Promise.reject(Error('Failed to select node. device_id: ' + device['id']));
        }

        //nodeに紐付くユーザー(N件)を取得する
        var users = yield userRepository.findByNodeId(node['id']);

        //Nodeのオーナーを取得する
        var nodeOwner = yield userRepository.findByGatewayId(node['gateway_id']);
        if(!nodeOwner) {
            logger.addWarn(Error('Failed to find node owner. device_id: ' + device['id']));
            return;
        }

        users.push(nodeOwner);
        return users;
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};
