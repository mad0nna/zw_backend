// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');

module.exports = EmptyNotification;

function EmptyNotification() {
    if (!(this instanceof EmptyNotification)) return new EmptyNotification();
}

EmptyNotification.prototype.init = function(context, data) {
};

EmptyNotification.prototype.execute = function() {
    return co(function *(){}.bind(this));
};
