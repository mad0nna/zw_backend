// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var dateFormat = require('dateformat');
var DB = require('../db/db');
var DeviceRepository = require('../repository/device_repository');
var GatewayRepository = require('../repository/gateway_repository');
var logger = require('../logger');
var NodeRepository = require('../repository/node_repository');
var UserRepository = require('../repository/user_repository');
var serviceContainer = require('../service_container');

module.exports = GatewayFirmwareVersionChangeNotification;


function GatewayFirmwareVersionChangeNotification() {
    if (!(this instanceof GatewayFirmwareVersionChangeNotification)) return new GatewayFirmwareVersionChangeNotification();
}

GatewayFirmwareVersionChangeNotification.prototype.connection = null;
GatewayFirmwareVersionChangeNotification.prototype.data = null;

GatewayFirmwareVersionChangeNotification.prototype.init = function(context, data) {
    this.connection = context['connection'];
    this.data = data;
};

GatewayFirmwareVersionChangeNotification.prototype.execute = function() {
    var self = this;
    return co(function *(){

// この通知はユーザー通知はしない
//        var users = yield self.getTargetUserList(self.connection, self.data);

        yield self.updateFirmwareVersion(self.connection, self.data);
//        for(var i in users) {
//            yield self.sendMessage(self.connection, users[i]['login_id'], self.data);
//        }

    }.bind(this)).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
};


GatewayFirmwareVersionChangeNotification.prototype.updateFirmwareVersion = function(connection, data) {
    var promise = co(function *(){

        var gatewayRepository = new GatewayRepository(connection);

        //ゲートウェイの割当て直後など、登録されたGatewayがコミットされる前に
        //通知を受けるとゲートウェイが見つからないため、1秒間隔で5回まで確認を行う。
        var gateway = null;
        var loop = 0;
        gateway = yield gatewayRepository.findByName(data['gw_name']);
        if(!gateway) {
            return Promise.reject(Error('Gatewayが見つかりません。 gw_name: ' + data['gw_name']));
        }
        var gatewayId = gateway['id'];
        var fwVersion = data['fw_version'];

        //ノード情報を保存する
        yield gatewayRepository.updateFirmwareVersion(gatewayId, fwVersion);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};


GatewayFirmwareVersionChangeNotification.prototype.getTargetUserList = function(connection, data) {
    var promise = co(function *(){
        var gatewayRepository = new GatewayRepository(connection);
        var gateway = yield gatewayRepository.findByName(data['gw_name']);
        if(!gateway) {
            return Promise.reject(Error('Gatewayが見つかりません。 gw_name: ' + data['gw_name']));
        }

        var gatewayId = gateway['id'];
        var userRepository = new UserRepository(connection);

        //Gatewayのオーナーを取得する
        var owner = yield userRepository.findByGatewayId(gatewayId);
        if(!owner) {
            logger.addWarn(Error('Failed to find gateway owner. gateway_id: ' + gatewayId));
            return users;
        }
        var users = [];
        users.push(owner);
        return users;
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};


GatewayFirmwareVersionChangeNotification.prototype.sendMessage = function(connection, userId, data) {
    //TODO: グローバルにアクセス出来る場所から取得しない。何らかの形で外部から受け取る
    var mobileMessagingAdapter = serviceContainer.get('mobile_messaging_adapter');
    var self = this;
    var promise = co(function *(){

        var gatewayRepository = new GatewayRepository(connection);
        var gateway = yield gatewayRepository.findByName(data['gw_name']);
        if(!gateway) {
            return Promise.reject(Error('Gatewayが見つかりません。 gw_name: ' + data['gw_name']));
        }

        //モバイルへの通知用のオブジェクトを作成する。
        var message = {
            gateway_id: gateway['id'],
            fw_version: data['fw_version']
        };
        mobileMessagingAdapter.send(userId, 'gw_fw_version', message);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};
