// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var FirmwareVersionsRepository = require('../repository/firmware_versions_repository');
var logger = require('../logger');

module.exports = NewGatewayFirmwareNotification;

function NewGatewayFirmwareNotification() {
    if (!(this instanceof NewGatewayFirmwareNotification)) return new NewGatewayFirmwareNotification();
}

NewGatewayFirmwareNotification.prototype.connection = null;
NewGatewayFirmwareNotification.prototype.data = null;

NewGatewayFirmwareNotification.prototype.init = function(context, data) {
    this.connection = context['connection'];
    this.data = data;
};

NewGatewayFirmwareNotification.prototype.execute = function() {
    var self = this;
    return co(function *(){
        yield self.saveFirmwareVersion(self.connection, self.data);
    }.bind(this)).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
};

NewGatewayFirmwareNotification.prototype.saveFirmwareVersion = function(connection, data) {
    var promise = co(function *(){

        var fwVersionsRepository = new FirmwareVersionsRepository(connection);
        var fwVersion = data['fw_version'];
        yield fwVersionsRepository.save(fwVersion);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};
