// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var dateFormat = require('dateformat');
var DB = require('../db/db');
var DeviceRepository = require('../repository/device_repository');
var NodeRepository = require('../repository/node_repository');
var logger = require('../logger');
var UserRepository = require('../repository/user_repository');
var serviceContainer = require('../service_container');

module.exports = NodeConfigUpdateNotification;


function NodeConfigUpdateNotification() {
    if (!(this instanceof NodeConfigUpdateNotification)) return new NodeConfigUpdateNotification();
}

NodeConfigUpdateNotification.prototype.connection = null;
NodeConfigUpdateNotification.prototype.data = null;

NodeConfigUpdateNotification.prototype.init = function(context, data) {
    this.connection = context['connection'];
    this.data = data;
};

NodeConfigUpdateNotification.prototype.execute = function() {
    var self = this;
    var nodeId = self.data['node_id'];

    return co(function *(){
        //AMQPの通知に含まれるNodeの情報でDBを更新
        yield self.saveNodeConfig(self.connection, self.data);

        //通知対象ユーザーを特定し、通知を行う
        var users = yield self.getTargetUserList(self.connection, self.data);

        for(var i in users) {
            yield self.sendMessage(self.connection, users[i]['login_id'], self.data);
        }

    }.bind(this)).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
};


NodeConfigUpdateNotification.prototype.saveNodeConfig = function(connection, data) {
    var promise = co(function *(){
        
        
        if (typeof data['result_code'] != 'undefined') {
            // result_code が入っているものは処理しない
            return Promise.resolve(true);
        }

        var nodeRepository = new NodeRepository(connection);
        var config = yield nodeRepository.findConfig(data);
        if (!config) {
            // pendingになっている事前の通知がなければ何もしないで処理を抜ける
            return Promise.resolve(true);
        }
        // update node config
        yield nodeRepository.updateConfig(config['id'], data);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};

NodeConfigUpdateNotification.prototype.getTargetUserList = function(connection, data) {
    var promise = co(function *(){
        var nodeRepository = new NodeRepository(connection);
        var node = yield nodeRepository.find(data['node_id']);
        if(!node) {
            return Promise.reject(Error('Nodeが見つかりません。 node_id: ' + data['node_id']));;
        }

        var gatewayId = node['gateway_id'];
        var userRepository = new UserRepository(connection);

        //Node共有しているユーザーを取得する
        var users = yield userRepository.findByNodeId(data['node_id']);

        //Nodeのオーナーを取得する
        var nodeOwner = yield userRepository.findByGatewayId(gatewayId);
        if(!nodeOwner) {
            logger.addWarn(Error('Failed to find node owner. node_id: ' + data['node_id']));
            return users;
        }
        users.push(nodeOwner);

        return users;
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
}


NodeConfigUpdateNotification.prototype.sendMessage = function(connection, userId, data) {
    var mobileMessagingAdapter = serviceContainer.get('mobile_messaging_adapter');
    var self = this;
    var promise = co(function *(){
        var nodeRepository = new NodeRepository(connection);
        var node = yield nodeRepository.find(data['node_id']);
        if(!node) {
            return Promise.reject(Error('Nodeが見つかりません。 node_id: ' + data['node_id']));
        }

        //モバイルへの通知用のオブジェクトを作成する。
        if (data['config_type'] == 'zwave') {
            var message = {
                node_id: data['node_id'],
                config_type: data['config_type'],
                param_type: data['param_type'],
                param_size: data['param_size'],
                param_id: data['param_id'],
                value: data['value']
            };
        } else {
            var message = {
                node_id: data['node_id'],
                config_type: data['config_type'],
                param_type: data['param_type'],
                param_id: data['param_id'],
                value: data['value']
            };
        }

        mobileMessagingAdapter.send(userId, 'node_config_update', message);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};
