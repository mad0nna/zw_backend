// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var dateFormat = require('dateformat');
var DB = require('../db/db');
var DeviceRepository = require('../repository/device_repository');
var GatewayRepository = require('../repository/gateway_repository');
var ScenarioRepository = require('../repository/scenario_repository');
var logger = require('../logger');
var NodeRepository = require('../repository/node_repository');
var UserRepository = require('../repository/user_repository');
var serviceContainer = require('../service_container');
var request = require('request-promise');

module.exports = NodeExclusionNotification;


function NodeExclusionNotification() {
    if (!(this instanceof NodeExclusionNotification)) return new NodeExclusionNotification();
}

NodeExclusionNotification.prototype.connection = null;
NodeExclusionNotification.prototype.data = null;

NodeExclusionNotification.prototype.init = function(context, data) {
    this.connection = context['connection'];
    this.data = data;
};

NodeExclusionNotification.prototype.execute = function() {
    var self = this;
    var nodeId = self.data['node_id'];
    return co(function *(){

        var users = yield self.getTargetUserList(self.connection, self.data);

        //GatewayとNodeの接続を解除する
        yield self.excludeNode(self.connection, self.data);

        for(var i in users) {
            yield self.sendMessage(self.connection, users[i]['login_id'], self.data);
        }

    }.bind(this)).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
};


NodeExclusionNotification.prototype.excludeNode = function(connection, data) {
    var self = this;
    var promise = co(function *(){

        var gatewayRepository = new GatewayRepository(connection);
        var gateway = yield gatewayRepository.findByName(data['gw_name']);
        if(!gateway) {
            return Promise.reject(Error('Gatewayが見つかりません。 gw_name: ' + data['gw_name']));
        }
        var gatewayId = gateway['id'];
        var nodeId = data['node_id'];
        
        var nodeRepository = new NodeRepository(connection);
        // ノードの存在チェック
        var node = yield nodeRepository.find(nodeId);
        if(!node) {
            return Promise.reject(Error('Nodeが見つかりません。 node_id: ' + nodeId));
        }
        // ノードが属しているゲートウェイと、gw_name で指定されたゲートウェイが一致の確認
        if(node['gateway_id'] != gatewayId) {
            return Promise.reject(Error('ノードのgateway_idとgw_nameで指定されたGatewayが一致しません。'
              + 'nodes.gateway_id=' + node['gateway_id'] + ', gw_name->gateway_id=' + gatewayId));
        }

        //ノード情報を保存する
        yield nodeRepository.clearGatewayAssociation(nodeId);

        // 自動登録シナリオの削除
        yield self.deregAutoScenarios(connection, data);

        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};

NodeExclusionNotification.prototype.deregAutoScenarios = function(connection, data) {

    var nodeId = data['node_id'];

    var promise = co(function *(){
        //ノードが関連しているシナリオのうち、自動登録のシナリオを登録解除する
        var deviceRepository = new DeviceRepository(connection);
        var devices = yield deviceRepository.findByNodeId(nodeId);
        for(var i in devices) {
            var deviceId = devices[i]['id'];
            var scenarioRepository = new ScenarioRepository(connection);
            var scenarios = yield scenarioRepository.findScenarioByDeviceId(deviceId);
            for (var i in scenarios) {
                if (scenarios[i]['auto_config'] != 0) {
                    //自動登録したシナリオの登録を解除する
                    var options = {
                        uri: 'http://localhost/api/v3/scenarios/' + scenarios[i]['id'] + '/autoconfig',
                        method: 'DELETE',
                        json: true
                    };
                    
                    yield request(options, function(error, response, body) {
                        if (error || response.statusCode != 200) {
                            return Promise.reject(Error('シナリオの自動登録解除に失敗しました。'));
                        }
                    });
                }
            }
        }
        return Promise.resolve(true);
    });
    return promise;
};

NodeExclusionNotification.prototype.getTargetUserList = function(connection, data) {
    var promise = co(function *(){
        var gatewayRepository = new GatewayRepository(connection);
        var gateway = yield gatewayRepository.findByName(data['gw_name']);
        if(!gateway) {
            return Promise.reject(Error('Gatewayが見つかりません。 gw_name: ' + data['gw_name']));;
        }

        var gatewayId = gateway['id'];
        var userRepository = new UserRepository(connection);

        //Node共有しているユーザーを取得する
        var users = yield userRepository.findByNodeId(data['node_id']);

        //Nodeのオーナーを取得する
        var nodeOwner = yield userRepository.findByGatewayId(gatewayId);
        if(!nodeOwner) {
            logger.addWarn(Error('Failed to find node owner. node_id: ' + data['node_id']));
            return users;
        }
        users.push(nodeOwner);
        return users;
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
}


NodeExclusionNotification.prototype.sendMessage = function(connection, userId, data) {
    //TODO: グローバルにアクセス出来る場所から取得しない。何らかの形で外部から受け取る
    var mobileMessagingAdapter = serviceContainer.get('mobile_messaging_adapter');
    var self = this;
    var promise = co(function *(){
        //モバイルへの通知用のオブジェクトを作成する。
        var message = {
            id: data['node_id']
        };
        mobileMessagingAdapter.send(userId, 'node_exclusion', message);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};
