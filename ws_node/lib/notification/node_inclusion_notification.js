// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var constants = require('../constants');
var co = require('co');
var dateFormat = require('dateformat');
var DB = require('../db/db');
var DeviceRepository = require('../repository/device_repository');
var GatewayRepository = require('../repository/gateway_repository');
var logger = require('../logger');
var NodeRepository = require('../repository/node_repository');
var UserRepository = require('../repository/user_repository');
var serviceContainer = require('../service_container');
var request = require('request-promise');

module.exports = NodeInclusionNotification;


function NodeInclusionNotification() {
    if (!(this instanceof NodeInclusionNotification)) return new NodeInclusionNotification();
}

NodeInclusionNotification.prototype.connection = null;
NodeInclusionNotification.prototype.data = null;

NodeInclusionNotification.prototype.init = function(context, data) {
    this.connection = context['connection'];
    this.data = data;
};

NodeInclusionNotification.prototype.execute = function() {
    var self = this;
    var nodeId = self.data['node_id'];
    var promise = co(function *(){

        if (self.data['result_code'] == undefined || self.data['result_code'] == 200) {

            var errors = yield self.checkNotification(self.connection, self.data);
            if (0 < errors.length) {
                yield self.sendGeneralErrorNotification(self.connection, self.data['gw_name']);
                return Promise.reject(Error(errors[0]));
            }

            //AMQPの通知に含まれるNodeの情報でDBを更新
            yield self.saveNodeInfo(self.connection, self.data);

            var users = yield self.getTargetUserList(self.connection, self.data);

            var i = 0;
            for(i in users) {
                yield self.sendMessage(self.connection, users[i]['login_id'], self.data);
            }
        } else {
            yield self.sendGeneralErrorNotification(self.connection, self.data['gw_name']);
            return Promise.reject(Error('Node Inclusion が失敗しました。 result_code: ' + self.data['result_code']));
        }

    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};

NodeInclusionNotification.prototype.sendGeneralErrorNotification = function(connection, gw_name) {
    var self = this;
    var promise = co(function *(){
        if (gw_name == undefined) {
            return Promise.reject(Error('Node Inclusion Notification でエラーが発生しましたが、gw_name が指定されていないため通知されませんでした。'));
        }
        var gateway = yield self.findGatewayFromName(self.connection, gw_name);
        if (gateway == null) {
            return Promise.reject(Error('Node Inclusion Notification でエラーが発生しましたが、対象の Gateway が見つからなかったため通知されませんでした。 gw_name: ' + gw_name));
        }

        var mobileMessagingAdapter = serviceContainer.get('mobile_messaging_adapter');
        var userRepository = new UserRepository(self.connection);
        var user = yield userRepository.find(gateway['user_id']);
        if (!user) {
            return Promise.reject(Error('Node Inclusion Notification でエラーが発生しましたが、対象の User が見つからなかったため通知されませんでした。 id: ' + gateway['user_id']));
        }
        mobileMessagingAdapter.sendError(user['login_id'], constants.GENERAL_ERROR_NODE_INCLUSION, self.data['gw_name']);
    });
    return promise;
};

NodeInclusionNotification.prototype.findGatewayFromName = function(connection, gw_name) {

    var self = this;
    var promise = co(function *(){

        if (gw_name == null || gw_name.length == 0) {
            return null;
        }

        var gatewayRepository = new GatewayRepository(connection);

        //ゲートウェイの割当て直後など、登録されたGatewayがコミットされる前に
        //通知を受けるとゲートウェイが見つからないため、1秒間隔で5回まで確認を行う。
        var gateway = null;
        gateway = yield gatewayRepository.findByName(gw_name);
        if(!gateway) {
            return null;
        }
        return gateway;
    });
    return promise;
};

NodeInclusionNotification.prototype.checkNotification = function(connection, data) {
    var self = this;
    var promise = co(function *(){
        
        var errors = [];

        var nodeId = data['node_id'];

        // ゲートウェイの情報を取得する
        var gateway = yield self.findGatewayFromName(connection, data['gw_name']);
        if (gateway == null) {
            errors.push('Gatewayが見つかりません。 gw_name: ' + data['gw_name']);
        }

        if (0 < errors.length) {
            return errors;
        }

        var nodeRepository = new NodeRepository(connection);
        var node = yield nodeRepository.find(nodeId);

        // Include 済みのノードへの Node Inclusion Notification はエラーとする。
        if (node['reason'] == 'node_incl') {
            errors.push('Include 済みのノードに対する Node Inclusion Notification です。 node_id: ' + nodeId);
        }
        return errors;
    });
    return promise;
};

NodeInclusionNotification.prototype.saveNodeInfo = function(connection, data) {
    var self = this;
    var promise = co(function *(){

        var nodeId = data['node_id'];

        // ゲートウェイの情報を取得する
        var gateway = yield self.findGatewayFromName(connection, data['gw_name']);
        if (gateway == null) {
            return Promise.reject(Error('Gatewayが見つかりません。 gw_name: ' + data['gw_name']));
        }

        var nodeRepository = new NodeRepository(connection);

        //既にNodeが登録されているか確認する。登録されている場合は所有者が同じか確認する。
        var currentNodeInfo = yield nodeRepository.find(nodeId);

        // Include 済みのノードへの Node Inclusion Notification はエラーとする。
        if (currentNodeInfo['reason'] == 'node_incl') {
            return Promise.reject(Error('Include 済みのノードに対する Node Inclusion Notification です。 node_id: ' + nodeId));
        }

        var gatewayId = gateway['id'];
        var newUserId = gateway['user_id'];
        var oldUserId = currentNodeInfo['prev_user_id'] || 0;
        var isSameOwner = (newUserId == oldUserId);

        var joinKeySet = data['dust_ip'] != undefined && data['dust_ip']['join_key_set'] != undefined ? data['dust_ip']['join_key_set'] : null;

        var node = {
            gateway_id: gatewayId,
            manufacturer: data['manufacturer'],
            product: data['product'],
            serial_no: data['serial_no'],
            failed_cmd: data['failed_cmd'],
            yet_another_state: data['state'],
            node_type: data['node_type'],
            reason: 'node_incl',
            join_key_set: joinKeySet,
        };

        //ノード情報を保存する
        yield nodeRepository.save(nodeId, node);
        if(!isSameOwner) {
            //以前と所有者が変わっている場合は古い情報を削除する
            yield nodeRepository.removeAllLabels(nodeId);
            yield nodeRepository.clearNodeSharing(nodeId);
        }

        //デバイス情報を保存
        var deviceRepository = new DeviceRepository(connection);
        var devices = [];
        for(var i in data['devices']) {
            devices.push({
                id: data['devices'][i]['device_id'],
                type: deviceRepository.getDeviceTypeFromName(data['devices'][i]['device_type'])
            });

            //以前と所有者が変わっている場合はDeviceの古い情報も削除する
            if(!isSameOwner) {
                yield deviceRepository.removeAllLabels(data['devices'][i]['device_id']);
                yield deviceRepository.removeAllData(data['devices'][i]['device_id']);
            }
        }
        yield deviceRepository.replaceDevicesByNodeId(nodeId, devices);

        //アクション情報を保存
        //アクション情報は配列の要素名が同じため、そのまま連携する
        yield nodeRepository.replaceActions(nodeId, data['actions']);
        
        //電池デバイスについて電池残量低下シナリオの自動登録
        yield self.registLowBatteryScenario(node, devices, newUserId);

        // save app field to database if any
        for(var i in data['app']) {
            var appName = data['app'][i]['app_name'];
            var config = data['app'][i]['configurations'];
            yield nodeRepository.saveAppSpecificNodeConfig(nodeId, appName, config);
        }

        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};


NodeInclusionNotification.prototype.getTargetUserList = function(connection, data) {
    var promise = co(function *(){
        var gatewayRepository = new GatewayRepository(connection);
        var gateway = yield gatewayRepository.findByName(data['gw_name']);
        if(!gateway) {
            return Promise.reject(Error('Gatewayが見つかりません。 gw_name: ' + data['gw_name']));;
        }

        var gatewayId = gateway['id'];
        var userRepository = new UserRepository(connection);

        //Node共有しているユーザーを取得する
        var users = yield userRepository.findByNodeId(data['node_id']);

        //Nodeのオーナーを取得する
        var nodeOwner = yield userRepository.findByGatewayId(gatewayId);
        if(!nodeOwner) {
            logger.addWarn(Error('Failed to find node owner. node_id: ' + data['node_id']));
            return users;
        }
        users.push(nodeOwner);
        return users;
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};


NodeInclusionNotification.prototype.sendMessage = function(connection, userId, data) {
    var mobileMessagingAdapter = serviceContainer.get('mobile_messaging_adapter');
    var self = this;
    var promise = co(function *(){
        var gatewayRepository = new GatewayRepository(connection);
        var nodeRepository = new NodeRepository(connection);
        var gateway = yield gatewayRepository.findByName(data['gw_name']);
        if(!gateway) {
            return Promise.reject(Error('Gatewayが見つかりません。 gw_name: ' + data['gw_name']));
        }
        var gatewayId = gateway['id'];

        var deviceUrls = [];
        for(var i in data['devices']) {
            deviceUrls.push('/api/v3/devices/' + data['devices'][i]['device_id']);
        }

        var node = yield nodeRepository.find(data['node_id']);
        var labels = yield nodeRepository.findLabels(data['node_id']);
        var sharedUsers = yield nodeRepository.findSharedUsers(data['node_id']);

        var sharedUserUrls = [];
        for(var i in sharedUsers) {
            sharedUserUrls.push('/api/v3/users/' + sharedUsers[i]['login_id']);
        }

        //モバイルへの通知用のオブジェクトを作成する。
        var message = {
            id: data['node_id'],
            labels: labels,
            gateway: '/api/v3/gateways/' + gatewayId,
            protocol: 'zwave', //現時点ではzwaveのみ
            sharing_level: nodeRepository.getSharingTypeNameFromType(node['sharing_level']),
            devices: deviceUrls,
            shared_with: sharedUserUrls,
            location: {
                type: '',//,data['location']['type'],
                name: ''//,data['location']['name']
            },
            zwave: {
                devices: deviceUrls,
                manufacturer: data['manufacturer'],
                product: data['product'],
                serial_no: data['serial_no'],
                failed_cmd: data['failed_cmd'],
            }
        };
        mobileMessagingAdapter.send(userId, 'node_inclusion', message);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};


NodeInclusionNotification.prototype.registLowBatteryScenario = function(node, devices, userId) {
    var promise = co(function *() {
        // 電池残量の低下通知シナリオの自動登録
       var targetDevices = [
            // Vision       : ZD 2201JP-5(1)
        	['0109', '201f1f10'],	
            // Vision       : ZD 2201JP-5(2)
        	['0109', '201f1f20'],	
            // Vision       : ZP 3111JP-5
        	['0109', '20212101'],	
            // Philio       : PST02-A
        	['013c', '0002000c'],	
            // Sharp(Zworks): DN3G6JA062
        	['024d', '47610001'],	
            // Danalock     : ZD-DN150
        	['010e', '00080002'],	
            // Sharp        : DN3G6JA069
        	['010b', '47610069'],	
            // ???          : ??? (Smoke sensor)
        	['0123', '0a010001'],	
        ];
        for(var targetDevice in targetDevices) {
        	if(targetDevices[targetDevice][0] === node['manufacturer']
        		&& targetDevices[targetDevice][1] === node['product']) {
                // バッテリーを探す
                for(var device in devices) {
                    if (devices[device]['type'] == 7) {
                        // シナリオを登録する
                        var scenario = '{"scenario_type":"simple",'
                                        + '"labels":['
                                           + '"電池残量低下の通知（自動登録シナリオ）"'
                                           + '],'
                                        + '"condition_operator":"OR",'
                                        + '"conditions":['
                                            + '{'
                                            + '"lhs":"' + devices[device]['id'] + '",'
                                            + '"operator":"==",'
                                            + '"rhs":"255",'
                                            + '"unit":"%"'
                                            + '},'
                                            + '{'
                                            + '"lhs":"' + devices[device]['id'] + '",'
                                            + '"operator":"==",'
                                            + '"rhs":"10",'
                                            + '"unit":"%"'
                                            + '}'
                                            + '],'
                                        + '"trigger_after":0,'
                                        + '"enabled":true,'
                                        + '"actions":[{'
                                            + '"type":"send_message",'
                                            + '"content":"電池交換が必要なセンサーデバイスがあります"'
                                            + '}],'
                                        + '"user_id":' + userId
                                        + '}';
                        var headers = {
                            'Content-Type':'application/json'
                        };
                        var options = {
                            uri: 'http://localhost/api/v3/scenarios/autoconfig',
                            method: 'POST',
                            headers: headers,
                            form: scenario,
                            json: true
                        };
                        
                        yield request(options, function(error, response, body) {
                            if (error || response.statusCode != 200) {
                                return Promise.reject(Error('シナリオの自動登録に失敗しました。'));
                            }
                        });
                    }
                }
        	}
        }
    });
    return promise;
};
