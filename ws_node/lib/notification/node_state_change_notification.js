// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var logger = require('../logger');
var NodeRepository = require('../repository/node_repository');
var UserRepository = require('../repository/user_repository');
var serviceContainer = require('../service_container');

module.exports = NodeStateChangeNotification;

function NodeStateChangeNotification() {
    if (!(this instanceof NodeStateChangeNotification)) return new NodeStateChangeNotification();
}

NodeStateChangeNotification.prototype.connection = null;
NodeStateChangeNotification.prototype.data = null;

NodeStateChangeNotification.prototype.init = function(context, data) {
    this.connection = context['connection'];
    this.data = data;
};

NodeStateChangeNotification.prototype.execute = function() {
    var self = this;
    return co(function *(){
        var users = yield self.getTargetUserList(self.connection, self.data);
        yield self.changeNodeState(self.connection, self.data);

        for(var i in users) {
            yield self.sendMessage(self.connection, users[i]['login_id'], self.data);
        }

    }.bind(this)).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
};

NodeStateChangeNotification.prototype.changeNodeState = function(connection, data) {
    var promise = co(function *(){
        var nodeRepository = new NodeRepository(connection);
        var nodeId = data['node_id'];
        var state = data['state'];
        yield nodeRepository.changeNodeState(nodeId, state);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};

NodeStateChangeNotification.prototype.getTargetUserList = function(connection, data) {
    var promise = co(function *(){
        var nodeRepository = new NodeRepository(connection);
        var node = yield nodeRepository.find(data['node_id']);
        var gatewayId = node['gateway_id'];

        //Node共有しているユーザーを取得する
        var userRepository = new UserRepository(connection);
        var users = yield userRepository.findByNodeId(data['node_id']);

        //Nodeのオーナーを取得する
        var nodeOwner = yield userRepository.findByGatewayId(gatewayId);
        if(!nodeOwner) {
            logger.addWarn(Error('Failed to find node owner. node_id: ' + data['node_id']));
            return users;
        }
        users.push(nodeOwner);
        
        return users;
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};

NodeStateChangeNotification.prototype.sendMessage = function(connection, userId, data) {
    //TODO: グローバルにアクセス出来る場所から取得しない。何らかの形で外部から受け取る
    var mobileMessagingAdapter = serviceContainer.get('mobile_messaging_adapter');
    var self = this;
    var promise = co(function *(){
        //モバイルへの通知用のオブジェクトを作成する。
        var message = {
            id: data['node_id'],
            state: data['state'],
        };
        mobileMessagingAdapter.send(userId, 'node_state_change', message);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};
