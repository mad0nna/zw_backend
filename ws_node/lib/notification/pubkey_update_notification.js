// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var logger = require('../logger');
var GatewayRepository = require('../repository/gateway_repository');

module.exports = PubkeyUpdateNotification;

function PubkeyUpdateNotification() {
    if (!(this instanceof PubkeyUpdateNotification)) return new PubkeyUpdateNotification();
}

PubkeyUpdateNotification.prototype.connection = null;
PubkeyUpdateNotification.prototype.data = null;

PubkeyUpdateNotification.prototype.init = function(context, data) {
    this.connection = context['connection'];
    this.data = data;
};

PubkeyUpdateNotification.prototype.execute = function() {
    var self = this;
    return co(function *(){
        yield self.updatePublicKey(self.connection, self.data);
    }.bind(this)).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
};

PubkeyUpdateNotification.prototype.updatePublicKey = function(connection, data) {
    var promise = co(function *(){
        var gatewayRepository = new GatewayRepository(connection);
        var gwName = data['gw_name'];
        var publicKey = data['public_key'];
        yield gatewayRepository.updatePublicKey(gwName, publicKey);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};
