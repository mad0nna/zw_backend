// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var dateFormat = require('dateformat');
var DB = require('../db/db');
var ScenarioRepository = require('../repository/scenario_repository');
var logger = require('../logger');
var UserRepository = require('../repository/user_repository');
var serviceContainer = require('../service_container');
var DeviceRepository = require('../repository/device_repository');

module.exports = ScenarioEventNotification;


function ScenarioEventNotification() {
    if (!(this instanceof ScenarioEventNotification)) return new ScenarioEventNotification();
}

ScenarioEventNotification.prototype.context = null;
ScenarioEventNotification.prototype.connection = null;
ScenarioEventNotification.prototype.data = null;
ScenarioEventNotification.prototype.scenarioRepository = null;
ScenarioEventNotification.prototype.userRepository = null;
ScenarioEventNotification.prototype.deivceRepository = null;

ScenarioEventNotification.prototype.init = function(context, data) {
    this.context = context;
    this.connection = context['connection'];
    this.data = data;
    this.scenarioRepository = new ScenarioRepository(this.connection);
    this.userRepository = new UserRepository(this.connection);
    this.deviceRepository = new DeviceRepository(this.connection);
};

ScenarioEventNotification.prototype.execute = function() {
    var self = this;
    return co(function *(){

        //
        yield self.saveEvent(self.data);

        var users = yield self.getTargetUserList(self.data);

        //対象ユーザーに通知を行う
        for(var i in users) {
            yield self.sendMessage(users[i], self.data);
        }

    }.bind(this)).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
};


ScenarioEventNotification.prototype.saveEvent = function(data) {
    var self = this;
    var promise = co(function *(){

        var scenario = yield self.scenarioRepository.find(data['scenario_id']);
        if(!scenario) {
            return Promise.reject(Error('Scenarioが見つかりません。 scenario_id: ' + data['scenario_id']));
        }
        var scenarioId = scenario['id'];
        var event = {
            message: data['content']
        };

        //ノード情報を保存する
        yield self.scenarioRepository.registerEvent(scenarioId, event);
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};


ScenarioEventNotification.prototype.getTargetUserList = function(data) {
    var self = this;
    var promise = co(function *(){
        var user = yield self.userRepository.findByScenarioId(data['scenario_id']);
        if(!user) {
            return Promise.reject(Error('Userが見つかりません。 scenario_id: ' + data['scenario_id']));
        }

        var users = [];
        var appendUser = true;
        var devices = yield self.scenarioRepository.findDevices(data['scenario_id']);
        for (var i in devices) {
          var isGrantedUser = yield self.deviceRepository.isGrantedUser(devices[i], user['id'], false);
          if (!isGrantedUser) {
            logger.addDebug('Scenario Event: Access denied. Device ID = ' + devices[i] + ', User ID = ' + user['id']);
            appendUser = false;
            break;
          }
        }
        if (appendUser) {
        users.push(user);
        }
        return users;
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};


ScenarioEventNotification.prototype.sendMessage = function(user, data) {
    var mobileMessagingAdapter = serviceContainer.get('mobile_messaging_adapter');
    var mobilePushAdapter = serviceContainer.get('mobile_push_adapter');
    var self = this;
    var promise = co(function *(){
        //モバイルへの通知用のオブジェクトを作成する。
        var currentTime = new Date();
        var message = {
            id: data['scenario_id'],
            message: data['content'],
            timestamp: +currentTime  //数値にキャストしてmilli-secondに変換する。
        };
        mobileMessagingAdapter.send(user['login_id'], 'scenario_message', message);

        //モバイルトークンに対応するユーザーは複数存在する可能性があるため、
        //トークンに対応するユーザーの中でログイン日時が最新のユーザーを取得する。
        //このユーザーが最新でない場合は通知しない。
        //（アプリでUser-Aがログインして一度ログアウト、その後User-Bがログインするケースなどが考えられる）
        if(user['token'] != '') {
            var tokenHolder = yield self.userRepository.findUserByToken(user['token']);
            if(tokenHolder['id'] != user['id']) {
                logger.addDebug('token:' + user['token'] + ' に対応するユーザーはid:' + tokenHolder['id'] + ' のため、id:' + user['id'] + 'へのPUSH通知はスキップされました。');
                return Promise.resolve(true);
            }
        }

        yield mobilePushAdapter.publish(self.context, user, data);

        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};
