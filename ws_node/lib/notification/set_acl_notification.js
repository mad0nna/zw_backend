// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var logger = require('../logger');
var GatewayRepository = require('../repository/gateway_repository');

module.exports = SetAclNotification;

function SetAclNotification() {
    if (!(this instanceof SetAclNotification)) return new SetAclNotification();
}

SetAclNotification.prototype.connection = null;
SetAclNotification.prototype.data = null;

SetAclNotification.prototype.init = function(context, data) {
    this.connection = context['connection'];
    this.data = data;
};

SetAclNotification.prototype.execute = function() {
    var self = this;
    return co(function *(){
        yield self.updateDustIp(self.connection, self.data);
    }.bind(this)).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
};

SetAclNotification.prototype.updateDustIp = function(connection, data) {
    var promise = co(function *(){
        var gatewayRepository = new GatewayRepository(connection);
        var gwName = data['gw_name'];

        if (data['result_code'] == 200) {
            var networkId = data['network_id'];
            yield gatewayRepository.updateDustIp(gwName, networkId);
        }

        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};
