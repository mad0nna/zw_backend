// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var constants = require('../constants');
var DB = require('../db/db');
var mysql = require('mysql');
var dateFormat = require('dateformat');


module.exports = DeviceDataRepository;


function DeviceDataRepository(connection) {
    if(!(this instanceof DeviceDataRepository)) {
        return new DeviceDataRepository(connection);
    }
    this.connection = connection;
}




DeviceDataRepository.prototype.connection = null;


DeviceDataRepository.prototype.save = function(data) {

    var sql = "INSERT INTO device_data SET "
            + " device_id=?, "
            + " time_stamp=?, "
            + " value=?, "
            + " unit=?";

    var formattedSql = mysql.format(sql, [
        data['device_id'],
        dateFormat(new Date(data['timestamp']), 'yyyy-mm-dd HH:MM:ss'),
        data['value'],
        data['unit']
    ]);

    return DB.query(this.connection, formattedSql).then(function(result){
        return result;
    }).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        Promise.reject(Error(err));
    });
};


DeviceDataRepository.prototype.findByDeviceId = function(deviceId) {

    var sharingLevel = constants.NODE_SHARING_LEVEL_USERS;
    var sql = "SELECT users.id, users.login_id, users.name, users.email, "
        + "           users.platform, users.token, users.gateway_id, "
        + "           users.created, users.updated "
        + "  FROM users "
        + " INNER JOIN nodes_users ON (nodes_users.user_id=users.id) "
        + " INNER JOIN nodes ON (nodes.id=nodes_users.node_id) "
        + " INNER JOIN devices ON (devices.node_id=nodes.id) "
        + " WHERE devices.id=? AND nodes.sharing_level=?";

    var formattedSql = mysql.format(sql, [deviceId, sharingLevel]);
    return DB.query(this.connection, formattedSql);
};

DeviceDataRepository.prototype.findCurrentValue = function(deviceId) {
    var sql = "SELECT value FROM device_data WHERE device_id=? ORDER BY time_stamp DESC LIMIT 1";

    var formattedSql = mysql.format(sql, deviceId);
    return DB.query(this.connection, formattedSql);
};
