// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var constants = require('../constants');
var DB = require('../db/db');
var mysql = require('mysql');
var co = require('co');
var dateFormat = require('dateformat');

module.exports = DeviceRepository;


function DeviceRepository(connection) {
    if(!(this instanceof DeviceRepository)) {
        return new DeviceRepository(connection);
    }
    this.connection = connection;
}




DeviceRepository.prototype.connection = null;


DeviceRepository.prototype.findByNodeName = function(nodeName, deviceType) {
    var splitNodeName = nodeName.split('_');
    var manufacturer = splitNodeName[0] || '';
    var product = splitNodeName[1] || '';
    var serialNo = splitNodeName[2] || '';

    var sql = "SELECT devices.id, devices.type "
            + "  FROM devices "
            + " INNER JOIN nodes ON (nodes.id=devices.node_id) "
            + " WHERE nodes.manufacturer=? AND nodes.product=? AND serial_no=? AND devices.type=? "
    ;

    var formattedSql = mysql.format(sql, [
        manufacturer,
        product,
        serialNo,
        deviceType
    ]);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        throw Error(err);
    });
};

DeviceRepository.prototype.findByTypes = function(types) {
    var typeList = types.join(',');
    var sql = "SELECT devices.id, devices.type "
            + "  FROM devices "
            + " INNER JOIN nodes ON (nodes.id=devices.node_id) "
            + " WHERE devices.type IN (" + typeList + ") AND nodes.gateway_id != 0"
    ;
    return DB.query(this.connection, sql);
};

DeviceRepository.prototype.registerDevice = function(device) {
    var sql = "INSERT INTO devices SET "
        + " id=?, "
        + " node_id=?, "
        + " type=?, "
        + " created=? "
        + " ON DUPLICATE KEY UPDATE "
        + " node_id=?, "
        + " type=?, "
        + " created=? "
        ;

    var formattedSql = mysql.format(sql, [
        device['id'],
        device['node_id'],
        device['type'],
        dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss'),
        device['node_id'],
        device['type'],
        dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss')
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = (err instanceof Error) ? err : Error(err);
        return Promise.reject(err);
    });
};

DeviceRepository.prototype.replaceDevicesByNodeId = function(nodeId, devices) {
    var self = this;

    return co(function *(){
        yield self.removeDevicesByNodeId(nodeId);

        for(var i in devices) {
            devices[i]['node_id'] = nodeId;
            yield self.registerDevice(devices[i]);
        }
    }).catch(function(err){
        err = (err instanceof Error) ? err : Error(err);
        return Promise.reject(err);
    });
};


DeviceRepository.prototype.removeDevicesByNodeId = function(nodeId) {
    var sql = "DELETE FROM devices WHERE node_id=? ";

    var formattedSql = mysql.format(sql, [
        nodeId
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = (err instanceof Error) ? err : Error(err);
        return Promise.reject(err);
    });
};


DeviceRepository.prototype.removeAllLabels = function(deviceId) {
    var sql = "DELETE FROM labels WHERE type=3 AND entity_id=?";

    var formattedSql = mysql.format(sql, [
        deviceId
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        return Promise.reject(Error(err));
    });
};


DeviceRepository.prototype.removeAllData = function(deviceId) {
    var sql = "DELETE FROM device_data WHERE device_id=?";

    var formattedSql = mysql.format(sql, [
        deviceId
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        return Promise.reject(Error(err));
    });
};


DeviceRepository.prototype.getDeviceTypeMapping = function(){
    return {
        1: 'temperature',
        2: 'humidity',
        3: 'motion',
        4: 'open_close',
        5: 'lock',
        6: 'cover',
        7: 'battery',
        8: 'luminance',
        9: 'call_button',
        10: 'smart_lock',
        11: 'power',
        12: 'acc_energy',
        13: 'power_switch',
        14: 'apparent_energy',
        15: 'acc_gas',
        16: 'acc_water',
        17: 'heart_rate',
        18: 'breathing_rate',
        19: 'body_motion',
        20: 'smoke',
        21: 'person_state',
        22: 'distance',
        23: 'parkers_car',
        24: 'parkers_led',
        25: 'parkers_mode',
        26: 'parkers_battery',
    };
};

DeviceRepository.prototype.getDeviceTypeNameFromType = function(type){
    var mapping = this.getDeviceTypeMapping();
    return mapping[type];
};

DeviceRepository.prototype.getDeviceTypeFromName = function(name){
    var mapping = this.getDeviceTypeMapping();
    for(var i in mapping) {
        if(mapping[i] == name) {
            return i;
        }
    }
    return -1;
};

DeviceRepository.prototype.findByNodeId = function(nodeId) {

    var sql = "SELECT id, type, node_id, created "
            + "  FROM devices "
            + " WHERE node_id=? "
    ;

    var formattedSql = mysql.format(sql, [
        nodeId,
    ]);

    return DB.query(this.connection, formattedSql);
};

DeviceRepository.prototype.isGrantedUser = function (deviceId, userId, isOwnerOnly) {
    var self = this;
    return co(function* () {
        var sql = "SELECT nodes.id as node_id, nodes.sharing_level as sharing_level "
            + " FROM devices "
            + " INNER JOIN nodes ON (nodes.id=devices.node_id) "
            + " WHERE devices.id=? ";

        var formattedSql = mysql.format(sql, [
            deviceId,
        ]);

        var node = yield DB.query(self.connection, formattedSql);
        if (!node) {
            return Promise.resolve(false);
        }

        var nodeId = node[0]['node_id'];

        //Nodeが属するGatewayのユーザーと一致するか
        sql = "SELECT nodes.id as node_id FROM nodes "
            + " INNER JOIN gateways ON (gateways.id=nodes.gateway_id) "
            + " WHERE nodes.id=? AND gateways.user_id=? ";

        formattedSql = mysql.format(sql, [
            nodeId,
            userId,
        ]);

        var row = yield DB.query(self.connection, formattedSql);
        if (row && 0 < row.length) {
            return Promise.resolve(true);
        }

        if (isOwnerOnly) {
            //Nodeのオーナーのみの場合、この時点でfalseを返す。
            return Promise.resolve(false);
        }

        //Node共有しているユーザーに含まれるか
        var sharingLevel = constants.NODE_SHARING_LEVEL_USERS;
        sql = "SELECT node_id FROM nodes_users "
            + " INNER JOIN nodes ON (nodes.id=nodes_users.node_id) "
            + " WHERE node_id=? AND user_id=? AND nodes.sharing_level=?";

        formattedSql = mysql.format(sql, [
            nodeId,
            userId,
            sharingLevel,
        ]);

        row = yield DB.query(self.connection, formattedSql);
        if (!row || row.length == 0) {
            return Promise.resolve(false);
        }

        return Promise.resolve(true);

    }).catch(function(err){
        return Promise.reject(Error(err));
    });
}
