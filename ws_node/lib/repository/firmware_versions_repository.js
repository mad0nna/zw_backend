// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var constants = require('../constants');
var DB = require('../db/db');
var mysql = require('mysql');
var dateFormat = require('dateformat');


module.exports = FirmwareVersionsRepository;


function FirmwareVersionsRepository(connection) {
    if(!(this instanceof FirmwareVersionsRepository)) {
        return new FirmwareVersionsRepository(connection);
    }
    this.connection = connection;
}

FirmwareVersionsRepository.prototype.connection = null;

FirmwareVersionsRepository.prototype.save = function(version) {

    var sql = "INSERT INTO `firmware_versions` SET `version`=?, `datetime`=? ";

    var formattedSql = mysql.format(sql, [
        version,
        dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss')
    ]);

    return DB.query(this.connection, formattedSql).then(function(result){
        return result;
    }).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        Promise.reject(Error(err));
    });
};
