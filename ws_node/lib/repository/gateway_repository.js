// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var DB = require('../db/db');
var mysql = require('mysql');
var co = require('co');
var dateFormat = require('dateformat');
var logger = require('../logger');

module.exports = GatewayRepository;


function GatewayRepository(connection) {
    if(!(this instanceof GatewayRepository)) {
        return new GatewayRepository(connection);
    }
    this.connection = connection;
}


GatewayRepository.prototype.connection = null;

GatewayRepository.prototype.find = function(id) {

    var sql = "SELECT id, mac_address, fw_version, manufacturer, state, user_id, created, updated "
            + "  FROM gateways "
            + " WHERE id=? AND user_id != 0";

    var formattedSql = mysql.format(sql, id);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        throw Error(err);
    });
};


GatewayRepository.prototype.findByName = function(name) {

    var sql = "SELECT id, mac_address, fw_version, manufacturer, state, user_id, created, updated "
        + "  FROM gateways "
        + " WHERE mac_address=? AND user_id != 0";

    var formattedSql = mysql.format(sql, name);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        return Promise.reject(Error(err));
    });
};

GatewayRepository.prototype.updateState = function(gatewayId, newState) {
    var sql = "UPDATE gateways SET state=? WHERE id=? ";

    var formattedSql = mysql.format(sql, [
        newState,
        gatewayId
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        return Promise.reject(err);
    });
};

GatewayRepository.prototype.getStatusMapping = function(){
    return {
        1: 'connected',
        2: 'disconnected',
        3: 'inclusion',
        4: 'exclusion',
        5: 'initial',
        6: 'configuring',
        7: 'joining',
        8: 'normal',
        9: 'failed',
    };
};

GatewayRepository.prototype.getStatusNameFromType = function(type){
    var mapping = this.getStatusMapping();
    return mapping[type];
};

GatewayRepository.prototype.getStatusTypeFromName = function(name){
    var mapping = this.getStatusMapping();
    for(var i in mapping) {
        if(mapping[i] == name) {
            return i;
        }
    }
    return -1;
};

GatewayRepository.prototype.updateFirmwareVersion = function(gatewayId, fwVersion) {
    var sql = "UPDATE gateways SET fw_version=? WHERE id=? ";

    var formattedSql = mysql.format(sql, [
        fwVersion,
        gatewayId
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        return Promise.reject(err);
    });
};

GatewayRepository.prototype.updatePublicKey = function(gwName, publicKey) {
    var sql = "UPDATE gateways SET public_key=? WHERE mac_address=? ";

    var formattedSql = mysql.format(sql, [
        publicKey,
        gwName
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        return Promise.reject(err);
    });
};

GatewayRepository.prototype.dustable = function(gatewayId) {
    var sql = "select exists (select * from capabilities where gateway_id=? and capability=4) as dustable";

    var formattedSql = mysql.format(sql, [
        gatewayId,
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        return Promise.reject(err);
    });
};

GatewayRepository.prototype.updateDustIp = function(gwName, networkId) {
    var sql = "update dust_gateways set network_id=? where gateway_id = (select id from gateways where gw_name = ?)";

    var formattedSql = mysql.format(sql, [
        networkId,
        gwName,
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        return Promise.reject(err);
    });
};

GatewayRepository.prototype.deleteDustIp = function(gwName) {
    var sql = "delete from dust_gateways where gateway_id = (select id from gateways where gw_name = ?)";

    var formattedSql = mysql.format(sql, [
        gwName,
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        return Promise.reject(err);
    });
};
