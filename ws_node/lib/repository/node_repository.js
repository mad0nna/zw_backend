// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var constants = require('../constants');
var DB = require('../db/db');
var mysql = require('mysql');
var co = require('co');
var dateFormat = require('dateformat');
var logger = require('../logger');

module.exports = NodeRepository;


function NodeRepository(connection) {
    if(!(this instanceof NodeRepository)) {
        return new NodeRepository(connection);
    }
    this.connection = connection;
}




NodeRepository.prototype.connection = null;

NodeRepository.prototype.getIdByName = function(nodeName) {
    var splitNodeName = nodeName.split('_');
    var manufacturer = splitNodeName[0] || '';
    var product = splitNodeName[1] || '';
    var serialNo = splitNodeName[2] || '';

    var sql = "SELECT id from nodes "
            + " WHERE manufacturer=? AND product=? AND serial_no=? "
    ;

    var formattedSql = mysql.format(sql, [
        manufacturer,
        product,
        serialNo,
    ]);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        throw Error(err);
    });
};

NodeRepository.prototype.saveAppSpecificNodeConfig = function(nodeId, appName, config) {

    var self = this;
    var promise = co(function *(){

        // wipe any previous app configs with the same node id and app name
        var wipeSql = "delete from nodes_apps where node_id=? and app_name=?";
        var formattedWipeSql = mysql.format(wipeSql, [
            nodeId,
            appName
        ]);
        yield DB.query(self.connection, formattedWipeSql);

        var nodeAppSql = "insert into nodes_apps (node_id, app_name, created) values (?, ?, ?)";

        // insert new node app pair into database
        var formattedNodeAppSql = mysql.format(nodeAppSql, [
            nodeId,
            appName, 
            dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss'),
        ]);
        yield DB.query(self.connection, formattedNodeAppSql);

        // get app id
        var lastSql = "SELECT LAST_INSERT_ID() as app_id";
        var dirtyAppId = yield DB.query(self.connection, lastSql);
        var appId = dirtyAppId[0]['app_id'];

        // loop through config, insert every app param pair into database
        for (var i in config) {
            var paramName = config[i]['param_name'];
            var paramValue = JSON.stringify(config[i]['param_value']);

            var appParamSql = "insert into apps_params (app_id, param_name, param_value, created) values (?, ?, ?, ?)";
            var formattedAppParamSql = mysql.format(appParamSql, [
                appId,
                paramName, 
                paramValue, 
                dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss'),
            ]);
            yield DB.query(self.connection, formattedAppParamSql);
        }

    }).catch(function(err){
        return Promise.reject(Error(err));
    });
    return promise;
};

NodeRepository.prototype.find = function(id) {

    var sql = 'SELECT id, gateway_id, protocol, sharing_level, location_type, location_name, '
        + '       manufacturer, product, serial_no, prev_user_id, reason, created, updated, join_key_set '
        + '  FROM nodes '
        + ' WHERE id=?';

    var formattedSql = mysql.format(sql, id);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        return Promise.reject(Error(err));
    });
};


NodeRepository.prototype.findByDeviceId = function(deviceId) {

    var sql = "SELECT nodes.id, nodes.gateway_id, nodes.protocol, nodes.sharing_level, "
            + "       nodes.location_type, nodes.location_name, nodes.manufacturer, "
            + "       nodes.product, nodes.serial_no, nodes.prev_user_id, nodes.reason, "
            + "       nodes.created, nodes.updated, nodes.join_key_set "
            + "  FROM nodes "
            + "  INNER JOIN devices ON (devices.node_id=nodes.id) "
            + " WHERE devices.id=?";

    var formattedSql = mysql.format(sql, [deviceId]);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        return Promise.reject(Error(err));
    });
};


NodeRepository.prototype.findLabels = function(nodeId) {

    var sql = "SELECT label FROM labels WHERE type=2 AND entity_id=?";

    var formattedSql = mysql.format(sql, [nodeId]);

    return DB.query(this.connection, formattedSql).then(function(rows){
        var resultSet = [];
        for(var i in rows) {
            resultSet.push(rows[i]['label'].toString());
        }
        return resultSet;
    }).catch(function(err){
        return Promise.reject(Error(err));
    });
};


NodeRepository.prototype.findSharedUsers = function(nodeId) {
    var sql = "SELECT users.* FROM  nodes_users "
            + " INNER JOIN users ON (users.id=nodes_users.user_id) "
            + " WHERE node_id=?";

    var formattedSql = mysql.format(sql, [
        nodeId
    ]);

    return DB.query(this.connection, formattedSql).then(function(result){
        return result;
    }).catch(function(err){
        return Promise.reject(Error(err));
    });
};

NodeRepository.prototype.getYetAnotherStateMapping = function(){
    return {
        0: 'unknown',
        1: 'operational',
        2: 'lost',
        3: 'negotiating',
    };
};

NodeRepository.prototype.getYetAnotherStateNameFromType = function(type){
    var mapping = this.getYetAnotherStateMapping();
    return mapping[type];
};

NodeRepository.prototype.getYetAnotherStateTypeFromName = function(name){
    var mapping = this.getYetAnotherStateMapping();
    for(var i in mapping) {
        if(mapping[i] == name) {
            return i;
        }
    }
    return -1;
};

NodeRepository.prototype.save = function(id, data) {

    var self = this;
    var sql = '';
    var formattedSql = '';
    var promise = co(function *(){

        //既に存在するか確認
        var node = yield self.find(id);
        var state = (data['failed_cmd'] == undefined || data['failed_cmd'].length == 0) ? constants.NODE_STATE_INCLUDED : constants.NODE_STATE_FAILED;
        var failed_cmd = (data['failed_cmd'] == undefined) ? [] : data['failed_cmd'];
        var yet_another_state = (data['yet_another_state'] == undefined) ? constants.NODE_YET_ANOTHER_STATE_UNKNOWN : self.getYetAnotherStateTypeFromName(data['yet_another_state']);
        var node_type = (data['node_type'] == undefined) ? "" : data['node_type'];
        if(!node) {
            //INSERT
            sql = "INSERT INTO nodes SET "
                + " id=?, "
                + " gateway_id=?, "
                + " protocol=?, "
                + " sharing_level=?, "
                + " manufacturer=?, "
                + " product=?, "
                + " serial_no=?, "
                + " state=?, "
                + " failed_cmd=?, "
                + " reason=?, "
                + " join_key_set=?, "
                + " yet_another_state=?, "
                + " node_type=?, "
                + " created=? ";

            formattedSql = mysql.format(sql, [
                id,
                data['gateway_id'],
                1,
                1,
                data['manufacturer'],
                data['product'],
                data['serial_no'],
                state,
                JSON.stringify(failed_cmd),
                data['reason'],
                data['join_key_set'],
                yet_another_state,
                node_type,
                dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss')
            ]);
            yield DB.query(self.connection, formattedSql);
        } else {
            //UPDATE
            sql = "UPDATE nodes SET "
                + " gateway_id=?, "
                + " protocol=?, "
                + " manufacturer=?, "
                + " product=?, "
                + " serial_no=?, "
                + " state=?, "
                + " failed_cmd=?, "
                + " reason=?, "
                + " join_key_set=?, "
                + " yet_another_state=?, "
                + " node_type=?, "
                + " updated=? "
                + " WHERE id=?";

            formattedSql = mysql.format(sql, [
                data['gateway_id'],
                1,
                data['manufacturer'],
                data['product'],
                data['serial_no'],
                state,
                JSON.stringify(failed_cmd),
                data['reason'],
                data['join_key_set'],
                yet_another_state,
                node_type,
                dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss'),
                id
            ]);
            yield DB.query(self.connection, formattedSql);
        }
    }).catch(function(err){
        Promise.reject(Error(err));
    });

    return promise;
};


NodeRepository.prototype.clearGatewayAssociation = function(nodeId) {

    var self = this;
    var promise = co(function *(){

        //再度Node inclusionが実行された場合に備え、接続していたgatewayの所有者のusers.idを記録しておく。
        //後に改めてNode inclusionが実行された際、同じユーザーかの確認を行い、一致した場合はラベルやデバイスデータを削除せずに保持する。
        var prevUserSql = "SELECT gateways.user_id FROM nodes "
            + " INNER JOIN gateways ON (gateways.id=nodes.gateway_id)"
            + " WHERE nodes.id=?";

        var formattedPrevUserSql = mysql.format(prevUserSql, [
            nodeId
        ]);
        var prevUsers = yield DB.query(self.connection, formattedPrevUserSql);
        var prevUserInfo = prevUsers[0] || {};
        var prevUserId = prevUserInfo['user_id'] || 0;
        var reason = 'node_excl';


        var sql = "UPDATE nodes SET prev_user_id=?, gateway_id=0, state=0, failed_cmd='', reason=?, updated=? WHERE id=?";

        var formattedSql = mysql.format(sql, [
            prevUserId,
            reason,
            dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss'),
            nodeId
        ]);

        return DB.query(self.connection, formattedSql);

    }).catch(function(err){
        return Promise.reject(Error(err));
    });
    return promise;
};


NodeRepository.prototype.clearNodeSharing = function(nodeId) {
    var sql = "DELETE FROM  nodes_users WHERE node_id=?";

    var formattedSql = mysql.format(sql, [
        nodeId
    ]);

    return DB.query(this.connection, formattedSql).then(function(result){
        return result;
    }).catch(function(err){
        return Promise.reject(Error(err));
    });
};


NodeRepository.prototype.registerAction = function(nodeId, action) {
    var sql = "INSERT INTO node_actions SET "
        + " node_id=?, "
        + " type=?, "
        + " description=?, "
        + " created=? ";

    var formattedSql = mysql.format(sql, [
        nodeId,
        this.getActionTypeFromName(action['type']),  //TODO: 無効な値だった場合の処理
        action['description'],
        dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss')
    ]);

    return DB.query(this.connection, formattedSql).then(function(result){
        return result;
    }).catch(function(err){
        return Promise.reject(Error(err));
    });
};

NodeRepository.prototype.replaceActions = function(nodeId, actions) {
    var self = this;

    return co(function *(){
        yield self.removeAllActions(nodeId);

        for(var i in actions) {
            yield self.registerAction(nodeId, actions[i]);
        }
    }).catch(function(err){
        return Promise.reject(Error(err));
    });
};


NodeRepository.prototype.removeAllActions = function(nodeId) {
    var sql = "DELETE FROM node_actions WHERE node_id=? ";

    var formattedSql = mysql.format(sql, [
        nodeId
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        return Promise.reject(Error(err));
    });
};

NodeRepository.prototype.removeAllLabels = function(nodeId) {
    var sql = "DELETE FROM labels WHERE type=2 AND entity_id=?";

    var formattedSql = mysql.format(sql, [
        nodeId
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        return Promise.reject(Error(err));
    });
};

NodeRepository.prototype.getActionTypeMapping = function(){
    return {
        1: 'binary',
        2: 'range',
        3: 'trigger'
    };
};

NodeRepository.prototype.getActionNameFromType = function(type){
    var mapping = this.getActionTypeMapping();
    return mapping[type];
};

NodeRepository.prototype.getActionTypeFromName = function(name){
    var mapping = this.getActionTypeMapping();
    for(var i in mapping) {
        if(mapping[i] == name) {
            return i;
        }
    }
    return -1;
};

NodeRepository.prototype.getSharingTypeMapping = function(){
    return {
        1: 'private',
        2: 'users',
    };
};

NodeRepository.prototype.getSharingTypeNameFromType = function(type){
    var mapping = this.getSharingTypeMapping();
    return mapping[type];
};

NodeRepository.prototype.getSharingTypeFromName = function(name){
    var mapping = this.getSharingTypeMapping();
    for(var i in mapping) {
        if(mapping[i] == name) {
            return i;
        }
    }
    return -1;
};

NodeRepository.prototype.findConfig　= function(data) {
    var sql = "SELECT * FROM node_configs WHERE "
            + " node_id=? "
            + " AND config_type=? "
            + " AND param_id=? ";

    var formattedSql = mysql.format(sql, [
        data['node_id'],
        data['config_type'],
        data['param_id']
    ]);

    return DB.query(this.connection, formattedSql).then(function(result){
        return result;
    }).catch(function(err){
        return Promise.reject(Error(err));
    });
}

NodeRepository.prototype.updateConfig　= function(id, data) {
    
    if (data['config_type'] == 'zwave') {
        var sql = "UPDATE node_configs SET "
            + " node_id=?, "
            + " config_type=?, "
            + " param_type=?, "
            + " param_size=?, "
            + " param_id=?, "
            + " value=?, "
            + " update_pending=?, "
            + " updated=? "
            + " WHERE "
            + " id=? ";
    
        var formattedSql = mysql.format(sql, [
            data['node_id'],
            data['config_type'],
            data['param_type'],
            data['param_size'],
            data['param_id'],
            data['value'],
            false,
            dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss'),
            id
        ]);
    } else {
        var sql = "UPDATE node_configs SET "
            + " node_id=?, "
            + " config_type=?, "
            + " param_type=?, "
            + " param_id=?, "
            + " value=?, "
            + " update_pending=?, "
            + " updated=? "
            + " WHERE "
            + " id=? ";
    
        var formattedSql = mysql.format(sql, [
            data['node_id'],
            data['config_type'],
            data['param_type'],
            data['param_id'],
            data['value'],
            false,
            dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss'),
            id
        ]);
    }

    return DB.query(this.connection, formattedSql).then(function(result){
        return result;
    }).catch(function(err){
        return Promise.reject(Error(err));
    });
};

NodeRepository.prototype.changeNodeState = function(nodeId, state) {
    var sql = "UPDATE nodes SET yet_another_state=? WHERE id=? ";

    var formattedSql = mysql.format(sql, [
        this.getYetAnotherStateTypeFromName(state),
        nodeId
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = err + '. SQL: ' + formattedSql;
        return Promise.reject(err);
    });
};
