// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var DB = require('../db/db');
var mysql = require('mysql');
var co = require('co');
var dateFormat = require('dateformat');
var logger = require('../logger');
var DeviceRepository = require('./device_repository');

module.exports = ScenarioRepository;


function ScenarioRepository(connection) {
    if(!(this instanceof ScenarioRepository)) {
        return new ScenarioRepository(connection);
    }
    this.connection = connection;
}


ScenarioRepository.prototype.connection = null;

ScenarioRepository.prototype.find = function(id) {

    var sql = "SELECT id, user_id, scenario_type, condition_operator, "
            + "       trigger_after, wbgt_threshold, "
            + "       created, updated "
            + "  FROM scenarios "
            + " WHERE id=?";

    var formattedSql = mysql.format(sql, id);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        throw Error(err);
    });
};


ScenarioRepository.prototype.findEvents = function(scenarioId) {
    var sql = "SELECT id, scenario_id, message, created, updated "
            + "  FROM scenario_events "
            + " WHERE scenario_id=?";

    var formattedSql = mysql.format(sql, [scenarioId]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        throw Error(err);
    });
};

ScenarioRepository.prototype.findActions = function(scenarioId) {
    var sql = "SELECT id, scenario_id, type, content, created "
        + "  FROM scenario_actions "
        + " WHERE scenario_id=?";

    var formattedSql = mysql.format(sql, [scenarioId]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        throw Error(err);
    });
};

ScenarioRepository.prototype.findDevices = function(scenarioId) {
    var self = this;
    return co(function *(){

        var devices = [];

        var sqlScenarios = "SELECT temperature_dev, humidity_dev, device_list "
            + "  FROM scenarios "
            + " WHERE id=?";

        var formattedSqlScenarios = mysql.format(sqlScenarios, [scenarioId]);

        var scenarios = yield DB.query(self.connection, formattedSqlScenarios);
        for (var i in scenarios) {
            if (scenarios[i]['temperature_dev']) {
                devices.push(scenarios[i]['temperature_dev']);
            }
            if (scenarios[i]['humidity_dev']) {
                devices.push(scenarios[i]['humidity_dev']);
            }
            if (scenarios[i]['device_list']) {
                devices = devices.concat(scenarios[i]['device_list'].split(','));
            }
        }

        var sqlConditions = "SELECT lhs "
            + "  FROM scenario_conditions "
            + " WHERE scenario_id=?";

        var formattedSqlConditions = mysql.format(sqlConditions, [scenarioId]);

        var conditions = yield DB.query(self.connection, formattedSqlConditions);
        for (var i in conditions) {
            if (conditions[i]['lhs']) {
                devices.push(conditions[i]['lhs']);
            }
        }

        return Promise.resolve(devices);

    }).catch(function(err){
        return Promise.reject(Error(err));
    });
};



ScenarioRepository.prototype.registerEvent = function(scenarioId, event) {
    var sql = "INSERT INTO scenario_events SET "
            + " scenario_id=?, "
            + " message=?, "
            + " created=? "
        ;

    var formattedSql = mysql.format(sql, [
        scenarioId,
        event['message'],
        dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss.l')
    ]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        throw Error(err);
    });
};


/**
 * PUSH通知に必要な情報を含んだ形でシナリオ条件の一覧を返す。
 * @param {int} scenarioId
 * @return {Promise}
 */
ScenarioRepository.prototype.getScenarioConditionsForPushNotification = function(scenarioId) {
    var sql = "SELECT scenario_id, lhs, operator, rhs, unit, devices.type AS device_type "
        + "  FROM scenario_conditions "
        + "  LEFT OUTER JOIN devices ON (devices.id=lhs) "
        + " WHERE scenario_id=?";

    var formattedSql = mysql.format(sql, [scenarioId]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        throw Error(err);
    });
};

ScenarioRepository.prototype.findScenarioByDeviceId = function(deviceId) {
    
    var sql = "SELECT scenarios.* "
        + "  FROM scenarios "
        + " LEFT OUTER JOIN scenario_conditions ON (scenario_conditions.scenario_id=scenarios.id)"
        + " WHERE lhs=?";

    var formattedSql = mysql.format(sql, [deviceId]);

    return DB.query(this.connection, formattedSql).catch(function(err){
        throw Error(err);
    });

};
