// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var constants = require('../constants');
var DB = require('../db/db');
var co = require('co');
var mysql = require('mysql');

module.exports = UserRepository;


function UserRepository(connection) {
    if(!(this instanceof UserRepository)) {
        return new UserRepository(connection);
    }

    this.connection = connection;
}




UserRepository.prototype.connection = null;


UserRepository.prototype.find = function(id) {

    var sql = "SELECT id, login_id, name, email, platform, token, push_endpoint_arn, created, updated "
            + '  FROM users '
            + ' WHERE id=?';

    var formattedSql = mysql.format(sql, id);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        err = (err instanceof Error) ? err : Error(err);
        return Promise.reject(err);
    });
};

UserRepository.prototype.findByEmail = function(email) {

    var sql = "SELECT id, login_id, name, email, platform, token, push_endpoint_arn, created, updated "
        + '  FROM users '
        + ' WHERE email=?';

    var formattedSql = mysql.format(sql, email);

    return DB.query(this.connection, formattedSql).catch(function(err){
        err = (err instanceof Error) ? err : Error(err);
        return Promise.reject(err);
    });
};


UserRepository.prototype.findByGatewayId = function(gatewayId) {

    var sql = 'SELECT users.id, users.login_id, users.name, '
            + '       users.email, users.platform, users.token, users.push_endpoint_arn, users.created, users.updated '
            + '  FROM users '
            + ' INNER JOIN gateways ON (gateways.user_id=users.id) '
            + ' WHERE gateways.id=?';

    var formattedSql = mysql.format(sql, gatewayId);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        err = (err instanceof Error) ? err : Error(err);
        return Promise.reject(err);
    });
};


UserRepository.prototype.findByAuthToken = function(token) {

    var sql = 'SELECT users.id, users.login_id, users.name, '
        + '       users.email, users.platform, users.token, users.push_endpoint_arn, users.created, users.updated '
        + '  FROM users '
        + ' INNER JOIN auth_tokens ON (auth_tokens.user_id=users.id) '
        + ' WHERE auth_tokens.token=? AND auth_tokens.expires >= NOW()';

    var formattedSql = mysql.format(sql, [token]);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        err = (err instanceof Error) ? err : Error(err);
        return Promise.reject(err);
    });
};

UserRepository.prototype.findByNodeId = function(nodeId) {

    //Gatewayから切り離されたNodeも存在するため、Gatewayと紐づいているもののみ対象とする。
    var sharingLevel = constants.NODE_SHARING_LEVEL_USERS;
    var sql = "SELECT users.id, users.login_id, users.name, users.email, "
        + "           users.platform, users.token, users.push_endpoint_arn, "
        + "           users.created, users.updated "
        + "  FROM users "
        + " INNER JOIN nodes_users ON (nodes_users.user_id=users.id) "
        + " INNER JOIN nodes ON (nodes.id=nodes_users.node_id) "
        + " INNER JOIN gateways ON (gateways.id=nodes.gateway_id) "
        + " WHERE nodes.id=? AND nodes.sharing_level=?";

    var formattedSql = mysql.format(sql, [nodeId, sharingLevel]);
    return DB.query(this.connection, formattedSql);
};


UserRepository.prototype.findByScenarioId = function(token) {

    var sql = 'SELECT users.id, users.login_id, users.name, '
        + '       users.email, users.platform, users.token, users.push_endpoint_arn, users.created, users.updated '
        + '  FROM users '
        + ' INNER JOIN scenarios ON (scenarios.user_id=users.id) '
        + ' WHERE scenarios.id=?';

    var formattedSql = mysql.format(sql, [token]);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        err = (err instanceof Error) ? err : Error(err);
        return Promise.reject(err);
    });
};


/**
 * 指定されたモバイルトークンに対応するユーザーを返す。
 *
 * @param {String} token モバイルトークン(users.token)
 *
 * ユーザーは1つのトークンに対し複数存在し得るため、
 * ログインの最終日時が最も新しいユーザーを対象に返す。
 */
UserRepository.prototype.findUserByToken = function(token) {

    var sql = 'SELECT users.id, users.login_id, users.name, '
        + '       users.email, users.platform, users.token, users.push_endpoint_arn, users.created, users.updated '
        + '  FROM users '
        + ' INNER JOIN auth_tokens ON (auth_tokens.user_id=users.id) '
        + ' WHERE users.token=? '
        + ' ORDER BY auth_tokens.expires DESC '
        + ' LIMIT 1 '
        ;

    var formattedSql = mysql.format(sql, [token.toString()]);

    return DB.query(this.connection, formattedSql).then(function(rows){
        if(!rows || rows.length == 0) {
            return false;
        }
        return rows[0];
    }).catch(function(err){
        err = (err instanceof Error) ? err : Error(err);
        return Promise.reject(err);
    });
};
