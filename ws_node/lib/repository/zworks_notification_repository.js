// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var DB = require('../db/db');
var mysql = require('mysql');

var dateFormat = require('dateformat');

function  ZworksNotificationRepository(db) {
    if (!(this instanceof ZworksNotificationRepository)) return new ZworksNotificationRepository();
    this.db = db;
}

ZworksNotificationRepository.prototype.db = null;

ZworksNotificationRepository.prototype.findLogsSince = function(baseTime){
    //未処理の通知ログを取得する。
    //ws-nodeでは、correlation_idがセットされていないログを対象とする。
    //(correlation_idがセットされているものはHTTP APIのレスポンスとしての通知であるため)
    var sql = "SELECT * FROM zworks_notifications WHERE created >= ? AND state in (0, 251, 252, 253, 254) AND correlation_id=0 ORDER BY created";
    var formattedDate =  dateFormat(baseTime, "yyyy-mm-dd HH:MM:ss");
    var formattedSql = mysql.format(sql, [formattedDate]);

    return DB.query(this.db, formattedSql, {'logging': false});
};


ZworksNotificationRepository.prototype.updateLogState = function(messageIds, newState){
    if(typeof messageIds != 'object') {
        messageIds = [messageIds];
    }

    if(messageIds.length == 0) {
        return new Promise(function(resolve){
            //処理するデータが存在しない場合は何も行わず空の結果を返す。
            resolve(true);
        });
    }

    var updated = new Date();
    var messageIdList = messageIds.join(',');

    var sql = "UPDATE zworks_notifications SET state=?, updated=? WHERE id IN(" + messageIdList + ")";
    var formattedDate = dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss');
    var formattedSql = mysql.format(sql, [newState, formattedDate]);

    return DB.query(this.db, formattedSql);
};


ZworksNotificationRepository.prototype.register = function(notification){
    var sql = "INSERT INTO zworks_notifications SET "
            + " type=?, "
            + " data=?, "
            + " state=?, "
            + " correlation_id=?, "
            + " created=? "
    ;
    var created = dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss');
    var formattedSql = mysql.format(sql, [
        notification['type'],
        notification['data'],
        notification['state'],
        0,
        created
    ]);
    return DB.query(this.db, formattedSql);
};


module.exports = ZworksNotificationRepository;
