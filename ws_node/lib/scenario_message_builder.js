// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var constants = require('./constants.js');

/**
 * 与えられたシナリオ情報から通知メッセージを構築するオブジェクト
 * @returns {ScenarioMessageBuilder}
 * @constructor
 */
function ScenarioMessageBuilder() {
    if(!(this instanceof ScenarioMessageBuilder) ) {
        return new ScenarioMessageBuilder();
    }

}

ScenarioMessageBuilder.prototype.scenario = null;
ScenarioMessageBuilder.prototype.conditions = null;
ScenarioMessageBuilder.prototype.description = null;

ScenarioMessageBuilder.prototype.setScenario = function(scenario) {
    this.scenario = scenario;
};

/**
 * シナリオ条件をセットする
 * @param {object} conditions PUSH通知用のシナリオ条件の配列(device_typeも含んでいるもの)
 */
ScenarioMessageBuilder.prototype.setScenarioConditions = function(conditions){
    this.conditions = conditions;
};

/**
 * メッセージに埋め込む説明文をセットするシナリオのactionsにセットされた値
 * @param {string} description
 */
ScenarioMessageBuilder.prototype.setDescription = function(description){
    this.description = description;
};

/**
 * 通知の本文を構築して返す。
 * @returns {string}
 */
ScenarioMessageBuilder.prototype.build = function(){

    var message = '';
    
    if(this.scenario['scenario_type'] == constants.SCENARIO_TYPE_SIMPLE) {

        var conditionTextBuilder = new ConditionTextBuilder();
        var conditionMessages = [];

        var conditionMessage = '';
        for(var i in this.conditions) {
            conditionMessage = conditionTextBuilder.buildMessage(this.conditions[i]);

            // 同じ内容の文面は通知内容には含めないようにする
            var isNewMessage = true;
            for(var j in conditionMessages) {
                if (conditionMessage == conditionMessages[j]) {
                    isNewMessage = false;
                    break;
                }
            }
            if(isNewMessage == true) {
                conditionMessages.push(conditionMessage);
            }
        }

        var threshold = this.getHumanReadableTimeString(this.scenario['trigger_after']);
        if (0 < this.scenario['trigger_after']) {
            message = this.description + "\n"
                        + "\n"
                        + '【通知したシナリオ条件】' + "\n"
                        + '以下の状態であることを' + threshold + '以上検出しました。' + "\n";
        } else {
            message = this.description + "\n"
                        + "\n"
                        + '【通知したシナリオ条件】' + "\n"
                        + '以下の状態であることを検出しました。' + "\n";
        }

        var prefix = '';
        for(i in conditionMessages) {
            if(i > 0 && this.scenario['condition_operator'] == constants.SCENARIO_CONDITION_OPERATOR_OR) {
                prefix = 'または';
            }
            message += '・' + prefix + conditionMessages[i] + "\n";
        }

    } else if(this.scenario['scenario_type'] == constants.SCENARIO_TYPE_WBGT) {

        message = this.description + "\n"
                    + "\n"
                    + '【通知したシナリオ条件】' + "\n"
                    + '以下の状態であることを検出しました。' + "\n";

        if(this.scenario['wbgt_threshold'] >= 30) {
            message += '・' + '危険温度になりました' + "\n";
        } else if(this.scenario['wbgt_threshold'] >= 25) {
            message += '・' + '注意温度になりました' + "\n";
        } else {
            message += '・' + '指定された温度になりました(' + this.scenario['wbgt_threshold'] + ' ℃)' + "\n";
        }

    } else if(this.scenario['scenario_type'] == constants.SCENARIO_TYPE_SCXML) {

        message = this.description + "\n";

    }

    return message;
};


ScenarioMessageBuilder.prototype.getHumanReadableTimeString = function(time) {
    var hour = Math.floor(time / 60);
    var minute = time % 60;

    if(hour > 0) {
        if (minute > 0) {
            return hour + '時間' + minute + '分';
        } else {
            return hour + '時間';
        }
    }
    return minute + '分';
};


/**
 * シナリオ条件を文章化するオブジェクト
 * @constructor
 */
function ConditionTextBuilder() {
}

/**
 * シナリオ条件を文章化して返す。
 * @param {object} condition PUSH通知用のシナリオ条件の配列(device_typeも含んでいるもの)
 */
ConditionTextBuilder.prototype.buildMessage = function(condition) {

    var filters = [
        this.inequalityFilter,
        this.equalityFilter,
        this.frequencyFilter
    ];

    var message = '';
    for(var i in filters) {
        message = filters[i].bind(this)(condition);
        if(message != null) {
            return message;
        }
    }
    return '';
};

/**
 * 不等号用の文章を構築するフィルタ
 * @param {object} condition PUSH通知用のシナリオ条件の配列(device_typeも含んでいるもの)
 * @return {string|null} メッセージを処理した場合メッセージ文字列。それ以外の条件の場合、null
 */
ConditionTextBuilder.prototype.inequalityFilter = function(condition) {
    var validOperators = [
        constants.SCENARIO_OPERATOR_LESS,
        constants.SCENARIO_OPERATOR_LESS_EQ,
        constants.SCENARIO_OPERATOR_GREATER,
        constants.SCENARIO_OPERATOR_GREATER_EQ
    ];

    if(validOperators.indexOf(condition['operator']) == -1) {
        return null;
    }

    var suffixMapping = {};
    suffixMapping[constants.SCENARIO_OPERATOR_LESS]       = 'よりも下';
    suffixMapping[constants.SCENARIO_OPERATOR_LESS_EQ]    = '以下';
    suffixMapping[constants.SCENARIO_OPERATOR_GREATER]    = 'よりも上';
    suffixMapping[constants.SCENARIO_OPERATOR_GREATER_EQ] = '以上';

    var sensorName = this.getSensorNameFromType(condition['device_type']);
    var suffix = suffixMapping[condition['operator']];
    var unit = this.getUnitString(condition['unit']);

    return sensorName + '：' + condition['rhs'] + unit + suffix;
};


/**
 * 等号用の文章を構築するフィルタ
 * @param {object} condition PUSH通知用のシナリオ条件の配列(device_typeも含んでいるもの)
 * @return {string|null} メッセージを処理した場合メッセージ文字列。それ以外の条件の場合、null
 */
ConditionTextBuilder.prototype.equalityFilter = function(condition) {
    if(condition['operator'] != constants.SCENARIO_OPERATOR_EQ) {
        return null;
    }

    var sensorName = this.getSensorNameFromType(condition['device_type']);
    var unit = this.getUnitString(condition['unit']);
    var deviceValue = '';

    switch(condition['device_type']) {
        case constants.DEVICE_TYPE_OPEN_CLOSE:
        case constants.DEVICE_TYPE_COVER:
            deviceValue = (condition['rhs'] == 0) ? '閉まっています' : '開いています';
            break;
        case constants.DEVICE_TYPE_LOCK:
            deviceValue = '開いています';
            break;
        case constants.DEVICE_TYPE_MOTION:
            deviceValue = (condition['rhs'] == 0) ? '未検出' : '検出';
            break;
        case constants.DEVICE_TYPE_BATTERY:
            deviceValue = (condition['rhs'] == 255 || condition['rhs'] == 10) ? '電池残量の低下' : '電池残量の回復';
            break;

        case constants.DEVICE_TYPE_SMOKE:
            deviceValue = (condition['rhs'] == 255) ? '火災警報が発生しています' : '火災は検知されていません';
            break;

        case constants.DEVICE_TYPE_PERSON_STATE:
            var rhs = parseInt(condition['rhs']);
            switch(rhs) {
                case 0:
                    deviceValue = '人がいないことを検知しました';
                    break;
                case 1:
                    deviceValue = '人の存在を検知しました';
                    break;
                case 2:
                    deviceValue = '人の行動を検知しました';
                    break;
                default:
                    deviceValue = '状態を検知しましたが不明の状態です';
                    break;
            }
            break;

        default:
            deviceValue = condition['rhs'] + unit;
    }

    return sensorName + '：' + deviceValue;
};


/**
 * freq用の文章を構築するフィルタ
 * @param {object} condition PUSH通知用のシナリオ条件の配列(device_typeも含んでいるもの)
 * @return {string|null} メッセージを処理した場合メッセージ文字列。それ以外の条件の場合、null
 */
ConditionTextBuilder.prototype.frequencyFilter = function(condition) {
    if(condition['operator'] != constants.SCENARIO_OPERATOR_FREQ) {
        return null;
    }

    var sensorName = this.getSensorNameFromType(condition['device_type']);
    var unit = this.getUnitString(condition['unit']);
    var description = '';

    var rhs = condition['rhs'];
    if(rhs == 'high') {
        description = '頻繁に反応があります';
    } else if(rhs == 'med') {
        description = '反応があります';
    } else if(rhs == 'low') {
        description = '反応が少しだけあります';
    } else if(rhs == 'almost_none') {
        description = '反応がほとんどありません';
    } else if(/[0-9]+-[0-9]+/.test(rhs)) {
        description = '検出頻度 ';
        var numbers = rhs.split('-');
        if(numbers[0] == numbers[1]) {
            description += numbers[0] + '回／時';
        } else {
            description += numbers[0] + '〜' + numbers[1] + '回／時';
        }
    } else {
        return null;
    }

    return sensorName + '：' + description;
};


ConditionTextBuilder.prototype.getSensorNameFromType = function(sensorType) {
    var sensorNameMapping = {};
    sensorNameMapping[constants.DEVICE_TYPE_TEMPERATURE] = '温度センサー';
    sensorNameMapping[constants.DEVICE_TYPE_HUMIDITY] = '湿度センサー';
    sensorNameMapping[constants.DEVICE_TYPE_MOTION] = '人感センサー';
    sensorNameMapping[constants.DEVICE_TYPE_OPEN_CLOSE] = 'ドアセンサー';
    sensorNameMapping[constants.DEVICE_TYPE_LOCK] = '施錠センサー';
    sensorNameMapping[constants.DEVICE_TYPE_COVER] = 'センサー開閉';
    sensorNameMapping[constants.DEVICE_TYPE_BATTERY] = 'センサーバッテリー';
    sensorNameMapping[constants.DEVICE_TYPE_LUMINANCE] = '照度センサー';
    sensorNameMapping[constants.DEVICE_TYPE_HEART_RATE] = "心拍センサー";
    sensorNameMapping[constants.DEVICE_TYPE_BREATHING_RATE] = "呼吸センサー";
    sensorNameMapping[constants.DEVICE_TYPE_SMOKE] = "火災センサー";
    sensorNameMapping[constants.DEVICE_TYPE_PERSON_STATE] = "状態検知センサー";
    sensorNameMapping[constants.DEVICE_TYPE_DISTANCE] = "距離測定センサー";

    if(typeof sensorNameMapping[sensorType] == 'undefined') {
        return 'undefined';
    }
    return sensorNameMapping[sensorType];
};

ConditionTextBuilder.prototype.getUnitString = function(unit) {
    if(unit == 'C') {
        unit = '℃';
    }
    return unit;
};

module.exports = {
    ScenarioMessageBuilder: ScenarioMessageBuilder,
    ConditionTextBuilder: ConditionTextBuilder
};
