// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'


var services = {};

var ServiceContainer = {};

ServiceContainer.register = function(name, service){
    services[name] = service;
};

ServiceContainer.get = function(name){
    if(services[name]) {
        return services[name];
    }
    console.log('無効なサービスが指定されました：' + name);
    return null;
};


module.exports = ServiceContainer;
