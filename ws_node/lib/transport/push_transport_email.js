// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'


var co = require('co');
var nconf = require('nconf');
var nodeMailer = require('nodemailer');
var logger = require('../logger');
var ScenarioRepository = require('../repository/scenario_repository');
var SMB = require('../scenario_message_builder');


/**
 * Emailを使用してユーザーにPUSH通知を実行する
 * @returns {PushTransportEmail}
 * @constructor
 */
function PushTransportEmail() {
    if(!(this instanceof PushTransportEmail) ) {
        return new PushTransportEmail();
    }

}

/**
 * @type {SNS}
 */
PushTransportEmail.prototype.mailer = null;

PushTransportEmail.prototype.getMailer = function() {
    if(this.mailer) {
        return this.mailer;
    }

    var proxy_setting = nconf.get('push:email_proxy');
    if (proxy_setting != undefined) {
        this.mailer = nodeMailer.createTransport({proxy: proxy_setting});
    } else {
        this.mailer = nodeMailer.createTransport();
    }
    return this.mailer;
};


/**
 * 通知を実行する。
 * @param {object} context DBコネクションなどを含んだオブジェクト
 * @param {object} user usersのレコード
 * @param {object} data scenario_eventsのレコード
 * @returns {Promise}
 */
PushTransportEmail.prototype.publish = function(context, user, data) {
    var self = this;
    var promise = co(function *(){

        var mailer = self.getMailer();
        var message = yield self.getMailBody(context['connection'], data['scenario_id']);

        var params = {
            from: nconf.get('push:email_from'),
            to: user['email'].toString(),
            subject: nconf.get('push:email_subject'),
            text: message
        };

        logger.addDebug('[Send Email]' + JSON.stringify(params));
        var result = mailer.sendMail(params, function(err, info){
            if(err) {
                logger.addWarn(Error(err));
                return info;
            }
            logger.addDebug('[Send Email] Result: ' + JSON.stringify(info));
            return info;
        });
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};


PushTransportEmail.prototype.getMailBody = function(connection, scenarioId){
    var self = this;
    var promise = co(function *(){

        var scenarioRepository = new ScenarioRepository(connection);
        //シナリオを取得する
        var scenario = yield scenarioRepository.find(scenarioId);

        //シナリオのactionを取得する
        var actions = yield scenarioRepository.findActions(scenarioId);
        //現在は常に1件のみの想定
        var action = actions[0];
        if(typeof action == 'undefined') {
            return Promise.reject('シナリオにactionが登録されていません。 scenario_id=' + scenarioId);
        }
        var description = action['content'];

        //シナリオの条件一覧をdevice_type付きで取得する
        var conditions = yield scenarioRepository.getScenarioConditionsForPushNotification(scenarioId);

        //メール本文の構築
        var scenarioMessageBuilder = new SMB.ScenarioMessageBuilder();
        scenarioMessageBuilder.setScenario(scenario);
        scenarioMessageBuilder.setScenarioConditions(conditions);
        scenarioMessageBuilder.setDescription(description);

        return scenarioMessageBuilder.build();

    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};


module.exports = PushTransportEmail;
