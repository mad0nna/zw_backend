// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'


var AWS = require('aws-sdk');
var nconf = require('nconf');
var logger = require('../logger');
var proxy = require('proxy-agent');

/**
 * Amazon SNS を使用してユーザーにPUSH通知を実行する
 * @returns {PushTransportSns}
 * @constructor
 */
function PushTransportSns() {
    if(!(this instanceof PushTransportSns) ) {
        return new PushTransportSns();
    }
}

/**
 * @type {SNS}
 */
PushTransportSns.prototype.snsClient = null;


PushTransportSns.prototype.getClient = function() {
    if(this.snsClient) {
        return this.snsClient;
    }

    var options = {
        accessKeyId: nconf.get('aws:access_key'),
        secretAccessKey: nconf.get('aws:secret_key'),
        region: nconf.get('aws:region'),
        apiVersion: nconf.get('aws:sns_api_version')
    };

    if(nconf.get('aws:api_proxy')) {
        options['httpOptions'] = {
            agent: proxy(nconf.get('aws:api_proxy'))
        };
    }

    this.snsClient = new AWS.SNS(options);

    return this.snsClient;
};


/**
 * 通知を実行する。
 * @param {object} context DBコネクションなどを含んだオブジェクト
 * @param {object} user usersのレコード
 * @param {object} data scenario_eventsのレコード
 * @returns {Promise}
 */
PushTransportSns.prototype.publish = function(context, user, data) {
    var self = this;
    var promise = new Promise(function(resolve, reject){

        var client = self.getClient();
        var message = data['content'];
        var params = {
            Message: JSON.stringify({
                default: message,
                GCM: JSON.stringify({data: {message: message, scenario_id: parseInt(data['scenario_id'])}}),
                APNS: JSON.stringify({aps: {alert: message, sound: 'default'}, scenario_id: parseInt(data['scenario_id'])}),
                APNS_SANDBOX: JSON.stringify({aps: {alert: message, sound: 'default'}, scenario_id: parseInt(data['scenario_id'])})
            }),
            MessageStructure: 'json',
            TargetArn: user['push_endpoint_arn'].toString()
        };

        logger.addDebug('[SNS API CALL]' + JSON.stringify(params));
        client.publish(params, function(err, data){
            if(err) {
                logger.addWarn(Error(err));
                resolve(data);
                return;
            }
            logger.addDebug('[SNS API RESPONSE]' + JSON.stringify(data));
            resolve(data);
        });

    });
    return promise;
};



module.exports = PushTransportSns;
