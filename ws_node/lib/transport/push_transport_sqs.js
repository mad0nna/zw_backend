// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'


var AWS = require('aws-sdk');
var nconf = require('nconf');
var logger = require('../logger');
var proxy = require('proxy-agent');

/**
 * Amazon SQS を使用してユーザーにPUSH通知を実行する
 * @returns {PushTransportSqs}
 * @constructor
 */
function PushTransportSqs() {
    if(!(this instanceof PushTransportSqs) ) {
        return new PushTransportSqs();
    }
}

/**
 * @type {SQS}
 */
PushTransportSqs.prototype.sqsClient = null;


PushTransportSqs.prototype.getClient = function() {
    if(this.sqsClient) {
        return this.sqsClient;
    }

    var options = {
        accessKeyId: nconf.get('aws:access_key'),
        secretAccessKey: nconf.get('aws:secret_key'),
        region: nconf.get('aws:region'),
        apiVersion: nconf.get('aws:sqs_api_version')
    };

    if(nconf.get('aws:api_proxy')) {
        options['httpOptions'] = {
            agent: proxy(nconf.get('aws:api_proxy'))
        };
    }

    this.sqsClient = new AWS.SQS(options);

    return this.sqsClient;
};


/**
 * 通知を実行する。
 * @param {object} context DBコネクションなどを含んだオブジェクト
 * @param {object} user usersのレコード
 * @param {object} data scenario_eventsのレコード
 * @returns {Promise}
 */
PushTransportSqs.prototype.publish = function(context, user, data) {
    var self = this;
    var promise = new Promise(function(resolve, reject){

        var client = self.getClient();
        var message = data['content'];
        var params = {
          MessageBody: message,
          QueueUrl: nconf.get('aws:sqs_queue_url'),
        };

        logger.addDebug('[SQS API CALL]' + JSON.stringify(params));
        client.sendMessage(params, function(err, data){
            if(err) {
                logger.addWarn(Error(err));
                resolve(data);
                return;
            }
            logger.addDebug('[SQS API RESPONSE]' + JSON.stringify(data));
            resolve(data);
        });

    });
    return promise;
};



module.exports = PushTransportSqs;
