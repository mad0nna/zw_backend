// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'


module.exports = WebSocketHandlerBase;

/**
 * WebSocketハンドラの基底クラス
 * @constructor
 */
function WebSocketHandlerBase() {
}


/**
 * WebSocketハンドラを束ねるMessagingAdapter
 * 他のハンドラとの通信などの用途に使用する
 * @var {MobileMessagingAdapter} adapter
 */
WebSocketHandlerBase.prototype.mobileMessagingAdapter = null;

/**
 * WebSocket用にイベントハンドラの上書き等初期処理を実行する
 * @param {http.Server} httpServer
 */
WebSocketHandlerBase.prototype.init = function(httpServer) {
};


/**
 * WebSocketでの通知を実行する
 * @param {int} userId users.login_id
 * @param {string} notificationType
 * @param {object} data
 */
WebSocketHandlerBase.prototype.send = function(userId, notificationType, data) {
};


/**
 * WebSocketハンドラを束ねるMessagingAdapterを設定する。
 * @param {MobileMessagingAdapter} adapter
 */
WebSocketHandlerBase.prototype.setMobileMessagingAdapter = function(adapter) {
    this.mobileMessagingAdapter = adapter;
};



/**
 * 他のハンドラから通知されたイベントの処理を行う
 * @param {string} type イベント名
 * @param {object} data データを含んだJSON
 * @param {string} origin イベントの送信元のハンドラ名
 */
WebSocketHandlerBase.prototype.processHandlerEvent = function(type, data, origin) {
};
