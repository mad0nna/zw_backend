// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var Authenticator = require('../authenticator');
var co = require('co');
var constants = require('../constants');
var dateFormat = require('dateformat');
var DB = require('../db/db');
var logger = require('../logger');
var nconf = require('nconf');
var socketio = require( 'socket.io' );
var socketioClient = require( 'socket.io/lib/client' );
var ServiceContainer = require('../service_container');
var UserRepository = require('../repository/user_repository');
var WebSocketHandlerBase = require('./web_socket_handler_base');

module.exports = WebSocketHandlerSocketIO;

function WebSocketHandlerSocketIO() {
    if (!(this instanceof WebSocketHandlerSocketIO)) return new WebSocketHandlerSocketIO();
}

//WebSocketHandlerBaseを継承
WebSocketHandlerSocketIO.prototype.__proto__ = WebSocketHandlerBase.prototype;


WebSocketHandlerSocketIO.prototype.webSocketServer = {};
WebSocketHandlerSocketIO.prototype.nameSpace = {};

/**
 * ユーザーIDをキーにした、ソケットIDと接続時刻の配列。
 * 正規のネームスペースへの切り替えが行われていないソケットのみ管理する。
 * (一定時間経過しても正規のネームスペースに切り換えられない場合、切断させるため)
 * @type {object} {%users.login_id%: {socket_id: %ソケットID%, time: %接続時刻(ミリ秒)%}
 */
WebSocketHandlerSocketIO.prototype.pendingConnections = {};

/**
 * users.login_idをキーにしたソケットIDの一覧。
 * 正規のネームスペースへの切り替えが行われたソケットのみ管理する。
 * @type {object} {%users.login_id%: %ソケットID%}
 */
WebSocketHandlerSocketIO.prototype.connections = {};

WebSocketHandlerSocketIO.prototype.logConnections = false;
WebSocketHandlerSocketIO.prototype.pendingConnectionTimeout = 0;
WebSocketHandlerSocketIO.prototype.originName = 'SocketIO';
//WebSocketHandlerSocketIO.prototype.authenticator = null;


WebSocketHandlerSocketIO.prototype.init = function(httpServer) {

    this.webSocketServer = socketio.listen( httpServer, {allowUpgrades: true} );
    this.nameSpace = this.webSocketServer.of(nconf.get('ws:name_space'));
    this.pendingConnectionTimeout = nconf.get('ws:pending_connection_timeout');
    this.logConnections = nconf.get('log:log_connection');

    //誤ったネームスペースが指定された場合に、
    //指定されたネームスペースにDISCONNECTパケットを送信して切断するため、
    //socket.io/lib/client.jsの動作を上書きする。
    this._overrideSocketIoInternalClient();

    var self = this;
    //ネームスペースへの接続時に行う認証
    this.webSocketServer.use(this.authenticate.bind(this));

    //認証成功後に呼び出される処理
    this.webSocketServer.on('connection', function(socket){
        //接続の認証を行い、NGな場合は切断する。
        self.onConnected(socket);

    });

    //正規のネームスペースへの切替時に行う認証
    this.nameSpace.use(this.authenticate.bind(this));
    // 正規のネームスペースに切り替えた際に呼び出される接続処理
    this.nameSpace.on('connection', function (socket) {

        //ここで入ってきたコネクションを通知対象として管理する。
        self.onNameSpaceConnected(socket);

        socket.on('disconnect', function onDisconnect(socket) {
            self.onDisconnect(this.id);
        });
    });

    //ネームスペースが切り換えられていない接続の監視をスタートする。
    setInterval(this.watchPendingConnection.bind(this), 1000);
};


/**
 * 認証処理を行う。Socket.IOのミドルウェアとして使用する。
 * @param {Socket} socket Socket.IOのSocketオブジェクト
 * @param {function} next 次のミドルウェアを実行する関数。
 */
WebSocketHandlerSocketIO.prototype.authenticate = function(socket, next) {
    var self = this;
    co(function *(){

        if(!socket._lcasAuthenticator) {
            socket._lcasAuthenticator = new Authenticator();
        }
        yield socket._lcasAuthenticator.authenticateFromRequest(socket.request);
        next();

    }).catch(function(err){
        logger.addWarn(err);

        //認証NGの場合、この時点で接続を拒否する。
        socket.disconnect();
        if(socket.nsp.name == '/') {
            //ネームスペースが"/"の場合のみ、next()にErrorオブジェクトを渡すと
            //クライアントが切断されないため、何も渡さずnext()を呼び出す。
            next();
        } else {
            next(new Error('Authentication error.'));
        }
    });
};

/**
 * WebSocketでの接続開始時に呼び出されるメソッド。
 * (ネームスペース指定ありでもなしでもコールされる)
 *
 * @param {Socket} socket Socket.IOのソケット
 */
WebSocketHandlerSocketIO.prototype.onConnected = function(socket) {

    if(!socket._lcasAuthenticator) {
        logger.addError('Cannot read authentication result from socket. socket_id: ' + socket.id);
        socket.client.disconnect();
        return;
    }
    var userId = socket._lcasAuthenticator.getLoginId();
    if(userId == 0) {
        //認証処理に失敗した状態で呼び出された場合は処理を抜ける。

        //wscatなどSocket.IOのプロトコルを意識しないクライアントを切断するため、
        //socket.client.disconnect()でソケットを強制的に切断する。
        //setTimeoutで時間を空けているのは、以下の理由のため。
        // ・認証の失敗時点でSocket.IOのDISCONNECTパケットが送信される時間を待つため
        // ・ネームスペースが指定されていた場合に、指定されている全ネームスペースにDISCONNECTパケットを
        //   送信した後ソケットを閉じる必要があるため
        // このパケットがSocket.IOクライアントに届く前に切断すると、クライアント側で自動再接続が行われる。)
        setTimeout(function(){
            socket.client.disconnect();
        }, 100);
        return;
    }

    //ペンディングリストに登録する。
    //(一定時間経っても正規のネームスペースに切り替わらない場合は切断するため)
    this.addPendingConnectionList(userId, socket);

    if(this.logConnections) {
        logger.addDebug('WEBSOCKET CONNECT(SocketIO): Connection Accepted. socket_id: ' + socket.id);
    }
};

/**
 * WebSocket接続の確立後、ネームスペース切替時に呼び出されるメソッド。
 * @param socket
 */
WebSocketHandlerSocketIO.prototype.onNameSpaceConnected = function(socket) {
    var self = this;

    if(this.logConnections) {
        logger.addDebug('WEBSOCKET CONNECT(SocketIO): Name space changed. ' + socket.id);
    }

    //認証失敗時はこの処理は呼び出されないため、この時点では認証が成功していることが保証されている。
    if(!socket._lcasAuthenticator) {
        logger.addError('Name space connected: Cannot read authentication result from socket. socket_id: ' + socket.id);
        socket.client.disconnect();
        return;
    }

    //ネームスペースの切り替えに成功したため、一時接続リストからは削除する。
    this.removeSocketFromPendingList(socket.id);

    //通知の対象として接続一覧に追加する。
    this.addSocket(socket._lcasAuthenticator.getLoginId(), socket.id);

    //-- デバッグ用 --------------------------------------
    if(process.env['NODE_ENV'] != 'production') {
        //ダミーのAMQP通知受信処理
        //受け取ったメッセージをAMQP通知テーブルに記録し、AMQPメッセージの受信をエミュレートする。
        socket.on('test_notification', this._handleDummyNotification.bind(this));
    }
    //-- /デバッグ用 --------------------------------------
};


/**
 * 正規のネームスペースに切り替えた状態で接続が切断された場合に呼び出される。
 * @param socketId
 */
WebSocketHandlerSocketIO.prototype.onDisconnect = function(socketId) {
    this.removeSocket(socketId);
};


WebSocketHandlerSocketIO.prototype.findSocketIdByUserId = function(userId) {
    if(this.connections[userId]) {
        return this.connections[userId];
    }
    return null;
};


/**
 * 指定されたsocketIdを通知対象として管理する。
 * (通知対象とするのは、正規のネームスペースに切り換えられた接続のみ)
 * @param userId
 * @param socketId
 */
WebSocketHandlerSocketIO.prototype.addSocket = function(userId, socketId) {

    if(this.connections[userId]) {
        //同じユーザーが複数接続しようとした場合、古い接続を切断する。
        var oldSocketId = this.connections[userId];
        if(this.nameSpace.connected[oldSocketId]) {
            if(this.logConnections) {
                logger.addDebug('WEBSOCKET CONNECT(SocketIO): Old connection found: ' + userId + '(' + oldSocketId + ')');
            }
            this.nameSpace.connected[oldSocketId].disconnect();
        }
    }

    this.connections[userId] = socketId;

    //他のハンドラに通知する。
    this.mobileMessagingAdapter.triggerHandlerEvent(
        constants.WS_HANDLER_EVENT_CONNECT,
        {user_id: userId},
        this.originName
    );

    if(this.logConnections) {
        logger.addDebug('WEBSOCKET CONNECT(SocketIO): USER LOGIN: ' + userId + '(' + socketId + ')');
    }
};


/**
 * 指定されたソケットを通知対象から外す
 * @param socketId
 */
WebSocketHandlerSocketIO.prototype.removeSocket = function(socketId) {
    var userId = 0;
    for(userId in this.connections) {
        if(this.connections[userId] == socketId) {
            delete this.connections[userId];
            if(this.logConnections) {
                logger.addDebug('WEBSOCKET CONNECT(SocketIO): DISCONNECT: ' + userId + '(' + socketId + ')');
            }
            return;
        }
    }
    this.removeSocketFromPendingList(socketId);
};


WebSocketHandlerSocketIO.prototype.send = function(userId, notificationType, data) {
    var socketId = this.findSocketIdByUserId(userId);
    var message = {
        notification_type: notificationType,
        content: data
    };
    if(!socketId) {
        return false;
    }
    logger.addDebug('WEBSOCKET MESSAGE: ' + userId + '(' + socketId + '), message: ' + JSON.stringify(message));

    if(this.nameSpace.connected[socketId]) {
        this.nameSpace.to(socketId).send(message);
    }
    return true;
};


/**
 * 他のハンドラから通知されたイベントの処理を行う
 * @param {string} type イベント名
 * @param {object} data データを含んだJSON
 * @param {string} origin イベントの送信元のハンドラ名
 */
WebSocketHandlerSocketIO.prototype.processHandlerEvent = function(type, data, origin) {

    if(origin == this.originName) {
        //発行元が自分の場合は無視する
        return;
    }

    switch(type) {
        case constants.WS_HANDLER_EVENT_CONNECT:
            //他のハンドラでユーザーが接続した。
            //同じユーザーが既に接続中の場合、接続を切断する。

            var userId = data['user_id'] || 0;
            var socketId = this.findSocketIdByUserId(userId);

            if(socketId) {
                this.nameSpace.connected[socketId].disconnect();
                this.removeSocket(socketId);
            }
            break;
        case constants.WS_HANDLER_EVENT_DUMP_CONNECTIONS:
            //コネクションの一覧をダンプするシグナルを受信した。
            logger.log('[SIGNAL RECEIVED] Socket IO active connections:');
            logger.log('-------------------------------------------');
            for(var userId in this.connections) {
                logger.log(userId + ': ' + this.connections[userId]);
            }
            logger.log('-------------------------------------------');
            logger.log('');
            logger.log('[SIGNAL RECEIVED] Socket IO pending connections:');
            logger.log('-------------------------------------------');
            var buffer = {};
            for(var userId in this.pendingConnections) {
                buffer = {
                    socket_id: this.pendingConnections[userId]['socket_id'],
                    time: dateFormat(this.pendingConnections[userId]['time'], 'yyyy-mm-dd HH:MM:ss.l')
                };
                logger.log(userId + ': ' + JSON.stringify(buffer));
            }
            logger.log('-------------------------------------------');
            logger.log('');
            break;
    }
};


/**
 * 接続が確立され、正規のネームスペースへの切り替え待ち状態の接続を登録する。
 * このリストに登録された接続は、一定時間以内に正規のネームスペースに切り換えられない場合切断する。
 * @param {int} userId users.login_id (ログインしていないユーザーの場合、null)
 * @param {Socket} socket
 */
WebSocketHandlerSocketIO.prototype.addPendingConnectionList = function(userId, socket){
    var socketId = socket.id;
    //同じユーザーIDでコネクションが存在する場合、古い方を切断する。
    if(this.pendingConnections[userId]) {
        var oldSocketId = this.pendingConnections[userId]['socket_id'];
        if(this.webSocketServer.sockets.connected[oldSocketId]) {
            this.webSocketServer.sockets.connected[oldSocketId].disconnect();
        }
    }

    var currentTime = new Date();
    this.pendingConnections[userId] = {
        socket_id: socketId,
        time: +currentTime
    };
};


WebSocketHandlerSocketIO.prototype.removeSocketFromPendingList = function(socketId) {
    var userId = 0;

    for(userId in this.pendingConnections) {
        if(this.pendingConnections[userId]['socket_id'] == socketId) {
            delete this.pendingConnections[userId];

            if(this.logConnections) {
                logger.addDebug('WEBSOCKET CONNECT(SocketIO): Remove from pending list: ' + userId + '(' + socketId + ')');
            }
        }
    }
};


/**
 * ネームスペースの切り換えられていないソケットを監視する
 */
WebSocketHandlerSocketIO.prototype.watchPendingConnection = function() {
    var connectedTime = 0;
    var currentTime = new Date();
    var userId = 0;
    var socketId = '';
    for(userId in this.pendingConnections) {
        connectedTime = this.pendingConnections[userId]['time'];
        socketId = this.pendingConnections[userId]['socket_id'];
        //リストに追加されて一定時間経過した接続は、制限時間内に
        //ネームスペースの切り替えがなかったと判断し切断する。
        if(+currentTime - connectedTime > this.pendingConnectionTimeout) {
            if(this.webSocketServer.sockets.connected[socketId]) {
                //ネームスペースの切り換えられていないクライアントを切断するため、
                //client.disconnect()で強制的にソケットを切断する。
                this.webSocketServer.sockets.connected[socketId].client.disconnect();
            }
            this.removeSocket(socketId);
        }
    }
};


/**
 * ダミーの通知を処理する。
 * 受け取ったメッセージをAMQP通知テーブルに記録し、AMQPメッセージの受信をエミュレートする。
 * @param {object} data
 * @private
 */
WebSocketHandlerSocketIO.prototype._handleDummyNotification = function(data){
    var connection = null;
    var zworksNotificationHandler = ServiceContainer.get('zworks_notification_handler');

    DB.getConnection().then(function(con){
        connection = con;
        co(function *(){
            var context = {
                connection: connection
            };
            var notification = {
                data: data
            };
            yield DB.query(connection, 'BEGIN');
            logger.log('Dummy Notification Received: ' + JSON.stringify(notification['data']));
            yield zworksNotificationHandler.onNotification(context, notification);
            yield DB.query(connection, 'COMMIT');
            connection.release();
        }).catch(function(err){
            connection.destroy();
            err = logger.makeError(err);
            logger.addError(err);
        });
    });
};


/**
 * 誤ったネームスペースが指定された場合に接続を切断するよう、
 * socket.io/lib/client.jsのClient::connectにフックする。
 *
 * Socket.IOの標準の動作では不正なネームスペース指定時、Errorパケットを返すのみで、
 * この場合サーバーから接続を切断しても完全には切断が行われない(自動再接続がかかる)ため、
 * 無効なネームスペースが指定された場合DISCONNECTパケットを送ることで正常に接続が切断されるよう対処する。
 *
 * @private
 */
WebSocketHandlerSocketIO.prototype._overrideSocketIoInternalClient = function(){

    var webSocketHandler = this;

    var originalFunc = socketioClient.prototype.connect;
    socketioClient.prototype.connect = function(name){

        //存在しないネームスペースが指定された場合、接続を切断するパケットを返す。
        var packetTypeDisconnect = 1;  // = socket.io-parser: parser.DISCONNECT
        if(!this.server.nsps[name]) {
            logger.addDebug('[DEBUG] Invalid namespace: ' + name + ', socket_id: ' + this.id);
            this.packet({ type: packetTypeDisconnect, nsp: name, data : 'Invalid namespace'});
            webSocketHandler.removeSocketFromPendingList(this.id);
        }

        //オリジナルの処理を実行する
        originalFunc.bind(this)(name);
    };
};
