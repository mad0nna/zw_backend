// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var Authenticator = require('../authenticator');
var co = require('co');
var constants = require('../constants');
var DB = require('../db/db');
var fs = require('fs');
var logger = require('../logger');
var nconf = require('nconf');
var ServiceContainer = require('../service_container');
var UserRepository = require('../repository/user_repository');
var WebSocketHandlerBase = require('./web_socket_handler_base');
var WebSocketServer = require('websocket').server;

module.exports = WebSocketHandlerWebSocket;

function WebSocketHandlerWebSocket() {
    if (!(this instanceof WebSocketHandlerWebSocket)) return new WebSocketHandlerWebSocket();
}

//WebSocketHandlerBaseを継承
WebSocketHandlerWebSocket.prototype.__proto__ = WebSocketHandlerBase.prototype;


WebSocketHandlerWebSocket.prototype.webSocketServer = {};
WebSocketHandlerWebSocket.prototype.connections = {};
WebSocketHandlerWebSocket.prototype.webSocketPath = '';
WebSocketHandlerWebSocket.prototype.logConnections = false;
WebSocketHandlerWebSocket.prototype.originName = 'WebSocket';


WebSocketHandlerWebSocket.prototype.init = function(httpServer) {

    this.webSocketServer = new WebSocketServer({
        httpServer: httpServer,
        autoAcceptConnections: false
    });
    this.logConnections = nconf.get('log:log_connection');
    this.webSocketPath = nconf.get('ws:name_space');

    var self = this;
    //接続時に呼び出される処理
    this.webSocketServer.on('request', function(webSocketRequest){
        //接続の認証を行い、NGな場合は切断する。
        self.onWebSocketRequest(webSocketRequest);
    });

};


/**
 * Check if the request's origin is part of the allowed hosts
 * @param {string} origin Origin of the request
 */
WebSocketHandlerWebSocket.prototype.isOriginAllowed = function(origin) {
    var configFile = nconf.get('origin:allowed_domains_config_file');
    if (configFile == null) {
        configFile = '/var/www/lcas/data/allow-origins.txt';
    }

    var found = false;

    try {
        var domains = fs.readFileSync(configFile).toString().split("\n");
        for (var i in domains) {
            if (domains[i] === origin) {
                found = true;
            } else if (domains[i] === "*") {
                found = true;
            }
        }
    } catch (e) {
        if (e.code === 'ENOENT') {
            logger.addWarn('WEBSOCKET CONNECT(Standard): Allowed domains config file not found (' + configFile + ')');
        } else {
          throw e;
        }
    }

    return found;
};


/**
 * WebSocketでの接続開始時に呼び出されるメソッド。
 * @param {WebSocketRequest} websocketライブラリの WebSocketRequestクラス
 */
WebSocketHandlerWebSocket.prototype.onWebSocketRequest = function(webSocketRequest) {

    var self = this;

    //パスが一致しなければ処理しない
    var resource = webSocketRequest.resource.split('?')[0];
    if(resource != this.webSocketPath) {
        return;
    }

    var checkOrigin = nconf.get('origin:check_origin');
    if (checkOrigin === true && !this.isOriginAllowed(webSocketRequest.origin)) {
        // Make sure we only accept requests from an allowed origin
        webSocketRequest.reject();
        logger.addDebug('WEBSOCKET CONNECT(Standard): Connection from origin '
            + webSocketRequest.origin + ' rejected.');
        return;
    }

    var authenticator = new Authenticator();
    co(function *(){

        if(self.logConnections) {
            logger.addDebug('WEBSOCKET CONNECT(Standard): Accepted: ' + webSocketRequest.resource);
        }

        yield authenticator.authenticateFromRequest(webSocketRequest.httpRequest);

        //認証エラー時に401レスポンスで接続を拒否するため、認証成功の確認後にacceptする。
        var webSocketConnection = null;
        var subProtocol = null;
        webSocketConnection = webSocketRequest.accept(subProtocol, webSocketRequest.origin);

        //認証に成功した場合、コネクションをユーザーIDと紐付けて管理する。
        //Socket.IOと違いソケットIDは存在しないため、コネクションオブジェクトを直接管理する。
        self.addSocket(authenticator.getLoginId(), webSocketConnection);

        webSocketConnection.on('close', function(reasonCode, description){
            self.onDisconnect(this);
        });

    }).catch(function(err){
        //認証NGの場合、この時点で接続を拒否する。
        if(self.logConnections) {
            logger.addDebug('WEBSOCKET CONNECT(Standard): Auth failed:' + err);
        }
        logger.addWarn(err);
        webSocketRequest.reject(401);
    });

};



WebSocketHandlerWebSocket.prototype.onDisconnect = function(connection) {
    this.removeSocket(connection);
};


WebSocketHandlerWebSocket.prototype.findSocketByUserId = function(userId) {
    if(this.connections[userId]) {
        return this.connections[userId];
    }
    return null;
};

WebSocketHandlerWebSocket.prototype.addSocket = function(userId, connection) {

    if(this.connections[userId]) {
        //同じユーザーが複数接続しようとした場合、古い接続を切断する。
        var oldConnection = this.connections[userId];
        oldConnection.drop();
    }

    this.connections[userId] = connection;

    //他のハンドラに通知する。
    this.mobileMessagingAdapter.triggerHandlerEvent(
        constants.WS_HANDLER_EVENT_CONNECT,
        {user_id: userId},
        this.originName
    );

    logger.addDebug(this.logConnections);

    if(this.logConnections) {
        logger.addDebug('WEBSOCKET CONNECT(Standard): USER LOGIN: ' + userId);
    }
};


WebSocketHandlerWebSocket.prototype.removeSocket = function(connection) {
    for(var userId in this.connections) {
        if(this.connections[userId] == connection) {
            delete this.connections[userId];
            if(this.logConnections) {
                logger.addDebug('WEBSOCKET CONNECT(Standard): DISCONNECT: ' + userId);
            }
            return;
        }
    }
};

WebSocketHandlerWebSocket.prototype.send = function(userId, notificationType, data) {
    var connection = this.findSocketByUserId(userId);
    var message = {
        notification_type: notificationType,
        content: data
    };
    if(!connection) {
        return false;
    }
    logger.addDebug('WEBSOCKET MESSAGE(Standard): ' + userId + ', message: ' + JSON.stringify(message));

    if(connection.connected) {
        connection.send(JSON.stringify(message));
    }
    return true;
};


/**
 * 他のハンドラから通知されたイベントの処理を行う
 * @param {string} type イベント名
 * @param {object} data データを含んだJSON
 * @param {string} origin イベントの送信元のハンドラ名
 */
WebSocketHandlerWebSocket.prototype.processHandlerEvent = function(type, data, origin) {

    if(origin == this.originName) {
        //発行元が自分の場合は無視する
        return;
    }

    switch(type) {
        case constants.WS_HANDLER_EVENT_CONNECT:
            //他のハンドラでユーザーが接続した。
            //同じユーザーが既に接続中の場合、接続を切断する。
            var userId = data['user_id'] || 0;
            var websocketConnection = this.findSocketByUserId(userId);

            if(websocketConnection) {
                websocketConnection.drop();
                this.removeSocket(websocketConnection);
            }
            break;
        case constants.WS_HANDLER_EVENT_DUMP_CONNECTIONS:
            //コネクションの一覧をダンプするシグナルを受信した。
            logger.log('[SIGNAL RECEIVED] Standard WebSocket active connections:');
            logger.log('-------------------------------------------');
            for(var userId in this.connections) {
                logger.log(userId);
            }
            logger.log('-------------------------------------------');
            logger.log('');
            break;
    }
};
