// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var transform = require('lodash.transform');
var DB = require('./db/db');
var logger = require('./logger');
var nconf = require('nconf');
var ZworksNotificationRepository = require('./repository/zworks_notification_repository');


function ZworksNotificationAdapter() {
    if (!(this instanceof ZworksNotificationAdapter)) return new ZworksNotificationAdapter();
    this.duration = nconf.get('zworks_notification:watch_duration');
    this.locked = false;
}

ZworksNotificationAdapter.prototype.subscribers = [];
ZworksNotificationAdapter.prototype.duration = [];

ZworksNotificationAdapter.prototype.subscribe = function subscribe(callback) {
    this.subscribers.push(callback);
};


ZworksNotificationAdapter.prototype.watch = function watch() {

    var self = this;

    var watchLoop = setInterval(function(){

        if (!self.locked) { 

            self.locked = true;

            var notifications = null;
            var notificationIds = [];
            DB.getConnection().then(function(connection){
                co(function *(){

                    var startTime = new Date();
                    startTime.setSeconds(startTime.getSeconds()-((self.duration/1000) * 60));
                    var notificationRepository = new ZworksNotificationRepository(connection);
                    notifications = yield notificationRepository.findLogsSince(startTime);
                    var notificationStates = transform(notifications, function(result, notification){
                        result.push(notification['state']);
                    });
                    logger.addDebug(notificationStates);

                    if(notifications.length > 0) {
                        logger.addDebug('NOTIFICATION RECEIVED:');
                    }

                    //他のプロセスが読み取らないよう、
                    //通知ログのステータスを一度"読み込み済"に変更する
                    notificationIds = transform(notifications, function(result, notification){
                        result.push(notification['id']);
                    });
                    yield notificationRepository.updateLogState(notificationIds, 1);

                    self.locked = false;

                    var context = {
                        connection: connection
                    };

                    //SET [SESSION|GLOBAL] のSESSION,GLOBALのどちらも指定しない場合、
                    //次に実行されるトランザクションのみのトランザクション分離レベルを指定する。
                    //https://dev.mysql.com/doc/refman/5.6/ja/set-transaction.html#set-transaction-scope
                    yield DB.query(connection, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
                    yield DB.query(connection, 'BEGIN');
                    yield DB.query(connection, 'savepoint seibu');

                    for(var i = 0; i < notifications.length; i++) {
                        //通知を実行し、通知が成功したら通知ログを処理済に更新する
                        logger.addDebug('NOTIFICATION PROCESS START:' + notifications[i]['id']);
                        logger.addDebug('NOTIFICATION: ' + JSON.stringify(notifications[i]));

                        try {
                            yield self.notify(context, notifications[i]);
                            yield notificationRepository.updateLogState(notifications[i]['id'], 2);
                            yield DB.query(connection, 'savepoint seibu');
                        } catch (err) {
                            yield DB.query(connection, 'ROLLBACK to savepoint seibu');
                            var oldState = notificationStates[i];
                            var newState;
                            
                            if (oldState == 0) {
                                newState = 251;
                            } else if (oldState == 251 || oldState == 252 || oldState == 253 || oldState == 254) {
                                newState = oldState + 1;
                            }
                            yield notificationRepository.updateLogState(notifications[i]['id'], newState);
                            yield DB.query(connection, 'savepoint seibu');
                            err = logger.makeError(err);
                            logger.addError(err);
                        }

                        logger.addDebug('NOTIFICATION PROCESS COMPLETE:' + notifications[i]['id']);
                    }

                    yield DB.query(connection, 'COMMIT');
                    connection.release();
                }).catch(function(err){
                    self.locked = false;
                    connection.destroy();
                    err = logger.makeError(err);
                    logger.addError(err);
                    return Promise.reject(err);
                });
            }).catch(function(err){
                //接続エラー
                self.locked = false;
                err = logger.makeError(err);
                logger.addError(err);
            });

        }

    }, self.duration);
};


ZworksNotificationAdapter.prototype.notify = function notify(context, data) {
    var self = this;
    var promise = co(function *(){
        var ok = false;
        for(var i in self.subscribers) {
            yield self.subscribers[i](context, data);
        }
        return Promise.resolve(true);
    }).catch(function(err){
        err = logger.makeError(err);
        return Promise.reject(err);
    });
    return promise;
};


module.exports = ZworksNotificationAdapter;
