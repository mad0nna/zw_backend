// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var DB = require('./db/db');
var logger = require('./logger');
var ZworksNotificationRepository = require('./repository/zworks_notification_repository');

module.exports = ZworksNotificationHandler;

function ZworksNotificationHandler() {
    if(!(this instanceof ZworksNotificationHandler)) {
        return new ZworksNotificationHandler();
    }
}


ZworksNotificationHandler.prototype.onNotification = function(context, record) {

    //種別に応じてハンドラを生成する
    var self = this;
    var json = JSON.parse(record.data);
    if(!json) {
        return Promise.reject(Error('無効なデータを受信しました。: ' + record.data));
    }
    var notification = self.createNotification(context, json);
    if(!notification) {
        return Promise.reject(Error('無効な通知を受信しました。: ' + record.data['command']));
    }
    //実行する
    return notification.execute().catch(function(err){
        return Promise.reject(err);
    });
};


ZworksNotificationHandler.prototype.createNotification = function(context, data) {
    var notification = null;
    switch(data['command']) {
        case 'device_data':
            notification = require('./notification/device_data_notification')();
            break;
        case 'node_incl':
            notification = require('./notification/node_inclusion_notification')();
            break;
        case 'node_excl':
            notification = require('./notification/node_exclusion_notification')();
            break;
        case 'node_config_update':
            notification = require('./notification/node_config_update_notification')();
            break;
        case 'gw_state_change':
            notification = require('./notification/gateway_state_change_notification')();
            break;
        case 'gw_fw_version':
            notification = require('./notification/gateway_firmware_version_change_notification')();
            break;
        case 'scenario_event':
            notification = require('./notification/scenario_event_notification')();
            break;
        
        case 'new_gw_fw':
            notification = require('./notification/new_gateway_firmware_notification')();
            break;

        case 'pubkey_update':
            notification = require('./notification/pubkey_update_notification')();
            break;

        case 'node_state_change':
            notification = require('./notification/node_state_change_notification')();
            break;

        case 'app_sp_node_config_update':
            notification = require('./notification/app_sp_node_config_update_notification')();
            break;

        case 'set_acl':
            notification = require('./notification/set_acl_notification')();
            break;

        // 下記はws-nodeで通知を受けるが何もしないものを列挙
        case 'gw_fw_update_start':
            notification = require('./notification/empty_notification')();
            break;

        default:
            return false;
    }
    notification.init(context, data);
    return notification;
};
