// vim: ts=4 sw=4 et ff=unix fenc=utf8
'use strict'

var co = require('co');
var constants = require('./lib/constants');
var cookie = require('cookie');
var fs = require('fs');
var http = require( 'http' );
var socketio = require( 'socket.io' );
var nconf = require('nconf');
var DB = require('./lib/db/db');
var logger = require('./lib/logger');
var ServiceContainer = require('./lib/service_container');
var MobileMessagingAdapter = require('./lib/mobile_messaging_adapter');
var MobilePushAdapter = require('./lib/mobile_push_adapter');
var PushTransportSns = require('./lib/transport/push_transport_sns');
var PushTransportSqs = require('./lib/transport/push_transport_sqs');
var PushTransportEmail = require('./lib/transport/push_transport_email');
var WebSocketHandlerSocketIO = require('./lib/websocket/web_socket_handler_socketio');
var WebSocketHandlerWebSocket = require('./lib/websocket/web_socket_handler_web_socket');
var ZworksNotificationAdapter = require('./lib/zworks_notification_adapter');
var ZworksNotificationHandler = require('./lib/zworks_notification_handler');



//---------------------------------
// 設定情報のロード
nconf.file('./config/config.json');
var env_config_path = './config/config_' + process.env['NODE_ENV'] + '.json';
if(fs.statSync(env_config_path).isFile()) {
    nconf.file(env_config_path);
} else {
    console.log('failed to open config file. path:' + env_config_path);
    process.exit(1);
}

updatePidFile(nconf.get('pid_file'), process.pid);

logger.log('Application started. pid=' + process.pid);


//---------------------------------
// DB初期設定
DB.init({
    host: nconf.get('db:host'),
    port: nconf.get('db:port'),
    user: nconf.get('db:user'),
    pass: nconf.get('db:pass'),
    name: nconf.get('db:name')
});


//---------------------------------
// サービスの初期設定

var mobileMessagingAdapter = new MobileMessagingAdapter();
var webSocketHandlerSocketIO = new WebSocketHandlerSocketIO();
var webSocketHandlerWebSocket = new WebSocketHandlerWebSocket();
var zworksNotificationAdapter = new ZworksNotificationAdapter();
var zworksNotificationHandler = new ZworksNotificationHandler();

ServiceContainer.register('mobile_messaging_adapter', mobileMessagingAdapter);
ServiceContainer.register('mobile_push_adapter', new MobilePushAdapter());
ServiceContainer.register('push_transport_sns', new PushTransportSns());
ServiceContainer.register('push_transport_sqs', new PushTransportSqs());
ServiceContainer.register('push_transport_email', new PushTransportEmail());
ServiceContainer.register('web_socket_handler_socketio', webSocketHandlerSocketIO);
ServiceContainer.register('zworks_notification_adapter', zworksNotificationAdapter);
ServiceContainer.register('zworks_notification_handler', zworksNotificationHandler);


//---------------------------------
// アプリケーションの起動処理

//WebSocket用ポートでHTTPサーバーのlisten開始(標準WebSocket用)
var stdHttpServer = http.createServer( function( req, res ) {
    res.writeHead(404, { 'Content-Type' : 'text/html' });
    res.end( '' );
}).listen(nconf.get('ws:std_port'));

//WebSocket用ポートでHTTPサーバーのlisten開始(SocketIO用)
var httpServer = http.createServer( function( req, res ) {
    res.writeHead(404, { 'Content-Type' : 'text/html' });
    res.end( '' );
}).listen(nconf.get('ws:port'));


//標準WebSocketの設定(/ws/v3/mainのリクエストの受け付け開始、各種イベントの受け付け)
webSocketHandlerWebSocket.init(stdHttpServer);

//SocketIOの設定(/socket.io/のリクエストの受付開始、各種イベントの受け付け)
webSocketHandlerSocketIO.init(httpServer);

//WebSocket通知用のAdapterに登録
mobileMessagingAdapter.addHandler(webSocketHandlerWebSocket);
mobileMessagingAdapter.addHandler(webSocketHandlerSocketIO);


//キュー(DBテーブル)監視の開始
zworksNotificationAdapter.subscribe(zworksNotificationHandler.onNotification.bind(zworksNotificationHandler));
zworksNotificationAdapter.watch();


process.on('SIGHUP', function(){
    logger.reopen();
});

process.on('SIGUSR1', function(){
    mobileMessagingAdapter.triggerHandlerEvent(constants.WS_HANDLER_EVENT_DUMP_CONNECTIONS, {}, '');
});


function updatePidFile(path, pid) {
    try {
        fs.writeFileSync(path, pid);
    } catch(e) {
        console.log('Failed to write pid file. path:' + path);
        throw e;
    }
}
